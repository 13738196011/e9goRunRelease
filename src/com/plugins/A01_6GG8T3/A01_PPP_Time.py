#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.interfaceReflect.A01_A1B2C3 import A01_A1B2C3
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.plugins.A01_6GG8T3 import A01_Para
from com.zxy.z_debug import z_debug
class A01_PPP_Time(z_debug):
    strContinue      = "1"   
    strResult        = ""
    def __init__(self):
        pass
    def init_start(self):
        A01_Para.objInit = True
        objA01_abc = A01_A1B2C3()
        if Com_Para.devsys == "linux":
            objA01_abc.param_value2 = "ping "+Com_Para.ServerIP+" -c 5"
            uL = UsAdmin_Log(Com_Para.ApplicationPath, "ping 云服务器:"+objA01_abc.param_value2,"NetWork")
            uL.WriteLog() 
            temValue = objA01_abc.CmdExecLinux()
            bPing = False
            for temPro in temValue:
                uL = UsAdmin_Log(Com_Para.ApplicationPath, "ping 返回值:"+str(temPro.decode(Com_Para.U_CODE)),"NetWork")
                uL.WriteLog() 
                if temPro.decode(Com_Para.U_CODE).find(": seq=") != -1:
                    A01_Para.objPPP = 0
                    bPing = True
                    break
            if  bPing :           
                uL = UsAdmin_Log(Com_Para.ApplicationPath, "ping 结果:"+str(bPing),"NetWork")
                uL.WriteLog()   
            else:
                uL = UsAdmin_Log(Com_Para.ApplicationPath, "ping 结果:===================================================>"+str(bPing),"NetWork")
                uL.WriteLog()        
            if not bPing:
                bFlag = False
                objA01_abc.param_value2 = "ps | grep ppp"
                uL = UsAdmin_Log(Com_Para.ApplicationPath, "ppp 进程查找:"+objA01_abc.param_value2,"NetWork")
                uL.WriteLog()
                temValue = objA01_abc.CmdExecLinux()
                for temPro in temValue:    
                    uL = UsAdmin_Log(Com_Para.ApplicationPath, "ppp 查找返回值:"+str(temPro.decode(Com_Para.U_CODE)),"NetWork")
                    uL.WriteLog()                 
                    for temGetV in temPro.decode(Com_Para.U_CODE).split("    "):
                        if temGetV.find("pppd") == 0:
                            bFlag = True                      
                            break
                if bFlag :
                    uL = UsAdmin_Log(Com_Para.ApplicationPath, "pppd 进程结果:"+str(bFlag),"NetWork")
                    uL.WriteLog()  
                else:
                    uL = UsAdmin_Log(Com_Para.ApplicationPath, "pppd 进程结果:===================================================>"+str(bFlag),"NetWork")
                    uL.WriteLog()          
                if bFlag or A01_Para.objPPP >= 3:
                    pass
                else:   
                    A01_Para.objPPP = A01_Para.objPPP + 1              
                    objA01_abc.param_value2 = "pppd call gprs_dxin &"
                    uL = UsAdmin_Log(Com_Para.ApplicationPath, "pppd 拨号命令启动:"+objA01_abc.param_value2,"NetWork")
                    uL.WriteLog()                    
                    temValue = objA01_abc.CmdExecLinux()
                    for temPro in temValue:    
                        uL = UsAdmin_Log(Com_Para.ApplicationPath, "pppd 拨号命令返回值:"+str(temPro.decode(Com_Para.U_CODE)),"NetWork")
                        uL.WriteLog()
        else:
            pass
