#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import time,json,struct,urllib.parse
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_G46HK9.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_G46HK9.A01_G46HK9_MODBUS import A01_G46HK9_MODBUS
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.common import Com_Para
from com.zxy.z_debug import z_debug
class A01_G46HK9_Time(z_debug):
    strContinue      = "1"   
    strResult        = ""
    def __init__(self):
        pass
    def init_start(self):
        bodc = Bus_Ope_DB_Cent()
        bodc.init_page()    
        temt_station_info = bodc.Get_t_station_info()
        temdbc2 = Db_Common2()
        temA01modbus = A01_G46HK9_MODBUS()
        for tsi in temt_station_info:
            tbMonth = "monitor_data_min"+Com_Fun.GetTime("%Y%m")
            get_time = Com_Fun.GetTime("%Y-%m-%d %H:%M:00")
            update_time = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
            strSqlInsert = "insert into "
            strSqlInsert2 = "insert into monitor_data_minu "
            strSqlUpdate = "update monitor_data_real "
            strSqlValues = "values "
            inputValues = {}                         
            strSqlInsert = strSqlInsert + tbMonth+"(station_code,get_time,update_time"
            strSqlInsert2 = strSqlInsert2+"(station_code,get_time,update_time"
            strSqlUpdate = strSqlUpdate+"set get_time='"+get_time+"',update_time='"+update_time+"'"
            strSqlValues = strSqlValues+"('"+tsi.attstation_code+"','"+get_time+"','"+Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")+"'"
            inputValues["param_value1"] = tsi.attstation_code
            inputValues["param_value2"] = Com_Fun.py_urlencode(get_time)
            temt_modbus_addr = bodc.Get_t_modbus_addr_by_station_id(str(tsi.attmain_id))
            all_value = ""
            m_flag = 0
            iIndex = 0
            for tmda in temt_modbus_addr:
                iIndex = iIndex + 1
                red = temA01modbus.get_data_rtu(tmda.attcom_port,tmda.attmodbus_addr, tmda.attmodbus_begin, tmda.attmodbus_length)
                data_val = "" 
                if len(red) == 0:      
                    data_val = "-1"
                else:
                    data_val = str(format(float(red[0])/10,".4f"))
                all_value = all_value + data_val               
                strSqlInsert = strSqlInsert +","+tmda.attelement_val
                strSqlInsert2 = strSqlInsert2 +","+tmda.attelement_val
                strSqlValues = strSqlValues +",'"+data_val+"'"
                strSqlUpdate = strSqlUpdate +","+tmda.attelement_val+"='"+data_val+"'"
                inputValues["param_value"+str(iIndex+2)] = data_val
                time.sleep(0.5)
            inputValues["param_value05"] = ""
            inputValues["param_value06"] = ""
            inputValues["param_value07"] = ""
            inputValues["param_value08"] = ""
            inputValues["param_value09"] = ""
            inputValues["param_value10"] = ""
            inputValues["param_value11"] = ""
            inputValues["param_value12"] = ""
            inputValues["param_value13"] = ""
            inputValues["param_value14"] = ""
            inputValues["param_value15"] = ""
            inputValues["param_value16"] = ""
            inputValues["param_value17"] = ""
            inputValues["param_value18"] = ""
            inputValues["param_value19"] = ""
            inputValues["param_value20"] = ""
            inputValues["param_value21"] = ""
            inputValues["param_value22"] = ""
            if len(all_value) > 0:
                if bodc.Get_monitor_data_real_true(tsi.attstation_code) == True:
                    temdbc2.CommonExec_Sql(strSqlUpdate+" where station_code ='"+tsi.attstation_code+"'")
                else:
                    temdbc2.CommonExec_Sql(strSqlInsert2+",m_flag) "+strSqlValues+",'"+str(m_flag)+"')")
                temdbc2.CommonExec_Sql(strSqlInsert2+",m_flag) "+strSqlValues+",'"+str(m_flag)+"')")
                temdbc2.CommonExec_Sql(strSqlInsert+",m_flag) "+strSqlValues+",'"+str(m_flag)+"')")