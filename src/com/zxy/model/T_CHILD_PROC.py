#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class T_CHILD_PROC(object):
    attMAIN_ID = -1
    attCHILD_ID = -1
    attINF_CN_NAME = ""
    attINF_EN_NAME = ""
    attS_DESC = ""
    attCREATE_DATE = None
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
