var iTop = 0;
var ileft = 0;
//附件类别
var FILE_TYPE_options = {};

//业务逻辑数据开始
function biz_start() {
	layer.close(ly_index);
    iTop = (winHeight - 300) / 2;
    ileft = (winWidth - 300) / 2;
    $.jgrid.defaults.styleUI = "Bootstrap";   
    /*biz begin*/
    getWindowSize();
	var inputdata = {
	        "param_name": "T01_sel_t_file_type_ISAVA",
	        "session_id": session_id,
	        "login_id": login_id
	};
	get_ajax_baseurl(inputdata, "get_T01_sel_t_file_type_ISAVA");
    /*biz end*/
}

/*biz step begin*/
function get_T01_sel_t_file_type_ISAVA(objTag){
	layer.close(ly_index);
	var objValue = objTag.T01_sel_t_file_type_ISAVA;
	if (Call_QryResult(objValue) == true)
	{
		$.each(objValue, function (i, obj) {
			FILE_TYPE_options[obj.MAIN_ID] = obj.TYPE_NAME;
		});
		init();
	}

    $(window).bind("resize",
        function (){
        var width = $(".jqGrid_wrapper").width();
        $("#t_excel_tag_list").setGridWidth(width);
    });
}
/*biz step end*/

/*页面结束*/
function page_end(){
}

//excel标签表显示列定义
var t_excel_tag = [
	/*table column begin*/
    {        label: '主键',        name: 'MAIN_ID',        width: '30px',        index: 'MAIN_ID',        editable: false,        key: true,        readOnly: true,        editrules: {required: true}    }    ,{        label: '单元格标签名称',        name: 'TAG_NAME',        width: '70px',        editable: true,        editrules: true    }    ,{        label: '附件类别ID',        name: 'FILE_TYPE_ID',        width: '40px',        editable: true,        editrules: true,
        edittype: 'select',
        formatter: 'select',
        editoptions: {
            value: FILE_TYPE_options
        }    }    ,{        label: '行起始标签',        name: 'ROW_TAG',        width: '70px',        editable: true,        editrules: true    }    ,{        label: '列起始标签',        name: 'COLUMN_TAG',        width: '70px',        editable: true,        editrules: true    }    ,{        label: '数据单元格上邻标签',        name: 'CELL_UP',        width: '70px',        editable: true,        editrules: true    }    ,{        label: '数据单元格下邻标签',        name: 'CELL_DOWN',        width: '70px',        editable: true,        editrules: true    }    ,{        label: '数据单元格左邻标签',        name: 'CELL_LEFT',        width: '70px',        editable: true,        editrules: true    }    ,{        label: '数据单元格右邻标签',        name: 'CELL_RIGHT',        width: '70px',        editable: true,        editrules: true    }    ,{        label: '数据单元格类型',        name: 'CELL_TYPE',        width: '40px',        editable: true,        editrules: true,
        edittype: 'select',
        formatter: 'select',
        editoptions: {
            value: {0:"table格式", 1:"list格式"},
            defaultValue: '0'
        },
        formatoptions: {
        	value: {0:"table格式", 1:"list格式"}
        }    }    ,{        label: '数据特殊处理拦截器',        name: 'CELL_SPECIAL',        width: '70px',        editable: true,        editrules: true    }    ,{        label: '是否启用 ',        name: 'IS_AVA',        width: '40px',        editable: true,        editrules: true,        edittype: 'select',        formatter: 'select',        editoptions: {            value: {1: '是',0: '否'},
            defaultValue: '1'        },        formatoptions: {            value: {1: '是',0: '否'}        }    }    ,{        label: '系统时间',        name: 'CREATE_DATE',        width: '70px',        editable: true,        editrules: true,        readOnly: true,        formatter: function (value, row) {return new Date(value).Format('yyyy-MM-dd hh:mm:ss');},        editoptions: {defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')}    }    ,{        label: '备注',        name: 'S_DESC',        width: '70px',        editable: true,        editrules: true    }	/*table column end*/
];

function T01_sel_t_excel_tag(input) {
	//查询失败
    if (Call_QryResult(input.T01_sel_t_excel_tag) == false)
        return false;

    var total = $("#t_excel_tag_list").jqGrid('getGridParam', 'records')
        if (typeof(total) == "undefined") {
            $("#t_excel_tag_list").jqGrid({
                data: input.T01_sel_t_excel_tag,
                datatype: "local",
                height: "450",
                autowidth: true,
                shrinkToFit: true,
                rowNum: 10,
                rowList: [10, 20, 30],
                colModel: t_excel_tag,
                editurl: 'clientArray',
                altRows: true,
                pager: "#t_excel_tag_div",
                viewrecords: true,
                caption: "excel标签表列表",
                //rownumbers:true,
                gridview: true
            });

            //导航后面，增加增删改查按钮
            $("#t_excel_tag_list").jqGrid("navGrid", "#t_excel_tag_div", {
                edit: true,
                add: true,
                del: true,
                search: true,
                view: false,
                position: "left",
                cloneToTop: false
            }, {
                beforeShowForm: function () {
                    $('#MAIN_ID').attr('readOnly', true);
                    /*datetime format begin*/
                    laydate.render({                        elem: '#CREATE_DATE',                        type: 'datetime',                    });                    /*datetime format end*/
                },
                onclickSubmit: updateDbByMainId,
                closeAfterEdit: true
            }, {
                beforeShowForm: function () {
                    /*datetime format begin*/
                    laydate.render({                        elem: '#CREATE_DATE',                        type: 'datetime',                    });                    /*datetime format end*/
                },
                onclickSubmit: insertDb,
                closeAfterAdd: true
            }, {
                top: iTop,
                left: ileft,
                onclickSubmit: delDbByMainId
            }, {
                closeAfterSearch: true
            }, {
                height: 200,
                reloadAfterSubmit: true
            });
        } else {
            $("#t_excel_tag_list").jqGrid("clearGridData");
            $("#t_excel_tag_list").jqGrid('setGridParam', {
                data: input.T01_sel_t_excel_tag
            }).trigger("reloadGrid");
        }
        layer.close(ly_index);
}

//初始化
function init() {
    ly_index = layer.load();   
    var inputdata = {
    	"param_name": "T01_sel_t_excel_tag",
    	"session_id": session_id,
        "login_id": login_id
    };
    get_ajax_baseurl(inputdata, "T01_sel_t_excel_tag");
}

$(document).ready(function () {
    //init_page();
});

function T01_ins_t_excel_tag(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_ins_t_excel_tag) == true)
        init();
}

var insertDb = function () {
    ly_index = layer.load();
    var inputdata = {
        	"param_name": "T01_ins_t_excel_tag",
        	"session_id": session_id,
            "login_id": login_id
            /*insert param begin*/
            ,param_value1:$("#TAG_NAME").val()            ,param_value2:$("#FILE_TYPE_ID").val()            ,param_value3:$("#ROW_TAG").val()            ,param_value4:$("#COLUMN_TAG").val()            ,param_value5:$("#CELL_UP").val()            ,param_value6:$("#CELL_DOWN").val()            ,param_value7:$("#CELL_LEFT").val()            ,param_value8:$("#CELL_RIGHT").val()            ,param_value9:$("#CELL_TYPE").val()            ,param_value10:$("#CELL_SPECIAL").val()            ,param_value11:$("#IS_AVA").val()            ,param_value12:$("#CREATE_DATE").val()            ,param_value13:$("#S_DESC").val()            /*insert param end*/
        };
    get_ajax_baseurl(inputdata, "T01_ins_t_excel_tag");
};

function T01_upd_t_excel_tag(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_upd_t_excel_tag) == true)
        init();
}

var updateDbByMainId = function () {
    var rowid = $("#t_excel_tag_list").jqGrid("getGridParam", "selrow");
    var rowMAIN_ID = $('#t_excel_tag_list').jqGrid('getRowData', rowid).MAIN_ID;
    if (rowid.indexOf("jqg") != -1 || rowMAIN_ID.indexOf("jqg") != -1) {
        swal({
            title: "提示信息",
            text: "无法修改缓存记录，请选择正确修改!"
        });
        return false;
    }
    ly_index = layer.load();
    var inputdata = {
        "param_name": "T01_upd_t_excel_tag",
        "session_id": session_id,
        "login_id": login_id
        /*update param begin*/
            ,param_value1:$("#TAG_NAME").val()            ,param_value2:$("#FILE_TYPE_ID").val()            ,param_value3:$("#ROW_TAG").val()            ,param_value4:$("#COLUMN_TAG").val()            ,param_value5:$("#CELL_UP").val()            ,param_value6:$("#CELL_DOWN").val()            ,param_value7:$("#CELL_LEFT").val()            ,param_value8:$("#CELL_RIGHT").val()            ,param_value9:$("#CELL_TYPE").val()            ,param_value10:$("#CELL_SPECIAL").val()            ,param_value11:$("#IS_AVA").val()            ,param_value12:$("#CREATE_DATE").val()            ,param_value13:$("#S_DESC").val()            ,param_value14:rowMAIN_ID        /*update param end*/
    };
    get_ajax_baseurl(inputdata, "T01_upd_t_excel_tag");
};

function T01_del_t_excel_tag(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_del_t_excel_tag) == true)
        init();
}

var delDbByMainId = function () {
    var rowid = $("#t_excel_tag_list").jqGrid("getGridParam", "selrow");
    var rowMAIN_ID = $('#t_excel_tag_list').jqGrid('getRowData', rowid).MAIN_ID;
    if (rowid.indexOf("jqg") != -1 || rowMAIN_ID.indexOf("jqg") != -1) {
        swal({
            title: "提示信息",
            text: "无法删除缓存记录，请选择正确删除!"
        });
        return false;
    }
    ly_index = layer.load();
    var inputdata = {
            "param_name": "T01_del_t_excel_tag",
            "session_id": session_id,
            "login_id": login_id
            /*delete param begin*/
            ,param_value1:rowMAIN_ID            /*delete param end*/
        };
    get_ajax_baseurl(inputdata, "T01_del_t_excel_tag");
};

$(document).ready(function () {
    //页面初始化
    //init_page();
});

window.onpageshow = function (e) {
    if (e.persisted || (window.performance.navigation.type == 2)) {
        is_history_back = 1;
    } else {
        is_history_back = 0;
    }
    //页面初始化
    init_page();
};