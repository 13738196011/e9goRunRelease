#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import os, time, sys, threading, binascii,importlib
from com.zxy.common import Com_Para
from com.plugins.A01_IHGDW0 import A01_Para
from com.plugins.A01_IHGDW0.A01_Fun import A01_Fun
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.main.Init_Page import Init_Page
from com.plugins.A01_C3T8H8.A01_C3T8H8_MODBUS import A01_C3T8H8_MODBUS
from com.plugins.A01_T5GH04.A01_T5GH04_MODBUS import A01_T5GH04_MODBUS
from com.plugins.A01_ATATAT.A01_ATATAT_PORT import A01_ATATAT_PORT
from com.zxy.interfaceReflect.A01_A1B2C3 import A01_A1B2C3
from urllib.parse import unquote
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.comport.ComDev import ComDev
import urllib.parse
from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.plugins.A01_6GG8T3.A01_PPP_Time import A01_PPP_Time
from com.plugins.A01_IHGDW0.A01_IHGDW0_Time import A01_IHGDW0_Time
from com.plugins.A01_IHGDW0.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_3W3XHW.HJpacket import HJpacket
from com.zxy.comport.ComModBus import ComModBus
from com.plugins.A01_6GG8T3.A01_6GG8T3_MODBUS import A01_6GG8T3_MODBUS
from com.plugins.A01_IHGDW0.monitor_data_model import monitor_data_model
import subprocess, platform
import re,random,datetime
import webbrowser
import time,sys,socket
from com.plugins.A01_TH83KV.A01_TH83KV_Time import A01_TH83KV_Time
from com.plugins.usereflect.Hex_Release import Hex_Release
from com.plugins.usereflect.A01_STATICDBI_IN import A01_STATICDBI_IN
from com.plugins.B01_2HCM66.B01_2HCM66_Time import B01_2HCM66_Time
class e9gomainCmd(object):
    def __init__(self, params):
        pass
    if __name__ == '__main__':
        Com_Para.ApplicationPath = os.path.abspath(os.path.dirname(__file__))
        if Com_Para.ApplicationPath[len(Com_Para.ApplicationPath) - 1 :len(Com_Para.ApplicationPath)] == "\\" or Com_Para.ApplicationPath[len(Com_Para.ApplicationPath) - 1 :len(Com_Para.ApplicationPath)] == "/":
            Com_Para.ApplicationPath = Com_Para.ApplicationPath[0 :len(Com_Para.ApplicationPath) - 1]
        sysstr = platform.system()
        if sysstr.lower() == "windows":
            Com_Para.devsys = "windows"
            Com_Para.zxyPath = "\\"
        elif sysstr.lower() == "linux":
            Com_Para.devsys = "linux"
            Com_Para.zxyPath = "/"
        if Com_Para.devsys == "linux":
            if __file__.find("/") == -1:
                mpy = __file__
            else:
                mpy = __file__[__file__.rindex("/") + 1: len(__file__)]
            objA01_abc = A01_A1B2C3()
            objA01_abc.param_value2 = "ps | grep python3"
            temValue = objA01_abc.CmdExecLinux()
            pid = os.getpid()
            for temPro in temValue:
                bFlag = True
                for temV in temPro.decode(Com_Para.U_CODE).split(" "):
                    if temV == str(pid):
                        bFlag = False
                    if temV.find(str(mpy)) != -1 and bFlag:
                        print("program run in threads,exit")
                        sys.exit(0)
        endtime = datetime.datetime.now() + datetime.timedelta(seconds=5)
        while True:
            starttime = datetime.datetime.now()
            if starttime >= endtime :
                break
            else:
                if int(Com_Fun.GetTime("%S")) % 5 == 0: 
                    print("delay run:"+Com_Fun.GetTimeDef())
                time.sleep(1)
        print("GetData Main begin:" + Com_Fun.GetTimeDef())    
        Init_Page.Init_Load()    
        bodc = Bus_Ope_DB_Cent()
        bodc.init_page()
        Init_Page.Start_ComPort()
        Init_Page.Start_Http()
        Init_Page.Start_Server()
        Init_Page.Start_Client()
        Init_Page.Start_MQTT()
        Init_Page.Start_TimeTask(5)
        Init_Page.Start_TimeLoop()
        temA01modbus = ComModBus()
        print("GetData Main end:" + Com_Fun.GetTimeDef())