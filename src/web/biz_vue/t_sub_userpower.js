//选择某一行
var A29E3D_select_t_sub_userpower_rowId = "";
//按钮事件新增或编辑
var A29E3D_type = "";
//其他页面传到本页面参数
var A29E3D_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var A29E3D_tem_SUB_USER_ID = "0";var A29E3D_tem_PROC_ID = "-1";
/*declare query param end*/

//主从表传递参数
function A29E3D_param_set(){
	/*Main Subsuv Table Param Begin*/
	if(A29E3D_param.hasOwnProperty("PROC_ID_cn_name"))
		$("#A29E3D_find_PROC_ID_cn_name").val(s_decode(A29E3D_param["PROC_ID_cn_name"]));
	if(A29E3D_param.hasOwnProperty("PROC_ID"))
		$("#A29E3D_find_PROC_ID").val(A29E3D_param["PROC_ID"]);
	if(A29E3D_param.hasOwnProperty("hidden_find")){
		$("#A29E3D_Ope_PROC_ID").hide();
		$("#A29E3D_Clear_PROC_ID").hide();
	}
	/*Main Subsuv Table Param end*/
}

/*定义下拉框集合定义*/
/*declare select options begin*/
var A29E3D_ary_SUB_USER_ID = null;var A29E3D_ary_PROC_ID = null;var A29E3D_ary_LIMIT_TAG = [{'MAIN_ID': '1', 'CN_NAME': '年'}, {'MAIN_ID': '2', 'CN_NAME': '月'}, {'MAIN_ID': '3', 'CN_NAME': '日'}];
/*declare select options end*/

//业务逻辑数据开始
function A29E3D_t_sub_userpower_biz_start(inputparam) {
	layer.close(ly_index);
	A29E3D_param = inputparam;
	A29E3D_param_set();
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_sub_userpower$SUB_USER_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A29E3D_get_N01_t_sub_userpower$SUB_USER_ID");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function A29E3D_format_SUB_USER_ID(value, row, index) {    var objResult = value;    for(i = 0; i < A29E3D_ary_SUB_USER_ID.length; i++) {        var obj = A29E3D_ary_SUB_USER_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A29E3D_format_PROC_ID(value, row, index) {    var objResult = value;    for(i = 0; i < A29E3D_ary_PROC_ID.length; i++) {        var obj = A29E3D_ary_PROC_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A29E3D_format_LIMIT_TAG(value, row, index) {    var objResult = value;    for(i = 0; i < A29E3D_ary_LIMIT_TAG.length; i++) {        var obj = A29E3D_ary_LIMIT_TAG[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A29E3D_get_N01_t_sub_userpower$SUB_USER_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_sub_userpower$SUB_USER_ID) == false)        return false;    A29E3D_ary_SUB_USER_ID = input.N01_t_sub_userpower$SUB_USER_ID;    var inputdata = {        "param_name": "N01_t_proc_inparam$PROC_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A29E3D_get_N01_t_proc_inparam$PROC_ID");}function A29E3D_get_N01_t_proc_inparam$PROC_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_inparam$PROC_ID) == false)        return false;    A29E3D_ary_PROC_ID = input.N01_t_proc_inparam$PROC_ID;    $("#A29E3D_qry_SUB_USER_ID").append("<option value='-1'></option>")    $.each(A29E3D_ary_SUB_USER_ID, function (i, obj) {        addOptionValue("A29E3D_qry_SUB_USER_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    A29E3D_init_t_sub_userpower();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function A29E3D_PROC_ID_cn_name_fun(){    index_subhtml = "t_proc_name.vue"    random_subhtml = "A1BCF7";    if(loadHtmlSubVueFun("biz_vue/t_proc_name.vue","A29E3D_t_sub_userpower_call_vue") == true){        var n = Get_RandomDiv("A1BCF7","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#A1BCF7_t_proc_name_Events').bootstrapTable('resetView');                A1BCF7_param["ly_index"] = index;                A1BCF7_param["target_name"] = "A29E3D_find_PROC_ID_cn_name"                A1BCF7_param["target_id"] = "A29E3D_find_PROC_ID"                A1BCF7_param["sourc_id"] = "MAIN_ID"                A1BCF7_param["sourc_name"] = "INF_CN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function A29E3D_page_end(){
}

//平台用户授权显示列定义
var A29E3D_t_sub_userpower = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '平台用户信息',        field: 'SUB_USER_ID',        sortable: true        ,formatter: A29E3D_format_SUB_USER_ID    },    {        title: '接口名称',        field: 'PROC_ID',        sortable: true        ,formatter: A29E3D_format_PROC_ID    },    {        title: '使用有效期',        field: 'LIMIT_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '接口调用次数',        field: 'USE_LIMIT',        sortable: true    },    {        title: '调用次数限制',        field: 'LIMIT_NUMBER',        sortable: true    },    {        title: '次数限制类型',        field: 'LIMIT_TAG',        sortable: true        ,formatter: A29E3D_format_LIMIT_TAG    },    {        title: '初次使用时间',        field: 'FIRST_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    }	
	/*table column end*/
];

//页面初始化
function A29E3D_init_t_sub_userpower() {
	$(window).resize(function () {
		  $('#A29E3D_t_sub_userpower_Events').bootstrapTable('resetView');
	});
	//平台用户授权查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#A29E3D_btn_t_sub_userpower_query').click();
}

//查询接口
function A29E3D_t_sub_userpower_query() {
    $('#A29E3D_t_sub_userpower_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_sub_userpower",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": A29E3D_tem_SUB_USER_ID        ,"param_value2": A29E3D_tem_SUB_USER_ID        ,"param_value3": A29E3D_tem_PROC_ID        ,"param_value4": A29E3D_tem_PROC_ID		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "A29E3D_get_N01_sel_t_sub_userpower");
}

//查询结果
function A29E3D_get_N01_sel_t_sub_userpower(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_sub_userpower) == false)
        return false;    
	var s_data = input.N01_sel_t_sub_userpower;
    A29E3D_select_t_sub_userpower_rowId = "";
    $('#A29E3D_t_sub_userpower_Events').bootstrapTable('destroy');
    $("#A29E3D_t_sub_userpower_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#A29E3D_t_sub_userpower_Events').find("tr.changeColor").removeClass('changeColor');
                A29E3D_select_t_sub_userpower_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#A29E3D_t_sub_userpower_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                A29E3D_select_t_sub_userpower_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(A29E3D_param.hasOwnProperty("target_name"))
			{
				$("#"+A29E3D_param["target_id"]).val(eval("row."+A29E3D_param["sourc_id"].toUpperCase()));
				$("#"+A29E3D_param["target_name"]).val(s_decode(eval("row."+A29E3D_param["sourc_name"].toUpperCase())));				
				layer.close(A29E3D_param["ly_index"]);
			}
		},
        toolbar: "#A29E3D_t_sub_userpower_Toolbar",
        columns: A29E3D_t_sub_userpower,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#A29E3D_btn_t_sub_userpower_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    A29E3D_tem_SUB_USER_ID = "0";    A29E3D_tem_PROC_ID = "-1";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    B29E3D_ary_SUB_USER_ID = null;    B29E3D_ary_PROC_ID = null;    $("#A29E3D_find_PROC_ID_cn_name").val("");    $("#A29E3D_find_PROC_ID").val("-1");
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_sub_userpower$SUB_USER_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A29E3D_get_N01_t_sub_userpower$SUB_USER_ID");	
    /*biz end*/
})

//查询按钮
$('#A29E3D_btn_t_sub_userpower_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    A29E3D_tem_SUB_USER_ID = $("#A29E3D_qry_SUB_USER_ID").val();    A29E3D_tem_PROC_ID = $("#A29E3D_find_PROC_ID").val();	
    /*set query param end*/
	A29E3D_t_sub_userpower_query();
})

//vue回调
function A29E3D_t_sub_userpower_call_vue(objResult){
	if(index_subhtml == "t_sub_userpower_$.vue")
	{
		var n = Get_RandomDiv("B29E3D",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":A29E3D_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temSUB_USER_ID = $("#A29E3D_qry_SUB_USER_ID").val();                if(temSUB_USER_ID != ""){                    inputdata["SUB_USER_ID"] = temSUB_USER_ID;                }                var temPROC_ID = $("#A29E3D_find_PROC_ID_cn_name").val();                if(temPROC_ID != ""){                    inputdata["PROC_ID_cn_name"] = temPROC_ID;                    inputdata["PROC_ID"] = $("#A29E3D_find_PROC_ID").val();                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_sub_userpower_$.js","B29E3D_t_sub_userpower_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_proc_name.vue"){        var n = Get_RandomDiv("A1BCF7",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":A29E3D_type,                    "ly_index":index,                    "target_name":"A29E3D_find_PROC_ID_cn_name",                    "target_id":"A29E3D_find_PROC_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"INF_CN_NAME"                };                loadScript_hasparam("biz_vue/t_proc_name.js","A1BCF7_t_proc_name_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//新增按钮
$("#A29E3D_btn_t_sub_userpower_add").click(function () {
	A29E3D_type = "add";
	index_subhtml = "t_sub_userpower_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_sub_userpower_$.vue","A29E3D_t_sub_userpower_call_vue") == true){
		var n = Get_RandomDiv("B29E3D","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				B29E3D_param["type"] = A29E3D_type;
				B29E3D_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temSUB_USER_ID = $("#A29E3D_qry_SUB_USER_ID").val();                if(temSUB_USER_ID != ""){                    B29E3D_param["SUB_USER_ID"] = temSUB_USER_ID;                }                var temPROC_ID = $("#A29E3D_find_PROC_ID_cn_name").val();                if(temPROC_ID != ""){                    B29E3D_param["PROC_ID_cn_name"] = temPROC_ID;                    B29E3D_param["PROC_ID"] = $("#A29E3D_find_PROC_ID").val();                }				
				/*Send Two FindSelect param end*/

				B29E3D_clear_edit_info();
			},
			end: function(){
				B29E3D_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#A29E3D_btn_t_sub_userpower_edit").click(function () {
	if (A29E3D_select_t_sub_userpower_rowId != "") {
		A29E3D_type = "edit";
		index_subhtml = "t_sub_userpower_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_sub_userpower_$.vue","A29E3D_t_sub_userpower_call_vue") == true){
			var n = Get_RandomDiv("B29E3D","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					B29E3D_param["type"] = A29E3D_type;
					B29E3D_param["ly_index"] = index;
					B29E3D_clear_edit_info();
					B29E3D_get_edit_info();
				},
				end: function(){
					B29E3D_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#A29E3D_btn_t_sub_userpower_delete').click(function () {
	//单行选择
	var rowData = $("#A29E3D_t_sub_userpower_Events").bootstrapTable('getData')[A29E3D_select_t_sub_userpower_rowId];
	//多行选择
	var rowDatas = A29E3D_sel_row_t_sub_userpower();
	if (A29E3D_select_t_sub_userpower_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_sub_userpower",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "A29E3D_N01_del_t_sub_userpower");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_sub_userpower",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "A29E3D_N01_del_t_sub_userpower");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function A29E3D_N01_del_t_sub_userpower(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_sub_userpower) == true)
		A29E3D_t_sub_userpower_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function A29E3D_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function A29E3D_sel_row_t_sub_userpower(){
	//获得选中行
	var checkedbox= $("#A29E3D_t_sub_userpower_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}