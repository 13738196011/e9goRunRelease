#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import urllib.request,os
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.interfaceReflect.A01_A1B2C3 import A01_A1B2C3
from com.zxy.z_debug import z_debug
class GetVersion(z_debug):
    def __init__(self):
        pass
    def SaveRemoteFile(self,inputFileName,inputLocalFileName):
        try:
            temResult = urllib.request.urlretrieve(Com_Para.urlPath+"/"+inputFileName,Com_Para.ApplicationPath+Com_Para.zxyPath+inputLocalFileName)
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        return temResult
    def SaveVersionFile(self,version_no,inputLocalFileName):
        temStrTFileName = Com_Para.ApplicationPath+Com_Para.zxyPath+inputLocalFileName
        temFile = None
        try: 
            temFile = open(temStrTFileName, 'w',encoding=Com_Para.U_CODE)
            temFile.write(version_no)
            temFile.close()
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if temFile != None:
                temFile.close()
    def CheckVersion(self):
        temResult = self.GetRequestWeb()
        if temResult == "":
            return 0
        temUL = UsAdmin_Log(Com_Para.ApplicationPath,temResult,0)
        temVersion = temUL.ReadFile(Com_Para.ApplicationPath+Com_Para.zxyPath + "version.txt")
        temAryTem = []
        temVersion_No = ""
        if temVersion == "" and temResult != "":
            temAryTem = temResult.split("\r\n")
            for temLine in temAryTem:
                if temLine.find("version=") != -1:
                    temVersion_No = temLine
                elif temLine != "":
                    self.SaveRemoteFile(temLine,"back/"+temLine)
                    self.CopyFile(temLine) 
            if temVersion_No != "":
                self.SaveVersionFile(temVersion_No,"version.txt")      
        elif temResult != "":
            localVersion = ""
            temAryTem = temVersion.split("\n")
            for temLine in temAryTem:
                temArySub = temLine.split("=")
                if len(temArySub) > 1 and temArySub[0] == "version":
                    localVersion = temArySub[1]
                    break 
            bFlag = False
            temAryTem = temResult.split("\r\n")
            for temLine in temAryTem:
                temArySub = temLine.split("=")
                if len(temArySub) > 1 and temArySub[0] == "version":
                    try:
                        if int(localVersion) >= int(temArySub[1]):
                            bFlag = False
                        else:
                            temVersion_No = temLine
                            bFlag = True
                    except:
                        bFlag = False
                        break
                else:
                    if bFlag:
                        self.SaveRemoteFile(temLine,"back/"+temLine)
                        self.CopyFile(temLine)    
            if bFlag and temVersion_No != "":
                self.SaveVersionFile(temVersion_No,"version.txt")     
        return 1
    def RebootProgram(self):        
        aa = A01_A1B2C3()
        if Com_Para.devsys.lower() == "linux": 
            aa.param_value2 = "reboot -f"
            aa.CmdExecLinux()
        else:
            aa.RestartPro()
    def CopyFile(self,inputWebFile):
        temAryTem = inputWebFile.split("\r\n")
        for temLine in temAryTem:
            if temLine.find("=") == -1:
                aa = A01_A1B2C3()
                temFileName = temLine[0:temLine.find(".")]
                temFileAtt = temLine[temLine.find("."):len(temLine)].upper()
                if temFileAtt.upper() == ".ZIP":
                    aa.param_value2 = "unzip -o "+Com_Para.ApplicationPath+Com_Para.zxyPath+"back"+Com_Para.zxyPath+temLine+" -d "+Com_Para.ApplicationPath+Com_Para.zxyPath+"back"+Com_Para.zxyPath+temFileName+Com_Para.zxyPath
                    aa.param_value3 = Com_Para.ApplicationPath+Com_Para.zxyPath+"back"+Com_Para.zxyPath+temLine
                    aa.param_value4 = Com_Para.ApplicationPath+Com_Para.zxyPath+"back"+Com_Para.zxyPath+temFileName+Com_Para.zxyPath
                    aa.unzip_file()
                    if Com_Para.devsys.lower() == "linux":                 
                        aa.param_value2 = "cp -rf "+Com_Para.ApplicationPath+Com_Para.zxyPath+"back"+Com_Para.zxyPath+temFileName+Com_Para.zxyPath+"* "+Com_Para.ApplicationPath+Com_Para.zxyPath
                    else:
                        aa.param_value2 = "xcopy "+Com_Para.ApplicationPath+Com_Para.zxyPath+"back"+Com_Para.zxyPath+temFileName+Com_Para.zxyPath+"*.* "+Com_Para.ApplicationPath+" /E /Y"
                    aa.CmdExecLinux()
                else:
                    if Com_Para.devsys.lower() == "linux":                 
                        aa.param_value2 = "cp -rf "+Com_Para.ApplicationPath+Com_Para.zxyPath+"back"+Com_Para.zxyPath+temLine+" "+Com_Para.ApplicationPath+Com_Para.zxyPath
                    else:
                        aa.param_value2 = "copy "+Com_Para.ApplicationPath+Com_Para.zxyPath+"back"+Com_Para.zxyPath+temLine+" "+Com_Para.ApplicationPath+Com_Para.zxyPath+" /Y"
                    aa.CmdExecLinux()
    def GetRequestWeb(self):
        temResult = ""
        try:
            resp = urllib.request.urlopen(Com_Para.urlPath+"/version.txt")
            temResult = resp.read().decode(Com_Para.U_CODE)        
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+repr(e)
            else:
                temLog = self.debug_info()+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
            temResult = ""
        return temResult