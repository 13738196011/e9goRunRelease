#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import datetime
from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.z_debug import z_debug
class Power_Control(z_debug):
    attOpd                = None
    attTss                = None
    attTpn                = None
    attTsp                = None
    attTsu                = None
    attTsup             = None
    attSub_code        = ""
    attSub_usercode    = ""
    attParam_name        = ""
    attError            = ""
    attSub_IP            = ""
    def __init__(self, intputSsub_code, inputSsub_usercode, inputParam_name, inputSub_ip):        
        attOpd  = Ope_DB_Cent()
        self.attSub_code = intputSsub_code
        self.attSub_usercode = inputSsub_usercode
        self.attParam_name = inputParam_name
        self.attSub_IP = inputSub_ip
        self.attTss = attOpd.Get_T_SUB_SYS(intputSsub_code)
        self.attTpn = attOpd.Get_T_PROC_NAME(inputParam_name,self.attTss.attMAIN_ID)
        self.attTsp = attOpd.Get_T_SUB_POWER(self.attTss.attMAIN_ID,self.attTpn.attMAIN_ID)
        self.attTsu = attOpd.Get_T_SUB_USER(self.attTss.attMAIN_ID,inputSsub_usercode)
        self.attTsup = attOpd.Get_T_SUB_USERPOWER(self.attTsu.attMAIN_ID,self.attTpn.attMAIN_ID)
        self.attError = self.Ope_Power()
    def Ope_Power(self):
        temStrResult = ""
        lgNow = datetime.datetime.now()#Com_Fun.getTimeLong()
        try:
            if self.attTss.attMAIN_ID == -1:
                temStrResult = "{\""+ self.attParam_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"该业务平台code未授权\"}]}"
            elif self.attTpn.attMAIN_ID == -1:
                temStrResult = "{\""+ self.attParam_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"该接口未定义，或未关联平台与接口权限\"}]}"
            elif lgNow > Com_Fun.GetToTimeInput("%Y-%m-%d %H:%M:%S",self.attTss.attLIMIT_DATE+" 00:00:00"):
                temStrResult = "{\""+ self.attParam_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"该业务平台超过使用期限,期限时间为" + self.attTss.attLIMIT_DATE+ "\"}]}"
            elif self.attTsp.attMAIN_ID != -1 and lgNow > Com_Fun.GetToTimeInput("%Y-%m-%d %H:%M:%S",self.attTsp.attLIMIT_DATE+" 00:00:00"):
                temStrResult = "{\""+ self.attParam_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"该业务平台对接口调用超过使用期限,期限时间为:"+ self.attTsp.attLIMIT_DATE+ "\"}]}"
            elif self.attTsu.attMAIN_ID != -1 and lgNow > Com_Fun.GetToTimeInput("%Y-%m-%d %H:%M:%S",self.attTsu.attLIMIT_DATE+" 00:00:00"):
                temStrResult ="{\"" + self.attParam_name + "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"该用户超过使用期限,期限时间为:"+ self.attTsu.attLIMIT_DATE + "\"}]}"
            elif self.attTsup.attMAIN_ID != -1 and lgNow > Com_Fun.GetToTimeInput("%Y-%m-%d %H:%M:%S",self.attTsup.attLIMIT_DATE+" 00:00:00"):
                temStrResult = "{\"" + self.attParam_name + "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"该用户对接口调用超过使用期限,期限时间为:"+ self.attTsup.attLIMIT_DATE+ "\"}]}"
            elif self.attTss.attIP_ADDR != "" and self.attTss.attIP_ADDR != "0.0.0.0" and self.attTss.attIP_ADDR.strip().find("|" + self.attSub_IP + "|") == -1:
                temStrResult = "{\"" + self.attParam_name + "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"该业务平台调用IP未授权,您的IP:"+ self.attSub_IP + "\"}]}"
            elif self.attTsp.attMAIN_ID != -1 and self.attTsp.attUSE_LIMIT > self.attTsp.attLIMIT_NUMBER:
                temStrResult = "{\"" + self.attParam_name + "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"该业务平台超过调用次数,次数限制为"+ self.attTsp.attLIMIT_NUMBER+ "次"
                if self.attTsp.attLIMIT_TAG == "1" and Com_Fun.GetTime("%Y") == Com_Fun.GetStrTimeInput("%Y",self.attTsp.attFIRST_DATE):
                    temStrResult += "每年,本年已调用" + self.attTsp.attUSE_LIMIT + "次\"}]}"
                elif self.attTsp.attLIMIT_TAG == "2" and Com_Fun.GetTime("%Y-%m") == Com_Fun.GetStrTimeInput("%Y-%m",self.attTsp.attFIRST_DATE):
                    temStrResult += "每月,本月已调用" + self.attTsp.attUSE_LIMIT + "次\"}]}"
                elif self.attTsp.attLIMIT_TAG == "3" and Com_Fun.GetTime("%Y-%m-%d") == Com_Fun.GetStrTimeInput("%Y-%m-%d",self.attTsp.attFIRST_DATE):
                    temStrResult += "每日,本日已调用" + self.attTsp.attUSE_LIMIT + "次\"}]}"
                else:
                    temStrResult += "\"}]}"
            elif self.attTsup.attMAIN_ID != -1 and self.attTsup.attUSE_LIMIT > self.attTsup.attLIMIT_NUMBER:
                temStrResult = "{\"" + self.attParam_name + "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"该用户对接口调用超过调用次数,次数限制为"+ self.attTsup.attLIMIT_NUMBER + "次"
                if self.attTsup.attLIMIT_TAG == "1" and Com_Fun.GetTime("%Y") == Com_Fun.GetStrTimeInput("%Y",self.attTsup.attFIRST_DATE):
                    temStrResult += "每年,本年已调用" + self.attTsup.attUSE_LIMIT + "次\"}]}"
                elif self.attTsup.attLIMIT_TAG == "2" and Com_Fun.GetTime("%Y-%m") == Com_Fun.GetStrTimeInput("%Y-%m",self.attTsup.attFIRST_DATE):
                    temStrResult += "每月,本月已调用" + self.attTsup.attUSE_LIMIT + "次\"}]}"
                elif self.attTsup.attLIMIT_TAG == "3" and Com_Fun.GetTime("%Y-%m-%d") == Com_Fun.GetStrTimeInput("%Y-%m-%d",self.attTsup.attFIRST_DATE):
                    temStrResult += "每日,本日已调用" + self.attTsup.attUSE_LIMIT + "次\"}]}"
                else:
                    temStrResult += "\"}]}"
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        return temStrResult