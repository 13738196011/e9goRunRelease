#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import threading
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.model.T_PROC_NAME import T_PROC_NAME
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.B01_2HCM66.B01_2HCM66_Time import B01_2HCM66_Time
from com.zxy.z_debug import z_debug
class A01_BRU8BU_IN(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    temSqlException  = ""
    strContinue      = 1
    def __init__(self):
        pass
    def init_start(self):
        self.strContinue = 0
        self.Ins_t_file_info(self)
        if self.temSqlException != "":
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"excel导入入库操作失败："+self.temSqlException+"\"}]}"
            return
        temfile_id = self.Get_t_file_info_ID(self)
        if self.temSqlException != "":
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"excel导入入库操作失败："+self.temSqlException+"\"}]}"
            return
        self.Ins_t_file_expand(self,temfile_id)
        if self.temSqlException != "":
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"excel导入入库操作失败："+self.temSqlException+"\"}]}"
        else:
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"1\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"excel导入成功等待数据识别\"}]}"
            b2t = B01_2HCM66_Time()
            b2t.param_name = self.param_name
            b2t.param_value1 = self.param_value1
            b2t.param_value2 = self.param_value2
            b2t.param_value3 = self.param_value3
            b2t.param_value4 = self.param_value4
            b2t.param_value5 = self.param_value5
            b2t.param_value6 = str(temfile_id)            
            t2 = threading.Thread(target=b2t.run, name="B01_2HCM66_Time")
            t2.start()           
    def Ins_t_file_expand(self,inputfile_id):
        temSqlIns = "insert into t_file_expand(file_id,date_type,date_begin,date_end,is_ava,create_date) values(?,?,?,?,?,?)"
        temParameters = [inputfile_id,self.param_value4,self.param_value5,self.param_value5,1,Com_Fun.GetTimeDef()]
        temParamTypes = ["INT","STRING","DATE","DATE","INT","DATE"]
        temParamOutName = []
        temParamOutType = []
        temDb2 = Db_Common2()
        temTpn = T_PROC_NAME()
        temTpn.attINF_EN_SQL = temSqlIns        
        temDb2.Common_Sql_Proc("Ins_t_file_info",temParameters,temParamTypes,temParamOutName,temParamOutType,temTpn)
        self.temSqlException = temDb2.attSqlException
    def Ins_t_file_info(self):
        temSqlIns = "insert into t_file_info(file_type_id,file_name,file_url,is_ava,create_date) values(?,?,?,?,?)"
        temParameters = [self.param_value1,self.param_value2,self.param_value3,1,Com_Fun.GetTimeDef()]
        temParamTypes = ["INT","STRING","STRING","INT","DATE"]
        temParamOutName = []
        temParamOutType = []
        temDb2 = Db_Common2()
        temTpn = T_PROC_NAME()
        temTpn.attINF_EN_SQL = temSqlIns        
        temDb2.Common_Sql_Proc("Ins_t_file_info",temParameters,temParamTypes,temParamOutName,temParamOutType,temTpn)
        self.temSqlException = temDb2.attSqlException
    def Get_t_file_info_ID(self):
        temResult = -1
        temSqlIns = "select main_id from t_file_info where file_url = ? order by main_id desc limit 1"
        temParameters = [self.param_value3]
        temParamTypes = ["STRING"]
        temParamOutName = []
        temParamOutType = []
        try:
            temDb2 = Db_Common2()
            temTpn = T_PROC_NAME()
            temTpn.attINF_EN_SQL = temSqlIns        
            temRs = temDb2.Common_Sql_Proc("Get_t_file_info_ID",temParameters,temParamTypes,temParamOutName,temParamOutType,temTpn)
            self.temSqlException = temDb2.attSqlException
            for temItem in temRs:
                temResult = Com_Fun.ZeroNull(temItem[0])
                break 
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+repr(e)
            else:
                temLog = self.debug_info()+repr(e)                
            self.temSqlException = temLog
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog+"\r\n"+temSqlIns)
            uL.WriteLog()
        return temResult