#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_proc_name(object):
    attMAIN_ID = -1
    attDB_ID = 0
    attINF_CN_NAME = ""
    attINF_EN_NAME = ""
    attINF_EN_SQL = ""
    attINF_TYPE = 0
    attS_DESC = ""
    attCREATE_DATE = None
    attREFLECT_IN_CLASS = ""
    attREFLECT_OUT_CLASS = ""
    attIS_AUTHORITY = 0
    lenINF_CN_NAME = 50
    lenINF_EN_NAME = 50
    lenINF_EN_SQL = 5000
    lenS_DESC = 2000
    lenREFLECT_IN_CLASS = 50
    lenREFLECT_OUT_CLASS = 50
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["MAIN_ID"] = "主键"
        temHt["DB_ID"] = "数据源"
        temHt["INF_CN_NAME"] = "接口中文名"
        temHt["INF_EN_NAME"] = "接口英文名称"
        temHt["INF_EN_SQL"] = "SQL语句或存储过程"
        temHt["INF_TYPE"] = "类型"
        temHt["S_DESC"] = "备注"
        temHt["CREATE_DATE"] = "系统时间"
        temHt["REFLECT_IN_CLASS"] = "输入参数拦截器类名"
        temHt["REFLECT_OUT_CLASS"] = "输出参数拦截器类名"
        temHt["IS_AUTHORITY"] = "是否权限认证"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["INF_CN_NAME"] = "接口中文名"
        temHtQry["INF_EN_NAME"] = "接口英文名称"
        temHtQry["IS_AUTHORITY"] = "是否权限认证"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["DB_ID"] = "select MAIN_ID,DB_CN_NAME as CN_NAME from t_db_config"
        temHtSelect["INF_TYPE"] = [{"MAIN_ID":"1","CN_NAME":"存储过程"},{"MAIN_ID":"2","CN_NAME":"SQL语句"}]
        temHtSelect["IS_AUTHORITY"] = [{"MAIN_ID":"1","CN_NAME":"是"},{"MAIN_ID":"0","CN_NAME":"否"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
