//选择某一行
var AD6233_select_t_code_element_rowId = "";
//按钮事件新增或编辑
var AD6233_type = "";
//其他页面传到本页面参数
var AD6233_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var AD6233_tem_element_name = "";var AD6233_tem_element_type = "0";var AD6233_tem_s_state = "0";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var AD6233_ary_element_type = [{'main_id': '1', 'cn_name': '污染源水'}, {'main_id': '2', 'cn_name': '污染源气'}];var AD6233_ary_s_state = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];
/*declare select options end*/

//业务逻辑数据开始
function AD6233_t_code_element_biz_start(inputparam) {
	layer.close(ly_index);
	AD6233_param = inputparam;
    /*biz begin*/
    if($("#AD6233_qry_element_type").is("select") && $("#AD6233_qry_element_type")[0].options.length == 0)    {        $("#AD6233_qry_element_type").append("<option value='-1'></option>")        $.each(AD6233_ary_element_type, function (i, obj) {            addOptionValue("AD6233_qry_element_type", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#AD6233_qry_s_state").is("select") && $("#AD6233_qry_s_state")[0].options.length == 0)    {        $("#AD6233_qry_s_state").append("<option value='-1'></option>")        $.each(AD6233_ary_s_state, function (i, obj) {            addOptionValue("AD6233_qry_s_state", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    AD6233_init_t_code_element()	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function AD6233_format_element_type(value, row, index) {    var objResult = value;    for(i = 0; i < AD6233_ary_element_type.length; i++) {        var obj = AD6233_ary_element_type[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AD6233_format_s_state(value, row, index) {    var objResult = value;    for(i = 0; i < AD6233_ary_s_state.length; i++) {        var obj = AD6233_ary_s_state[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function AD6233_page_end(){
}

//因子设置显示列定义
var AD6233_t_code_element = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '因子编码',        field: 'ELEMENT_CODE',        sortable: true        ,formatter: set_s_decode    },    {        title: '因子名称',        field: 'ELEMENT_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '存储位',        field: 'ELEMENT_VAL',        sortable: true        ,formatter: set_s_decode    },    {        title: '因子类型',        field: 'ELEMENT_TYPE',        sortable: true        ,formatter: AD6233_format_element_type    },    {        title: '因子单位',        field: 'ELEMENT_UNIT',        sortable: true        ,formatter: set_s_decode    },    {        title: '是否启用',        field: 'S_STATE',        sortable: true        ,formatter: AD6233_format_s_state    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '小数点位',        field: 'DOT_NUM',        sortable: true    },    {        title: '报警上限',        field: 'UP_LEVEL',        sortable: true        ,formatter: set_s_decode    },    {        title: '报警下限',        field: 'DOWN_LEVEL',        sortable: true        ,formatter: set_s_decode    },    {        title: '量程上限',        field: 'UP_LIMIT',        sortable: true        ,formatter: set_s_decode    },    {        title: '量程下限',        field: 'DOWN_LIMIT',        sortable: true        ,formatter: set_s_decode    },    {        title: '因子编码2',        field: 'ELEMENT_CODE2',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function AD6233_init_t_code_element() {
	$(window).resize(function () {
		  $('#AD6233_t_code_element_Events').bootstrapTable('resetView');
	});
	//因子设置查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AD6233_btn_t_code_element_query').click();
}

//查询接口
function AD6233_t_code_element_query() {
    $('#AD6233_t_code_element_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_code_element",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(AD6233_tem_element_name)        ,"param_value2": AD6233_tem_element_type        ,"param_value3": AD6233_tem_element_type        ,"param_value4": AD6233_tem_s_state        ,"param_value5": AD6233_tem_s_state		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AD6233_get_N01_sel_t_code_element");
}

//查询结果
function AD6233_get_N01_sel_t_code_element(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_code_element) == false)
        return false;    
	var s_data = input.N01_sel_t_code_element;
    AD6233_select_t_code_element_rowId = "";
    $('#AD6233_t_code_element_Events').bootstrapTable('destroy');
    $("#AD6233_t_code_element_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AD6233_t_code_element_Events').find("tr.changeColor").removeClass('changeColor');
                AD6233_select_t_code_element_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AD6233_t_code_element_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AD6233_select_t_code_element_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AD6233_param.hasOwnProperty("target_name"))
			{
				$("#"+AD6233_param["target_id"]).val(eval("row."+AD6233_param["sourc_id"].toUpperCase()));
				$("#"+AD6233_param["target_name"]).val(s_decode(eval("row."+AD6233_param["sourc_name"].toUpperCase())));				
				layer.close(AD6233_param["ly_index"]);
			}
		},
        toolbar: "#AD6233_t_code_element_Toolbar",
        columns: AD6233_t_code_element,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AD6233_btn_t_code_element_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    AD6233_tem_element_name = "";    AD6233_tem_element_type = "0";    AD6233_tem_s_state = "0";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/

	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    if($("#AD6233_qry_element_type").is("select") && $("#AD6233_qry_element_type")[0].options.length == 0)    {        $("#AD6233_qry_element_type").append("<option value='-1'></option>")        $.each(AD6233_ary_element_type, function (i, obj) {            addOptionValue("AD6233_qry_element_type", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#AD6233_qry_s_state").is("select") && $("#AD6233_qry_s_state")[0].options.length == 0)    {        $("#AD6233_qry_s_state").append("<option value='-1'></option>")        $.each(AD6233_ary_s_state, function (i, obj) {            addOptionValue("AD6233_qry_s_state", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    AD6233_init_t_code_element()	
    /*biz end*/
})

//查询按钮
$('#AD6233_btn_t_code_element_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    AD6233_tem_element_name = $("#AD6233_qry_element_name").val();    AD6233_tem_element_type = $("#AD6233_qry_element_type").val();    AD6233_tem_s_state = $("#AD6233_qry_s_state").val();	
    /*set query param end*/
	AD6233_t_code_element_query();
})

//vue回调
function AD6233_t_code_element_call_vue(objResult){
	if(index_subhtml == "t_code_element_$.vue")
	{
		var n = Get_RandomDiv("BD6233",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AD6233_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temelement_name = $("#AD6233_qry_element_name").val();                if(temelement_name != ""){                    inputdata["element_name"] = temelement_name;                }                var temelement_type = $("#AD6233_qry_element_type").val();                if(temelement_type != ""){                    inputdata["element_type"] = temelement_type;                }                var tems_state = $("#AD6233_qry_s_state").val();                if(tems_state != ""){                    inputdata["s_state"] = tems_state;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_code_element_$.js","BD6233_t_code_element_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//新增按钮
$("#AD6233_btn_t_code_element_add").click(function () {
	AD6233_type = "add";
	index_subhtml = "t_code_element_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_code_element_$.vue","AD6233_t_code_element_call_vue") == true){
		var n = Get_RandomDiv("BD6233","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BD6233_param["type"] = AD6233_type;
				BD6233_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temelement_name = $("#AD6233_qry_element_name").val();                if(temelement_name != ""){                    BD6233_param["element_name"] = temelement_name;                }                var temelement_type = $("#AD6233_qry_element_type").val();                if(temelement_type != ""){                    BD6233_param["element_type"] = temelement_type;                }                var tems_state = $("#AD6233_qry_s_state").val();                if(tems_state != ""){                    BD6233_param["s_state"] = tems_state;                }				
				/*Send Two FindSelect param end*/

				BD6233_clear_edit_info();
			},
			end: function(){
				BD6233_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AD6233_btn_t_code_element_edit").click(function () {
	if (AD6233_select_t_code_element_rowId != "") {
		AD6233_type = "edit";
		index_subhtml = "t_code_element_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_code_element_$.vue","AD6233_t_code_element_call_vue") == true){
			var n = Get_RandomDiv("BD6233","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BD6233_param["type"] = AD6233_type;
					BD6233_param["ly_index"] = index;
					BD6233_clear_edit_info();
					BD6233_get_edit_info();
				},
				end: function(){
					BD6233_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AD6233_btn_t_code_element_delete').click(function () {
	//单行选择
	var rowData = $("#AD6233_t_code_element_Events").bootstrapTable('getData')[AD6233_select_t_code_element_rowId];
	//多行选择
	var rowDatas = AD6233_sel_row_t_code_element();
	if (AD6233_select_t_code_element_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_code_element",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AD6233_N01_del_t_code_element");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_code_element",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AD6233_N01_del_t_code_element");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AD6233_N01_del_t_code_element(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_code_element) == true)
		AD6233_t_code_element_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function AD6233_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AD6233_sel_row_t_code_element(){
	//获得选中行
	var checkedbox= $("#AD6233_t_code_element_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}