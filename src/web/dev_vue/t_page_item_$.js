//按钮事件新增或编辑
var B1A6C5_type = "";
//其他页面传到本页面参数
var B1A6C5_param = {};
//暂时没用
var B1A6C5_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var B1A6C5_ary_PAGE_ID = null;var B1A6C5_ary_PROC_ID = null;var B1A6C5_ary_ITEM_TYPE = [{'MAIN_ID': '1', 'CN_NAME': '表格模式'}, {'MAIN_ID': '2', 'CN_NAME': '列表模式'}];
/*declare select options end*/

//主从表传递参数
function B1A6C5_param_set(){
	/*Main Subsuv Table Param Begin*/
    if(B1A6C5_param.hasOwnProperty("PROC_ID_cn_name"))        $("#B1A6C5_find_PROC_ID_cn_name").val(s_decode(B1A6C5_param["PROC_ID_cn_name"]));    if(B1A6C5_param.hasOwnProperty("PROC_ID"))        $("#B1A6C5_find_PROC_ID").val(B1A6C5_param["PROC_ID"]);    if(B1A6C5_param.hasOwnProperty("hidden_find")){        $("#B1A6C5_Ope_PROC_ID").hide();        $("#B1A6C5_Clear_PROC_ID").hide();    }	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function B1A6C5_t_page_item_biz_start(inputdata) {
	B1A6C5_param = inputdata;
	layer.close(ly_index);
	B1A6C5_param_set();
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_page_item$PAGE_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B1A6C5_get_N01_t_page_item$PAGE_ID");	
    /*biz end*/
}

/*biz step begin*/
function B1A6C5_format_PAGE_ID(value, row, index) {    var objResult = value;    for(i = 0; i < B1A6C5_ary_PAGE_ID.length; i++) {        var obj = B1A6C5_ary_PAGE_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B1A6C5_format_PROC_ID(value, row, index) {    var objResult = value;    for(i = 0; i < B1A6C5_ary_PROC_ID.length; i++) {        var obj = B1A6C5_ary_PROC_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B1A6C5_format_ITEM_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < B1A6C5_ary_ITEM_TYPE.length; i++) {        var obj = B1A6C5_ary_ITEM_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B1A6C5_get_N01_t_page_item$PAGE_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_page_item$PAGE_ID) == false)        return false;    B1A6C5_ary_PAGE_ID = input.N01_t_page_item$PAGE_ID;    if($("#B1A6C5_PAGE_ID").is("select") && $("#B1A6C5_PAGE_ID")[0].options.length == 0)    {        $.each(B1A6C5_ary_PAGE_ID, function (i, obj) {            addOptionValue("B1A6C5_PAGE_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    var inputdata = {        "param_name": "N01_t_proc_inparam$PROC_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B1A6C5_get_N01_t_proc_inparam$PROC_ID");}function B1A6C5_get_N01_t_proc_inparam$PROC_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_inparam$PROC_ID) == false)        return false;    B1A6C5_ary_PROC_ID = input.N01_t_proc_inparam$PROC_ID;    if($("#B1A6C5_PROC_ID").is("select") && $("#B1A6C5_PROC_ID")[0].options.length == 0)    {        $.each(B1A6C5_ary_PROC_ID, function (i, obj) {            addOptionValue("B1A6C5_PROC_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#B1A6C5_ITEM_TYPE").is("select") && $("#B1A6C5_ITEM_TYPE")[0].options.length == 0)    {        $.each(B1A6C5_ary_ITEM_TYPE, function (i, obj) {            addOptionValue("B1A6C5_ITEM_TYPE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    B1A6C5_init_t_page_item();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function B1A6C5_PROC_ID_cn_name_fun(){    index_subhtml = "t_proc_name.vue"    random_subhtml = "";    if(loadHtmlSubVueFun("dev_vue/t_proc_name.vue","B1A6C5_t_page_item_call_vue") == true){        var n = Get_RandomDiv("","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#_t_proc_name_Events').bootstrapTable('resetView');                _param["ly_index"] = index;                _param["target_name"] = "B1A6C5_find_PROC_ID_cn_name"                _param["target_id"] = "B1A6C5_PROC_ID"                _param["sourc_id"] = "MAIN_ID"                _param["sourc_name"] = "INF_CN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function B1A6C5_page_end(){
	if(B1A6C5_param["type"] == "edit"){
		B1A6C5_get_edit_info();
	}
}

//页面初始化方法
function B1A6C5_init_t_page_item() {
	//type = getUrlParam("type");
	if(B1A6C5_param["type"] == "add"){
		$("#B1A6C5_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#B1A6C5_PAGE_ID").val(B1A6C5_param["PAGE_ID"]);        $("#B1A6C5_PROC_ID").val(B1A6C5_param["PROC_ID"]);        $("#B1A6C5_find_PROC_ID_cn_name").val(B1A6C5_param["PROC_ID_cn_name"]);		
		/*Get Find Select param end*/	
	}
	else if(B1A6C5_param["type"] == "edit"){
	
	}
	
	//表单验证
	B1A6C5_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B1A6C5_CREATE_DATE").val() == "")    {        $("#B1A6C5_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B1A6C5_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B1A6C5_page_end();
}

//提交表单数据
function B1A6C5_SubmitForm(){
	if(B1A6C5_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_page_item",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#B1A6C5_PAGE_ID").val()                ,"param_value2": $("#B1A6C5_PROC_ID").val()                ,"param_value3": s_encode($("#B1A6C5_ITEM_NAME").val())                ,"param_value4": $("#B1A6C5_ITEM_TYPE").val()                ,"param_value5": $("#B1A6C5_COLUMN_COUNT").val()                ,"param_value6": $("#B1A6C5_CREATE_DATE").val()                ,"param_value7": s_encode($("#B1A6C5_S_DESC").val())                ,"param_value8": s_encode($("#B1A6C5_URL_IN_PARAM").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B1A6C5_get_N01_ins_t_page_item");
	}
	else if(B1A6C5_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_page_item",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#B1A6C5_PAGE_ID").val()                ,"param_value2": $("#B1A6C5_PROC_ID").val()                ,"param_value3": s_encode($("#B1A6C5_ITEM_NAME").val())                ,"param_value4": $("#B1A6C5_ITEM_TYPE").val()                ,"param_value5": $("#B1A6C5_COLUMN_COUNT").val()                ,"param_value6": $("#B1A6C5_CREATE_DATE").val()                ,"param_value7": s_encode($("#B1A6C5_S_DESC").val())                ,"param_value8": s_encode($("#B1A6C5_URL_IN_PARAM").val())                ,"param_value9": $("#B1A6C5_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B1A6C5_get_N01_upd_t_page_item");
	}
}

//vue回调
function B1A6C5_t_page_item_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_proc_name.vue"){        var n = Get_RandomDiv("",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":B1A6C5_type,                    "ly_index":index,                    "target_name":"B1A6C5_find_PROC_ID_cn_name",                    "target_id":"B1A6C5_find_PROC_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"INF_CN_NAME"                };                loadScript_hasparam("dev_vue/t_proc_name.js","A43B7C_t_proc_name_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//for表单提交
$("#B1A6C5_save_t_page_item_Edit").click(function () {
	$("form[name='B1A6C5_DataModal']").submit();
})

/*修改数据*/
function B1A6C5_get_N01_upd_t_page_item(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_page_item) == true)
	{
		swal("修改数据成功!", "", "success");
		A1A6C5_t_page_item_query();
		B1A6C5_clear_validate();
		layer.close(B1A6C5_param["ly_index"]);
	}
}

/*添加数据*/
function B1A6C5_get_N01_ins_t_page_item(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_page_item) == true)
	{
		swal("添加数据成功!", "", "success");
		A1A6C5_t_page_item_query();
		B1A6C5_clear_validate();
		layer.close(B1A6C5_param["ly_index"]);
	}
}

//取消编辑
$("#B1A6C5_cancel_t_page_item_Edit").click(function () {
	layer.close(B1A6C5_param["ly_index"]);
	B1A6C5_clear_validate();
	$("[id^='B1A6C5_div']").hide();
})

//清除查找框
function B1A6C5_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B1A6C5_clear_validate(){
	$("#B1A6C5_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B1A6C5_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B1A6C5_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B1A6C5_clear_edit_info(){
	var inputs = $("#B1A6C5_DataModal").find('input');
	var selects = $("#B1A6C5_DataModal").find("select");
	var textareas = $("#B1A6C5_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
    B1A6C5_clear_input_cn_name('B1A6C5_find_PROC_ID_cn_name','B1A6C5_PROC_ID')	
	/*input validate clear end*/
	B1A6C5_init_t_page_item();
}

//页面输入框赋值
function B1A6C5_get_edit_info(){
	var rowData = $("#A1A6C5_t_page_item_Events").bootstrapTable('getData')[A1A6C5_select_t_page_item_rowId];
	var inputs = $("#B1A6C5_DataModal").find('input');
	var selects = $("#B1A6C5_DataModal").find("select");
	var textareas = $("#B1A6C5_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"A1A6C5","B1A6C5");
}

//form验证
function B1A6C5_checkFormInput() {
    B1A6C5_validate = $("#B1A6C5_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B1A6C5_main_id: {}            ,B1A6C5_PAGE_ID: {}            ,B1A6C5_PROC_ID: {}            ,B1A6C5_ITEM_NAME: {}            ,B1A6C5_ITEM_TYPE: {}            ,B1A6C5_COLUMN_COUNT: {digits: true,required : true,maxlength:10}            ,B1A6C5_CREATE_DATE: {date: true,required : true,maxlength:19}            ,B1A6C5_S_DESC: {}            ,B1A6C5_URL_IN_PARAM: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B1A6C5_main_id: {}            ,B1A6C5_PAGE_ID: {}            ,B1A6C5_PROC_ID: {}            ,B1A6C5_ITEM_NAME: {}            ,B1A6C5_ITEM_TYPE: {}            ,B1A6C5_COLUMN_COUNT: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,B1A6C5_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B1A6C5_S_DESC: {}            ,B1A6C5_URL_IN_PARAM: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B1A6C5_SubmitForm();
        	return false;
        }
    })
}