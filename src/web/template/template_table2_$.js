var type = "";

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

//业务逻辑数据开始
function biz_start() {
	layer.close(ly_index);
    /*biz begin*/
	
    /*biz end*/
}

/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function page_end(){
}


//页面初始化方法
function init_{template_table_EN}() {
	type = getUrlParam("type");
	if(type == "add"){
		$("#main_id").parent().parent().parent().hide();
	}
	else if(type == "edit"){
	
	}
	
	//表单验证
	checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
	
    /*form datetime init end*/
}

//提交表单数据
function SubmitForm(){
	if(type == "add"){
		var inputdata = {
				"param_name": "T01_ins_{template_table_EN}",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_ins_{template_table_EN}");
	}
	else if(type == "edit"){
		var inputdata = {
				"param_name": "T01_upd_{template_table_EN}",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_upd_{template_table_EN}");
	}
}

//以下为页面表单验证
$("#save_{template_table_EN}_Edit").click(function () {
	$("form[name='DataModal']").submit();
})

/*修改数据*/
function get_T01_upd_{template_table_EN}(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_upd_{template_table_EN}) == true)
	{
        //init();
		parent.swal("修改数据成功!", "", "success");
		parent.{template_table_EN}_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

/*添加数据*/
function get_T01_ins_{template_table_EN}(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_ins_{template_table_EN}) == true)
	{
		parent.swal("添加数据成功!", "", "success");
		parent.{template_table_EN}_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

//取消编辑
$("#cancel_{template_table_EN}_Edit").click(function () {
	var index = parent.layer.getFrameIndex(window.name);
	parent.layer.close(index);
})

function clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

$(document).ready(function () {
});

window.onpageshow = function (e) {
    if (e.persisted || (window.performance.navigation.type == 2)) {
        is_history_back = 1;
    } else {
        is_history_back = 0;
    }
    //页面初始化
    init_page();
};

//form验证
function checkFormInput() {
    $("#DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	SubmitForm();
        	return false;
        }
    })
}