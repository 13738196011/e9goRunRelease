#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import sys,csv,re,xlrd
import collections as col
from openpyxl import load_workbook    
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.plugins.A01_IHGDW0 import A01_Para
from com.plugins.A01_IHGDW0.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.zxy.z_debug import z_debug
class A01_G03KKL_IN(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = 1
    def __init__(self):
        pass
    def init_start(self):
        bFlag = True
        excelFileName = Com_Para.ApplicationPath+Com_Para.zxyPath+"web"+Com_Para.zxyPath+"file"+Com_Para.zxyPath+self.param_value1
        if excelFileName.find(".") != -1 and excelFileName.split(".")[1].lower() == "xls":
            ws = xlrd.open_workbook(excelFileName)
            sheet = ws.sheet_by_index(0)
            val_ary = ["get_time"]
            iIndex = 0;
            bodc = Bus_Ope_DB_Cent()
            temc_type_num = -1
            for i_row in range(sheet.nrows):
                if iIndex == 0 :
                    for j_cell in range(sheet.ncols):
                        for t_code_element in A01_Para.ht_t_code_element:
                            j_cell_value = sheet.cell_value(rowx=i_row,colx=j_cell)
                            if j_cell_value != None and j_cell_value == t_code_element.attelement_name+"("+t_code_element.attelement_unit+")":
                                val_ary.append(t_code_element.attelement_val)
                                break;
                else:
                    if temc_type_num == -1:
                        for j_cell in range(sheet.ncols):
                            c_type = sheet.cell(i_row,j_cell).ctype
                            if c_type == 3:
                                temc_type_num = j_cell
                                break;
                    temResult = bodc.Ins_data2(val_ary,sheet.row_values(rowx=i_row),self.param_value2,temc_type_num)
                    if bFlag:
                        bFlag = temResult
                iIndex = iIndex + 1            
        elif excelFileName.find(".") != -1 and excelFileName.split(".")[1].lower() == "xlsx":
            ws = load_workbook(excelFileName)                        
            sheet = ws.worksheets[0]
            iIndex = 0;
            val_ary = ["get_time"]
            bodc = Bus_Ope_DB_Cent()
            for i_row in  sheet.rows:
                if iIndex == 0 :
                    for j_cell in i_row:
                        for t_code_element in A01_Para.ht_t_code_element:
                            if j_cell.value != None and j_cell.value == t_code_element.attelement_name+"("+t_code_element.attelement_unit+")":
                                val_ary.append(t_code_element.attelement_val)
                                break;
                else:
                    temResult = bodc.Ins_data(val_ary,i_row,self.param_value2)
                    if bFlag:
                        bFlag = temResult
                iIndex = iIndex + 1
        if bFlag:
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"1\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"数据导入成功\"}]}"
        else:
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"数据导入失败\"}]}"
        self.strContinue = 0
