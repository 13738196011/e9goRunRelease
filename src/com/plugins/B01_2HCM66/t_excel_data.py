#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_excel_data(object):
    attmain_id = -1
    attfile_id = 0
    attexcel_tag_id = 0
    attdata_value = ""
    attcreate_date = None
    atts_desc = ""
    lentype_name = 500
    lendata_value = 2000
    lens_desc = 2000
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["file_id"] = "附件ID"  
        temHt["excel_tag_id"] = "Excel标签ID"  
        temHt["data_value"] = "数据内容"  
        temHt["create_date"] = "系统时间"  
        temHt["s_desc"] = "系统备注"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["file_id"] = "附件信息"
        temHtQry["excel_tag_id"] = "Excel标签"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["file_id"] = "select main_id,file_name as cn_name from t_file_info"
        temHtSelect["excel_tag_id"] = "select main_id,tag_name as cn_name from t_excel_tag"
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn