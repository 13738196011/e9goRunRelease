#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.db2.ConnectionPool2 import ConnectionPool2
from com.zxy.db_Self.ConnectionPool_Self import ConnectionPool_Self
from com.zxy.z_debug import z_debug
class DBManager2(z_debug):
    attConn = None
    attConnectionPool = None
    def __init__(self):
        temTdc = Com_Fun.GetHashTable(Com_Para.htDb,2)
        temConnStr = Com_Fun.GetDbUrl(temTdc.attDB_url)
        if temTdc.attDB_DriverClassName == "org.sqlite.JDBC":
            temConnStr = temConnStr.replace("jdbc:sqlite:@",Com_Para.ApplicationPath+ Com_Para.zxyPath)
        if temConnStr == Com_Para.url:
            self.attConnectionPool = ConnectionPool_Self(temTdc.attDB_DriverClassName,temConnStr,temTdc.attDB_username,temTdc.attDB_password)
        else:
            self.attConnectionPool = ConnectionPool2(temTdc.attDB_DriverClassName,temConnStr,temTdc.attDB_username,temTdc.attDB_password)
        try:
            self.attConnectionPool.createPool()
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            pass
    def getConnection(self):
        try:
            self.attConn = self.attConnectionPool.getConnection()
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            return self.attConn
    def returnConnection(self,inputConn):
        return self.attConnectionPool.returnConnection(inputConn)
    @staticmethod
    def closeConnectionPoolTimeOut(self):
        try:
            self.attConnectionPool.closeConnectionPoolTimeOut()
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            pass