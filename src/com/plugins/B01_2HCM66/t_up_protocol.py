#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_up_protocol(object):
    attmain_id = -1
    attport_name = ""
    attcn_name = ""
    attcreate_date = None
    atts_desc = ""
    atts_state = 0
    lent_up_protocol = 50
    lencn_name = 50
    lens_desc = 500
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["port_name"] = "上传协议模块"
        temHt["cn_name"] = "协议名称"
        temHt["create_date"] = "系统时间"
        temHt["s_desc"] = "备注"
        temHt["s_state"] = "是否启用"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["cn_name"] = "协议名称"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["s_state"] = [{"main_id":"1","cn_name":"启用"},{"main_id":"0","cn_name":"禁用"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
