#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class eova_user(object):
    attmain_id = -1
    attid = 0
    attlogin_id = ""
    attlogin_pwd = ""
    attnickname = ""
    attrid = 0
    attS_STATE = 0
    attCREATE_DATE = None
    attS_DESC = ""
    lenlogin_id = 50
    lenlogin_pwd = 50
    lennickname = 50
    lenS_DESC = 500
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["id"] = "主键"
        temHt["login_id"] = "登录账号"
        temHt["login_pwd"] = "登录密码"
        temHt["nickname"] = "用户姓名"
        temHt["rid"] = "所属角色"
        temHt["S_STATE"] = "是否禁用"
        temHt["CREATE_DATE"] = "系统时间"
        temHt["S_DESC"] = "备注"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["login_id"] = "登录账号"
        temHtQry["nickname"] = "用户姓名"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["rid"] = [{"MAIN_ID":"1","CN_NAME":"超级管理员"},{"MAIN_ID":"2","CN_NAME":"系统管理员"},{"MAIN_ID":"3","CN_NAME":"运维人员"},{"MAIN_ID":"4","CN_NAME":"普通用户"}]
        temHtSelect["S_STATE"] = [{"MAIN_ID":"0","CN_NAME":"启用"},{"MAIN_ID":"1","CN_NAME":"禁用"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
