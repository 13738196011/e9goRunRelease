#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import time,json,struct,urllib.parse
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_ATATAT.A01_ATATAT_PORT import A01_ATATAT_PORT
from com.zxy.z_debug import z_debug
class A01_ATATAT_Time(z_debug):
    strContinue      = "1"   
    strResult        = ""
    def __init__(self):
        pass
    def init_start(self):     
        temA01modbus = A01_ATATAT_PORT()
        temA01modbus.get_data_com("AT","AT+CGMR\r\n")
        temA01modbus.get_data_com("AT","AT+CSQ\r\n")
        temA01modbus.get_data_com("AT","AT+CPIN?\r\n")
        temA01modbus.get_data_com("AT","AT+CGDCONT=1,\"ip\",\"3gnet\"\r\n")
        temA01modbus.get_data_com("AT","AT+GTRNDIS=1,1\r\n")
        temA01modbus.get_data_com("AT","AT+GTRNDIS=0,1\r\n")
        temA01modbus.get_data_com("AT","AT+CGACT=0,1\r\n")