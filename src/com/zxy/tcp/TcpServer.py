#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import time
from com.zxy.business.Analyse_Data import Analyse_Data
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.z_debug import z_debug
class TcpServer(z_debug):
    def __init__(self,skey):
        self.skey = skey
    def server_link(self, sock, addr):
        Com_Fun.SetThreadList(self.skey, sock)
        while True:
            temInit_msg = b'' # 初始化流
            try:
                temInit_msg = sock.recv(1024*50) # 接收数据        
                temValue = temInit_msg.decode(Com_Para.U_CODE)
                if temValue == "Exit":
                    Com_Fun.RemoveThreadList(self.skey)
                    if sock is not None:
                        sock.close()
                    break
                elif len(temValue) > 0:
                    ad = Analyse_Data()
                    ad.SubAnalyseRecBytes(temValue,sock,True, sock.getpeername()[0],str(sock.getpeername()[1]))
                elif temValue == "":
                    Com_Fun.RemoveHashTable(Com_Para.dServThreadList,self.skey)
                    if sock is not None:
                        sock.close()
                    break
                time.sleep(0.1)
            except Exception as e:
                Com_Fun.RemoveThreadList(self.ckeys)
                if sock is not None:
                    sock.close()
                if str(type(self)) == "<class 'type'>":
                    self.debug_in(self,repr(e))#打印异常信息
                else:
                    self.debug_in(repr(e))#打印异常信息
                break