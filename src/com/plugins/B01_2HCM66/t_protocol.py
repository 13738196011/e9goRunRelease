#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_protocol(object):
    attmain_id = -1
    attprot_name = ""
    attoem_name = ""
    attcreate_date = None
    atts_desc = ""
    atts_state = 0
    attcom_port = ""
    lenprot_name = 50
    lenoem_name = 50
    lens_desc = 500
    lencom_port = 50
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["prot_name"] = "采集协议模块"
        temHt["oem_name"] = "采集协议名称"
        temHt["create_date"] = "系统时间"
        temHt["s_desc"] = "备注"
        temHt["s_state"] = "是否启用"
        temHt["com_port"] = "串口号"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["prot_name"] = "采集协议模块"
        temHtQry["oem_name"] = "采集协议名称"
        temHtQry["s_state"] = "是否启用"
        temHtQry["com_port"] = "串口号"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["s_state"] = [{"main_id":"1","cn_name":"启用"},{"main_id":"0","cn_name":"禁用"}]
        temHtSelect["com_port"] = "select com_eflag as main_id,com_name || s_desc as cn_name from t_com_port"
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
