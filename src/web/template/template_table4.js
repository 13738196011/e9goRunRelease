//选择某一行
var {$RDA$}_select_{template_table_EN}_rowId = "";
//按钮事件新增或编辑
var {$RDA$}_type = "";
//其他页面传到本页面参数
var {$RDA$}_param = {};

/*定义查询条件变量*/
/*declare query param begin*/

/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

//主从表传递参数
function {$RDA$}_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function {$RDA$}_{template_table_EN}_biz_start(inputparam) {
	layer.close(ly_index);
	{$RDA$}_param = inputparam;
	//主从表传递参数
	{$RDA$}_param_set();	
    /*biz begin*/
	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function {$RDA$}_page_end(){
	{$RDA$}_adjust_tab();
}

//{template_table_CN}显示列定义
var {$RDA$}_{template_table_EN} = [
	{ 
		checkbox:true
	},
	/*table column begin*/
	
	/*table column end*/
];

//页面初始化
function {$RDA$}_init_{template_table_EN}() {
	$(window).resize(function () {
		  $('#{$RDA$}_{template_table_EN}_Events').bootstrapTable('resetView');
	});
	//{template_table_CN}查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#{$RDA$}_btn_{template_table_EN}_query').click();
}

//查询接口
function {$RDA$}_{template_table_EN}_query() {
    $('#{$RDA$}_{template_table_EN}_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_{template_table_EN}",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "{$RDA$}_get_N01_sel_{template_table_EN}");
}

//查询结果
function {$RDA$}_get_N01_sel_{template_table_EN}(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_{template_table_EN}) == false)
        return false;
    //调整table各列宽度
    $.each({$RDA$}_{template_table_EN}, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_{template_table_EN};
    {$RDA$}_select_{template_table_EN}_rowId = "";
    $('#{$RDA$}_{template_table_EN}_Events').bootstrapTable('destroy');
    $("#{$RDA$}_{template_table_EN}_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#{$RDA$}_{template_table_EN}_Events').find("tr.changeColor").removeClass('changeColor');
                {$RDA$}_select_{template_table_EN}_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#{$RDA$}_{template_table_EN}_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                {$RDA$}_select_{template_table_EN}_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#{$RDA$}_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if({$RDA$}_param.hasOwnProperty("target_name"))
			{
				$("#"+{$RDA$}_param["target_id"]).val(eval("row."+{$RDA$}_param["sourc_id"].toUpperCase()));
				$("#"+{$RDA$}_param["target_name"]).val(s_decode(eval("row."+{$RDA$}_param["sourc_name"].toUpperCase())));				
				layer.close({$RDA$}_param["ly_index"]);
			}
		},
        toolbar: "#{$RDA$}_{template_table_EN}_Toolbar",
        columns: {$RDA$}_{template_table_EN},
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
    {$RDA$}_page_end();
}

//刷新按钮
$('#{$RDA$}_btn_{template_table_EN}_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/

	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/

	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
	
    /*biz end*/
})

//查询按钮
$('#{$RDA$}_btn_{template_table_EN}_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
	
    /*set query param end*/
	{$RDA$}_{template_table_EN}_query();
})

//vue回调
function {$RDA$}_{template_table_EN}_call_vue(objResult){
	if(index_subhtml == "{template_table_EN}_$.vue")
	{
		var n = Get_RandomDiv("{$RBC$}",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":{$RDA$}_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/{template_table_EN}_$.js","{$RBC$}_{template_table_EN}_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/

	/*get tab subvue end*/
}

//新增按钮
$("#{$RDA$}_btn_{template_table_EN}_add").click(function () {
	{$RDA$}_type = "add";
	index_subhtml = "{template_table_EN}_$.vue";
	if(loadHtmlSubVueFun("biz_vue/{template_table_EN}_$.vue","{$RDA$}_{template_table_EN}_call_vue") == true){
		var n = Get_RandomDiv("{$RBC$}","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				{$RBC$}_param["type"] = {$RDA$}_type;
				{$RBC$}_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
				
				/*Send Two FindSelect param end*/

				{$RBC$}_clear_edit_info();
			},
			end: function(){
				{$RBC$}_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#{$RDA$}_btn_{template_table_EN}_edit").click(function () {
	if ({$RDA$}_select_{template_table_EN}_rowId != "") {
		{$RDA$}_type = "edit";
		index_subhtml = "{template_table_EN}_$.vue";
		if(loadHtmlSubVueFun("biz_vue/{template_table_EN}_$.vue","{$RDA$}_{template_table_EN}_call_vue") == true){
			var n = Get_RandomDiv("{$RBC$}","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					{$RBC$}_param["type"] = {$RDA$}_type;
					{$RBC$}_param["ly_index"] = index;
					{$RBC$}_clear_edit_info();
					{$RBC$}_get_edit_info();
				},
				end: function(){
					{$RBC$}_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#{$RDA$}_btn_{template_table_EN}_delete').click(function () {
	//单行选择
	var rowData = $("#{$RDA$}_{template_table_EN}_Events").bootstrapTable('getData')[{$RDA$}_select_{template_table_EN}_rowId];
	//多行选择
	var rowDatas = {$RDA$}_sel_row_{template_table_EN}();
	if ({$RDA$}_select_{template_table_EN}_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_{template_table_EN}",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "{$RDA$}_N01_del_{template_table_EN}");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_{template_table_EN}",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "{$RDA$}_N01_del_{template_table_EN}");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function {$RDA$}_N01_del_{template_table_EN}(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_{template_table_EN}) == true)
		{$RDA$}_{template_table_EN}_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function {$RDA$}_adjust_tab(){
	if(typeof($("#{$RDA$}_TAB_MAIN")[0]) != "undefined" && $("#{$RDA$}_TAB_MAIN")[0].length != 0){
		{$RDA$}_Flag = "1";
		$(Get_RDivNoBuild("{$RDA$}","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("{$RDA$}","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("{$RDA$}","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#{$RDA$}_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/

/*Tab Click Fun End*/

//显示tab页选项卡
function {$RDA$}_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("biz_vue/"+inputUrl,"{$RDA$}_{template_table_EN}_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if ({$RDA$}_select_{template_table_EN}_rowId != "") 
			rowData = $("#{$RDA$}_{template_table_EN}_Events").bootstrapTable('getData')[{$RDA$}_select_t_proc_name_rowId];
		else
			rowData = $("#{$RDA$}_{template_table_EN}_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			$.each(temPar, function (i, obj) {
				eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
		}
		var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
		$("#"+inputrandom+"_btn_"+tbName+"_query").click();
	}
}

//清除 查找框
function {$RDA$}_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function {$RDA$}_sel_row_{template_table_EN}(){
	//获得选中行
	var checkedbox= $("#{$RDA$}_{template_table_EN}_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}