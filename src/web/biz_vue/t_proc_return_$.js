//按钮事件新增或编辑
var B20D9D_type = "";
//其他页面传到本页面参数
var B20D9D_param = {};
//暂时没用
var B20D9D_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var B20D9D_ary_PROC_ID = null;var B20D9D_ary_RETURN_TYPE = null;var B20D9D_ary_IS_IMG = [{'MAIN_ID': '1', 'CN_NAME': '是'}, {'MAIN_ID': '0', 'CN_NAME': '否'}];var B20D9D_ary_IS_URLENCODE = [{'MAIN_ID': '1', 'CN_NAME': '是'}, {'MAIN_ID': '0', 'CN_NAME': '否'}];var B20D9D_ary_LIST_HIDDEN = [{'MAIN_ID': '1', 'CN_NAME': '是'}, {'MAIN_ID': '0', 'CN_NAME': '否'}];
/*declare select options end*/

//主从表传递参数
function B20D9D_param_set(){
	/*Main Subsuv Table Param Begin*/
	if(B20D9D_param.hasOwnProperty("PROC_ID_cn_name"))
		$("#B20D9D_find_PROC_ID_cn_name").val(s_decode(A20D9D_param["PROC_ID_cn_name"]));
	if(B20D9D_param.hasOwnProperty("PROC_ID"))
		$("#B20D9D_find_PROC_ID").val(B20D9D_param["PROC_ID"]);
	if(B20D9D_param.hasOwnProperty("hidden_find")){
		$("#B20D9D_Ope_PROC_ID").hide();
		$("#B20D9D_Clear_PROC_ID").hide();
	}
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function B20D9D_t_proc_return_biz_start(inputdata) {
	B20D9D_param = inputdata;
	layer.close(ly_index);
	B20D9D_param_set();
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_proc_inparam$PROC_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B20D9D_get_N01_t_proc_inparam$PROC_ID");	
    /*biz end*/
}

/*biz step begin*/
function B20D9D_format_PROC_ID(value, row, index) {    var objResult = value;    for(i = 0; i < B20D9D_ary_PROC_ID.length; i++) {        var obj = B20D9D_ary_PROC_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B20D9D_format_RETURN_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < B20D9D_ary_RETURN_TYPE.length; i++) {        var obj = B20D9D_ary_RETURN_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B20D9D_format_IS_IMG(value, row, index) {    var objResult = value;    for(i = 0; i < B20D9D_ary_IS_IMG.length; i++) {        var obj = B20D9D_ary_IS_IMG[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B20D9D_format_IS_URLENCODE(value, row, index) {    var objResult = value;    for(i = 0; i < B20D9D_ary_IS_URLENCODE.length; i++) {        var obj = B20D9D_ary_IS_URLENCODE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B20D9D_format_LIST_HIDDEN(value, row, index) {    var objResult = value;    for(i = 0; i < B20D9D_ary_LIST_HIDDEN.length; i++) {        var obj = B20D9D_ary_LIST_HIDDEN[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B20D9D_get_N01_t_proc_inparam$PROC_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_inparam$PROC_ID) == false)        return false;    B20D9D_ary_PROC_ID = input.N01_t_proc_inparam$PROC_ID;    if($("#B20D9D_PROC_ID").is("select") && $("#B20D9D_PROC_ID")[0].options.length == 0)    {        $.each(B20D9D_ary_PROC_ID, function (i, obj) {            addOptionValue("B20D9D_PROC_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    var inputdata = {        "param_name": "N01_t_proc_return$RETURN_TYPE",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B20D9D_get_N01_t_proc_return$RETURN_TYPE");}function B20D9D_get_N01_t_proc_return$RETURN_TYPE(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_return$RETURN_TYPE) == false)        return false;    B20D9D_ary_RETURN_TYPE = input.N01_t_proc_return$RETURN_TYPE;    if($("#B20D9D_RETURN_TYPE").is("select") && $("#B20D9D_RETURN_TYPE")[0].options.length == 0)    {        $.each(B20D9D_ary_RETURN_TYPE, function (i, obj) {            addOptionValue("B20D9D_RETURN_TYPE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#B20D9D_IS_IMG").is("select") && $("#B20D9D_IS_IMG")[0].options.length == 0)    {        $.each(B20D9D_ary_IS_IMG, function (i, obj) {            addOptionValue("B20D9D_IS_IMG", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#B20D9D_IS_URLENCODE").is("select") && $("#B20D9D_IS_URLENCODE")[0].options.length == 0)    {        $.each(B20D9D_ary_IS_URLENCODE, function (i, obj) {            addOptionValue("B20D9D_IS_URLENCODE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#B20D9D_LIST_HIDDEN").is("select") && $("#B20D9D_LIST_HIDDEN")[0].options.length == 0)    {        $.each(B20D9D_ary_LIST_HIDDEN, function (i, obj) {            addOptionValue("B20D9D_LIST_HIDDEN", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    B20D9D_init_t_proc_return();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function B20D9D_PROC_ID_cn_name_fun(){    index_subhtml = "t_proc_name.vue"    random_subhtml = "A1BCF7";    if(loadHtmlSubVueFun("biz_vue/t_proc_name.vue","B20D9D_t_proc_return_call_vue") == true){        var n = Get_RandomDiv("A1BCF7","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#A1BCF7_t_proc_name_Events').bootstrapTable('resetView');                A1BCF7_param["ly_index"] = index;                A1BCF7_param["target_name"] = "B20D9D_find_PROC_ID_cn_name"                A1BCF7_param["target_id"] = "B20D9D_PROC_ID"                A1BCF7_param["sourc_id"] = "MAIN_ID"                A1BCF7_param["sourc_name"] = "INF_CN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function B20D9D_page_end(){
	if(B20D9D_param["type"] == "edit"){
		B20D9D_get_edit_info();
	}
}

//页面初始化方法
function B20D9D_init_t_proc_return() {
	//type = getUrlParam("type");
	if(B20D9D_param["type"] == "add"){
		$("#B20D9D_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#B20D9D_PROC_ID").val(B20D9D_param["PROC_ID"]);        $("#B20D9D_find_PROC_ID_cn_name").val(B20D9D_param["PROC_ID_cn_name"]);		
		/*Get Find Select param end*/	
	}
	else if(B20D9D_param["type"] == "edit"){
	
	}
	
	//表单验证
	B20D9D_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B20D9D_CREATE_DATE").val() == "")    {        $("#B20D9D_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B20D9D_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B20D9D_page_end();
}

//提交表单数据
function B20D9D_SubmitForm(){
	if(B20D9D_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_proc_return",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#B20D9D_PROC_ID").val()                ,"param_value2": s_encode($("#B20D9D_RETURN_NAME").val())                ,"param_value3": s_encode($("#B20D9D_RETURN_TYPE").val())                ,"param_value4": s_encode($("#B20D9D_S_DESC").val())                ,"param_value5": $("#B20D9D_CREATE_DATE").val()                ,"param_value6": $("#B20D9D_IS_IMG").val()                ,"param_value7": $("#B20D9D_IS_URLENCODE").val()                ,"param_value8": $("#B20D9D_LIST_HIDDEN").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B20D9D_get_N01_ins_t_proc_return");
	}
	else if(B20D9D_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_proc_return",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#B20D9D_PROC_ID").val()                ,"param_value2": s_encode($("#B20D9D_RETURN_NAME").val())                ,"param_value3": s_encode($("#B20D9D_RETURN_TYPE").val())                ,"param_value4": s_encode($("#B20D9D_S_DESC").val())                ,"param_value5": $("#B20D9D_CREATE_DATE").val()                ,"param_value6": $("#B20D9D_IS_IMG").val()                ,"param_value7": $("#B20D9D_IS_URLENCODE").val()                ,"param_value8": $("#B20D9D_LIST_HIDDEN").val()                ,"param_value9": $("#B20D9D_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B20D9D_get_N01_upd_t_proc_return");
	}
}

//vue回调
function B20D9D_t_proc_return_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_proc_name.vue"){        var n = Get_RandomDiv("A1BCF7",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":B20D9D_type,                    "ly_index":index,                    "target_name":"B20D9D_find_PROC_ID_cn_name",                    "target_id":"B20D9D_find_PROC_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"INF_CN_NAME"                };                loadScript_hasparam("biz_vue/t_proc_name.js","A1BCF7_t_proc_name_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//for表单提交
$("#B20D9D_save_t_proc_return_Edit").click(function () {
	$("form[name='B20D9D_DataModal']").submit();
})

/*修改数据*/
function B20D9D_get_N01_upd_t_proc_return(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_proc_return) == true)
	{
		swal("修改数据成功!", "", "success");
		A20D9D_t_proc_return_query();
		B20D9D_clear_validate();
		layer.close(B20D9D_param["ly_index"]);
	}
}

/*添加数据*/
function B20D9D_get_N01_ins_t_proc_return(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_proc_return) == true)
	{
		swal("添加数据成功!", "", "success");
		A20D9D_t_proc_return_query();
		B20D9D_clear_validate();
		layer.close(B20D9D_param["ly_index"]);
	}
}

//取消编辑
$("#B20D9D_cancel_t_proc_return_Edit").click(function () {
	layer.close(B20D9D_param["ly_index"]);
	B20D9D_clear_validate();
	$("[id^='B20D9D_div']").hide();
})

//清除查找框
function B20D9D_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B20D9D_clear_validate(){
	$("#B20D9D_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B20D9D_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B20D9D_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B20D9D_clear_edit_info(){
	var inputs = $("#B20D9D_DataModal").find('input');
	var selects = $("#B20D9D_DataModal").find("select");
	var textareas = $("#B20D9D_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
    B20D9D_clear_input_cn_name('B20D9D_find_PROC_ID_cn_name','B20D9D_PROC_ID')	
	/*input validate clear end*/
	B20D9D_init_t_proc_return();
}

//页面输入框赋值
function B20D9D_get_edit_info(){
	var rowData = $("#A20D9D_t_proc_return_Events").bootstrapTable('getData')[A20D9D_select_t_proc_return_rowId];
	var inputs = $("#B20D9D_DataModal").find('input');
	var selects = $("#B20D9D_DataModal").find("select");
	var textareas = $("#B20D9D_DataModal").find('textarea');
	$.each(selects, function (i, obj) {
		if(typeof(eval("A20D9D_ary_"+obj.id.toString().replace("B20D9D_",""))) == "object")
		{
			$.each(eval("A20D9D_ary_"+obj.id.toString().replace("B20D9D_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("B20D9D_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("B20D9D_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("A20D9D_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#B20D9D_DataModal").find('[id="'+"B20D9D_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("B20D9D_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("B20D9D_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//form验证
function B20D9D_checkFormInput() {
    B20D9D_validate = $("#B20D9D_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B20D9D_main_id: {}            ,B20D9D_PROC_ID: {}            ,B20D9D_RETURN_NAME: {}            ,B20D9D_RETURN_TYPE: {}            ,B20D9D_S_DESC: {}            ,B20D9D_CREATE_DATE: {date: true,required : true,maxlength:19}            ,B20D9D_IS_IMG: {}            ,B20D9D_IS_URLENCODE: {}            ,B20D9D_LIST_HIDDEN: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B20D9D_main_id: {}            ,B20D9D_PROC_ID: {}            ,B20D9D_RETURN_NAME: {}            ,B20D9D_RETURN_TYPE: {}            ,B20D9D_S_DESC: {}            ,B20D9D_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B20D9D_IS_IMG: {}            ,B20D9D_IS_URLENCODE: {}            ,B20D9D_LIST_HIDDEN: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B20D9D_SubmitForm();
        	return false;
        }
    })
}