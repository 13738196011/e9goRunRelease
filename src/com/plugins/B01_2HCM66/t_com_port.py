#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_com_port(object):
    attmain_id = -1
    attcom_name = ""
    attcom_bot = 0
    attcom_data = 0
    attcom_ct = 0
    attcom_eflag = ""
    atts_desc = ""
    attcreate_date = None
    lencom_name = 50
    lencom_eflag = 50
    lens_desc = 500
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["com_name"] = "串口名称"
        temHt["com_bot"] = "波特率"
        temHt["com_data"] = "数据位"
        temHt["com_ct"] = "奇偶校验"
        temHt["com_eflag"] = "英文标签"
        temHt["s_desc"] = "备注"
        temHt["create_date"] = "系统时间"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
