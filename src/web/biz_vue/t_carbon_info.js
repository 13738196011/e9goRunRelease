//选择某一行
var AE35F9_select_t_carbon_info_rowId = "";
//按钮事件新增或编辑
var AE35F9_type = "";
//其他页面传到本页面参数
var AE35F9_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var AE35F9_tem_begin_get_time = new Date().Format("yyyy-MM-dd hh:mm:ss");var AE35F9_tem_end_get_time = new Date().Format("yyyy-MM-dd hh:mm:ss");var AE35F9_tem_file_id = "-1";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var AE35F9_ary_file_id = null;
/*declare select options end*/

//业务逻辑数据开始
function AE35F9_t_carbon_info_biz_start(inputparam) {
	layer.close(ly_index);
	AE35F9_param = inputparam;
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_carbon_info$file_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE35F9_get_N01_t_carbon_info$file_id");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function AE35F9_format_file_id(value, row, index) {    var objResult = value;    for(i = 0; i < AE35F9_ary_file_id.length; i++) {        var obj = AE35F9_ary_file_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AE35F9_get_N01_t_carbon_info$file_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_carbon_info$file_id) == false)        return false;    AE35F9_ary_file_id = input.N01_t_carbon_info$file_id;    AE35F9_init_t_carbon_info();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function AE35F9_file_id_cn_name_fun(){    index_subhtml = "t_file_info.vue"    random_subhtml = "AE32DA";    if(loadHtmlSubVueFun("biz_vue/t_file_info.vue","AE35F9_t_carbon_info_call_vue") == true){        var n = Get_RandomDiv("AE32DA","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#AE32DA_t_file_info_Events').bootstrapTable('resetView');                AE32DA_param["ly_index"] = index;                AE32DA_param["target_name"] = "AE35F9_find_file_id_cn_name"                AE32DA_param["target_id"] = "AE35F9_find_file_id"                AE32DA_param["sourc_id"] = "MAIN_ID"                AE32DA_param["sourc_name"] = "FILE_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function AE35F9_page_end(){
}

//能源碳排显示列定义
var AE35F9_t_carbon_info = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '附件信息',        field: 'FILE_ID',        sortable: true        ,formatter: AE35F9_format_file_id    },    {        title: '日期',        field: 'GET_TIME',        sortable: true        ,formatter: set_time_decode    },    {        title: '用煤碳排量',        field: 'COAL_CARBON',        sortable: true    },    {        title: '用油碳排量',        field: 'OIL_CARBON',        sortable: true    },    {        title: '用气碳排量',        field: 'GAS_CARBON',        sortable: true    },    {        title: '用电碳排量',        field: 'ELE_CARBON',        sortable: true    },    {        title: '用热碳排量',        field: 'HOT_CARBON',        sortable: true    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '系统备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function AE35F9_init_t_carbon_info() {
	$(window).resize(function () {
		  $('#AE35F9_t_carbon_info_Events').bootstrapTable('resetView');
	});
	//能源碳排查询条件初始化设置
	/*query conditions init begin*/
   $("#AE35F9_qry_begin_get_time").val(DateAdd("d", -5, new Date()).Format('yyyy-MM-dd hh:mm:ss'));   laydate.render({       elem: '#AE35F9_qry_begin_get_time',       type: 'datetime',       trigger: 'click'   });   $("#AE35F9_qry_end_get_time").val(DateAdd("d", 1, new Date()).Format('yyyy-MM-dd hh:mm:ss'));   laydate.render({       elem: '#AE35F9_qry_end_get_time',       type: 'datetime',       trigger: 'click'   });	
    /*query conditions init end*/
	
	$('#AE35F9_btn_t_carbon_info_query').click();
}

//查询接口
function AE35F9_t_carbon_info_query() {
    $('#AE35F9_t_carbon_info_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_carbon_info",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": AE35F9_tem_begin_get_time        ,"param_value2": AE35F9_tem_end_get_time        ,"param_value3": AE35F9_tem_file_id        ,"param_value4": AE35F9_tem_file_id		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AE35F9_get_N01_sel_t_carbon_info");
}

//查询结果
function AE35F9_get_N01_sel_t_carbon_info(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_carbon_info) == false)
        return false;    
	var s_data = input.N01_sel_t_carbon_info;
    AE35F9_select_t_carbon_info_rowId = "";
    $('#AE35F9_t_carbon_info_Events').bootstrapTable('destroy');
    $("#AE35F9_t_carbon_info_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AE35F9_t_carbon_info_Events').find("tr.changeColor").removeClass('changeColor');
                AE35F9_select_t_carbon_info_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AE35F9_t_carbon_info_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AE35F9_select_t_carbon_info_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AE35F9_param.hasOwnProperty("target_name"))
			{
				$("#"+AE35F9_param["target_id"]).val(eval("row."+AE35F9_param["sourc_id"].toUpperCase()));
				$("#"+AE35F9_param["target_name"]).val(s_decode(eval("row."+AE35F9_param["sourc_name"].toUpperCase())));				
				layer.close(AE35F9_param["ly_index"]);
			}
		},
        toolbar: "#AE35F9_t_carbon_info_Toolbar",
        columns: AE35F9_t_carbon_info,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AE35F9_btn_t_carbon_info_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    AE35F9_tem_begin_get_time = new Date().Format("yyyy-MM-dd hh:mm:ss");    AE35F9_tem_end_get_time = new Date().Format("yyyy-MM-dd hh:mm:ss");    AE35F9_tem_file_id = "-1";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    BE35F9_ary_file_id = null;    $("#AE35F9_find_file_id_cn_name").val("");    $("#AE35F9_find_file_id").val("-1");
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_carbon_info$file_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE35F9_get_N01_t_carbon_info$file_id");	
    /*biz end*/
})

//查询按钮
$('#AE35F9_btn_t_carbon_info_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    AE35F9_tem_begin_get_time = $("#AE35F9_qry_begin_get_time").val();    AE35F9_tem_end_get_time = $("#AE35F9_qry_end_get_time").val();    AE35F9_tem_file_id = $("#AE35F9_find_file_id").val();	
    /*set query param end*/
	AE35F9_t_carbon_info_query();
})

//vue回调
function AE35F9_t_carbon_info_call_vue(objResult){
	if(index_subhtml == "t_carbon_info_$.vue")
	{
		var n = Get_RandomDiv("BE35F9",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AE35F9_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temget_time = $("#AE35F9_qry_get_time").val();                if(temget_time != ""){                    inputdata["get_time"] = temget_time;                }                var temfile_id = $("#AE35F9_find_file_id_cn_name").val();                if(temfile_id != ""){                    inputdata["file_id_cn_name"] = temfile_id;                    inputdata["file_id"] = $("#AE35F9_file_id").val();                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_carbon_info_$.js","BE35F9_t_carbon_info_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_file_info.vue"){        var n = Get_RandomDiv("AE32DA",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":AE35F9_type,                    "ly_index":index,                    "target_name":"AE35F9_find_file_id_cn_name",                    "target_id":"AE35F9_find_file_id",                    "sourc_id":"MAIN_ID",                    "sourc_name":"FILE_NAME"                };                loadScript_hasparam("biz_vue/t_file_info.js","AE32DA_t_file_info_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//新增按钮
$("#AE35F9_btn_t_carbon_info_add").click(function () {
	AE35F9_type = "add";
	index_subhtml = "t_carbon_info_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_carbon_info_$.vue","AE35F9_t_carbon_info_call_vue") == true){
		var n = Get_RandomDiv("BE35F9","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BE35F9_param["type"] = AE35F9_type;
				BE35F9_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temget_time = $("#AE35F9_qry_get_time").val();                if(temget_time != ""){                    BE35F9_param["get_time"] = temget_time;                }                var temfile_id = $("#AE35F9_find_file_id_cn_name").val();                if(temfile_id != ""){                    BE35F9_param["file_id_cn_name"] = temfile_id;                    BE35F9_param["file_id"] = $("#AE35F9_file_id").val();                }				
				/*Send Two FindSelect param end*/

				BE35F9_clear_edit_info();
			},
			end: function(){
				BE35F9_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AE35F9_btn_t_carbon_info_edit").click(function () {
	if (AE35F9_select_t_carbon_info_rowId != "") {
		AE35F9_type = "edit";
		index_subhtml = "t_carbon_info_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_carbon_info_$.vue","AE35F9_t_carbon_info_call_vue") == true){
			var n = Get_RandomDiv("BE35F9","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BE35F9_param["type"] = AE35F9_type;
					BE35F9_param["ly_index"] = index;
					BE35F9_clear_edit_info();
					BE35F9_get_edit_info();
				},
				end: function(){
					BE35F9_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AE35F9_btn_t_carbon_info_delete').click(function () {
	//单行选择
	var rowData = $("#AE35F9_t_carbon_info_Events").bootstrapTable('getData')[AE35F9_select_t_carbon_info_rowId];
	//多行选择
	var rowDatas = AE35F9_sel_row_t_carbon_info();
	if (AE35F9_select_t_carbon_info_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_carbon_info",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AE35F9_N01_del_t_carbon_info");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_carbon_info",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AE35F9_N01_del_t_carbon_info");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AE35F9_N01_del_t_carbon_info(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_carbon_info) == true)
		AE35F9_t_carbon_info_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function AE35F9_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AE35F9_sel_row_t_carbon_info(){
	//获得选中行
	var checkedbox= $("#AE35F9_t_carbon_info_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}