//按钮事件新增或编辑
var B9FE70_type = "";
//其他页面传到本页面参数
var B9FE70_param = {};
//暂时没用
var B9FE70_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var B9FE70_ary_PROC_ID = null;var B9FE70_ary_PARAM_TYPE = null;var B9FE70_ary_IS_URLENCODE = [{'MAIN_ID': '1', 'CN_NAME': '是'}, {'MAIN_ID': '0', 'CN_NAME': '否'}];
/*declare select options end*/

//主从表传递参数
function B9FE70_param_set(){
	/*Main Subsuv Table Param Begin*/
    if(B9FE70_param.hasOwnProperty("PROC_ID_cn_name"))        $("#B9FE70_find_PROC_ID_cn_name").val(s_decode(B9FE70_param["PROC_ID_cn_name"]));    if(B9FE70_param.hasOwnProperty("PROC_ID"))        $("#B9FE70_find_PROC_ID").val(B9FE70_param["PROC_ID"]);    if(B9FE70_param.hasOwnProperty("hidden_find")){        $("#B9FE70_Ope_PROC_ID").hide();        $("#B9FE70_Clear_PROC_ID").hide();    }	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function B9FE70_t_proc_inparam_biz_start(inputdata) {
	B9FE70_param = inputdata;
	layer.close(ly_index);
	B9FE70_param_set();
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_proc_inparam$PROC_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B9FE70_get_N01_t_proc_inparam$PROC_ID");	
    /*biz end*/
}

/*biz step begin*/
function B9FE70_format_PROC_ID(value, row, index) {    var objResult = value;    for(i = 0; i < B9FE70_ary_PROC_ID.length; i++) {        var obj = B9FE70_ary_PROC_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B9FE70_format_PARAM_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < B9FE70_ary_PARAM_TYPE.length; i++) {        var obj = B9FE70_ary_PARAM_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B9FE70_format_IS_URLENCODE(value, row, index) {    var objResult = value;    for(i = 0; i < B9FE70_ary_IS_URLENCODE.length; i++) {        var obj = B9FE70_ary_IS_URLENCODE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B9FE70_get_N01_t_proc_inparam$PROC_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_inparam$PROC_ID) == false)        return false;    B9FE70_ary_PROC_ID = input.N01_t_proc_inparam$PROC_ID;    if($("#B9FE70_PROC_ID").is("select") && $("#B9FE70_PROC_ID")[0].options.length == 0)    {        $.each(B9FE70_ary_PROC_ID, function (i, obj) {            addOptionValue("B9FE70_PROC_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    var inputdata = {        "param_name": "N01_t_proc_inparam$PARAM_TYPE",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B9FE70_get_N01_t_proc_inparam$PARAM_TYPE");}function B9FE70_get_N01_t_proc_inparam$PARAM_TYPE(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_inparam$PARAM_TYPE) == false)        return false;    B9FE70_ary_PARAM_TYPE = input.N01_t_proc_inparam$PARAM_TYPE;    if($("#B9FE70_PARAM_TYPE").is("select") && $("#B9FE70_PARAM_TYPE")[0].options.length == 0)    {        $.each(B9FE70_ary_PARAM_TYPE, function (i, obj) {            addOptionValue("B9FE70_PARAM_TYPE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#B9FE70_IS_URLENCODE").is("select") && $("#B9FE70_IS_URLENCODE")[0].options.length == 0)    {        $.each(B9FE70_ary_IS_URLENCODE, function (i, obj) {            addOptionValue("B9FE70_IS_URLENCODE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    B9FE70_init_t_proc_inparam();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function B9FE70_PROC_ID_cn_name_fun(){    index_subhtml = "t_proc_name.vue"    random_subhtml = "";    if(loadHtmlSubVueFun("dev_vue/t_proc_name.vue","B9FE70_t_proc_inparam_call_vue") == true){        var n = Get_RandomDiv("","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#_t_proc_name_Events').bootstrapTable('resetView');                _param["ly_index"] = index;                _param["target_name"] = "B9FE70_find_PROC_ID_cn_name"                _param["target_id"] = "B9FE70_PROC_ID"                _param["sourc_id"] = "MAIN_ID"                _param["sourc_name"] = "INF_CN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function B9FE70_page_end(){
	if(B9FE70_param["type"] == "edit"){
		B9FE70_get_edit_info();
	}
}

//页面初始化方法
function B9FE70_init_t_proc_inparam() {
	//type = getUrlParam("type");
	if(B9FE70_param["type"] == "add"){
		$("#B9FE70_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#B9FE70_PROC_ID").val(B9FE70_param["PROC_ID"]);        $("#B9FE70_find_PROC_ID_cn_name").val(B9FE70_param["PROC_ID_cn_name"]);		
		/*Get Find Select param end*/	
	}
	else if(B9FE70_param["type"] == "edit"){
	
	}
	
	//表单验证
	B9FE70_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B9FE70_CREATE_DATE").val() == "")    {        $("#B9FE70_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B9FE70_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B9FE70_page_end();
}

//提交表单数据
function B9FE70_SubmitForm(){
	if(B9FE70_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_proc_inparam",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#B9FE70_PROC_ID").val()                ,"param_value2": s_encode($("#B9FE70_PARAM_CN_NAME").val())                ,"param_value3": s_encode($("#B9FE70_PARAM_EN_NAME").val())                ,"param_value4": s_encode($("#B9FE70_PARAM_TYPE").val())                ,"param_value5": $("#B9FE70_PARAM_SIZE").val()                ,"param_value6": s_encode($("#B9FE70_S_DESC").val())                ,"param_value7": $("#B9FE70_CREATE_DATE").val()                ,"param_value8": $("#B9FE70_IS_URLENCODE").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B9FE70_get_N01_ins_t_proc_inparam");
	}
	else if(B9FE70_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_proc_inparam",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#B9FE70_PROC_ID").val()                ,"param_value2": s_encode($("#B9FE70_PARAM_CN_NAME").val())                ,"param_value3": s_encode($("#B9FE70_PARAM_EN_NAME").val())                ,"param_value4": s_encode($("#B9FE70_PARAM_TYPE").val())                ,"param_value5": $("#B9FE70_PARAM_SIZE").val()                ,"param_value6": s_encode($("#B9FE70_S_DESC").val())                ,"param_value7": $("#B9FE70_CREATE_DATE").val()                ,"param_value8": $("#B9FE70_IS_URLENCODE").val()                ,"param_value9": $("#B9FE70_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B9FE70_get_N01_upd_t_proc_inparam");
	}
}

//vue回调
function B9FE70_t_proc_inparam_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_proc_name.vue"){        var n = Get_RandomDiv("",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":B9FE70_type,                    "ly_index":index,                    "target_name":"B9FE70_find_PROC_ID_cn_name",                    "target_id":"B9FE70_find_PROC_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"INF_CN_NAME"                };                loadScript_hasparam("dev_vue/t_proc_name.js","_t_proc_name_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//for表单提交
$("#B9FE70_save_t_proc_inparam_Edit").click(function () {
	$("form[name='B9FE70_DataModal']").submit();
})

/*修改数据*/
function B9FE70_get_N01_upd_t_proc_inparam(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_proc_inparam) == true)
	{
		swal("修改数据成功!", "", "success");
		A9FE70_t_proc_inparam_query();
		B9FE70_clear_validate();
		layer.close(B9FE70_param["ly_index"]);
	}
}

/*添加数据*/
function B9FE70_get_N01_ins_t_proc_inparam(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_proc_inparam) == true)
	{
		swal("添加数据成功!", "", "success");
		A9FE70_t_proc_inparam_query();
		B9FE70_clear_validate();
		layer.close(B9FE70_param["ly_index"]);
	}
}

//取消编辑
$("#B9FE70_cancel_t_proc_inparam_Edit").click(function () {
	layer.close(B9FE70_param["ly_index"]);
	B9FE70_clear_validate();
	$("[id^='B9FE70_div']").hide();
})

//清除查找框
function B9FE70_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B9FE70_clear_validate(){
	$("#B9FE70_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B9FE70_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B9FE70_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B9FE70_clear_edit_info(){
	var inputs = $("#B9FE70_DataModal").find('input');
	var selects = $("#B9FE70_DataModal").find("select");
	var textareas = $("#B9FE70_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
    B9FE70_clear_input_cn_name('B9FE70_find_PROC_ID_cn_name','B9FE70_PROC_ID')	
	/*input validate clear end*/
	B9FE70_init_t_proc_inparam();
}

//页面输入框赋值
function B9FE70_get_edit_info(){
	var rowData = $("#A9FE70_t_proc_inparam_Events").bootstrapTable('getData')[A9FE70_select_t_proc_inparam_rowId];
	var inputs = $("#B9FE70_DataModal").find('input');
	var selects = $("#B9FE70_DataModal").find("select");
	var textareas = $("#B9FE70_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"A9FE70","B9FE70");
}

//form验证
function B9FE70_checkFormInput() {
    B9FE70_validate = $("#B9FE70_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B9FE70_main_id: {}            ,B9FE70_PROC_ID: {}            ,B9FE70_PARAM_CN_NAME: {}            ,B9FE70_PARAM_EN_NAME: {}            ,B9FE70_PARAM_TYPE: {}            ,B9FE70_PARAM_SIZE: {digits: true,required : true,maxlength:10}            ,B9FE70_S_DESC: {}            ,B9FE70_CREATE_DATE: {date: true,required : true,maxlength:19}            ,B9FE70_IS_URLENCODE: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B9FE70_main_id: {}            ,B9FE70_PROC_ID: {}            ,B9FE70_PARAM_CN_NAME: {}            ,B9FE70_PARAM_EN_NAME: {}            ,B9FE70_PARAM_TYPE: {}            ,B9FE70_PARAM_SIZE: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,B9FE70_S_DESC: {}            ,B9FE70_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B9FE70_IS_URLENCODE: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B9FE70_SubmitForm();
        	return false;
        }
    })
}