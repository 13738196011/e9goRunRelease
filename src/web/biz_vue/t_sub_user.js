//选择某一行
var A27B94_select_t_sub_user_rowId = "";
//按钮事件新增或编辑
var A27B94_type = "";
//其他页面传到本页面参数
var A27B94_param = {};

/*定义查询条件变量*/
/*declare query param begin*/

/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var A27B94_ary_SUB_ID = null;
/*declare select options end*/

//业务逻辑数据开始
function A27B94_t_sub_user_biz_start(inputparam) {
	layer.close(ly_index);
	A27B94_param = inputparam;
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_sub_power$SUB_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A27B94_get_N01_t_sub_power$SUB_ID");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function A27B94_format_SUB_ID(value, row, index) {    var objResult = value;    for(i = 0; i < A27B94_ary_SUB_ID.length; i++) {        var obj = A27B94_ary_SUB_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A27B94_get_N01_t_sub_power$SUB_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_sub_power$SUB_ID) == false)        return false;    A27B94_ary_SUB_ID = input.N01_t_sub_power$SUB_ID;    A27B94_init_t_sub_user();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function A27B94_page_end(){
}

//平台用户信息显示列定义
var A27B94_t_sub_user = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '应用平台信息',        field: 'SUB_ID',        sortable: true        ,formatter: A27B94_format_SUB_ID    },    {        title: '用户名称',        field: 'SUB_USERNAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '用户授权码',        field: 'SUB_USERCODE',        sortable: true        ,formatter: set_s_decode    },    {        title: '有效期限',        field: 'LIMIT_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    }	
	/*table column end*/
];

//页面初始化
function A27B94_init_t_sub_user() {
	$(window).resize(function () {
		  $('#A27B94_t_sub_user_Events').bootstrapTable('resetView');
	});
	//平台用户信息查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#A27B94_btn_t_sub_user_query').click();
}

//查询接口
function A27B94_t_sub_user_query() {
    $('#A27B94_t_sub_user_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_sub_user",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "A27B94_get_N01_sel_t_sub_user");
}

//查询结果
function A27B94_get_N01_sel_t_sub_user(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_sub_user) == false)
        return false;    
	var s_data = input.N01_sel_t_sub_user;
    A27B94_select_t_sub_user_rowId = "";
    $('#A27B94_t_sub_user_Events').bootstrapTable('destroy');
    $("#A27B94_t_sub_user_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#A27B94_t_sub_user_Events').find("tr.changeColor").removeClass('changeColor');
                A27B94_select_t_sub_user_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#A27B94_t_sub_user_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                A27B94_select_t_sub_user_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(A27B94_param.hasOwnProperty("target_name"))
			{
				$("#"+A27B94_param["target_id"]).val(eval("row."+A27B94_param["sourc_id"].toUpperCase()));
				$("#"+A27B94_param["target_name"]).val(s_decode(eval("row."+A27B94_param["sourc_name"].toUpperCase())));				
				layer.close(A27B94_param["ly_index"]);
			}
		},
        toolbar: "#A27B94_t_sub_user_Toolbar",
        columns: A27B94_t_sub_user,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#A27B94_btn_t_sub_user_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/

	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    B27B94_ary_SUB_ID = null;
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_sub_power$SUB_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A27B94_get_N01_t_sub_power$SUB_ID");	
    /*biz end*/
})

//查询按钮
$('#A27B94_btn_t_sub_user_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
	
    /*set query param end*/
	A27B94_t_sub_user_query();
})

//vue回调
function A27B94_t_sub_user_call_vue(objResult){
	if(index_subhtml == "t_sub_user_$.vue")
	{
		var n = Get_RandomDiv("B27B94",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":A27B94_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_sub_user_$.js","B27B94_t_sub_user_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//新增按钮
$("#A27B94_btn_t_sub_user_add").click(function () {
	A27B94_type = "add";
	index_subhtml = "t_sub_user_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_sub_user_$.vue","A27B94_t_sub_user_call_vue") == true){
		var n = Get_RandomDiv("B27B94","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				B27B94_param["type"] = A27B94_type;
				B27B94_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
				
				/*Send Two FindSelect param end*/

				B27B94_clear_edit_info();
			},
			end: function(){
				B27B94_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#A27B94_btn_t_sub_user_edit").click(function () {
	if (A27B94_select_t_sub_user_rowId != "") {
		A27B94_type = "edit";
		index_subhtml = "t_sub_user_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_sub_user_$.vue","A27B94_t_sub_user_call_vue") == true){
			var n = Get_RandomDiv("B27B94","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					B27B94_param["type"] = A27B94_type;
					B27B94_param["ly_index"] = index;
					B27B94_clear_edit_info();
					B27B94_get_edit_info();
				},
				end: function(){
					B27B94_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#A27B94_btn_t_sub_user_delete').click(function () {
	//单行选择
	var rowData = $("#A27B94_t_sub_user_Events").bootstrapTable('getData')[A27B94_select_t_sub_user_rowId];
	//多行选择
	var rowDatas = A27B94_sel_row_t_sub_user();
	if (A27B94_select_t_sub_user_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_sub_user",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "A27B94_N01_del_t_sub_user");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_sub_user",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "A27B94_N01_del_t_sub_user");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function A27B94_N01_del_t_sub_user(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_sub_user) == true)
		A27B94_t_sub_user_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function A27B94_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function A27B94_sel_row_t_sub_user(){
	//获得选中行
	var checkedbox= $("#A27B94_t_sub_user_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}