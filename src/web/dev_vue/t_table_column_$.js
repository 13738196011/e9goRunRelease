//按钮事件新增或编辑
var B567C2_type = "";
//其他页面传到本页面参数
var B567C2_param = {};
//暂时没用
var B567C2_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var B567C2_ary_TABLE_ID = null;var B567C2_ary_COLUMN_TYPE = [{'MAIN_ID': '1', 'CN_NAME': 'STRING'}, {'MAIN_ID': '2', 'CN_NAME': 'INT'}, {'MAIN_ID': '3', 'CN_NAME': 'FLOAT'}, {'MAIN_ID': '4', 'CN_NAME': 'DATE'}];var B567C2_ary_COLUMN_QRY = [{'MAIN_ID': '0', 'CN_NAME': ''}, {'MAIN_ID': '1', 'CN_NAME': '输入框'}, {'MAIN_ID': '2', 'CN_NAME': '下拉框'}, {'MAIN_ID': '3', 'CN_NAME': '查找框'}];
/*declare select options end*/

//主从表传递参数
function B567C2_param_set(){
	/*Main Subsuv Table Param Begin*/
    if(B567C2_param.hasOwnProperty("TABLE_ID_cn_name"))        $("#B567C2_find_TABLE_ID_cn_name").val(s_decode(B567C2_param["TABLE_ID_cn_name"]));    if(B567C2_param.hasOwnProperty("TABLE_ID"))        $("#B567C2_find_TABLE_ID").val(B567C2_param["TABLE_ID"]);    if(B567C2_param.hasOwnProperty("hidden_find")){        $("#B567C2_Ope_TABLE_ID").hide();        $("#B567C2_Clear_TABLE_ID").hide();    }	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function B567C2_t_table_column_biz_start(inputdata) {
	B567C2_param = inputdata;
	layer.close(ly_index);
	B567C2_param_set();
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_table_column$TABLE_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B567C2_get_N01_t_table_column$TABLE_ID");	
    /*biz end*/
}

/*biz step begin*/
function B567C2_format_TABLE_ID(value, row, index) {    var objResult = value;    for(i = 0; i < B567C2_ary_TABLE_ID.length; i++) {        var obj = B567C2_ary_TABLE_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B567C2_format_COLUMN_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < B567C2_ary_COLUMN_TYPE.length; i++) {        var obj = B567C2_ary_COLUMN_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B567C2_format_COLUMN_QRY(value, row, index) {    var objResult = value;    for(i = 0; i < B567C2_ary_COLUMN_QRY.length; i++) {        var obj = B567C2_ary_COLUMN_QRY[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B567C2_get_N01_t_table_column$TABLE_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_table_column$TABLE_ID) == false)        return false;    B567C2_ary_TABLE_ID = input.N01_t_table_column$TABLE_ID;    if($("#B567C2_TABLE_ID").is("select") && $("#B567C2_TABLE_ID")[0].options.length == 0)    {        $.each(B567C2_ary_TABLE_ID, function (i, obj) {            addOptionValue("B567C2_TABLE_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#B567C2_COLUMN_TYPE").is("select") && $("#B567C2_COLUMN_TYPE")[0].options.length == 0)    {        $.each(B567C2_ary_COLUMN_TYPE, function (i, obj) {            addOptionValue("B567C2_COLUMN_TYPE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#B567C2_COLUMN_QRY").is("select") && $("#B567C2_COLUMN_QRY")[0].options.length == 0)    {        $.each(B567C2_ary_COLUMN_QRY, function (i, obj) {            addOptionValue("B567C2_COLUMN_QRY", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    B567C2_init_t_table_column();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function B567C2_TABLE_ID_cn_name_fun(){    index_subhtml = "t_single_table.vue"    random_subhtml = "AEC35F";    if(loadHtmlSubVueFun("dev_vue/t_single_table.vue","B567C2_t_table_column_call_vue") == true){        var n = Get_RandomDiv("AEC35F","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#AEC35F_t_single_table_Events').bootstrapTable('resetView');                AEC35F_param["ly_index"] = index;                AEC35F_param["target_name"] = "B567C2_find_TABLE_ID_cn_name"                AEC35F_param["target_id"] = "B567C2_TABLE_ID"                AEC35F_param["sourc_id"] = "MAIN_ID"                AEC35F_param["sourc_name"] = "TABLE_EN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function B567C2_page_end(){
	if(B567C2_param["type"] == "edit"){
		B567C2_get_edit_info();
	}
}

//页面初始化方法
function B567C2_init_t_table_column() {
	//type = getUrlParam("type");
	if(B567C2_param["type"] == "add"){
		$("#B567C2_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#B567C2_COLUMN_EN_NAME").val(B567C2_param["COLUMN_EN_NAME"]);        $("#B567C2_TABLE_ID").val(B567C2_param["TABLE_ID"]);        $("#B567C2_TABLE_ID").val(B567C2_param["TABLE_ID"]);        $("#B567C2_find_TABLE_ID_cn_name").val(B567C2_param["TABLE_ID_cn_name"]);		
		/*Get Find Select param end*/	
	}
	else if(B567C2_param["type"] == "edit"){
	
	}
	
	//表单验证
	B567C2_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B567C2_CREATE_DATE").val() == "")    {        $("#B567C2_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B567C2_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B567C2_page_end();
}

//提交表单数据
function B567C2_SubmitForm(){
	if(B567C2_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_table_column",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#B567C2_TABLE_ID").val()                ,"param_value2": s_encode($("#B567C2_COLUMN_CN_NAME").val())                ,"param_value3": s_encode($("#B567C2_COLUMN_EN_NAME").val())                ,"param_value4": $("#B567C2_COLUMN_TYPE").val()                ,"param_value5": $("#B567C2_COLUMN_LENGTH").val()                ,"param_value6": $("#B567C2_COLUMN_QRY").val()                ,"param_value7": s_encode($("#B567C2_COLUMN_QRY_FORMAT").val())                ,"param_value8": s_encode($("#B567C2_COLUMN_FIND_HTML").val())                ,"param_value9": s_encode($("#B567C2_COLUMN_FIND_RETURN").val())                ,"param_value10": $("#B567C2_CREATE_DATE").val()                ,"param_value11": s_encode($("#B567C2_S_DESC").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B567C2_get_N01_ins_t_table_column");
	}
	else if(B567C2_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_table_column",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#B567C2_TABLE_ID").val()                ,"param_value2": s_encode($("#B567C2_COLUMN_CN_NAME").val())                ,"param_value3": s_encode($("#B567C2_COLUMN_EN_NAME").val())                ,"param_value4": $("#B567C2_COLUMN_TYPE").val()                ,"param_value5": $("#B567C2_COLUMN_LENGTH").val()                ,"param_value6": $("#B567C2_COLUMN_QRY").val()                ,"param_value7": s_encode($("#B567C2_COLUMN_QRY_FORMAT").val())                ,"param_value8": s_encode($("#B567C2_COLUMN_FIND_HTML").val())                ,"param_value9": s_encode($("#B567C2_COLUMN_FIND_RETURN").val())                ,"param_value10": $("#B567C2_CREATE_DATE").val()                ,"param_value11": s_encode($("#B567C2_S_DESC").val())                ,"param_value12": $("#B567C2_MAIN_ID").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B567C2_get_N01_upd_t_table_column");
	}
}

//vue回调
function B567C2_t_table_column_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_single_table.vue"){        var n = Get_RandomDiv("AEC35F",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":B567C2_type,                    "ly_index":index,                    "target_name":"B567C2_find_TABLE_ID_cn_name",                    "target_id":"B567C2_find_TABLE_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"TABLE_EN_NAME"                };                loadScript_hasparam("dev_vue/t_single_table.js","AEC35F_t_single_table_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//for表单提交
$("#B567C2_save_t_table_column_Edit").click(function () {
	$("form[name='B567C2_DataModal']").submit();
})

/*修改数据*/
function B567C2_get_N01_upd_t_table_column(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_table_column) == true)
	{
		swal("修改数据成功!", "", "success");
		A567C2_t_table_column_query();
		B567C2_clear_validate();
		layer.close(B567C2_param["ly_index"]);
	}
}

/*添加数据*/
function B567C2_get_N01_ins_t_table_column(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_table_column) == true)
	{
		swal("添加数据成功!", "", "success");
		A567C2_t_table_column_query();
		B567C2_clear_validate();
		layer.close(B567C2_param["ly_index"]);
	}
}

//取消编辑
$("#B567C2_cancel_t_table_column_Edit").click(function () {
	layer.close(B567C2_param["ly_index"]);
	B567C2_clear_validate();
	$("[id^='B567C2_div']").hide();
})

//清除查找框
function B567C2_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B567C2_clear_validate(){
	$("#B567C2_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B567C2_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B567C2_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B567C2_clear_edit_info(){
	var inputs = $("#B567C2_DataModal").find('input');
	var selects = $("#B567C2_DataModal").find("select");
	var textareas = $("#B567C2_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
    B567C2_clear_input_cn_name('B567C2_find_TABLE_ID_cn_name','B567C2_TABLE_ID')	
	/*input validate clear end*/
	B567C2_init_t_table_column();
}

//页面输入框赋值
function B567C2_get_edit_info(){
	var rowData = $("#A567C2_t_table_column_Events").bootstrapTable('getData')[A567C2_select_t_table_column_rowId];
	var inputs = $("#B567C2_DataModal").find('input');
	var selects = $("#B567C2_DataModal").find("select");
	var textareas = $("#B567C2_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"A567C2","B567C2");
}

//form验证
function B567C2_checkFormInput() {
    B567C2_validate = $("#B567C2_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B567C2_MAIN_ID: {}            ,B567C2_TABLE_ID: {}            ,B567C2_COLUMN_CN_NAME: {}            ,B567C2_COLUMN_EN_NAME: {}            ,B567C2_COLUMN_TYPE: {}            ,B567C2_COLUMN_LENGTH: {digits: true,required : true,maxlength:10}            ,B567C2_COLUMN_QRY: {}            ,B567C2_COLUMN_QRY_FORMAT: {}            ,B567C2_COLUMN_FIND_HTML: {}            ,B567C2_COLUMN_FIND_RETURN: {}            ,B567C2_CREATE_DATE: {date: true,required : true,maxlength:19}            ,B567C2_S_DESC: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B567C2_MAIN_ID: {}            ,B567C2_TABLE_ID: {}            ,B567C2_COLUMN_CN_NAME: {}            ,B567C2_COLUMN_EN_NAME: {}            ,B567C2_COLUMN_TYPE: {}            ,B567C2_COLUMN_LENGTH: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,B567C2_COLUMN_QRY: {}            ,B567C2_COLUMN_QRY_FORMAT: {}            ,B567C2_COLUMN_FIND_HTML: {}            ,B567C2_COLUMN_FIND_RETURN: {}            ,B567C2_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B567C2_S_DESC: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B567C2_SubmitForm();
        	return false;
        }
    })
}