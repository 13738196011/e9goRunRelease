//选择某一行
var AE34FE_select_t_excel_data_rowId = "";
//按钮事件新增或编辑
var AE34FE_type = "";
//其他页面传到本页面参数
var AE34FE_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var AE34FE_tem_file_id = "0";var AE34FE_tem_excel_tag_id = "0";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var AE34FE_ary_file_id = null;var AE34FE_ary_excel_tag_id = null;
/*declare select options end*/

//业务逻辑数据开始
function AE34FE_t_excel_data_biz_start(inputparam) {
	layer.close(ly_index);
	AE34FE_param = inputparam;
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_excel_data$file_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE34FE_get_N01_t_excel_data$file_id");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function AE34FE_format_file_id(value, row, index) {    var objResult = value;    for(i = 0; i < AE34FE_ary_file_id.length; i++) {        var obj = AE34FE_ary_file_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AE34FE_format_excel_tag_id(value, row, index) {    var objResult = value;    for(i = 0; i < AE34FE_ary_excel_tag_id.length; i++) {        var obj = AE34FE_ary_excel_tag_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AE34FE_get_N01_t_excel_data$file_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_excel_data$file_id) == false)        return false;    AE34FE_ary_file_id = input.N01_t_excel_data$file_id;    var inputdata = {        "param_name": "N01_t_excel_data$excel_tag_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE34FE_get_N01_t_excel_data$excel_tag_id");}function AE34FE_get_N01_t_excel_data$excel_tag_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_excel_data$excel_tag_id) == false)        return false;    AE34FE_ary_excel_tag_id = input.N01_t_excel_data$excel_tag_id;    $("#AE34FE_qry_file_id").append("<option value='-1'></option>")    $.each(AE34FE_ary_file_id, function (i, obj) {        addOptionValue("AE34FE_qry_file_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    $("#AE34FE_qry_excel_tag_id").append("<option value='-1'></option>")    $.each(AE34FE_ary_excel_tag_id, function (i, obj) {        addOptionValue("AE34FE_qry_excel_tag_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    AE34FE_init_t_excel_data();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function AE34FE_page_end(){
}

//excel数据内容显示列定义
var AE34FE_t_excel_data = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '附件ID',        field: 'FILE_ID',        sortable: true        ,formatter: AE34FE_format_file_id    },    {        title: 'Excel标签ID',        field: 'EXCEL_TAG_ID',        sortable: true        ,formatter: AE34FE_format_excel_tag_id    },    {        title: '数据内容',        field: 'DATA_VALUE',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '系统备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function AE34FE_init_t_excel_data() {
	$(window).resize(function () {
		  $('#AE34FE_t_excel_data_Events').bootstrapTable('resetView');
	});
	//excel数据内容查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AE34FE_btn_t_excel_data_query').click();
}

//查询接口
function AE34FE_t_excel_data_query() {
    $('#AE34FE_t_excel_data_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_excel_data",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": AE34FE_tem_file_id        ,"param_value2": AE34FE_tem_file_id        ,"param_value3": AE34FE_tem_excel_tag_id        ,"param_value4": AE34FE_tem_excel_tag_id		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AE34FE_get_N01_sel_t_excel_data");
}

//查询结果
function AE34FE_get_N01_sel_t_excel_data(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_excel_data) == false)
        return false;    
	var s_data = input.N01_sel_t_excel_data;
    AE34FE_select_t_excel_data_rowId = "";
    $('#AE34FE_t_excel_data_Events').bootstrapTable('destroy');
    $("#AE34FE_t_excel_data_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AE34FE_t_excel_data_Events').find("tr.changeColor").removeClass('changeColor');
                AE34FE_select_t_excel_data_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AE34FE_t_excel_data_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AE34FE_select_t_excel_data_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AE34FE_param.hasOwnProperty("target_name"))
			{
				$("#"+AE34FE_param["target_id"]).val(eval("row."+AE34FE_param["sourc_id"].toUpperCase()));
				$("#"+AE34FE_param["target_name"]).val(s_decode(eval("row."+AE34FE_param["sourc_name"].toUpperCase())));				
				layer.close(AE34FE_param["ly_index"]);
			}
		},
        toolbar: "#AE34FE_t_excel_data_Toolbar",
        columns: AE34FE_t_excel_data,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AE34FE_btn_t_excel_data_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    AE34FE_tem_file_id = "0";    AE34FE_tem_excel_tag_id = "0";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    BE34FE_ary_file_id = null;    BE34FE_ary_excel_tag_id = null;
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_excel_data$file_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE34FE_get_N01_t_excel_data$file_id");	
    /*biz end*/
})

//查询按钮
$('#AE34FE_btn_t_excel_data_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    AE34FE_tem_file_id = $("#AE34FE_qry_file_id").val();    AE34FE_tem_excel_tag_id = $("#AE34FE_qry_excel_tag_id").val();	
    /*set query param end*/
	AE34FE_t_excel_data_query();
})

//vue回调
function AE34FE_t_excel_data_call_vue(objResult){
	if(index_subhtml == "t_excel_data_$.vue")
	{
		var n = Get_RandomDiv("BE34FE",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AE34FE_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temfile_id = $("#AE34FE_qry_file_id").val();                if(temfile_id != ""){                    inputdata["file_id"] = temfile_id;                }                var temexcel_tag_id = $("#AE34FE_qry_excel_tag_id").val();                if(temexcel_tag_id != ""){                    inputdata["excel_tag_id"] = temexcel_tag_id;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_excel_data_$.js","BE34FE_t_excel_data_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//新增按钮
$("#AE34FE_btn_t_excel_data_add").click(function () {
	AE34FE_type = "add";
	index_subhtml = "t_excel_data_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_excel_data_$.vue","AE34FE_t_excel_data_call_vue") == true){
		var n = Get_RandomDiv("BE34FE","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BE34FE_param["type"] = AE34FE_type;
				BE34FE_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temfile_id = $("#AE34FE_qry_file_id").val();                if(temfile_id != ""){                    BE34FE_param["file_id"] = temfile_id;                }                var temexcel_tag_id = $("#AE34FE_qry_excel_tag_id").val();                if(temexcel_tag_id != ""){                    BE34FE_param["excel_tag_id"] = temexcel_tag_id;                }				
				/*Send Two FindSelect param end*/

				BE34FE_clear_edit_info();
			},
			end: function(){
				BE34FE_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AE34FE_btn_t_excel_data_edit").click(function () {
	if (AE34FE_select_t_excel_data_rowId != "") {
		AE34FE_type = "edit";
		index_subhtml = "t_excel_data_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_excel_data_$.vue","AE34FE_t_excel_data_call_vue") == true){
			var n = Get_RandomDiv("BE34FE","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BE34FE_param["type"] = AE34FE_type;
					BE34FE_param["ly_index"] = index;
					BE34FE_clear_edit_info();
					BE34FE_get_edit_info();
				},
				end: function(){
					BE34FE_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AE34FE_btn_t_excel_data_delete').click(function () {
	//单行选择
	var rowData = $("#AE34FE_t_excel_data_Events").bootstrapTable('getData')[AE34FE_select_t_excel_data_rowId];
	//多行选择
	var rowDatas = AE34FE_sel_row_t_excel_data();
	if (AE34FE_select_t_excel_data_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_excel_data",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AE34FE_N01_del_t_excel_data");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_excel_data",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AE34FE_N01_del_t_excel_data");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AE34FE_N01_del_t_excel_data(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_excel_data) == true)
		AE34FE_t_excel_data_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function AE34FE_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AE34FE_sel_row_t_excel_data(){
	//获得选中行
	var checkedbox= $("#AE34FE_t_excel_data_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}