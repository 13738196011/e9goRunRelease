//选择某一行
var C60403_select_V_VTC001_rowId = "";
//按钮事件新增或编辑
var C60403_type = "";
//其他页面传到本页面参数
var C60403_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var C60403_tem_TABLE_CN_NAME = "";var C60403_tem_TABLE_EN_NAME = "";var C60403_tem_COLUMN_CN_NAME = "";var C60403_tem_COLUMN_EN_NAME = "";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var C60403_ary_COLUMN_TYPE = [{'MAIN_ID': '1', 'CN_NAME': 'STRING'}, {'MAIN_ID': '2', 'CN_NAME': 'INT'}, {'MAIN_ID': '3', 'CN_NAME': 'FLOAT'}, {'MAIN_ID': '4', 'CN_NAME': 'DATE'}];
/*declare select options end*/

//主从表传递参数
function C60403_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function C60403_V_VTC001_biz_start(inputparam) {
	layer.close(ly_index);
	C60403_param = inputparam;
	//主从表传递参数
	C60403_param_set();	
    /*biz begin*/
    if($("#C60403_qry_COLUMN_TYPE").is("select") && $("#C60403_qry_COLUMN_TYPE")[0].options.length == 0)    {        $("#C60403_qry_COLUMN_TYPE").append("<option value='-1'></option>")        $.each(C60403_ary_COLUMN_TYPE, function (i, obj) {            addOptionValue("C60403_qry_COLUMN_TYPE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    C60403_init_V_VTC001()	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function C60403_format_COLUMN_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < C60403_ary_COLUMN_TYPE.length; i++) {        var obj = C60403_ary_COLUMN_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function C60403_page_end(){
	C60403_adjust_tab();
}

//表字段视图显示列定义
var C60403_V_VTC001 = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '字段主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '表中文名',        field: 'TABLE_CN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '表英文名',        field: 'TABLE_EN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '字段中文名',        field: 'COLUMN_CN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '字段英文名',        field: 'COLUMN_EN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '字段类型',        field: 'COLUMN_TYPE',        sortable: true        ,formatter: C60403_format_COLUMN_TYPE    }	
	/*table column end*/
];

//页面初始化
function C60403_init_V_VTC001() {
	$(window).resize(function () {
		  $('#C60403_V_VTC001_Events').bootstrapTable('resetView');
	});
	//表字段视图查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#C60403_btn_V_VTC001_query').click();
}

//查询接口
function C60403_V_VTC001_query() {
    $('#C60403_V_VTC001_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_V_VTC001",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(C60403_tem_TABLE_CN_NAME)        ,"param_value2": s_encode(C60403_tem_TABLE_EN_NAME)        ,"param_value3": s_encode(C60403_tem_COLUMN_CN_NAME)        ,"param_value4": s_encode(C60403_tem_COLUMN_EN_NAME)		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "C60403_get_N01_sel_V_VTC001");
}

//查询结果
function C60403_get_N01_sel_V_VTC001(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_V_VTC001) == false)
        return false;
    //调整table各列宽度
    $.each(C60403_V_VTC001, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_V_VTC001;
    C60403_select_V_VTC001_rowId = "";
    $('#C60403_V_VTC001_Events').bootstrapTable('destroy');
    $("#C60403_V_VTC001_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#C60403_V_VTC001_Events').find("tr.changeColor").removeClass('changeColor');
                C60403_select_V_VTC001_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#C60403_V_VTC001_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                C60403_select_V_VTC001_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#C60403_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(C60403_param.hasOwnProperty("target_name"))
			{
				$("#"+C60403_param["target_id"]).val(eval("row."+C60403_param["sourc_id"].toUpperCase()));
				var sourc_value = s_decode(row.TABLE_EN_NAME) +"=>"+ s_decode(eval("row."+C60403_param["sourc_name"].toUpperCase()))
				$("#"+C60403_param["target_name"]).val(sourc_value);				
				layer.close(C60403_param["ly_index"]);
			}
		},
        toolbar: "#C60403_V_VTC001_Toolbar",
        columns: C60403_V_VTC001,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
    C60403_page_end();
}

//刷新按钮
$('#C60403_btn_V_VTC001_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    C60403_tem_TABLE_CN_NAME = "";    C60403_tem_TABLE_EN_NAME = "";    C60403_tem_COLUMN_CN_NAME = "";    C60403_tem_COLUMN_EN_NAME = "";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/

	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    if($("#C60403_qry_COLUMN_TYPE").is("select") && $("#C60403_qry_COLUMN_TYPE")[0].options.length == 0)    {        $("#C60403_qry_COLUMN_TYPE").append("<option value='-1'></option>")        $.each(C60403_ary_COLUMN_TYPE, function (i, obj) {            addOptionValue("C60403_qry_COLUMN_TYPE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    C60403_init_V_VTC001()	
    /*biz end*/
})

//查询按钮
$('#C60403_btn_V_VTC001_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    C60403_tem_TABLE_CN_NAME = $("#C60403_qry_TABLE_CN_NAME").val();    C60403_tem_TABLE_EN_NAME = $("#C60403_qry_TABLE_EN_NAME").val();    C60403_tem_COLUMN_CN_NAME = $("#C60403_qry_COLUMN_CN_NAME").val();    C60403_tem_COLUMN_EN_NAME = $("#C60403_qry_COLUMN_EN_NAME").val();	
    /*set query param end*/
	C60403_V_VTC001_query();
})

//vue回调
function C60403_V_VTC001_call_vue(objResult){
	if(index_subhtml == "V_VTC001_$.vue")
	{
		var n = Get_RandomDiv("B6071F",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":C60403_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temTABLE_CN_NAME = $("#C60403_qry_TABLE_CN_NAME").val();                if(temTABLE_CN_NAME != ""){                    inputdata["TABLE_CN_NAME"] = temTABLE_CN_NAME;                }                var temTABLE_EN_NAME = $("#C60403_qry_TABLE_EN_NAME").val();                if(temTABLE_EN_NAME != ""){                    inputdata["TABLE_EN_NAME"] = temTABLE_EN_NAME;                }                var temCOLUMN_CN_NAME = $("#C60403_qry_COLUMN_CN_NAME").val();                if(temCOLUMN_CN_NAME != ""){                    inputdata["COLUMN_CN_NAME"] = temCOLUMN_CN_NAME;                }                var temCOLUMN_EN_NAME = $("#C60403_qry_COLUMN_EN_NAME").val();                if(temCOLUMN_EN_NAME != ""){                    inputdata["COLUMN_EN_NAME"] = temCOLUMN_EN_NAME;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/V_VTC001_$.js","B6071F_V_VTC001_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/

	/*get tab subvue end*/
}

//新增按钮
$("#C60403_btn_V_VTC001_add").click(function () {
	C60403_type = "add";
	index_subhtml = "V_VTC001_$.vue";
	if(loadHtmlSubVueFun("biz_vue/V_VTC001_$.vue","C60403_V_VTC001_call_vue") == true){
		var n = Get_RandomDiv("B6071F","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				B6071F_param["type"] = C60403_type;
				B6071F_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temTABLE_CN_NAME = $("#C60403_qry_TABLE_CN_NAME").val();                if(temTABLE_CN_NAME != ""){                    B6071F_param["TABLE_CN_NAME"] = temTABLE_CN_NAME;                }                var temTABLE_EN_NAME = $("#C60403_qry_TABLE_EN_NAME").val();                if(temTABLE_EN_NAME != ""){                    B6071F_param["TABLE_EN_NAME"] = temTABLE_EN_NAME;                }                var temCOLUMN_CN_NAME = $("#C60403_qry_COLUMN_CN_NAME").val();                if(temCOLUMN_CN_NAME != ""){                    B6071F_param["COLUMN_CN_NAME"] = temCOLUMN_CN_NAME;                }                var temCOLUMN_EN_NAME = $("#C60403_qry_COLUMN_EN_NAME").val();                if(temCOLUMN_EN_NAME != ""){                    B6071F_param["COLUMN_EN_NAME"] = temCOLUMN_EN_NAME;                }				
				/*Send Two FindSelect param end*/

				B6071F_clear_edit_info();
			},
			end: function(){
				B6071F_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#C60403_btn_V_VTC001_edit").click(function () {
	if (C60403_select_V_VTC001_rowId != "") {
		C60403_type = "edit";
		index_subhtml = "V_VTC001_$.vue";
		if(loadHtmlSubVueFun("biz_vue/V_VTC001_$.vue","C60403_V_VTC001_call_vue") == true){
			var n = Get_RandomDiv("B6071F","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					B6071F_param["type"] = C60403_type;
					B6071F_param["ly_index"] = index;
					B6071F_clear_edit_info();
					B6071F_get_edit_info();
				},
				end: function(){
					B6071F_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#C60403_btn_V_VTC001_delete').click(function () {
	//单行选择
	var rowData = $("#C60403_V_VTC001_Events").bootstrapTable('getData')[C60403_select_V_VTC001_rowId];
	//多行选择
	var rowDatas = C60403_sel_row_V_VTC001();
	if (C60403_select_V_VTC001_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_V_VTC001",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "C60403_N01_del_V_VTC001");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_V_VTC001",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "C60403_N01_del_V_VTC001");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function C60403_N01_del_V_VTC001(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_V_VTC001) == true)
		C60403_V_VTC001_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function C60403_adjust_tab(){
	if(typeof($("#C60403_TAB_MAIN")[0]) != "undefined" && $("#C60403_TAB_MAIN")[0].length != 0){
		A1BCF7_Flag = "1";
		$(Get_RDivNoBuild("C60403","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("C60403","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("C60403","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#C60403_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/

//$("#A1BCF7_tab_1").click(function(){
//	hide_tab_fun();	
//	show_tab_fun("t_proc_inparam.vue","A1F0D4");
//});
//
//$("#A1BCF7_tab_2").click(function(){
//	hide_tab_fun();
//	show_tab_fun("t_proc_return.vue","A20D9D");
//});
//
//$("#A1BCF7_tab_3").click(function(){
//	hide_tab_fun();
//	show_tab_fun("t_sub_power.vue","A24E7B");
//});
//
//$("#A1BCF7_tab_4").click(function(){
//	hide_tab_fun();
//	show_tab_fun("t_sub_userpower.vue","A29E3D");
//});

//隐藏tab页选项卡
//function hide_tab_fun(){
//	var n = null;
//	n = Get_RDivNoBuild("A1F0D4","");
//	$(n).hide();
//	n = Get_RDivNoBuild("A20D9D","");
//	$(n).hide();
//	n = Get_RDivNoBuild("A24E7B","");
//	$(n).hide();
//	n = Get_RDivNoBuild("A29E3D","");
//	$(n).hide();
//}
/*Tab Click Fun End*/

//显示tab页选项卡
//function show_tab_fun(inputUrl,inputrandom){
//  index_subhtml = inputUrl;
//  random_subhtml = inputrandom;
//  if(loadHtmlSubVueFun("biz_vue/"+inputUrl,"C60403_V_VTC001_call_vue") == true){
//      var n = Get_RandomDiv(inputrandom,"");
//		$(n).show();
//   }
//}
function show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("biz_vue/"+inputUrl,"C60403_V_VTC001_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (C60403_select_V_VTC001_rowId != "") 
			rowData = $("#C60403_V_VTC001_Events").bootstrapTable('getData')[C60403_select_t_proc_name_rowId];
		else
			rowData = $("#C60403_V_VTC001_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			$.each(temPar, function (i, obj) {
				eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
		}
		var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
		$("#"+inputrandom+"_btn_"+tbName+"_query").click();
	}
}

//清除 查找框
function C60403_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function C60403_sel_row_V_VTC001(){
	//获得选中行
	var checkedbox= $("#C60403_V_VTC001_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}