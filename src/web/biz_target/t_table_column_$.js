var type = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var ary_table_id = null;var ary_column_type = [{'MAIN_ID': '1', 'CN_NAME': 'STRING'}, {'MAIN_ID': '2', 'CN_NAME': 'INT'}, {'MAIN_ID': '3', 'CN_NAME': 'FLOAT'}, {'MAIN_ID': '4', 'CN_NAME': 'DATE'}];var ary_column_qry = [{'MAIN_ID': '0', 'CN_NAME': ''}, {'MAIN_ID': '1', 'CN_NAME': '输入框'}, {'MAIN_ID': '2', 'CN_NAME': '下拉框'}, {'MAIN_ID': '3', 'CN_NAME': '查找框'}];
/*declare select options end*/

//业务逻辑数据开始
function biz_start() {
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "T01_t_table_column$table_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "get_T01_t_table_column$table_id");	
    /*biz end*/
}

/*biz step begin*/
function format_table_id(value, row, index) {    var objResult = value;    for(i = 0; i < ary_table_id.length; i++) {        var obj = ary_table_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function format_column_type(value, row, index) {    var objResult = value;    for(i = 0; i < ary_column_type.length; i++) {        var obj = ary_column_type[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function format_column_qry(value, row, index) {    var objResult = value;    for(i = 0; i < ary_column_qry.length; i++) {        var obj = ary_column_qry[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function get_T01_t_table_column$table_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_t_table_column$table_id) == false)        return false;    ary_table_id = input.T01_t_table_column$table_id;    if($("#table_id").is("select") && $("#table_id")[0].options.length == 0)    {        $.each(ary_table_id, function (i, obj) {            addOptionValue("table_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#column_type").is("select") && $("#column_type")[0].options.length == 0)    {        $.each(ary_column_type, function (i, obj) {            addOptionValue("column_type", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#column_qry").is("select") && $("#column_qry")[0].options.length == 0)    {        $.each(ary_column_qry, function (i, obj) {            addOptionValue("column_qry", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    init_t_table_column();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function table_id_cn_name_fun(){    layer.open({        type: 2,        area: ['1000px', '570px'],        fixed: false, //不固定        maxmin: true,        content: "t_single_table.html?target_name=find_table_id_cn_name&target_id=table_id&sourc_id=MAIN_ID&sourc_name=TABLE_EN_NAME",        success: function(layero, index){            //var body = layer.getChildFrame('body',index);            //var main_id = $("#find_table_id").val();        }    });}
/*find qry fun end*/

/*页面结束*/
function page_end(){
}


//页面初始化方法
function init_t_table_column() {
	type = getUrlParam("type");
	if(type == "add"){
		$("#main_id").parent().parent().parent().hide();
	}
	else if(type == "edit"){
	
	}
	
	//表单验证
	checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#create_date").val() == "")    {        $("#create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
}

//提交表单数据
function SubmitForm(){
	if(type == "add"){
		var inputdata = {
				"param_name": "T01_ins_t_table_column",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#table_id").val()                ,"param_value2": s_encode($("#column_cn_name").val())                ,"param_value3": s_encode($("#column_en_name").val())                ,"param_value4": $("#column_type").val()                ,"param_value5": $("#column_length").val()                ,"param_value6": $("#column_qry").val()                ,"param_value7": s_encode($("#column_qry_format").val())                ,"param_value8": s_encode($("#column_find_html").val())                ,"param_value9": s_encode($("#column_find_return").val())                ,"param_value10": $("#create_date").val()                ,"param_value11": s_encode($("#s_desc").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_ins_t_table_column");
	}
	else if(type == "edit"){
		var inputdata = {
				"param_name": "T01_upd_t_table_column",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#table_id").val()                ,"param_value2": s_encode($("#column_cn_name").val())                ,"param_value3": s_encode($("#column_en_name").val())                ,"param_value4": $("#column_type").val()                ,"param_value5": $("#column_length").val()                ,"param_value6": $("#column_qry").val()                ,"param_value7": s_encode($("#column_qry_format").val())                ,"param_value8": s_encode($("#column_find_html").val())                ,"param_value9": s_encode($("#column_find_return").val())                ,"param_value10": $("#create_date").val()                ,"param_value11": s_encode($("#s_desc").val())                ,"param_value12": $("#main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_upd_t_table_column");
	}
}

//以下为页面表单验证
$("#save_t_table_column_Edit").click(function () {
	$("form[name='DataModal']").submit();
})

/*修改数据*/
function get_T01_upd_t_table_column(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_upd_t_table_column) == true)
	{
        //init();
		parent.swal("修改数据成功!", "", "success");
		parent.t_table_column_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

/*添加数据*/
function get_T01_ins_t_table_column(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_ins_t_table_column) == true)
	{
		parent.swal("添加数据成功!", "", "success");
		parent.t_table_column_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

//取消编辑
$("#cancel_t_table_column_Edit").click(function () {
	var index = parent.layer.getFrameIndex(window.name);
	parent.layer.close(index);
})

function clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

$(document).ready(function () {
});

window.onpageshow = function (e) {
    if (e.persisted || (window.performance.navigation.type == 2)) {
        is_history_back = 1;
    } else {
        is_history_back = 0;
    }
    //页面初始化
    init_page();
};

//form验证
function checkFormInput() {
    $("#DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            main_id: {}            ,table_id: {}            ,column_cn_name: {}            ,column_en_name: {}            ,column_type: {}            ,column_length: {digits: true,required : true,maxlength:10}            ,column_qry: {}            ,column_qry_format: {}            ,column_find_html: {}            ,column_find_return: {}            ,create_date: {date: true,required : true,maxlength:19}            ,s_desc: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            main_id: {}            ,table_id: {}            ,column_cn_name: {}            ,column_en_name: {}            ,column_type: {}            ,column_length: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,column_qry: {}            ,column_qry_format: {}            ,column_find_html: {}            ,column_find_return: {}            ,create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,s_desc: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	SubmitForm();
        	return false;
        }
    })
}