#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import threading
ACTIONPATH = "dbconfig.properties"
driverClassName = ""
url = ""
username = ""
password = ""
port = -1
UserDebug = "0"
devsys = "linux"
iTimeCount2 = 0
AccSocketPort = 8090
attUpFile = ".txt|.zip|.rar|.tar|.json|.jpg|.bmp|.gif|.xls|.xlsx|.sql|.doc|.docx|"
htDb = {}
htSession = {}
zxyPath = "/"
Inf_Name = "CenterData"
web_Name = [".html", ".js", ".css", ".jpg", ".gif", ".png", ".svg", ".eot", ".ttf", ".woff2", ".woff", ".ico" ,".json", ".log",".xlsx",".xls",".vue"]
Child_Code = ""
Child_Name = ""
ServerIP = "0.0.0.0"
ServerPort = 8090
If_Child_Device = 0
If_Parent_Device = 0
TimeCount = 30000
HTTP_Path = ""
SessionTimeOut = 30  # 10080  // 分钟
Login_Session = 0
htInterSession = {}
HttpUrl = ""
bTimeFlag = False
U_CODE = "UTF-8"
sub_code = "8A0731CC39614C90A5D474BC17253713"
sub_usercode = "414A6DB3BBE6419DA3768E6E25127310"
Is_UserLogin = ""
InterSession = ""
S_DESC = "zxyong我的设备"
IL_DB = 0
incrementalConnections = 2
maxConnections = 10
socket_count = 0
bThread = True
htServSockets = {}
htChildNode = {}
htChildInfo = {}
htTempChildInfo = {}
clientSocket = None
ShowSqlError = 1
strSessionAva = ""
EncryptUtilType = 1  # 1:SM32  2:MD5 3:SHA1
ServerREFLECT_IN_CLASS = "com.plugins.usereflect.testServerReflectInClass"
ClientREFLECT_IN_CLASS = "com.plugins.usereflect.testClientReflectClass"
TimeREFLECT_IN_CLASS = "com.plugins.usereflect.testTimeReflectClass"
CustTimeTaskNum = ""
CustTimeREFLECT_IN_CLASS = ""
ServerIPList = ""  # xx.x.xx.xx:9090;xx.xx.x.xx:9098;
ComPortList = ""  # /dev/ttyS0,9600,8,0,A;/dev/ttyS1.9600,8,0,B
htComPort = {}
dClientThreadList = {}
dServThreadList = {}
ApplicationPath = ""
urlPath = ""
ServerWebPort = -1
lock = threading.Lock()
Dblock1 = threading.Lock()
Dblock2 = threading.Lock()
Dblock3 = threading.Lock()
ht_cond__real_value = {}  
ht_cal_variables = {}
ht_cal_fun = {}
ht_cal_run = {}
list_cal_run_param = []
list_cal_run_step = []
Real_date = 0
iReturnUppLower = 1
MQTT_host = ""
MQTT_username = ""
MQTT_pwd = ""
MQTT_topic_pull = ""
MQTT_topic_get = ""
mqclient = None
htMqttTopicPull = {}
htMqttTopicGet = {}
MQTTREFLECT_IN_CLASS = ""
objAryRtuMaster = {}#modubusRtuMaster对象
objAryAsciiMaster = {} #modubusAsciiMaster对象
