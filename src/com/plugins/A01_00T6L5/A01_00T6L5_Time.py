#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import time,json,importlib,binascii,datetime
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_IHGDW0.A01_Fun import A01_Fun
from com.plugins.A01_IHGDW0.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_IHGDW0 import A01_Para
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.comport.ComModBus import ComModBus
from com.plugins.A01_IHGDW0.monitor_data_model import monitor_data_model
from com.plugins.usereflect.A01_RXOB6C_IN import A01_RXOB6C_IN
from openpyxl.descriptors.base import Length
from com.zxy.z_debug import z_debug
class A01_00T6L5_Time(z_debug):
    strContinue      = "1"   
    strResult        = ""
    def __init__(self):
        pass
    def init_start(self):
        try:
            temprotocol = A01_Fun.Get_protocol("A01_00T6L5")
            temCom = None
            if temprotocol is not None:
                temIndex = 0 
                temmonitor_data_model = A01_Fun.Get_monitor_sec()
                temmonitor_flag_model = A01_Fun.Get_monitor_flag_sec()
                for temv_prot_element in A01_Para.ht_v_prot_element:       
                    if temv_prot_element.attprot_name == "A01_00T6L5":                                    
                        data_val_int = 0
                        data_val = ""                        
                        temA01modbus = ComModBus()
                        for temt_modbus_addr in A01_Para.ht_t_modbus_addr:
                            if str(temv_prot_element.attmain_id) == str(temt_modbus_addr.attelement_id) and str(temv_prot_element.attprot_id) == str(temt_modbus_addr.attprotocol_id):                         
                                temCom = temt_modbus_addr.attcom_port
                                red = temA01modbus.get_data_rtu(temt_modbus_addr.attcom_port,temt_modbus_addr.attmodbus_addr,temt_modbus_addr.attmodbus_begin,temt_modbus_addr.attmodbus_length)
                                if red is not None and len(red) > 0:
                                    data_val_int = float(temA01modbus.ReadFloat(red[0],red[1],False))
                                    break
                                else:
                                    return None  
                        if data_val_int < 0.0:
                            data_val_int = 0  
                        data_val = str(round(data_val_int,temv_prot_element.attdot_num))                    
                        temIndex = temIndex + 1
                        if hasattr(temmonitor_data_model, temv_prot_element.attelement_val.lower()) and data_val != "":
                            setattr(temmonitor_data_model, temv_prot_element.attelement_val.lower(),data_val)                        
                        if hasattr(temmonitor_flag_model, temv_prot_element.attelement_val.lower()):
                            setattr(temmonitor_flag_model, temv_prot_element.attelement_val.lower(),"N")
                if A01_Para.attLock.acquire():
                    Com_Fun.SetHashTable(A01_Para.ht_monitor_sec, temmonitor_data_model.get_time, temmonitor_data_model)
                    Com_Fun.SetHashTable(A01_Para.ht_monitor_flag_sec, temmonitor_flag_model.get_time, temmonitor_flag_model)
                    A01_Para.attLock.release()
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            pass