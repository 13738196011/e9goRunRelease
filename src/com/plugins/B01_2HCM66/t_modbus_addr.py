#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_modbus_addr(object):
    attmain_id = -1
    attmodbus_gateway = ""
    attcom_port = ""
    attmodbus_addr = ""
    attmodbus_begin = ""
    attmodbus_length = 0
    attelement_id = 0
    attprotocol_id = 0
    attcreate_date = None
    atts_desc = ""
    lenmodbus_gateway = 50
    lencom_port = 50
    lenmodbus_addr = 50
    lenmodbus_begin = 50
    lens_desc = 500
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["modbus_gateway"] = "设备网关地址"
        temHt["com_port"] = "串口号"
        temHt["modbus_addr"] = "设备地址"
        temHt["modbus_begin"] = "起始位或4017通道号"
        temHt["modbus_length"] = "数据长度"
        temHt["element_id"] = "因子名称"
        temHt["protocol_id"] = "采集协议模块"
        temHt["create_date"] = "系统时间"
        temHt["s_desc"] = "备注"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["com_port"] = "串口号"
        temHtQry["element_id"] = "因子名称"
        temHtQry["protocol_id"] = "采集协议模块"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["com_port"] = "T01_t_protocol$com_port"
        temHtSelect["element_id"] = "T01_t_prot_ele_link$element_id"
        temHtSelect["protocol_id"] = "T01_t_prot_ele_link$prot_id"
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
