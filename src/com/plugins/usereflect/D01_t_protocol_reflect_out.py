#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.db_Self.Db_Common_Self import Db_Common_Self
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.z_debug import z_debug
class D01_t_protocol_reflect_out(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = "1"
    def __init__(self):
        pass
    def init_start(self):
        temdbc2 = Db_Common2()        
        temdbc_self = Db_Common_Self()
        try:
            temStrSql = "select main_id,prot_name,oem_name,s_state,com_port from t_protocol where s_state = '1' and prot_name !='A01_AA1BB2'"
            temRs = temdbc2.Common_Sql(temStrSql)
            temparam_value1 = "5;30"
            temparam_value2 = "com.plugins.A01_IHGDW0.A01_IHGDW0_Time;com.plugins.A01_TH83KV.A01_TH83KV_Time"
            iIndex = 0
            for temItem in temRs:
                temparam_value1 = temparam_value1 +";5"
                temparam_value2 = temparam_value2 +";com.plugins."+Com_Fun.NoNull(temItem[1])+"."+Com_Fun.NoNull(temItem[1])+"_Time"
                iIndex = iIndex + 1
            temStrSql = "update t_param_value set param_value = '"+temparam_value1+"' where param_key = 'CustTimeTaskNum'"
            temdbc_self.CommonExec_Sql(temStrSql)            
            temStrSql = "update t_param_value set param_value = '"+temparam_value2+"' where param_key = 'CustTimeREFLECT_IN_CLASS'"
            temdbc_self.CommonExec_Sql(temStrSql)            
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
            return False    
