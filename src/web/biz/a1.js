var columns =[
	{ title: '主键', field: GetLowUpp('main_id'),sortable : true  },
	{ title: '站点编号', field: GetLowUpp('station_code'),sortable : true  },
	{ title: '时间', field: GetLowUpp('get_time'),sortable : true  },
	{ title: '温度', field: GetLowUpp('val_01'),sortable : true },
	{ title: '湿度', field: GetLowUpp('val_02'),sortable : true }
	];
var columnData=[];
var param_name = 'D01_monitor_data_minu';
var iIndex = 0;
var iPageSize = 1000;
var param_value1 = "";
var param_value2 = "";
var param_value3 = "";
var objU = "";

$(document).ready(function () {
	initTime();
	//initTable2();
	//query2();

	//initTable();
	query();
});

function query2()
{
	$('#exampleTableEvents').bootstrapTable("removeAll");
	param_value1 = $('#station_code').val();
	param_value2 = $('#starttimeHour').val();
	param_value3 = $('#endtimeHour').val();
	param_value4 = iIndex;
	param_value5 = iPageSize;	
	objU = bizzUrl + '&param_name='+param_name;
	//+'&param_value1='+param_value1+'&param_value2='
	//	+param_value2+'&param_value3='+param_value3+"&param_value4="+param_value4+"&param_value5="+param_value5;
	initTable2();
}

function initTable2()
{
	$("#exampleTableEvents").bootstrapTable({
		url: objU,//请求后台的URL（*）
	    method: 'get',
	    dataType: "json",
	    dataField: 'rows',
	    striped: true,//设置为 true 会有隔行变色效果
	    undefinedText: "空",//当数据为 undefined 时显示的字符
	    pagination: true, //设置为 true 会在表格底部显示分页条。
	    showToggle: "true",//是否显示 切换试图（table/card）按钮
	    showColumns: "true",//是否显示 内容列下拉框
	    pageNumber: 1,//初始化加载第一页，默认第一页
	    pageSize: iPageSize,//每页的记录行数（*）
	    pageList: [30, 100, 500, 2000],//可供选择的每页的行数（*），当记录条数大于最小可选择条数时才会出现
	    paginationPreText: '上一页',
	    paginationNextText: '下一页',
	    search: false, //是否显示表格搜索,bootstrap-table服务器分页不能使用搜索功能，可以自定义搜索框，上面jsp中已经给出，操作方法也已经给出
	    striped : true,//隔行变色
	    showColumns: false,//是否显示 内容列下拉框
	    showToggle: false, //是否显示详细视图和列表视图的切换按钮
	    clickToSelect: true,  //是否启用点击选中行
	    data_local: "zh-US",//表格汉化
	    sidePagination: "server", //服务端处理分页
	    queryParamsType : "limit",//设置为 ‘limit’ 则会发送符合 RESTFul 格式的参数.
	    queryParams: function (params) {//自定义参数，这里的参数是传给后台的，我这是是分页用的
	//        请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
	//　　　queryParamsType = 'limit' ,返回参数必须包含limit, offset, search, sort, order 
	//        queryParamsType = 'undefined', 返回参数必须包含: pageSize, pageNumber, searchText, sortName, sortOrder. 
	//        返回false将会终止请求。
	        return {//这里的params是table提供的
	        	param_value1: param_value1,
	        	param_value2: param_value2,
	        	param_value3: param_value3,
	        	param_value4: params.offset,//从数据库第几条记录开始
	        	param_value5: params.limit//找多少条
	        };
	    },
	    responseHandler: function (res) {
	　　		//如果后台返回的json格式不是{rows:[{...},{...}],total:100},可以在这块处理成这样的格式
	    	//alert(res);
	    	var json_list = {};
	    	json_list["total"] = 760;
	    	json_list["totalNotFiltered"] = 760;
	    	json_list["rows"] = res[param_name];
	　　		return json_list;
	    },
	    columns: columns,
	    onLoadSuccess: function () {
	    },
	    onLoadError: function () {
	        alert("数据加载失败！");
	    }
	});
}

function initTable()
{
	$("#exampleTableEvents").bootstrapTable({
		search: !0,
		pagination: !0,
		showRefresh: !0,
		showToggle: !0,
		showColumns: !0,
		iconSize: "outline",
		toolbar: "#exampleTableEventsToolbar",
		columns: columns,
		data: columnData,
		pageNumber: 1,
		pageSize: iPageSize,      //每页的记录行数（*）
		pageList: [30, 100, 500, 2000],  //可供选择的每页的行数（*）
		icons: {
			refresh: "glyphicon-repeat",
			toggle: "glyphicon-list-alt",
			columns: "glyphicon-list"
		}
	});
}

function query()
{
	$('#exampleTableEvents').bootstrapTable("removeAll");
	var param_value1 = $('#station_code').val();
	var param_value2 = $('#starttimeHour').val();
	var param_value3 = $('#endtimeHour').val();
	var objU = bizzUrl + '&param_name='+param_name+'&param_value1='+param_value1+'&param_value2='
		+param_value2+'&param_value3='+param_value3+"&param_value4="+iIndex+"&param_value5="+30;
	$.ajax({
		type : 'POST',
		url : objU,
		dataType : 'json',
		success : function(datas) {//返回list数据并循环获取
			var data = eval('datas.'+param_name);
			for (var i = 0; i < data.length; i++) 
			{
				var jsonObj = {'main_id':data[i][GetLowUpp('main_id')],'station_code':data[i][GetLowUpp('station_code')],'get_time':set_time_decode(data[i].[GetLowUpp('get_time')]),'val_01':data[i][GetLowUpp('val_01')]};
				for(var j=4;j<columns.length;j++)
				{
					var f = columns[j][GetLowUpp(field)];
					eval('jsonObj.'+ f +' ='+ 'data[i].'+f);
				}
				columnData.push(jsonObj);
			}
			$('#exampleTableEvents').bootstrapTable('destroy');
			initTable();
		}
	});
}

function initTime()
{
	$("#endtimeHour").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));
	$('#starttimeHour').datetimepicker({ 
		format: 'yyyy-mm-dd hh:00:00',
		startView: 2, 
		todayHighlight: 1,//今天高亮
		//initialDate:new Date(),
		autoclose :true,
		forceParse: 0,//设置为0，时间不会跳转1899，会显示当前时间。
		language: 'zh-CN',//显示中文
		minView: "day",//设置只显示到月份
		todayBtn: true//显示今日按钮
	});
	$('#endtimeHour').datetimepicker({ 
		format: 'yyyy-mm-dd hh:00:00',
		startView: 2, 
		todayHighlight: 1,//今天高亮
		//initialDate:new Date(),
		autoclose :true,
		forceParse: 0,//设置为0，时间不会跳转1899，会显示当前时间。
		language: 'zh-CN',//显示中文
		minView: "day",//设置只显示到月份
		todayBtn: true//显示今日按钮
	});
	 $('#starttimeDay').datepicker({ 
		format: 'yyyy-mm-dd',
		startView: 0, maxViewMode: 0,minViewMode:0,
		initialDate:new Date(),
		autoclose :true
	});

	$('#starttimeMonth').datepicker({ 
		format: 'yyyy-mm',
		startView: 1, maxViewMode: 1,minViewMode:1,
		initialDate:'2019-01',
		autoclose :true
	});
}