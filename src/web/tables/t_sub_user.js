﻿var iTop = 0;
var ileft = 0;
var ly_index;

//接口名称
var mod_t_sub_user;

function biz_start(){
}

function T01_sel_t_sub_sys(input) {
	var data = input.T01_sel_t_sub_sys;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
	
    var options = ""
        $.each(input.T01_sel_t_sub_sys, function (i, obj) {
            if (i != input.T01_sel_t_sub_sys.length - 1) {
                options += obj.MAIN_ID + ":" + obj.SUB_NAME + ";";
            } else {
                options += obj.MAIN_ID + ":" + obj.SUB_NAME;
            }
        });

    mod_t_sub_user = [{
            label: '主键',
            name: 'MAIN_ID',
            width: '30px',
            index: 'MAIN_ID',
            editable: false,
            key: true,
            readOnly: true,
            editrules: {
                required: true
            }
        }, {
            label: '应用平台主键',
            name: 'SUB_ID',
            width: '30px',
            editable: true,
            editrules: true,
            readOnly: true,
            edittype: 'select',
            editoptions: {
                value: options
            }
        }, {
            label: '授权用户名称',
            name: 'SUB_USERNAME',
            width: '30px',
            editable: true,
            editrules: true
        }, {
            label: '用户授权码',
            name: 'SUB_USERCODE',
            width: '70px',
            editable: true,
            editrules: true,
            editoptions: {
                defaultValue: new GUID().newGUID()
            }
        }, {
            label: '使用有效期',
            name: 'LIMIT_DATE',
            width: '70px',
            editable: true,
            editrules: true,
            readOnly: true,
            formatter: function (value, row) {
                return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
            },
            editoptions: {
                defaultValue: DateAdd("y", 30, new Date()).Format('yyyy-MM-dd hh:mm:ss')
            }
        }, {
            label: '备注',
            name: 'S_DESC',
            width: '70px',
            editable: true,
            editrules: true
        }, {
            label: '系统时间',
            name: 'CREATE_DATE',
            width: '70px',
            editable: true,
            editrules: true,
            readOnly: true,
            formatter: function (value, row) {
                return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
            },
            editoptions: {
                defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')
            }
        }
    ];

    layer.close(ly_index);
    init();
}

//获取应用平台信息
function get_T01_sel_t_sub_sys() {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_t_sub_sys",
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_sub_sys",
        success: function (data2) {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

function T01_sel_t_sub_user(input) {
    data = input.T01_sel_t_sub_user;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
    $("#table_list_1").jqGrid({
        data: data,
        datatype: "local",
        height: "450",
        autowidth: true,
        shrinkToFit: true,
        rowNum: 10,
        rowList: [10, 20, 30],
        colModel: mod_t_sub_user,
        editurl: 'clientArray',
        altRows: true,
        pager: "#pager_list_1",
        viewrecords: true,
        caption: "平台用户信息列表"
    });
    //导航后面，增加增删改查按钮
    $("#table_list_1").jqGrid("navGrid", "#pager_list_1", {
        edit: true,
        add: true,
        del: true,
        search: true,
        view: false,
        position: "left",
        cloneToTop: false
    }, {
        beforeShowForm: function () {
            $('#MAIN_ID').attr('readOnly', true);
            $("#SUB_ID").attr('disabled', "disabled");
            laydate.render({
                elem: '#LIMIT_DATE',
                type: 'datetime'
            });
            laydate.render({
                elem: '#CREATE_DATE',
                type: 'datetime'
            });
        },
        onclickSubmit: updateDbByMainId,
        closeAfterEdit: true
    }, {
        beforeShowForm: function () {
            laydate.render({
                elem: '#LIMIT_DATE',
                type: 'datetime'
            });
            laydate.render({
                elem: '#CREATE_DATE',
                type: 'datetime'
            });
        },
        onclickSubmit: insertDb,
        closeAfterAdd: true
    }, {
        top: iTop,
        left: ileft,
        onclickSubmit: delDbByMainId
    }, {
        closeAfterSearch: true
    }, {
        height: 200,
        reloadAfterSubmit: true
    });
    layer.close(ly_index);
}

//初始化
function init() {
    ly_index = layer.load();
    iTop = (winHeight - 300) / 2;
    ileft = (winWidth - 300) / 2;
    $.jgrid.defaults.styleUI = "Bootstrap";
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_t_sub_user",
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_sub_user",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

$(document).ready(function () {
    init_page();
    getWindowSize();
    get_T01_sel_t_sub_sys();
    $(window).bind("resize",
        function () {
        var width = $(".jqGrid_wrapper").width();
        $("#table_list_1").setGridWidth(width);
    });
});

function T01_ins_t_sub_user(input) {
    data = input.T01_ins_t_sub_user;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
}

var insertDb = function () {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_ins_t_sub_user",
            session_id: session_id,
            param_value1: $("#SUB_ID").val(),
            param_value2: $("#SUB_USERNAME").val(),
            param_value3: $("#SUB_USERCODE").val(),
            param_value4: $("#LIMIT_DATE").val(),
            param_value5: $("#S_DESC").val(),
            param_value6: $("#CREATE_DATE").val()
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_ins_t_sub_user",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_upd_t_sub_user(input) {
    data = input.T01_upd_t_sub_user;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
    layer.close(ly_index);
}

var updateDbByMainId = function () {
    ly_index = layer.load();
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_upd_t_sub_user",
            session_id: session_id,
            param_value1: $("#SUB_USERNAME").val(),
            param_value2: $("#SUB_USERCODE").val(),
            param_value3: $("#LIMIT_DATE").val(),
            param_value4: $("#S_DESC").val(),
            param_value5: $("#CREATE_DATE").val(),
            param_value6: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_upd_t_sub_user",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_del_t_sub_user(input) {
    data = input.T01_del_t_sub_user;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
    layer.close(ly_index);

    //重新设置jqgrid大小尺寸
    //getWindowSize();
    /*
    $(" #delmodgrid-table").css({
    "top": iTop,
    "left": ileft,
    "height": "140px",
    "width": "390px"
    });*/
}

var delDbByMainId = function () {
    ly_index = layer.load();
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_del_t_sub_user",
            session_id: session_id,
            param_value1: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_del_t_sub_user",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};