#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import binascii
import struct
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.z_debug import z_debug
class A01_ATATAT_PORT(z_debug):
    def __init__(self):
        pass
    def get_data_com(self,inputComPort,CmdStr):
        Data = None         
        try:  
            com_at = Com_Fun.GetHashTable(Com_Para.htComPort,inputComPort)
            inputByte  = CmdStr.encode(Com_Para.U_CODE)
            if com_at.WritePortData(inputByte) > 0:
                comValue = com_at.attReturnValue
                if comValue is None:
                    print("com port not data return")
                    return None
                else:
                    Data = str(comValue, encoding=Com_Para.U_CODE)
                    print("com get value:"+Data)
                com_at.attReturnValue = None
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
            Data = -1
        return Data      