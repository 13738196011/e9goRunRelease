#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import time,json,datetime
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_IHGDW0.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_IHGDW0 import A01_Para
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.db2.Db_Common2 import Db_Common2
from com.plugins.A01_U5H09F.A01_U5H09F_Time import A01_U5H09F_Time
from com.plugins.A01_IHGDW0.monitor_data_model import monitor_data_model
from com.plugins.A01_IHGDW0.A01_Fun import A01_Fun
from com.plugins.A01_LXNOQU.A01_LXNOQU_Time import A01_LXNOQU_Time
from com.plugins.A01_R790KS.A01_R790KS_Time import A01_R790KS_Time
from com.plugins.A01_T8MN36.A01_T8MN36_Time import A01_T8MN36_Time
from com.plugins.A01_905MFA.A01_905MFA_Time import A01_905MFA_Time
from com.plugins.A01_HHT335.A01_HHT335_Time import A01_HHT335_Time
from com.zxy.z_debug import z_debug
class A01_IHGDW0_Time(z_debug):
    strContinue      = "1"   
    strResult        = ""
    def __init__(self):
        pass
    def init_start(self):
        printFlag          = "a"
        try:
            bodc = Bus_Ope_DB_Cent()
            printFlag = "b"
            if A01_Para.codsampletime is None:
                A01_Para.codsampletime = datetime.datetime.now()
            temModel_Time = None
            for it in range(int(60 / int(Com_Fun.GetHashTable(A01_Para.ht_t_param_value,"realdata_interval")))):
                printFlag = "c"
                if int(Com_Fun.GetTime("%S")) >= it * int(Com_Fun.GetHashTable(A01_Para.ht_t_param_value,"realdata_interval")) - int(Com_Fun.GetHashTable(A01_Para.ht_t_param_value,"sec_interval")) and int(Com_Fun.GetTime("%S")) < it * int(Com_Fun.GetHashTable(A01_Para.ht_t_param_value,"realdata_interval")):
                    printFlag = "d"
                    for temv_server_protocol in A01_Para.ht_v_server_protocol:
                        printFlag = "e"
                        if temv_server_protocol.attprot_name == "A01_LXNOQU":
                            printFlag = "f"
                            a01_time = A01_LXNOQU_Time()
                            a01_time.strResult = self.strResult
                            a01_time.strIP = temv_server_protocol.attserver_ip
                            a01_time.strPort = temv_server_protocol.attserver_port
                            printFlag = "g"
                            a01_time.SendC14()
                            printFlag = "h"
                        elif temv_server_protocol.attprot_name == "A01_R790KS":
                            printFlag = "i"
                            a01_time = A01_R790KS_Time()
                            printFlag = "j"
                            a01_time.strResult = self.strResult
                            a01_time.strIP = temv_server_protocol.attserver_ip
                            a01_time.strPort = temv_server_protocol.attserver_port
                            a01_time.SendC14()
                            printFlag = "k"
                        elif temv_server_protocol.attprot_name == "A0_T8MN36":
                            printFlag = "i1"
                            a01_time = A01_T8MN36_Time()
                            printFlag = "j2"
                            a01_time.strResult = self.strResult
                            a01_time.strIP = temv_server_protocol.attserver_ip
                            a01_time.strPort = temv_server_protocol.attserver_port
                            a01_time.SendC14()
                            printFlag = "k2"
                    break
                it = it + 1
            printFlag = "l"
            if int(Com_Fun.GetTime("%M")) >= 20 and int(Com_Fun.GetTime("%M")) < 21 and int(Com_Fun.GetTime("%S")) >= 20 and int(Com_Fun.GetTime("%S")) < 25:
                printFlag = "m"
                bodc.init_page()
                printFlag = "n"
            if  int(Com_Fun.GetTime("%S")) < int(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "sec_interval")):
                newGUID = Com_Fun.Get_New_GUID()
                logInfo = "GUID:"+newGUID+"\r\n"
                printFlag = "o"
                temmonitor_data_model = monitor_data_model()
                printFlag = "p"
                temmonitor_data_model.get_time = Com_Fun.DateTimeAdd(datetime.datetime.now(),"M",-1).strftime("%Y-%m-%d %H:%M:%S")
                printFlag = "q"
                temIndex = [0]*len(A01_Para.ht_t_code_element)
                temLog = ""
                for key in list(A01_Para.ht_monitor_sec.keys()):
                    printFlag = "r"                    
                    temMDM = A01_Para.ht_monitor_sec[key]
                    temTime = ""
                    printFlag = "s"  
                    if isinstance(temmonitor_data_model.get_time, datetime.datetime) :
                        printFlag = "t"  
                        temTime = temmonitor_data_model.get_time.strftime("%Y-%m-%d %H:%M")
                        printFlag = "u"  
                    else:
                        printFlag = "v"  
                        temTime = temmonitor_data_model.get_time[0:16]
                        printFlag = "w"
                    printFlag = "x" 
                    if key[0:16] == temTime :  
                        iI2 = 0  
                        temLog = temLog + "main_id:"+str(temMDM.main_id)+"    "
                        temLog = temLog + "station_code:"+temMDM.station_code+"    "
                        printFlag = "y"  
                        temLog = temLog + "get_time:"+str(key)+"    "              
                        for tcount in A01_Para.ht_t_code_element:                                
                            temV = getattr(temMDM, tcount.attelement_val.lower())
                            temA = getattr(temmonitor_data_model, tcount.attelement_val.lower())
                            tem_Ary = getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")                            
                            if tcount.attelement_val == "val_23":
                                logInfo = logInfo+" 秒累加值："+str(temA)+" max平均值:"+str(tem_Ary[1])+" count数量:"+str(int(temIndex[iI2]))+"\r\n"                      
                            FloatV = 0
                            if temA == "" and temV != "":
                                temIndex[iI2] = temIndex[iI2] + 1  
                                if tcount.attelement_code == "a01011":
                                    FloatV = float(temV)*float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "speed_ratio"))
                                else:
                                    FloatV = float(temV)
                                if tem_Ary[0] > FloatV or tem_Ary[0] == 0:
                                    tem_Ary[0] = round(FloatV,tcount.attdot_num)
                                tem_Ary[1] = round(FloatV,tcount.attdot_num)
                                if tem_Ary[2] < FloatV:
                                    tem_Ary[2] = round(FloatV,tcount.attdot_num)                     
                                tem_Ary[3] = tem_Ary[1]#round(FloatV,tcount.attdot_num)              
                                tem_Ary[4] = tem_Ary[0]#round(FloatV,tcount.attdot_num)              
                                tem_Ary[5] = tem_Ary[1]#round(FloatV,tcount.attdot_num)              
                                tem_Ary[6] = tem_Ary[2]#round(FloatV,tcount.attdot_num)
                                setattr(temmonitor_data_model,tcount.attelement_val.lower(),str(round(FloatV,tcount.attdot_num)))
                                setattr(temmonitor_data_model,tcount.attelement_val.lower()+"_ary",tem_Ary)
                            elif temV != "":
                                calV = 0
                                if tcount.attelement_code == "a01011":
                                    calV = float(temV)*float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "speed_ratio"))
                                else:
                                    calV = float(temV)
                                temIndex[iI2] = temIndex[iI2] + 1
                                FloatV = float(temA)+calV
                                if tem_Ary[0] > calV or tem_Ary[0] == 0:
                                    tem_Ary[0] = round(calV,tcount.attdot_num) 
                                tem_Ary[1] = round(FloatV,tcount.attdot_num)
                                if tem_Ary[2] < calV:
                                    tem_Ary[2] = round(calV,tcount.attdot_num)
                                tem_Ary[3] = tem_Ary[1]#round(calV,tcount.attdot_num)              
                                tem_Ary[4] = tem_Ary[0]#round(calV,tcount.attdot_num)              
                                tem_Ary[5] = tem_Ary[1]#round(calV,tcount.attdot_num)              
                                tem_Ary[6] = tem_Ary[2]#round(calV,tcount.attdot_num)
                                setattr(temmonitor_data_model,tcount.attelement_val.lower(),str(round(FloatV,tcount.attdot_num)))
                                setattr(temmonitor_data_model,tcount.attelement_val.lower()+"_ary",tem_Ary)
                            iI2 = iI2 + 1                            
                            temLog = temLog + tcount.attelement_val.lower()+":"+getattr(temmonitor_data_model, tcount.attelement_val.lower())+"    "
                        printFlag = "z"  
                        temLog = temLog + "update_time:"+str(temMDM.update_time)+"    "
                        temLog = temLog + "m_flag:"+str(temMDM.m_flag)+"    \r\n"                
                if len(A01_Para.ht_monitor_sec) > 6 :
                    printFlag = "a1" 
                    iNum2 = 0    
                    temSec = "{" 
                    temSec = temSec + "\"main_id\":\""+str(temmonitor_data_model.main_id)+"\","
                    temSec = temSec + "\"station_code\":\""+temmonitor_data_model.station_code+"\","
                    printFlag = "a2" 
                    temSec = temSec + "\"get_time\":\""+str(temmonitor_data_model.get_time)+"\","  
                    printFlag = "a3"        
                    va01011 = 0
                    va01011_ary = [0,0,0,0,0,0,0,0,0,0,0,0]
                    va00000 = 0
                    va00000_ary = [0,0,0,0,0,0,0,0,0,0,0,0]    
                    FloatV = None          
                    temmonitor_data_sec_last = A01_Fun.Get_monitor_data_sec_last()                   
                    for tcount in A01_Para.ht_t_code_element:
                        printFlag = "a3_1"  
                        temA = getattr(temmonitor_data_model, tcount.attelement_val.lower())
                        printFlag = "a3_2"
                        temA_Ary = getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")
                        if tcount.attelement_val == "val_23":
                            logInfo = logInfo+" 分钟值："+str(temA)+" max平均值:"+str(temA_Ary[1])+" count数量:"+str(temIndex[iNum2])+"\r\n"                      
                        printFlag = "a3_3"
                        if temA != "":
                            if tcount.attelement_val == "val_23":
                                logInfo = logInfo+" 分钟值："+str(temA)+" max平均值:"+str(temA_Ary[3])+" count数量:"+str(temIndex[iNum2])+"\r\n"                      
                            printFlag = "a3_4"
                            FloatV = float(temA)/temIndex[iNum2]
                            intC = int(temIndex[iNum2])
                            tem = float(temA_Ary[1]) / intC
                            temA_Ary[1] = round(tem,tcount.attdot_num)
                            tem = float(temA_Ary[3]) / intC
                            temA_Ary[3] = round(tem,tcount.attdot_num)
                            tem = float(temA_Ary[5]) / intC
                            temA_Ary[5] = round(tem,tcount.attdot_num)
                            setattr(temmonitor_data_model,tcount.attelement_val.lower(),str(round(FloatV,tcount.attdot_num)))
                            setattr(temmonitor_data_model,tcount.attelement_val.lower()+"_ary",temA_Ary)
                            temSec = temSec + "\""+tcount.attelement_val.lower()+"\":\""+getattr(temmonitor_data_model, tcount.attelement_val.lower())+"\","                                                            
                        if tcount.attelement_code == "a01011" and FloatV is not None:
                            printFlag = "a3_5"
                            va01011 = FloatV
                            va01011_ary = temA_Ary
                        if tcount.attelement_code == "w00000" and FloatV is not None:                                    
                            printFlag = "w3_5"
                            va00000 = FloatV
                            va00000_ary = temA_Ary
                            timeInterval = int(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "timeInterval"))                                                        
                            if Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "w00000") != "":
                                if FloatV is not None and FloatV >= float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "w00000")):
                                    A01_Para.w000000time = Com_Fun.GetToTimeInput('%Y-%m-%d %H:%M:%S',str(temmonitor_data_model.get_time))
                                    temA01_HHT = A01_HHT335_Time()
                                    temA01_HHT.param_value1 = "1"
                                    temA01_HHT.init_start()
                                    uL = UsAdmin_Log(Com_Para.ApplicationPath, "send sample begin do===>持续供样时间:"+Com_Fun.GetTimeDef()+" 最小流量:"+str(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "w00000"))+" 有效流量:"+str(FloatV))
                                    uL.SaveFileDaySub("sendsample")
                                    if temA01_HHT.Judge_Do_Sample() == True:
                                        if A01_Para.codsampletime is not None and A01_Para.codsampletime + datetime.timedelta(seconds=timeInterval) <= datetime.datetime.now():
                                            temA01_95M = A01_905MFA_Time()
                                            temA01_95M.init_start()
                                            uL = UsAdmin_Log(Com_Para.ApplicationPath, "do sample begin do===>上次做样时间:"+str(A01_Para.codsampletime)+" 本次做样时间:"+Com_Fun.GetTimeDef()+" 时间间隔:"+str(timeInterval)+" 有效流量时间:"+str(A01_Para.w000000time))
                                            uL.SaveFileDaySub("dosample")                                    
                                            A01_Para.codsampletime = datetime.datetime.now()                                    
                                elif A01_Para.w000000data >= float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "w00000")) :
                                    A01_Para.w000000time = Com_Fun.GetToTimeInput('%Y-%m-%d %H:%M:%S',str(temmonitor_data_model.get_time))
                                else:
                                    if A01_Para.w000000time is None:
                                        A01_Para.w000000time = Com_Fun.GetToTimeInput('%Y-%m-%d %H:%M:%S',str(temmonitor_data_model.get_time))
                                A01_Para.w000000data = FloatV                                
                                temLog = "==>流量值:"+str(round(FloatV,4))+" 有效流量时间:"+str(A01_Para.w000000time)+" 当前时间:"+Com_Fun.GetTimeDef()
                                uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
                                uL.SaveFileDaySub("dosample")                                        
                                if A01_Para.codsampletime is not None and A01_Para.codsampletime + datetime.timedelta(seconds=timeInterval) < datetime.datetime.now():
                                    if A01_Para.w000000time is not None and A01_Para.w000000time +datetime.timedelta(seconds=2*timeInterval) >= datetime.datetime.now():
                                        temA01_95M = A01_905MFA_Time()
                                        temA01_95M.init_start()
                                        uL = UsAdmin_Log(Com_Para.ApplicationPath, "do sample begin do===>上次做样时间:"+str(A01_Para.codsampletime)+" 本次做样时间:"+Com_Fun.GetTimeDef()+" 时间间隔:"+str(timeInterval)+" 有效流量时间:"+str(A01_Para.w000000time))
                                        uL.SaveFileDaySub("dosample")                                        
                                        A01_Para.codsampletime = datetime.datetime.now()
                                    else:
                                        temA01_HHT = A01_HHT335_Time()
                                        temA01_HHT.param_value1 = "0"
                                        temA01_HHT.init_start()
                            else:
                                pass
                        if tcount.attelement_code == "a00000":
                            printFlag = "a3_6"
                            temA = va01011 * float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "area_measure"))
                            temA_Ary[0] = round(va01011_ary[0] * float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "area_measure")),tcount.attdot_num)
                            temA_Ary[1] = round(va01011_ary[1] * float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "area_measure")),tcount.attdot_num)
                            temA_Ary[2] = round(va01011_ary[2] * float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "area_measure")),tcount.attdot_num)
                            temA_Ary[3] = round(va01011_ary[3] * float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "area_measure")),tcount.attdot_num)
                            temA_Ary[4] = round(va01011_ary[4] * float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "area_measure")),tcount.attdot_num)
                            temA_Ary[5] = round(va01011_ary[5] * float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "area_measure")),tcount.attdot_num)
                            temA_Ary[6] = round(va01011_ary[6] * float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "area_measure")),tcount.attdot_num)
                            if temA_Ary[1] != "":
                                temA_Ary[9] = temA_Ary[1] * 5
                            setattr(temmonitor_data_model,tcount.attelement_val.lower(),str(round(temA,tcount.attdot_num)))
                            setattr(temmonitor_data_model,tcount.attelement_val.lower()+"_ary",temA_Ary)
                        if tcount.attelement_code == "w00000":
                            printFlag = "w3_6"
                            temA = va00000
                            temA_Ary[0] = round(va00000_ary[0] ,tcount.attdot_num)
                            temA_Ary[1] = round(va00000_ary[1] ,tcount.attdot_num)
                            temA_Ary[2] = round(va00000_ary[2] ,tcount.attdot_num)
                            temA_Ary[3] = round(va00000_ary[3] ,tcount.attdot_num)
                            temA_Ary[4] = round(va00000_ary[4] ,tcount.attdot_num)
                            temA_Ary[5] = round(va00000_ary[5] ,tcount.attdot_num)
                            temA_Ary[6] = round(va00000_ary[6] ,tcount.attdot_num)
                            if temA_Ary[1] != "":
                                temA_Ary[9] = temA_Ary[1] /60
                            setattr(temmonitor_data_model,tcount.attelement_val.lower(),str(round(temA,tcount.attdot_num)))
                            setattr(temmonitor_data_model,tcount.attelement_val.lower()+"_ary",temA_Ary)
                        if tcount.attelement_code == "a05002" or tcount.attelement_code == "a24087" or tcount.attelement_code == "a24088" or tcount.attelement_code == "a25002" or tcount.attelement_code == "a25003" or tcount.attelement_code == "a25005"  or tcount.attelement_code == "a24087" :
                            printFlag = "a3_7"
                            varTV = getattr(temmonitor_data_sec_last,tcount.attelement_val.lower())
                            if varTV != "":
                                setattr(temmonitor_data_model,tcount.attelement_val.lower(),str(round(float(varTV),tcount.attdot_num)))
                        iNum2 = iNum2 + 1
                    temSec = temSec + "\"update_time\":\""+str(temmonitor_data_model.update_time)+"\""
                    temSec = temSec +"}"
                    printFlag = "a4" 
                    printFlag = "a5" 
                    A01_Fun.SaveDataFile(Com_Para.ApplicationPath+Com_Para.zxyPath+"minute", temSec)
                    printFlag = "a6" 
                    temModel_Time = temmonitor_data_model.get_time
                    bodc.ins_minute(temmonitor_data_model)
                    printFlag = "a7" 
                    bodc.ins_minute_flag()
                    printFlag = "a8" 
                    for temv_server_protocol in A01_Para.ht_v_server_protocol:
                        if temv_server_protocol.attprot_name == "A01_LXNOQU":
                            printFlag = "a9" 
                            a01_time = A01_LXNOQU_Time()
                            a01_time.strResult = self.strResult
                            a01_time.strIP = temv_server_protocol.attserver_ip
                            a01_time.strPort = temv_server_protocol.attserver_port
                            a01_time.SendC16(temmonitor_data_model)
                            printFlag = "a10" 
                        elif temv_server_protocol.attprot_name == "A01_R790KS":
                            printFlag = "a11" 
                            a01_time = A01_R790KS_Time()
                            a01_time.strResult = self.strResult
                            a01_time.strIP = temv_server_protocol.attserver_ip
                            a01_time.strPort = temv_server_protocol.attserver_port
                            a01_time.SendC16(temmonitor_data_model)
                            printFlag = "a12" 
                        elif temv_server_protocol.attprot_name == "A01_T8MN36":
                            printFlag = "a11_2" 
                            a01_time = A01_T8MN36_Time()
                            a01_time.strResult = self.strResult
                            a01_time.strIP = temv_server_protocol.attserver_ip
                            a01_time.strPort = temv_server_protocol.attserver_port
                            a01_time.SendC16(temmonitor_data_model)
                            printFlag = "a12_2" 
                    if int(int(Com_Fun.GetTime("%M")) % 20) == 0:
                        printFlag = "a13" 
                        temmonitor_data_model.get_time = Com_Fun.DateTimeAdd(datetime.datetime.now(),"H",-1).strftime("%Y-%m-%d %H:%M:%S")
                        printFlag = "a14" 
                        if bodc.get_hour_had(temmonitor_data_model) == False:
                            printFlag = "a15" 
                            temhourmonitor_data_model = bodc.ins_data_hour(temmonitor_data_model)
                            printFlag = "a16" 
                            temhourmonitor_flag_model = bodc.ins_flag_hour(temmonitor_data_model)
                            printFlag = "a17" 
                        if bodc.get_hour_max_had(temmonitor_data_model) == False:
                            inputList = bodc.get_hour(temmonitor_data_model)
                            temhourmonitor_max_model = bodc.cal_max_data(temmonitor_data_model, inputList, 2)
                            printFlag = "a17_k" 
                            bodc.ins_max_hour(temhourmonitor_max_model)
                for key in list(A01_Para.ht_monitor_sec.keys()):
                    printFlag = "a18" 
                    printFlag = "a19" 
                    if temModel_Time is not None and Com_Fun.GetToTimeInput("%Y-%m-%d %H:%M:%S",key) < Com_Fun.DateTimeAdd(Com_Fun.GetToTimeInput("%Y-%m-%d %H:%M:%S",temModel_Time),"M",-1):#Com_Fun.GetToTimeInput("%Y-%m-%d %H:%M:%S",temModel_Time):                                
                        printFlag = "a20"
                        A01_Para.attLock.acquire()
                        del A01_Para.ht_monitor_sec[key]
                        A01_Para.attLock.release() 
                for key in list(A01_Para.ht_monitor_flag_sec.keys()):
                    printFlag = "a21"
                    if temModel_Time is not None and Com_Fun.GetToTimeInput("%Y-%m-%d %H:%M:%S",key) < Com_Fun.DateTimeAdd(Com_Fun.GetToTimeInput("%Y-%m-%d %H:%M:%S",temModel_Time),"M",-1):#Com_Fun.GetToTimeInput("%Y-%m-%d %H:%M:%S",temModel_Time):                                
                        printFlag = "a21"
                        A01_Para.attLock.acquire()
                        del A01_Para.ht_monitor_flag_sec[key]
                        A01_Para.attLock.release()
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            pass