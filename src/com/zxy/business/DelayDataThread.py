#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.zxy.common import Com_Para
from com.zxy.z_debug import z_debug
class DelayDataThread(z_debug):
    attPcl = None
    attSub_code = ""
    attParam_name = ""
    attAryParamValue = []
    attDelay_code = ""
    attSession_id = ""
    attStrIP = "0.0.0.0"
    attHtParam = {}
    def __init__(self, temPcl, inputSub_code, inputParam_name, inputAryParamValue, inputDelay_code, inputStrIP,inputSession_id,inputHtParam):
        self.attPcl = temPcl
        self.attSub_code = inputSub_code
        self.attParam_name = inputParam_name
        self.attAryParamValue = inputAryParamValue
        self.attDelay_code = inputDelay_code
        self.attStrIP = inputStrIP
        self.attSession_id = inputSession_id
        self.attHtParam = inputHtParam
    def run(self):
        temOpd = Ope_DB_Cent()
        temSbd = temOpd.Cal_Data(self.attPcl,self.attSub_code,self.attParam_name,self.attAryParamValue,self.attStrIP,self.attSession_id,self.attHtParam)
        temUL = UsAdmin_Log(Com_Para.ApplicationPath,temSbd,0)
        try:
            temUL.InitWriteData(self.attDelay_code)
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息