#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import datetime,time,serial
from com.zxy.z_debug import z_debug
class testTimeReflectClass(z_debug):
    strContinue      = "1"   
    strResult        = ""
    def __init__(self):
        pass
    def test_comport(self):
        attSerial = serial.Serial("/dev/ttyAMA0",115200,8)
        attSerial.timeout = 10        
        inputString = '680B007316'
        inputByte  = bytes().fromhex(inputString)
        print("send byte:"+str(inputByte))
        if not attSerial.isOpen():
            attSerial.open()        
        attSerial.write(inputByte)
        print("get byte:"+attSerial.readline())
        starttime = datetime.datetime.now()    
        endtime = datetime.datetime.now() + datetime.timedelta(seconds=10)
        while starttime <= endtime:
            time.sleep(0.2)
            temNum = attSerial.inWaiting()
            if temNum > 0:
                valuestr = attSerial.read(temNum)
                print("get com info:"+str(valuestr))
                break
            starttime = datetime.datetime.now()
        print("end prog")
        attSerial.close()
    def init_start(self):
            pass
