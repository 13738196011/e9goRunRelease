//选择某一行
var AE339C_select_t_prot_ele_link_rowId = "";
//按钮事件新增或编辑
var AE339C_type = "";
//其他页面传到本页面参数
var AE339C_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var AE339C_tem_prot_id = "0";var AE339C_tem_element_id = "0";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var AE339C_ary_prot_id = null;var AE339C_ary_element_id = null;
/*declare select options end*/

//业务逻辑数据开始
function AE339C_t_prot_ele_link_biz_start(inputparam) {
	layer.close(ly_index);
	AE339C_param = inputparam;
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_prot_ele_link$prot_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE339C_get_N01_t_prot_ele_link$prot_id");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function AE339C_format_prot_id(value, row, index) {    var objResult = value;    for(i = 0; i < AE339C_ary_prot_id.length; i++) {        var obj = AE339C_ary_prot_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AE339C_format_element_id(value, row, index) {    var objResult = value;    for(i = 0; i < AE339C_ary_element_id.length; i++) {        var obj = AE339C_ary_element_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AE339C_get_N01_t_prot_ele_link$prot_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_prot_ele_link$prot_id) == false)        return false;    AE339C_ary_prot_id = input.N01_t_prot_ele_link$prot_id;    var inputdata = {        "param_name": "N01_t_prot_ele_link$element_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE339C_get_N01_t_prot_ele_link$element_id");}function AE339C_get_N01_t_prot_ele_link$element_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_prot_ele_link$element_id) == false)        return false;    AE339C_ary_element_id = input.N01_t_prot_ele_link$element_id;    $("#AE339C_qry_prot_id").append("<option value='-1'></option>")    $.each(AE339C_ary_prot_id, function (i, obj) {        addOptionValue("AE339C_qry_prot_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    $("#AE339C_qry_element_id").append("<option value='-1'></option>")    $.each(AE339C_ary_element_id, function (i, obj) {        addOptionValue("AE339C_qry_element_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    AE339C_init_t_prot_ele_link();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function AE339C_page_end(){
}

//协议因子关联设置显示列定义
var AE339C_t_prot_ele_link = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '采集协议名称',        field: 'PROT_ID',        sortable: true        ,formatter: AE339C_format_prot_id    },    {        title: '因子名称',        field: 'ELEMENT_ID',        sortable: true        ,formatter: AE339C_format_element_id    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '备注信息',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: 'AD模块量程上限',        field: 'PROT_UP_LIMIT',        sortable: true        ,formatter: set_s_decode    },    {        title: 'AD模块量程下限',        field: 'PROT_DOWN_LIMIT',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function AE339C_init_t_prot_ele_link() {
	$(window).resize(function () {
		  $('#AE339C_t_prot_ele_link_Events').bootstrapTable('resetView');
	});
	//协议因子关联设置查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AE339C_btn_t_prot_ele_link_query').click();
}

//查询接口
function AE339C_t_prot_ele_link_query() {
    $('#AE339C_t_prot_ele_link_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_prot_ele_link",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": AE339C_tem_prot_id        ,"param_value2": AE339C_tem_prot_id        ,"param_value3": AE339C_tem_element_id        ,"param_value4": AE339C_tem_element_id		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AE339C_get_N01_sel_t_prot_ele_link");
}

//查询结果
function AE339C_get_N01_sel_t_prot_ele_link(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_prot_ele_link) == false)
        return false;    
	var s_data = input.N01_sel_t_prot_ele_link;
    AE339C_select_t_prot_ele_link_rowId = "";
    $('#AE339C_t_prot_ele_link_Events').bootstrapTable('destroy');
    $("#AE339C_t_prot_ele_link_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AE339C_t_prot_ele_link_Events').find("tr.changeColor").removeClass('changeColor');
                AE339C_select_t_prot_ele_link_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AE339C_t_prot_ele_link_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AE339C_select_t_prot_ele_link_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AE339C_param.hasOwnProperty("target_name"))
			{
				$("#"+AE339C_param["target_id"]).val(eval("row."+AE339C_param["sourc_id"].toUpperCase()));
				$("#"+AE339C_param["target_name"]).val(s_decode(eval("row."+AE339C_param["sourc_name"].toUpperCase())));				
				layer.close(AE339C_param["ly_index"]);
			}
		},
        toolbar: "#AE339C_t_prot_ele_link_Toolbar",
        columns: AE339C_t_prot_ele_link,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AE339C_btn_t_prot_ele_link_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    AE339C_tem_prot_id = "0";    AE339C_tem_element_id = "0";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    BE339C_ary_prot_id = null;    BE339C_ary_element_id = null;
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_prot_ele_link$prot_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE339C_get_N01_t_prot_ele_link$prot_id");	
    /*biz end*/
})

//查询按钮
$('#AE339C_btn_t_prot_ele_link_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    AE339C_tem_prot_id = $("#AE339C_qry_prot_id").val();    AE339C_tem_element_id = $("#AE339C_qry_element_id").val();	
    /*set query param end*/
	AE339C_t_prot_ele_link_query();
})

//vue回调
function AE339C_t_prot_ele_link_call_vue(objResult){
	if(index_subhtml == "t_prot_ele_link_$.vue")
	{
		var n = Get_RandomDiv("BE339C",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AE339C_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temprot_id = $("#AE339C_qry_prot_id").val();                if(temprot_id != ""){                    inputdata["prot_id"] = temprot_id;                }                var temelement_id = $("#AE339C_qry_element_id").val();                if(temelement_id != ""){                    inputdata["element_id"] = temelement_id;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_prot_ele_link_$.js","BE339C_t_prot_ele_link_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//新增按钮
$("#AE339C_btn_t_prot_ele_link_add").click(function () {
	AE339C_type = "add";
	index_subhtml = "t_prot_ele_link_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_prot_ele_link_$.vue","AE339C_t_prot_ele_link_call_vue") == true){
		var n = Get_RandomDiv("BE339C","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BE339C_param["type"] = AE339C_type;
				BE339C_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temprot_id = $("#AE339C_qry_prot_id").val();                if(temprot_id != ""){                    BE339C_param["prot_id"] = temprot_id;                }                var temelement_id = $("#AE339C_qry_element_id").val();                if(temelement_id != ""){                    BE339C_param["element_id"] = temelement_id;                }				
				/*Send Two FindSelect param end*/

				BE339C_clear_edit_info();
			},
			end: function(){
				BE339C_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AE339C_btn_t_prot_ele_link_edit").click(function () {
	if (AE339C_select_t_prot_ele_link_rowId != "") {
		AE339C_type = "edit";
		index_subhtml = "t_prot_ele_link_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_prot_ele_link_$.vue","AE339C_t_prot_ele_link_call_vue") == true){
			var n = Get_RandomDiv("BE339C","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BE339C_param["type"] = AE339C_type;
					BE339C_param["ly_index"] = index;
					BE339C_clear_edit_info();
					BE339C_get_edit_info();
				},
				end: function(){
					BE339C_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AE339C_btn_t_prot_ele_link_delete').click(function () {
	//单行选择
	var rowData = $("#AE339C_t_prot_ele_link_Events").bootstrapTable('getData')[AE339C_select_t_prot_ele_link_rowId];
	//多行选择
	var rowDatas = AE339C_sel_row_t_prot_ele_link();
	if (AE339C_select_t_prot_ele_link_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_prot_ele_link",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AE339C_N01_del_t_prot_ele_link");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_prot_ele_link",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AE339C_N01_del_t_prot_ele_link");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AE339C_N01_del_t_prot_ele_link(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_prot_ele_link) == true)
		AE339C_t_prot_ele_link_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function AE339C_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AE339C_sel_row_t_prot_ele_link(){
	//获得选中行
	var checkedbox= $("#AE339C_t_prot_ele_link_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}