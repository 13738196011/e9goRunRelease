#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import json, importlib, os,platform,datetime,xlrd,sys
from xlrd import xldate_as_tuple
from urllib.parse import unquote
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.db_Self.Db_Common_Self import Db_Common_Self
from com.plugins.A01_IHGDW0 import A01_Para
from com.plugins.A01_IHGDW0.t_code_element import t_code_element
from com.plugins.A01_IHGDW0.v_prot_element import v_prot_element
from com.plugins.A01_IHGDW0.t_modbus_addr import t_modbus_addr
from com.plugins.A01_IHGDW0.t_protocol import t_protocol
from com.plugins.A01_IHGDW0.v_server_protocol import v_server_protocol
from com.plugins.A01_IHGDW0.monitor_data_model import monitor_data_model
from com.plugins.A01_IHGDW0.A01_Fun import A01_Fun
from com.zxy.business.Power_Control import Power_Control
from com.plugins.usereflect.D01_add_t_server_ip_out import D01_add_t_server_ip_out
from com.plugins.usereflect.D01_t_com_port_reflect_out import D01_t_com_port_reflect_out
from com.plugins.usereflect.D01_t_protocol_reflect_out import D01_t_protocol_reflect_out
from com.zxy.z_debug import z_debug
class Bus_Ope_DB_Cent(z_debug):
    def __init__(self):
        pass
    def init_page(self):
        temTSI = D01_add_t_server_ip_out()
        temTSI.init_start()
        temTCR = D01_t_com_port_reflect_out()
        temTCR.init_start()
        temTRO = D01_t_protocol_reflect_out()
        temTRO.init_start()
        self.monitor_had(0)
        self.monitor_had(30)
        if len(A01_Para.ht_t_param_value) == 0 :
            self.Get_t_param_value()
            A01_Para.ht_t_code_element = self.Get_t_code_element()
            A01_Para.ht_t_modbus_addr = self.Get_t_modbus_addr()
            A01_Para.ht_t_protocol = self.Get_t_protocol()
            A01_Para.ht_v_prot_element = self.Get_v_prot_element()
            A01_Para.ht_v_server_protocol = self.Get_v_server_protocol()
    def ins_minute(self,temmonitor_data_model):
        temdbc2 = Db_Common2()
        tbName = "monitor_data_min"+Com_Fun.GetDateInput("%Y%m","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)
        strSql = "insert into "+tbName+"(station_code,get_time,update_time,m_flag"
        strValue = " values('"+temmonitor_data_model.station_code+"','"+temmonitor_data_model.get_time[0:16]+":00"+"','"+Com_Fun.GetTimeDef()+"','0'"
        tbName2 = "monitor_max_min"+Com_Fun.GetDateInput("%Y%m","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)
        strSql2 = "insert into "+tbName2+"(station_code,get_time,update_time,m_flag"
        strValue2 = " values('"+temmonitor_data_model.station_code+"','"+temmonitor_data_model.get_time[0:16]+":00"+"','"+Com_Fun.GetTimeDef()+"','0'"
        for tcount in A01_Para.ht_t_code_element:
            strSql = strSql+","+tcount.attelement_val
            strValue = strValue +",'"+getattr(temmonitor_data_model, tcount.attelement_val.lower())+"'"
            strSql2 = strSql2+","+tcount.attelement_val
            strValue2 = strValue2 +",'"+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary"))+"'"
        strRunSql = strSql +") "+strValue+")"
        strRunSql2 = strSql2 +") "+strValue2+")"
        temdbc2.CommonExec_Sql(strRunSql)
        temdbc2.CommonExec_Sql(strRunSql2)
    def ins_minute_flag(self):
        temKey = ""
        for key in list(A01_Para.ht_monitor_flag_sec.keys()):
            if temKey < key:
                temKey = key
        if temKey is not None and temKey != "":
            temmonitor_flag_model = A01_Fun.Get_monitor_flag_sec()
            temdbc2 = Db_Common2()
            tbName = "monitor_flag_min"+Com_Fun.GetDateInput("%Y%m","%Y-%m-%d %H:%M:%S",temmonitor_flag_model.get_time)
            strSql = "insert into "+tbName+"(station_code,get_time,update_time,m_flag"
            strValue = " values('"+temmonitor_flag_model.station_code+"','"+temmonitor_flag_model.get_time[0:16]+":00"+"','"+Com_Fun.GetTimeDef()+"','0'"
            for tcount in A01_Para.ht_t_code_element:
                strSql = strSql+","+tcount.attelement_val
                strValue = strValue +",'"+getattr(temmonitor_flag_model, tcount.attelement_val.lower())+"'"
            strRunSql = strSql +") "+strValue+")"
            temdbc2.CommonExec_Sql(strRunSql)
            temSec = "{" 
            temSec = temSec + "\"main_id\":\""+str(temmonitor_flag_model.main_id)+"\","
            temSec = temSec + "\"station_code\":\""+temmonitor_flag_model.station_code+"\","
            temSec = temSec + "\"get_time\":\""+str(temmonitor_flag_model.get_time)+"\","                                
            for tcount in A01_Para.ht_t_code_element:
                temSec = temSec + "\""+tcount.attelement_val.lower()+"\":\""+getattr(temmonitor_flag_model, tcount.attelement_val.lower())+"\","                                                            
            temSec = temSec + "\"update_time\":\""+str(temmonitor_flag_model.update_time)+"\""
            temSec = temSec +"}"
            A01_Fun.SaveDataFile(Com_Para.ApplicationPath+Com_Para.zxyPath+"minflag", temSec)
    def get_day_had(self,temmonitor_data_model):
        temStrSql = "select count(1) from monitor_data_day where strftime('%Y-%m-%d 00:00:00',get_time) = '"+Com_Fun.GetDateInput("%Y-%m-%d 00:00:00","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)+"'"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        try:
            for temItem in temRs:
                if Com_Fun.ZeroNull(temItem[0]) > 0:
                    return True
                else:
                    return False
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
            return False
    def get_hour_had(self,temmonitor_data_model):
        temStrSql = "select count(1) from monitor_data_hour where strftime('%Y-%m-%d %H:00:00',get_time) = '"+Com_Fun.GetDateInput("%Y-%m-%d %H:00:00","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)+"'"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        try:
            for temItem in temRs:
                if Com_Fun.ZeroNull(temItem[0]) > 0:
                    return True
                else:
                    return False
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
            return False
    def get_hour_max_had(self,temmonitor_data_model):
        temStrSql = "select count(1) from monitor_max_hour where strftime('%Y-%m-%d %H:00:00',get_time) = '"+Com_Fun.GetDateInput("%Y-%m-%d %H:00:00","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)+"'"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        try:
            for temItem in temRs:
                if Com_Fun.ZeroNull(temItem[0]) > 0:
                    return True
                else:
                    return False
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
            return False
    def set_upload_day_flag(self,temmonitor_hour_model,inputFlag):
        temdbc2 = Db_Common2()
        strSql = "update monitor_data_day set "+inputFlag+"='1' where get_time='"+temmonitor_hour_model.get_time+"'"
        temdbc2.CommonExec_Sql(strSql)
        strSql = "update monitor_flag_hour set "+inputFlag+"='1' where get_time='"+temmonitor_hour_model.get_time+"'"
        temdbc2.CommonExec_Sql(strSql)
        strSql = "update monitor_max_hour set "+inputFlag+"='1' where get_time='"+temmonitor_hour_model.get_time+"'"
        temdbc2.CommonExec_Sql(strSql)
    def set_upload_hour_flag(self,temmonitor_hour_model,inputFlag):
        temdbc2 = Db_Common2()
        strSql = "update monitor_data_hour set "+inputFlag+"='1' where get_time='"+temmonitor_hour_model.get_time+"'"
        temdbc2.CommonExec_Sql(strSql)
        strSql = "update monitor_flag_hour set "+inputFlag+"='1' where get_time='"+temmonitor_hour_model.get_time+"'"
        temdbc2.CommonExec_Sql(strSql)
        strSql = "update monitor_max_hour set "+inputFlag+"='1' where get_time='"+temmonitor_hour_model.get_time+"'"
        temdbc2.CommonExec_Sql(strSql)
    def get_noupload_max_hour(self):
        temdbc2 = Db_Common2()
        strSql = "select main_id,station_code,get_time "
        tbName = "monitor_max_hour"
        for tcount in A01_Para.ht_t_code_element:
            strSql = strSql+","+tcount.attelement_val
        strSql = strSql + " from " + tbName+" where (1 !=1 "
        temFlag = False
        for serv in A01_Para.ht_v_server_protocol:
            if serv.attprot_name != "":
                if temFlag == False:
                    strSql = strSql + "or m_flag"+str(serv.atts_desc)+"='0' "
                    temFlag = True
                else:
                    strSql = strSql + "or m_flag"+str(serv.atts_desc)+"='0' "
        strSql = strSql + ") order by get_time asc  limit 1000"
        temList = []
        temRs = temdbc2.Common_Sql(strSql)
        try:
            for temItem in temRs:
                hourmonitor_max_model = monitor_data_model()
                hourmonitor_max_model.main_id = Com_Fun.ZeroNull(temItem[0])
                hourmonitor_max_model.station_code = Com_Fun.NoNull(temItem[1])
                hourmonitor_max_model.get_time = Com_Fun.NoNull(temItem[2])
                vk = 1
                for tcount in A01_Para.ht_t_code_element:
                    setattr(hourmonitor_max_model, tcount.attelement_val.lower(),Com_Fun.NoNull(temItem[2+vk]))
                    vk = vk + 1
                temList.append(hourmonitor_max_model)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+strSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+strSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def get_noupload_max_day(self,temmonitor_day_model):
        inputList = self.get_hour(temmonitor_day_model)
        temHour = self.cal_max_data(temmonitor_day_model, inputList, 3)
        return temHour
    def get_noupload_flag_day(self,temmonitor_day_model):
        temdbc2 = Db_Common2()
        strSql = "select main_id,station_code,get_time"
        tbName = "monitor_flag_hour"
        for tcount in A01_Para.ht_t_code_element:
            strSql = strSql+","+tcount.attelement_val
        strSql = strSql + " from " + tbName+" where strftime('%Y-%m-%d 00:00:00',get_time) = '"+temmonitor_day_model.get_time+"'"
        strSql = strSql + " order by get_time desc limit 1"
        temRs = temdbc2.Common_Sql(strSql)
        hourmonitor_flag_model = monitor_data_model()
        try:
            for temItem in temRs:
                hourmonitor_flag_model.main_id = Com_Fun.ZeroNull(temItem[0])
                hourmonitor_flag_model.station_code = Com_Fun.NoNull(temItem[1])
                hourmonitor_flag_model.get_time = Com_Fun.NoNull(temItem[2])
                vk = 1
                for tcount in A01_Para.ht_t_code_element:
                    setattr(hourmonitor_flag_model, tcount.attelement_val.lower(),Com_Fun.NoNull(temItem[2+vk]))
                    vk = vk + 1
                break
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+strSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+strSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return hourmonitor_flag_model
    def get_noupload_flag_hour(self):
        temdbc2 = Db_Common2()
        strSql = "select main_id,station_code,get_time"
        tbName = "monitor_flag_hour"
        for tcount in A01_Para.ht_t_code_element:
            strSql = strSql+","+tcount.attelement_val
        strSql = strSql + " from " + tbName+" where ( 1 != 1 "
        temFlag = False
        for serv in A01_Para.ht_v_server_protocol:
            if serv.attprot_name != "":
                if temFlag == False:
                    strSql = strSql + "or m_flag"+str(serv.atts_desc)+"='0' "
                    temFlag = True
                else:
                    strSql = strSql + "or m_flag"+str(serv.atts_desc)+"='0' "
        strSql = strSql + ") order by get_time asc limit 1000"
        temList = []
        temRs = temdbc2.Common_Sql(strSql)
        try:
            for temItem in temRs:
                hourmonitor_flag_model = monitor_data_model()
                hourmonitor_flag_model.main_id = Com_Fun.ZeroNull(temItem[0])
                hourmonitor_flag_model.station_code = Com_Fun.NoNull(temItem[1])
                hourmonitor_flag_model.get_time = Com_Fun.NoNull(temItem[2])
                vk = 1
                for tcount in A01_Para.ht_t_code_element:
                    setattr(hourmonitor_flag_model, tcount.attelement_val.lower(),Com_Fun.NoNull(temItem[2+vk]))
                    vk = vk + 1
                temList.append(hourmonitor_flag_model)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+strSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+strSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def get_noupload_data_day(self):
        temdbc2 = Db_Common2()
        strSql = "select main_id,station_code,get_time,m_flag,m_flag1,m_flag2,m_flag3,m_flag4,m_flag5,m_flag6,m_flag7,m_flag8,m_flag9 "
        tbName = "monitor_data_day"
        for tcount in A01_Para.ht_t_code_element:
            strSql = strSql+","+tcount.attelement_val
        strSql = strSql + " from " + tbName+" where (1 != 1 "
        temFlag = False
        for serv in A01_Para.ht_v_server_protocol:
            if serv.attprot_name != "":
                if temFlag == False:
                    strSql = strSql + "or m_flag"+str(serv.atts_desc)+"='0' "
                    temFlag = True
                else:
                    strSql = strSql + "or m_flag"+str(serv.atts_desc)+"='0' "
        strSql = strSql + ") order by get_time asc limit 1000 "
        temList = []
        temRs = temdbc2.Common_Sql(strSql)
        try:
            for temItem in temRs:
                tvhourmonitor_data_model = monitor_data_model()
                tvhourmonitor_data_model.main_id = Com_Fun.ZeroNull(temItem[0])
                tvhourmonitor_data_model.station_code = Com_Fun.NoNull(temItem[1])
                tvhourmonitor_data_model.get_time = Com_Fun.NoNull(temItem[2])
                tvhourmonitor_data_model.m_flag = Com_Fun.NoNull(temItem[3])
                tvhourmonitor_data_model.m_flag1 = Com_Fun.NoNull(temItem[4])
                tvhourmonitor_data_model.m_flag2 = Com_Fun.NoNull(temItem[5])
                tvhourmonitor_data_model.m_flag3 = Com_Fun.NoNull(temItem[6])
                tvhourmonitor_data_model.m_flag4 = Com_Fun.NoNull(temItem[7])
                tvhourmonitor_data_model.m_flag5 = Com_Fun.NoNull(temItem[8])
                tvhourmonitor_data_model.m_flag6 = Com_Fun.NoNull(temItem[9])
                tvhourmonitor_data_model.m_flag7 = Com_Fun.NoNull(temItem[10])
                tvhourmonitor_data_model.m_flag8 = Com_Fun.NoNull(temItem[11])
                tvhourmonitor_data_model.m_flag9 = Com_Fun.NoNull(temItem[12])
                vk = 1
                for tcount in A01_Para.ht_t_code_element:
                    setattr(tvhourmonitor_data_model, tcount.attelement_val.lower(),Com_Fun.NoNull(temItem[12+vk]))
                    vk = vk + 1
                temList.append(tvhourmonitor_data_model)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+strSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+strSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def get_noupload_data_hour(self):
        temdbc2 = Db_Common2()
        strSql = "select main_id,station_code,get_time,m_flag,m_flag1,m_flag2,m_flag3,m_flag4,m_flag5,m_flag6,m_flag7,m_flag8,m_flag9 "
        tbName = "monitor_data_hour"
        for tcount in A01_Para.ht_t_code_element:
            strSql = strSql+","+tcount.attelement_val
        strSql = strSql + " from " + tbName+" where ( 1 !=1"
        temFlag = False
        for serv in A01_Para.ht_v_server_protocol:
            if serv.attprot_name != "":
                if temFlag == False:
                    strSql = strSql + " or m_flag"+str(serv.atts_desc)+"='0' "
                    temFlag = True
                else:
                    strSql = strSql + "or m_flag"+str(serv.atts_desc)+"='0' "
        strSql = strSql + ") order by get_time asc limit 1000 "
        temList = []
        temRs = temdbc2.Common_Sql(strSql)
        try:
            for temItem in temRs:
                tvhourmonitor_data_model = monitor_data_model()
                tvhourmonitor_data_model.main_id = Com_Fun.ZeroNull(temItem[0])
                tvhourmonitor_data_model.station_code = Com_Fun.NoNull(temItem[1])
                tvhourmonitor_data_model.get_time = Com_Fun.NoNull(temItem[2])
                tvhourmonitor_data_model.m_flag = Com_Fun.NoNull(temItem[3])
                tvhourmonitor_data_model.m_flag1 = Com_Fun.NoNull(temItem[4])
                tvhourmonitor_data_model.m_flag2 = Com_Fun.NoNull(temItem[5])
                tvhourmonitor_data_model.m_flag3 = Com_Fun.NoNull(temItem[6])
                tvhourmonitor_data_model.m_flag4 = Com_Fun.NoNull(temItem[7])
                tvhourmonitor_data_model.m_flag5 = Com_Fun.NoNull(temItem[8])
                tvhourmonitor_data_model.m_flag6 = Com_Fun.NoNull(temItem[9])
                tvhourmonitor_data_model.m_flag7 = Com_Fun.NoNull(temItem[10])
                tvhourmonitor_data_model.m_flag8 = Com_Fun.NoNull(temItem[11])
                tvhourmonitor_data_model.m_flag9 = Com_Fun.NoNull(temItem[12])
                vk = 1
                for tcount in A01_Para.ht_t_code_element:
                    setattr(tvhourmonitor_data_model, tcount.attelement_val.lower(),Com_Fun.NoNull(temItem[12+vk]))
                    vk = vk + 1
                temList.append(tvhourmonitor_data_model)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+strSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+strSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def ins_data_day(self,temmonitor_data_model):
        temdbc2 = Db_Common2()
        daymonitor_data_model = monitor_data_model()
        temupdate_time = Com_Fun.GetTimeDef()
        strSqlIns = "insert into monitor_data_day(station_code,get_time,update_time,m_flag"
        tbName = "monitor_data_hour"
        strSql = "select station_code,strftime('%Y-%m-%d 00:00:00',get_time) as get_time,'"+temupdate_time+"','0'"
        for tcount in A01_Para.ht_t_code_element:
            strSqlIns = strSqlIns +","+tcount.attelement_val
            strSql = strSql+",round(sum("+tcount.attelement_val+") / sum(case when "+tcount.attelement_val+" != '' then 1 else 0 end),"+str(tcount.attdot_num)+") as "+tcount.attelement_val
        strSql = strSql + " from " + tbName +" where get_time >= '"+Com_Fun.GetDateInput("%Y-%m-%d 00:00:00","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)+"' and get_time <= '"+Com_Fun.GetDateInput("%Y-%m-%d 23:59:59","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)+"' group by strftime('%Y-%m-%d 00:00:00',get_time)"   
        daymonitor_data_model.station_code = temmonitor_data_model.station_code
        daymonitor_data_model.get_time = Com_Fun.GetDateInput("%Y-%m-%d 00:00:00","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)
        daymonitor_data_model.update_time = temupdate_time
        temRs = temdbc2.Common_Sql(strSql)
        try:
            for temItem in temRs:
                iIndex = 4
                for tcount in A01_Para.ht_t_code_element:
                    setattr(daymonitor_data_model, tcount.attelement_val.lower(),Com_Fun.NoNull(temItem[iIndex]))
                    iIndex = iIndex + 1
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+strSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+strSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        strRunSql = strSqlIns +") "+ strSql
        temdbc2.CommonExec_Sql(strRunSql)
        return daymonitor_data_model
    def ins_data_hour(self,temmonitor_data_model):
        temdbc2 = Db_Common2()
        hourmonitor_data_model = monitor_data_model()
        temupdate_time = Com_Fun.GetTimeDef()
        strSqlIns = "insert into monitor_data_hour(station_code,get_time,update_time,m_flag"
        tbName = "monitor_data_min"+Com_Fun.GetDateInput("%Y%m","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)
        strSql = "select station_code,strftime('%Y-%m-%d %H:00:00',get_time) as get_time,'"+temupdate_time+"','0'"
        for tcount in A01_Para.ht_t_code_element:
            strSqlIns = strSqlIns +","+tcount.attelement_val
            strSql = strSql+",round(sum("+tcount.attelement_val+") / sum(case when "+tcount.attelement_val+" != '' then 1 else 0 end),"+str(tcount.attdot_num)+") as "+tcount.attelement_val
        strSql = strSql + " from " + tbName +" where get_time >= '"+Com_Fun.GetDateInput("%Y-%m-%d %H:00:00","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)+"' and get_time <= '"+Com_Fun.GetDateInput("%Y-%m-%d %H:59:59","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)+"' group by strftime('%Y-%m-%d %H:00:00',get_time)"   
        hourmonitor_data_model.station_code = temmonitor_data_model.station_code
        hourmonitor_data_model.get_time = Com_Fun.GetDateInput("%Y-%m-%d %H:00:00","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)
        hourmonitor_data_model.update_time = temupdate_time
        temRs = temdbc2.Common_Sql(strSql)
        try:
            for temItem in temRs:
                iIndex = 4
                for tcount in A01_Para.ht_t_code_element:
                    setattr(hourmonitor_data_model, tcount.attelement_val.lower(),Com_Fun.NoNull(temItem[iIndex]))
                    iIndex = iIndex + 1
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+strSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+strSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        strRunSql = strSqlIns +") "+ strSql
        temdbc2.CommonExec_Sql(strRunSql)
        return hourmonitor_data_model
    def ins_flag_hour(self,temmonitor_data_model):
        temKey = ""
        for key in list(A01_Para.ht_monitor_flag_sec.keys()):
            if temKey < key:
                temKey = key
        if temKey is not None and temKey != "":
            temmonitor_flag_model = A01_Fun.Get_monitor_flag_real()          
            temdbc2 = Db_Common2()
            tbName = "monitor_flag_hour"
            strSql = "insert into "+tbName+"(station_code,get_time,update_time,m_flag"
            strValue = " values('"+temmonitor_flag_model.station_code+"','"+Com_Fun.GetDateInput("%Y-%m-%d %H:00:00","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)+"','"+Com_Fun.GetTimeDef()+"','0'"
            for tcount in A01_Para.ht_t_code_element:
                strSql = strSql+","+tcount.attelement_val
                strValue = strValue +",'"+getattr(temmonitor_flag_model, tcount.attelement_val.lower())+"'"
            strRunSql = strSql +") "+strValue+")"
            temdbc2.CommonExec_Sql(strRunSql)
            return temmonitor_flag_model
        else:
            return None
    def ins_max_hour(self,temmonitor_data_model):
        temdbc2 = Db_Common2()
        tbName = "monitor_max_hour"
        strSql = "insert into "+tbName+"(station_code,get_time,update_time,m_flag"
        strValue = " values('"+temmonitor_data_model.station_code+"','"+temmonitor_data_model.get_time[0:13]+":00:00"+"','"+Com_Fun.GetTimeDef()+"','0'"
        for tcount in A01_Para.ht_t_code_element:
            strSql = strSql+","+tcount.attelement_val
            strValue = strValue +",'"+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary"))+"'"
        strRunSql = strSql +") "+strValue+")"
        temdbc2.CommonExec_Sql(strRunSql)
    def get_min_data(self,temmonitor_data_model):
        temdbc2 = Db_Common2()
        strSql = "select main_id,station_code,get_time,m_flag,m_flag1,m_flag2,m_flag3,m_flag4,m_flag5,m_flag6,m_flag7,m_flag8,m_flag9 "
        tbName = "monitor_data_min"+Com_Fun.GetDateInput("%Y%m","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)
        for tcount in A01_Para.ht_t_code_element:
            strSql = strSql+","+tcount.attelement_val
        strSql = strSql + " from " + tbName+" where strftime('%Y-%m-%d %H:00:00',get_time) ='"+Com_Fun.GetDateInput("%Y-%m-%d %H:00:00","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)+"'  order by get_time asc "
        temList = []
        temRs = temdbc2.Common_Sql(strSql)
        try:
            for temItem in temRs:
                tvhourmonitor_data_model = monitor_data_model()
                tvhourmonitor_data_model.main_id = Com_Fun.ZeroNull(temItem[0])
                tvhourmonitor_data_model.station_code = Com_Fun.NoNull(temItem[1])
                tvhourmonitor_data_model.get_time = Com_Fun.NoNull(temItem[2])
                tvhourmonitor_data_model.m_flag = Com_Fun.NoNull(temItem[3])
                tvhourmonitor_data_model.m_flag1 = Com_Fun.NoNull(temItem[4])
                tvhourmonitor_data_model.m_flag2 = Com_Fun.NoNull(temItem[5])
                tvhourmonitor_data_model.m_flag3 = Com_Fun.NoNull(temItem[6])
                tvhourmonitor_data_model.m_flag4 = Com_Fun.NoNull(temItem[7])
                tvhourmonitor_data_model.m_flag5 = Com_Fun.NoNull(temItem[8])
                tvhourmonitor_data_model.m_flag6 = Com_Fun.NoNull(temItem[9])
                tvhourmonitor_data_model.m_flag7 = Com_Fun.NoNull(temItem[10])
                tvhourmonitor_data_model.m_flag8 = Com_Fun.NoNull(temItem[11])
                tvhourmonitor_data_model.m_flag9 = Com_Fun.NoNull(temItem[12])
                vk = 1
                for tcount in A01_Para.ht_t_code_element:
                    setattr(tvhourmonitor_data_model, tcount.attelement_val.lower(),Com_Fun.NoNull(temItem[12+vk]))
                    vk = vk + 1
                temList.append(tvhourmonitor_data_model)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+strSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+strSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def get_min_flag_last(self):
        temdbc2 = Db_Common2()
        strSql = "select main_id,station_code,get_time,m_flag,m_flag1,m_flag2,m_flag3,m_flag4,m_flag5,m_flag6,m_flag7,m_flag8,m_flag9 "
        tbName = "monitor_flag_min202107"#+Com_Fun.GetTime("%Y%m")
        for tcount in A01_Para.ht_t_code_element:
            strSql = strSql+","+tcount.attelement_val
        strSql = strSql + " from " + tbName+"  order by get_time desc limit 1 "
        tvhourmonitor_data_model = monitor_data_model()
        temRs = temdbc2.Common_Sql(strSql)
        try:
            for temItem in temRs:
                tvhourmonitor_data_model.main_id = Com_Fun.ZeroNull(temItem[0])
                tvhourmonitor_data_model.station_code = Com_Fun.NoNull(temItem[1])
                tvhourmonitor_data_model.get_time = Com_Fun.NoNull(temItem[2])
                tvhourmonitor_data_model.m_flag = Com_Fun.NoNull(temItem[3])
                tvhourmonitor_data_model.m_flag1 = Com_Fun.NoNull(temItem[4])
                tvhourmonitor_data_model.m_flag2 = Com_Fun.NoNull(temItem[5])
                tvhourmonitor_data_model.m_flag3 = Com_Fun.NoNull(temItem[6])
                tvhourmonitor_data_model.m_flag4 = Com_Fun.NoNull(temItem[7])
                tvhourmonitor_data_model.m_flag5 = Com_Fun.NoNull(temItem[8])
                tvhourmonitor_data_model.m_flag6 = Com_Fun.NoNull(temItem[9])
                tvhourmonitor_data_model.m_flag7 = Com_Fun.NoNull(temItem[10])
                tvhourmonitor_data_model.m_flag8 = Com_Fun.NoNull(temItem[11])
                tvhourmonitor_data_model.m_flag9 = Com_Fun.NoNull(temItem[12])
                vk = 1
                for tcount in A01_Para.ht_t_code_element:
                    setattr(tvhourmonitor_data_model, tcount.attelement_val.lower(),Com_Fun.NoNull(temItem[12+vk]))
                    vk = vk + 1
                break
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+strSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+strSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return tvhourmonitor_data_model
    def get_min_data_last(self):
        temdbc2 = Db_Common2()
        strSql = "select main_id,station_code,get_time,m_flag,m_flag1,m_flag2,m_flag3,m_flag4,m_flag5,m_flag6,m_flag7,m_flag8,m_flag9 "
        tbName = "monitor_data_min202107"#+Com_Fun.GetTime("%Y%m")
        for tcount in A01_Para.ht_t_code_element:
            strSql = strSql+","+tcount.attelement_val
        strSql = strSql + " from " + tbName+"  order by get_time desc limit 1 "
        tvhourmonitor_data_model = monitor_data_model()
        temRs = temdbc2.Common_Sql(strSql)
        try:
            for temItem in temRs:
                tvhourmonitor_data_model.main_id = Com_Fun.ZeroNull(temItem[0])
                tvhourmonitor_data_model.station_code = Com_Fun.NoNull(temItem[1])
                tvhourmonitor_data_model.get_time = Com_Fun.NoNull(temItem[2])
                tvhourmonitor_data_model.m_flag = Com_Fun.NoNull(temItem[3])
                tvhourmonitor_data_model.m_flag1 = Com_Fun.NoNull(temItem[4])
                tvhourmonitor_data_model.m_flag2 = Com_Fun.NoNull(temItem[5])
                tvhourmonitor_data_model.m_flag3 = Com_Fun.NoNull(temItem[6])
                tvhourmonitor_data_model.m_flag4 = Com_Fun.NoNull(temItem[7])
                tvhourmonitor_data_model.m_flag5 = Com_Fun.NoNull(temItem[8])
                tvhourmonitor_data_model.m_flag6 = Com_Fun.NoNull(temItem[9])
                tvhourmonitor_data_model.m_flag7 = Com_Fun.NoNull(temItem[10])
                tvhourmonitor_data_model.m_flag8 = Com_Fun.NoNull(temItem[11])
                tvhourmonitor_data_model.m_flag9 = Com_Fun.NoNull(temItem[12])
                vk = 1
                for tcount in A01_Para.ht_t_code_element:
                    setattr(tvhourmonitor_data_model, tcount.attelement_val.lower(),Com_Fun.NoNull(temItem[12+vk]))
                    vk = vk + 1
                break
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+strSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+strSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return tvhourmonitor_data_model
    def cal_max_data(self,inputmonitor_data_model,inputList,inputType):    
        if inputType == 1:
            temmonitor_data_model = monitor_data_model()
            try:
                temmonitor_data_model.get_time = Com_Fun.DateTimeAdd(datetime.datetime.now(),"M",-1).strftime("%Y-%m-%d %H:%M:%S")
                temIndex = [0]*len(A01_Para.ht_t_code_element)
                temLog = ""
                for key in list(inputList.keys()):
                    temMDM = inputList[key]
                    temTime = ""
                    if isinstance(temmonitor_data_model.get_time, datetime.datetime) :
                        temTime = temmonitor_data_model.get_time.strftime("%Y-%m-%d %H:%M")
                    else:
                        temTime = temmonitor_data_model.get_time[0:16]
                    if key[0:16] == temTime :  
                        iI2 = 0
                        temLog = temLog + "main_id:"+str(temMDM.main_id)+"    "
                        temLog = temLog + "station_code:"+temMDM.station_code+"    "
                        temLog = temLog + "get_time:"+str(key)+"    "
                        for tcount in A01_Para.ht_t_code_element:
                            temV = getattr(temMDM, tcount.attelement_val.lower())
                            temA = getattr(temmonitor_data_model, tcount.attelement_val.lower())
                            tem_Ary = getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")                            
                            FloatV = 0
                            if temA == "" and temV != "":
                                temIndex[iI2] = temIndex[iI2] + 1  
                                if tcount.attelement_code == "a01011":
                                    FloatV = float(temV)*float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "speed_ratio"))
                                else:
                                    FloatV = float(temV)
                                if tem_Ary[0] > FloatV or tem_Ary[0] == 0:
                                    tem_Ary[0] = round(FloatV,tcount.attdot_num)
                                tem_Ary[1] = tem_Ary[1] + round(FloatV,tcount.attdot_num)
                                if tem_Ary[2] < FloatV:
                                    tem_Ary[2] = round(FloatV,tcount.attdot_num)                     
                                setattr(temmonitor_data_model,tcount.attelement_val.lower(),str(round(FloatV,tcount.attdot_num)))
                                setattr(temmonitor_data_model,tcount.attelement_val.lower()+"_ary",tem_Ary)
                            elif temV != "":
                                calV = 0
                                if tcount.attelement_code == "a01011":
                                    calV = float(temV)*float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "speed_ratio"))
                                else:
                                    calV = float(temV)
                                temIndex[iI2] = temIndex[iI2] + 1
                                FloatV = float(temA)+calV
                                if tem_Ary[0] > calV or tem_Ary[0] == 0:
                                    tem_Ary[0] = round(calV,tcount.attdot_num) 
                                tem_Ary[1] = tem_Ary[1] + round(FloatV,tcount.attdot_num)
                                if tem_Ary[2] < calV:
                                    tem_Ary[2] = round(calV,tcount.attdot_num)
                                setattr(temmonitor_data_model,tcount.attelement_val.lower(),str(round(FloatV,tcount.attdot_num)))
                                setattr(temmonitor_data_model,tcount.attelement_val.lower()+"_ary",tem_Ary)
                            iI2 = iI2 + 1                            
                            temLog = temLog + tcount.attelement_val.lower()+":"+temV+"    "
                        temLog = temLog + "update_time:"+str(temMDM.update_time)+"    "
                        temLog = temLog + "m_flag:"+str(temMDM.m_flag)+"    \r\n"
                a01011 = 0
                iI2 = 0
                for tcount in A01_Para.ht_t_code_element:
                    temA = getattr(temmonitor_data_model, tcount.attelement_val.lower())
                    if temIndex[iI2] > 0:
                        temA = float(temA) / temIndex[iI2]
                    elif temA != "":
                        temA = float(temA)
                    else:
                        temA = 0
                    if tcount.attelement_code == "a01011": 
                        a01011 = float(temA)
                        break
                    iI2 = iI2 + 1
                a00000 = 0
                for tcount in A01_Para.ht_t_code_element:    
                    if tcount.attelement_code == "a00000":
                        a00000 = a01011 * float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "area_measure"))
                        break
                    elif tcount.attelement_code == "w00000":
                        a00000 = temA
                        break
                iI2 = 0
                for tcount in A01_Para.ht_t_code_element:
                    temA = getattr(temmonitor_data_model, tcount.attelement_val.lower())
                    if temIndex[iI2] > 0:
                        temA = float(temA) / temIndex[iI2]
                    elif temA != "":
                        temA = float(temA)
                    else:
                        temA = 0
                    tem_Ary = getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")  
                    tem_Ary[1] = round(temA,tcount.attdot_num)
                    tem_Ary[4] = tem_Ary[1]
                    tem_Ary[5] = tem_Ary[0]
                    tem_Ary[6] = tem_Ary[1]
                    tem_Ary[7] = tem_Ary[2]
                    if tcount.attelement_code != "a00000" and tcount.attelement_code != "a01011" and tcount.attelement_code != "w00000":
                        if Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "station_type") == "2":
                            tem_Ary[9] = round(a00000 * 60 * tem_Ary[1],tcount.attdot_num)
                        elif Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "station_type") == "1":
                            tem_Ary[9] = round(a00000 * tem_Ary[1],tcount.attdot_num)
                    elif tcount.attelement_code == "a00000" :
                        tem_Ary[9] = round(a00000 * 60,tcount.attdot_num)
                    elif tcount.attelement_code == "w00000" :
                        tem_Ary[9] = round(a00000,tcount.attdot_num)
                    setattr(temmonitor_data_model,tcount.attelement_val.lower(),str(round(temA,tcount.attdot_num)))
                    setattr(temmonitor_data_model,tcount.attelement_val.lower()+"_ary",tem_Ary)
                    iI2 = iI2 + 1
            except Exception as e:
                if str(type(self)) == "<class 'type'>":
                    self.debug_in(self,repr(e))#打印异常信息
                else:
                    self.debug_in(repr(e))#打印异常信息
            return temmonitor_data_model
        elif inputType == 2:
            temmonitor_data_model = monitor_data_model()
            try:
                temmonitor_data_model.get_time = inputmonitor_data_model.get_time
                temIndex = [0]*len(A01_Para.ht_t_code_element)
                temLog = ""
                for temkey in inputList:
                    temTime = ""
                    printFlag = "s"  
                    if isinstance(temmonitor_data_model.get_time, datetime.datetime) :
                        temTime = temmonitor_data_model.get_time.strftime("%Y-%m-%d %H")
                    else:
                        temTime = temmonitor_data_model.get_time[0:13]
                    printFlag = "x"
                    if temkey.get_time[0:13] == temTime :  
                        iI2 = 0
                        temLog = temLog + "main_id:"+str(temkey.main_id)+"    "
                        temLog = temLog + "station_code:"+temkey.station_code+"    "
                        printFlag = "y"  
                        temLog = temLog + "get_time:"+str(temkey.get_time)+"    "
                        for tcount in A01_Para.ht_t_code_element:
                            temV = getattr(temkey, tcount.attelement_val.lower())
                            temA = getattr(temmonitor_data_model, tcount.attelement_val.lower())
                            tem_Ary = getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")                            
                            FloatV = 0
                            if temA == "" and temV != "":
                                temIndex[iI2] = temIndex[iI2] + 1  
                                if tcount.attelement_code == "a01011":
                                    FloatV = float(temV)*float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "speed_ratio"))
                                else:
                                    FloatV = float(temV)
                                if tem_Ary[0] > FloatV or tem_Ary[0] == 0:
                                    tem_Ary[0] = round(FloatV,tcount.attdot_num)
                                tem_Ary[1] = tem_Ary[1] + round(FloatV,tcount.attdot_num)
                                if tem_Ary[2] < FloatV:
                                    tem_Ary[2] = round(FloatV,tcount.attdot_num)                     
                                setattr(temmonitor_data_model,tcount.attelement_val.lower(),str(round(FloatV,tcount.attdot_num)))
                                setattr(temmonitor_data_model,tcount.attelement_val.lower()+"_ary",tem_Ary)
                            elif temV != "":
                                calV = 0
                                if tcount.attelement_code == "a01011":
                                    calV = float(temV)*float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "speed_ratio"))
                                else:
                                    calV = float(temV)
                                temIndex[iI2] = temIndex[iI2] + 1
                                FloatV = float(temA)+calV
                                if tem_Ary[0] > calV or tem_Ary[0] == 0:
                                    tem_Ary[0] = round(calV,tcount.attdot_num) 
                                tem_Ary[1] = tem_Ary[1] + round(FloatV,tcount.attdot_num)
                                if tem_Ary[2] < calV:
                                    tem_Ary[2] = round(calV,tcount.attdot_num)
                                setattr(temmonitor_data_model,tcount.attelement_val.lower(),str(round(FloatV,tcount.attdot_num)))
                                setattr(temmonitor_data_model,tcount.attelement_val.lower()+"_ary",tem_Ary)
                            iI2 = iI2 + 1                            
                            temLog = temLog + tcount.attelement_val.lower()+":"+temV+"    "
                        printFlag = "z"  
                        temLog = temLog + "update_time:"+str(temkey.update_time)+"    "
                        temLog = temLog + "m_flag:"+str(temkey.m_flag)+"    \r\n"
                a01011 = 0
                iI2 = 0
                for tcount in A01_Para.ht_t_code_element:
                    temA = getattr(temmonitor_data_model, tcount.attelement_val.lower())
                    if temIndex[iI2] > 0:
                        temA = float(temA) / temIndex[iI2]
                    elif temA != "":
                        temA = float(temA)
                    else:
                        temA = 0
                    if tcount.attelement_code == "a01011": 
                        a01011 = float(temA)
                        break
                    iI2 = iI2 + 1
                a00000 = 0
                for tcount in A01_Para.ht_t_code_element:    
                    if tcount.attelement_code == "a00000":
                        a00000 = a01011 * float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "area_measure"))
                        break
                    elif tcount.attelement_code == "w00000":
                        a00000 = temA
                        break
                iI2 = 0
                for tcount in A01_Para.ht_t_code_element:
                    temA = getattr(temmonitor_data_model, tcount.attelement_val.lower())
                    if temIndex[iI2] > 0:
                        temA = float(temA) / temIndex[iI2]
                    elif temA != "":
                        temA = float(temA)
                    else:
                        temA = 0
                    tem_Ary = getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")  
                    tem_Ary[1] = round(temA,tcount.attdot_num)
                    tem_Ary[4] = tem_Ary[1]
                    tem_Ary[5] = tem_Ary[0]
                    tem_Ary[6] = tem_Ary[1]
                    tem_Ary[7] = tem_Ary[2]
                    if tcount.attelement_code != "a00000" and tcount.attelement_code != "a01011" and tcount.attelement_code != "w00000":                                                #气
                        if Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "station_type") == "2":
                            tem_Ary[9] = round(a00000 * 3.6 * tem_Ary[1],tcount.attdot_num)
                        elif Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "station_type") == "1":
                            tem_Ary[9] = round(a00000 * tem_Ary[1],tcount.attdot_num)
                    elif tcount.attelement_code == "a00000" :
                        tem_Ary[9] = round(a00000 * 3600,tcount.attdot_num)
                    elif tcount.attelement_code == "w00000" :
                        tem_Ary[9] = round(a00000,tcount.attdot_num)
                    setattr(temmonitor_data_model,tcount.attelement_val.lower(),str(round(temA,tcount.attdot_num)))
                    setattr(temmonitor_data_model,tcount.attelement_val.lower()+"_ary",tem_Ary)
                    iI2 = iI2 + 1
            except Exception as e:
                if str(type(self)) == "<class 'type'>":
                    self.debug_in(self,repr(e))#打印异常信息
                else:
                    self.debug_in(repr(e))#打印异常信息
            return temmonitor_data_model
        elif inputType == 3:
            temmonitor_data_model = monitor_data_model()
            try:
                temmonitor_data_model.get_time = inputmonitor_data_model.get_time
                temIndex = [0]*len(A01_Para.ht_t_code_element)
                temLog = ""
                for temkey in inputList:
                    temTime = ""
                    if isinstance(temmonitor_data_model.get_time, datetime.datetime) :
                        temTime = temmonitor_data_model.get_time.strftime("%Y-%m-%d")
                    else:
                        temTime = temmonitor_data_model.get_time[0:10]
                    printFlag = "x"
                    if temkey.get_time[0:10] == temTime :  
                        iI2 = 0
                        temLog = temLog + "main_id:"+str(temkey.main_id)+"    "
                        temLog = temLog + "station_code:"+temkey.station_code+"    "
                        printFlag = "y"  
                        temLog = temLog + "get_time:"+str(temkey.get_time)+"    "
                        for tcount in A01_Para.ht_t_code_element:
                            temV = getattr(temkey, tcount.attelement_val.lower())
                            temA = getattr(temmonitor_data_model, tcount.attelement_val.lower())
                            tem_Ary = getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")                            
                            FloatV = 0
                            if temA == "" and temV != "":
                                temIndex[iI2] = temIndex[iI2] + 1  
                                if tcount.attelement_code == "a01011":
                                    FloatV = float(temV)*float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "speed_ratio"))
                                else:
                                    FloatV = float(temV)
                                if tem_Ary[0] > FloatV or tem_Ary[0] == 0:
                                    tem_Ary[0] = round(FloatV,tcount.attdot_num)
                                tem_Ary[1] = tem_Ary[1] + round(FloatV,tcount.attdot_num)
                                if tem_Ary[2] < FloatV:
                                    tem_Ary[2] = round(FloatV,tcount.attdot_num)                     
                                setattr(temmonitor_data_model,tcount.attelement_val.lower(),str(round(FloatV,tcount.attdot_num)))
                                setattr(temmonitor_data_model,tcount.attelement_val.lower()+"_ary",tem_Ary)
                            elif temV != "":
                                calV = 0
                                if tcount.attelement_code == "a01011":
                                    calV = float(temV)*float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "speed_ratio"))
                                else:
                                    calV = float(temV)
                                temIndex[iI2] = temIndex[iI2] + 1
                                FloatV = float(temA)+calV
                                if tem_Ary[0] > calV or tem_Ary[0] == 0:
                                    tem_Ary[0] = round(calV,tcount.attdot_num) 
                                tem_Ary[1] = tem_Ary[1] + round(FloatV,tcount.attdot_num)
                                if tem_Ary[2] < calV:
                                    tem_Ary[2] = round(calV,tcount.attdot_num)
                                setattr(temmonitor_data_model,tcount.attelement_val.lower(),str(round(FloatV,tcount.attdot_num)))
                                setattr(temmonitor_data_model,tcount.attelement_val.lower()+"_ary",tem_Ary)
                            iI2 = iI2 + 1                            
                            temLog = temLog + tcount.attelement_val.lower()+":"+temV+"    "
                        printFlag = "z"  
                        temLog = temLog + "update_time:"+str(temkey.update_time)+"    "
                        temLog = temLog + "m_flag:"+str(temkey.m_flag)+"    \r\n"
                a01011 = 0
                iI2 = 0
                for tcount in A01_Para.ht_t_code_element:
                    temA = getattr(temmonitor_data_model, tcount.attelement_val.lower())
                    if temIndex[iI2] > 0:
                        temA = float(temA) / temIndex[iI2]
                    elif temA != "":
                        temA = float(temA)
                    else:
                        temA = 0
                    if tcount.attelement_code == "a01011": 
                        a01011 = float(temA)
                        break
                    iI2 = iI2 + 1
                a00000 = 0
                for tcount in A01_Para.ht_t_code_element:    
                    if tcount.attelement_code == "a00000":
                        a00000 = a01011 * float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "area_measure"))
                        break                    
                    elif tcount.attelement_code == "w00000":
                        a00000 = temA
                        break
                iI2 = 0
                for tcount in A01_Para.ht_t_code_element:
                    temA = getattr(temmonitor_data_model, tcount.attelement_val.lower())
                    if temIndex[iI2] > 0:
                        temA = float(temA) / temIndex[iI2]
                    elif temA != "":
                        temA = float(temA)
                    else:
                        temA = 0
                    tem_Ary = getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")  
                    tem_Ary[1] = round(temA,tcount.attdot_num)
                    tem_Ary[4] = tem_Ary[1]
                    tem_Ary[5] = tem_Ary[0]
                    tem_Ary[6] = tem_Ary[1]
                    tem_Ary[7] = tem_Ary[2]
                    if tcount.attelement_code != "a01011" and tcount.attelement_code != "a00000" and tcount.attelement_code != "w00000" :
                        if Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "station_type") == "2":
                            tem_Ary[9] = round(a00000 * 3.6 * 24 * tem_Ary[1],tcount.attdot_num)
                        elif Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "station_type") == "1":
                            tem_Ary[9] = round(a00000 * 24 * tem_Ary[1],tcount.attdot_num)                 
                    elif tcount.attelement_code == "a00000" :
                        tem_Ary[9] = round(a00000 * 3600 * 24,tcount.attdot_num)
                    elif tcount.attelement_code == "w00000" :
                        tem_Ary[9] = round(a00000 * 24,tcount.attdot_num) 
                    setattr(temmonitor_data_model,tcount.attelement_val.lower(),str(round(temA,tcount.attdot_num)))
                    setattr(temmonitor_data_model,tcount.attelement_val.lower()+"_ary",tem_Ary)
                    iI2 = iI2 + 1
            except Exception as e:
                if str(type(self)) == "<class 'type'>":
                    self.debug_in(self,repr(e))#打印异常信息
                else:
                    self.debug_in(repr(e))#打印异常信息
            return temmonitor_data_model
    def get_hour(self,inputmonitor_data_model):
        temdbc2 = Db_Common2()
        tbName = "monitor_data_hour"
        strSql = "select main_id,station_code,get_time,update_time,m_flag"
        for tcount in A01_Para.ht_t_code_element:
            strSql = strSql+","+tcount.attelement_val
        strSql = strSql +" from "+tbName+" where strftime('%Y-%m-%d %H:00:00',get_time) >= '"+Com_Fun.GetDateInput("%Y-%m-%d 00:00:00","%Y-%m-%d %H:%M:%S",inputmonitor_data_model.get_time)+"' and strftime('%Y-%m-%d 00:00:00',get_time) <= '"+Com_Fun.GetDateInput("%Y-%m-%d 23:59:59","%Y-%m-%d %H:%M:%S",inputmonitor_data_model.get_time)+"'"
        temList = []
        temRs = temdbc2.Common_Sql(strSql)
        try:
            for temItem in temRs:
                temmonitor_data_model = monitor_data_model()
                temmonitor_data_model.main_id = Com_Fun.ZeroNull(temItem[0])
                temmonitor_data_model.station_code = Com_Fun.NoNull(temItem[1])
                temmonitor_data_model.get_time = Com_Fun.NoNull(temItem[2])
                temmonitor_data_model.update_time = Com_Fun.NoNull(temItem[3])
                temmonitor_data_model.m_flag = Com_Fun.ZeroNull(temItem[4])
                vk = 1
                for tcount in A01_Para.ht_t_code_element:
                    setattr(temmonitor_data_model, tcount.attelement_val.lower(),Com_Fun.NoNull(temItem[4+vk]))
                    vk = vk + 1
                temList.append(temmonitor_data_model)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+strSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+strSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def get_min(self,inputmonitor_data_model):
        temdbc2 = Db_Common2()
        tbName = "monitor_data_min"+Com_Fun.GetDateInput("%Y%m","%Y-%m-%d %H:%M:%S",inputmonitor_data_model.get_time)
        strSql = "select main_id,station_code,get_time,update_time,m_flag"
        for tcount in A01_Para.ht_t_code_element:
            strSql = strSql+","+tcount.attelement_val
        strSql = strSql +" from "+tbName+" where strftime('%Y-%m-%d %H:%M:00',get_time) >= '"+Com_Fun.GetDateInput("%Y-%m-%d %H:00:00","%Y-%m-%d %H:%M:%S",inputmonitor_data_model.get_time)+"' and strftime('%Y-%m-%d %H:%M:00',get_time) <= '"+Com_Fun.GetDateInput("%Y-%m-%d %H:59:59","%Y-%m-%d %H:%M:%S",inputmonitor_data_model.get_time)+"'"
        temList = []
        temRs = temdbc2.Common_Sql(strSql)
        try:
            for temItem in temRs:
                temmonitor_data_model = monitor_data_model()
                temmonitor_data_model.main_id = Com_Fun.ZeroNull(temItem[0])
                temmonitor_data_model.station_code = Com_Fun.NoNull(temItem[1])
                temmonitor_data_model.get_time = Com_Fun.NoNull(temItem[2])
                temmonitor_data_model.update_time = Com_Fun.NoNull(temItem[3])
                temmonitor_data_model.m_flag = Com_Fun.ZeroNull(temItem[4])
                vk = 1
                for tcount in A01_Para.ht_t_code_element:
                    setattr(temmonitor_data_model, tcount.attelement_val.lower(),Com_Fun.NoNull(temItem[4+vk]))
                    vk = vk + 1
                temList.append(temmonitor_data_model)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+strSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+strSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def get_max_hour(self,inputmonitor_data_model):
        temdbc2 = Db_Common2()
        tbName = "monitor_max_min"+Com_Fun.GetDateInput("%Y%m","%Y-%m-%d %H:%M:%S",inputmonitor_data_model.get_time)
        strSql = "select main_id,station_code,get_time,update_time,m_flag"
        for tcount in A01_Para.ht_t_code_element:
            strSql = strSql+","+tcount.attelement_val
        strSql = strSql +" from "+tbName+" where strftime('%Y-%m-%d %H:00:00',get_time) = '"+Com_Fun.GetDateInput("%Y-%m-%d %H:00:00","%Y-%m-%d %H:%M:%S",inputmonitor_data_model.get_time)+"'"
        temList = []
        temRs = temdbc2.Common_Sql(strSql)
        try:
            for temItem in temRs:
                temmonitor_data_model = monitor_data_model()
                temmonitor_data_model.main_id = Com_Fun.ZeroNull(temItem[0])
                temmonitor_data_model.station_code = Com_Fun.NoNull(temItem[1])
                temmonitor_data_model.get_time = Com_Fun.NoNull(temItem[2])
                temmonitor_data_model.update_time = Com_Fun.NoNull(temItem[3])
                temmonitor_data_model.m_flag = Com_Fun.ZeroNull(temItem[4])
                vk = 1
                for tcount in A01_Para.ht_t_code_element:
                    setattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary",Com_Fun.NoNull(temItem[4+vk]))
                    vk = vk + 1
                temList.append(temmonitor_data_model)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+strSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+strSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def monitor_had(self,iNum):
        temdbc2 = Db_Common2()
        tbName = "monitor_data_min"+Com_Fun.DateTimeAdd(datetime.datetime.now(), "d",iNum).strftime('%Y%m')
        tbName2 = "monitor_flag_min"+Com_Fun.DateTimeAdd(datetime.datetime.now(), "d",iNum).strftime('%Y%m')
        tbName3 = "monitor_max_min"+Com_Fun.DateTimeAdd(datetime.datetime.now(), "d",iNum).strftime('%Y%m')
        strSql = "select count(1) from "+tbName+" limit 1"
        temValue = temdbc2.Common_Sql(strSql)
        if temValue == None:
            temTdc = Com_Fun.GetHashTable(Com_Para.htDb,2)
            if temTdc.attDB_DriverClassName != "org.sqlite.JDBC":
                strSql = "Create  TABLE "+tbName+"("
                strSql = strSql+"MAIN_ID int(11) NOT NULL AUTO_INCREMENT"
                strSql = strSql+",station_code varchar(14) NOT NULL"
                strSql = strSql+",get_time datetime NOT NULL DEFAULT now()"
                strSql = strSql+",val_01 varchar(10)"
                strSql = strSql+",val_02 varchar(10)"
                strSql = strSql+",val_03 varchar(10)"
                strSql = strSql+",val_04 varchar(10)"
                strSql = strSql+",val_05 varchar(10)"
                strSql = strSql+",val_06 varchar(10)"
                strSql = strSql+",val_07 varchar(10)"
                strSql = strSql+",val_08 varchar(10)"
                strSql = strSql+",val_09 varchar(10)"
                strSql = strSql+",val_10 varchar(10)"
                strSql = strSql+",val_11 varchar(10)"
                strSql = strSql+",val_12 varchar(10)"
                strSql = strSql+",val_13 varchar(10)"
                strSql = strSql+",val_14 varchar(10)"
                strSql = strSql+",val_15 varchar(10)"
                strSql = strSql+",val_16 varchar(10)"
                strSql = strSql+",val_17 varchar(10)"
                strSql = strSql+",val_18 varchar(10)"
                strSql = strSql+",val_19 varchar(10)"
                strSql = strSql+",val_20 varchar(10)"
                strSql = strSql+",val_21 varchar(10)"
                strSql = strSql+",val_22 varchar(10)"
                strSql = strSql+",val_23 varchar(10)"
                strSql = strSql+",val_24 varchar(10)"
                strSql = strSql+",val_25 varchar(10)"
                strSql = strSql+",val_26 varchar(10)"
                strSql = strSql+",val_27 varchar(10)"
                strSql = strSql+",val_28 varchar(10)"
                strSql = strSql+",val_29 varchar(10)"
                strSql = strSql+",val_30 varchar(10)"
                strSql = strSql+",val_31 varchar(10)"
                strSql = strSql+",val_32 varchar(10)"
                strSql = strSql+",val_33 varchar(10)"
                strSql = strSql+",val_34 varchar(10)"
                strSql = strSql+",val_35 varchar(10)"
                strSql = strSql+",val_36 varchar(10)"
                strSql = strSql+",val_37 varchar(10)"
                strSql = strSql+",val_38 varchar(10)"
                strSql = strSql+",val_39 varchar(10)"
                strSql = strSql+",val_40 varchar(10)"
                strSql = strSql+",PRIMARY KEY (MAIN_ID)"
                strSql = strSql+",UNIQUE KEY MAIN_ID (MAIN_ID)"
                strSql = strSql+");" 
            else:
                strSql = "Create  TABLE "+tbName+"("
                strSql = strSql+"[main_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"
                strSql = strSql+",[station_code] varchar(14) NOT NULL"
                strSql = strSql+",[get_time] datetime NOT NULL DEFAULT (datetime('now'))"
                strSql = strSql+",[val_01] varchar(10)"
                strSql = strSql+",[val_02] varchar(10)"
                strSql = strSql+",[val_03] varchar(10)"
                strSql = strSql+",[val_04] varchar(10)"
                strSql = strSql+",[val_05] varchar(10)"
                strSql = strSql+",[val_06] varchar(10)"
                strSql = strSql+",[val_07] varchar(10)"
                strSql = strSql+",[val_08] varchar(10)"
                strSql = strSql+",[val_09] varchar(10)"
                strSql = strSql+",[val_10] varchar(10)"
                strSql = strSql+",[val_11] varchar(10)"
                strSql = strSql+",[val_12] varchar(10)"
                strSql = strSql+",[val_13] varchar(10)"
                strSql = strSql+",[val_14] varchar(10)"
                strSql = strSql+",[val_15] varchar(10)"
                strSql = strSql+",[val_16] varchar(10)"
                strSql = strSql+",[val_17] varchar(10)"
                strSql = strSql+",[val_18] varchar(10)"
                strSql = strSql+",[val_19] varchar(10)"
                strSql = strSql+",[val_20] varchar(10)"
                strSql = strSql+",[val_21] varchar(10)"
                strSql = strSql+",[val_22] varchar(10)"
                strSql = strSql+",[val_23] varchar(10)"
                strSql = strSql+",[val_24] varchar(10)"
                strSql = strSql+",[val_25] varchar(10)"
                strSql = strSql+",[val_26] varchar(10)"
                strSql = strSql+",[val_27] varchar(10)"
                strSql = strSql+",[val_28] varchar(10)"
                strSql = strSql+",[val_29] varchar(10)"
                strSql = strSql+",[val_30] varchar(10)"
                strSql = strSql+",[val_31] varchar(10)"
                strSql = strSql+",[val_32] varchar(10)"
                strSql = strSql+",[val_33] varchar(10)"
                strSql = strSql+",[val_34] varchar(10)"
                strSql = strSql+",[val_35] varchar(10)"
                strSql = strSql+",[val_36] varchar(10)"
                strSql = strSql+",[val_37] varchar(10)"
                strSql = strSql+",[val_38] varchar(10)"
                strSql = strSql+",[val_39] varchar(10)"
                strSql = strSql+",[val_40] varchar(10)"
                strSql = strSql+",[update_time] datetime DEFAULT (datetime('now'))"
                strSql = strSql+",[m_flag] int"
                strSql = strSql+",[m_flag1] int"
                strSql = strSql+",[m_flag2] int"
                strSql = strSql+",[m_flag3] int"
                strSql = strSql+",[m_flag4] int"
                strSql = strSql+",[m_flag5] int"
                strSql = strSql+",[m_flag6] int"
                strSql = strSql+",[m_flag7] int"
                strSql = strSql+",[m_flag8] int"
                strSql = strSql+",[m_flag9] int"
                strSql = strSql+",[m_flag10] int"
                strSql = strSql+");"
            temdbc2.CommonExec_Sql(strSql)
            if temTdc.attDB_DriverClassName != "org.sqlite.JDBC":
                strSql = "Create  TABLE "+tbName2+"("
                strSql = strSql+"MAIN_ID int(11) NOT NULL AUTO_INCREMENT"
                strSql = strSql+",station_code varchar(14) NOT NULL"
                strSql = strSql+",get_time datetime NOT NULL DEFAULT now()"
                strSql = strSql+",val_01 varchar(10)"
                strSql = strSql+",val_02 varchar(10)"
                strSql = strSql+",val_03 varchar(10)"
                strSql = strSql+",val_04 varchar(10)"
                strSql = strSql+",val_05 varchar(10)"
                strSql = strSql+",val_06 varchar(10)"
                strSql = strSql+",val_07 varchar(10)"
                strSql = strSql+",val_08 varchar(10)"
                strSql = strSql+",val_09 varchar(10)"
                strSql = strSql+",val_10 varchar(10)"
                strSql = strSql+",val_11 varchar(10)"
                strSql = strSql+",val_12 varchar(10)"
                strSql = strSql+",val_13 varchar(10)"
                strSql = strSql+",val_14 varchar(10)"
                strSql = strSql+",val_15 varchar(10)"
                strSql = strSql+",val_16 varchar(10)"
                strSql = strSql+",val_17 varchar(10)"
                strSql = strSql+",val_18 varchar(10)"
                strSql = strSql+",val_19 varchar(10)"
                strSql = strSql+",val_20 varchar(10)"
                strSql = strSql+",val_21 varchar(10)"
                strSql = strSql+",val_22 varchar(10)"
                strSql = strSql+",val_23 varchar(10)"
                strSql = strSql+",val_24 varchar(10)"
                strSql = strSql+",val_25 varchar(10)"
                strSql = strSql+",val_26 varchar(10)"
                strSql = strSql+",val_27 varchar(10)"
                strSql = strSql+",val_28 varchar(10)"
                strSql = strSql+",val_29 varchar(10)"
                strSql = strSql+",val_30 varchar(10)"
                strSql = strSql+",val_31 varchar(10)"
                strSql = strSql+",val_32 varchar(10)"
                strSql = strSql+",val_33 varchar(10)"
                strSql = strSql+",val_34 varchar(10)"
                strSql = strSql+",val_35 varchar(10)"
                strSql = strSql+",val_36 varchar(10)"
                strSql = strSql+",val_37 varchar(10)"
                strSql = strSql+",val_38 varchar(10)"
                strSql = strSql+",val_39 varchar(10)"
                strSql = strSql+",val_40 varchar(10)"
                strSql = strSql+",PRIMARY KEY (MAIN_ID)"
                strSql = strSql+",UNIQUE KEY MAIN_ID (MAIN_ID)"
                strSql = strSql+");" 
            else:
                strSql = "Create  TABLE "+tbName2+"("
                strSql = strSql+"[main_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"
                strSql = strSql+",[station_code] varchar(14) NOT NULL"
                strSql = strSql+",[get_time] datetime NOT NULL DEFAULT (datetime('now'))"
                strSql = strSql+",[val_01] varchar(10)"
                strSql = strSql+",[val_02] varchar(10)"
                strSql = strSql+",[val_03] varchar(10)"
                strSql = strSql+",[val_04] varchar(10)"
                strSql = strSql+",[val_05] varchar(10)"
                strSql = strSql+",[val_06] varchar(10)"
                strSql = strSql+",[val_07] varchar(10)"
                strSql = strSql+",[val_08] varchar(10)"
                strSql = strSql+",[val_09] varchar(10)"
                strSql = strSql+",[val_10] varchar(10)"
                strSql = strSql+",[val_11] varchar(10)"
                strSql = strSql+",[val_12] varchar(10)"
                strSql = strSql+",[val_13] varchar(10)"
                strSql = strSql+",[val_14] varchar(10)"
                strSql = strSql+",[val_15] varchar(10)"
                strSql = strSql+",[val_16] varchar(10)"
                strSql = strSql+",[val_17] varchar(10)"
                strSql = strSql+",[val_18] varchar(10)"
                strSql = strSql+",[val_19] varchar(10)"
                strSql = strSql+",[val_20] varchar(10)"
                strSql = strSql+",[val_21] varchar(10)"
                strSql = strSql+",[val_22] varchar(10)"
                strSql = strSql+",[val_23] varchar(10)"
                strSql = strSql+",[val_24] varchar(10)"
                strSql = strSql+",[val_25] varchar(10)"
                strSql = strSql+",[val_26] varchar(10)"
                strSql = strSql+",[val_27] varchar(10)"
                strSql = strSql+",[val_28] varchar(10)"
                strSql = strSql+",[val_29] varchar(10)"
                strSql = strSql+",[val_30] varchar(10)"
                strSql = strSql+",[val_31] varchar(10)"
                strSql = strSql+",[val_32] varchar(10)"
                strSql = strSql+",[val_33] varchar(10)"
                strSql = strSql+",[val_34] varchar(10)"
                strSql = strSql+",[val_35] varchar(10)"
                strSql = strSql+",[val_36] varchar(10)"
                strSql = strSql+",[val_37] varchar(10)"
                strSql = strSql+",[val_38] varchar(10)"
                strSql = strSql+",[val_39] varchar(10)"
                strSql = strSql+",[val_40] varchar(10)"
                strSql = strSql+",[update_time] datetime DEFAULT (datetime('now'))"
                strSql = strSql+",[m_flag] int"
                strSql = strSql+",[m_flag1] int"
                strSql = strSql+",[m_flag2] int"
                strSql = strSql+",[m_flag3] int"
                strSql = strSql+",[m_flag4] int"
                strSql = strSql+",[m_flag5] int"
                strSql = strSql+",[m_flag6] int"
                strSql = strSql+",[m_flag7] int"
                strSql = strSql+",[m_flag8] int"
                strSql = strSql+",[m_flag9] int"
                strSql = strSql+",[m_flag10] int"
                strSql = strSql+");"
            temdbc2.CommonExec_Sql(strSql)
            if temTdc.attDB_DriverClassName != "org.sqlite.JDBC":
                strSql = "Create  TABLE "+tbName3+"("
                strSql = strSql+"MAIN_ID int(11) NOT NULL AUTO_INCREMENT"
                strSql = strSql+",station_code varchar(14) NOT NULL"
                strSql = strSql+",get_time datetime NOT NULL DEFAULT now()"
                strSql = strSql+",val_01 varchar(10)"
                strSql = strSql+",val_02 varchar(10)"
                strSql = strSql+",val_03 varchar(10)"
                strSql = strSql+",val_04 varchar(10)"
                strSql = strSql+",val_05 varchar(10)"
                strSql = strSql+",val_06 varchar(10)"
                strSql = strSql+",val_07 varchar(10)"
                strSql = strSql+",val_08 varchar(10)"
                strSql = strSql+",val_09 varchar(10)"
                strSql = strSql+",val_10 varchar(10)"
                strSql = strSql+",val_11 varchar(10)"
                strSql = strSql+",val_12 varchar(10)"
                strSql = strSql+",val_13 varchar(10)"
                strSql = strSql+",val_14 varchar(10)"
                strSql = strSql+",val_15 varchar(10)"
                strSql = strSql+",val_16 varchar(10)"
                strSql = strSql+",val_17 varchar(10)"
                strSql = strSql+",val_18 varchar(10)"
                strSql = strSql+",val_19 varchar(10)"
                strSql = strSql+",val_20 varchar(10)"
                strSql = strSql+",val_21 varchar(10)"
                strSql = strSql+",val_22 varchar(10)"
                strSql = strSql+",val_23 varchar(10)"
                strSql = strSql+",val_24 varchar(10)"
                strSql = strSql+",val_25 varchar(10)"
                strSql = strSql+",val_26 varchar(10)"
                strSql = strSql+",val_27 varchar(10)"
                strSql = strSql+",val_28 varchar(10)"
                strSql = strSql+",val_29 varchar(10)"
                strSql = strSql+",val_30 varchar(10)"
                strSql = strSql+",val_31 varchar(10)"
                strSql = strSql+",val_32 varchar(10)"
                strSql = strSql+",val_33 varchar(10)"
                strSql = strSql+",val_34 varchar(10)"
                strSql = strSql+",val_35 varchar(10)"
                strSql = strSql+",val_36 varchar(10)"
                strSql = strSql+",val_37 varchar(10)"
                strSql = strSql+",val_38 varchar(10)"
                strSql = strSql+",val_39 varchar(10)"
                strSql = strSql+",val_40 varchar(10)"
                strSql = strSql+",PRIMARY KEY (MAIN_ID)"
                strSql = strSql+",UNIQUE KEY MAIN_ID (MAIN_ID)"
                strSql = strSql+");" 
            else:
                strSql = "Create  TABLE "+tbName3+"("
                strSql = strSql+"[main_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"
                strSql = strSql+",[station_code] varchar(14) NOT NULL"
                strSql = strSql+",[get_time] datetime NOT NULL DEFAULT (datetime('now'))"
                strSql = strSql+",[val_01] varchar(80)"
                strSql = strSql+",[val_02] varchar(80)"
                strSql = strSql+",[val_03] varchar(80)"
                strSql = strSql+",[val_04] varchar(80)"
                strSql = strSql+",[val_05] varchar(80)"
                strSql = strSql+",[val_06] varchar(80)"
                strSql = strSql+",[val_07] varchar(80)"
                strSql = strSql+",[val_08] varchar(80)"
                strSql = strSql+",[val_09] varchar(80)"
                strSql = strSql+",[val_10] varchar(80)"
                strSql = strSql+",[val_11] varchar(80)"
                strSql = strSql+",[val_12] varchar(80)"
                strSql = strSql+",[val_13] varchar(80)"
                strSql = strSql+",[val_14] varchar(80)"
                strSql = strSql+",[val_15] varchar(80)"
                strSql = strSql+",[val_16] varchar(80)"
                strSql = strSql+",[val_17] varchar(80)"
                strSql = strSql+",[val_18] varchar(80)"
                strSql = strSql+",[val_19] varchar(80)"
                strSql = strSql+",[val_20] varchar(80)"
                strSql = strSql+",[val_21] varchar(80)"
                strSql = strSql+",[val_22] varchar(80)"
                strSql = strSql+",[val_23] varchar(80)"
                strSql = strSql+",[val_24] varchar(80)"
                strSql = strSql+",[val_25] varchar(80)"
                strSql = strSql+",[val_26] varchar(80)"
                strSql = strSql+",[val_27] varchar(80)"
                strSql = strSql+",[val_28] varchar(80)"
                strSql = strSql+",[val_29] varchar(80)"
                strSql = strSql+",[val_30] varchar(80)"
                strSql = strSql+",[val_31] varchar(80)"
                strSql = strSql+",[val_32] varchar(80)"
                strSql = strSql+",[val_33] varchar(80)"
                strSql = strSql+",[val_34] varchar(80)"
                strSql = strSql+",[val_35] varchar(80)"
                strSql = strSql+",[val_36] varchar(80)"
                strSql = strSql+",[val_37] varchar(80)"
                strSql = strSql+",[val_38] varchar(80)"
                strSql = strSql+",[val_39] varchar(80)"
                strSql = strSql+",[val_40] varchar(80)"
                strSql = strSql+",[update_time] datetime DEFAULT (datetime('now'))"
                strSql = strSql+",[m_flag] int"
                strSql = strSql+",[m_flag1] int"
                strSql = strSql+",[m_flag2] int"
                strSql = strSql+",[m_flag3] int"
                strSql = strSql+",[m_flag4] int"
                strSql = strSql+",[m_flag5] int"
                strSql = strSql+",[m_flag6] int"
                strSql = strSql+",[m_flag7] int"
                strSql = strSql+",[m_flag8] int"
                strSql = strSql+",[m_flag9] int"
                strSql = strSql+",[m_flag10] int"
                strSql = strSql+");"
            temdbc2.CommonExec_Sql(strSql)
        if iNum == 30:
            return 0
    def Get_v_server_protocol(self):
        temStrSql = "select * from v_server_protocol where s_state = '1'"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temv_server_protocol = v_server_protocol()
                temv_server_protocol.attserver_ip = Com_Fun.NoNull(temItem[0])
                temv_server_protocol.attserver_port = Com_Fun.NoNull(temItem[1])
                temv_server_protocol.attprot_name = Com_Fun.NoNull(temItem[2])
                temv_server_protocol.attcn_name = Com_Fun.NoNull(temItem[3])
                temv_server_protocol.atts_state = Com_Fun.ZeroNull(temItem[4])
                temv_server_protocol.atts_desc = Com_Fun.ZeroNull(temItem[5])
                temList.append(temv_server_protocol)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def Get_t_param_value(self):
        temStrSql = "select * from t_param_value"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        try:
            for temItem in temRs:
                Com_Fun.SetHashTable(A01_Para.ht_t_param_value, Com_Fun.NoNull(temItem[1]), Com_Fun.NoNull(temItem[2]))
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
    def Get_v_prot_element(self):
        temStrSql = "select * from v_prot_element where element_type = "+Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "station_type")
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temv_prot_element = v_prot_element()
                vTime = ""
                temv_prot_element.attprot_id = Com_Fun.ZeroNull(temItem[0])
                temv_prot_element.attprot_name = Com_Fun.NoNull(temItem[1])
                temv_prot_element.attmain_id = Com_Fun.ZeroNull(temItem[2])
                temv_prot_element.attelement_code = Com_Fun.NoNull(temItem[3])
                temv_prot_element.attelement_name = Com_Fun.NoNull(temItem[4])
                temv_prot_element.attelement_val = Com_Fun.NoNull(temItem[5])
                temv_prot_element.attelement_type = Com_Fun.ZeroNull(temItem[6])
                temv_prot_element.attelement_unit = Com_Fun.NoNull(temItem[7])
                temv_prot_element.attelement_state = Com_Fun.ZeroNull(temItem[8])
                if len(temItem[9]) == 10:
                    vTime = " 00:00:00"
                temv_prot_element.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[9]+vTime)
                temv_prot_element.atts_desc = Com_Fun.NoNull(temItem[10])
                temv_prot_element.attdot_num = Com_Fun.ZeroNull(temItem[11])                                
                temv_prot_element.attup_level = Com_Fun.NoNull(temItem[12])
                temv_prot_element.attdown_level = Com_Fun.NoNull(temItem[13])
                temv_prot_element.attup_limit = Com_Fun.NoNull(temItem[14])
                temv_prot_element.attdown_limit = Com_Fun.NoNull(temItem[15]) 
                temv_prot_element.attelement_code2 = Com_Fun.NoNull(temItem[16])                
                temv_prot_element.attcom_port = Com_Fun.NoNull(temItem[17])             
                temv_prot_element.attprot_up_limit = Com_Fun.NoNull(temItem[18])             
                temv_prot_element.attprot_down_limit = Com_Fun.NoNull(temItem[19])
                temList.append(temv_prot_element)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def Get_t_code_element(self):
        temStrSql = "select * from t_code_element where main_id in (select element_id from t_prot_ele_link a join t_protocol b on a.prot_id = b.main_id where b.s_state = 1) and s_state = 1 and element_type = "+Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "station_type")
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temt_code_element = t_code_element()
                vTime = ""
                temt_code_element.attmain_id = Com_Fun.ZeroNull(temItem[0])
                temt_code_element.attelement_code = Com_Fun.NoNull(temItem[1])
                temt_code_element.attelement_name = Com_Fun.NoNull(temItem[2])
                temt_code_element.attelement_val = Com_Fun.NoNull(temItem[3])
                temt_code_element.attelement_type = Com_Fun.NoNull(temItem[4])
                temt_code_element.attelement_unit = Com_Fun.NoNull(temItem[5])
                temt_code_element.attelement_state = Com_Fun.ZeroNull(temItem[6])
                if len(temItem[7]) == 10:
                    vTime = " 00:00:00"
                temt_code_element.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[7]+vTime)
                temt_code_element.atts_desc = Com_Fun.NoNull(temItem[8])
                temt_code_element.attdot_num = Com_Fun.ZeroNull(temItem[9])                
                temt_code_element.attup_level = Com_Fun.NoNull(temItem[10])
                temt_code_element.attdown_level = Com_Fun.NoNull(temItem[11])
                temt_code_element.attup_limit = Com_Fun.NoNull(temItem[12])
                temt_code_element.attdown_limit = Com_Fun.NoNull(temItem[13])   
                temt_code_element.attelement_code2 = Com_Fun.NoNull(temItem[14])  
                temList.append(temt_code_element)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def Get_t_modbus_addr(self):
        temStrSql = "select a.* from t_modbus_addr a join t_code_element b on a.element_id = b.main_id join t_protocol c on a.protocol_id = c.main_id where b.s_state = 1 and c.s_state = 1"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temt_modbus_addr = t_modbus_addr()
                temt_modbus_addr.attmain_id = Com_Fun.ZeroNull(temItem[0])
                temt_modbus_addr.attmodbus_gateway = Com_Fun.NoNull(temItem[1])
                temt_modbus_addr.attcom_port = Com_Fun.NoNull(temItem[2])
                temt_modbus_addr.attmodbus_addr = Com_Fun.ZeroNull(temItem[3])
                temt_modbus_addr.attmodbus_begin = Com_Fun.ZeroNull(temItem[4])
                temt_modbus_addr.attmodbus_length = Com_Fun.ZeroNull(temItem[5])
                temt_modbus_addr.attelement_id = Com_Fun.ZeroNull(temItem[6])
                temt_modbus_addr.attprotocol_id = Com_Fun.ZeroNull(temItem[7])
                vTime = ""
                if len(temItem[8]) == 10:
                    vTime = " 00:00:00"
                temt_modbus_addr.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[8]+vTime)
                temt_modbus_addr.atts_desc = Com_Fun.NoNull(temItem[9])
                temList.append(temt_modbus_addr)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def Get_t_protocol(self):
        temStrSql = "select * from t_protocol where s_state = 1"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temt_protocol = t_protocol()
                temt_protocol.attmain_id = Com_Fun.ZeroNull(temItem[0])
                temt_protocol.attprot_name = Com_Fun.NoNull(temItem[1])
                temt_protocol.attoem_name = Com_Fun.NoNull(temItem[2])
                vTime = ""
                if len(temItem[3]) == 10:
                    vTime = " 00:00:00"
                temt_protocol.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[3]+vTime)
                temt_protocol.atts_desc = Com_Fun.NoNull(temItem[4])
                temt_protocol.atts_state = Com_Fun.ZeroNull(temItem[5])
                temt_protocol.attcom_port = Com_Fun.NoNull(temItem[6])
                temList.append(temt_protocol)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def Upd_t_param_value(self,inputparam_key,inputparam_value):
        temStrSql = "update t_param_value set param_value = '"+inputparam_value+"' where param_key = '"+inputparam_key+"'"
        temDb2 = Db_Common2()
        temDb2.CommonExec_Sql(temStrSql)
    def Ins_data2(self,inputcolumn_ary,inputi_row,inputdata_type,inputc_type_num):
        try:
            temSqlVlaues = "values('"+Com_Fun.GetHashTable(A01_Para.ht_t_param_value,"station_code")+"'"
            temIns2 = ""
            inputparam_value1 = ""
            iIndex = 0
            for j_cell in inputi_row:
                temCellValue = str(j_cell);
                if iIndex == inputc_type_num:
                    temCellValue = datetime.datetime(*xldate_as_tuple(j_cell,0)).strftime("%Y-%m-%d %H:%M:%S")
                if iIndex == 0:
                    inputparam_value1 = str(temCellValue)
                temIns2 = temIns2+","+inputcolumn_ary[iIndex]+""
                temSqlVlaues = temSqlVlaues +",'"+ temCellValue+"'"
                iIndex = iIndex + 1
            tbName = ""
            if inputdata_type == "1":
                tbName = "monitor_data_min"+Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", inputparam_value1).strftime('%Y%m')
            elif inputdata_type == "2":
                tbName = "monitor_data_hour"
            elif inputdata_type == "3":  
                tbName = "monitor_data_day" 
            temSqlInsert = "insert into "+tbName+"(station_code"+temIns2
            temSqlInsert = temSqlInsert +") "
            temSqlVlaues = temSqlVlaues +") "
            dbc2 = Db_Common2()
            temParameters = []
            temParamTypes = []
            temParamOutName = []
            temParamOutType = []
            temPcl = Power_Control("","","","")
            temPcl.attTpn.attINF_TYPE = "2"
            temPcl.attTpn.attINF_EN_SQL = temSqlInsert + temSqlVlaues
            dbc2.Common_Sql_Proc("Ins_data",temParameters,temParamTypes,temParamOutName,temParamOutType,temPcl.attTpn)
            return True
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temSqlInsert + temSqlVlaues +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temSqlInsert + temSqlVlaues +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
            return False
    def Ins_data(self,inputcolumn_ary,inputi_row,inputdata_type):
        try:
            temSqlVlaues = "values('"+Com_Fun.GetHashTable(A01_Para.ht_t_param_value,"station_code")+"'"
            temIns2 = ""
            inputparam_value1 = ""
            iIndex = 0
            for j_cell in inputi_row:
                if iIndex == 0:
                    inputparam_value1 = str(j_cell.value)
                temIns2 = temIns2+","+inputcolumn_ary[iIndex]+""
                temSqlVlaues = temSqlVlaues +",'"+ str(j_cell.value)+"'"
                iIndex = iIndex + 1
            tbName = ""
            if inputdata_type == "1":
                tbName = "monitor_data_min"+Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", inputparam_value1).strftime('%Y%m')
            elif inputdata_type == "2":
                tbName = "monitor_data_hour"
            elif inputdata_type == "3":  
                tbName = "monitor_data_day" 
            temSqlInsert = "insert into "+tbName+"(station_code"+temIns2
            temSqlInsert = temSqlInsert +") "
            temSqlVlaues = temSqlVlaues +") "
            dbc2 = Db_Common2()
            temParameters = []
            temParamTypes = []
            temParamOutName = []
            temParamOutType = []
            temPcl = Power_Control("","","","")
            temPcl.attTpn.attINF_TYPE = "2"
            temPcl.attTpn.attINF_EN_SQL = temSqlInsert + temSqlVlaues
            dbc2.Common_Sql_Proc("Ins_data",temParameters,temParamTypes,temParamOutName,temParamOutType,temPcl.attTpn)
            return True
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temSqlInsert + temSqlVlaues +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temSqlInsert + temSqlVlaues +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
            return False
    def Qry_data(self,inputparam_name,inputparam_value1,inputparam_value2,inputparam_value3,inputparam_value4,inputparam_value5):
        tbName = ""
        if inputparam_value5 == "1":
            tbName = "monitor_data_min"+Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", inputparam_value1).strftime('%Y%m')
        elif inputparam_value5 == "2":
            tbName = "monitor_data_hour"
        elif inputparam_value5 == "3":  
            tbName = "monitor_data_day" 
        temSql = "select strftime('%Y-%m-%d %H:%M:00',get_time) "
        for temElement in A01_Para.ht_t_code_element:
            temSql = temSql +","+ temElement.attelement_val
        temSql = temSql + " from "+tbName+" where get_time between ? and ? order by get_time asc limit ?,?"
        dbc2 = Db_Common2()
        temParameters = [inputparam_value1,inputparam_value2,inputparam_value3,inputparam_value4]
        temParamTypes = ["STRING","STRING","INT","INT"]
        temParamOutName = []
        temParamOutType = []
        temPcl = Power_Control("","","","")
        temPcl.attTpn.attINF_TYPE = "2"
        temPcl.attTpn.attINF_EN_SQL = temSql
        temRs = dbc2.Common_Sql_Proc(inputparam_name,temParameters,temParamTypes,temParamOutName,temParamOutType,temPcl.attTpn)
        return temRs
