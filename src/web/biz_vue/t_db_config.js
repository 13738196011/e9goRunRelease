//选择某一行
var AF1166_select_t_db_config_rowId = "";
//按钮事件新增或编辑
var AF1166_type = "";
//其他页面传到本页面参数
var AF1166_param = {};

/*定义查询条件变量*/
/*declare query param begin*/

/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

//业务逻辑数据开始
function AF1166_t_db_config_biz_start(inputparam) {
	layer.close(ly_index);
	AF1166_param = inputparam;
    /*biz begin*/
    AF1166_init_t_db_config()	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function AF1166_page_end(){
}

//数据源配置2显示列定义
var AF1166_t_db_config = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '子系统名称',        field: 'DB_CN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '子系统驱动',        field: 'DB_DRIVERCLASSNAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '子系统url',        field: 'DB_URL',        sortable: true        ,formatter: set_s_decode    },    {        title: '子系统登录账号',        field: 'DB_USERNAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '子系统登录密码',        field: 'DB_PASSWORD',        sortable: true        ,formatter: set_s_decode    },    {        title: '子系统版本',        field: 'DB_VERSION',        sortable: true        ,formatter: set_s_decode    },    {        title: '子系统编码',        field: 'DB_CODE',        sortable: true        ,formatter: set_s_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    }	
	/*table column end*/
];

//页面初始化
function AF1166_init_t_db_config() {
	$(window).resize(function () {
		  $('#AF1166_t_db_config_Events').bootstrapTable('resetView');
	});
	//数据源配置2查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AF1166_btn_t_db_config_query').click();
}

//查询接口
function AF1166_t_db_config_query() {
    $('#AF1166_t_db_config_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_db_config",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AF1166_get_N01_sel_t_db_config");
}

//查询结果
function AF1166_get_N01_sel_t_db_config(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_db_config) == false)
        return false;    
	var s_data = input.N01_sel_t_db_config;
    AF1166_select_t_db_config_rowId = "";
    $('#AF1166_t_db_config_Events').bootstrapTable('destroy');
    $("#AF1166_t_db_config_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AF1166_t_db_config_Events').find("tr.changeColor").removeClass('changeColor');
                AF1166_select_t_db_config_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AF1166_t_db_config_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AF1166_select_t_db_config_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AF1166_param.hasOwnProperty("target_name"))
			{
				$("#"+AF1166_param["target_id"]).val(eval("row."+AF1166_param["sourc_id"].toUpperCase()));
				$("#"+AF1166_param["target_name"]).val(s_decode(eval("row."+AF1166_param["sourc_name"].toUpperCase())));				
				layer.close(AF1166_param["ly_index"]);
			}
		},
        toolbar: "#AF1166_t_db_config_Toolbar",
        columns: AF1166_t_db_config,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AF1166_btn_t_db_config_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/

	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/

	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    AF1166_init_t_db_config()	
    /*biz end*/
})

//查询按钮
$('#AF1166_btn_t_db_config_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
	
    /*set query param end*/
	AF1166_t_db_config_query();
})

//vue回调
function AF1166_t_db_config_call_vue(objResult){
	if(index_subhtml == "t_db_config_$.vue")
	{
		var n = Get_RandomDiv("BF1166",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AF1166_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_db_config_$.js","BF1166_t_db_config_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//新增按钮
$("#AF1166_btn_t_db_config_add").click(function () {
	AF1166_type = "add";
	index_subhtml = "t_db_config_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_db_config_$.vue","AF1166_t_db_config_call_vue") == true){
		var n = Get_RandomDiv("BF1166","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BF1166_param["type"] = AF1166_type;
				BF1166_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
				
				/*Send Two FindSelect param end*/

				BF1166_clear_edit_info();
			},
			end: function(){
				BF1166_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AF1166_btn_t_db_config_edit").click(function () {
	if (AF1166_select_t_db_config_rowId != "") {
		AF1166_type = "edit";
		index_subhtml = "t_db_config_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_db_config_$.vue","AF1166_t_db_config_call_vue") == true){
			var n = Get_RandomDiv("BF1166","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BF1166_param["type"] = AF1166_type;
					BF1166_param["ly_index"] = index;
					BF1166_clear_edit_info();
					BF1166_get_edit_info();
				},
				end: function(){
					BF1166_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AF1166_btn_t_db_config_delete').click(function () {
	//单行选择
	var rowData = $("#AF1166_t_db_config_Events").bootstrapTable('getData')[AF1166_select_t_db_config_rowId];
	//多行选择
	var rowDatas = AF1166_sel_row_t_db_config();
	if (AF1166_select_t_db_config_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_db_config",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AF1166_N01_del_t_db_config");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_db_config",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AF1166_N01_del_t_db_config");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AF1166_N01_del_t_db_config(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_db_config) == true)
		AF1166_t_db_config_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function AF1166_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AF1166_sel_row_t_db_config(){
	//获得选中行
	var checkedbox= $("#AF1166_t_db_config_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}

$("#del_truncate").click(function () {
    swal({
        title: "告警",
        text: "是否删除冗余数据",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "确定",
        closeOnConfirm: false
    	},
        function () {
        ly_index = layer.load();
        var inputdata = {
        	"param_name": "T01_truncate_table",
        	"session_id": session_id,
        	"login_id": login_id
        };
        ly_index = layer.load();
        get_ajax_staticurl(inputdata, "get_T01_truncate_table");
    });
});

$("#get_initpage").click(function () {
    swal({
        title: "告警",
        text: "是否读取全局变量信息",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "确定",
        closeOnConfirm: false
    	},
        function () {
        ly_index = layer.load();
        var inputdata = {
        	"param_name": "init_page",
        	"session_id": session_id,
        	"login_id": login_id
        };
        ly_index = layer.load();
        get_ajax_staticurl(inputdata, "get_tem_init_page");
    });
});

//操作结果
function get_tem_init_page(input){
	layer.close(ly_index);
	//查询失败
    if (Call_OpeResult(input.init_page) == false)
        return false;  
}

//操作结果
function get_T01_truncate_table(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_OpeResult(input.T01_truncate_table) == false)
        return false;  
}