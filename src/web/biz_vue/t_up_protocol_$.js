//按钮事件新增或编辑
var BAE647_type = "";
//其他页面传到本页面参数
var BAE647_param = {};
//暂时没用
var BAE647_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BAE647_ary_s_state = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];
/*declare select options end*/

//业务逻辑数据开始
function BAE647_t_up_protocol_biz_start(inputdata) {
	BAE647_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    $.each(BAE647_ary_s_state, function (i, obj) {        addOptionValue("BAE647_s_state", obj[GetLowUpp("main_id")], objGetLowUpp("cn_name")]);    });    BAE647_init_t_up_protocol()	
    /*biz end*/
}

/*biz step begin*/
function BAE647_format_s_state(value, row, index) {    var objResult = value;    for(i = 0; i < BAE647_ary_s_state.length; i++) {        var obj = BAE647_ary_s_state[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function BAE647_page_end(){
	if(BAE647_param["type"] == "edit"){
		BAE647_get_edit_info();
	}
}

//页面初始化方法
function BAE647_init_t_up_protocol() {
	//type = getUrlParam("type");
	if(BAE647_param["type"] == "add"){
		$("#BAE647_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BAE647_cn_name").val(BAE647_param["cn_name"]);		
		/*Get Find Select param end*/
	
	}
	else if(BAE647_param["type"] == "edit"){
	
	}
	
	//表单验证
	BAE647_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BAE647_create_date").val() == "")    {        $("#BAE647_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BAE647_create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BAE647_page_end();
}

//提交表单数据
function BAE647_SubmitForm(){
	if(BAE647_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_up_protocol",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#BAE647_port_name").val())                ,"param_value2": s_encode($("#BAE647_cn_name").val())                ,"param_value3": $("#BAE647_create_date").val()                ,"param_value4": s_encode($("#BAE647_s_desc").val())                ,"param_value5": $("#BAE647_s_state").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BAE647_get_N01_ins_t_up_protocol");
	}
	else if(BAE647_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_up_protocol",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#BAE647_port_name").val())                ,"param_value2": s_encode($("#BAE647_cn_name").val())                ,"param_value3": $("#BAE647_create_date").val()                ,"param_value4": s_encode($("#BAE647_s_desc").val())                ,"param_value5": $("#BAE647_s_state").val()                ,"param_value6": $("#BAE647_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BAE647_get_N01_upd_t_up_protocol");
	}
}

//vue回调
function BAE647_t_up_protocol_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#BAE647_save_t_up_protocol_Edit").click(function () {
	$("form[name='BAE647_DataModal']").submit();
})

/*修改数据*/
function BAE647_get_N01_upd_t_up_protocol(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_up_protocol) == true)
	{
		swal("修改数据成功!", "", "success");
		AAE645_t_up_protocol_query();
		BAE647_clear_validate();
		layer.close(BAE647_param["ly_index"]);
	}
}

/*添加数据*/
function BAE647_get_N01_ins_t_up_protocol(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_up_protocol) == true)
	{
		swal("添加数据成功!", "", "success");
		AAE645_t_up_protocol_query();
		BAE647_clear_validate();
		layer.close(BAE647_param["ly_index"]);
	}
}

//取消编辑
$("#BAE647_cancel_t_up_protocol_Edit").click(function () {
	layer.close(BAE647_param["ly_index"]);
	BAE647_clear_validate();
	$("[id^='BAE647_div']").hide();
})

//清除查找框
function BAE647_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BAE647_clear_validate(){
	$("#BAE647_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BAE647_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BAE647_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BAE647_clear_edit_info(){
	var inputs = $("#BAE647_DataModal").find('input');
	var selects = $("#BAE647_DataModal").find("select");
	var textareas = $("#BAE647_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	BAE647_init_t_up_protocol();
}

//页面输入框赋值
function BAE647_get_edit_info(){
	var rowData = $("#AAE645_t_up_protocol_Events").bootstrapTable('getData')[AAE645_select_t_up_protocol_rowId];
	var inputs = $("#BAE647_DataModal").find('input');
	var selects = $("#BAE647_DataModal").find("select");
	var textareas = $("#BAE647_DataModal").find('textarea');
	$.each(selects, function (i, obj) {
		if(typeof(eval("AAE645_ary_"+obj.id.toString().replace("BAE647_",""))) == "object")
		{
			$.each(eval("AAE645_ary_"+obj.id.toString().replace("BAE647_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("BAE647_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("BAE647_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("AAE645_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#BAE647_DataModal").find('[id="'+"BAE647_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("BAE647_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("BAE647_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//form验证
function BAE647_checkFormInput() {
    BAE647_validate = $("#BAE647_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BAE647_main_id: {}            ,BAE647_port_name: {}            ,BAE647_cn_name: {}            ,BAE647_create_date: {date: true,required : true,maxlength:19}            ,BAE647_s_desc: {}            ,BAE647_s_state: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BAE647_main_id: {}            ,BAE647_port_name: {}            ,BAE647_cn_name: {}            ,BAE647_create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BAE647_s_desc: {}            ,BAE647_s_state: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BAE647_SubmitForm();
        	return false;
        }
    })
}