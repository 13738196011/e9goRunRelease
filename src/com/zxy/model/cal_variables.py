#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class cal_variables(object):
    attMAIN_ID = -1
    attVAR_NAME = ""
    attVAR_KEY = ""
    attVAR_TYPE = ""
    attVAR_DESC = ""
    attCREATE_DATE = None
    attS_DESC = ""
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") 