//按钮事件新增或编辑
var B6FC61_type = "";
//其他页面传到本页面参数
var B6FC61_param = {};
//暂时没用
var B6FC61_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var B6FC61_ary_MAIN_TB_NAME = null;var B6FC61_ary_SUB_TB_NAME = null;
/*declare select options end*/

//主从表传递参数
function B6FC61_param_set(){
	/*Main Subsuv Table Param Begin*/
    if(B6FC61_param.hasOwnProperty("MAIN_TB_NAME_cn_name"))        $("#B6FC61_find_MAIN_TB_NAME_cn_name").val(s_decode(B6FC61_param["MAIN_TB_NAME_cn_name"]));    if(B6FC61_param.hasOwnProperty("MAIN_TB_NAME"))        $("#B6FC61_find_MAIN_TB_NAME").val(B6FC61_param["MAIN_TB_NAME"]);    if(B6FC61_param.hasOwnProperty("hidden_find")){        $("#B6FC61_Ope_MAIN_TB_NAME").hide();        $("#B6FC61_Clear_MAIN_TB_NAME").hide();    }    if(B6FC61_param.hasOwnProperty("SUB_TB_NAME_cn_name"))        $("#B6FC61_find_SUB_TB_NAME_cn_name").val(s_decode(B6FC61_param["SUB_TB_NAME_cn_name"]));    if(B6FC61_param.hasOwnProperty("SUB_TB_NAME"))        $("#B6FC61_find_SUB_TB_NAME").val(B6FC61_param["SUB_TB_NAME"]);    if(B6FC61_param.hasOwnProperty("hidden_find")){        $("#B6FC61_Ope_SUB_TB_NAME").hide();        $("#B6FC61_Clear_SUB_TB_NAME").hide();    }	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function B6FC61_t_main_sub_link_biz_start(inputdata) {
	B6FC61_param = inputdata;
	layer.close(ly_index);
	B6FC61_param_set();
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_main_sub_link$MAIN_TB_NAME",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B6FC61_get_N01_t_main_sub_link$MAIN_TB_NAME");	
    /*biz end*/
}

/*biz step begin*/
function B6FC61_format_MAIN_TB_NAME(value, row, index) {    var objResult = value;    for(i = 0; i < B6FC61_ary_MAIN_TB_NAME.length; i++) {        var obj = B6FC61_ary_MAIN_TB_NAME[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B6FC61_format_SUB_TB_NAME(value, row, index) {    var objResult = value;    for(i = 0; i < B6FC61_ary_SUB_TB_NAME.length; i++) {        var obj = B6FC61_ary_SUB_TB_NAME[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B6FC61_get_N01_t_main_sub_link$MAIN_TB_NAME(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_main_sub_link$MAIN_TB_NAME) == false)        return false;    B6FC61_ary_MAIN_TB_NAME = input.N01_t_main_sub_link$MAIN_TB_NAME;    if($("#B6FC61_MAIN_TB_NAME").is("select") && $("#B6FC61_MAIN_TB_NAME")[0].options.length == 0)    {        $.each(B6FC61_ary_MAIN_TB_NAME, function (i, obj) {            addOptionValue("B6FC61_MAIN_TB_NAME", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    var inputdata = {        "param_name": "N01_t_main_sub_link$SUB_TB_NAME",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B6FC61_get_N01_t_main_sub_link$SUB_TB_NAME");}function B6FC61_get_N01_t_main_sub_link$SUB_TB_NAME(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_main_sub_link$SUB_TB_NAME) == false)        return false;    B6FC61_ary_SUB_TB_NAME = input.N01_t_main_sub_link$SUB_TB_NAME;    if($("#B6FC61_SUB_TB_NAME").is("select") && $("#B6FC61_SUB_TB_NAME")[0].options.length == 0)    {        $.each(B6FC61_ary_SUB_TB_NAME, function (i, obj) {            addOptionValue("B6FC61_SUB_TB_NAME", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    B6FC61_init_t_main_sub_link();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function B6FC61_MAIN_TB_NAME_cn_name_fun(){    index_subhtml = "V_VTC001.vue"    random_subhtml = "CC0427";    if(loadHtmlSubVueFun("dev_vue/V_VTC001.vue","B6FC61_t_main_sub_link_call_vue") == true){        var n = Get_RandomDiv("CC0427","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#CC0427_V_VTC001_Events').bootstrapTable('resetView');                CC0427_param["ly_index"] = index;                CC0427_param["target_name"] = "B6FC61_find_MAIN_TB_NAME_cn_name"                CC0427_param["target_id"] = "B6FC61_MAIN_TB_NAME"                CC0427_param["sourc_id"] = "MAIN_ID"                CC0427_param["sourc_name"] = "COLUMN_EN_NAME"            },            end: function(){                $(n).hide();            }        });     }}function B6FC61_SUB_TB_NAME_cn_name_fun(){    index_subhtml = "V_VTC001.vue"    random_subhtml = "CC0427";    if(loadHtmlSubVueFun("dev_vue/V_VTC001.vue","B6FC61_t_main_sub_link_call_vue") == true){        var n = Get_RandomDiv("CC0427","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#CC0427_V_VTC001_Events').bootstrapTable('resetView');                CC0427_param["ly_index"] = index;                CC0427_param["target_name"] = "B6FC61_find_SUB_TB_NAME_cn_name"                CC0427_param["target_id"] = "B6FC61_SUB_TB_NAME"                CC0427_param["sourc_id"] = "MAIN_ID"                CC0427_param["sourc_name"] = "COLUMN_EN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function B6FC61_page_end(){
	if(B6FC61_param["type"] == "edit"){
		B6FC61_get_edit_info();
	}
}

//页面初始化方法
function B6FC61_init_t_main_sub_link() {
	//type = getUrlParam("type");
	if(B6FC61_param["type"] == "add"){
		$("#B6FC61_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#B6FC61_MAIN_TB_NAME").val(B6FC61_param["MAIN_TB_NAME"]);        $("#B6FC61_find_MAIN_TB_NAME_cn_name").val(B6FC61_param["MAIN_TB_NAME_cn_name"]);        $("#B6FC61_SUB_TB_NAME").val(B6FC61_param["SUB_TB_NAME"]);        $("#B6FC61_find_SUB_TB_NAME_cn_name").val(B6FC61_param["SUB_TB_NAME_cn_name"]);		
		/*Get Find Select param end*/	
	}
	else if(B6FC61_param["type"] == "edit"){
	
	}
	
	//表单验证
	B6FC61_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
	
    /*form datetime init end*/
	
	B6FC61_page_end();
}

//提交表单数据
function B6FC61_SubmitForm(){
	if(B6FC61_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_main_sub_link",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#B6FC61_MAIN_TB_NAME").val())                ,"param_value2": s_encode($("#B6FC61_SUB_TB_NAME").val())                ,"param_value3": s_encode($("#B6FC61_MAIN_TITLE_COLUMN").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B6FC61_get_N01_ins_t_main_sub_link");
	}
	else if(B6FC61_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_main_sub_link",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#B6FC61_MAIN_TB_NAME").val())                ,"param_value2": s_encode($("#B6FC61_SUB_TB_NAME").val())                ,"param_value3": s_encode($("#B6FC61_MAIN_TITLE_COLUMN").val())                ,"param_value4": $("#B6FC61_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B6FC61_get_N01_upd_t_main_sub_link");
	}
}

//vue回调
function B6FC61_t_main_sub_link_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "V_VTC001.vue"){        var n = Get_RandomDiv("CC0427",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":B6FC61_type,                    "ly_index":index,                    "target_name":"B6FC61_find_MAIN_TB_NAME_cn_name",                    "target_id":"B6FC61_find_MAIN_TB_NAME",                    "sourc_id":"MAIN_ID",                    "sourc_name":"COLUMN_EN_NAME"                };                loadScript_hasparam("dev_vue/V_VTC001.js","CC0427_V_VTC001_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }    else if(index_subhtml == "V_VTC001.vue"){        var n = Get_RandomDiv("CC0427",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":B6FC61_type,                    "ly_index":index,                    "target_name":"B6FC61_find_SUB_TB_NAME_cn_name",                    "target_id":"B6FC61_find_SUB_TB_NAME",                    "sourc_id":"MAIN_ID",                    "sourc_name":"COLUMN_EN_NAME"                };                loadScript_hasparam("dev_vue/V_VTC001.js","CC0427_V_VTC001_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//for表单提交
$("#B6FC61_save_t_main_sub_link_Edit").click(function () {
	$("form[name='B6FC61_DataModal']").submit();
})

/*修改数据*/
function B6FC61_get_N01_upd_t_main_sub_link(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_main_sub_link) == true)
	{
		swal("修改数据成功!", "", "success");
		A6FC61_t_main_sub_link_query();
		B6FC61_clear_validate();
		layer.close(B6FC61_param["ly_index"]);
	}
}

/*添加数据*/
function B6FC61_get_N01_ins_t_main_sub_link(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_main_sub_link) == true)
	{
		swal("添加数据成功!", "", "success");
		A6FC61_t_main_sub_link_query();
		B6FC61_clear_validate();
		layer.close(B6FC61_param["ly_index"]);
	}
}

//取消编辑
$("#B6FC61_cancel_t_main_sub_link_Edit").click(function () {
	layer.close(B6FC61_param["ly_index"]);
	B6FC61_clear_validate();
	$("[id^='B6FC61_div']").hide();
})

//清除查找框
function B6FC61_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B6FC61_clear_validate(){
	$("#B6FC61_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B6FC61_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B6FC61_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B6FC61_clear_edit_info(){
	var inputs = $("#B6FC61_DataModal").find('input');
	var selects = $("#B6FC61_DataModal").find("select");
	var textareas = $("#B6FC61_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
    B6FC61_clear_input_cn_name('B6FC61_find_MAIN_TB_NAME_cn_name','B6FC61_MAIN_TB_NAME')    B6FC61_clear_input_cn_name('B6FC61_find_SUB_TB_NAME_cn_name','B6FC61_SUB_TB_NAME')	
	/*input validate clear end*/
	B6FC61_init_t_main_sub_link();
}

//页面输入框赋值
function B6FC61_get_edit_info(){
	var rowData = $("#A6FC61_t_main_sub_link_Events").bootstrapTable('getData')[A6FC61_select_t_main_sub_link_rowId];
	var inputs = $("#B6FC61_DataModal").find('input');
	var selects = $("#B6FC61_DataModal").find("select");
	var textareas = $("#B6FC61_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"A6FC61","B6FC61");
}

//form验证
function B6FC61_checkFormInput() {
    B6FC61_validate = $("#B6FC61_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B6FC61_main_id: {}            ,B6FC61_MAIN_TB_NAME: {}            ,B6FC61_SUB_TB_NAME: {}            ,B6FC61_MAIN_TITLE_COLUMN: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B6FC61_main_id: {}            ,B6FC61_MAIN_TB_NAME: {}            ,B6FC61_SUB_TB_NAME: {}            ,B6FC61_MAIN_TITLE_COLUMN: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B6FC61_SubmitForm();
        	return false;
        }
    })
}