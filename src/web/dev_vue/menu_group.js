//主从表tab选项卡标志位
var A35691_Tab_Flag = -1;
//选择某一行
var A35691_select_menu_group_rowId = "";
//按钮事件新增或编辑
var A35691_type = "";
//其他页面传到本页面参数
var A35691_param = {};
//table选中数据，如无则默认第一条
var A35691_rowCheckData = null;

/*定义查询条件变量*/
/*declare query param begin*/

/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var A35691_ary_IS_AVA = [{'MAIN_ID': '1', 'CN_NAME': '启用'}, {'MAIN_ID': '0', 'CN_NAME': '禁用'}];var A35691_ary_ROLE_ID = [{'MAIN_ID': '1', 'CN_NAME': '开发者'}, {'MAIN_ID': '2', 'CN_NAME': '系统管理员'}, {'MAIN_ID': '3', 'CN_NAME': '运维人员'}, {'MAIN_ID': '4', 'CN_NAME': '普通用户'}];
/*declare select options end*/

/*绑定show监听事件*/
if(A35691_Tab_Flag == "-1"){
	var n = Get_RandomDiv("A35691","");
	$(n).bind("show", function(objTag){
		A35691_Adjust_Sub_Sequ();
		if(A35691_Tab_Flag > 0)
			A35691_adjust_tab();		
	});
}

//设置主从表div顺序
function A35691_Adjust_Sub_Sequ(){
	var temSubDivs = $("#content-main").find("div[data-id]");
	var MainDiv = $(Get_RandomDiv("A35691",""));
	for(i = 0; i < temSubDivs.length; i++) {
		if(A35691_Is_Sub_Div(temSubDivs[i].id)){
			$(temSubDivs[i]).before($(MainDiv));
			break;
		}
	}
}

//主从表传递参数
function A35691_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function A35691_menu_group_biz_start(inputparam) {
	layer.close(ly_index);
	A35691_param = inputparam;
	//主从表传递参数
	A35691_param_set();	
    /*biz begin*/
    if($("#A35691_qry_IS_AVA").is("select") && $("#A35691_qry_IS_AVA")[0].options.length == 0)    {        $("#A35691_qry_IS_AVA").append("<option value='-1'></option>")        $.each(A35691_ary_IS_AVA, function (i, obj) {            addOptionValue("A35691_qry_IS_AVA", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#A35691_qry_ROLE_ID").is("select") && $("#A35691_qry_ROLE_ID")[0].options.length == 0)    {        $("#A35691_qry_ROLE_ID").append("<option value='-1'></option>")        $.each(A35691_ary_ROLE_ID, function (i, obj) {            addOptionValue("A35691_qry_ROLE_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    A35691_init_menu_group()	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function A35691_format_IS_AVA(value, row, index) {    var objResult = value;    for(i = 0; i < A35691_ary_IS_AVA.length; i++) {        var obj = A35691_ary_IS_AVA[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A35691_format_ROLE_ID(value, row, index) {    var objResult = "";    for(i = 0; i < A35691_ary_ROLE_ID.length; i++) {        var obj = A35691_ary_ROLE_ID[i];
        var value_s = s_decode(value.toString()).split(",");
        for(j = 0; j < value_s.length; j ++){
	        if (obj[GetLowUpp("MAIN_ID")].toString() == value_s[j].toString()) {
	        	if(objResult == "")
	        		objResult += obj[GetLowUpp("CN_NAME")];
	        	else
	        		objResult += ","+obj[GetLowUpp("CN_NAME")];
	        }
		}    }    return objResult;}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function A35691_page_end(){
	A35691_adjust_tab();
}

//菜单组显示列定义
var A35691_menu_group = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '菜单组英文名',        field: 'GROUP_EN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '菜单组中文名',        field: 'GROUP_CN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '菜单组图标',        field: 'GROUP_ICON',        sortable: true        ,formatter: set_s_decode    },    {        title: '是否启用',        field: 'IS_AVA',        sortable: true        ,formatter: A35691_format_IS_AVA    },    {        title: '角色',        field: 'ROLE_ID',        sortable: true        ,formatter: A35691_format_ROLE_ID    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function A35691_init_menu_group() {
	$(window).resize(function () {
		  $('#A35691_menu_group_Events').bootstrapTable('resetView');
	});
	//菜单组查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#A35691_btn_menu_group_query').click();
}

//查询接口
function A35691_menu_group_query() {
    $('#A35691_menu_group_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_menu_group",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "A35691_get_N01_sel_menu_group");
}

//查询结果
function A35691_get_N01_sel_menu_group(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_menu_group) == false)
        return false;
    A35691_rowCheckData = null;
    //调整table各列宽度
    $.each(A35691_menu_group, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_menu_group;
    if(s_data.length > 0)
    	A35691_rowCheckData = s_data[0];    
    A35691_select_menu_group_rowId = "";
    A35691_Tab_Flag = 0;
    $('#A35691_menu_group_Events').bootstrapTable('destroy');
    $("#A35691_menu_group_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#A35691_menu_group_Events').find("tr.changeColor").removeClass('changeColor');
                A35691_select_menu_group_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#A35691_menu_group_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                A35691_select_menu_group_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#A35691_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(A35691_param.hasOwnProperty("target_name"))
			{
				$("#"+A35691_param["target_id"]).val(eval("row."+A35691_param["sourc_id"].toUpperCase()));
				$("#"+A35691_param["target_name"]).val(s_decode(eval("row."+A35691_param["sourc_name"].toUpperCase())));				
				layer.close(A35691_param["ly_index"]);
			}
		},
        toolbar: "#A35691_menu_group_Toolbar",
        columns: A35691_menu_group,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        onPostBody:function(){
        	if(s_data.length != 0)
            	A35691_page_end();
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#A35691_btn_menu_group_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/

	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/

	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    if($("#A35691_qry_IS_AVA").is("select") && $("#A35691_qry_IS_AVA")[0].options.length == 0)    {        $("#A35691_qry_IS_AVA").append("<option value='-1'></option>")        $.each(A35691_ary_IS_AVA, function (i, obj) {            addOptionValue("A35691_qry_IS_AVA", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#A35691_qry_ROLE_ID").is("select") && $("#A35691_qry_ROLE_ID")[0].options.length == 0)    {        $("#A35691_qry_ROLE_ID").append("<option value='-1'></option>")        $.each(A35691_ary_ROLE_ID, function (i, obj) {            addOptionValue("A35691_qry_ROLE_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    A35691_init_menu_group()	
    /*biz end*/
})

//查询按钮
$('#A35691_btn_menu_group_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
	
    /*set query param end*/
	A35691_menu_group_query();
})

//vue回调
function A35691_menu_group_call_vue(objResult){
	//选择某条记录或自动选择第一条
	if (A35691_select_menu_group_rowId != "") 
		A35691_rowCheckData = $("#A35691_menu_group_Events").bootstrapTable('getData')[A35691_select_menu_group_rowId];
	
	if(index_subhtml == "menu_group_$.vue")
	{
		var n = Get_RandomDiv("B35691",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":A35691_type,"ly_index":index};
				if(A35691_param.hasOwnProperty("hidden_find")){
					inputdata["hidden_find"] = "1";
				}				
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("dev_vue/menu_group_$.js","B35691_menu_group_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/
    else if(index_subhtml == "menu_info.vue"){        var n = Get_RandomDiv("A77565",objResult);        $(n).show();        //传递参数        var inputdata = {"hidden_find":"1"};        if(A35691_rowCheckData != null){            inputdata["GROUP_ID_cn_name"] = A35691_rowCheckData[GetLowUpp("GROUP_CN_NAME")];            inputdata["GROUP_ID"] = A35691_rowCheckData[GetLowUpp("MAIN_ID")];        }        else{            inputdata["GROUP_ID_cn_name"] = "";            inputdata["GROUP_ID"] = "-999";        }        loadScript_hasparam("dev_vue/menu_info.js","A77565_menu_info_biz_start",inputdata);    }
	/*get tab subvue end*/
}

//新增按钮
$("#A35691_btn_menu_group_add").click(function () {
	A35691_type = "add";
	index_subhtml = "menu_group_$.vue";
	if(loadHtmlSubVueFun("dev_vue/menu_group_$.vue","A35691_menu_group_call_vue") == true){
		var n = Get_RandomDiv("B35691","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				B35691_param["type"] = A35691_type;
				B35691_param["ly_index"]= index;
				if(A35691_param.hasOwnProperty("hidden_find")){
					B35691_param["hidden_find"] = "1";
				}
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
				
				/*Send Two FindSelect param end*/

				B35691_clear_edit_info();
			},
			end: function(){
				B35691_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#A35691_btn_menu_group_edit").click(function () {
	if (A35691_select_menu_group_rowId != "") {
		A35691_type = "edit";
		index_subhtml = "menu_group_$.vue";
		if(loadHtmlSubVueFun("dev_vue/menu_group_$.vue","A35691_menu_group_call_vue") == true){
			var n = Get_RandomDiv("B35691","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					B35691_param["type"] = A35691_type;
					B35691_param["ly_index"] = index;
					if(A35691_param.hasOwnProperty("hidden_find")){
						B35691_param["hidden_find"] = "1";
					}
					B35691_clear_edit_info();
					B35691_get_edit_info();
				},
				end: function(){
					B35691_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#A35691_btn_menu_group_delete').click(function () {
	//单行选择
	var rowData = $("#A35691_menu_group_Events").bootstrapTable('getData')[A35691_select_menu_group_rowId];
	//多行选择
	var rowDatas = A35691_sel_row_menu_group();
	if (A35691_select_menu_group_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_menu_group",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "A35691_N01_del_menu_group");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_menu_group",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "A35691_N01_del_menu_group");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function A35691_N01_del_menu_group(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_menu_group) == true)
		A35691_menu_group_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function A35691_adjust_tab(){
	if(typeof($("#A35691_TAB_MAIN")[0]) != "undefined" && $("#A35691_TAB_MAIN")[0].length != 0){
		A35691_Tab_Flag = 1;
		$(Get_RDivNoBuild("A35691","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("A35691","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("A35691","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#A35691_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/
$("#A35691_tab_1").click(function(){    A35691_Tab_Flag = 1;    A35691_hide_tab_fun();    //主子表参数传递    var temPar = [{"sourc_id":"MAIN_ID","target_id":"GROUP_ID"},{"sourc_id":"GROUP_CN_NAME","target_id":"GROUP_ID_cn_name"}];    A35691_show_tab_fun("menu_info.vue","A77565",temPar);});//隐藏tab页选项卡function A35691_hide_tab_fun(){    var n = null;    n = Get_RDivNoBuild("A77565","");    $(n).hide();}//判断是否sub子项div页面function A35691_Is_Sub_Div(temDivId){    if(temDivId.indexOf("XX$TTT") == 0)        return false;    else if(temDivId.indexOf("A77565") == 0)        return true;    return false;}
/*Tab Click Fun End*/

function A35691_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("dev_vue/"+inputUrl,"A35691_menu_group_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (A35691_select_menu_group_rowId != "") 
			rowData = $("#A35691_menu_group_Events").bootstrapTable('getData')[A35691_select_menu_group_rowId];
		else
			rowData = A35691_rowCheckData;
		//$("#A35691_menu_group_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			var temFlag = true;
			$.each(temPar, function (i, obj) {
				if(eval(inputrandom+"_param.hasOwnProperty(\""+obj.target_id+"\")") && eval(inputrandom+"_param[\""+obj.target_id+"\"]").toString() == rowData[obj.sourc_id].toString())
					temFlag = false;
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			if(temFlag){
				//传递子页面隐藏find功能
				eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
				//参数传递并赋值
				eval(inputrandom+"_param_set()");
				var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
				$("#"+inputrandom+"_btn_"+tbName+"_query").click();
			}
		}
		else{
			$.each(temPar, function (i, obj) {
				if(obj.sourc_id.toUpperCase() == "MAIN_ID")
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"-999\"");
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"\"");
			});
			//传递子页面隐藏find功能
			eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
			var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
			$("#"+inputrandom+"_btn_"+tbName+"_query").click();		
		}
	}
}

//清除 查找框
function A35691_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function A35691_sel_row_menu_group(){
	//获得选中行
	var checkedbox= $("#A35691_menu_group_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}