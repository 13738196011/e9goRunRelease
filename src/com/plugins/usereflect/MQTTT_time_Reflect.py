#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.tcp.MqttThread import MqttThread
from com.zxy.z_debug import z_debug
class MQTTT_time_Reflect(z_debug):
    strContinue      = "1"   
    strResult        = ""
    def __init__(self):
        pass
    def init_start(self):
        if Com_Para.mqclient.is_connected() == True:
            for strtopic in Com_Para.MQTT_topic_pull.split(","):
                tempayload = strtopic+"    "+Com_Fun.Get_New_GUID()+"    "+Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")+"    客户端往服务端发送mqtt数据" 
                if str(type(self)) == "<class 'type'>":
                    self.debug_in(self,tempayload)#打印异常信息
                else:
                    self.debug_in(tempayload)#打印异常信息
                temResult = Com_Para.mqclient.publish(strtopic,payload=tempayload,qos=2)
        else:
            try:
                Com_Para.mqclient.connect(Com_Para.MQTT_host.split(":")[0],port=int(Com_Para.MQTT_host.split(":")[1]))
                Com_Para.mqclient.loop_start()
            except Exception as e:
                if str(type(self)) == "<class 'type'>":
                    self.debug_in(self,repr(e))#打印异常信息
                else:
                    self.debug_in(repr(e))#打印异常信息
        pass