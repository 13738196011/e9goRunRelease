#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import time,json,struct,urllib.parse
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_6GG8T3.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_6GG8T3.A01_6GG8T3_MODBUS import A01_6GG8T3_MODBUS
from com.plugins.A01_6GG8T3 import A01_Para
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.common import Com_Para
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.z_debug import z_debug
class A01_6GG8T3_Time(z_debug):
    strContinue      = "1"   
    strResult        = ""
    def __init__(self):
        pass
    def Upd_Data(self):
        pass
    def init_start(self):
        bodc = Bus_Ope_DB_Cent()
        bodc.init_page()    
        temt_station_info = bodc.Get_t_station_info()
        temdbc2 = Db_Common2()
        temA01modbus = A01_6GG8T3_MODBUS()
        for tsi in temt_station_info:
            tbMonth = "monitor_data_min"+Com_Fun.GetTime("%Y%m")
            get_time = Com_Fun.GetTime("%Y-%m-%d %H:%M:00")
            update_time = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
            strSqlInsert = "insert into "
            strSqlInsert2 = "insert into monitor_data_real "
            strSqlUpdate = "update monitor_data_real "
            strSqlValues = "values "
            inputValues = {}
            strSqlInsert = strSqlInsert + tbMonth+"(station_code,get_time,update_time"
            strSqlInsert2 = strSqlInsert2+"(station_code,get_time,update_time"
            strSqlUpdate = strSqlUpdate+"set get_time='"+get_time+"',update_time='"+update_time+"'"
            strSqlValues = strSqlValues+"('"+tsi.attstation_code+"','"+get_time+"','"+Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")+"'"
            inputValues["param_value1"] = tsi.attstation_code
            inputValues["param_value2"] = Com_Fun.py_urlencode(get_time).replace("s_value=","")
            temt_modbus_addr = bodc.Get_t_modbus_addr_by_station_id(str(tsi.attmain_id))
            all_value = ""
            m_flag = 0
            iIndex = 0
            temmodbus_addr = 0
            temmodbus_gateway = 0
            temmodbus_length = 0
            temIndex = 0
            for tmda in temt_modbus_addr:
                if temIndex == 0:
                    temmodbus_addr = tmda.attmodbus_addr
                    temmodbus_gateway = tmda.attmodbus_gateway
                temmodbus_length = temmodbus_length + tmda.attmodbus_length            
                temIndex = temIndex + 1
            red = temA01modbus.get_data_tcp(temmodbus_addr,temmodbus_gateway,temmodbus_length)
            for tmda in temt_modbus_addr:
                data_val = ""                 
                if len(red) <= 2 * iIndex:      
                    data_val = "-1"
                else:         
                    n = (int)(red[2 * iIndex+0])
                    m = (int)(red[2 * iIndex+1])
                    data_val = str(temA01modbus.ReadFloat(n,m,True))
                all_value = all_value + data_val               
                strSqlInsert = strSqlInsert +","+tmda.attelement_val
                strSqlInsert2 = strSqlInsert2 +","+tmda.attelement_val
                strSqlValues = strSqlValues +",'"+data_val+"'"
                strSqlUpdate = strSqlUpdate +","+tmda.attelement_val+"='"+data_val+"'"
                iIndex = iIndex + 1
                inputValues["param_value"+str(iIndex+2)] = data_val
            inputValues["param_value11"] = ""
            inputValues["param_value12"] = ""
            inputValues["param_value13"] = ""
            inputValues["param_value14"] = ""
            inputValues["param_value15"] = ""
            inputValues["param_value16"] = ""
            inputValues["param_value17"] = ""
            inputValues["param_value18"] = ""
            inputValues["param_value19"] = ""
            inputValues["param_value20"] = ""
            inputValues["param_value21"] = ""
            inputValues["param_value22"] = ""
            print("post_data_***begin***:"+Com_Fun.GetTime("%Y-%m-%d %H:%M:%S"))
            inputUrl = "http://" + Com_Para.ServerIP + ":"+ str(Com_Para.ServerWebPort)+ "/CenterData/getdata.jsp?a=1" + A01_Para.strCon
            m_result = Com_Fun.PostHttp(inputUrl, inputValues)
            inputUrl = "http://localhost:8080/CenterData/getdata.jsp?a=1" + A01_Para.strCon
            m_result = Com_Fun.PostHttp(inputUrl, inputValues)
            print("post_data_end*****end:"+Com_Fun.GetTime("%Y-%m-%d %H:%M:%S"))
            print("post_data:"+inputValues["param_value2"])
            if m_result == "-1" or m_result == "":
                m_flag = 0
            else:    
                try:
                    temJso = json.loads(m_result)
                    temJsoary = temJso["C01_T7H5R4"]
                    for temobj in temJsoary:
                        temSResult = temobj["s_result"]
                        if temSResult == 1:
                            m_flag = 1
                except Exception as e:
                    m_flag = 0
                    if str(type(self)) == "<class 'type'>":
                        self.debug_in(self,repr(e)+"==>"+m_result)#打印异常信息
                    else:
                        self.debug_in(repr(e)+"==>"+m_result)#打印异常信息
            if len(all_value) > 0:
                if bodc.Get_monitor_data_real_true(tsi.attstation_code) == True:
                    temdbc2.CommonExec_Sql(strSqlUpdate+" where station_code ='"+tsi.attstation_code+"'")
                else:
                    temdbc2.CommonExec_Sql(strSqlInsert2+",m_flag) "+strSqlValues+",'"+str(m_flag)+"')")
                temdbc2.CommonExec_Sql(strSqlInsert+",m_flag) "+strSqlValues+",'"+str(m_flag)+"')")