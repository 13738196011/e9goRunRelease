#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from Crypto.Cipher import AES
import base64
import re
from com.zxy.common.EncodeUtil import EncodeUtil
class Aes_ECB(object):
    def __init__(self,key):
        self.key = key
        self.MODE = AES.MODE_ECB
        self.BS = AES.block_size
        self.pad = lambda s: s + (self.BS - len(s) % self.BS) * chr(self.BS - len(s) % self.BS)
        self.unpad = lambda s: s[0:-ord(s[-1])]
    def add_to_16(self,value):
        while len(value) % 16 != 0:
            value += '\0'
        return str.encode(value)  # 返回bytes
    def AES_encrypt(self, inputContent):
        eu = EncodeUtil()
        temResult = eu.aesEncrypt_2(inputContent, self.key)
        SM = bytes(temResult).hex().upper()
        return SM
    def AES_decrypt(self, text):
        pass