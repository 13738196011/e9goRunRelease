#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import json
from com.plugins.A01_IHGDW0 import A01_Para
from com.plugins.A01_IHGDW0.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_IHGDW0.monitor_data_model import monitor_data_model
from com.plugins.A01_IHGDW0.A01_Fun import A01_Fun
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.z_debug import z_debug
class A01_H09T12(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = "0"
    def __init__(self):
        pass
    def init_start(self):        
        jso = {}
        jsary1 = []
        jsary2 = []
        jsary3 = []
        temjso1 = {}
        temjso2 = {}
        temjso3 = {}
        temIndex = 0
        temCount = len(A01_Para.ht_monitor_sec)
        if A01_Para.allmonitor_data_min_last is None and temCount > 0:
            A01_Para.allmonitor_data_min_last = monitor_data_model()        
        A01_Para.allmonitor_flag_min_last = A01_Fun.Get_monitor_flag_real()
        for key in list(A01_Para.ht_monitor_sec.keys()): 
            temMDM = A01_Para.ht_monitor_sec[key]
            if temIndex >= temCount - 3:
                A01_Para.allmonitor_data_min_last.get_time = temMDM.get_time
                for tcount in A01_Para.ht_t_code_element:
                    temV = getattr(temMDM, tcount.attelement_val.lower())
                    if str(temV) != "" and str(temV) != "-1":
                        setattr(A01_Para.allmonitor_data_min_last, tcount.attelement_val.lower(),str(temV))
            temIndex = temIndex + 1
        if A01_Para.allmonitor_data_min_last is not None and A01_Para.allmonitor_data_min_last.get_time is not None:
            temjso2[Com_Fun.GetLowUpp("get_time")] = str(A01_Para.allmonitor_data_min_last.get_time)
            temjso3[Com_Fun.GetLowUpp("get_time")] = str(A01_Para.allmonitor_flag_min_last.get_time)
            for tcount in A01_Para.ht_t_code_element:
                temjso1[Com_Fun.GetLowUpp(tcount.attelement_val.lower())] = tcount.attelement_name+"["+tcount.attelement_unit+"]"
                temV = getattr(A01_Para.allmonitor_data_min_last, tcount.attelement_val.lower())
                temjso2[Com_Fun.GetLowUpp(tcount.attelement_val.lower())] = temV
                temV2 = getattr(A01_Para.allmonitor_flag_min_last, tcount.attelement_val.lower())
                temjso3[Com_Fun.GetLowUpp(tcount.attelement_val.lower())] = temV2
            jsary1.append(temjso1)
            jsary2.append(temjso2)
            jsary3.append(temjso3)
            jso[self.param_name+"_TITLE"] = jsary1
            jso[self.param_name+"_DATA"] = jsary2
            jso[self.param_name+"_FLAG"] = jsary3
        else:
            temjso2[Com_Fun.GetLowUpp("get_time")] = "1900-01-01 00:00:00"
            temjso3[Com_Fun.GetLowUpp("get_time")] = "1900-01-01 00:00:00"
            bodc = Bus_Ope_DB_Cent()
            temMonitor_data_model = bodc.get_min_data_last()
            temMonitor_flag_model = bodc.get_min_flag_last()
            if temMonitor_data_model.main_id != -1:                
                temjso2[Com_Fun.GetLowUpp("get_time")] = temMonitor_data_model.get_time
                temjso3[Com_Fun.GetLowUpp("get_time")] = temMonitor_flag_model.get_time
                for tcount in A01_Para.ht_t_code_element:
                    temjso1[Com_Fun.GetLowUpp(tcount.attelement_val.lower())] = tcount.attelement_name+"["+tcount.attelement_unit+"]"
                    temV = getattr(temMonitor_data_model, tcount.attelement_val.lower())
                    temjso2[Com_Fun.GetLowUpp(tcount.attelement_val.lower())] = temV
                    temV2 = getattr(temMonitor_flag_model, tcount.attelement_val.lower())
                    temjso3[Com_Fun.GetLowUpp(tcount.attelement_val.lower())] = temV2
            else:
                for tcount in A01_Para.ht_t_code_element:
                    temjso1[Com_Fun.GetLowUpp(tcount.attelement_val.lower())] = tcount.attelement_name+"["+tcount.attelement_unit+"]"
                    temV = "-"
                    temjso2[Com_Fun.GetLowUpp(tcount.attelement_val.lower())] = temV
                    temV2 = "-"
                    temjso3[Com_Fun.GetLowUpp(tcount.attelement_val.lower())] = temV2
            jsary1.append(temjso1)
            jsary2.append(temjso2)
            jsary3.append(temjso3)
            jso[self.param_name+"_TITLE"] = jsary1
            jso[self.param_name+"_DATA"] = jsary2
            jso[self.param_name+"_FLAG"] = jsary3
        self.strResult = json.dumps(jso,ensure_ascii=False)
        self.strContinue = "0"