#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import threading, json
from com.zxy.z_debug import z_debug
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.business.DelayDataThread import DelayDataThread
from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.zxy.business.Power_Control import Power_Control
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.model.Return_Value import Return_Value
class Query_Data(z_debug):
    attSession_id    = ""
    attReturn_Value  = Return_Value()
    def __init__(self,inputReturn_Value):
        self.attReturn_Value = inputReturn_Value
    def GetDataList(self,inputSub_code, inputSub_usercode, inputParam_name, inputAryParamValue, inputStrUrl, inputDelay_data, inputDelay_code, inputStrIP,inputHtParam):
        temPcl = Power_Control(inputSub_code,inputSub_usercode,inputParam_name,inputStrIP)        
        temOpd = Ope_DB_Cent()
        temResult = ""
        if temPcl.attTss.attMAIN_ID == -1:
            temResult = temPcl.attError
            if  temResult != "":
                self.attReturn_Value.attS_result = 0
                self.attReturn_Value.attErr_desc = temResult
            if len(Com_Para.htServSockets) > 0:
                try:
                    temChild_Node = temOpd.Get_Child_Node(inputParam_name)          
                    if len(Com_Para.htChildNode) == 0:
                        self.attReturn_Value.attS_result = 0
                        self.attReturn_Value.attErr_desc = "{\""+ inputParam_name + "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"子级设备未正常连接01\"}]}"
                        temResult = "{\""+ inputParam_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"子级设备未正常连接01\"}]}"
                        return temResult
                    temSkStr = Com_Fun.GetHashTable(Com_Para.htChildNode,temChild_Node)
                    temSk = Com_Fun(Com_Para.htServSockets,temSkStr)
                    if temSk is not None and not temSk._closed:
                        Com_Fun.SetHashTable(Com_Para.htChildInfo,temChild_Node,"")
                        Com_Fun.Send02(inputStrUrl,temSk)
                        temDtSBegin = Com_Fun.getTimeLong()
                        temDtSEnd = Com_Fun.getTimeLong()
                        temTimes = 0
                        while temTimes <= Com_Para.TimeCount and (Com_Fun.GetHashTable(Com_Para.htChildInfo,temChild_Node) =="" or len(Com_Fun.GetHashTable(Com_Para.htChildInfo,temChild_Node)) == 0):
                            temTimes = temDtSEnd - temDtSBegin
                            temDtSEnd = Com_Fun.getTimeLong()
                        if Com_Fun.GetHashTable(Com_Para.htChildInfo,temChild_Node) == "" or len(Com_Fun.GetHashTable(Com_Para.htChildInfo,temChild_Node)) == 0:
                            temResult = Com_Fun.GetHashTable(Com_Para.htChildInfo,temChild_Node)
                            Com_Fun.SetHashTable(Com_Para.htChildInfo,temChild_Node,"")
                            Com_Fun.SetHashTable(Com_Para.htTempChildInfo,temChild_Node,"")
                        else:
                            self.attReturn_Value.attS_result = 0
                            self.attReturn_Value.attErr_desc = "{\""+ inputParam_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"获取子级设备数据超时\"}]}"
                            temResult = "{\""+ inputParam_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"获取子级设备数据超时\"}]}"
                    else:
                        Com_Fun.RemoveHashTable(Com_Para.htServSockets,temSkStr)
                        self.attReturn_Value.attS_result = 0
                        self.attReturn_Value.attErr_desc = "{\"" + inputParam_name + "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"发送子级设备数据失败:Mod_Data.htChildNode获取失败,socket断开\"}]}"
                        temResult = "{\""+ inputParam_name + "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"发送子级设备数据失败:Mod_Data.htChildNode获取失败,socket断开\"}]}"
                except Exception as e:
                    self.attReturn_Value.attS_result = 0
                    self.attReturn_Value.attErr_desc.append("{\""+ inputParam_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"发送子级设备数据失败:"+ repr(e) + "\"}]}")
                    temResult = "{\""+ inputParam_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"发送子级设备数据失败"+ repr(e) + "\"}]}"
                finally:
                    pass
            else:
                self.attReturn_Value.attS_result = 0
                self.attReturn_Value.attErr_desc ="{\"" + inputParam_name + "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"子级设备未正常连接,或接口不存在\"}]}"
        else:     
            if inputDelay_data == "1"  and  len(inputDelay_code) == 32:
                try:
                    temUL = UsAdmin_Log(Com_Para.ApplicationPath,temResult,0)
                    temResult = temUL.ReadData(inputDelay_code,inputParam_name)
                except Exception as e:
                    self.attReturn_Value.attS_result = 0
                    self.attReturn_Value.attErr_desc ="{\"" + inputParam_name + "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"延时获取数据失败:"+ repr(e)+ "\"}]}"
            elif inputDelay_data == "0"  and  len(inputDelay_code) == 32:
                temDt = DelayDataThread(temPcl,inputSub_code,inputParam_name,inputAryParamValue,inputDelay_code,inputStrIP,self.attSession_id,inputHtParam)
                t2 = threading.Thread(target=temDt.run, name="DelayDataThread")
                t2.start()
                temResult = "{\""+Com_Fun.GetLowUpp("s_result")+"\":\"1\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"请求延时数据成功,请稍候获取数据\"}"
            else:
                temResult = temOpd.Cal_Data(temPcl,inputSub_code,inputParam_name,inputAryParamValue,inputStrIP,self.attSession_id,inputHtParam)
        if inputParam_name == "APP_USER_ZXY":
            temJso = None
            try:
                temJso = json.loads(temResult)
                temJsoary = temJso["APP_USER_ZXY"]
                temStrNextInfName = ""
                for temobj in temJsoary:
                    temStrINFNAME = temobj["INF_EN_NAME"]
                    if temStrINFNAME != temStrNextInfName:
                        temArySubParamValue = []
                        if len(temobj["URL_IN_PARAM"]) > 0:
                            for strtemParam in temobj["URL_IN_PARAM"].split(","):
                                strtemParam = inputAryParamValue[int(strtemParam.replace("param_value","")) - 1]
                                temArySubParamValue.append(strtemParam)
                        pcl = Power_Control(inputSub_code,inputSub_usercode,temStrINFNAME,inputStrIP)
                        temJsonSub = json.loads(temOpd.Cal_Data(pcl,inputSub_code,temStrINFNAME,temArySubParamValue,inputStrIP,self.attSession_id,inputHtParam))
                        temJso[temStrINFNAME] = temJsonSub[temStrINFNAME]
                        temStrNextInfName = temStrINFNAME            
                temResult = json.dumps(temJso,ensure_ascii=False)
            except Exception as e:
                temResult = "{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"读取APP_USER_ZXY接口失败(GetDataList)JSONException"+ repr(e) + "\"}"
        return temResult