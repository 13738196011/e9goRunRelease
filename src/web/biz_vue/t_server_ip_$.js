//按钮事件新增或编辑
var BB2B81_type = "";
//其他页面传到本页面参数
var BB2B81_param = {};
//暂时没用
var BB2B81_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BB2B81_ary_up_protocol_id = null;
/*declare select options end*/

//业务逻辑数据开始
function BB2B81_t_server_ip_biz_start(inputdata) {
	BB2B81_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_server_ip$up_protocol_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BB2B81_get_N01_t_server_ip$up_protocol_id");	
    /*biz end*/
}

/*biz step begin*/
function BB2B81_format_up_protocol_id(value, row, index) {    var objResult = value;    for(i = 0; i < BB2B81_ary_up_protocol_id.length; i++) {        var obj = BB2B81_ary_up_protocol_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BB2B81_get_N01_t_server_ip$up_protocol_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_server_ip$up_protocol_id) == false)        return false;    BB2B81_ary_up_protocol_id = input.N01_t_server_ip$up_protocol_id;    if($("#BB2B81_up_protocol_id").is("select") && $("#BB2B81_up_protocol_id")[0].options.length == 0)    {        $.each(BB2B81_ary_up_protocol_id, function (i, obj) {            addOptionValue("BB2B81_up_protocol_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    BB2B81_init_t_server_ip();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function BB2B81_page_end(){
	if(BB2B81_param["type"] == "edit"){
		BB2B81_get_edit_info();
	}
}

//页面初始化方法
function BB2B81_init_t_server_ip() {
	//type = getUrlParam("type");
	if(BB2B81_param["type"] == "add"){
		$("#BB2B81_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BB2B81_up_protocol_id").val(BB2B81_param["up_protocol_id"]);		
		/*Get Find Select param end*/
	}
	else if(BB2B81_param["type"] == "edit"){
	
	}
	
	//表单验证
	BB2B81_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BB2B81_create_date").val() == "")    {        $("#BB2B81_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BB2B81_create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BB2B81_page_end();
}

//提交表单数据
function BB2B81_SubmitForm(){
	if(BB2B81_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_server_ip",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#BB2B81_server_ip").val())                ,"param_value2": s_encode($("#BB2B81_server_port").val())                ,"param_value3": s_encode($("#BB2B81_server_name").val())                ,"param_value4": $("#BB2B81_create_date").val()                ,"param_value5": s_encode($("#BB2B81_s_desc").val())                ,"param_value6": $("#BB2B81_up_protocol_id").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BB2B81_get_N01_ins_t_server_ip");
	}
	else if(BB2B81_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_server_ip",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#BB2B81_server_ip").val())                ,"param_value2": s_encode($("#BB2B81_server_port").val())                ,"param_value3": s_encode($("#BB2B81_server_name").val())                ,"param_value4": $("#BB2B81_create_date").val()                ,"param_value5": s_encode($("#BB2B81_s_desc").val())                ,"param_value6": $("#BB2B81_up_protocol_id").val()                ,"param_value7": $("#BB2B81_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BB2B81_get_N01_upd_t_server_ip");
	}
}

//vue回调
function BB2B81_t_server_ip_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#BB2B81_save_t_server_ip_Edit").click(function () {
	$("form[name='BB2B81_DataModal']").submit();
})

/*修改数据*/
function BB2B81_get_N01_upd_t_server_ip(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_server_ip) == true)
	{
		swal("修改数据成功!", "", "success");
		AB2B81_t_server_ip_query();
		BB2B81_clear_validate();
		layer.close(BB2B81_param["ly_index"]);
	}
}

/*添加数据*/
function BB2B81_get_N01_ins_t_server_ip(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_server_ip) == true)
	{
		swal("添加数据成功!", "", "success");
		AB2B81_t_server_ip_query();
		BB2B81_clear_validate();
		layer.close(BB2B81_param["ly_index"]);
	}
}

//取消编辑
$("#BB2B81_cancel_t_server_ip_Edit").click(function () {
	layer.close(BB2B81_param["ly_index"]);
	BB2B81_clear_validate();
	$("[id^='BB2B81_div']").hide();
})

//清除查找框
function BB2B81_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BB2B81_clear_validate(){
	$("#BB2B81_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BB2B81_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BB2B81_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BB2B81_clear_edit_info(){
	var inputs = $("#BB2B81_DataModal").find('input');
	var selects = $("#BB2B81_DataModal").find("select");
	var textareas = $("#BB2B81_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	BB2B81_init_t_server_ip();
}

//页面输入框赋值
function BB2B81_get_edit_info(){
	var rowData = $("#AB2B81_t_server_ip_Events").bootstrapTable('getData')[AB2B81_select_t_server_ip_rowId];
	var inputs = $("#BB2B81_DataModal").find('input');
	var selects = $("#BB2B81_DataModal").find("select");
	var textareas = $("#BB2B81_DataModal").find('textarea');
	$.each(selects, function (i, obj) {
		if(typeof(eval("AB2B81_ary_"+obj.id.toString().replace("BB2B81_",""))) == "object")
		{
			$.each(eval("AB2B81_ary_"+obj.id.toString().replace("BB2B81_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("BB2B81_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("BB2B81_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("AB2B81_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#BB2B81_DataModal").find('[id="'+"BB2B81_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("BB2B81_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("BB2B81_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//form验证
function BB2B81_checkFormInput() {
    BB2B81_validate = $("#BB2B81_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BB2B81_main_id: {}            ,BB2B81_server_ip: {}            ,BB2B81_server_port: {}            ,BB2B81_server_name: {}            ,BB2B81_create_date: {date: true,required : true,maxlength:19}            ,BB2B81_s_desc: {}            ,BB2B81_up_protocol_id: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BB2B81_main_id: {}            ,BB2B81_server_ip: {}            ,BB2B81_server_port: {}            ,BB2B81_server_name: {}            ,BB2B81_create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BB2B81_s_desc: {}            ,BB2B81_up_protocol_id: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BB2B81_SubmitForm();
        	return false;
        }
    })
}