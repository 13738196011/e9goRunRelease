﻿var ly_index;

function biz_start(){
}

function init() {
    ly_index = layer.load();

    $("#btnDebug").click(function () {
        $("#debug_result").val("");
        ly_index = layer.load();
        $.ajax({
            type: "POST",
            async: false,
            url: baseUrl,
            data: {},
            //跨域请求的URL
            dataType: "html",
            jsonpCallback: "A01_A1B2C3",
            url: baseUrl,
            data: {
                param_name: "A01_A1B2C3",
                session_id: session_id,
                param_value1: $("#param_value1").val(),
                param_value2: s_encode($("#param_value2").val()),
                param_value3: $("#param_value3").val()
            },
            success: function (data2) {
                $("#debug_result").val(data2);
                layer.close(ly_index);
            },
            error: function () {
                layer.close(ly_index);
                swal({
                    title: "告警",
                    text: "网络异常或系统故障，请刷新页面！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "刷新",
                    closeOnConfirm: false
                },
                    function () {
                    window.location.reload();
                });
            },
            // 请求完成后的回调函数 (请求成功或失败之后均调用)
            complete: function (XMLHttpRequest, textStatus) {}
        });
    });
    layer.close(ly_index);
    return false;
}

function A01_A1B2C3(input) {
    $("#debug_result").val(input);
    layer.close(ly_index);
}

$(document).ready(function () {
    init_page();
    init();
});
