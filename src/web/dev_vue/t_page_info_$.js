//按钮事件新增或编辑
var BD67FA_type = "";
//其他页面传到本页面参数
var BD67FA_param = {};
//暂时没用
var BD67FA_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

//主从表传递参数
function BD67FA_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function BD67FA_t_page_info_biz_start(inputdata) {
	BD67FA_param = inputdata;
	layer.close(ly_index);
	BD67FA_param_set();
    /*biz begin*/
    BD67FA_init_t_page_info()	
    /*biz end*/
}

/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function BD67FA_page_end(){
	if(BD67FA_param["type"] == "edit"){
		BD67FA_get_edit_info();
	}
}

//页面初始化方法
function BD67FA_init_t_page_info() {
	//type = getUrlParam("type");
	if(BD67FA_param["type"] == "add"){
		$("#BD67FA_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
		
		/*Get Find Select param end*/	
	}
	else if(BD67FA_param["type"] == "edit"){
	
	}
	
	//表单验证
	BD67FA_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BD67FA_CREATE_DATE").val() == "")    {        $("#BD67FA_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BD67FA_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BD67FA_page_end();
}

//提交表单数据
function BD67FA_SubmitForm(){
	if(BD67FA_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_page_info",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#BD67FA_PAGE_CODE").val())                ,"param_value2": s_encode($("#BD67FA_PAGE_TITLE").val())                ,"param_value3": s_encode($("#BD67FA_PAGE_URL").val())                ,"param_value4": $("#BD67FA_CREATE_DATE").val()                ,"param_value5": s_encode($("#BD67FA_S_DESC").val())                ,"param_value6": s_encode($("#BD67FA_JS_URL").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BD67FA_get_N01_ins_t_page_info");
	}
	else if(BD67FA_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_page_info",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#BD67FA_PAGE_CODE").val())                ,"param_value2": s_encode($("#BD67FA_PAGE_TITLE").val())                ,"param_value3": s_encode($("#BD67FA_PAGE_URL").val())                ,"param_value4": $("#BD67FA_CREATE_DATE").val()                ,"param_value5": s_encode($("#BD67FA_S_DESC").val())                ,"param_value6": s_encode($("#BD67FA_JS_URL").val())                ,"param_value7": $("#BD67FA_MAIN_ID").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BD67FA_get_N01_upd_t_page_info");
	}
}

//vue回调
function BD67FA_t_page_info_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#BD67FA_save_t_page_info_Edit").click(function () {
	$("form[name='BD67FA_DataModal']").submit();
})

/*修改数据*/
function BD67FA_get_N01_upd_t_page_info(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_page_info) == true)
	{
		swal("修改数据成功!", "", "success");
		AD67FA_t_page_info_query();
		BD67FA_clear_validate();
		layer.close(BD67FA_param["ly_index"]);
	}
}

/*添加数据*/
function BD67FA_get_N01_ins_t_page_info(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_page_info) == true)
	{
		swal("添加数据成功!", "", "success");
		AD67FA_t_page_info_query();
		BD67FA_clear_validate();
		layer.close(BD67FA_param["ly_index"]);
	}
}

//取消编辑
$("#BD67FA_cancel_t_page_info_Edit").click(function () {
	layer.close(BD67FA_param["ly_index"]);
	BD67FA_clear_validate();
	$("[id^='BD67FA_div']").hide();
})

//清除查找框
function BD67FA_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BD67FA_clear_validate(){
	$("#BD67FA_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BD67FA_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BD67FA_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BD67FA_clear_edit_info(){
	var inputs = $("#BD67FA_DataModal").find('input');
	var selects = $("#BD67FA_DataModal").find("select");
	var textareas = $("#BD67FA_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	BD67FA_init_t_page_info();
}

//页面输入框赋值
function BD67FA_get_edit_info(){
	var rowData = $("#AD67FA_t_page_info_Events").bootstrapTable('getData')[AD67FA_select_t_page_info_rowId];
	var inputs = $("#BD67FA_DataModal").find('input');
	var selects = $("#BD67FA_DataModal").find("select");
	var textareas = $("#BD67FA_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"AD67FA","BD67FA");
}

//form验证
function BD67FA_checkFormInput() {
    BD67FA_validate = $("#BD67FA_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BD67FA_main_id: {}            ,BD67FA_PAGE_CODE: {}            ,BD67FA_PAGE_TITLE: {}            ,BD67FA_PAGE_URL: {}            ,BD67FA_CREATE_DATE: {date: true,required : true,maxlength:19}            ,BD67FA_S_DESC: {}            ,BD67FA_JS_URL: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BD67FA_main_id: {}            ,BD67FA_PAGE_CODE: {}            ,BD67FA_PAGE_TITLE: {}            ,BD67FA_PAGE_URL: {}            ,BD67FA_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BD67FA_S_DESC: {}            ,BD67FA_JS_URL: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BD67FA_SubmitForm();
        	return false;
        }
    })
}