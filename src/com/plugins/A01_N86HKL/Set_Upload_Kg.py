#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''
import json,datetime
from com.plugins.A01_N86HKL import A01_Para
from com.plugins.A01_N86HKL.kg_upload_flag import kg_upload_flag
from com.plugins.A01_N86HKL.kg_hour import kg_hour
from com.plugins.A01_N86HKL.data_min import data_min
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
class Set_Upload_Kg(object):
    def __init__(self):
        pass
    def Start_Pro(self):
        if A01_Para.kuf  is None:
            A01_Para.kuf = kg_upload_flag()
            self.get_kg_upload_flag()
        if Com_Fun.GetTime("%d %H:%M") == "01 00:05":
            temNDate = Com_Fun.DateTimeAdd(datetime.datetime.now(),"d",-1)
            temTbName = "data_min"+Com_Fun.GetTimeInput("%Y%m",temNDate)            
            temMonitor_data = self.get_kg_minuter(temTbName)
            for tsi in temMonitor_data:
                try:
                    if self.send_kg_minuter(tsi,temTbName):
                        A01_Para.kuf.attminuter_flag = tsi.attcpu_time
                        A01_Para.kuf.attminuter_create_date = Com_Fun.GetTimeDef()
                except Exception as e:
                    print("A01_Upload_Kg.init_start 1 error:"+repr(e))
                finally:
                    pass
            temkhs = self.get_kg_hour(temTbName,"1")
            for tsi in temkhs:
                try:
                    if self.send_kg_hour(tsi,"1"):
                        A01_Para.kuf.atthour_flag = tsi.attcpu_time;
                        A01_Para.kuf.atthour_create_date = Com_Fun.GetTimeDef()
                        self.upd_kg_upload_flag()
                except Exception as e:
                    print("A01_Upload_Kg.init_start 2 error:"+repr(e))
                finally:
                    pass
            temkhs = self.get_kg_hour(temTbName,"2")
            for tsi in temkhs:
                try:
                    if self.send_kg_hour(tsi,"2"):
                        A01_Para.kuf.atthour_flag2 = tsi.attcpu_time;
                        A01_Para.kuf.atthour_create_date2 = Com_Fun.GetTimeDef()
                        self.upd_kg_upload_flag()
                except Exception as e:
                    print("A01_Upload_Kg.init_start 3 error:"+repr(e))
                finally:
                    pass
        temTbName = "data_min"+Com_Fun.GetTime("%Y%m")
        temMonitor_data = self.get_kg_minuter(temTbName)
        for tsi in temMonitor_data:
            try:
                if self.send_kg_minuter(tsi,temTbName):
                    A01_Para.kuf.attminuter_flag = tsi.attcpu_time
                    A01_Para.kuf.attminuter_create_date = Com_Fun.GetTimeDef()
            except Exception as e:
                print("A01_Upload_Kg.init_start 4 error:"+repr(e))
            finally:
                pass
        temkhs = self.get_kg_hour(temTbName,"1")
        for tsi in temkhs:
            try:
                if self.send_kg_hour(tsi,"1"):
                    A01_Para.kuf.atthour_flag = tsi.attcpu_time;
                    A01_Para.kuf.atthour_create_date = Com_Fun.GetTimeDef()
                    self.upd_kg_upload_flag()
            except Exception as e:
                print("A01_Upload_Kg.init_start 5 error:"+repr(e))
            finally:
                pass
        temkhs = self.get_kg_hour(temTbName,"2")
        for tsi in temkhs:
            try:
                if self.send_kg_hour(tsi,"2"):
                    A01_Para.kuf.atthour_flag2 = tsi.attcpu_time;
                    A01_Para.kuf.atthour_create_date2 = Com_Fun.GetTimeDef()
                    self.upd_kg_upload_flag()
            except Exception as e:
                print("A01_Upload_Kg.init_start 6 error:"+repr(e))
            finally:
                pass
    def upd_kg_upload_flag(self):
        try:
            temStrSql = "update kg_upload_falg set hour_flag='" + Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S",A01_Para.kuf.atthour_flag) + "',minuter_flag='" + Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S",A01_Para.kuf.attminuter_flag)
            temStrSql = temStrSql + "',hour_create_date='" + Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S",A01_Para.kuf.atthour_create_date) + "',minuter_create_date='" + Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S",A01_Para.kuf.attminuter_create_date)
            temStrSql = temStrSql + "',hour_flag2='" + Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S",A01_Para.kuf.atthour_flag2) + "',hour_create_date2='" + Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S",A01_Para.kuf.atthour_create_date2) + "'"
            temDb2 = Db_Common2()
            temDb2.CommonExec_Sql(temStrSql)
        except Exception as e:
            print("A01_Upload_Kg.upd_kg_upload_flag error:"+repr(e))
        finally:
            pass
    def send_kg_hour(self,inputkg_hour,inputS_Type):
        inputValues = {}
        inputValues["param_value1"] = str(inputkg_hour.attcpu_time).replace(" ","%20")
        inputValues["param_value2"] = inputkg_hour.atttank_weight
        inputValues["param_value3"] = Com_Fun.GetTimeDef()
        inputValues["param_value4"] = "1"
        inputValues["param_value5"] = inputS_Type
        inputUrl = "http://localhost:8080/CenterData/getdata.jsp?a=1" + A01_Para.strCon+"hdpe_upload"
        m_result = Com_Fun.PostHttp(inputUrl, inputValues)
        if m_result == "-1" or m_result == "":
            return False
        else:
            try:
                temJso = json.loads(m_result)
                temJsoary = temJso["hdpe_upload"]
                for temobj in temJsoary:
                    temSResult = temobj["s_result"]
                    if temSResult == 1:
                        return True
                    else:
                        return False
            except Exception as es:
                print("A01_Upload_Kg.send_kg_hour error:"+m_result)
                return False                
    def send_kg_minuter(self,inputMonitor_Data,inputTbName):
        inputValues = {}
        inputValues["param_value1"] = inputMonitor_Data.attcpu_time.replace(" ","%20")
        inputValues["param_value2"] = inputMonitor_Data.attautomatic_on
        inputValues["param_value3"] = inputMonitor_Data.atttime_base
        inputValues["param_value4"] = inputMonitor_Data.attdoor_closed
        inputValues["param_value5"] = inputMonitor_Data.attextruder_on
        inputValues["param_value6"] = inputMonitor_Data.attbatch_counter
        inputValues["param_value7"] = inputMonitor_Data.attcycle_time
        inputValues["param_value8"] = inputMonitor_Data.atttank_weight
        inputValues["param_value9"] = inputMonitor_Data.attcooling_water_temp
        inputValues["param_value10"] = inputMonitor_Data.attair_pressure
        inputValues["param_value11"] = inputMonitor_Data.atts_type
        inputUrl = "http://localhost:8080/CenterData/getdata.jsp?a=1" + A01_Para.strCon+"data_min_upload"
        m_result = Com_Fun.PostHttp(inputUrl, inputValues)
        if m_result == "-1" or m_result == "":
            return False
        else:
            try:
                temJso = json.loads(m_result)
                temJsoary = temJso["data_min_upload"]
                for temobj in temJsoary:
                    temSResult = temobj["s_result"]
                    if temSResult == 1:
                        temDb2 = Db_Common2()
                        temUpdstrSql = "update "+inputTbName+" set m_flag = '1' where main_id='"+str(inputMonitor_Data.attmain_id)+"'"
                        temDb2.CommonExec_Sql(temUpdstrSql)
                        return True
            except Exception as es:
                print("A01_Upload_Kg.send_kg_minuter error:"+m_result)
                return True
    def get_kg_upload_flag(self):
        temStrSql = "SELECT * FROM kg_upload_falg"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        try:
            for temItem in temRs:
                A01_Para.kuf.attMAIN_ID = Com_Fun.Str_To_Int(temItem[0])
                A01_Para.kuf.atthour_flag = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S",temItem[1])
                A01_Para.kuf.attminuter_flag = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S",temItem[2])
                A01_Para.kuf.atthour_create_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S",temItem[3])
                A01_Para.kuf.attminuter_create_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S",temItem[4])
                A01_Para.kuf.atthour_flag2 = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S",temItem[6])
                A01_Para.kuf.atthour_create_date2 = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S",temItem[7])
        except Exception as e:
            temStr = "A01_Upload_Kg error [A01_Upload_Kg.get_kg_upload_falg]" + "\r\n" + repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temStr)
            uL.WriteLog()
        finally:
            pass
    def get_kg_hour(self,inputTbName,inputS_Type):                
        temhour_date = Com_Fun.DateTimeAdd(Com_Fun.GetToTimeInput("%Y-%m-%d %H:%M:%S",A01_Para.kuf.atthour_flag),"H",-1).strftime("%Y-%m-%d %H:00:00")            
        temStrSql = "SELECT sum(round(tank_weight,6)) as 'tank_weight',strftime('%Y-%m-%d %H:00:00',cpu_time) as 'cpu_time' "
        temStrSql = temStrSql + " FROM "+inputTbName +" where s_type='"+inputS_Type+"' and strftime('%Y-%m-%d %H:00:00',cpu_time) >='"+str(temhour_date)+"' and cpu_time < '"+Com_Fun.GetTimeDef()+"' "
        temStrSql = temStrSql + " group by strftime('%Y-%m-%d %H:00:00',cpu_time) "
        temStrSql = temStrSql + " order by strftime('%Y-%m-%d %H:00:00',cpu_time) asc limit 100"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temkg_hour  = kg_hour()
                temkg_hour.atttank_weight = Com_Fun.NoNull(temItem[0])
                temkg_hour.attcpu_time = Com_Fun.GetTimeInput("%Y-%m-%d %H:00:00",temItem[1])
                temList.append(temkg_hour)
        except Exception as e:
            temStr = "get_kg_hour error [A01_Upload_Kg.get_kg_hour]" + "\r\n" + repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temStr)
            uL.WriteLog()
        finally:
            pass
        return temList
    def get_kg_minuter(self,inputTbName):
        temStrSql = "SELECT main_id,cpu_time,automatic_on,time_base,door_closed,extruder_on,batch_counter,cycle_time,tank_weight,cooling_water_temp,air_pressure,s_type FROM "+inputTbName+" where m_flag=0 order by cpu_time asc limit 100"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []        
        try:
            for temItem in temRs:
                temdata_min = data_min()
                temdata_min.attmain_id = Com_Fun.Str_To_Int(temItem[0])
                temdata_min.attcpu_time = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S",temItem[1])
                temdata_min.attautomatic_on = Com_Fun.NoNull(temItem[2])
                temdata_min.atttime_base = Com_Fun.NoNull(temItem[3])
                temdata_min.attdoor_closed = Com_Fun.NoNull(temItem[4])
                temdata_min.attextruder_on = Com_Fun.NoNull(temItem[5])
                temdata_min.attbatch_counter = Com_Fun.NoNull(temItem[6])
                temdata_min.attcycle_time = Com_Fun.NoNull(temItem[7])
                temdata_min.atttank_weight = Com_Fun.NoNull(temItem[8])
                temdata_min.attcooling_water_temp = Com_Fun.NoNull(temItem[9])
                temdata_min.attair_pressure = Com_Fun.NoNull(temItem[10])  
                temdata_min.atts_type = Com_Fun.NoNull(temItem[11])                
                temList.append(temdata_min)
        except Exception as es:
            temError = "get data error [A01_Upload_Kg.get_kg_minuter]" + temStrSql +"\r\n"+repr(es)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temError)
            uL.WriteLog()
        return temList
