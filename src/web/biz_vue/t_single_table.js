//选择某一行
var AE35BE_select_t_single_table_rowId = "";
//按钮事件新增或编辑
var AE35BE_type = "";
//其他页面传到本页面参数
var AE35BE_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var AE35BE_tem_table_cn_name = "";var AE35BE_tem_table_en_name = "";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

//业务逻辑数据开始
function AE35BE_t_single_table_biz_start(inputparam) {
	layer.close(ly_index);
	AE35BE_param = inputparam;
    /*biz begin*/
    AE35BE_init_t_single_table()	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function AE35BE_page_end(){
}

//元数据表管理显示列定义
var AE35BE_t_single_table = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '表中文名',        field: 'TABLE_CN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '表英文名',        field: 'TABLE_EN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '系统备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function AE35BE_init_t_single_table() {
	$(window).resize(function () {
		  $('#AE35BE_t_single_table_Events').bootstrapTable('resetView');
	});
	//元数据表管理查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AE35BE_btn_t_single_table_query').click();
}

//查询接口
function AE35BE_t_single_table_query() {
    $('#AE35BE_t_single_table_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_single_table",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(AE35BE_tem_table_cn_name)        ,"param_value2": s_encode(AE35BE_tem_table_en_name)		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AE35BE_get_N01_sel_t_single_table");
}

//查询结果
function AE35BE_get_N01_sel_t_single_table(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_single_table) == false)
        return false;    
	var s_data = input.N01_sel_t_single_table;
    AE35BE_select_t_single_table_rowId = "";
    $('#AE35BE_t_single_table_Events').bootstrapTable('destroy');
    $("#AE35BE_t_single_table_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AE35BE_t_single_table_Events').find("tr.changeColor").removeClass('changeColor');
                AE35BE_select_t_single_table_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AE35BE_t_single_table_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AE35BE_select_t_single_table_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AE35BE_param.hasOwnProperty("target_name"))
			{
				$("#"+AE35BE_param["target_id"]).val(eval("row."+AE35BE_param["sourc_id"].toUpperCase()));
				$("#"+AE35BE_param["target_name"]).val(s_decode(eval("row."+AE35BE_param["sourc_name"].toUpperCase())));				
				layer.close(AE35BE_param["ly_index"]);
			}
		},
        toolbar: "#AE35BE_t_single_table_Toolbar",
        columns: AE35BE_t_single_table,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AE35BE_btn_t_single_table_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    AE35BE_tem_table_cn_name = "";    AE35BE_tem_table_en_name = "";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/

	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    AE35BE_init_t_single_table()	
    /*biz end*/
})

//查询按钮
$('#AE35BE_btn_t_single_table_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    AE35BE_tem_table_cn_name = $("#AE35BE_qry_table_cn_name").val();    AE35BE_tem_table_en_name = $("#AE35BE_qry_table_en_name").val();	
    /*set query param end*/
	AE35BE_t_single_table_query();
})

//vue回调
function AE35BE_t_single_table_call_vue(objResult){
	if(index_subhtml == "t_single_table_$.vue")
	{
		var n = Get_RandomDiv("BE35BE",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AE35BE_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temtable_cn_name = $("#AE35BE_qry_table_cn_name").val();                if(temtable_cn_name != ""){                    inputdata["table_cn_name"] = temtable_cn_name;                }                var temtable_en_name = $("#AE35BE_qry_table_en_name").val();                if(temtable_en_name != ""){                    inputdata["table_en_name"] = temtable_en_name;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_single_table_$.js","BE35BE_t_single_table_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//新增按钮
$("#AE35BE_btn_t_single_table_add").click(function () {
	AE35BE_type = "add";
	index_subhtml = "t_single_table_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_single_table_$.vue","AE35BE_t_single_table_call_vue") == true){
		var n = Get_RandomDiv("BE35BE","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BE35BE_param["type"] = AE35BE_type;
				BE35BE_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temtable_cn_name = $("#AE35BE_qry_table_cn_name").val();                if(temtable_cn_name != ""){                    BE35BE_param["table_cn_name"] = temtable_cn_name;                }                var temtable_en_name = $("#AE35BE_qry_table_en_name").val();                if(temtable_en_name != ""){                    BE35BE_param["table_en_name"] = temtable_en_name;                }				
				/*Send Two FindSelect param end*/

				BE35BE_clear_edit_info();
			},
			end: function(){
				BE35BE_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AE35BE_btn_t_single_table_edit").click(function () {
	if (AE35BE_select_t_single_table_rowId != "") {
		AE35BE_type = "edit";
		index_subhtml = "t_single_table_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_single_table_$.vue","AE35BE_t_single_table_call_vue") == true){
			var n = Get_RandomDiv("BE35BE","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BE35BE_param["type"] = AE35BE_type;
					BE35BE_param["ly_index"] = index;
					BE35BE_clear_edit_info();
					BE35BE_get_edit_info();
				},
				end: function(){
					BE35BE_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AE35BE_btn_t_single_table_delete').click(function () {
	//单行选择
	var rowData = $("#AE35BE_t_single_table_Events").bootstrapTable('getData')[AE35BE_select_t_single_table_rowId];
	//多行选择
	var rowDatas = AE35BE_sel_row_t_single_table();
	if (AE35BE_select_t_single_table_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_single_table",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AE35BE_N01_del_t_single_table");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_single_table",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AE35BE_N01_del_t_single_table");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AE35BE_N01_del_t_single_table(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_single_table) == true)
		AE35BE_t_single_table_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function AE35BE_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AE35BE_sel_row_t_single_table(){
	//获得选中行
	var checkedbox= $("#AE35BE_t_single_table_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}