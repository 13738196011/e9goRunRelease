#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_page_item(object):
    attmain_id = -1
    attPAGE_ID = 0
    attPROC_ID = 0
    attITEM_NAME = ""
    attITEM_TYPE = 0
    attCOLUMN_COUNT = 0
    attCREATE_DATE = None
    attS_DESC = ""
    attURL_IN_PARAM = ""
    lenITEM_NAME = 50
    lenS_DESC = 500
    lenURL_IN_PARAM = 500
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["PAGE_ID"] = "接口集应用模板"
        temHt["PROC_ID"] = "接口名称"
        temHt["ITEM_NAME"] = "子项名称"
        temHt["ITEM_TYPE"] = "子项类别"
        temHt["COLUMN_COUNT"] = "统计列数"
        temHt["CREATE_DATE"] = "系统时间"
        temHt["S_DESC"] = "备注"
        temHt["URL_IN_PARAM"] = "输入参数"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["PAGE_ID"] = "接口集应用模板"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["PAGE_ID"] = "select MAIN_ID,PAGE_TITLE as CN_NAME from t_page_info"
        temHtSelect["PROC_ID"] = "N01_t_proc_inparam$PROC_ID"
        temHtSelect["ITEM_TYPE"] = [{"MAIN_ID":"1","CN_NAME":"表格模式"},{"MAIN_ID":"2","CN_NAME":"列表模式"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        temHtFind["PROC_ID"] = "t_proc_name.vue"
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        temHtFReturn["PROC_ID"] = ["MAIN_ID","INF_CN_NAME"]
        return temHtFReturn
