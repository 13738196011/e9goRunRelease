#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class T_PROC_RETURN(object):
    attMAIN_ID = -1
    attPROC_ID = -1
    attRETURN_NAME = ""
    attRETURN_TYPE = ""
    attS_DESC = ""
    attCREATE_DATE = None
    attIS_IMG = 0
    attIS_URLENCODE = 0
    attLIST_HIDDEN = 0
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")