#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class T_PROC_LOG(object):
    attMAIN_ID = -1
    attSUB_USER_ID = -1
    attPROC_ID = -1
    attS_COUNT = -1
    attSYS_DATE = None
    attS_DESC = ""
    attCREATE_DATE = None
    def __init__(self):
        self.attSYS_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
