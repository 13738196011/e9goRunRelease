//按钮事件新增或编辑
var B24E7B_type = "";
//其他页面传到本页面参数
var B24E7B_param = {};
//暂时没用
var B24E7B_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var B24E7B_ary_SUB_ID = null;var B24E7B_ary_PROC_ID = null;var B24E7B_ary_LIMIT_TAG = [{'MAIN_ID': '1', 'CN_NAME': '年'}, {'MAIN_ID': '2', 'CN_NAME': '月'}, {'MAIN_ID': '3', 'CN_NAME': '日'}];
/*declare select options end*/

//业务逻辑数据开始
function B24E7B_t_sub_power_biz_start(inputdata) {
	B24E7B_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_sub_power$SUB_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B24E7B_get_N01_t_sub_power$SUB_ID");	
    /*biz end*/
}

/*biz step begin*/
function B24E7B_format_SUB_ID(value, row, index) {    var objResult = value;    for(i = 0; i < B24E7B_ary_SUB_ID.length; i++) {        var obj = B24E7B_ary_SUB_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B24E7B_format_PROC_ID(value, row, index) {    var objResult = value;    for(i = 0; i < B24E7B_ary_PROC_ID.length; i++) {        var obj = B24E7B_ary_PROC_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B24E7B_format_LIMIT_TAG(value, row, index) {    var objResult = value;    for(i = 0; i < B24E7B_ary_LIMIT_TAG.length; i++) {        var obj = B24E7B_ary_LIMIT_TAG[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B24E7B_get_N01_t_sub_power$SUB_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_sub_power$SUB_ID) == false)        return false;    B24E7B_ary_SUB_ID = input.N01_t_sub_power$SUB_ID;    if($("#B24E7B_SUB_ID").is("select") && $("#B24E7B_SUB_ID")[0].options.length == 0)    {        $.each(B24E7B_ary_SUB_ID, function (i, obj) {            addOptionValue("B24E7B_SUB_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    var inputdata = {        "param_name": "N01_t_proc_inparam$PROC_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B24E7B_get_N01_t_proc_inparam$PROC_ID");}function B24E7B_get_N01_t_proc_inparam$PROC_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_inparam$PROC_ID) == false)        return false;    B24E7B_ary_PROC_ID = input.N01_t_proc_inparam$PROC_ID;    if($("#B24E7B_PROC_ID").is("select") && $("#B24E7B_PROC_ID")[0].options.length == 0)    {        $.each(B24E7B_ary_PROC_ID, function (i, obj) {            addOptionValue("B24E7B_PROC_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#B24E7B_LIMIT_TAG").is("select") && $("#B24E7B_LIMIT_TAG")[0].options.length == 0)    {        $.each(B24E7B_ary_LIMIT_TAG, function (i, obj) {            addOptionValue("B24E7B_LIMIT_TAG", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    B24E7B_init_t_sub_power();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function B24E7B_PROC_ID_cn_name_fun(){    index_subhtml = "t_proc_name.vue"    random_subhtml = "A1BCF7";    if(loadHtmlSubVueFun("biz_vue/t_proc_name.vue","B24E7B_t_sub_power_call_vue") == true){        var n = Get_RandomDiv("A1BCF7","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#A1BCF7_t_proc_name_Events').bootstrapTable('resetView');                A1BCF7_param["ly_index"] = index;                A1BCF7_param["target_name"] = "B24E7B_find_PROC_ID_cn_name"                A1BCF7_param["target_id"] = "B24E7B_PROC_ID"                A1BCF7_param["sourc_id"] = "MAIN_ID"                A1BCF7_param["sourc_name"] = "INF_CN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function B24E7B_page_end(){
	if(B24E7B_param["type"] == "edit"){
		B24E7B_get_edit_info();
	}
}

//页面初始化方法
function B24E7B_init_t_sub_power() {
	//type = getUrlParam("type");
	if(B24E7B_param["type"] == "add"){
		$("#B24E7B_main_id").parent().parent().parent().hide();
		$("#B24E7B_LIMIT_TAG").val("3");
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#B24E7B_SUB_ID").val(B24E7B_param["SUB_ID"]);        $("#B24E7B_PROC_ID").val(B24E7B_param["PROC_ID"]);        $("#B24E7B_find_PROC_ID_cn_name").val(B24E7B_param["PROC_ID_cn_name"]);		
		/*Get Find Select param end*/	
	}
	else if(B24E7B_param["type"] == "edit"){
	
	}
	
	//表单验证
	B24E7B_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B24E7B_LIMIT_DATE").val() == "")    {        $("#B24E7B_LIMIT_DATE").val(DateAdd("y",50,new Date()).Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B24E7B_LIMIT_DATE',        type: 'datetime',        trigger: 'click'    });    if($("#B24E7B_FIRST_DATE").val() == "")    {        $("#B24E7B_FIRST_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B24E7B_FIRST_DATE',        type: 'datetime',        trigger: 'click'    });    if($("#B24E7B_CREATE_DATE").val() == "")    {        $("#B24E7B_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B24E7B_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B24E7B_page_end();
}

//提交表单数据
function B24E7B_SubmitForm(){
	if(B24E7B_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_sub_power",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#B24E7B_SUB_ID").val()                ,"param_value2": $("#B24E7B_PROC_ID").val()                ,"param_value3": $("#B24E7B_LIMIT_DATE").val()                ,"param_value4": $("#B24E7B_USE_LIMIT").val()                ,"param_value5": $("#B24E7B_LIMIT_NUMBER").val()                ,"param_value6": s_encode($("#B24E7B_LIMIT_TAG").val())                ,"param_value7": $("#B24E7B_FIRST_DATE").val()                ,"param_value8": s_encode($("#B24E7B_S_DESC").val())                ,"param_value9": $("#B24E7B_CREATE_DATE").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B24E7B_get_N01_ins_t_sub_power");
	}
	else if(B24E7B_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_sub_power",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#B24E7B_SUB_ID").val()                ,"param_value2": $("#B24E7B_PROC_ID").val()                ,"param_value3": $("#B24E7B_LIMIT_DATE").val()                ,"param_value4": $("#B24E7B_USE_LIMIT").val()                ,"param_value5": $("#B24E7B_LIMIT_NUMBER").val()                ,"param_value6": s_encode($("#B24E7B_LIMIT_TAG").val())                ,"param_value7": $("#B24E7B_FIRST_DATE").val()                ,"param_value8": s_encode($("#B24E7B_S_DESC").val())                ,"param_value9": $("#B24E7B_CREATE_DATE").val()                ,"param_value10": $("#B24E7B_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B24E7B_get_N01_upd_t_sub_power");
	}
}

//vue回调
function B24E7B_t_sub_power_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_proc_name.vue"){        var n = Get_RandomDiv("A1BCF7",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":B24E7B_type,                    "ly_index":index,                    "target_name":"B24E7B_find_PROC_ID_cn_name",                    "target_id":"B24E7B_find_PROC_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"INF_CN_NAME"                };                loadScript_hasparam("biz_vue/t_proc_name.js","A1BCF7_t_proc_name_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//for表单提交
$("#B24E7B_save_t_sub_power_Edit").click(function () {
	$("form[name='B24E7B_DataModal']").submit();
})

/*修改数据*/
function B24E7B_get_N01_upd_t_sub_power(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_sub_power) == true)
	{
		swal("修改数据成功!", "", "success");
		A24E7B_t_sub_power_query();
		B24E7B_clear_validate();
		layer.close(B24E7B_param["ly_index"]);
	}
}

/*添加数据*/
function B24E7B_get_N01_ins_t_sub_power(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_sub_power) == true)
	{
		swal("添加数据成功!", "", "success");
		A24E7B_t_sub_power_query();
		B24E7B_clear_validate();
		layer.close(B24E7B_param["ly_index"]);
	}
}

//取消编辑
$("#B24E7B_cancel_t_sub_power_Edit").click(function () {
	layer.close(B24E7B_param["ly_index"]);
	B24E7B_clear_validate();
	$("[id^='B24E7B_div']").hide();
})

//清除查找框
function B24E7B_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B24E7B_clear_validate(){
	$("#B24E7B_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B24E7B_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B24E7B_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B24E7B_clear_edit_info(){
	var inputs = $("#B24E7B_DataModal").find('input');
	var selects = $("#B24E7B_DataModal").find("select");
	var textareas = $("#B24E7B_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
    B24E7B_clear_input_cn_name('B24E7B_find_PROC_ID_cn_name','B24E7B_PROC_ID')	
	/*input validate clear end*/
	B24E7B_init_t_sub_power();
}

//页面输入框赋值
function B24E7B_get_edit_info(){
	var rowData = $("#A24E7B_t_sub_power_Events").bootstrapTable('getData')[A24E7B_select_t_sub_power_rowId];
	var inputs = $("#B24E7B_DataModal").find('input');
	var selects = $("#B24E7B_DataModal").find("select");
	var textareas = $("#B24E7B_DataModal").find('textarea');
	$.each(selects, function (i, obj) {
		if(typeof(eval("A24E7B_ary_"+obj.id.toString().replace("B24E7B_",""))) == "object")
		{
			$.each(eval("A24E7B_ary_"+obj.id.toString().replace("B24E7B_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("B24E7B_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("B24E7B_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("A24E7B_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#B24E7B_DataModal").find('[id="'+"B24E7B_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("B24E7B_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("B24E7B_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//form验证
function B24E7B_checkFormInput() {
    B24E7B_validate = $("#B24E7B_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B24E7B_main_id: {}            ,B24E7B_SUB_ID: {}            ,B24E7B_PROC_ID: {}            ,B24E7B_LIMIT_DATE: {date: true,required : true,maxlength:19}            ,B24E7B_USE_LIMIT: {digits: true,required : true,maxlength:10}            ,B24E7B_LIMIT_NUMBER: {digits: true,required : true,maxlength:10}            ,B24E7B_LIMIT_TAG: {}            ,B24E7B_FIRST_DATE: {date: true,required : true,maxlength:19}            ,B24E7B_S_DESC: {}            ,B24E7B_CREATE_DATE: {date: true,required : true,maxlength:19}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B24E7B_main_id: {}            ,B24E7B_SUB_ID: {}            ,B24E7B_PROC_ID: {}            ,B24E7B_LIMIT_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B24E7B_USE_LIMIT: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,B24E7B_LIMIT_NUMBER: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,B24E7B_LIMIT_TAG: {}            ,B24E7B_FIRST_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B24E7B_S_DESC: {}            ,B24E7B_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B24E7B_SubmitForm();
        	return false;
        }
    })
}