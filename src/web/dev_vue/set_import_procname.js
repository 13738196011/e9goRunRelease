//业务逻辑数据开始
function A887T1_set_biz_start(inputparam) {
	layer.close(ly_index);
    /*biz begin*/
	$("#A887T1_btnFileUpload").click(function () {
    	var formData = new FormData($("#A887T1_updForm").eq(0)[0]);
    	var param_v = "&login_id="+login_id;
    	if($("#A887T1_file_set").val() == "")
    		swal("请选择正确附件!", "请选择正确附件", "warning");
    	else
    		get_ajax_upload(formData,param_v,"A887T1_get_upload_file");
    });
    
    $("#A887T1_btnClearFile").click(function () {
    	$("#A887T1_upd_file_list").html("");
    });
}

function A887T1_Clear(){
	$("#A887T1_file_set").val("");
	$("#A887T1_upd_file_list").html("");
}

function A887T1_get_A01_INPROC(objTag){
	var objValue = objTag.A01_INPROC;
	if (Call_OpeResult(objValue) == true)
	{
	}
}

function A887T1_get_upload_file(objTag){
	var objValue = objTag.A01_UpLoadFile;
	if (Call_OpeResult(objValue) == true)
	{
		$.each(objValue, function (i, obj) {
			var fileList = "<a href=\"../file/"+obj[GetLowUpp("newFileName")]+"\" target='_blank'>"+obj[GetLowUpp("oldFileName")]+"</a><br>";
			$("#A887T1_upd_file_list").append(fileList);
			
			if(obj[GetLowUpp("oldFileName")].indexOf("不符合上传文件格式") == -1)
			{				
			    var inputdata = {
			    	"param_name": "A01_INPROC",
			        "session_id": session_id,
			        "login_id": login_id,
			        "param_value1": s_encode(obj[GetLowUpp("newFileName")])
			    };
			    get_ajax_baseurl(inputdata, "A887T1_get_A01_INPROC");
			}
		});
	}
}