#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class v_server_protocol(object):
    attmain_id = -1
    attserver_ip = ""
    attserver_port = ""
    attprot_name = ""
    attcn_name = ""
    atts_state = 1
    atts_desc = ""
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")