#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_prot_ele_link(object):
    attmain_id = -1
    attprot_id = 0
    attelement_id = 0
    attcreate_date = None
    atts_desc = ""
    attprot_up_limit = ""
    attprot_down_limit = ""
    lens_desc = 500
    lenprot_up_limit = 50
    lenprot_down_limit = 50
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["prot_id"] = "采集协议名称"
        temHt["element_id"] = "因子名称"
        temHt["create_date"] = "系统时间"
        temHt["s_desc"] = "备注信息"
        temHt["prot_up_limit"] = "AD模块量程上限"
        temHt["prot_down_limit"] = "AD模块量程下限"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["prot_id"] = "采集协议名称"
        temHtQry["element_id"] = "因子名称"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["prot_id"] = "select main_id,prot_name || '[' || oem_name || ']' as cn_name from t_protocol where s_state = '1'"
        temHtSelect["element_id"] = "select main_id,element_name as cn_name from t_code_element where s_state = '1'"
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
