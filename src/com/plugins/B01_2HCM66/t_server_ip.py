#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_server_ip(object):
    attmain_id = -1
    attserver_ip = ""
    attserver_port = ""
    attserver_name = ""
    attcreate_date = None
    atts_desc = ""
    attup_protocol_id = 0
    lenserver_ip = 50
    lenserver_port = 50
    lenserver_name = 50
    lens_desc = 50
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["server_ip"] = "上传平台IP"
        temHt["server_port"] = "上传平台端口号"
        temHt["server_name"] = "上传协议名称"
        temHt["create_date"] = "系统时间"
        temHt["s_desc"] = "上传标识位(不得超过整数10)"
        temHt["up_protocol_id"] = "上传协议模块"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["up_protocol_id"] = "上传协议模块"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["up_protocol_id"] = "select main_id,prot_name || cn_name as cn_name from t_up_protocol"
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
