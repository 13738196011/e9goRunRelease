#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class T_CHILD_NODE(object):
    attMAIN_ID        = -1
    attCHILD_NAME    = ""
    attCHILD_NODE    = ""
    attS_DESC        = ""
    attCREATE_DATE    = None
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")