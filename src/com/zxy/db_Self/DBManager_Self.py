#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common import Com_Para
from com.zxy.common.DbConfigSelf import DbConfigSelf
from com.zxy.db_Self.ConnectionPool_Self import ConnectionPool_Self
from com.zxy.z_debug import z_debug
class DBManager_Self(z_debug):
    attConn = None
    attConnectionPool = None
    def __init__(self):
        if Com_Para.url == "":
            DbConfigSelf.GetDbConfigSelfNew()
        self.attConnectionPool = ConnectionPool_Self(Com_Para.driverClassName,Com_Para.url,Com_Para.username,Com_Para.password)
        try:
            self.attConnectionPool.createPool()
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            pass
    def getConnection(self):
        try:
            self.attConn = self.attConnectionPool.getConnection()
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            return self.attConn
    def returnConnection(self,inputConn):
        return self.attConnectionPool.returnConnection(inputConn)
    @staticmethod
    def closeConnectionPoolTimeOut(self):
        try:
            self.attConnectionPool.closeConnectionPoolTimeOut()
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            pass