#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_code_element(object):
    attmain_id = -1
    attelement_code = ""
    attelement_name = ""
    attelement_val = ""
    attelement_type = 0
    attelement_unit = ""
    atts_state = 0
    attcreate_date = None
    atts_desc = ""
    attdot_num = 0
    attup_level = ""
    attdown_level = ""
    attup_limit = ""
    attdown_limit = ""
    attelement_code2 = ""
    lenelement_code = 50
    lenelement_name = 50
    lenelement_val = 50
    lenelement_unit = 50
    lens_desc = 500
    lenup_level = 50
    lendown_level = 50
    lenup_limit = 50
    lendown_limit = 50
    lenelement_code2 = 50
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["element_code"] = "因子编码"
        temHt["element_name"] = "因子名称"
        temHt["element_val"] = "存储位"
        temHt["element_type"] = "因子类型"
        temHt["element_unit"] = "因子单位"
        temHt["s_state"] = "是否启用"
        temHt["create_date"] = "系统时间"
        temHt["s_desc"] = "备注"
        temHt["dot_num"] = "小数点位"
        temHt["up_level"] = "报警上限"
        temHt["down_level"] = "报警下限"
        temHt["up_limit"] = "量程上限"
        temHt["down_limit"] = "量程下限"
        temHt["element_code2"] = "因子编码2"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["element_name"] = "因子名称"
        temHtQry["element_type"] = "因子类型"
        temHtQry["s_state"] = "是否启用"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["element_type"] = [{"main_id":"1","cn_name":"污染源水"},{"main_id":"2","cn_name":"污染源气"}]
        temHtSelect["s_state"] = [{"main_id":"1","cn_name":"启用"},{"main_id":"0","cn_name":"禁用"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
