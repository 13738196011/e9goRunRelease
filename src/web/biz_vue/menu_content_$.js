//按钮事件新增或编辑
var BAB8DC_type = "";
//其他页面传到本页面参数
var BAB8DC_param = {};
//暂时没用
var BAB8DC_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BAB8DC_ary_menu_id = null;var BAB8DC_ary_is_ava = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];var BAB8DC_ary_role_id = [{'main_id': '1', 'cn_name': '开发者'}, {'main_id': '2', 'cn_name': '系统管理员'}, {'main_id': '3', 'cn_name': '运维人员'}, {'main_id': '4', 'cn_name': '普通用户'}];
/*declare select options end*/

//业务逻辑数据开始
function BAB8DC_menu_content_biz_start(inputdata) {
	BAB8DC_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "N01_menu_content$menu_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BAB8DC_get_N01_menu_content$menu_id");	
    /*biz end*/
}

/*biz step begin*/
function BAB8DC_format_menu_id(value, row, index) {    var objResult = value;    for(i = 0; i < BAB8DC_ary_menu_id.length; i++) {        var obj = BAB8DC_ary_menu_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BAB8DC_format_is_ava(value, row, index) {    var objResult = value;    for(i = 0; i < BAB8DC_ary_is_ava.length; i++) {        var obj = BAB8DC_ary_is_ava[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BAB8DC_format_role_id(value, row, index) {    var objResult = value;    for(i = 0; i < BAB8DC_ary_role_id.length; i++) {        var obj = BAB8DC_ary_role_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BAB8DC_get_N01_menu_content$menu_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_menu_content$menu_id) == false)        return false;    BAB8DC_ary_menu_id = input.N01_menu_content$menu_id;    if($("#BAB8DC_menu_id").is("select") && $("#BAB8DC_menu_id")[0].options.length == 0)    {        $.each(BAB8DC_ary_menu_id, function (i, obj) {            addOptionValue("BAB8DC_menu_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#BAB8DC_is_ava").is("select") && $("#BAB8DC_is_ava")[0].options.length == 0)    {        $.each(BAB8DC_ary_is_ava, function (i, obj) {            addOptionValue("BAB8DC_is_ava", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#BAB8DC_role_id").is("select") && $("#BAB8DC_role_id")[0].options.length == 0)    {        $.each(BAB8DC_ary_role_id, function (i, obj) {            addOptionValue("BAB8DC_role_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    BAB8DC_init_menu_content();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function BAB8DC_menu_id_cn_name_fun(){    index_subhtml = "menu_info.html"    random_subhtml = "XXXXXX";    if(loadHtmlSubVueFun("biz_vue/menu_info.html","BAB8DC_menu_content_call_vue") == true){        var n = Get_RandomDiv("XXXXXX","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#XXXXXX_menu_info.html_Events').bootstrapTable('resetView');                XXXXXX_param["ly_index"] = index;                XXXXXX_param["target_name"] = "BAB8DC_find_menu_id_cn_name"                XXXXXX_param["target_id"] = "BAB8DC_menu_id"                XXXXXX_param["sourc_id"] = "main_id"                XXXXXX_param["sourc_name"] = "menu_name"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function BAB8DC_page_end(){
	if(BAB8DC_param["type"] == "edit"){
		BAB8DC_get_edit_info();
	}
}

//页面初始化方法
function BAB8DC_init_menu_content() {
	//type = getUrlParam("type");
	if(BAB8DC_param["type"] == "add"){
		$("#BAB8DC_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BAB8DC_menu_id").val(BAB8DC_param["menu_id"]);        $("#BAB8DC_find_menu_id_cn_name").val(BAB8DC_param["menu_id_cn_name"]);		
		/*Get Find Select param end*/	
	}
	else if(BAB8DC_param["type"] == "edit"){
	
	}
	
	//表单验证
	BAB8DC_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
	
    /*form datetime init end*/
	
	BAB8DC_page_end();
}

//提交表单数据
function BAB8DC_SubmitForm(){
	if(BAB8DC_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_menu_content",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#BAB8DC_menu_id").val()                ,"param_value2": $("#BAB8DC_is_ava").val()                ,"param_value3": s_encode($("#BAB8DC_role_id").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BAB8DC_get_N01_ins_menu_content");
	}
	else if(BAB8DC_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_menu_content",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#BAB8DC_menu_id").val()                ,"param_value2": $("#BAB8DC_is_ava").val()                ,"param_value3": s_encode($("#BAB8DC_role_id").val())                ,"param_value4": $("#BAB8DC_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BAB8DC_get_N01_upd_menu_content");
	}
}

//vue回调
function BAB8DC_menu_content_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "menu_info.html"){        var n = Get_RandomDiv("XXXXXX",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":BAB8DC_type,                    "ly_index":index,                    "target_name":"BAB8DC_find_menu_id_cn_name",                    "target_id":"BAB8DC_find_menu_id",                    "sourc_id":"main_id",                    "sourc_name":"menu_name"                };                loadScript_hasparam("biz_vue/menu_info.html.js","XXXXXX_menu_info.html_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//for表单提交
$("#BAB8DC_save_menu_content_Edit").click(function () {
	$("form[name='BAB8DC_DataModal']").submit();
})

/*修改数据*/
function BAB8DC_get_N01_upd_menu_content(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_menu_content) == true)
	{
		swal("修改数据成功!", "", "success");
		AAB8DC_menu_content_query();
		BAB8DC_clear_validate();
		layer.close(BAB8DC_param["ly_index"]);
	}
}

/*添加数据*/
function BAB8DC_get_N01_ins_menu_content(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_menu_content) == true)
	{
		swal("添加数据成功!", "", "success");
		AAB8DC_menu_content_query();
		BAB8DC_clear_validate();
		layer.close(BAB8DC_param["ly_index"]);
	}
}

//取消编辑
$("#BAB8DC_cancel_menu_content_Edit").click(function () {
	layer.close(BAB8DC_param["ly_index"]);
	BAB8DC_clear_validate();
	$("[id^='BAB8DC_div']").hide();
})

//清除查找框
function BAB8DC_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BAB8DC_clear_validate(){
	$("#BAB8DC_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BAB8DC_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BAB8DC_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BAB8DC_clear_edit_info(){
	var inputs = $("#BAB8DC_DataModal").find('input');
	var selects = $("#BAB8DC_DataModal").find("select");
	var textareas = $("#BAB8DC_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
    BAB8DC_clear_input_cn_name('BAB8DC_find_menu_id_cn_name','BAB8DC_menu_id')	
	/*input validate clear end*/
	BAB8DC_init_menu_content();
}

//页面输入框赋值
function BAB8DC_get_edit_info(){
	var rowData = $("#AAB8DC_menu_content_Events").bootstrapTable('getData')[AAB8DC_select_menu_content_rowId];
	var inputs = $("#BAB8DC_DataModal").find('input');
	var selects = $("#BAB8DC_DataModal").find("select");
	var textareas = $("#BAB8DC_DataModal").find('textarea');
	$.each(selects, function (i, obj) {
		if(typeof(eval("AAB8DC_ary_"+obj.id.toString().replace("BAB8DC_",""))) == "object")
		{
			$.each(eval("AAB8DC_ary_"+obj.id.toString().replace("BAB8DC_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("BAB8DC_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("BAB8DC_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("AAB8DC_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#BAB8DC_DataModal").find('[id="'+"BAB8DC_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("BAB8DC_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("BAB8DC_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//form验证
function BAB8DC_checkFormInput() {
    BAB8DC_validate = $("#BAB8DC_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BAB8DC_main_id: {}            ,BAB8DC_menu_id: {}            ,BAB8DC_is_ava: {}            ,BAB8DC_role_id: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BAB8DC_main_id: {}            ,BAB8DC_menu_id: {}            ,BAB8DC_is_ava: {}            ,BAB8DC_role_id: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BAB8DC_SubmitForm();
        	return false;
        }
    })
}