var columns = [];
var columnData = [];
var param_name = 'D01_monitor_data_minu';
var iIndex = 0;
var iPageSize = 15;
var param_value1 = "";
var param_value2 = "";
var param_value3 = "";
var param_value4 = "";
var param_value5 = "";
var objU = "";

//{
//    title: '数据时间',
//    field: 'get_time',
//    sortable: true
//}, {
//    title: '烟气温度(度)',
//    field: 'val_01',
//    sortable: true
//}, {
//    title: '烟气压力(帕)',
//    field: 'val_02',
//    sortable: true
//}, {
//    title: '烟气流速(米/秒)',
//    field: 'val_03',
//    sortable: true
//}, {
//    title: '烟气含氧量(%)',
//    field: 'val_06',
//    sortable: true
//}, {
//    title: '烟气湿度(%)',
//    field: 'val_05',
//    sortable: true
//}, {
//    title: '废气流量(标况)(立方米/秒)',
//    field: 'val_07',
//    sortable: true
//}, {
//    title: '甲烷(毫克/立方米)',
//    field: 'val_21',
//    sortable: true
//}, {
//    title: '碳氢化合物(毫克/立方米)',
//    field: 'val_22',
//    sortable: true
//}, {
//    title: '非甲烷总烃(毫克/立方米)',
//    field: 'val_23',
//    sortable: true
//},
$(document).ready(function () {
	init_page();
    initTime();
    get_A01_YY6RM3();
});

function A01_RXOB6C(input){
	layer.close(ly_index);
    //查询失败
    if (Call_OpeResult(input.A01_RXOB6C) == false)
        return false;
}

//程序重启
function prog_reboot(){
	var inputdata = {
	        param_name: "A01_RXOB6C",
	        session_id: session_id,
			login_id: login_id,
	        param_value1:$("#cmdPwd").val(),
	        param_value2:"2"
	};
    ly_index = layer.load();
    get_ajax_baseurl(inputdata, "A01_RXOB6C");
}

//重启设备
function reboot(){
	var inputdata = {
	        param_name: "A01_RXOB6C",
	        session_id: session_id,
			login_id: login_id,
	        param_value1:$("#cmdPwd").val(),
	        param_value2:"1"
	};
    ly_index = layer.load();
    get_ajax_baseurl(inputdata, "A01_RXOB6C");
}

function importExcelDiv(){
	$("#importExcelDiv").modal();
}

function get_upload_file(objTag){
	var objValue = objTag.A01_UpLoadFile;
	if (Call_OpeResult(objValue) == true)
	{
		$.each(objValue, function (i, obj) {			
			if(obj[GetLowUpp("oldFileName")].indexOf("不符合上传文件格式") == -1)
			{
				v_file1 = obj[GetLowUpp("newFileName")];
				get_A01_G03KKL(v_file1);
			}
		});
	}
}

$("#importExcel").on('click', function () {
	ly_index = layer.load();	
	var formData = new FormData($("#updForm").eq(0)[0]);
	var param_v = "&login_id="+login_id;
	if($("#fileExcel").val() == "")
		swal("请选择正确附件!", "请选择正确附件", "warning");
	else
		get_ajax_upload(formData,param_v,"get_upload_file");
})

//有效因子结果
function A01_YY6RM3(input){
    layer.close(ly_index);
    //查询失败
    if (Call_QryResult(input.A01_YY6RM3) == false)
        return false;  
    $.each(input.A01_YY6RM3, function (i, obj) {
    	var objC = {};
    	objC["title"] = obj[GetLowUpp("element_name")];
    	objC["field"] = GetLowUpp(obj[GetLowUpp("element_val")]);
    	objC["sortable"] = true;
    	columns.push(objC);
    });
    query();
}

//获取有效因子
function get_A01_YY6RM3(){
	ly_index = layer.load();	
	 var inputdata = {
        "param_name": "A01_YY6RM3",
        "session_id": session_id,
        "login_id": login_id
    };
    ly_index = layer.load();
    get_ajax_baseurl(inputdata, "A01_YY6RM3");
}
//数据导入结果
function A01_G03KKL(input) {
    layer.close(ly_index);
	//操作失败
    if (Call_OpeResult(input.A01_G03KKL) == false)
        return false;
}

//数据导入
function get_A01_G03KKL(file_name) {
    ly_index = layer.load();
	var inputdata = {
		param_name: "A01_G03KKL",
        param_value1: file_name,
        param_value2: $("input[name='import_data_type']:checked").val(),
        session_id: session_id
    };
    ly_index = layer.load();
    get_ajax_baseurl(inputdata, "A01_G03KKL");
}

function exportExcel(){
	$('#exampleTableEvents').tableExport({
		//showExport: true,
		type: "excel",//[ 'csv', 'txt', 'sql', 'doc', 'excel', 'xlsx', 'pdf'],//导出文件类型，[ 'csv', 'txt', 'sql', 'doc', 'excel', 'xlsx', 'pdf']
        exportDataType: "all",//'basic':当前页的数据, 'all':全部的数据, 'selected':选中的数据
        fileName: "导出数据"
        //data: columnData,
    });
}

function initTable() {
    $("#exampleTableEvents").bootstrapTable({
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        striped: true,
        iconSize: "outline",
        toolbar: "#exampleTableEventsToolbar",
        columns: columns,
        data: columnData,
        pageNumber: 1,
        sidePagination: "client", //服务端处理分页
        pageSize: iPageSize, //每页的记录行数（*）
        pageList: [15, 30, 100, 1000, 2000], //可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        showExport: true, //是否显示导出按钮(此方法是自己写的目的是判断终端是电脑还是手机,电脑则返回true,手机返回falsee,手机不显示按钮)
        exportDataType: "all", //basic', 'all', 'selected'.
        exportTypes:['excel','xlsx'], //导出类型
        exportOptions:{
	        fileName: '数据导出', //文件名称设置
	        worksheetName: 'Sheet1', //表格工作区名称
	        tableName: '数据导出',
	        excelstyles: ['background-color', 'color', 'font-size', 'font-weight']
	    },
    });
}

function query() {
	ly_index = layer.load();
    $('#exampleTableEvents').bootstrapTable("removeAll");
    var param_value1 = $('#starttimeHour').val();
    var param_value2 = $('#endtimeHour').val();
    var param_value5 = $("input[name='data_type']:checked").val();
    var objU = bizzUrl + '&param_name=' + param_name + '&param_value1=' + param_value1 + '&param_value2='
        +param_value2 + '&param_value3=0&param_value4=' + 5000+"&param_value5="+param_value5;
    //iIndex
    $.ajax({
        type: 'POST',
        url: objU,
        dataType: 'json',
        success: function (datas) { //返回list数据并循环获取
            layer.close(ly_index);
            var data = eval('datas.' + param_name);
            for (var i = 0; i < data.length; i++) {
                var jsonObj = {
                    'GET_TIME': set_time_decode(data[i][GetLowUpp("get_time")])
                };
                for (var j = 1; j < columns.length; j++) {
                    var f = columns[j]["field"];
                    eval('jsonObj.' + f + ' =' + 'data[i].' + f);
                }
                columnData.push(jsonObj);
            }
            $('#exampleTableEvents').bootstrapTable('destroy');
            initTable();
        }
    });
}

function initTime() {
    $("#starttimeHour").val(new Date().Format('yyyy-MM-dd 00:00:00'));
    $("#endtimeHour").val(new Date().Format('yyyy-MM-dd 23:59:59'));
    $('#starttimeHour').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:ss',
        startView: 2,
        todayHighlight: 1, //今天高亮
        //initialDate:new Date(),
        autoclose: true,
        forceParse: 0, //设置为0，时间不会跳转1899，会显示当前时间。
        language: 'zh-CN', //显示中文
        minView: "day", //设置只显示到月份
        todayBtn: true //显示今日按钮
    });
    $('#endtimeHour').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:ss',
        startView: 2,
        todayHighlight: 1, //今天高亮
        //initialDate:new Date(),
        autoclose: true,
        forceParse: 0, //设置为0，时间不会跳转1899，会显示当前时间。
        language: 'zh-CN', //显示中文
        minView: "day", //设置只显示到月份
        todayBtn: true //显示今日按钮
    });
    $('#starttimeDay').datepicker({
        format: 'yyyy-mm-dd',
        startView: 0,
        maxViewMode: 0,
        minViewMode: 0,
        initialDate: new Date(),
        autoclose: true
    });

    $('#starttimeMonth').datepicker({
        format: 'yyyy-mm',
        startView: 1,
        maxViewMode: 1,
        minViewMode: 1,
        initialDate: '2019-01',
        autoclose: true
    });
}