#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.plugins.A01_3W3XHW.HJpacket import HJpacket
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.plugins.A01_IHGDW0 import A01_Para
from com.plugins.A01_IHGDW0.A01_Fun import A01_Fun
from com.plugins.A01_IHGDW0.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.comport.ComModBus import ComModBus
from com.zxy.z_debug import z_debug
class ReleasePacket(z_debug):
    strContinue      = "1"   
    strResult        = ""
    strcom_port          = ""
    def __init__(self):
        pass
    def init_start(self):
        if self.strResult.find("\\\\r\\\\n") != -1 and  self.strResult.find("\\\\r\\\\n") == len(self.strResult) - 4:
            strA = self.strResult.split("\\r\\n")
        elif self.strResult.find("\\r\\n") != -1 and  self.strResult.find("\\r\\n") != len(self.strResult) - 2:
            strA = self.strResult.split("\r\n")
        else:
            strA = self.strResult.split("\r\n")
        for temStrOT in strA:
            if temStrOT == "" :
                continue               
            for temStrAryT in temStrOT.split("##"):
                try:
                    temStrAryT = temStrAryT.replace("\r","").replace("\n","").replace("\\r","").replace("\\n","").strip()
                    if temStrAryT != "":
                        temStrAryT = "##"+temStrAryT
                        uL = UsAdmin_Log(Com_Para.ApplicationPath, Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") + ":"+temStrAryT,"NetWork")
                        uL.WriteLog()                        
                    if temStrAryT == "":
                        pass
                    elif temStrAryT.find("##") != 0 and len(temStrAryT) > 2:
                        uL = UsAdmin_Log(Com_Para.ApplicationPath, Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") + "接收数据包格式1错误##："+temStrAryT)
                        uL.WriteLog()
                    else:
                        temAry = temStrAryT.split("&&")
                        if len(temAry) != 3:
                            uL = UsAdmin_Log(Com_Para.ApplicationPath, Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") + "接收数据包格式2错误&&："+temStrAryT)
                            uL.WriteLog()
                        else:   
                            temCRCInit = temStrAryT.replace("\r","").replace("\n","")
                            if len(temCRCInit) > 10 :
                                temCRCInit = temCRCInit[6:len(temCRCInit) - 4]
                            initPacket = HJpacket()
                            initPacket.clear_value()       
                            temHJpacket = HJpacket()
                            temHJpacket.clear_value()
                            temHeadAry = temCRCInit.replace("##","").split(";")
                            for hn in temHeadAry:
                                if hn.split("=")[0].find("QN") == 4:
                                    temHJpacket.attQN = hn.split("=")[1]
                                    temHJpacket.attLength = hn.split("=")[0][0:4]
                                elif hn.split("=")[0] == "ST":
                                    temHJpacket.attST = hn.split("=")[1]
                                elif hn.split("=")[0] == "CN":
                                    temHJpacket.attCN = hn.split("=")[1]
                                elif hn.split("=")[0] == "PW":
                                    temHJpacket.attPW = hn.split("=")[1]
                                elif hn.split("=")[0] == "MN":
                                    temHJpacket.attMN = hn.split("=")[1]
                                elif hn.split("=")[0] == "Flag":
                                    temHJpacket.attFlag = hn.split("=")[1]
                            temHJpacket.attCP = temAry[1]
                            temHJpacket.attCRC = temAry[2]
                            temCRC = temHJpacket.CheckCRC16Z(temCRCInit).upper()
                            if temCRC != temHJpacket.attCRC:
                                uL = UsAdmin_Log(Com_Para.ApplicationPath, Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") + "接收数据包错误CRC校验失败：\r\n"+self.strResult)
                                uL.WriteLog()
                            elif temHJpacket.attPW != initPacket.attPW:
                                uL = UsAdmin_Log(Com_Para.ApplicationPath, Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") + "接收数据包错误PW不一致：\r\n"+self.strResult)
                                uL.WriteLog()
                            elif initPacket.attMN != temHJpacket.attMN :
                                uL = UsAdmin_Log(Com_Para.ApplicationPath, Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") + "接收数据包错误MN号不一致：\r\n"+self.strResult)
                                uL.WriteLog()
                            else:
                                self.ReleasePacket(temHJpacket)                            
                except Exception as e:
                    temLog = ""
                    if str(type(self)) == "<class 'type'>":
                        temLog = self.debug_info(self)+temStrAryT+"==>"+repr(e)
                    else:
                        temLog = self.debug_info()+temStrAryT+"==>"+repr(e)
                    uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
                    uL.WriteLog()
    def ReleasePacket(self,inputHJpacket):
        try:
            temCPAry = inputHJpacket.attCP.split(";")
            temHtCP = {}
            temHtDev = {}
            temInfoId = {}
            for cpinfo in temCPAry:
                if cpinfo.find(",") != -1:
                    iIndex = 0
                    for cpv in cpinfo.split(","):                    
                        if iIndex == 0:
                            Com_Fun.SetHashTable(temHtCP,cpv.split("=")[0],cpv.split("=")[1])
                        else:
                            if cpv.split("=")[0] == "InfoId":
                                Com_Fun.SetHashTable(temInfoId,cpv.split("=")[1],cpv.split("=")[1])
                            Com_Fun.SetHashTable(temHtDev,cpv.split("=")[0],cpv.split("=")[1])                        
                        iIndex = iIndex + 1
                elif len(cpinfo) > 1:
                    Com_Fun.SetHashTable(temHtCP,cpinfo.split("=")[0],cpinfo.split("=")[1])
            if inputHJpacket.attCN == "9011" or inputHJpacket.attCN == "9012":
                if len(temHtCP) > 2:
                    temcom_port = ""
                    temmonitor_data_model = A01_Fun.Get_monitor_data_sec_last()
                    temmonitor_flag_model = A01_Fun.Get_monitor_flag_sec_last() 
                    for temv_prot_element in A01_Para.ht_v_prot_element:
                        if temv_prot_element.attprot_name == "A01_3W3XHW":
                            temcom_port = temv_prot_element.attcom_port                                            
                            if hasattr(temmonitor_data_model, temv_prot_element.attelement_val.lower()):
                                temV = Com_Fun.GetHashTable(temHtCP,temv_prot_element.attelement_code+"-Rtd")
                                if temV != "":
                                    setattr(temmonitor_data_model, temv_prot_element.attelement_val.lower(),str(round(float(temV),4)))                       
                            if hasattr(temmonitor_flag_model, temv_prot_element.attelement_val.lower()):
                                setattr(temmonitor_flag_model, temv_prot_element.attelement_val.lower(),Com_Fun.GetHashTable(temHtDev,temv_prot_element.attelement_code+"-Flag"))                       
                    if A01_Para.attLock.acquire():
                        temmonitor_data_model.get_time = Com_Fun.GetTimeDef()
                        temmonitor_flag_model.get_time = Com_Fun.GetTimeDef()
                        A01_Para.allmonitor_data_min_last = temmonitor_data_model
                        A01_Para.allmonitor_flag_min_last = temmonitor_flag_model
                        Com_Fun.SetHashTable(A01_Para.ht_monitor_sec, temmonitor_data_model.get_time, temmonitor_data_model)
                        Com_Fun.SetHashTable(A01_Para.ht_monitor_flag_sec, temmonitor_flag_model.get_time, temmonitor_flag_model)
                        A01_Para.attLock.release()
                    self.SendB3_2(temcom_port)
            elif inputHJpacket.attCN == "2011" and inputHJpacket.attFlag == "5":
                temcom_port = ""
                temmonitor_data_model = A01_Fun.Get_monitor_data_sec_last()
                temmonitor_flag_model = A01_Fun.Get_monitor_flag_sec_last() 
                for temv_prot_element in A01_Para.ht_v_prot_element:
                    if temv_prot_element.attprot_name == "A01_3W3XHW":
                        temcom_port = temv_prot_element.attcom_port                                            
                        if hasattr(temmonitor_data_model, temv_prot_element.attelement_val.lower()):
                            setattr(temmonitor_data_model, temv_prot_element.attelement_val.lower(),Com_Fun.GetHashTable(temHtCP,temv_prot_element.attelement_code+"-Rtd"))                       
                        if hasattr(temmonitor_flag_model, temv_prot_element.attelement_val.lower()):
                            setattr(temmonitor_flag_model, temv_prot_element.attelement_val.lower(),Com_Fun.GetHashTable(temHtDev,temv_prot_element.attelement_code+"-Flag"))                       
                if A01_Para.attLock.acquire():
                    temmonitor_data_model.get_time = Com_Fun.GetTimeDef()
                    temmonitor_flag_model.get_time = Com_Fun.GetTimeDef()
                    A01_Para.allmonitor_data_min_last = temmonitor_data_model
                    A01_Para.allmonitor_flag_min_last = temmonitor_flag_model
                    A01_Para.attLock.release()
            elif inputHJpacket.attCN == "3020" and inputHJpacket.attFlag == "4": 
                pass
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+self.strResult+"==>"+repr(e)
            else:
                temLog = self.debug_info()+self.strResult+"==>"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
    def SendB3_2(self,temcom_port):
        temA01modbus = ComModBus()
        temHJpacket = HJpacket()
        temHJpacket.init_B_3_2()
        temSocketValue = temHJpacket.GetSendPack_CP()
        temA01modbus.get_data_com_value(temcom_port, temSocketValue)   