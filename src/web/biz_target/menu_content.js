//选择某一行
var select_menu_content_rowId = "";

/*定义查询条件变量*/
/*declare query param begin*/
var tem_menu_id = "-1";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var ary_menu_id = null;var ary_is_ava = [{'MAIN_ID': '1', 'CN_NAME': '启用'}, {'MAIN_ID': '0', 'CN_NAME': '禁用'}];var ary_role_id = [{'MAIN_ID': '1', 'CN_NAME': '开发者'}, {'MAIN_ID': '2', 'CN_NAME': '系统管理员'}, {'MAIN_ID': '3', 'CN_NAME': '运维人员'}, {'MAIN_ID': '4', 'cn_name': '普通用户'}];
/*declare select options end*/

//业务逻辑数据开始
function biz_start() {
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "T01_menu_content$menu_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "get_T01_menu_content$menu_id");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function format_menu_id(value, row, index) {    var objResult = value;    for(i = 0; i < ary_menu_id.length; i++) {        var obj = ary_menu_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function format_is_ava(value, row, index) {    var objResult = value;    for(i = 0; i < ary_is_ava.length; i++) {        var obj = ary_is_ava[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function format_role_id(value, row, index) {    var objResult = "";    for(i = 0; i < ary_role_id.length; i++) {        var obj = ary_role_id[i];
        var value_s = value.split(",");
        for(j = 0; j < value_s.length; j ++){	        if (obj[GetLowUpp("main_id")].toString() == value_s[j].toString()) {
	        	if(objResult == "")
	        		objResult += obj[GetLowUpp("cn_name")];
	        	else
	        		objResult += ","+obj[GetLowUpp("cn_name")];	        }
        }    }    return objResult;}function get_T01_menu_content$menu_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_menu_content$menu_id) == false)        return false;    ary_menu_id = input.T01_menu_content$menu_id;    init_menu_content();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function menu_id_cn_name_fun(){    layer.open({        type: 2,        area: ['1100px', '600px'],        fixed: false, //不固定        maxmin: true,        content: "menu_info.html?target_name=find_menu_id_cn_name&target_id=find_menu_id&sourc_id=main_id&sourc_name=menu_name",        success: function(layero, index){            //var body = layer.getChildFrame('body',index);            //var main_id = $("#find_menu_id").val();        }    });}
/*find qry fun end*/

/*页面结束*/
function page_end(){
}

//首页菜单显示列定义
var menu_content = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '菜单url',        field: 'MENU_ID',        sortable: true        ,formatter: format_menu_id    },    {        title: '是否启用',        field: 'IS_AVA',        sortable: true        ,formatter: format_is_ava    },    {        title: '角色',        field: 'ROLE_ID',        sortable: true        ,formatter: format_role_id    }	
	/*table column end*/
];

//页面初始化
function init_menu_content() {
	$(window).resize(function () {
		  $('#menu_content_Events').bootstrapTable('resetView');
	});
	//首页菜单查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#btn_menu_content_query').click();
}

//查询接口
function menu_content_query() {
    $('#menu_content_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "T01_sel_menu_content",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": tem_menu_id        ,"param_value2": tem_menu_id		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "get_T01_sel_menu_content");
}

//查询结果
function get_T01_sel_menu_content(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.T01_sel_menu_content) == false)
        return false;    
	var s_data = input.T01_sel_menu_content;
    $('#menu_content_Events').bootstrapTable('destroy');
    $("#menu_content_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#menu_content_Events').find("tr.changeColor").removeClass('changeColor');
                select_menu_content_rowId = "";
            }
            　　　　　else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#menu_content_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                select_menu_content_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(getUrlParam("target_name") != null)
			{
				/*查找框返回值*/
				parent.$("#"+getUrlParam("target_id")).val(eval("row."+getUrlParam("sourc_id").toUpperCase()));
				parent.$("#"+getUrlParam("target_name")).val(eval("row."+getUrlParam("sourc_name").toUpperCase()));			
				var index = parent.layer.getFrameIndex(window.name);
				parent.layer.close(index);
			}
		},
        toolbar: "#menu_content_Toolbar",
        columns: menu_content,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [30, 100, 500, 2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//查询按钮
$('#btn_menu_content_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    tem_menu_id = $("#find_menu_id").val();	
    /*set query param end*/
	menu_content_query();
})

//新增按钮
$("#btn_menu_content_add").click(function () {
	layer.open({
		type: 2,
        area: ['1100px', '600px'],
        fixed: false, //不固定
        maxmin: true,
        content: 'menu_content_$.html?type=add',
        success: function(layero, index){
			var body = layer.getChildFrame('body',index);
			var inputs = body.contents().find("#DataModal").find('input');
        	/*新增按钮页面传父类ID或其他信息*/
        	/*add param value begin*/
			
			$.each(inputs, function (i, obj) {
				if (obj.id.toUpperCase() == "find_menu_id_cn_name".toUpperCase()) {
					$(obj).val($("#find_menu_id_cn_name").val());
				}
				else if (("find_"+obj.id).toUpperCase() == "find_menu_id".toUpperCase()){
					$(obj).val($("#find_"+obj.id).val());
				}
			});        	
        	/*add param value end*/
        }
    });
})

//编辑按钮
$("#btn_menu_content_edit").click(function () {
	if (select_menu_content_rowId != "") {
		layer.open({
			type: 2,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: 'menu_content_$.html?type=edit',
			success: function(layero, index){
				var body = layer.getChildFrame('body',index);
				var rowData = $("#menu_content_Events").bootstrapTable('getData')[select_menu_content_rowId];
				var inputs = body.contents().find("#DataModal").find('input');
				var selects = body.contents().find("#DataModal").find("select");				
				$.each(selects, function (i, obj) {
					if(typeof(eval("ary_"+obj.id)) == "object")
					{
						$.each(eval("ary_"+obj.id), function (inx, obj_sub) {
							var div = "<option value='" + obj_sub[GetLowUpp("main_id")] + "'>" + obj_sub[GetLowUpp("cn_name")] + "</option>";
							$(obj).append(div);
						});
					}
				});
				
				Object.keys(rowData).forEach(function (key) {
					$.each(inputs, function (i, obj) {
						if (obj.id.toUpperCase() == key.toUpperCase()) {
							if(typeof(rowData[key]) == "string")
							{
								$(obj).val(s_decode(rowData[key]));
							}
							else
							{
								$(obj).val(rowData[key]);
							}
						}
						else if(obj.id.toUpperCase() == ("find_"+key+"_cn_name").toUpperCase()){
							$.each(eval("ary_"+key.toLowerCase()), function (jindex, obj2) {
								if(obj2[GetLowUpp("main_id")] == rowData[key]){
									body.contents().find("#DataModal").find('[id="'+("find_"+key.toLowerCase()+"_cn_name").toLowerCase()+'"]').val(obj2[GetLowUpp("cn_name")]);
								}
							});
						}
					});
					$.each(selects, function (i, obj) {
						if (obj.id.toUpperCase() == key.toUpperCase() && key.toUpperCase()== "ROLE_ID") {
						    var arr=rowData[key].split(',');
						    $(obj).selectpicker('val', arr);
						}
						else if (obj.id.toUpperCase() == key.toUpperCase()) {
							$(obj).val(rowData[key]);
						}
					});
				});
				$(body.contents().find('[data-id="role_id"]')[1]).hide();
			}
		});
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#btn_menu_content_delete').click(function () {
	//单行选择
	var rowData = $("#menu_content_Events").bootstrapTable('getData')[select_menu_content_rowId];
	//多行选择
	var rowDatas = sel_row_menu_content();
	if (select_menu_content_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "T01_del_menu_content",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "T01_del_menu_content");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "T01_del_menu_content",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "T01_del_menu_content");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function T01_del_menu_content(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.T01_del_menu_content) == true)
		menu_content_query();
}

//数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//清除 查找框
function clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function sel_row_menu_content(){
	//获得选中行
	var checkedbox= $("#menu_content_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}

$(document).ready(function () {
});

window.onpageshow = function (e) {
    if (e.persisted || (window.performance.navigation.type == 2)) {
        is_history_back = 1;
    } else {
        is_history_back = 0;
    }
    //页面初始化
    init_page();
};