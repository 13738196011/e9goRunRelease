//按钮事件新增或编辑
var B12594_type = "";
//其他页面传到本页面参数
var B12594_param = {};
//暂时没用
var B12594_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var B12594_ary_TABLE_TYPE = [{'MAIN_ID': '0', 'CN_NAME': '表'}, {'MAIN_ID': '1', 'CN_NAME': '视图'}];
/*declare select options end*/

//主从表传递参数
function B12594_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function B12594_t_single_table_biz_start(inputdata) {
	B12594_param = inputdata;
	layer.close(ly_index);
	B12594_param_set();
    /*biz begin*/
    $.each(B12594_ary_TABLE_TYPE, function (i, obj) {        addOptionValue("B12594_TABLE_TYPE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    B12594_init_t_single_table()	
    /*biz end*/
}

/*biz step begin*/
function B12594_format_TABLE_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < B12594_ary_TABLE_TYPE.length; i++) {        var obj = B12594_ary_TABLE_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function B12594_page_end(){
	if(B12594_param["type"] == "edit"){
		B12594_get_edit_info();
	}
}

//页面初始化方法
function B12594_init_t_single_table() {
	//type = getUrlParam("type");
	if(B12594_param["type"] == "add"){
		$("#B12594_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#B12594_TABLE_CN_NAME").val(B12594_param["TABLE_CN_NAME"]);        $("#B12594_TABLE_EN_NAME").val(B12594_param["TABLE_EN_NAME"]);        $("#B12594_TABLE_TYPE").val(B12594_param["TABLE_TYPE"]);		
		/*Get Find Select param end*/	
	}
	else if(B12594_param["type"] == "edit"){
	
	}
	
	//表单验证
	B12594_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B12594_CREATE_DATE").val() == "")    {        $("#B12594_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B12594_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B12594_page_end();
}

//提交表单数据
function B12594_SubmitForm(){
	if(B12594_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_single_table",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#B12594_TABLE_CN_NAME").val())                ,"param_value2": s_encode($("#B12594_TABLE_EN_NAME").val())                ,"param_value3": $("#B12594_CREATE_DATE").val()                ,"param_value4": s_encode($("#B12594_S_DESC").val())                ,"param_value5": $("#B12594_TABLE_TYPE").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B12594_get_N01_ins_t_single_table");
	}
	else if(B12594_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_single_table",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#B12594_TABLE_CN_NAME").val())                ,"param_value2": s_encode($("#B12594_TABLE_EN_NAME").val())                ,"param_value3": $("#B12594_CREATE_DATE").val()                ,"param_value4": s_encode($("#B12594_S_DESC").val())                ,"param_value5": $("#B12594_TABLE_TYPE").val()                ,"param_value6": $("#B12594_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B12594_get_N01_upd_t_single_table");
	}
}

//vue回调
function B12594_t_single_table_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#B12594_save_t_single_table_Edit").click(function () {
	$("form[name='B12594_DataModal']").submit();
})

/*修改数据*/
function B12594_get_N01_upd_t_single_table(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_single_table) == true)
	{
		swal("修改数据成功!", "", "success");
		A12594_t_single_table_query();
		B12594_clear_validate();
		layer.close(B12594_param["ly_index"]);
	}
}

/*添加数据*/
function B12594_get_N01_ins_t_single_table(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_single_table) == true)
	{
		swal("添加数据成功!", "", "success");
		A12594_t_single_table_query();
		B12594_clear_validate();
		layer.close(B12594_param["ly_index"]);
	}
}

//取消编辑
$("#B12594_cancel_t_single_table_Edit").click(function () {
	layer.close(B12594_param["ly_index"]);
	B12594_clear_validate();
	$("[id^='B12594_div']").hide();
})

//清除查找框
function B12594_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B12594_clear_validate(){
	$("#B12594_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B12594_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B12594_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B12594_clear_edit_info(){
	var inputs = $("#B12594_DataModal").find('input');
	var selects = $("#B12594_DataModal").find("select");
	var textareas = $("#B12594_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	B12594_init_t_single_table();
}

//页面输入框赋值
function B12594_get_edit_info(){
	var rowData = $("#A12594_t_single_table_Events").bootstrapTable('getData')[A12594_select_t_single_table_rowId];
	var inputs = $("#B12594_DataModal").find('input');
	var selects = $("#B12594_DataModal").find("select");
	var textareas = $("#B12594_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"A12594","B12594");
}

//form验证
function B12594_checkFormInput() {
    B12594_validate = $("#B12594_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B12594_main_id: {}            ,B12594_TABLE_CN_NAME: {}            ,B12594_TABLE_EN_NAME: {}            ,B12594_CREATE_DATE: {date: true,required : true,maxlength:19}            ,B12594_S_DESC: {}            ,B12594_TABLE_TYPE: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B12594_main_id: {}            ,B12594_TABLE_CN_NAME: {}            ,B12594_TABLE_EN_NAME: {}            ,B12594_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B12594_S_DESC: {}            ,B12594_TABLE_TYPE: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B12594_SubmitForm();
        	return false;
        }
    })
}