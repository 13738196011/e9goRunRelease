#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_station_info(object):
    attMAIN_ID = -1
    attSTATION_CODE = ""
    attSTATION_NAME = ""
    attSTATION_TYPE = ""
    attMN_CODE = ""
    attLONGITUDE = ""
    attLATITUDE = ""
    attS_STATE = 0
    attCREATE_DATE = None
    attS_DESC = ""
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")