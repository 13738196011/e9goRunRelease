var iTop = 0;
var ileft = 0;

//业务逻辑数据开始
function biz_start() {
    layer.close(ly_index);
    /*biz begin*/
    getWindowSize();
    init();
    $(window).bind("resize",
        function () {
        var width = $(".jqGrid_wrapper").width();
        $("#t_file_type_list").setGridWidth(width);
    });
    /*biz end*/
}

/*biz step begin*/
/*biz step end*/

/*页面结束*/
function page_end() {}

//文件附件类别显示列定义
var t_file_type = [
    /*table column begin*/
    {
        label: '主键',
        name: 'MAIN_ID',
        width: '30px',
        index: 'MAIN_ID',
        editable: false,
        key: true,
        readOnly: true,
        editrules: {
            required: true
        }
    }, {
        label: '附件类别名称',
        name: 'TYPE_NAME',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '是否启用 ',
        name: 'IS_AVA',
        width: '40px',
        editable: true,
        editrules: true,
        edittype: 'select',
        formatter: 'select',
        editoptions: {
            value: {
                1: '是',
                0: '否'
            },
            defaultValue: '1'
        },
        formatoptions: {
            value: {
                1: '是',
                0: '否'
            }
        }
    }, {
        label: '最大列',
        name: 'COL_LENGTH',
        width: '40px',
        editable: true,
        editrules: true,
        editoptions: {
            defaultValue: '40'
        }
    }, {
        label: '最大行',
        name: 'ROW_LENGTH',
        width: '40px',
        editable: true,
        editrules: true,
        editoptions: {
            defaultValue: '100'
        }
    }, {
        label: '系统时间',
        name: 'CREATE_DATE',
        width: '70px',
        editable: true,
        editrules: true,
        readOnly: true,
        formatter: function (value, row) {
            return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
        },
        editoptions: {
            defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')
        }
    }, {
        label: '备注',
        name: 'S_DESC',
        width: '70px',
        editable: true,
        editrules: true
    }
    /*table column end*/
];

function T01_sel_t_file_type(input) {
    //查询失败
    if (Call_QryResult(input.T01_sel_t_file_type) == false)
        return false;

    var total = $("#t_file_type_list").jqGrid('getGridParam', 'records');
    if (typeof(total) == "undefined") {
        $("#t_file_type_list").jqGrid({
            data: input.T01_sel_t_file_type,
            datatype: "local",
            height: "450",
            autowidth: true,
            shrinkToFit: true,
            rowNum: 10,
            rowList: [10, 20, 30],
            colModel: t_file_type,
            editurl: 'clientArray',
            altRows: true,
            pager: "#t_file_type_div",
            viewrecords: true,
            caption: "文件附件类别列表&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' id= 'data_reset' value='重置该类型数据'>",
            //rownumbers:true,
            gridview: true
        });

        $("#data_reset").click(function () {
            var rowid = $("#t_file_type_list").jqGrid("getGridParam", "selrow");
            var rowMAIN_ID = $('#t_file_type_list').jqGrid('getRowData', rowid).MAIN_ID;
            swal({
                title: "告警",
                text: "是否重置该类型数据，数据重置将无法恢复",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定",
                closeOnConfirm: false
            },
                function () {
                ly_index = layer.load();
                var inputdata = {
                    "param_name": "A01_data_reset",
                    "session_id": session_id,
                    "login_id": login_id
                    /*insert param begin*/
                    ,param_value1: rowMAIN_ID
                    /*insert param end*/
                };
                get_ajax_baseurl(inputdata, "A01_data_reset");
            });
        });

    //导航后面，增加增删改查按钮
    $("#t_file_type_list").jqGrid("navGrid", "#t_file_type_div", {
        edit: true,
        add: true,
        del: true,
        search: true,
        view: false,
        position: "left",
        cloneToTop: false
	    }, {
	        beforeShowForm: function () {
	            $('#MAIN_ID').attr('readOnly', true);
	            /*datetime format begin*/
	            laydate.render({
	                elem: '#CREATE_DATE',
	                type: 'datetime',
	            });
	            /*datetime format end*/
	        },
	        onclickSubmit: updateDbByMainId,
	        closeAfterEdit: true
	    }, {
	        beforeShowForm: function () {
	            /*datetime format begin*/
	            laydate.render({
	                elem: '#CREATE_DATE',
	                type: 'datetime',
	            });
	            /*datetime format end*/
	        },
	        onclickSubmit: insertDb,
	        closeAfterAdd: true
	    }, {
	        top: iTop,
	        left: ileft,
	        onclickSubmit: delDbByMainId
	    }, {
	        closeAfterSearch: true
	    }, {
	        height: 200,
	        reloadAfterSubmit: true
	    });
	}
	else {
	    $("#t_file_type_list").jqGrid("clearGridData");
	    $("#t_file_type_list").jqGrid('setGridParam', {
	        data: input.T01_sel_t_file_type
	    }).trigger("reloadGrid");
	}
	layer.close(ly_index);
}

//初始化
function init() {
    ly_index = layer.load();
    iTop = (winHeight - 300) / 2;
    ileft = (winWidth - 300) / 2;
    $.jgrid.defaults.styleUI = "Bootstrap";
    var inputdata = {
        "param_name": "T01_sel_t_file_type",
        "session_id": session_id,
        "login_id": login_id
    };
    get_ajax_baseurl(inputdata, "T01_sel_t_file_type");
}

$(document).ready(function () {
    //init_page();
});

function T01_ins_t_file_type(input) {
    layer.close(ly_index);
    if (Call_OpeResult(input.T01_ins_t_file_type) == true)
        init();
}

var insertDb = function () {
    ly_index = layer.load();
    var inputdata = {
        "param_name": "T01_ins_t_file_type",
        "session_id": session_id,
        "login_id": login_id
        /*insert param begin*/
    ,
        param_value1: $("#TYPE_NAME").val(),
        param_value2: $("#IS_AVA").val(),
        param_value3: $("#COL_LENGTH").val(),
        param_value4: $("#ROW_LENGTH").val(),
        param_value5: $("#CREATE_DATE").val(),
        param_value6: $("#S_DESC").val()
        /*insert param end*/
    };
    get_ajax_baseurl(inputdata, "T01_ins_t_file_type");
};

function T01_upd_t_file_type(input) {
    layer.close(ly_index);
    if (Call_OpeResult(input.T01_upd_t_file_type) == true)
        init();
}

var updateDbByMainId = function () {
    var rowid = $("#t_file_type_list").jqGrid("getGridParam", "selrow");
    var rowMAIN_ID = $('#t_file_type_list').jqGrid('getRowData', rowid).MAIN_ID;
    if (rowid.indexOf("jqg") != -1 || rowMAIN_ID.indexOf("jqg") != -1) {
        swal({
            title: "提示信息",
            text: "无法修改缓存记录，请选择正确修改!"
        });
        return false;
    }
    ly_index = layer.load();
    var inputdata = {
        "param_name": "T01_upd_t_file_type",
        "session_id": session_id,
        "login_id": login_id
        /*update param begin*/
    ,
        param_value1: $("#TYPE_NAME").val(),
        param_value2: $("#IS_AVA").val(),
        param_value3: $("#COL_LENGTH").val(),
        param_value4: $("#ROW_LENGTH").val(),
        param_value5: $("#CREATE_DATE").val(),
        param_value6: $("#S_DESC").val(),
        param_value7: rowMAIN_ID
        /*update param end*/
    };
    get_ajax_baseurl(inputdata, "T01_upd_t_file_type");
};

function T01_del_t_file_type(input) {
    layer.close(ly_index);
    if (Call_OpeResult(input.T01_del_t_file_type) == true)
        init();
}

var delDbByMainId = function () {
    var rowid = $("#t_file_type_list").jqGrid("getGridParam", "selrow");
    var rowMAIN_ID = $('#t_file_type_list').jqGrid('getRowData', rowid).MAIN_ID;
    if (rowid.indexOf("jqg") != -1 || rowMAIN_ID.indexOf("jqg") != -1) {
        swal({
            title: "提示信息",
            text: "无法删除缓存记录，请选择正确删除!"
        });
        return false;
    }
    ly_index = layer.load();
    var inputdata = {
        "param_name": "T01_del_t_file_type",
        "session_id": session_id,
        "login_id": login_id
        /*delete param begin*/
        ,param_value1: rowMAIN_ID
        /*delete param end*/
    };
    get_ajax_baseurl(inputdata, "T01_del_t_file_type");
};

$(document).ready(function () {
    //页面初始化
    //init_page();
});

window.onpageshow = function (e) {
    if (e.persisted || (window.performance.navigation.type == 2)) {
        is_history_back = 1;
    } else {
        is_history_back = 0;
    }
    //页面初始化
    init_page();
};