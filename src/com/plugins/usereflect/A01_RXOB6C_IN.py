#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.interfaceReflect.A01_A1B2C3 import A01_A1B2C3
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.comport.ComModBus import ComModBus
from com.plugins.A01_IHGDW0.A01_Fun import A01_Fun
from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.plugins.A01_IHGDW0 import A01_Para
from urllib.parse import unquote
import tempfile,datetime,subprocess,os, sys,binascii
from com.plugins.usereflect.Hex_Release import Hex_Release
from com.zxy.z_debug import z_debug
class A01_RXOB6C_IN(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = 1
    def __init__(self):
        pass
    def init_start(self):
        temOpd = Ope_DB_Cent()
        if self.param_value2 == "4":
            self.strResult = "{\""+self.param_name+"\": [{\""+Com_Fun.GetLowUpp("s_result")+"\": 1, \""+Com_Fun.GetLowUpp("error_desc")+"\": \"\",\""+Com_Fun.GetLowUpp("DateTime")+"\":\""+Com_Fun.GetTimeDef()+"\"}]}"
        elif self.param_value1 != "1qaz2wsxADMIN"+Com_Fun.GetTime("%Y%m%d%H"):
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"命令结果:调试密码错误,无法执行命令\"}]}"
        elif temOpd.Get_eova_user_rid(Com_Fun.getLogin_Id(self.session_id)) > 3:
            self.strResult = "{\""+self.param_name+"\": [{\""+Com_Fun.GetLowUpp("s_result")+"\": 0, \""+Com_Fun.GetLowUpp("error_desc")+"\": \"禁止操作，用户权限不足!\"}]}"
        elif self.param_value2 == "2":
            self.RestartPro()
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"1\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"执行命令成功\"}]}"  
        elif self.param_value2 == "1" and Com_Para.devsys == "linux":
            temValue = self.CmdExecLinux(self)
            self.strResult = "命令结果:\r\n"
            for temR in temValue:
                self.strResult = self.strResult+temR.decode(Com_Para.U_CODE)+"\r\n"
        elif self.param_value2 == "1" and Com_Para.devsys == "windows":
            temValue = self.CmdExecWin(self)
            self.strResult = "命令结果:\r\n"
            for temR in temValue:
                self.strResult = self.strResult+temR.decode(Com_Para.U_CODE)+"\r\n"
        elif self.param_value2 == "3":
            self.param_value3 = "echo \"123456\" | sudo -S rm -rf /etc/udev/rules.d/70-persistent-net.rules"
            temValue = self.CmdExecLinux(self)
            self.strResult = "命令结果:\r\n"
            for temR in temValue:
                self.strResult = self.strResult+temR.decode(Com_Para.U_CODE)+"\r\n"
            self.param_value3 = "reboot -f"
            temValue = self.CmdExecLinux(self)
        elif self.param_value2 == "5":
            temValue = ""
            if Com_Para.devsys == "linux":
                temValue = self.ChangeDateLinux(self)
            else:
                temValue = self.ChangeDateWin(self)
            if str(temValue) != "-1":
                self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"1\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"设置设备时间成功\"}]}"
            else:
                self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"设置设备时间失败，输入参数异常\"}]}"         
        elif self.param_value2 == "6":
            if Com_Para.devsys == "linux":
                self.param_value3 = "reboot -f"
                temValue = self.CmdExecLinux(self)
                self.strResult = "命令结果:\r\n"
                for temR in temValue:
                    self.strResult = self.strResult+temR.decode(Com_Para.U_CODE)+"\r\n"
            else:
                self.param_value3  = "shutdown -r"
                temValue = self.CmdExecWin(self)
                self.strResult = "命令结果:\r\n"
                for temR in temValue:
                    self.strResult = self.strResult+temR.decode(Com_Para.U_CODE)+"\r\n"
        elif self.param_value2 == "7":
            temprotocol = A01_Fun.Get_protocol("A01_77H4UY")
            temCom = None     
            temA01modbus = ComModBus()
            hex_r = Hex_Release()
            if temprotocol is not None:
                for temv_prot_element in A01_Para.ht_v_prot_element:       
                    if temv_prot_element.attprot_name == "A01_77H4UY": 
                        for temt_modbus_addr in A01_Para.ht_t_modbus_addr:
                            if str(temv_prot_element.attmain_id) == str(temt_modbus_addr.attelement_id) and str(temv_prot_element.attprot_id) == str(temt_modbus_addr.attprotocol_id):                         
                                temCom = temt_modbus_addr.attcom_port
                                hex_r.rec_byte = temA01modbus.get_data_com_hex(temCom,"0110120000010200015591")
                                hex_r.A01_77H4UY(1)
                                break
                    elif temv_prot_element.attprot_name == "A01_V47TK1": 
                        for temt_modbus_addr in A01_Para.ht_t_modbus_addr:
                            if str(temv_prot_element.attmain_id) == str(temt_modbus_addr.attelement_id) and str(temv_prot_element.attprot_id) == str(temt_modbus_addr.attprotocol_id):                         
                                temCom = temt_modbus_addr.attcom_port
                                temByte = temA01modbus.set_data_rtu(temCom,4001, 1, 1)
                                break
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\""+str(hex_r.s_result)+"\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\""+hex_r.error_desc+"\"}]}"   
        elif self.param_value2 == "8":
            temprotocol = A01_Fun.Get_protocol("A01_88H5UT")
            temCom = None     
            temA01modbus = ComModBus()
            hex_r = Hex_Release()
            if temprotocol is not None:
                for temv_prot_element in A01_Para.ht_v_prot_element:       
                    if temv_prot_element.attprot_name == "A01_88H5UT": 
                        for temt_modbus_addr in A01_Para.ht_t_modbus_addr:
                            if str(temv_prot_element.attmain_id) == str(temt_modbus_addr.attelement_id) and str(temv_prot_element.attprot_id) == str(temt_modbus_addr.attprotocol_id):                         
                                temCom = temt_modbus_addr.attcom_port
                                hex_r.rec_byte = temA01modbus.get_data_com_hex(temCom,"0110120000010200015591")
                                hex_r.A01_88H5UT(1)
                                break
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\""+str(hex_r.s_result)+"\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\""+hex_r.error_desc+"\"}]}"   
        elif self.param_value2 == "9":
            temprotocol = A01_Fun.Get_protocol("A01_99N4T3")
            temCom = None     
            temA01modbus = ComModBus()
            hex_r = Hex_Release()
            if temprotocol is not None:
                for temv_prot_element in A01_Para.ht_v_prot_element:       
                    if temv_prot_element.attprot_name == "A01_99N4T3": 
                        for temt_modbus_addr in A01_Para.ht_t_modbus_addr:
                            if str(temv_prot_element.attmain_id) == str(temt_modbus_addr.attelement_id) and str(temv_prot_element.attprot_id) == str(temt_modbus_addr.attprotocol_id):                         
                                temCom = temt_modbus_addr.attcom_port
                                hex_r.rec_byte = temA01modbus.get_data_com_hex(temCom,"0110000000020200010001EBAF")
                                hex_r.error_desc = "TP做样结果:"+str(hex_r.rec_byte)
                                break
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\""+str(hex_r.s_result)+"\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\""+hex_r.error_desc+"\"}]}"   
        elif self.param_value2 == "10":
            temprotocol = A01_Fun.Get_protocol("A01_99N4T3")
            temCom = None     
            temA01modbus = ComModBus()
            hex_r = Hex_Release()
            if temprotocol is not None:
                for temv_prot_element in A01_Para.ht_v_prot_element:       
                    if temv_prot_element.attprot_name == "A01_99N4T3": 
                        for temt_modbus_addr in A01_Para.ht_t_modbus_addr:
                            if str(temv_prot_element.attmain_id) == str(temt_modbus_addr.attelement_id) and str(temv_prot_element.attprot_id) == str(temt_modbus_addr.attprotocol_id):                         
                                temCom = temt_modbus_addr.attcom_port
                                hex_r.rec_byte = temA01modbus.get_data_com_hex(temCom,"0110000000020200010001EBAF")
                                hex_r.A01_88H5UT(1)
                                break
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\""+str(hex_r.s_result)+"\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\""+hex_r.error_desc+"\"}]}"   
        self.strContinue = "0"                 
    def ChangeDateWin(self):
        try:
            temDate = Com_Fun.GetDateInput('%Y-%m-%d', '%Y-%m-%d %H:%M:%S', unquote(unquote(self.param_value3.replace("+"," "),Com_Para.U_CODE)))
            temTime = Com_Fun.GetDateInput('%H:%M:%S', '%Y-%m-%d %H:%M:%S', unquote(unquote(self.param_value3.replace("+"," "),Com_Para.U_CODE)))
            temResult = os.system('date {} && time {}'.format(temDate,temTime))
        except:
            temResult = -1
        return temResult
    def ChangeDateLinux(self):
        try:            
            temCommand = unquote(unquote("date -s \""+self.param_value3.replace("+"," ").split(" ")[0]+"\"",Com_Para.U_CODE))
            temCommand = unquote(unquote("date -s \""+self.param_value3.replace("+"," ").split(" ")[1]+"\"",Com_Para.U_CODE))
            out_temp = tempfile.SpooledTemporaryFile(max_size=10*1000)
            fileno = out_temp.fileno()
            obj = subprocess.Popen(temCommand,stdout=fileno,stderr=fileno,shell=True)
            obj.wait()
            temCommand = unquote(unquote("hwclock -w",Com_Para.U_CODE))
            obj = subprocess.Popen(temCommand,stdout=fileno,stderr=fileno,shell=True)
            obj.wait()         
            out_temp.seek(0)
            temByt = out_temp.readlines()
            temResult = temByt[0].decode(Com_Para.U_CODE)
            if temResult == "" :
                temResult = "1"
        except Exception as e:
            temResult = "-1"        
        finally:
            if out_temp is not None:
                out_temp.close()
        return temResult
    def CmdExecLinux(self):
        out_temp = None
        try: 
            temCommand = unquote(unquote(self.param_value3,Com_Para.U_CODE))
            out_temp = tempfile.SpooledTemporaryFile(max_size=10*1000)
            fileno = out_temp.fileno()
            obj = subprocess.Popen(temCommand,stdout=fileno,stderr=fileno,shell=True)
            obj.wait()            
            out_temp.seek(0)
            temResult = out_temp.readlines()
        except Exception as e:
            temResult = repr(e)
        finally:
            if out_temp is not None:
                out_temp.close()
        return temResult
    def RestartPro(self):
        python = sys.executable
        os.execl(python,python,* sys.argv)
    def CmdExecWin(self):
        try:
            temCommand = unquote(unquote(self.param_value3,Com_Para.U_CODE))
            temResult = os.popen(temCommand).read()
        except Exception as e:
            temResult = repr(e)
        return temResult