#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.z_debug import z_debug
class testReflectInClass(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = "1"   
    def init_start(self):
        print("reflect input param_name:" + Com_Fun.NoNull(self.param_name))
        print("reflect input session_id:"+Com_Fun.NoNull(self.session_id))
        print("reflect input param_value1:" + Com_Fun.NoNull(self.param_value1))
        print("reflect input param_value2:" + Com_Fun.NoNull(self.param_value2))
    def __init__(self):
        pass