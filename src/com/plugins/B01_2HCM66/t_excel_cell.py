#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

class t_excel_cell(object):
    attrow_tag = ""
    attcolumn_tag = ""
    attcell_up = ""
    attcell_down = ""
    attcell_left = ""
    attcell_right = ""
    attcell_value = ""
    attt_excel_tag = None
    def __init__(self):
        pass  