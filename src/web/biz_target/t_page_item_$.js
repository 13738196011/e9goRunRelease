var type = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var ary_PAGE_ID = null;var ary_PROC_ID = null;var ary_ITEM_TYPE = [{'MAIN_ID': '1', 'CN_NAME': '表格模式'}, {'MAIN_ID': '2', 'CN_NAME': '列表模式'}];
/*declare select options end*/

//业务逻辑数据开始
function biz_start() {
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "T01_t_page_item$PAGE_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "get_T01_t_page_item$PAGE_ID");	
    /*biz end*/
}

/*biz step begin*/
function format_PAGE_ID(value, row, index) {    var objResult = value;    for(i = 0; i < ary_PAGE_ID.length; i++) {        var obj = ary_PAGE_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function format_PROC_ID(value, row, index) {    var objResult = value;    for(i = 0; i < ary_PROC_ID.length; i++) {        var obj = ary_PROC_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function format_ITEM_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < ary_ITEM_TYPE.length; i++) {        var obj = ary_ITEM_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function get_T01_t_page_item$PAGE_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_t_page_item$PAGE_ID) == false)        return false;    ary_PAGE_ID = input.T01_t_page_item$PAGE_ID;    if($("#PAGE_ID").is("select") && $("#PAGE_ID")[0].options.length == 0)    {        $.each(ary_PAGE_ID, function (i, obj) {            addOptionValue("PAGE_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    var inputdata = {        "param_name": "N01_t_proc_inparam$PROC_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "get_N01_t_proc_inparam$PROC_ID");}function get_N01_t_proc_inparam$PROC_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_inparam$PROC_ID) == false)        return false;    ary_PROC_ID = input.N01_t_proc_inparam$PROC_ID;    if($("#PROC_ID").is("select") && $("#PROC_ID")[0].options.length == 0)    {        $.each(ary_PROC_ID, function (i, obj) {            addOptionValue("PROC_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#ITEM_TYPE").is("select") && $("#ITEM_TYPE")[0].options.length == 0)    {        $.each(ary_ITEM_TYPE, function (i, obj) {            addOptionValue("ITEM_TYPE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    init_t_page_item();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function PROC_ID_cn_name_fun(){    layer.open({        type: 2,        area: ['1000px', '570px'],        fixed: false, //不固定        maxmin: true,        content: "t_proc_name.vue?target_name=find_PROC_ID_cn_name&target_id=PROC_ID&sourc_id=MAIN_ID&sourc_name=INF_CN_NAME",        success: function(layero, index){            //var body = layer.getChildFrame('body',index);            //var main_id = $("#find_PROC_ID").val();        }    });}
/*find qry fun end*/

/*页面结束*/
function page_end(){
}


//页面初始化方法
function init_t_page_item() {
	type = getUrlParam("type");
	if(type == "add"){
		$("#main_id").parent().parent().parent().hide();
	}
	else if(type == "edit"){
	
	}
	
	//表单验证
	checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#CREATE_DATE").val() == "")    {        $("#CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
}

//提交表单数据
function SubmitForm(){
	if(type == "add"){
		var inputdata = {
				"param_name": "T01_ins_t_page_item",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#PAGE_ID").val()                ,"param_value2": $("#PROC_ID").val()                ,"param_value3": s_encode($("#ITEM_NAME").val())                ,"param_value4": $("#ITEM_TYPE").val()                ,"param_value5": $("#COLUMN_COUNT").val()                ,"param_value6": $("#CREATE_DATE").val()                ,"param_value7": s_encode($("#S_DESC").val())                ,"param_value8": s_encode($("#URL_IN_PARAM").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_ins_t_page_item");
	}
	else if(type == "edit"){
		var inputdata = {
				"param_name": "T01_upd_t_page_item",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#PAGE_ID").val()                ,"param_value2": $("#PROC_ID").val()                ,"param_value3": s_encode($("#ITEM_NAME").val())                ,"param_value4": $("#ITEM_TYPE").val()                ,"param_value5": $("#COLUMN_COUNT").val()                ,"param_value6": $("#CREATE_DATE").val()                ,"param_value7": s_encode($("#S_DESC").val())                ,"param_value8": s_encode($("#URL_IN_PARAM").val())                ,"param_value9": $("#main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_upd_t_page_item");
	}
}

//以下为页面表单验证
$("#save_t_page_item_Edit").click(function () {
	$("form[name='DataModal']").submit();
})

/*修改数据*/
function get_T01_upd_t_page_item(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_upd_t_page_item) == true)
	{
        //init();
		parent.swal("修改数据成功!", "", "success");
		parent.t_page_item_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

/*添加数据*/
function get_T01_ins_t_page_item(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_ins_t_page_item) == true)
	{
		parent.swal("添加数据成功!", "", "success");
		parent.t_page_item_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

//取消编辑
$("#cancel_t_page_item_Edit").click(function () {
	var index = parent.layer.getFrameIndex(window.name);
	parent.layer.close(index);
})

function clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

$(document).ready(function () {
});

window.onpageshow = function (e) {
    if (e.persisted || (window.performance.navigation.type == 2)) {
        is_history_back = 1;
    } else {
        is_history_back = 0;
    }
    //页面初始化
    init_page();
};

//form验证
function checkFormInput() {
    $("#DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            main_id: {}            ,PAGE_ID: {}            ,PROC_ID: {}            ,ITEM_NAME: {}            ,ITEM_TYPE: {}            ,COLUMN_COUNT: {digits: true,required : true,maxlength:10}            ,CREATE_DATE: {date: true,required : true,maxlength:19}            ,S_DESC: {}            ,URL_IN_PARAM: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            main_id: {}            ,PAGE_ID: {}            ,PROC_ID: {}            ,ITEM_NAME: {}            ,ITEM_TYPE: {}            ,COLUMN_COUNT: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,S_DESC: {}            ,URL_IN_PARAM: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	SubmitForm();
        	return false;
        }
    })
}