//选择某一行
var A72A1E_select_t_com_port_rowId = "";
//按钮事件新增或编辑
var A72A1E_type = "";
//其他页面传到本页面参数
var A72A1E_param = {};

/*定义查询条件变量*/
/*declare query param begin*/

/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

//业务逻辑数据开始
function A72A1E_t_com_port_biz_start(inputparam) {
	layer.close(ly_index);
	A72A1E_param = inputparam;
    /*biz begin*/
    A72A1E_init_t_com_port()	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function A72A1E_page_end(){
}

//串口设置显示列定义
var A72A1E_t_com_port = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '串口名称',        field: 'COM_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '波特率',        field: 'COM_BOT',        sortable: true    },    {        title: '数据位',        field: 'COM_DATA',        sortable: true    },    {        title: '奇偶校验',        field: 'COM_CT',        sortable: true    },    {        title: '英文标签',        field: 'COM_EFLAG',        sortable: true        ,formatter: set_s_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    }	
	/*table column end*/
];

//页面初始化
function A72A1E_init_t_com_port() {
	$(window).resize(function () {
		  $('#A72A1E_t_com_port_Events').bootstrapTable('resetView');
	});
	//串口设置查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#A72A1E_btn_t_com_port_query').click();
}

//查询接口
function A72A1E_t_com_port_query() {
    $('#A72A1E_t_com_port_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_com_port",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "A72A1E_get_N01_sel_t_com_port");
}

//查询结果
function A72A1E_get_N01_sel_t_com_port(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_com_port) == false)
        return false;    
	var s_data = input.N01_sel_t_com_port;
    A72A1E_select_t_com_port_rowId = "";
    $('#A72A1E_t_com_port_Events').bootstrapTable('destroy');
    $("#A72A1E_t_com_port_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#A72A1E_t_com_port_Events').find("tr.changeColor").removeClass('changeColor');
                A72A1E_select_t_com_port_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#A72A1E_t_com_port_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                A72A1E_select_t_com_port_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(A72A1E_param.hasOwnProperty("target_name"))
			{
				$("#"+A72A1E_param["target_id"]).val(eval("row."+A72A1E_param["sourc_id"].toUpperCase()));
				$("#"+A72A1E_param["target_name"]).val(s_decode(eval("row."+A72A1E_param["sourc_name"].toUpperCase())));				
				layer.close(A72A1E_param["ly_index"]);
			}
		},
        toolbar: "#A72A1E_t_com_port_Toolbar",
        columns: A72A1E_t_com_port,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#A72A1E_btn_t_com_port_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/

	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/

	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    A72A1E_init_t_com_port()	
    /*biz end*/
})

//查询按钮
$('#A72A1E_btn_t_com_port_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
	
    /*set query param end*/
	A72A1E_t_com_port_query();
})

//vue回调
function A72A1E_t_com_port_call_vue(objResult){
	if(index_subhtml == "t_com_port_$.vue")
	{
		var n = Get_RandomDiv("B72A1E",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":A72A1E_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_com_port_$.js","B72A1E_t_com_port_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//新增按钮
$("#A72A1E_btn_t_com_port_add").click(function () {
	A72A1E_type = "add";
	index_subhtml = "t_com_port_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_com_port_$.vue","A72A1E_t_com_port_call_vue") == true){
		var n = Get_RandomDiv("B72A1E","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				B72A1E_param["type"] = A72A1E_type;
				B72A1E_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
				
				/*Send Two FindSelect param end*/

				B72A1E_clear_edit_info();
			},
			end: function(){
				B72A1E_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#A72A1E_btn_t_com_port_edit").click(function () {
	if (A72A1E_select_t_com_port_rowId != "") {
		A72A1E_type = "edit";
		index_subhtml = "t_com_port_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_com_port_$.vue","A72A1E_t_com_port_call_vue") == true){
			var n = Get_RandomDiv("B72A1E","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					B72A1E_param["type"] = A72A1E_type;
					B72A1E_param["ly_index"] = index;
					B72A1E_clear_edit_info();
					B72A1E_get_edit_info();
				},
				end: function(){
					B72A1E_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#A72A1E_btn_t_com_port_delete').click(function () {
	//单行选择
	var rowData = $("#A72A1E_t_com_port_Events").bootstrapTable('getData')[A72A1E_select_t_com_port_rowId];
	//多行选择
	var rowDatas = A72A1E_sel_row_t_com_port();
	if (A72A1E_select_t_com_port_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_com_port",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "A72A1E_N01_del_t_com_port");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_com_port",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "A72A1E_N01_del_t_com_port");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function A72A1E_N01_del_t_com_port(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_com_port) == true)
		A72A1E_t_com_port_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function A72A1E_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function A72A1E_sel_row_t_com_port(){
	//获得选中行
	var checkedbox= $("#A72A1E_t_com_port_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}