#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_sub_userpower(object):
    attMAIN_ID = -1
    attSUB_USER_ID = 0
    attPROC_ID = 0
    attLIMIT_DATE = None
    attUSE_LIMIT = 0
    attLIMIT_NUMBER = 0
    attLIMIT_TAG = 0
    attFIRST_DATE = None
    attS_DESC = ""
    attCREATE_DATE = None
    lenS_DESC = 500
    def __init__(self):
        self.attLIMIT_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        self.attFIRST_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["MAIN_ID"] = "主键"
        temHt["SUB_USER_ID"] = "平台用户信息"
        temHt["PROC_ID"] = "接口名称"
        temHt["LIMIT_DATE"] = "使用有效期"
        temHt["USE_LIMIT"] = "接口调用次数"
        temHt["LIMIT_NUMBER"] = "调用次数限制"
        temHt["LIMIT_TAG"] = "次数限制类型"
        temHt["FIRST_DATE"] = "初次使用时间"
        temHt["S_DESC"] = "备注"
        temHt["CREATE_DATE"] = "系统时间"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["SUB_USER_ID"] = "平台用户信息"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["SUB_USER_ID"] = "select MAIN_ID,SUB_USERNAME || SUB_USERCODE as CN_NAME from t_sub_user"
        temHtSelect["PROC_ID"] = "N01_t_proc_inparam$PROC_ID"
        temHtSelect["LIMIT_TAG"] = [{"MAIN_ID":"1","CN_NAME":"年"},{"MAIN_ID":"2","CN_NAME":"月"},{"MAIN_ID":"3","CN_NAME":"日"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        temHtFind["PROC_ID"] = "t_proc_name.vue"
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        temHtFReturn["PROC_ID"] = ["MAIN_ID","INF_CN_NAME"]
        return temHtFReturn
