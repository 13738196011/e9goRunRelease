#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_sub_user(object):
    attmain_id = -1
    attSUB_ID = 0
    attSUB_USERNAME = ""
    attSUB_USERCODE = ""
    attLIMIT_DATE = None
    attS_DESC = ""
    attCREATE_DATE = None
    lenSUB_USERNAME = 50
    lenSUB_USERCODE = 50
    lenS_DESC = 500
    def __init__(self):
        self.attLIMIT_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["SUB_ID"] = "应用平台信息"
        temHt["SUB_USERNAME"] = "用户名称"
        temHt["SUB_USERCODE"] = "用户授权码"
        temHt["LIMIT_DATE"] = "有效期限"
        temHt["S_DESC"] = "备注"
        temHt["CREATE_DATE"] = "系统时间"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["SUB_ID"] = "N01_t_sub_power$SUB_ID"
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
