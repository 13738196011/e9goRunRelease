//按钮事件新增或编辑
var BF0359_type = "";
//其他页面传到本页面参数
var BF0359_param = {};
//暂时没用
var BF0359_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BF0359_ary_MENU_ID = null;var BF0359_ary_IS_AVA = [{'MAIN_ID': '1', 'CN_NAME': '启用'}, {'MAIN_ID': '0', 'CN_NAME': '禁用'}];var BF0359_ary_ROLE_ID = [{'MAIN_ID': '1', 'CN_NAME': '开发者'}, {'MAIN_ID': '2', 'CN_NAME': '系统管理员'}, {'MAIN_ID': '3', 'CN_NAME': '运维人员'}, {'MAIN_ID': '4', 'CN_NAME': '普通用户'}];
/*declare select options end*/

//主从表传递参数
function BF0359_param_set(){
	/*Main Subsuv Table Param Begin*/
    if(BF0359_param.hasOwnProperty("MENU_ID_cn_name"))        $("#BF0359_find_MENU_ID_cn_name").val(s_decode(BF0359_param["MENU_ID_cn_name"]));    if(BF0359_param.hasOwnProperty("MENU_ID"))        $("#BF0359_find_MENU_ID").val(BF0359_param["MENU_ID"]);    if(BF0359_param.hasOwnProperty("hidden_find")){        $("#BF0359_Ope_MENU_ID").hide();        $("#BF0359_Clear_MENU_ID").hide();    }	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function BF0359_menu_content_biz_start(inputdata) {
	BF0359_param = inputdata;
	layer.close(ly_index);
	BF0359_param_set();
    /*biz begin*/
    var inputdata = {        "param_name": "N01_menu_content$MENU_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BF0359_get_N01_menu_content$MENU_ID");	
    /*biz end*/
}

/*biz step begin*/
function BF0359_format_MENU_ID(value, row, index) {    var objResult = value;    for(i = 0; i < BF0359_ary_MENU_ID.length; i++) {        var obj = BF0359_ary_MENU_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BF0359_format_IS_AVA(value, row, index) {    var objResult = value;    for(i = 0; i < BF0359_ary_IS_AVA.length; i++) {        var obj = BF0359_ary_IS_AVA[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BF0359_format_ROLE_ID(value, row, index) {    var objResult = value;    for(i = 0; i < BF0359_ary_ROLE_ID.length; i++) {        var obj = BF0359_ary_ROLE_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BF0359_get_N01_menu_content$MENU_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_menu_content$MENU_ID) == false)        return false;    BF0359_ary_MENU_ID = input.N01_menu_content$MENU_ID;    if($("#BF0359_MENU_ID").is("select") && $("#BF0359_MENU_ID")[0].options.length == 0)    {        $.each(BF0359_ary_MENU_ID, function (i, obj) {            addOptionValue("BF0359_MENU_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#BF0359_IS_AVA").is("select") && $("#BF0359_IS_AVA")[0].options.length == 0)    {        $.each(BF0359_ary_IS_AVA, function (i, obj) {            addOptionValue("BF0359_IS_AVA", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#BF0359_ROLE_ID").is("select") && $("#BF0359_ROLE_ID")[0].options.length == 0)    {        $.each(BF0359_ary_ROLE_ID, function (i, obj) {            addOptionValue("BF0359_ROLE_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }
	    BF0359_init_menu_content();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function BF0359_MENU_ID_cn_name_fun(){    index_subhtml = "menu_info.vue"    random_subhtml = "A77565";    if(loadHtmlSubVueFun("dev_vue/menu_info.vue","BF0359_menu_content_call_vue") == true){        var n = Get_RandomDiv("A77565","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#A77565_menu_info_Events').bootstrapTable('resetView');                A77565_param["ly_index"] = index;                A77565_param["target_name"] = "BF0359_find_MENU_ID_cn_name"                A77565_param["target_id"] = "BF0359_MENU_ID"                A77565_param["sourc_id"] = "MAIN_ID"                A77565_param["sourc_name"] = "MENU_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function BF0359_page_end(){
	$('#BF0359_ROLE_ID').selectpicker('refresh'); 
	$('#BF0359_ROLE_ID').selectpicker('show');
	if(BF0359_param["type"] == "edit"){
		BF0359_get_edit_info();
	}
}

//页面初始化方法
function BF0359_init_menu_content() {
	//type = getUrlParam("type");
	if(BF0359_param["type"] == "add"){
		$("#BF0359_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BF0359_MENU_ID").val(BF0359_param["MENU_ID"]);        $("#BF0359_find_MENU_ID_cn_name").val(BF0359_param["MENU_ID_cn_name"]);		
		/*Get Find Select param end*/	
	}
	else if(BF0359_param["type"] == "edit"){
	
	}
	
	//表单验证
	BF0359_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
	
    /*form datetime init end*/
	
	BF0359_page_end();
}

//提交表单数据
function BF0359_SubmitForm(){
	if(BF0359_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_menu_content",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#BF0359_MENU_ID").val()                ,"param_value2": $("#BF0359_IS_AVA").val()                ,"param_value3": s_encode($("#BF0359_ROLE_ID").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BF0359_get_N01_ins_menu_content");
	}
	else if(BF0359_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_menu_content",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#BF0359_MENU_ID").val()                ,"param_value2": $("#BF0359_IS_AVA").val()                ,"param_value3": s_encode($("#BF0359_ROLE_ID").val())                ,"param_value4": $("#BF0359_MAIN_ID").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BF0359_get_N01_upd_menu_content");
	}
}

//vue回调
function BF0359_menu_content_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "menu_info.vue"){        var n = Get_RandomDiv("A77565",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":BF0359_type,                    "ly_index":index,                    "target_name":"BF0359_find_MENU_ID_cn_name",                    "target_id":"BF0359_find_MENU_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"MENU_NAME"                };                loadScript_hasparam("dev_vue/menu_info.js","A77565_menu_info_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//for表单提交
$("#BF0359_save_menu_content_Edit").click(function () {
	$("form[name='BF0359_DataModal']").submit();
})

/*修改数据*/
function BF0359_get_N01_upd_menu_content(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_menu_content) == true)
	{
		swal("修改数据成功!", "", "success");
		AF0359_menu_content_query();
		BF0359_clear_validate();
		layer.close(BF0359_param["ly_index"]);
	}
}

/*添加数据*/
function BF0359_get_N01_ins_menu_content(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_menu_content) == true)
	{
		swal("添加数据成功!", "", "success");
		AF0359_menu_content_query();
		BF0359_clear_validate();
		layer.close(BF0359_param["ly_index"]);
	}
}

//取消编辑
$("#BF0359_cancel_menu_content_Edit").click(function () {
	layer.close(BF0359_param["ly_index"]);
	BF0359_clear_validate();
	$("[id^='BF0359_div']").hide();
})

//清除查找框
function BF0359_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BF0359_clear_validate(){
	$("#BF0359_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BF0359_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BF0359_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BF0359_clear_edit_info(){
	var inputs = $("#BF0359_DataModal").find('input');
	var selects = $("#BF0359_DataModal").find("select");
	var textareas = $("#BF0359_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
    BF0359_clear_input_cn_name('BF0359_find_MENU_ID_cn_name','BF0359_MENU_ID')	
	/*input validate clear end*/
	BF0359_init_menu_content();
}

//页面输入框赋值
function BF0359_get_edit_info(){	
	$('#BF0359_ROLE_ID').selectpicker('refresh'); 
	$('#BF0359_ROLE_ID').selectpicker('show');	
	var rowData = $("#AF0359_menu_content_Events").bootstrapTable('getData')[AF0359_select_menu_content_rowId];
	var inputs = $("#BF0359_DataModal").find('input');
	var selects = $("#BF0359_DataModal").find("select");
	var textareas = $("#BF0359_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"AF0359","BF0359");
	
	//多选框赋值
	$.each(selects, function (i, obj) {
		if (obj.id.toUpperCase() == "BF0359_ROLE_ID") {
			var arr = s_decode(rowData["ROLE_ID"]).split(',');
			$(obj).selectpicker('val', arr);
		}
	});
}

//form验证
function BF0359_checkFormInput() {
    BF0359_validate = $("#BF0359_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BF0359_main_id: {}            ,BF0359_MENU_ID: {}            ,BF0359_IS_AVA: {}            ,BF0359_ROLE_ID: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BF0359_main_id: {}            ,BF0359_MENU_ID: {}            ,BF0359_IS_AVA: {}            ,BF0359_ROLE_ID: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BF0359_SubmitForm();
        	return false;
        }
    })
}