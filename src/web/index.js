//新添加子页面的div名字
var divm = "";	  
//触发新页面动作
var index_subhtml = "";
//新页面随机码
var random_subhtml = "";
//回调函数
var call_vue_funcName = "";

(function($) {
	$.each(['show','hide'], function(i, val) {
        var _org = $.fn[val];
        $.fn[val] = function() {
            this.trigger(val);
            return _org.apply(this, arguments);
        };
    });
})(jQuery);


//仅获取子页div不创建
function Get_RDivNoBuild(temRandom,objResult){
	var divIndex = "";
	$.each($(".J_menuItem"), function (i, obj) {
		if($(obj).attr("attrandom") == temRandom){
			divIndex = temRandom +"_div"+$(obj).attr("data-index");
		}
	});
	if(divIndex == "")
		divIndex = temRandom +"_div";
	return "#"+divIndex;
}

//获取子页div，没有则创建并返回
function Get_RandomDiv(temRandom,objResult){
	var divIndex = "";
	$.each($(".J_menuItem"), function (i, obj) {
		if($(obj).attr("attrandom") == temRandom){
			divIndex = temRandom +"_div"+$(obj).attr("data-index");
		}
	});
	if(divIndex == "")
		divIndex = temRandom +"_div";
	if($("#"+divIndex).length > 0)
	{
		return "#"+divIndex;
	}
	else
	{
		var nn = "<div id='" + divIndex + "' name='" + divIndex + "' data-id=''></div>";
		$(".J_mainContent").append(nn);
		$("#"+divIndex).append(objResult);
		
		return "#"+divIndex; 
	}
}

//业务逻辑数据开始
function biz_start() {
	layer.close(ly_index);
    /*biz begin*/
	
	index_biz_start();	
    /*biz end*/
}

function index_biz_start(){
	//接口调试
	$("#upd_pwd").click(function(){
		index_subhtml = "upd_pwd.vue";
		if(loadHtmlSubVue(index_subhtml) == true){
			B44HT5_clear_input();
			layer.open({
				type:1,
				area: ['600px', '350px'],
				fixed: false, //不固定
				maxmin: true,
				content: $("#B44HT5_div"),
				success: function(layero, index){
					B44HT5_ly_index = index;
				},
				end: function(){
				}
			});
		}
	});
	$("#user_info").html("<i class='fa fa-user'></i>欢迎您："+nickname);
	//菜单加载
	set_menu_info();
}

//获取菜单及相关信息
function set_menu_info(){
	layer.close(ly_index);
    /*biz begin*/

    var inputdata = {
        "param_name": "APP_USER_ZXY",
        "session_id": session_id,
        "login_id": login_id,
        "param_value1":"16D0D0BD01343BD0AD6781ED5C671C8D"
    };
    ly_index = layer.load();
    get_ajax_baseurl(inputdata, "get_APP_USER_ZXY");	
}

//设置菜单及相关信息
function get_APP_USER_ZXY(input){
	layer.close(ly_index);
	//查询失败
	if (Call_QryResult(input.APP_USER_ZXY) == false)
		return false;
	var auz = input.APP_USER_ZXY;
	var menu_info = input.T01_JK7788;
	var menu_group = input.T01_JKTFSA;
	var menu_content = input.T01_JK3344;
	var menu_ccode = input.T01_KT33V5;
	$.each(menu_group, function (i, obj) {
		var divGroup = "<li class=\"active\" id=\""+obj[GetLowUpp("GROUP_EN_NAME")]+"\">"
				 +"		<a href=\"#\">"
				 +"			<i class=\""+s_decode(obj[GetLowUpp("GROUP_ICON")])+"\"></i>"
				 +"			<span class=\"nav-label\">"+s_decode(obj[GetLowUpp("GROUP_CN_NAME")])+"</span>"
				 +"			<span class=\"fa arrow\"></span>"
				 +"		</a>"
				 +"		<ul class=\"nav nav-second-level\" id =\""+obj[GetLowUpp("GROUP_EN_NAME")]+"_menu\">"
				 +"		</ul>"
				 +"</li>"
		$("#side-menu").append(divGroup);
		$.each(menu_info, function (j, objInfo) {
		if(obj[GetLowUpp("MAIN_ID")].toString() == objInfo[GetLowUpp("GROUP_ID")].toString()){
				if(s_decode(objInfo[GetLowUpp("MENU_URL")]).lastIndexOf(".vue") != -1 && s_decode(objInfo[GetLowUpp("MENU_URL")]).lastIndexOf(".vue") == s_decode(objInfo[GetLowUpp("MENU_URL")]).length - 4)
				{	var divMenuInfo = "<li>"
									+ "<a class=\"J_menuItem\" href=\""+s_decode(objInfo[GetLowUpp("MENU_URL")])+"\" attRandom=\""+objInfo[GetLowUpp("S_DESC")]+"\">"+s_decode(objInfo[GetLowUpp("MENU_NAME")])+"</a>"
									+ "</li>";
					$("#"+obj[GetLowUpp("GROUP_EN_NAME")]+"_menu").append(divMenuInfo);
				}
				else
				{
					var divMenuInfo = "<li>"
									+ "<a class=\"J_menuItem\" href=\""+s_decode(objInfo[GetLowUpp("MENU_URL")])+"\" attRandom=\""+objInfo[GetLowUpp("S_DESC")]+"\">"+s_decode(objInfo[GetLowUpp("MENU_NAME")])+"</a>"
									+ "</li>";
					$("#"+obj[GetLowUpp("GROUP_EN_NAME")]+"_menu").append(divMenuInfo);	
				}
			}
		});
	});
	
	var bFlag = false;
	var menu_cont_Random = "T00000";
	$.each(menu_ccode, function (j, objccfo){
		$.each(menu_info, function (j, objInfo){
			if(objccfo[GetLowUpp("MENU_ID")].toString() == objInfo[GetLowUpp("MAIN_ID")].toString()){
				if(objccfo[GetLowUpp("S_DESC")] != "")
					menu_cont_Random = objccfo[GetLowUpp("S_DESC")];
			}
		});
	});
	
	$.each(menu_content, function (k, objCont) {
		$.each(menu_info, function (j, objInfo) {
			if(objInfo[GetLowUpp("MAIN_ID")].toString() == objCont[GetLowUpp("MENU_ID")].toString()){
				var temUrl = s_decode(objInfo[GetLowUpp("MENU_URL")]);
				var iR = IsVueOrHml(temUrl);
				if(iR == 2){
					$("#main_menu").attr("data-id",temUrl);
					$("#iframe0").attr("src",temUrl);
					$("#iframe0").attr("data-id",temUrl);
					$("#iframe0").show();
				}
				else if(iR == 1){
					$("#main_menu").attr("data-id",temUrl);
					$("#main_menu").attr("attrandom",menu_cont_Random);
					$("#iframe0").hide();
					var nn = "<div id='"+menu_cont_Random+"_div999' name='"+menu_cont_Random+"_div999'></div>";
					$(".J_mainContent").append(nn);
					index_subhtml = temUrl;
					divm = menu_cont_Random+"_div999";
					random_subhtml = menu_cont_Random+"_";
					call_vue_funcName = "";
					$("#"+menu_cont_Random+"_div999").attr("data-id",temUrl);
					loadScript_nofun(temUrl+"?callback=ReadCommonRes");
				}
				bFlag = true;
			}
		});
	});
	
	if(bFlag == false)
	{
		$("#iframe0").hide();
		$("#main_menu").attr("data-id","init.vue");
		$("#main_menu").attr("attrandom",menu_cont_Random);
		var nn = "<div id='"+menu_cont_Random+"_div999' name='"+menu_cont_Random+"_div999'></div>";
		$(".J_mainContent").append(nn);
		index_subhtml = "init.vue";
		divm = menu_cont_Random+"_div999";
		random_subhtml = menu_cont_Random+"_";
		call_vue_funcName = "";
		$("#"+menu_cont_Random+"_div999").attr("data-id","init.vue");
		loadScript_nofun("init.vue?callback=ReadCommonRes");
	}
}

//vue回调
function index_call_vue(objResult){
	//更改密码
	if(index_subhtml == "upd_pwd.vue")
	{
		var divSub = "<div id='B44HT5_div' name='B44HT5_div' style='display:none'></div>";
		$("body").append(divSub)
		$("#B44HT5_div").append(objResult);		
		layer.open({
			type:1,
			area: ['600px', '350px'],
			fixed: false, //不固定
			maxmin: true,
			content: $("#B44HT5_div"),
			success: function(layero, index){
				B44HT5_ly_index = index;
				var data = {
					"ly_index":index
				};
	        	loadScript_hasparam("upd_pwd.js","B44HT5_start",data);
			},
			cancel: function(){
			}
		});
	}
	//其他事件触发
	else
	{
		//前端加载子页面div
		$("#"+divm).append(objResult);
		var index_suburl = index_subhtml.substring(0,index_subhtml.lastIndexOf(".vue"));
		var index_subname = index_suburl.substring(index_subhtml.lastIndexOf("/")+1,index_suburl.length);
		loadScript_hasparam(index_suburl+".js",random_subhtml+index_subname+"_biz_start",{});
	}
}

$(document).ready(function () {
	init_page();
});