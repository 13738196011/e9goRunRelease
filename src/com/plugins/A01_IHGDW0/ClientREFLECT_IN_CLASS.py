#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.plugins.A01_IHGDW0.v_server_protocol import v_server_protocol
from com.plugins.A01_IHGDW0 import A01_Para
from com.plugins.A01_LXNOQU.A01_LXNOQU_Time import A01_LXNOQU_Time
from com.plugins.A01_R790KS.A01_R790KS_Time import A01_R790KS_Time
from com.plugins.A01_T8MN36.A01_T8MN36_Time import A01_T8MN36_Time
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.z_debug import z_debug
class ClientREFLECT_IN_CLASS(z_debug):
    strContinue      = "1"   
    strResult        = ""
    strIP            = ""
    strPort          = ""
    strSend          = ""
    def __init__(self):
        pass
    def init_start(self):
        uL = UsAdmin_Log(Com_Para.ApplicationPath,self.strIP+":"+self.strPort+"==>"+self.strResult,"AcceptPacket")
        uL.WriteLog()
        for temv_server_protocol in A01_Para.ht_v_server_protocol:
            if temv_server_protocol.attserver_ip == self.strIP and temv_server_protocol.attprot_name == "A01_LXNOQU":
                a01_time = A01_LXNOQU_Time()
                a01_time.strResult = self.strResult
                a01_time.strIP = temv_server_protocol.attserver_ip
                a01_time.strPort = temv_server_protocol.attserver_port
                a01_time.init_start()
                self.strSend = a01_time.strSend
            elif temv_server_protocol.attserver_ip == self.strIP and temv_server_protocol.attprot_name == "A01_R790KS":
                a02_time = A01_R790KS_Time()
                a02_time.strResult = self.strResult
                a02_time.strIP = temv_server_protocol.attserver_ip
                a02_time.strPort = temv_server_protocol.attserver_port
                a02_time.init_start()
                self.strSend = a02_time.strSend
            elif temv_server_protocol.attserver_ip == self.strIP and temv_server_protocol.attprot_name == "A01_T8MN36":
                a03_time = A01_T8MN36_Time()
                a03_time.strResult = self.strResult
                a03_time.strIP = temv_server_protocol.attserver_ip
                a03_time.strPort = temv_server_protocol.attserver_port
                a03_time.init_start()
                self.strSend = a03_time.strSend