#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import time,json,random,datetime
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_N86HKL.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_N86HKL import A01_Para
from com.plugins.A01_N86HKL.A01_N86HKL_PLC import A01_N86HKL_PLC
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.plugins.A01_N86HKL.data_min import data_min
from com.plugins.A01_N86TYV.A01_N86TYV_Time import A01_N86TYV_Time
from com.zxy.db2.Db_Common2 import Db_Common2
from com.plugins.A01_N86HKL.Set_Upload_Kg import Set_Upload_Kg
from com.zxy.z_debug import z_debug
class A01_TestThreadsTime(z_debug):
    strContinue      = "1"   
    strResult        = ""
    strS_TYPE = "-1"
    Interval = 30
    def __init__(self):
        pass
    def show(self):
        try:
            self.Interval = random.randint(1,2)
            print("第"+str(self.strS_TYPE)+"个数据,时间间隔"+str(self.Interval)+",开始时间："+Com_Fun.GetTimeDef())
            starttime = datetime.datetime.now()
            if self.Interval != 0:
                endtime = starttime + datetime.timedelta(seconds=self.Interval)
                while True:
                    starttime = datetime.datetime.now()
                    if starttime >= endtime:
                        self.Interval = random.randint(1,2)
                        if starttime >= endtime + datetime.timedelta(seconds=self.Interval):
                            endtime = endtime + 2*datetime.timedelta(seconds=self.Interval)
                        else:
                            endtime = endtime + datetime.timedelta(seconds=self.Interval)
                        self.init_start()
                    else:
                        sleeps_flag = random.random()*1
                        print("第"+str(self.strS_TYPE)+"个数据,等待时间"+str(format(sleeps_flag,".4f"))+",当前时间："+Com_Fun.GetTimeDef())
                        time.sleep(sleeps_flag)  
        except Exception as e:                
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+repr(e)
                self.debug_in(self,repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+repr(e)
                self.debug_in(repr(e))#打印异常信息                
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
    def init_start(self):
        try:
            bodc = Bus_Ope_DB_Cent()
            bodc.init_page()
            tem_data_min = data_min()      
            tem_data_min.attcpu_time = Com_Fun.GetTimeDef()             
            tem_data_min.attautomatic_on = str(random.random()*100)
            tem_data_min.atttime_base = str(random.random()*100 + 100)
            tem_data_min.attdoor_closed = str(random.random()*100 + 200)
            tem_data_min.attextruder_on = str(random.random()*100 + 300)
            tem_data_min.attbatch_counter = str(random.random()*100 + 400)
            tem_data_min.attcycle_time = str(random.random()*100 + 500)
            tem_data_min.atttank_weight = str(random.random()*100 + 600)
            tem_data_min.attcooling_water_temp = str(random.random()*100 + 700)
            tem_data_min.attair_pressure = str(random.random()*100 + 800)          
            tem_data_min.atts_type = self.strS_TYPE
            bodc.Ins_data_min(tem_data_min)
            time.sleep(0.2) 
            print("第"+str(self.strS_TYPE)+"个数据,时间间隔"+str(self.Interval)+",入库时间："+Com_Fun.GetTimeDef())
        except Exception as e:
            temStr = self.debug_info() + "\r\n" + repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temStr)
            uL.WriteLog()
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息