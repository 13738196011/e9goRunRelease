#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class T_SUB_USERPOWER(object):
    attMAIN_ID = -1
    attSUB_USER_ID = -1
    attPROC_ID = -1
    attLIMIT_DATE = None
    attUSE_LIMIT = -1
    attLIMIT_NUMBER = -1
    attLIMIT_TAG = ""
    attFIRST_DATE = None
    attS_DESC = ""
    attCREATE_DATE = None
    def __init__(self):
        self.attLIMIT_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        self.attFIRST_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
