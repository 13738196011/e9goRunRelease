#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''
import json,os
from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.zxy.common import Com_Para
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.z_debug import z_debug
class A01_INPROC_IN(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = 1
    def __init__(self):
        pass
    def init_start(self):
        temStrTFileName = Com_Para.ApplicationPath+Com_Para.zxyPath+"web"+Com_Para.zxyPath+"file"+Com_Para.zxyPath+self.param_value1
        if os.path.exists(temStrTFileName) == False :            
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"异常:附件文件不存在\"}]}"
        else:
            try:
                uL = UsAdmin_Log(Com_Para.ApplicationPath, "")                
                jsonObject = json.loads(uL.ReadFile(temStrTFileName))
                opc = Ope_DB_Cent()
                if jsonObject != None and "t_proc_name" in jsonObject:
                    jsary = jsonObject["t_proc_name"]
                    for jso_t_proc_name in jsary:
                        PROC_ID = opc.get_t_proc_name_id(str(jso_t_proc_name["INF_EN_NAME"]));
                        if PROC_ID == "":
                            opc.Ins_Data_JSON(jso_t_proc_name,"t_proc_name",PROC_ID);
                            PROC_ID = opc.get_t_proc_name_id(jso_t_proc_name["INF_EN_NAME"]);
                            if jsonObject != None and "t_proc_inparam" in jsonObject:
                                jsary_t_proc_inparam = jsonObject["t_proc_inparam"]
                                for jso_t_proc_inparam in jsary_t_proc_inparam:
                                    if str(jso_t_proc_inparam["PROC_ID"]) == str(jso_t_proc_name["MAIN_ID"]):
                                        opc.Ins_Data_JSON(jso_t_proc_inparam,"t_proc_inparam",PROC_ID)
                            if jsonObject != None and "t_proc_outparam" in jsonObject:
                                jsary_t_proc_outparam = jsonObject["t_proc_outparam"]
                                for jso_t_proc_outparam in jsary_t_proc_outparam:
                                    if str(jso_t_proc_outparam["PROC_ID"]) == str(jso_t_proc_name["MAIN_ID"]):
                                        opc.Ins_Data_JSON(jso_t_proc_outparam,"t_proc_outparam",PROC_ID)
                            if jsonObject != None and "t_proc_return" in jsonObject:
                                jsary_t_proc_return = jsonObject["t_proc_return"]
                                for jso_t_proc_return in jsary_t_proc_return:
                                    if str(jso_t_proc_return["PROC_ID"]) == str(jso_t_proc_name["MAIN_ID"]):
                                        opc.Ins_Data_JSON(jso_t_proc_return,"t_proc_return",PROC_ID)                            
                            if jsonObject != None and "t_sub_power" in jsonObject:
                                jsary_t_sub_power = jsonObject["t_sub_power"]
                                for jso_t_sub_power in jsary_t_sub_power:
                                    if str(jso_t_sub_power["PROC_ID"]) == str(jso_t_proc_name["MAIN_ID"]):
                                        opc.Ins_Data_JSON(jso_t_sub_power,"t_sub_power",PROC_ID)
                            if jsonObject != None and "t_sub_userpower" in jsonObject:
                                jsary_t_sub_userpower = jsonObject["t_sub_userpower"]
                                for jso_t_sub_userpower in jsary_t_sub_userpower:
                                    if str(jso_t_sub_userpower["PROC_ID"]) == str(jso_t_proc_name["MAIN_ID"]):
                                        opc.Ins_Data_JSON(jso_t_sub_userpower,"t_sub_userpower",PROC_ID)                   
                self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"1\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"接口数据导入成功\"}]}"
            except Exception as e:
                temLog = ""
                if str(type(self)) == "<class 'type'>":
                    temLog = self.debug_info(self)+repr(e)
                else:
                    temLog = self.debug_info()+repr(e)
                uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
                uL.WriteLog()
                self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"异常:"+repr(e)+"\"}]}"
        self.strContinue = 0