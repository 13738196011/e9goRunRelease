//选择某一行
var C772A0_select_v_selproc_rowId = "";
//按钮事件新增或编辑
var C772A0_type = "";
//其他页面传到本页面参数
var C772A0_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var C772A0_tem_INF_CN_NAME = "";var C772A0_tem_INF_EN_NAME = "";var C772A0_tem_IS_AUTHORITY = "0";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var C772A0_ary_IS_AUTHORITY = [{'MAIN_ID': '1', 'CN_NAME': '是'}, {'MAIN_ID': '0', 'CN_NAME': '否'}];
/*declare select options end*/

//主从表传递参数
function C772A0_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function C772A0_v_selproc_biz_start(inputparam) {
	layer.close(ly_index);
	C772A0_param = inputparam;
	//主从表传递参数
	C772A0_param_set();	
    /*biz begin*/
    if($("#C772A0_qry_IS_AUTHORITY").is("select") && $("#C772A0_qry_IS_AUTHORITY")[0].options.length == 0)    {        $("#C772A0_qry_IS_AUTHORITY").append("<option value='-1'></option>")        $.each(C772A0_ary_IS_AUTHORITY, function (i, obj) {            addOptionValue("C772A0_qry_IS_AUTHORITY", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    C772A0_init_v_selproc()	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function C772A0_format_IS_AUTHORITY(value, row, index) {    var objResult = value;    for(i = 0; i < C772A0_ary_IS_AUTHORITY.length; i++) {        var obj = C772A0_ary_IS_AUTHORITY[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function C772A0_page_end(){
	C772A0_adjust_tab();
}

//获取数据接口视图显示列定义
var C772A0_v_selproc = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '数据源配置',        field: 'DB_ID',        sortable: true    },    {        title: '接口中文名',        field: 'INF_CN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '接口英文名',        field: 'INF_EN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: 'SQL语句',        field: 'INF_EN_SQL',        sortable: true        ,formatter: set_s_decode    },    {        title: '接口类型',        field: 'INF_TYPE',        sortable: true    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_s_decode    },    {        title: '入参拦截器',        field: 'REFLECT_IN_CLASS',        sortable: true        ,formatter: set_s_decode    },    {        title: '出参拦截器',        field: 'REFLECT_OUT_CLASS',        sortable: true        ,formatter: set_s_decode    },    {        title: '是否权限认证',        field: 'IS_AUTHORITY',        sortable: true        ,formatter: C772A0_format_IS_AUTHORITY    }	
	/*table column end*/
];

//页面初始化
function C772A0_init_v_selproc() {
	$(window).resize(function () {
		  $('#C772A0_v_selproc_Events').bootstrapTable('resetView');
	});
	//获取数据接口视图查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#C772A0_btn_v_selproc_query').click();
}

//查询接口
function C772A0_v_selproc_query() {
    $('#C772A0_v_selproc_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_proc_name",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(C772A0_tem_INF_CN_NAME)        ,"param_value2": s_encode(C772A0_tem_INF_EN_NAME)        ,"param_value3": C772A0_tem_IS_AUTHORITY        ,"param_value4": C772A0_tem_IS_AUTHORITY		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "C772A0_get_N01_sel_v_selproc");
}

//查询结果
function C772A0_get_N01_sel_v_selproc(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_proc_name) == false)
        return false;
    //调整table各列宽度
    $.each(C772A0_v_selproc, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_t_proc_name;
    C772A0_select_v_selproc_rowId = "";
    $('#C772A0_v_selproc_Events').bootstrapTable('destroy');
    $("#C772A0_v_selproc_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#C772A0_v_selproc_Events').find("tr.changeColor").removeClass('changeColor');
                C772A0_select_v_selproc_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#C772A0_v_selproc_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                C772A0_select_v_selproc_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#C772A0_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(C772A0_param.hasOwnProperty("target_name"))
			{
				$("#"+C772A0_param["target_id"]).val(eval("row."+C772A0_param["sourc_id"].toUpperCase()));
				$("#"+C772A0_param["target_name"]).val(s_decode(eval("row."+C772A0_param["sourc_name"].toUpperCase())));				
				layer.close(C772A0_param["ly_index"]);
			}
		},
        toolbar: "#C772A0_v_selproc_Toolbar",
        columns: C772A0_v_selproc,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
    C772A0_page_end();
}

//刷新按钮
$('#C772A0_btn_v_selproc_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    C772A0_tem_INF_CN_NAME = "";    C772A0_tem_INF_EN_NAME = "";    C772A0_tem_IS_AUTHORITY = "0";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/

	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    if($("#C772A0_qry_IS_AUTHORITY").is("select") && $("#C772A0_qry_IS_AUTHORITY")[0].options.length == 0)    {        $("#C772A0_qry_IS_AUTHORITY").append("<option value='-1'></option>")        $.each(C772A0_ary_IS_AUTHORITY, function (i, obj) {            addOptionValue("C772A0_qry_IS_AUTHORITY", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    C772A0_init_v_selproc()	
    /*biz end*/
})

//查询按钮
$('#C772A0_btn_v_selproc_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    C772A0_tem_INF_CN_NAME = $("#C772A0_qry_INF_CN_NAME").val();    C772A0_tem_INF_EN_NAME = $("#C772A0_qry_INF_EN_NAME").val();    C772A0_tem_IS_AUTHORITY = $("#C772A0_qry_IS_AUTHORITY").val();	
    /*set query param end*/
	C772A0_v_selproc_query();
})

//vue回调
function C772A0_v_selproc_call_vue(objResult){
	if(index_subhtml == "v_selproc_$.vue")
	{
		var n = Get_RandomDiv("B772A0",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":C772A0_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temINF_CN_NAME = $("#C772A0_qry_INF_CN_NAME").val();                if(temINF_CN_NAME != ""){                    inputdata["INF_CN_NAME"] = temINF_CN_NAME;                }                var temINF_EN_NAME = $("#C772A0_qry_INF_EN_NAME").val();                if(temINF_EN_NAME != ""){                    inputdata["INF_EN_NAME"] = temINF_EN_NAME;                }                var temIS_AUTHORITY = $("#C772A0_qry_IS_AUTHORITY").val();                if(temIS_AUTHORITY != ""){                    inputdata["IS_AUTHORITY"] = temIS_AUTHORITY;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/v_selproc_$.js","B772A0_v_selproc_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/

	/*get tab subvue end*/
}

//新增按钮
$("#C772A0_btn_v_selproc_add").click(function () {
	C772A0_type = "add";
	index_subhtml = "v_selproc_$.vue";
	if(loadHtmlSubVueFun("biz_vue/v_selproc_$.vue","C772A0_v_selproc_call_vue") == true){
		var n = Get_RandomDiv("B772A0","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				B772A0_param["type"] = C772A0_type;
				B772A0_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temINF_CN_NAME = $("#C772A0_qry_INF_CN_NAME").val();                if(temINF_CN_NAME != ""){                    B772A0_param["INF_CN_NAME"] = temINF_CN_NAME;                }                var temINF_EN_NAME = $("#C772A0_qry_INF_EN_NAME").val();                if(temINF_EN_NAME != ""){                    B772A0_param["INF_EN_NAME"] = temINF_EN_NAME;                }                var temIS_AUTHORITY = $("#C772A0_qry_IS_AUTHORITY").val();                if(temIS_AUTHORITY != ""){                    B772A0_param["IS_AUTHORITY"] = temIS_AUTHORITY;                }				
				/*Send Two FindSelect param end*/

				B772A0_clear_edit_info();
			},
			end: function(){
				B772A0_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#C772A0_btn_v_selproc_edit").click(function () {
	if (C772A0_select_v_selproc_rowId != "") {
		C772A0_type = "edit";
		index_subhtml = "v_selproc_$.vue";
		if(loadHtmlSubVueFun("biz_vue/v_selproc_$.vue","C772A0_v_selproc_call_vue") == true){
			var n = Get_RandomDiv("B772A0","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					B772A0_param["type"] = C772A0_type;
					B772A0_param["ly_index"] = index;
					B772A0_clear_edit_info();
					B772A0_get_edit_info();
				},
				end: function(){
					B772A0_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#C772A0_btn_v_selproc_delete').click(function () {
	//单行选择
	var rowData = $("#C772A0_v_selproc_Events").bootstrapTable('getData')[C772A0_select_v_selproc_rowId];
	//多行选择
	var rowDatas = C772A0_sel_row_v_selproc();
	if (C772A0_select_v_selproc_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_v_selproc",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "C772A0_N01_del_v_selproc");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_v_selproc",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "C772A0_N01_del_v_selproc");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function C772A0_N01_del_v_selproc(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_v_selproc) == true)
		C772A0_v_selproc_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function C772A0_adjust_tab(){
	if(typeof($("#C772A0_TAB_MAIN")[0]) != "undefined" && $("#C772A0_TAB_MAIN")[0].length != 0){
		C772A0_Flag = "1";
		$(Get_RDivNoBuild("C772A0","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("C772A0","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("C772A0","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#C772A0_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/

/*Tab Click Fun End*/

//显示tab页选项卡
function C772A0_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("biz_vue/"+inputUrl,"C772A0_v_selproc_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (C772A0_select_v_selproc_rowId != "") 
			rowData = $("#C772A0_v_selproc_Events").bootstrapTable('getData')[C772A0_select_t_proc_name_rowId];
		else
			rowData = $("#C772A0_v_selproc_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			$.each(temPar, function (i, obj) {
				eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
		}
		var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
		$("#"+inputrandom+"_btn_"+tbName+"_query").click();
	}
}

//清除 查找框
function C772A0_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function C772A0_sel_row_v_selproc(){
	//获得选中行
	var checkedbox= $("#C772A0_v_selproc_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}