#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import os
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.z_debug import z_debug
class UsAdmin_Log(z_debug):    
    attContent = ''
    attFolder = ''
    attTFileName = ''
    def __init__(self,*args):
        if len(args) == 2:
            self.attFolder = args[0] + Com_Para.zxyPath + "log"
            self.attContent = args[1]
        elif len(args) == 3:              
            self.attFolder = args[0] + '/log'
            self.attContent = args[1]
            self.attTFileName = args[2]
        elif len(args) == 4:
            self.attFolder =  args[0] + Com_Para.zxyPath + "data"
            self.attContent = args[1]
    def WriteData(self,inputDelay_code):
        self.HadForder()
        self.SaveFileData(inputDelay_code)
    def WriteLog(self):
        self.HadForder()
        self.SaveFileDay()
    def WriteExceptionLog(self,temClassName,temFunName):
        temIndexS = temClassName.rindex("\\")
        temIndexE = temClassName.rindex(".")
        temClass = temClassName[temIndexS+1:temIndexE]
        self.attContent = "==>"+temClass+"."+temFunName+"|\r"+self.attContent
        self.HadForder()
        self.SaveFileDay()
    def WriteLogAll(self, inputTContent):
        self.HadForder()
        self.SaveFileDayAll(inputTContent)
    def WriteLogSub(self, inputSubFile):
        self.HadForder()
        self.SaveFileDay(inputSubFile)       
    def WriteLogSubDEC(self):
        self.WriteLogSub()
        self.WriteLogAll('DEC_SEC_')
    def InitWriteData(self,inputDelay_code):
        self.HadForder()
        self.InitSaveFileData(inputDelay_code)
    def InitSaveFileData(self,inputDelay_code):
        temStrTFileName = self.attFolder + Com_Para.zxyPath + inputDelay_code + ".dat"
        temFile = None
        try: 
            if os.path.exists(temStrTFileName) == False :
                temFile = open(temStrTFileName, 'w',encoding=Com_Para.U_CODE)
                temFile.write(self.attContent + "\r\n")
                temFile.close()
            else:
                pass
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if temFile != None:
                temFile.close()
    def InitAppendFileData(self,inputDelay_code):
        temStrTFileName = self.attFolder + Com_Para.zxyPath + inputDelay_code + ".dat"
        temFile = None
        try: 
            if os.path.exists(temStrTFileName) == False :
                temFile = open(temStrTFileName, 'w',encoding=Com_Para.U_CODE)
                temFile.write(self.attContent + "\r\n")
                temFile.close()
            else:
                temFile = open(temStrTFileName, 'a',encoding=Com_Para.U_CODE)
                temFile.write(self.attContent + "\r\n")
                temFile.close()
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if temFile != None:
                temFile.close()
    def HadForder(self):
        if os.path.exists(self.attFolder) == False:
            os.mkdir(self.attFolder)
    def ReadFile(self,inputFileName):
        temStrResult = ''
        temFile = None
        try: 
            if os.path.exists(inputFileName) == False :
                temStrResult = ""
            else:
                temFile = open(inputFileName, 'r',encoding=Com_Para.U_CODE)
                while True:
                    temLines = temFile.readline()
                    temStrResult += temLines
                    if not temLines:
                        break
                    pass
        except Exception as e:
            temStrResult = ""
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if temFile != None:
                temFile.close()
        return temStrResult
    def ReadData(self,inputDelay_code,inputParam_name):
        temStrResult = ''
        temTFileName = self.attFolder + Com_Para.zxyPath + inputDelay_code + ".dat"
        temFile = None
        try: 
            if os.path.exists(temTFileName) == False :
                temStrResult = "{\""+ inputParam_name+ "\":[{\"s_result\":\"0\",\"error_desc\":\"数据查询中或数据结果未生成,请稍候在试"+ "\"}]}"
            else:
                temFile = open(temTFileName, 'r',encoding=Com_Para.U_CODE)
                while True:
                    temLines = temFile.readline() 
                    temStrResult += temLines
                    if not temLines:
                        break
                    pass
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if temFile != None:
                temFile.close()
        return temStrResult
    def SaveFileDayAll(self, inputTContent):
        temStrLogName = ""
        if self.attTFileName == "":
            temStrLogName = self.attFolder + '/zxyong_' + Com_Fun.GetTime('%Y%m%d') + '_1.log'
        else:
            temStrLogName = self.attFolder + '/' + self.attTFileName + '.log'
        temFile = None
        try: 
            if os.path.exists(temStrLogName) == False :
                temFile = open(temStrLogName, 'a',encoding=Com_Para.U_CODE)
                temFile.write("*********" + Com_Fun.GetTime('%Y-%m-%d %H:%M:%S') + "*********" + "\r\n")
                temFile.write(inputTContent + "\r\n")
                temFile.close()
            else:
                temLgSize = os.path.getsize(temStrLogName)
                i = 2
                while 1 == 1:
                    if temLgSize > 1024 * 1024 :
                        if self.attTFileName == "":
                            temStrLogName = self.attFolder + "/zxyong_" + Com_Fun.GetTime('%Y%m%d') + "_" + str(i) + ".log"
                        else:
                            temStrLogName = self.attFolder + "/" + self.attTFileName + Com_Fun.GetTime('%Y%m%d') + "_" + str(i) + ".log"
                        if os.path.exists(temStrLogName) == False :
                            temFile = open(temStrLogName, 'a',encoding=Com_Para.U_CODE)
                            temFile.write("*********" + Com_Fun.GetTime('%Y-%m-%d %H:%M:%S') + "*********" + "\r\n")
                            temFile.write(inputTContent + "\r\n")
                            temFile.close()
                            break
                        else:
                            temLgSize = os.path.getsize(temStrLogName)
                    else:
                        temFile = open(temStrLogName, 'a',encoding=Com_Para.U_CODE)
                        temFile.write("*********" + Com_Fun.GetTime('%Y-%m-%d %H:%M:%S') + "*********" + "\r\n")
                        temFile.write(inputTContent + "\r\n")
                        temFile.close()
                        break
                    i = i + 1
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if temFile != None:
                temFile.close()
    def SaveFileDaySub(self, inputSub):
        temStrLogName = self.attFolder + '/zxyong_' + inputSub+"_" + Com_Fun.GetTime('%Y%m%d') + '_1.log'
        temFile = None
        try: 
            if os.path.exists(temStrLogName) == False :
                temFile = open(temStrLogName, 'a',encoding=Com_Para.U_CODE)
                temFile.write("*********" + Com_Fun.GetTime('%Y-%m-%d %H:%M:%S') + "*********" + "\r\n")
                temFile.write(self.attContent + "\r\n")
                temFile.close()
            else:
                temLgSize = os.path.getsize(temStrLogName)
                i = 2
                while 1 == 1:
                    if temLgSize > 1024 * 1024 :
                        temStrLogName = self.attFolder + '/zxyong_' + inputSub+"_" + Com_Fun.GetTime('%Y%m%d') + "_" + str(i) + ".log"           
                        if os.path.exists(temStrLogName) == False :
                            temFile = open(temStrLogName, 'a',encoding=Com_Para.U_CODE)
                            temFile.write("*********" + Com_Fun.GetTime('%Y-%m-%d %H:%M:%S') + "*********" + "\r\n")
                            temFile.write(self.attContent + "\r\n")
                            temFile.close()
                            break
                        else:
                            temLgSize = os.path.getsize(temStrLogName)
                    else:
                        temFile = open(temStrLogName, 'a',encoding=Com_Para.U_CODE)
                        temFile.write("*********" + Com_Fun.GetTime('%Y-%m-%d %H:%M:%S') + "*********" + "\r\n")
                        temFile.write(self.attContent + "\r\n")
                        temFile.close()
                        break
                    i = i + 1
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if temFile != None:
                temFile.close()
    def SaveFileDay(self):
        temStrLogName = ""
        if self.attTFileName == "":
            temStrLogName = self.attFolder + '/zxyong_' + Com_Fun.GetTime('%Y%m%d') + '_1.log'
        else:
            temStrLogName = self.attFolder + '/' + self.attTFileName+Com_Fun.GetTime('%Y%m%d') + '_1.log'
        temFile = None
        try: 
            if os.path.exists(temStrLogName) == False :
                temFile = open(temStrLogName, 'a',encoding=Com_Para.U_CODE)
                temFile.write("*********" + Com_Fun.GetTime('%Y-%m-%d %H:%M:%S') + "*********" + "\r")
                temFile.write(self.attContent + "\r")
                temFile.close()
            else:
                temLgSize = os.path.getsize(temStrLogName)
                i = 2
                while 1 == 1:
                    if temLgSize > 1024 * 1024 :
                        if self.attTFileName == "":
                            temStrLogName = self.attFolder + "/zxyong_" + Com_Fun.GetTime('%Y%m%d') + "_" + str(i) + ".log"
                        else:
                            temStrLogName = self.attFolder + "/" + self.attTFileName + Com_Fun.GetTime('%Y%m%d') + "_" + str(i) + ".log"
                        if os.path.exists(temStrLogName) == False :
                            temFile = open(temStrLogName, 'a',encoding=Com_Para.U_CODE)
                            temFile.write("*********" + Com_Fun.GetTime('%Y-%m-%d %H:%M:%S') + "*********" + "\r")
                            temFile.write(self.attContent + "\r")
                            temFile.close()
                            break
                        else:
                            temLgSize = os.path.getsize(temStrLogName)
                    else:
                        temFile = open(temStrLogName, 'a',encoding=Com_Para.U_CODE)
                        temFile.write("*********" + Com_Fun.GetTime('%Y-%m-%d %H:%M:%S') + "*********" + "\r")
                        temFile.write(self.attContent + "\r")
                        temFile.close()
                        break
                    i = i + 1
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if temFile != None:
                temFile.close()