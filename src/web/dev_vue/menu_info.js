//主从表tab选项卡标志位
var A77565_Tab_Flag = -1;
//选择某一行
var A77565_select_menu_info_rowId = "";
//按钮事件新增或编辑
var A77565_type = "";
//其他页面传到本页面参数
var A77565_param = {};
//table选中数据，如无则默认第一条
var A77565_rowCheckData = null;

/*定义查询条件变量*/
/*declare query param begin*/
var A77565_tem_MENU_NAME = "";var A77565_tem_IS_AVA = "0";var A77565_tem_ROLE_ID = "";var A77565_tem_GROUP_ID = "-1";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var A77565_ary_GROUP_ID = null;var A77565_ary_IS_AVA = [{'MAIN_ID': '1', 'CN_NAME': '启用'}, {'MAIN_ID': '0', 'CN_NAME': '禁用'}];var A77565_ary_ROLE_ID = [{'MAIN_ID': '1', 'CN_NAME': '开发者'}, {'MAIN_ID': '2', 'CN_NAME': '系统管理员'}, {'MAIN_ID': '3', 'CN_NAME': '运维人员'}, {'MAIN_ID': '4', 'CN_NAME': '普通用户'}];
/*declare select options end*/

/*绑定show监听事件*/
if(A77565_Tab_Flag == "-1"){
	var n = Get_RandomDiv("A77565","");
	$(n).bind("show", function(objTag){
		A77565_Adjust_Sub_Sequ();
		if(A77565_Tab_Flag > 0)
			A77565_adjust_tab();		
	});
}

//设置主从表div顺序
function A77565_Adjust_Sub_Sequ(){
	var temSubDivs = $("#content-main").find("div[data-id]");
	var MainDiv = $(Get_RandomDiv("A77565",""));
	for(i = 0; i < temSubDivs.length; i++) {
		if(A77565_Is_Sub_Div(temSubDivs[i].id)){
			$(temSubDivs[i]).before($(MainDiv));
			break;
		}
	}
}

//主从表传递参数
function A77565_param_set(){
	/*Main Subsuv Table Param Begin*/
    if(A77565_param.hasOwnProperty("GROUP_ID_cn_name"))        $("#A77565_find_GROUP_ID_cn_name").val(s_decode(A77565_param["GROUP_ID_cn_name"]));    if(A77565_param.hasOwnProperty("GROUP_ID"))        $("#A77565_find_GROUP_ID").val(A77565_param["GROUP_ID"]);    if(A77565_param.hasOwnProperty("hidden_find")){        $("#A77565_Ope_GROUP_ID").hide();        $("#A77565_Clear_GROUP_ID").hide();    }	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function A77565_menu_info_biz_start(inputparam) {
	layer.close(ly_index);
	A77565_param = inputparam;
	//主从表传递参数
	A77565_param_set();	
    /*biz begin*/
    var inputdata = {        "param_name": "N01_menu_info$GROUP_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A77565_get_N01_menu_info$GROUP_ID");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function A77565_format_GROUP_ID(value, row, index) {    var objResult = value;    for(i = 0; i < A77565_ary_GROUP_ID.length; i++) {        var obj = A77565_ary_GROUP_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A77565_format_IS_AVA(value, row, index) {    var objResult = value;    for(i = 0; i < A77565_ary_IS_AVA.length; i++) {        var obj = A77565_ary_IS_AVA[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A77565_format_ROLE_ID(value, row, index) {    var objResult = "";    for(i = 0; i < A77565_ary_ROLE_ID.length; i++) {        var obj = A77565_ary_ROLE_ID[i];
        var value_s = s_decode(value.toString()).split(",");
        for(j = 0; j < value_s.length; j ++){
	        if (obj[GetLowUpp("MAIN_ID")].toString() == value_s[j].toString()) {
	        	if(objResult == "")
	        		objResult += obj[GetLowUpp("CN_NAME")];
	        	else
	        		objResult += ","+obj[GetLowUpp("CN_NAME")];
	        }
		}    }    return objResult;}function A77565_get_N01_menu_info$GROUP_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_menu_info$GROUP_ID) == false)        return false;    A77565_ary_GROUP_ID = input.N01_menu_info$GROUP_ID;    $("#A77565_qry_IS_AVA").append("<option value='-1'></option>")    $.each(A77565_ary_IS_AVA, function (i, obj) {        addOptionValue("A77565_qry_IS_AVA", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    $("#A77565_qry_ROLE_ID").append("<option value=''></option>")    $.each(A77565_ary_ROLE_ID, function (i, obj) {        addOptionValue("A77565_qry_ROLE_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    A77565_init_menu_info();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function A77565_GROUP_ID_cn_name_fun(){    index_subhtml = "menu_group.vue"    random_subhtml = "";    if(loadHtmlSubVueFun("dev_vue/menu_group.vue","A77565_menu_info_call_vue") == true){        var n = Get_RandomDiv("","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#_menu_group_Events').bootstrapTable('resetView');                _param["ly_index"] = index;                _param["target_name"] = "A77565_find_GROUP_ID_cn_name"                _param["target_id"] = "A77565_find_GROUP_ID"                _param["sourc_id"] = "MAIN_ID"                _param["sourc_name"] = "GROUP_CN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function A77565_page_end(){
	A77565_adjust_tab();
}

//菜单页面显示列定义
var A77565_menu_info = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '菜单组',        field: 'GROUP_ID',        sortable: true        ,formatter: A77565_format_GROUP_ID    },    {        title: '菜单url',        field: 'MENU_URL',        sortable: true        ,formatter: set_s_decode    },    {        title: '菜单名称',        field: 'MENU_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '是否启用',        field: 'IS_AVA',        sortable: true        ,formatter: A77565_format_IS_AVA    },    {        title: '角色',        field: 'ROLE_ID',        sortable: true        ,formatter: A77565_format_ROLE_ID    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function A77565_init_menu_info() {
	$(window).resize(function () {
		  $('#A77565_menu_info_Events').bootstrapTable('resetView');
	});
	//菜单页面查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#A77565_btn_menu_info_query').click();
}

//查询接口
function A77565_menu_info_query() {
    $('#A77565_menu_info_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_menu_info",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(A77565_tem_MENU_NAME)        ,"param_value2": A77565_tem_IS_AVA        ,"param_value3": A77565_tem_IS_AVA        ,"param_value4": s_encode(A77565_tem_ROLE_ID)        ,"param_value5": A77565_tem_GROUP_ID        ,"param_value6": A77565_tem_GROUP_ID		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "A77565_get_N01_sel_menu_info");
}

//查询结果
function A77565_get_N01_sel_menu_info(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_menu_info) == false)
        return false;
    A77565_rowCheckData = null;
    //调整table各列宽度
    $.each(A77565_menu_info, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_menu_info;
    if(s_data.length > 0)
    	A77565_rowCheckData = s_data[0];    
    A77565_select_menu_info_rowId = "";
    A77565_Tab_Flag = 0;
    $('#A77565_menu_info_Events').bootstrapTable('destroy');
    $("#A77565_menu_info_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#A77565_menu_info_Events').find("tr.changeColor").removeClass('changeColor');
                A77565_select_menu_info_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#A77565_menu_info_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                A77565_select_menu_info_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#A77565_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(A77565_param.hasOwnProperty("target_name"))
			{
				$("#"+A77565_param["target_id"]).val(eval("row."+A77565_param["sourc_id"].toUpperCase()));
				$("#"+A77565_param["target_name"]).val(s_decode(eval("row."+A77565_param["sourc_name"].toUpperCase())));				
				layer.close(A77565_param["ly_index"]);
			}
		},
        toolbar: "#A77565_menu_info_Toolbar",
        columns: A77565_menu_info,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        onPostBody:function(){
        	if(s_data.length != 0)
            	A77565_page_end();
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#A77565_btn_menu_info_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    A77565_tem_MENU_NAME = "";    A77565_tem_IS_AVA = "0";    A77565_tem_ROLE_ID = "";    A77565_tem_GROUP_ID = "-1";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    B77565_ary_GROUP_ID = null;    $("#A77565_find_GROUP_ID_cn_name").val("");    $("#A77565_find_GROUP_ID").val("-1");
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_menu_info$GROUP_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A77565_get_N01_menu_info$GROUP_ID");	
    /*biz end*/
})

//查询按钮
$('#A77565_btn_menu_info_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    A77565_tem_MENU_NAME = $("#A77565_qry_MENU_NAME").val();    A77565_tem_IS_AVA = $("#A77565_qry_IS_AVA").val();    A77565_tem_ROLE_ID = $("#A77565_qry_ROLE_ID").val();    A77565_tem_GROUP_ID = $("#A77565_find_GROUP_ID").val();	
    /*set query param end*/
	A77565_menu_info_query();
})

//vue回调
function A77565_menu_info_call_vue(objResult){
	//选择某条记录或自动选择第一条
	if (A77565_select_menu_info_rowId != "") 
		A77565_rowCheckData = $("#A77565_menu_info_Events").bootstrapTable('getData')[A77565_select_menu_info_rowId];
	
	if(index_subhtml == "menu_info_$.vue")
	{
		var n = Get_RandomDiv("B77565",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":A77565_type,"ly_index":index};
				if(A77565_param.hasOwnProperty("hidden_find")){
					inputdata["hidden_find"] = "1";
				}				
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temMENU_NAME = $("#A77565_qry_MENU_NAME").val();                if(temMENU_NAME != ""){                    inputdata["MENU_NAME"] = temMENU_NAME;                }                var temIS_AVA = $("#A77565_qry_IS_AVA").val();                if(temIS_AVA != ""){                    inputdata["IS_AVA"] = temIS_AVA;                }                var temROLE_ID = $("#A77565_qry_ROLE_ID").val();                if(temROLE_ID != ""){                    inputdata["ROLE_ID"] = temROLE_ID;                }                var temGROUP_ID = $("#A77565_find_GROUP_ID_cn_name").val();                if(temGROUP_ID != ""){                    inputdata["GROUP_ID_cn_name"] = temGROUP_ID;                    inputdata["GROUP_ID"] = $("#A77565_GROUP_ID").val();                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("dev_vue/menu_info_$.js","B77565_menu_info_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "menu_group.vue"){        var n = Get_RandomDiv("",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":A77565_type,                    "ly_index":index,                    "target_name":"A77565_find_GROUP_ID_cn_name",                    "target_id":"A77565_find_GROUP_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"GROUP_CN_NAME"                };                loadScript_hasparam("dev_vue/menu_group.js","_menu_group_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/

	/*get tab subvue end*/
}

//新增按钮
$("#A77565_btn_menu_info_add").click(function () {
	A77565_type = "add";
	index_subhtml = "menu_info_$.vue";
	if(loadHtmlSubVueFun("dev_vue/menu_info_$.vue","A77565_menu_info_call_vue") == true){
		var n = Get_RandomDiv("B77565","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				B77565_param["type"] = A77565_type;
				B77565_param["ly_index"]= index;
				if(A77565_param.hasOwnProperty("hidden_find")){
					B77565_param["hidden_find"] = "1";
				}
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temMENU_NAME = $("#A77565_qry_MENU_NAME").val();                if(temMENU_NAME != ""){                    B77565_param["MENU_NAME"] = temMENU_NAME;                }                var temIS_AVA = $("#A77565_qry_IS_AVA").val();                if(temIS_AVA != ""){                    B77565_param["IS_AVA"] = temIS_AVA;                }                var temROLE_ID = $("#A77565_qry_ROLE_ID").val();                if(temROLE_ID != ""){                    B77565_param["ROLE_ID"] = temROLE_ID;                }                var temGROUP_ID = $("#A77565_find_GROUP_ID_cn_name").val();                if(temGROUP_ID != ""){                    B77565_param["GROUP_ID_cn_name"] = temGROUP_ID;                    B77565_param["GROUP_ID"] = $("#A77565_GROUP_ID").val();                }				
				/*Send Two FindSelect param end*/

				B77565_clear_edit_info();
			},
			end: function(){
				B77565_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#A77565_btn_menu_info_edit").click(function () {
	if (A77565_select_menu_info_rowId != "") {
		A77565_type = "edit";
		index_subhtml = "menu_info_$.vue";
		if(loadHtmlSubVueFun("dev_vue/menu_info_$.vue","A77565_menu_info_call_vue") == true){
			var n = Get_RandomDiv("B77565","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					B77565_param["type"] = A77565_type;
					B77565_param["ly_index"] = index;
					if(A77565_param.hasOwnProperty("hidden_find")){
						B77565_param["hidden_find"] = "1";
					}
					B77565_clear_edit_info();
					B77565_get_edit_info();
				},
				end: function(){
					B77565_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#A77565_btn_menu_info_delete').click(function () {
	//单行选择
	var rowData = $("#A77565_menu_info_Events").bootstrapTable('getData')[A77565_select_menu_info_rowId];
	//多行选择
	var rowDatas = A77565_sel_row_menu_info();
	if (A77565_select_menu_info_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_menu_info",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "A77565_N01_del_menu_info");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_menu_info",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "A77565_N01_del_menu_info");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function A77565_N01_del_menu_info(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_menu_info) == true)
		A77565_menu_info_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function A77565_adjust_tab(){
	if(typeof($("#A77565_TAB_MAIN")[0]) != "undefined" && $("#A77565_TAB_MAIN")[0].length != 0){
		A77565_Tab_Flag = 1;
		$(Get_RDivNoBuild("A77565","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("A77565","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("A77565","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#A77565_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/
//隐藏tab页选项卡function A77565_hide_tab_fun(){    var n = null;}//判断是否sub子项div页面function A77565_Is_Sub_Div(temDivId){    if(temDivId.indexOf("XX$TTT") == 0)        return false;    return false;}
/*Tab Click Fun End*/

function A77565_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("dev_vue/"+inputUrl,"A77565_menu_info_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (A77565_select_menu_info_rowId != "") 
			rowData = $("#A77565_menu_info_Events").bootstrapTable('getData')[A77565_select_menu_info_rowId];
		else
			rowData = A77565_rowCheckData;
		//$("#A77565_menu_info_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			var temFlag = true;
			$.each(temPar, function (i, obj) {
				if(eval(inputrandom+"_param.hasOwnProperty(\""+obj.target_id+"\")") && eval(inputrandom+"_param[\""+obj.target_id+"\"]").toString() == rowData[obj.sourc_id].toString())
					temFlag = false;
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			if(temFlag){
				//传递子页面隐藏find功能
				eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
				//参数传递并赋值
				eval(inputrandom+"_param_set()");
				var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
				$("#"+inputrandom+"_btn_"+tbName+"_query").click();
			}
		}
		else{
			$.each(temPar, function (i, obj) {
				if(obj.sourc_id.toUpperCase() == "MAIN_ID")
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"-999\"");
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"\"");
			});
			//传递子页面隐藏find功能
			eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
			var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
			$("#"+inputrandom+"_btn_"+tbName+"_query").click();		
		}
	}
}

//清除 查找框
function A77565_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function A77565_sel_row_menu_info(){
	//获得选中行
	var checkedbox= $("#A77565_menu_info_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}