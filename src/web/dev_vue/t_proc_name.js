//主从表tab选项卡标志位
var A43B7C_Tab_Flag = -1;
//选择某一行
var A43B7C_select_t_proc_name_rowId = "";
//按钮事件新增或编辑
var A43B7C_type = "";
//其他页面传到本页面参数
var A43B7C_param = {};
//table选中数据，如无则默认第一条
var A43B7C_rowCheckData = null;

/*定义查询条件变量*/
/*declare query param begin*/
var A43B7C_tem_INF_CN_NAME = "";var A43B7C_tem_INF_EN_NAME = "";var A43B7C_tem_IS_AUTHORITY = "0";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var A43B7C_ary_DB_ID = null;var A43B7C_ary_INF_TYPE = [{'MAIN_ID': '1', 'CN_NAME': '存储过程'}, {'MAIN_ID': '2', 'CN_NAME': 'SQL语句'}];var A43B7C_ary_IS_AUTHORITY = [{'MAIN_ID': '1', 'CN_NAME': '是'}, {'MAIN_ID': '0', 'CN_NAME': '否'}];
/*declare select options end*/

/*绑定show监听事件*/
if(A43B7C_Tab_Flag == "-1"){
	var A43B7C_n = Get_RandomDiv("A43B7C","");
	$(A43B7C_n).bind("show", function(objTag){
		A43B7C_Adjust_Sub_Sequ();
		if(A43B7C_Tab_Flag > 0)
			A43B7C_adjust_tab();
	});
}

//设置主从表div顺序
function A43B7C_Adjust_Sub_Sequ(){
	var temSubDivs = $("#content-main").find("div[data-id]");
	var MainDiv = $(Get_RandomDiv("A43B7C",""));
	for(i = 0; i < temSubDivs.length; i++) {
		if(A43B7C_Is_Sub_Div(temSubDivs[i].id)){
			$(temSubDivs[i]).before($(MainDiv));
			break;
		}
	}
}

//主从表传递参数
function A43B7C_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function A43B7C_t_proc_name_biz_start(inputparam) {
	layer.close(ly_index);
	A43B7C_param = inputparam;
	//主从表传递参数
	A43B7C_param_set();	
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_proc_name$DB_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A43B7C_get_N01_t_proc_name$DB_ID");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function A43B7C_format_DB_ID(value, row, index) {    var objResult = value;    for(i = 0; i < A43B7C_ary_DB_ID.length; i++) {        var obj = A43B7C_ary_DB_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A43B7C_format_INF_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < A43B7C_ary_INF_TYPE.length; i++) {        var obj = A43B7C_ary_INF_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A43B7C_format_IS_AUTHORITY(value, row, index) {    var objResult = value;    for(i = 0; i < A43B7C_ary_IS_AUTHORITY.length; i++) {        var obj = A43B7C_ary_IS_AUTHORITY[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A43B7C_get_N01_t_proc_name$DB_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_name$DB_ID) == false)        return false;    A43B7C_ary_DB_ID = input.N01_t_proc_name$DB_ID;    $("#A43B7C_qry_IS_AUTHORITY").append("<option value='-1'></option>")    $.each(A43B7C_ary_IS_AUTHORITY, function (i, obj) {        addOptionValue("A43B7C_qry_IS_AUTHORITY", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    A43B7C_init_t_proc_name();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function A43B7C_page_end(){
	A43B7C_adjust_tab();
}

//接口名称2显示列定义
var A43B7C_t_proc_name = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: true    },    {        title: '数据源',        field: 'DB_ID',        sortable: true        ,formatter: A43B7C_format_DB_ID    },    {        title: '接口中文名',        field: 'INF_CN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '接口英文名称',        field: 'INF_EN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: 'SQL语句或存储过程',        field: 'INF_EN_SQL',        sortable: true        ,formatter: set_s_decode    },    {        title: '类型',        field: 'INF_TYPE',        sortable: true        ,formatter: A43B7C_format_INF_TYPE    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '输入参数拦截器类名',        field: 'REFLECT_IN_CLASS',        sortable: true        ,formatter: set_s_decode    },    {        title: '输出参数拦截器类名',        field: 'REFLECT_OUT_CLASS',        sortable: true        ,formatter: set_s_decode    },    {        title: '是否权限认证',        field: 'IS_AUTHORITY',        sortable: true        ,formatter: A43B7C_format_IS_AUTHORITY    }	
	/*table column end*/
];

//页面初始化
function A43B7C_init_t_proc_name() {
	$(window).resize(function () {
		  $('#A43B7C_t_proc_name_Events').bootstrapTable('resetView');
	});
	//接口名称2查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#A43B7C_btn_t_proc_name_query').click();
}

//查询接口
function A43B7C_t_proc_name_query() {
    $('#A43B7C_t_proc_name_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_proc_name",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(A43B7C_tem_INF_CN_NAME)        ,"param_value2": s_encode(A43B7C_tem_INF_EN_NAME)        ,"param_value3": A43B7C_tem_IS_AUTHORITY        ,"param_value4": A43B7C_tem_IS_AUTHORITY		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "A43B7C_get_N01_sel_t_proc_name");
}

//查询结果
function A43B7C_get_N01_sel_t_proc_name(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_proc_name) == false)
        return false;
    A43B7C_rowCheckData = null;
    //调整table各列宽度
    $.each(A43B7C_t_proc_name, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_t_proc_name;
    if(s_data.length > 0)
    	A43B7C_rowCheckData = s_data[0];    
    A43B7C_select_t_proc_name_rowId = "";
    A43B7C_Tab_Flag = 0;
    $('#A43B7C_t_proc_name_Events').bootstrapTable('destroy');
    $("#A43B7C_t_proc_name_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#A43B7C_t_proc_name_Events').find("tr.changeColor").removeClass('changeColor');
                A43B7C_select_t_proc_name_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#A43B7C_t_proc_name_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                A43B7C_select_t_proc_name_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#A43B7C_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(A43B7C_param.hasOwnProperty("target_name"))
			{
				$("#"+A43B7C_param["target_id"]).val(eval("row."+A43B7C_param["sourc_id"].toUpperCase()));
				$("#"+A43B7C_param["target_name"]).val(s_decode(eval("row."+A43B7C_param["sourc_name"].toUpperCase())));				
				layer.close(A43B7C_param["ly_index"]);
			}
		},
        toolbar: "#A43B7C_t_proc_name_Toolbar",
        columns: A43B7C_t_proc_name,
        data: s_data,
        pageNumber: 1,
        pageSize: 5, // 每页的记录行数（*）
        pageList: [5,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        onPostBody:function(){
        	if(s_data.length != 0)
            	A43B7C_page_end();
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#A43B7C_btn_t_proc_name_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    A43B7C_tem_INF_CN_NAME = "";    A43B7C_tem_INF_EN_NAME = "";    A43B7C_tem_IS_AUTHORITY = "0";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    B43B7C_ary_DB_ID = null;
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_proc_name$DB_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A43B7C_get_N01_t_proc_name$DB_ID");	
    /*biz end*/
})

//查询按钮
$('#A43B7C_btn_t_proc_name_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    A43B7C_tem_INF_CN_NAME = $("#A43B7C_qry_INF_CN_NAME").val();    A43B7C_tem_INF_EN_NAME = $("#A43B7C_qry_INF_EN_NAME").val();    A43B7C_tem_IS_AUTHORITY = $("#A43B7C_qry_IS_AUTHORITY").val();	
    /*set query param end*/
	A43B7C_t_proc_name_query();
})

//vue回调
function A43B7C_t_proc_name_call_vue(objResult){
	//选择某条记录或自动选择第一条
	if (A43B7C_select_t_proc_name_rowId != "") 
		A43B7C_rowCheckData = $("#A43B7C_t_proc_name_Events").bootstrapTable('getData')[A43B7C_select_t_proc_name_rowId];
	
	if(index_subhtml == "t_proc_name_$.vue")
	{
		var n = Get_RandomDiv("B43B7C",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":A43B7C_type,"ly_index":index};
				if(A43B7C_param.hasOwnProperty("hidden_find")){
					inputdata["hidden_find"] = "1";
				}				
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temINF_CN_NAME = $("#A43B7C_qry_INF_CN_NAME").val();                if(temINF_CN_NAME != ""){                    inputdata["INF_CN_NAME"] = temINF_CN_NAME;                }                var temINF_EN_NAME = $("#A43B7C_qry_INF_EN_NAME").val();                if(temINF_EN_NAME != ""){                    inputdata["INF_EN_NAME"] = temINF_EN_NAME;                }                var temIS_AUTHORITY = $("#A43B7C_qry_IS_AUTHORITY").val();                if(temIS_AUTHORITY != ""){                    inputdata["IS_AUTHORITY"] = temIS_AUTHORITY;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("dev_vue/t_proc_name_$.js","B43B7C_t_proc_name_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/
    else if(index_subhtml == "t_proc_inparam.vue"){        var n = Get_RandomDiv("A9FE70",objResult);        $(n).show();        //传递参数        var inputdata = {"hidden_find":"1"};        if(A43B7C_rowCheckData != null){            inputdata["PROC_ID_cn_name"] = A43B7C_rowCheckData[GetLowUpp("INF_CN_NAME")];            inputdata["PROC_ID"] = A43B7C_rowCheckData[GetLowUpp("MAIN_ID")];        }        else{            inputdata["PROC_ID_cn_name"] = "";            inputdata["PROC_ID"] = "-999";        }        loadScript_hasparam("dev_vue/t_proc_inparam.js","A9FE70_t_proc_inparam_biz_start",inputdata);    }    else if(index_subhtml == "t_proc_return.vue"){        var n = Get_RandomDiv("AD21F1",objResult);        $(n).show();        //传递参数        var inputdata = {"hidden_find":"1"};        if(A43B7C_rowCheckData != null){            inputdata["PROC_ID_cn_name"] = A43B7C_rowCheckData[GetLowUpp("INF_CN_NAME")];            inputdata["PROC_ID"] = A43B7C_rowCheckData[GetLowUpp("MAIN_ID")];        }        else{            inputdata["PROC_ID_cn_name"] = "";            inputdata["PROC_ID"] = "-999";        }        loadScript_hasparam("dev_vue/t_proc_return.js","AD21F1_t_proc_return_biz_start",inputdata);    }    else if(index_subhtml == "t_sub_power.vue"){        var n = Get_RandomDiv("AA1D24",objResult);        $(n).show();        //传递参数        var inputdata = {"hidden_find":"1"};        if(A43B7C_rowCheckData != null){            inputdata["PROC_ID_cn_name"] = A43B7C_rowCheckData[GetLowUpp("INF_CN_NAME")];            inputdata["PROC_ID"] = A43B7C_rowCheckData[GetLowUpp("MAIN_ID")];        }        else{            inputdata["PROC_ID_cn_name"] = "";            inputdata["PROC_ID"] = "-999";        }        loadScript_hasparam("dev_vue/t_sub_power.js","AA1D24_t_sub_power_biz_start",inputdata);    }    else if(index_subhtml == "t_sub_userpower.vue"){        var n = Get_RandomDiv("AD0AFB",objResult);        $(n).show();        //传递参数        var inputdata = {"hidden_find":"1"};        if(A43B7C_rowCheckData != null){            inputdata["PROC_ID_cn_name"] = A43B7C_rowCheckData[GetLowUpp("INF_CN_NAME")];            inputdata["PROC_ID"] = A43B7C_rowCheckData[GetLowUpp("MAIN_ID")];        }        else{            inputdata["PROC_ID_cn_name"] = "";            inputdata["PROC_ID"] = "-999";        }        loadScript_hasparam("dev_vue/t_sub_userpower.js","AD0AFB_t_sub_userpower_biz_start",inputdata);    }						  
    else if(index_subhtml == "debug_t_proc_name.vue"){
    	var n = Get_RandomDiv("AH5T84",objResult);	
		layer.open({
			type: 1,
	        area: ['1000px', '800px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
	        	inputdata = {};
	        	loadScript_hasparam("dev_vue/debug_t_proc_name.js","AH5T84_debug_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
    }
    else if(index_subhtml == "set_import_procname.vue"){
    	var n = Get_RandomDiv("A887T1",objResult);	
		layer.open({
			type: 1,
	        area: ['800px', '400px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
	        	inputdata = {};
	        	loadScript_hasparam("dev_vue/set_import_procname.js","A887T1_set_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
    }
	
	/*get tab subvue end*/
}

//调试接口
$("#A43B7C_debug_t_proc_name").click(function () {
	index_subhtml = "debug_t_proc_name.vue";
	if(loadHtmlSubVueFun("dev_vue/debug_t_porc_name.vue","A43B7C_t_proc_name_call_vue") == true){
		var n = Get_RandomDiv("AH5T84","");
		layer.open({
			type: 1,
			area: ['1000px', '800px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				/*Send Two FindSelect param end*/
	        	inputdata = {};
				AH5T84_debug_biz_start(inputdata);
			},
			end: function(){
				$(n).hide();
			}
		});
	}
})

//新增按钮
$("#A43B7C_btn_t_proc_name_add").click(function () {
	A43B7C_type = "add";
	index_subhtml = "t_proc_name_$.vue";
	if(loadHtmlSubVueFun("dev_vue/t_proc_name_$.vue","A43B7C_t_proc_name_call_vue") == true){
		var n = Get_RandomDiv("B43B7C","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				B43B7C_param["type"] = A43B7C_type;
				B43B7C_param["ly_index"]= index;
				if(A43B7C_param.hasOwnProperty("hidden_find")){
					B43B7C_param["hidden_find"] = "1";
				}
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temINF_CN_NAME = $("#A43B7C_qry_INF_CN_NAME").val();                if(temINF_CN_NAME != ""){                    B43B7C_param["INF_CN_NAME"] = temINF_CN_NAME;                }                var temINF_EN_NAME = $("#A43B7C_qry_INF_EN_NAME").val();                if(temINF_EN_NAME != ""){                    B43B7C_param["INF_EN_NAME"] = temINF_EN_NAME;                }                var temIS_AUTHORITY = $("#A43B7C_qry_IS_AUTHORITY").val();                if(temIS_AUTHORITY != ""){                    B43B7C_param["IS_AUTHORITY"] = temIS_AUTHORITY;                }				
				/*Send Two FindSelect param end*/

				B43B7C_clear_edit_info();
			},
			end: function(){
				B43B7C_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#A43B7C_btn_t_proc_name_edit").click(function () {
	if (A43B7C_select_t_proc_name_rowId != "") {
		A43B7C_type = "edit";
		index_subhtml = "t_proc_name_$.vue";
		if(loadHtmlSubVueFun("dev_vue/t_proc_name_$.vue","A43B7C_t_proc_name_call_vue") == true){
			var n = Get_RandomDiv("B43B7C","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					B43B7C_param["type"] = A43B7C_type;
					B43B7C_param["ly_index"] = index;
					if(A43B7C_param.hasOwnProperty("hidden_find")){
						B43B7C_param["hidden_find"] = "1";
					}
					B43B7C_clear_edit_info();
					B43B7C_get_edit_info();
				},
				end: function(){
					B43B7C_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#A43B7C_btn_t_proc_name_delete').click(function () {
	//单行选择
	var rowData = $("#A43B7C_t_proc_name_Events").bootstrapTable('getData')[A43B7C_select_t_proc_name_rowId];
	//多行选择
	var rowDatas = A43B7C_sel_row_t_proc_name();
	if (A43B7C_select_t_proc_name_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_proc_name",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "A43B7C_N01_del_t_proc_name");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_proc_name",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "A43B7C_N01_del_t_proc_name");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function A43B7C_N01_del_t_proc_name(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_proc_name) == true)
		A43B7C_t_proc_name_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function A43B7C_adjust_tab(){
	if(typeof($("#A43B7C_TAB_MAIN")[0]) != "undefined" && $("#A43B7C_TAB_MAIN")[0].length != 0){
		A43B7C_Tab_Flag = 1;
		$(Get_RDivNoBuild("A43B7C","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("A43B7C","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("A43B7C","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#A43B7C_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/
$("#A43B7C_tab_1").click(function(){    A43B7C_Tab_Flag = 1;    A43B7C_hide_tab_fun();    //主子表参数传递    var temPar = [{"sourc_id":"MAIN_ID","target_id":"PROC_ID"},{"sourc_id":"INF_CN_NAME","target_id":"PROC_ID_cn_name"}];    A43B7C_show_tab_fun("t_proc_inparam.vue","A9FE70",temPar);});$("#A43B7C_tab_2").click(function(){    A43B7C_Tab_Flag = 2;    A43B7C_hide_tab_fun();    //主子表参数传递    var temPar = [{"sourc_id":"MAIN_ID","target_id":"PROC_ID"},{"sourc_id":"INF_CN_NAME","target_id":"PROC_ID_cn_name"}];    A43B7C_show_tab_fun("t_proc_return.vue","AD21F1",temPar);});$("#A43B7C_tab_3").click(function(){    A43B7C_Tab_Flag = 3;    A43B7C_hide_tab_fun();    //主子表参数传递    var temPar = [{"sourc_id":"MAIN_ID","target_id":"PROC_ID"},{"sourc_id":"INF_CN_NAME","target_id":"PROC_ID_cn_name"}];    A43B7C_show_tab_fun("t_sub_power.vue","AA1D24",temPar);});$("#A43B7C_tab_4").click(function(){    A43B7C_Tab_Flag = 4;    A43B7C_hide_tab_fun();    //主子表参数传递    var temPar = [{"sourc_id":"MAIN_ID","target_id":"PROC_ID"},{"sourc_id":"INF_CN_NAME","target_id":"PROC_ID_cn_name"}];    A43B7C_show_tab_fun("t_sub_userpower.vue","AD0AFB",temPar);});//隐藏tab页选项卡function A43B7C_hide_tab_fun(){    var n = null;    n = Get_RDivNoBuild("A9FE70","");    $(n).hide();    n = Get_RDivNoBuild("AD21F1","");    $(n).hide();    n = Get_RDivNoBuild("AA1D24","");    $(n).hide();    n = Get_RDivNoBuild("AD0AFB","");    $(n).hide();}//判断是否sub子项div页面function A43B7C_Is_Sub_Div(temDivId){    if(temDivId.indexOf("XX$TTT") == 0)        return false;    else if(temDivId.indexOf("A9FE70") == 0)        return true;    else if(temDivId.indexOf("AD21F1") == 0)        return true;    else if(temDivId.indexOf("AA1D24") == 0)        return true;    else if(temDivId.indexOf("AD0AFB") == 0)        return true;    return false;}
/*Tab Click Fun End*/

function A43B7C_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("dev_vue/"+inputUrl,"A43B7C_t_proc_name_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (A43B7C_select_t_proc_name_rowId != "") 
			rowData = $("#A43B7C_t_proc_name_Events").bootstrapTable('getData')[A43B7C_select_t_proc_name_rowId];
		else
			rowData = A43B7C_rowCheckData;
		//$("#A43B7C_t_proc_name_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			var temFlag = true;
			$.each(temPar, function (i, obj) {
				if(eval(inputrandom+"_param.hasOwnProperty(\""+obj.target_id+"\")") && eval(inputrandom+"_param[\""+obj.target_id+"\"]").toString() == rowData[obj.sourc_id].toString())
					temFlag = false;
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			if(temFlag){
				//传递子页面隐藏find功能
				eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
				//参数传递并赋值
				eval(inputrandom+"_param_set()");
				var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
				$("#"+inputrandom+"_btn_"+tbName+"_query").click();
			}
		}
		else{
			$.each(temPar, function (i, obj) {
				if(obj.sourc_id.toUpperCase() == "MAIN_ID")
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"-999\"");
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"\"");
			});
			//传递子页面隐藏find功能
			eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
			var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
			$("#"+inputrandom+"_btn_"+tbName+"_query").click();		
		}
	}
}

//清除 查找框
function A43B7C_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function A43B7C_sel_row_t_proc_name(){
	//获得选中行
	var checkedbox= $("#A43B7C_t_proc_name_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}

//获取选中行
function A43B7C_getSelectedMain_id(objType){  
	//多行选择
	var rowDatas = A43B7C_sel_row_t_proc_name();
	var main_ids = "";
	//遍历访问这个集合  
	$(rowDatas).each(function (index, obj){  
		if(main_ids == "")
			main_ids = obj.MAIN_ID;
		else
			main_ids += ","+obj.MAIN_ID;
	});
	
	//导出接口
	if(objType == 1)
	{
		var inputdata = {"param_name":"A01_OUTPROC","param_value1":main_ids,"session_id":session_id};
		get_ajax_baseurl(inputdata, "A43B7C_A01_OUTPROC");
	}
	//权限认证(是)
	else if(objType == 2)
	{
		var inputdata = {"param_name":"A01_SETAUTHORITY","param_value1":main_ids,"param_value2":1,"session_id":session_id};
		get_ajax_baseurl(inputdata, "A43B7C_A01_SETAUTHORITY");
	}
	//权限认证(否)
	else if(objType == 3)
	{
		var inputdata = {"param_name":"A01_SETAUTHORITY","param_value1":main_ids,"param_value2":0,"session_id":session_id};
		get_ajax_baseurl(inputdata, "A43B7C_A01_SETAUTHORITY");
	}
}

//设置接口权限鉴证
function A43B7C_A01_SETAUTHORITY(input){
	layer.close(ly_index);
    //查询失败
    if (Call_OpeResult(input.A01_SETAUTHORITY) == false)
        return false;
    A43B7C_t_proc_name_query();
}

//导入接口数据
function A43B7C_A01_INPROC_IN(input){
	layer.close(ly_index);
    //查询失败
    if (Call_OpeResult(input.A01_OUTPROC) == false)
        return false;
    A43B7C_t_proc_name_query();
}

//导出接口数据
function A43B7C_A01_OUTPROC(input){
    layer.close(ly_index);
    //查询失败
    if (Call_OpeResult(input.A01_OUTPROC) == false)
        return false;
    else
    {
    	var content = JSON.stringify(input.A01_OUTPROC);
    	var blob = new Blob([content], {type: "text/plain;charset=utf-8"});    	
    	var fileName = new Date().Format('yyyyMMddhhmmss')+"proc.json";    	
    	saveAs(blob, fileName);
    }
}

//导出接口
$("#A43B7C_load_out_proc_name").click(function () {
	A43B7C_getSelectedMain_id(1);
});

//导入接口
$("#A43B7C_load_in_proc_name").click(function () {
	index_subhtml = "set_import_procname.vue";
	if(loadHtmlSubVueFun("dev_vue/set_import_procname.vue","A43B7C_t_proc_name_call_vue") == true){
		var n = Get_RandomDiv("A887T1","");
		layer.open({
			type: 1,
			area: ['800px', '400px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				/*Send Two FindSelect param end*/
	        	inputdata = {};
	        	A887T1_Clear();
	        	A887T1_set_biz_start(inputdata);
			},
			end: function(){
				$(n).hide();
			}
		});
	}
});

//权限认证(是)
$("#A43B7C_yes_authority").click(function () {
	A43B7C_getSelectedMain_id(2);
});

//权限认证(否)
$("#A43B7C_no_authority").click(function () {
	A43B7C_getSelectedMain_id(3);
});