#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.z_debug import z_debug
class A01_DevGrants(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None  
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    param_value11     = None  
    param_value12     = None
    param_value13     = None
    param_value14     = None
    param_value15     = None
    param_value16     = None
    param_value17     = None
    param_value18     = None
    param_value19     = None
    param_value20     = None
    strContinue      = "1"
    def __init__(self):
        pass
    def init_start(self):       
        temOpd = Ope_DB_Cent()
        if temOpd.Get_eova_user_rid(Com_Fun.getLogin_Id(self.session_id)) != 1:
            self.strResult = "{\""+self.param_name+"\": [{\""+Com_Fun.GetLowUpp("s_result")+"\": 0, \""+Com_Fun.GetLowUpp("error_desc")+"\": \"禁止操作，用户权限不足!\"}]}"
            self.strContinue = "0"
