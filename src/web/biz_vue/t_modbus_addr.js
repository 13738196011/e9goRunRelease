//选择某一行
var AE98AD_select_t_modbus_addr_rowId = "";
//按钮事件新增或编辑
var AE98AD_type = "";
//其他页面传到本页面参数
var AE98AD_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var AE98AD_tem_com_port = "";var AE98AD_tem_element_id = "0";var AE98AD_tem_protocol_id = "0";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var AE98AD_ary_com_port = null;var AE98AD_ary_element_id = null;var AE98AD_ary_protocol_id = null;
/*declare select options end*/

//业务逻辑数据开始
function AE98AD_t_modbus_addr_biz_start(inputparam) {
	layer.close(ly_index);
	AE98AD_param = inputparam;
    /*biz begin*/
    var inputdata = {        "param_name": "T01_t_protocol$com_port",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE98AD_get_T01_t_protocol$com_port");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function AE98AD_format_com_port(value, row, index) {    var objResult = value;    for(i = 0; i < AE98AD_ary_com_port.length; i++) {        var obj = AE98AD_ary_com_port[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AE98AD_format_element_id(value, row, index) {    var objResult = value;    for(i = 0; i < AE98AD_ary_element_id.length; i++) {        var obj = AE98AD_ary_element_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AE98AD_format_protocol_id(value, row, index) {    var objResult = value;    for(i = 0; i < AE98AD_ary_protocol_id.length; i++) {        var obj = AE98AD_ary_protocol_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AE98AD_get_T01_t_protocol$com_port(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_t_protocol$com_port) == false)        return false;    AE98AD_ary_com_port = input.T01_t_protocol$com_port;    var inputdata = {        "param_name": "T01_t_prot_ele_link$element_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE98AD_get_T01_t_prot_ele_link$element_id");}function AE98AD_get_T01_t_prot_ele_link$element_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_t_prot_ele_link$element_id) == false)        return false;    AE98AD_ary_element_id = input.T01_t_prot_ele_link$element_id;    var inputdata = {        "param_name": "T01_t_prot_ele_link$prot_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE98AD_get_T01_t_prot_ele_link$prot_id");}function AE98AD_get_T01_t_prot_ele_link$prot_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_t_prot_ele_link$prot_id) == false)        return false;    AE98AD_ary_protocol_id = input.T01_t_prot_ele_link$prot_id;    $("#AE98AD_qry_com_port").append("<option value='-1'></option>")    $.each(AE98AD_ary_com_port, function (i, obj) {        addOptionValue("AE98AD_qry_com_port", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    $("#AE98AD_qry_element_id").append("<option value='-1'></option>")    $.each(AE98AD_ary_element_id, function (i, obj) {        addOptionValue("AE98AD_qry_element_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    $("#AE98AD_qry_protocol_id").append("<option value='-1'></option>")    $.each(AE98AD_ary_protocol_id, function (i, obj) {        addOptionValue("AE98AD_qry_protocol_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    AE98AD_init_t_modbus_addr();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function AE98AD_page_end(){
}

//协议因子地址对照显示列定义
var AE98AD_t_modbus_addr = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '设备网关地址',        field: 'MODBUS_GATEWAY',        sortable: true        ,formatter: set_s_decode    },    {        title: '串口号',        field: 'COM_PORT',        sortable: true        ,formatter: AE98AD_format_com_port    },    {        title: '设备地址',        field: 'MODBUS_ADDR',        sortable: true        ,formatter: set_s_decode    },    {        title: '起始位或4017通道号',        field: 'MODBUS_BEGIN',        sortable: true        ,formatter: set_s_decode    },    {        title: '数据长度',        field: 'MODBUS_LENGTH',        sortable: true    },    {        title: '因子名称',        field: 'ELEMENT_ID',        sortable: true        ,formatter: AE98AD_format_element_id    },    {        title: '采集协议模块',        field: 'PROTOCOL_ID',        sortable: true        ,formatter: AE98AD_format_protocol_id    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function AE98AD_init_t_modbus_addr() {
	$(window).resize(function () {
		  $('#AE98AD_t_modbus_addr_Events').bootstrapTable('resetView');
	});
	//协议因子地址对照查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AE98AD_btn_t_modbus_addr_query').click();
}

//查询接口
function AE98AD_t_modbus_addr_query() {
    $('#AE98AD_t_modbus_addr_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_modbus_addr",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(AE98AD_tem_com_port)        ,"param_value2": AE98AD_tem_element_id        ,"param_value3": AE98AD_tem_element_id        ,"param_value4": AE98AD_tem_protocol_id        ,"param_value5": AE98AD_tem_protocol_id		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AE98AD_get_N01_sel_t_modbus_addr");
}

//查询结果
function AE98AD_get_N01_sel_t_modbus_addr(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_modbus_addr) == false)
        return false;    
	var s_data = input.N01_sel_t_modbus_addr;
    AE98AD_select_t_modbus_addr_rowId = "";
    $('#AE98AD_t_modbus_addr_Events').bootstrapTable('destroy');
    $("#AE98AD_t_modbus_addr_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AE98AD_t_modbus_addr_Events').find("tr.changeColor").removeClass('changeColor');
                AE98AD_select_t_modbus_addr_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AE98AD_t_modbus_addr_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AE98AD_select_t_modbus_addr_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AE98AD_param.hasOwnProperty("target_name"))
			{
				$("#"+AE98AD_param["target_id"]).val(eval("row."+AE98AD_param["sourc_id"].toUpperCase()));
				$("#"+AE98AD_param["target_name"]).val(s_decode(eval("row."+AE98AD_param["sourc_name"].toUpperCase())));				
				layer.close(AE98AD_param["ly_index"]);
			}
		},
        toolbar: "#AE98AD_t_modbus_addr_Toolbar",
        columns: AE98AD_t_modbus_addr,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AE98AD_btn_t_modbus_addr_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    AE98AD_tem_com_port = "";    AE98AD_tem_element_id = "0";    AE98AD_tem_protocol_id = "0";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    BE98AD_ary_com_port = null;    BE98AD_ary_element_id = null;    BE98AD_ary_protocol_id = null;
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "T01_t_protocol$com_port",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE98AD_get_T01_t_protocol$com_port");	
    /*biz end*/
})

//查询按钮
$('#AE98AD_btn_t_modbus_addr_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    AE98AD_tem_com_port = $("#AE98AD_qry_com_port").val();    AE98AD_tem_element_id = $("#AE98AD_qry_element_id").val();    AE98AD_tem_protocol_id = $("#AE98AD_qry_protocol_id").val();	
    /*set query param end*/
	AE98AD_t_modbus_addr_query();
})

//vue回调
function AE98AD_t_modbus_addr_call_vue(objResult){
	if(index_subhtml == "t_modbus_addr_$.vue")
	{
		var n = Get_RandomDiv("BE98AD",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AE98AD_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temcom_port = $("#AE98AD_qry_com_port").val();                if(temcom_port != ""){                    inputdata["com_port"] = temcom_port;                }                var temelement_id = $("#AE98AD_qry_element_id").val();                if(temelement_id != ""){                    inputdata["element_id"] = temelement_id;                }                var temprotocol_id = $("#AE98AD_qry_protocol_id").val();                if(temprotocol_id != ""){                    inputdata["protocol_id"] = temprotocol_id;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_modbus_addr_$.js","BE98AD_t_modbus_addr_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//新增按钮
$("#AE98AD_btn_t_modbus_addr_add").click(function () {
	AE98AD_type = "add";
	index_subhtml = "t_modbus_addr_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_modbus_addr_$.vue","AE98AD_t_modbus_addr_call_vue") == true){
		var n = Get_RandomDiv("BE98AD","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BE98AD_param["type"] = AE98AD_type;
				BE98AD_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temcom_port = $("#AE98AD_qry_com_port").val();                if(temcom_port != ""){                    BE98AD_param["com_port"] = temcom_port;                }                var temelement_id = $("#AE98AD_qry_element_id").val();                if(temelement_id != ""){                    BE98AD_param["element_id"] = temelement_id;                }                var temprotocol_id = $("#AE98AD_qry_protocol_id").val();                if(temprotocol_id != ""){                    BE98AD_param["protocol_id"] = temprotocol_id;                }				
				/*Send Two FindSelect param end*/

				BE98AD_clear_edit_info();
			},
			end: function(){
				BE98AD_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AE98AD_btn_t_modbus_addr_edit").click(function () {
	if (AE98AD_select_t_modbus_addr_rowId != "") {
		AE98AD_type = "edit";
		index_subhtml = "t_modbus_addr_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_modbus_addr_$.vue","AE98AD_t_modbus_addr_call_vue") == true){
			var n = Get_RandomDiv("BE98AD","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BE98AD_param["type"] = AE98AD_type;
					BE98AD_param["ly_index"] = index;
					BE98AD_clear_edit_info();
					BE98AD_get_edit_info();
				},
				end: function(){
					BE98AD_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AE98AD_btn_t_modbus_addr_delete').click(function () {
	//单行选择
	var rowData = $("#AE98AD_t_modbus_addr_Events").bootstrapTable('getData')[AE98AD_select_t_modbus_addr_rowId];
	//多行选择
	var rowDatas = AE98AD_sel_row_t_modbus_addr();
	if (AE98AD_select_t_modbus_addr_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_modbus_addr",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AE98AD_N01_del_t_modbus_addr");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_modbus_addr",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AE98AD_N01_del_t_modbus_addr");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AE98AD_N01_del_t_modbus_addr(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_modbus_addr) == true)
		AE98AD_t_modbus_addr_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function AE98AD_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AE98AD_sel_row_t_modbus_addr(){
	//获得选中行
	var checkedbox= $("#AE98AD_t_modbus_addr_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}