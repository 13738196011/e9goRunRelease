#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''
import json,os,importlib
from com.zxy.common import Com_Para
from com.zxy.db1.Db_Common1 import Db_Common1
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.plugins.usereflect.A01_STATICDB2_IN import A01_STATICDB2_IN
from com.plugins.B01_2HCM66.B01_2HCM66_Time import B01_2HCM66_Time
from com.zxy.business.Power_Control import Power_Control
from com.zxy.z_debug import z_debug
from com.zxy.common.Com_Fun import Com_Fun
class A01_YY77HH_IN(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = 1
    def __init__(self):
        pass
    def init_start(self):
        self.strContinue = 0
        bodc = Ope_DB_Cent()
        try:
            temdbc2 = Db_Common2()   
            temdbc1 = Db_Common1()  
            temStrSql = "select MAIN_ID,TABLE_CN_NAME,TABLE_EN_NAME from t_single_table where main_id = '"+self.param_value1+"'"
            temResultSet = temdbc2.Common_Sql(temStrSql)
            if len(temResultSet) > 0:
                temTableName = Com_Fun.NoNull(temResultSet[0][2])
                if temTableName == "":
                    self.strResult = "{\""+self.param_name+"\":[{\"s_result\":\"0\",\"error_desc\":\"元数据表重置业务失败，元数据表名不能为空\"}]}"
                    return None 
                strSql = "drop table "+temTableName
                temdbc2.CommonExec_Sql(strSql)            
                strSql = "delete from t_proc_name where INF_EN_NAME in ('T01_sel_"+temTableName+"_ByID','T01_sel_"+temTableName+"','T01_ins_"+temTableName+"','T01_upd_"+temTableName+"','T01_del_"+temTableName+"')"
                temdbc1.CommonExec_Sql(strSql)       
                temPcl = Power_Control(Com_Para.sub_code,Com_Para.sub_usercode,"T01_truncate_table","")  
                bodc.Cal_Data(temPcl,Com_Para.sub_code,"T01_truncate_table",[],"","",{})        
                b2t = B01_2HCM66_Time()
                b2t.Delete_SingleTable_Py(temTableName)
                self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"1\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"元数据表重置业务成功\"}]}"                
            else:
                self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"元数据表重置业务失败，元数据表记录不存在\"}]}"
                return None
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+repr(e)
            else:
                temLog = self.debug_info()+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"异常:"+repr(e)+"\"}]}"