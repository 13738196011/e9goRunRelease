#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''
import json,os
from com.zxy.common import Com_Para
from com.zxy.db_Self.Db_Common_Self import Db_Common_Self
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.z_debug import z_debug
class A01_SETAUTHORITY_IN(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = 1
    def __init__(self):
        pass
    def init_start(self):
        try:
            temDbSelf = Db_Common_Self()
            strRunSql = "update t_proc_name set IS_AUTHORITY = '"+self.param_value2+"' where main_id in ('" + self.param_value1.replace(",","','") + "')"
            temDbSelf.CommonExec_Sql(strRunSql)                 
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"1\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"权限认证设置成功\"}]}"
        except Exception as e:            
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+repr(e)
            else:
                temLog = self.debug_info()+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"异常:"+repr(e)+"\"}]}"
        self.strContinue = 0