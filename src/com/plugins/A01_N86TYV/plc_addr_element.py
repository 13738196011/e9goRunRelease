#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class plc_addr_element(object):
    attmain_id = -1
    attp_main_id = -1
    attelement_code = ""
    attdata_type = ""
    attdata_area = ""
    attdb_num = ""
    attaddress = ""
    attcreate_date = None
    atts_desc = ""
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")