//按钮事件新增或编辑
var B29E3D_type = "";
//其他页面传到本页面参数
var B29E3D_param = {};
//暂时没用
var B29E3D_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var B29E3D_ary_SUB_USER_ID = null;var B29E3D_ary_PROC_ID = null;var B29E3D_ary_LIMIT_TAG = [{'MAIN_ID': '1', 'CN_NAME': '年'}, {'MAIN_ID': '2', 'CN_NAME': '月'}, {'MAIN_ID': '3', 'CN_NAME': '日'}];
/*declare select options end*/

//业务逻辑数据开始
function B29E3D_t_sub_userpower_biz_start(inputdata) {
	B29E3D_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_sub_userpower$SUB_USER_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B29E3D_get_N01_t_sub_userpower$SUB_USER_ID");	
    /*biz end*/
}

/*biz step begin*/
function B29E3D_format_SUB_USER_ID(value, row, index) {    var objResult = value;    for(i = 0; i < B29E3D_ary_SUB_USER_ID.length; i++) {        var obj = B29E3D_ary_SUB_USER_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B29E3D_format_PROC_ID(value, row, index) {    var objResult = value;    for(i = 0; i < B29E3D_ary_PROC_ID.length; i++) {        var obj = B29E3D_ary_PROC_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B29E3D_format_LIMIT_TAG(value, row, index) {    var objResult = value;    for(i = 0; i < B29E3D_ary_LIMIT_TAG.length; i++) {        var obj = B29E3D_ary_LIMIT_TAG[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B29E3D_get_N01_t_sub_userpower$SUB_USER_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_sub_userpower$SUB_USER_ID) == false)        return false;    B29E3D_ary_SUB_USER_ID = input.N01_t_sub_userpower$SUB_USER_ID;    if($("#B29E3D_SUB_USER_ID").is("select") && $("#B29E3D_SUB_USER_ID")[0].options.length == 0)    {        $.each(B29E3D_ary_SUB_USER_ID, function (i, obj) {            addOptionValue("B29E3D_SUB_USER_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    var inputdata = {        "param_name": "N01_t_proc_inparam$PROC_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B29E3D_get_N01_t_proc_inparam$PROC_ID");}function B29E3D_get_N01_t_proc_inparam$PROC_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_inparam$PROC_ID) == false)        return false;    B29E3D_ary_PROC_ID = input.N01_t_proc_inparam$PROC_ID;    if($("#B29E3D_PROC_ID").is("select") && $("#B29E3D_PROC_ID")[0].options.length == 0)    {        $.each(B29E3D_ary_PROC_ID, function (i, obj) {            addOptionValue("B29E3D_PROC_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#B29E3D_LIMIT_TAG").is("select") && $("#B29E3D_LIMIT_TAG")[0].options.length == 0)    {        $.each(B29E3D_ary_LIMIT_TAG, function (i, obj) {            addOptionValue("B29E3D_LIMIT_TAG", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    B29E3D_init_t_sub_userpower();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function B29E3D_PROC_ID_cn_name_fun(){    index_subhtml = "t_proc_name.vue"    random_subhtml = "A1BCF7";    if(loadHtmlSubVueFun("biz_vue/t_proc_name.vue","B29E3D_t_sub_userpower_call_vue") == true){        var n = Get_RandomDiv("A1BCF7","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#A1BCF7_t_proc_name_Events').bootstrapTable('resetView');                A1BCF7_param["ly_index"] = index;                A1BCF7_param["target_name"] = "B29E3D_find_PROC_ID_cn_name"                A1BCF7_param["target_id"] = "B29E3D_PROC_ID"                A1BCF7_param["sourc_id"] = "MAIN_ID"                A1BCF7_param["sourc_name"] = "INF_CN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function B29E3D_page_end(){
	if(B29E3D_param["type"] == "edit"){
		B29E3D_get_edit_info();
	}
}

//页面初始化方法
function B29E3D_init_t_sub_userpower() {
	//type = getUrlParam("type");
	if(B29E3D_param["type"] == "add"){
		$("#B29E3D_main_id").parent().parent().parent().hide();
		$("#B29E3D_LIMIT_TAG").val("3");
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#B29E3D_SUB_USER_ID").val(B29E3D_param["SUB_USER_ID"]);        $("#B29E3D_PROC_ID").val(B29E3D_param["PROC_ID"]);        $("#B29E3D_find_PROC_ID_cn_name").val(B29E3D_param["PROC_ID_cn_name"]);		
		/*Get Find Select param end*/
	}
	else if(B29E3D_param["type"] == "edit"){
	
	}
	
	//表单验证
	B29E3D_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B29E3D_LIMIT_DATE").val() == "")    {        $("#B29E3D_LIMIT_DATE").val(DateAdd("y",50,new Date()).Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B29E3D_LIMIT_DATE',        type: 'datetime',        trigger: 'click'    });    if($("#B29E3D_FIRST_DATE").val() == "")    {        $("#B29E3D_FIRST_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B29E3D_FIRST_DATE',        type: 'datetime',        trigger: 'click'    });    if($("#B29E3D_CREATE_DATE").val() == "")    {        $("#B29E3D_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B29E3D_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B29E3D_page_end();
}

//提交表单数据
function B29E3D_SubmitForm(){
	if(B29E3D_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_sub_userpower",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#B29E3D_SUB_USER_ID").val()                ,"param_value2": $("#B29E3D_PROC_ID").val()                ,"param_value3": $("#B29E3D_LIMIT_DATE").val()                ,"param_value4": $("#B29E3D_USE_LIMIT").val()                ,"param_value5": $("#B29E3D_LIMIT_NUMBER").val()                ,"param_value6": $("#B29E3D_LIMIT_TAG").val()                ,"param_value7": $("#B29E3D_FIRST_DATE").val()                ,"param_value8": s_encode($("#B29E3D_S_DESC").val())                ,"param_value9": $("#B29E3D_CREATE_DATE").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B29E3D_get_N01_ins_t_sub_userpower");
	}
	else if(B29E3D_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_sub_userpower",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#B29E3D_SUB_USER_ID").val()                ,"param_value2": $("#B29E3D_PROC_ID").val()                ,"param_value3": $("#B29E3D_LIMIT_DATE").val()                ,"param_value4": $("#B29E3D_USE_LIMIT").val()                ,"param_value5": $("#B29E3D_LIMIT_NUMBER").val()                ,"param_value6": $("#B29E3D_LIMIT_TAG").val()                ,"param_value7": $("#B29E3D_FIRST_DATE").val()                ,"param_value8": s_encode($("#B29E3D_S_DESC").val())                ,"param_value9": $("#B29E3D_CREATE_DATE").val()                ,"param_value10": $("#B29E3D_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B29E3D_get_N01_upd_t_sub_userpower");
	}
}

//vue回调
function B29E3D_t_sub_userpower_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_proc_name.vue"){        var n = Get_RandomDiv("A1BCF7",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":B29E3D_type,                    "ly_index":index,                    "target_name":"B29E3D_find_PROC_ID_cn_name",                    "target_id":"B29E3D_find_PROC_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"INF_CN_NAME"                };                loadScript_hasparam("biz_vue/t_proc_name.js","A1BCF7_t_proc_name_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//for表单提交
$("#B29E3D_save_t_sub_userpower_Edit").click(function () {
	$("form[name='B29E3D_DataModal']").submit();
})

/*修改数据*/
function B29E3D_get_N01_upd_t_sub_userpower(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_sub_userpower) == true)
	{
		swal("修改数据成功!", "", "success");
		A29E3D_t_sub_userpower_query();
		B29E3D_clear_validate();
		layer.close(B29E3D_param["ly_index"]);
	}
}

/*添加数据*/
function B29E3D_get_N01_ins_t_sub_userpower(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_sub_userpower) == true)
	{
		swal("添加数据成功!", "", "success");
		A29E3D_t_sub_userpower_query();
		B29E3D_clear_validate();
		layer.close(B29E3D_param["ly_index"]);
	}
}

//取消编辑
$("#B29E3D_cancel_t_sub_userpower_Edit").click(function () {
	layer.close(B29E3D_param["ly_index"]);
	B29E3D_clear_validate();
	$("[id^='B29E3D_div']").hide();
})

//清除查找框
function B29E3D_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B29E3D_clear_validate(){
	$("#B29E3D_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B29E3D_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B29E3D_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B29E3D_clear_edit_info(){
	var inputs = $("#B29E3D_DataModal").find('input');
	var selects = $("#B29E3D_DataModal").find("select");
	var textareas = $("#B29E3D_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
    B29E3D_clear_input_cn_name('B29E3D_find_PROC_ID_cn_name','B29E3D_PROC_ID')	
	/*input validate clear end*/
	B29E3D_init_t_sub_userpower();
}

//页面输入框赋值
function B29E3D_get_edit_info(){
	var rowData = $("#A29E3D_t_sub_userpower_Events").bootstrapTable('getData')[A29E3D_select_t_sub_userpower_rowId];
	var inputs = $("#B29E3D_DataModal").find('input');
	var selects = $("#B29E3D_DataModal").find("select");
	var textareas = $("#B29E3D_DataModal").find('textarea');
	$.each(selects, function (i, obj) {
		if(typeof(eval("A29E3D_ary_"+obj.id.toString().replace("B29E3D_",""))) == "object")
		{
			$.each(eval("A29E3D_ary_"+obj.id.toString().replace("B29E3D_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("B29E3D_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("B29E3D_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("A29E3D_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#B29E3D_DataModal").find('[id="'+"B29E3D_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("B29E3D_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("B29E3D_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//form验证
function B29E3D_checkFormInput() {
    B29E3D_validate = $("#B29E3D_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B29E3D_main_id: {}            ,B29E3D_SUB_USER_ID: {}            ,B29E3D_PROC_ID: {}            ,B29E3D_LIMIT_DATE: {date: true,required : true,maxlength:19}            ,B29E3D_USE_LIMIT: {digits: true,required : true,maxlength:10}            ,B29E3D_LIMIT_NUMBER: {digits: true,required : true,maxlength:10}            ,B29E3D_LIMIT_TAG: {}            ,B29E3D_FIRST_DATE: {date: true,required : true,maxlength:19}            ,B29E3D_S_DESC: {}            ,B29E3D_CREATE_DATE: {date: true,required : true,maxlength:19}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B29E3D_main_id: {}            ,B29E3D_SUB_USER_ID: {}            ,B29E3D_PROC_ID: {}            ,B29E3D_LIMIT_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B29E3D_USE_LIMIT: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,B29E3D_LIMIT_NUMBER: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,B29E3D_LIMIT_TAG: {}            ,B29E3D_FIRST_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B29E3D_S_DESC: {}            ,B29E3D_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B29E3D_SubmitForm();
        	return false;
        }
    })
}