//按钮事件新增或编辑
var BF958F_type = "";
//其他页面传到本页面参数
var BF958F_param = {};
//暂时没用
var BF958F_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BF958F_ary_rid = [{'MAIN_ID': '1', 'CN_NAME': '超级管理员'}, {'MAIN_ID': '2', 'CN_NAME': '系统管理员'}, {'MAIN_ID': '3', 'CN_NAME': '运维人员'}, {'MAIN_ID': '4', 'CN_NAME': '普通用户'}];var BF958F_ary_S_STATE = [{'MAIN_ID': '0', 'CN_NAME': '启用'}, {'MAIN_ID': '1', 'CN_NAME': '禁用'}];
/*declare select options end*/

//主从表传递参数
function BF958F_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function BF958F_eova_user_biz_start(inputdata) {
	BF958F_param = inputdata;
	layer.close(ly_index);
	BF958F_param_set();
    /*biz begin*/
    $.each(BF958F_ary_rid, function (i, obj) {        addOptionValue("BF958F_rid", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    $.each(BF958F_ary_S_STATE, function (i, obj) {        addOptionValue("BF958F_S_STATE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    BF958F_init_eova_user()	
    /*biz end*/
}

/*biz step begin*/
function BF958F_format_rid(value, row, index) {    var objResult = value;    for(i = 0; i < BF958F_ary_rid.length; i++) {        var obj = BF958F_ary_rid[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BF958F_format_S_STATE(value, row, index) {    var objResult = value;    for(i = 0; i < BF958F_ary_S_STATE.length; i++) {        var obj = BF958F_ary_S_STATE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function BF958F_page_end(){
	if(BF958F_param["type"] == "edit"){
		BF958F_get_edit_info();
		$("#BF958F_login_pwd").val("*");
	}
}

//页面初始化方法
function BF958F_init_eova_user() {
	//type = getUrlParam("type");
	if(BF958F_param["type"] == "add"){
		$("#BF958F_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BF958F_login_id").val(BF958F_param["login_id"]);        $("#BF958F_nickname").val(BF958F_param["nickname"]);		
		/*Get Find Select param end*/	
	}
	else if(BF958F_param["type"] == "edit"){
	
	}
	
	//表单验证
	BF958F_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BF958F_CREATE_DATE").val() == "")    {        $("#BF958F_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BF958F_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BF958F_page_end();
}

//提交表单数据
function BF958F_SubmitForm(){
	if(BF958F_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_eova_user",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#BF958F_login_id").val())                ,"param_value2": s_encode($("#BF958F_login_pwd").val())                ,"param_value3": s_encode($("#BF958F_nickname").val())                ,"param_value4": $("#BF958F_rid").val()                ,"param_value5": $("#BF958F_S_STATE").val()                ,"param_value6": $("#BF958F_CREATE_DATE").val()                ,"param_value7": s_encode($("#BF958F_S_DESC").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BF958F_get_N01_ins_eova_user");
	}
	else if(BF958F_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_eova_user",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#BF958F_login_id").val())                ,"param_value2": s_encode($("#BF958F_login_pwd").val())                ,"param_value3": s_encode($("#BF958F_nickname").val())                ,"param_value4": $("#BF958F_rid").val()                ,"param_value5": $("#BF958F_S_STATE").val()                ,"param_value6": $("#BF958F_CREATE_DATE").val()                ,"param_value7": s_encode($("#BF958F_S_DESC").val())                ,"param_value8": $("#BF958F_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BF958F_get_N01_upd_eova_user");
	}
}

//vue回调
function BF958F_eova_user_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#BF958F_save_eova_user_Edit").click(function () {
	$("form[name='BF958F_DataModal']").submit();
})

/*修改数据*/
function BF958F_get_N01_upd_eova_user(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_eova_user) == true)
	{
		swal("修改数据成功!", "", "success");
		AF958F_eova_user_query();
		BF958F_clear_validate();
		layer.close(BF958F_param["ly_index"]);
	}
}

/*添加数据*/
function BF958F_get_N01_ins_eova_user(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_eova_user) == true)
	{
		swal("添加数据成功!", "", "success");
		AF958F_eova_user_query();
		BF958F_clear_validate();
		layer.close(BF958F_param["ly_index"]);
	}
}

//取消编辑
$("#BF958F_cancel_eova_user_Edit").click(function () {
	layer.close(BF958F_param["ly_index"]);
	BF958F_clear_validate();
	$("[id^='BF958F_div']").hide();
})

//清除查找框
function BF958F_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BF958F_clear_validate(){
	$("#BF958F_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BF958F_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BF958F_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BF958F_clear_edit_info(){
	var inputs = $("#BF958F_DataModal").find('input');
	var selects = $("#BF958F_DataModal").find("select");
	var textareas = $("#BF958F_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	BF958F_init_eova_user();
}

//页面输入框赋值
function BF958F_get_edit_info(){
	var rowData = $("#AF958F_eova_user_Events").bootstrapTable('getData')[AF958F_select_eova_user_rowId];
	var inputs = $("#BF958F_DataModal").find('input');
	var selects = $("#BF958F_DataModal").find("select");
	var textareas = $("#BF958F_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"AF958F","BF958F");
}

//form验证
function BF958F_checkFormInput() {
    BF958F_validate = $("#BF958F_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BF958F_id: {}            ,BF958F_login_id: {}            ,BF958F_login_pwd: {}            ,BF958F_nickname: {}            ,BF958F_rid: {}            ,BF958F_S_STATE: {}            ,BF958F_CREATE_DATE: {date: true,required : true,maxlength:19}            ,BF958F_S_DESC: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BF958F_id: {}            ,BF958F_login_id: {}            ,BF958F_login_pwd: {}            ,BF958F_nickname: {}            ,BF958F_rid: {}            ,BF958F_S_STATE: {}            ,BF958F_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BF958F_S_DESC: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BF958F_SubmitForm();
        	return false;
        }
    })
}