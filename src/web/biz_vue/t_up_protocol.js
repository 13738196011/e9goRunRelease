//选择某一行
var AAE645_select_t_up_protocol_rowId = "";
//按钮事件新增或编辑
var AAE645_type = "";
//其他页面传到本页面参数
var AAE645_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var AAE645_tem_cn_name = "";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var AAE645_ary_s_state = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];
/*declare select options end*/

//业务逻辑数据开始
function AAE645_t_up_protocol_biz_start(inputparam) {
	layer.close(ly_index);
	AAE645_param = inputparam;
    /*biz begin*/
    if($("#AAE645_qry_s_state").is("select") && $("#AAE645_qry_s_state")[0].options.length == 0)    {        $("#AAE645_qry_s_state").append("<option value='-1'></option>")        $.each(AAE645_ary_s_state, function (i, obj) {            addOptionValue("AAE645_qry_s_state", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    AAE645_init_t_up_protocol()	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function AAE645_format_s_state(value, row, index) {    var objResult = value;    for(i = 0; i < AAE645_ary_s_state.length; i++) {        var obj = AAE645_ary_s_state[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function AAE645_page_end(){
}

//上传协议设置显示列定义
var AAE645_t_up_protocol = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '上传协议模块',        field: 'PORT_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '协议名称',        field: 'CN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '是否启用',        field: 'S_STATE',        sortable: true        ,formatter: AAE645_format_s_state    }	
	/*table column end*/
];

//页面初始化
function AAE645_init_t_up_protocol() {
	$(window).resize(function () {
		  $('#AAE645_t_up_protocol_Events').bootstrapTable('resetView');
	});
	//上传协议设置查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AAE645_btn_t_up_protocol_query').click();
}

//查询接口
function AAE645_t_up_protocol_query() {
    $('#AAE645_t_up_protocol_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_up_protocol",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(AAE645_tem_cn_name)		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AAE645_get_N01_sel_t_up_protocol");
}

//查询结果
function AAE645_get_N01_sel_t_up_protocol(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_up_protocol) == false)
        return false;    
	var s_data = input.N01_sel_t_up_protocol;
    AAE645_select_t_up_protocol_rowId = "";
    $('#AAE645_t_up_protocol_Events').bootstrapTable('destroy');
    $("#AAE645_t_up_protocol_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AAE645_t_up_protocol_Events').find("tr.changeColor").removeClass('changeColor');
                AAE645_select_t_up_protocol_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AAE645_t_up_protocol_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AAE645_select_t_up_protocol_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AAE645_param.hasOwnProperty("target_name"))
			{
				$("#"+AAE645_param["target_id"]).val(eval("row."+AAE645_param["sourc_id"].toUpperCase()));
				$("#"+AAE645_param["target_name"]).val(s_decode(eval("row."+AAE645_param["sourc_name"].toUpperCase())));				
				layer.close(AAE645_param["ly_index"]);
			}
		},
        toolbar: "#AAE645_t_up_protocol_Toolbar",
        columns: AAE645_t_up_protocol,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AAE645_btn_t_up_protocol_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    AAE645_tem_cn_name = "";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/

	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    if($("#AAE645_qry_s_state").is("select") && $("#AAE645_qry_s_state")[0].options.length == 0)    {        $("#AAE645_qry_s_state").append("<option value='-1'></option>")        $.each(AAE645_ary_s_state, function (i, obj) {            addOptionValue("AAE645_qry_s_state", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    AAE645_init_t_up_protocol()	
    /*biz end*/
})

//查询按钮
$('#AAE645_btn_t_up_protocol_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    AAE645_tem_cn_name = $("#AAE645_qry_cn_name").val();	
    /*set query param end*/
	AAE645_t_up_protocol_query();
})

//vue回调
function AAE645_t_up_protocol_call_vue(objResult){
	if(index_subhtml == "t_up_protocol_$.vue")
	{
		var n = Get_RandomDiv("BAE647",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AAE645_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temcn_name = $("#AAE645_qry_cn_name").val();                if(temcn_name != ""){                    inputdata["cn_name"] = temcn_name;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_up_protocol_$.js","BAE647_t_up_protocol_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//新增按钮
$("#AAE645_btn_t_up_protocol_add").click(function () {
	AAE645_type = "add";
	index_subhtml = "t_up_protocol_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_up_protocol_$.vue","AAE645_t_up_protocol_call_vue") == true){
		var n = Get_RandomDiv("BAE647","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BAE647_param["type"] = AAE645_type;
				BAE647_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temcn_name = $("#AAE645_qry_cn_name").val();                if(temcn_name != ""){                    BAE647_param["cn_name"] = temcn_name;                }				
				/*Send Two FindSelect param end*/

				BAE647_clear_edit_info();
			},
			end: function(){
				BAE647_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AAE645_btn_t_up_protocol_edit").click(function () {
	if (AAE645_select_t_up_protocol_rowId != "") {
		AAE645_type = "edit";
		index_subhtml = "t_up_protocol_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_up_protocol_$.vue","AAE645_t_up_protocol_call_vue") == true){
			var n = Get_RandomDiv("BAE647","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BAE647_param["type"] = AAE645_type;
					BAE647_param["ly_index"] = index;
					BAE647_clear_edit_info();
					BAE647_get_edit_info();
				},
				end: function(){
					BAE647_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AAE645_btn_t_up_protocol_delete').click(function () {
	//单行选择
	var rowData = $("#AAE645_t_up_protocol_Events").bootstrapTable('getData')[AAE645_select_t_up_protocol_rowId];
	//多行选择
	var rowDatas = AAE645_sel_row_t_up_protocol();
	if (AAE645_select_t_up_protocol_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_up_protocol",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AAE645_N01_del_t_up_protocol");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_up_protocol",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AAE645_N01_del_t_up_protocol");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AAE645_N01_del_t_up_protocol(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_up_protocol) == true)
		AAE645_t_up_protocol_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function AAE645_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AAE645_sel_row_t_up_protocol(){
	//获得选中行
	var checkedbox= $("#AAE645_t_up_protocol_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}