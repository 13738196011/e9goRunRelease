#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_table_sample(object):
    attmain_id = -1
    attfile_id = 0
    attget_time = None
    attcoal_carbon = 0.0
    attoil_carbon = 0.0
    attgas_carbon = 0.0
    attele_carbon = 0.0
    atthot_carbon = 0.0
    attcreate_date = None
    atts_desc = ""
    lendata_value = 2000
    lens_desc = 2000
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        self.attget_time = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["file_id"] = "附件信息"  
        temHt["get_time"] = "日期"  
        temHt["coal_carbon"] = "用煤碳排量"  
        temHt["oil_carbon"] = "用油碳排量"  
        temHt["gas_carbon"] = "用气碳排量"  
        temHt["ele_carbon"] = "用电碳排量"  
        temHt["hot_carbon"] = "用热碳排量"  
        temHt["create_date"] = "系统时间"  
        temHt["s_desc"] = "系统备注"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["get_time"] = "业务"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["file_id"] = "T01_t_excel_data$file_id"#"select main_id,file_name as cn_name from t_file_info"  
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        temHtFind["file_id"] = "t_file_info.html"
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        temHtFReturn["file_id"] = ["MAIN_ID","FILE_NAME"]
        return temHtFReturn