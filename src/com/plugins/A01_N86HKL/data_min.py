#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class data_min(object):
    attmain_id = -1
    attcpu_time = None
    attautomatic_on = ""
    atttime_base = ""
    attdoor_closed = ""
    attextruder_on = ""
    attbatch_counter = ""
    attcycle_time = ""
    atttank_weight = ""
    attcooling_water_temp = ""
    attair_pressure = ""
    atts_type = "1"
    def __init__(self):
        self.attcpu_time = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")   