var B44HT5_ly_index = "";

//业务逻辑数据开始
function B44HT5_start(input) {
	B44HT5_clear_input();
	B44HT5_ly_index = input.ly_index;
	layer.close(ly_index);
    /*biz begin*/

	$("#B44HT5_btnUpd").click(function(){
		$("#B44HT5_error_info").html("");
		if($("#B44HT5_new_pwd").val() != $("#B44HT5_new_pwd2").val())
		{
			$("#B44HT5_error_info").append("<font color='red'>确认密码输入错误，请重新输入!</font>");
			return false;
		}
		
		ly_index = layer.load();
		var inputdata = {
            param_name: "T01_upd_pwd",
            session_id: session_id,
            param_value1:$("#B44HT5_new_pwd").val(),
            param_value2:$("#B44HT5_init_pwd").val(),
            param_value3:login_id
        };
		get_ajax_staticurl(inputdata, "B44HT5_get_T01_upd_pwd");
	});
    /*biz end*/
}

//清除输入框
function B44HT5_clear_input(){
	$("#B44HT5_init_pwd").val("");
	$("#B44HT5_new_pwd").val("");
	$("#B44HT5_new_pwd2").val("");
	$("#B44HT5_error_info").html("");   	
}

//取消编辑
$("#B44HT5_cancel").click(function () {
	layer.close(B44HT5_ly_index);
})

//vue回调
function call_vue(objResult){
	
}

function B44HT5_get_T01_upd_pwd(input) {
    layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(input.T01_upd_pwd) == true)
    {
		swal("修改密码成功!", "", "success");
		layer.close(B44HT5_ly_index);
    }
    else
		$("#B44HT5_error_info").append("<font color='red'>密码更改失败！</font>");    	
}