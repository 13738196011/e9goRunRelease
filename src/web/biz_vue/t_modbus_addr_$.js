//按钮事件新增或编辑
var BE98AD_type = "";
//其他页面传到本页面参数
var BE98AD_param = {};
//暂时没用
var BE98AD_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BE98AD_ary_com_port = null;var BE98AD_ary_element_id = null;var BE98AD_ary_protocol_id = null;
/*declare select options end*/

//业务逻辑数据开始
function BE98AD_t_modbus_addr_biz_start(inputdata) {
	BE98AD_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "T01_t_protocol$com_port",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BE98AD_get_T01_t_protocol$com_port");	
    /*biz end*/
}

/*biz step begin*/
function BE98AD_format_com_port(value, row, index) {    var objResult = value;    for(i = 0; i < BE98AD_ary_com_port.length; i++) {        var obj = BE98AD_ary_com_port[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BE98AD_format_element_id(value, row, index) {    var objResult = value;    for(i = 0; i < BE98AD_ary_element_id.length; i++) {        var obj = BE98AD_ary_element_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BE98AD_format_protocol_id(value, row, index) {    var objResult = value;    for(i = 0; i < BE98AD_ary_protocol_id.length; i++) {        var obj = BE98AD_ary_protocol_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BE98AD_get_T01_t_protocol$com_port(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_t_protocol$com_port) == false)        return false;    BE98AD_ary_com_port = input.T01_t_protocol$com_port;    if($("#BE98AD_com_port").is("select") && $("#BE98AD_com_port")[0].options.length == 0)    {        $.each(BE98AD_ary_com_port, function (i, obj) {            addOptionValue("BE98AD_com_port", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    var inputdata = {        "param_name": "T01_t_prot_ele_link$element_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BE98AD_get_T01_t_prot_ele_link$element_id");}function BE98AD_get_T01_t_prot_ele_link$element_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_t_prot_ele_link$element_id) == false)        return false;    BE98AD_ary_element_id = input.T01_t_prot_ele_link$element_id;    if($("#BE98AD_element_id").is("select") && $("#BE98AD_element_id")[0].options.length == 0)    {        $.each(BE98AD_ary_element_id, function (i, obj) {            addOptionValue("BE98AD_element_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    var inputdata = {        "param_name": "T01_t_prot_ele_link$prot_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BE98AD_get_T01_t_prot_ele_link$prot_id");}function BE98AD_get_T01_t_prot_ele_link$prot_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_t_prot_ele_link$prot_id) == false)        return false;    BE98AD_ary_protocol_id = input.T01_t_prot_ele_link$prot_id;    if($("#BE98AD_protocol_id").is("select") && $("#BE98AD_protocol_id")[0].options.length == 0)    {        $.each(BE98AD_ary_protocol_id, function (i, obj) {            addOptionValue("BE98AD_protocol_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    BE98AD_init_t_modbus_addr();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function BE98AD_page_end(){
	if(BE98AD_param["type"] == "edit"){
		BE98AD_get_edit_info();
	}
}

//页面初始化方法
function BE98AD_init_t_modbus_addr() {
	//type = getUrlParam("type");
	if(BE98AD_param["type"] == "add"){
		$("#BE98AD_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BE98AD_com_port").val(BE98AD_param["com_port"]);        $("#BE98AD_element_id").val(BE98AD_param["element_id"]);        $("#BE98AD_protocol_id").val(BE98AD_param["protocol_id"]);		
		/*Get Find Select param end*/
		
	}
	else if(BE98AD_param["type"] == "edit"){
	
	}
	
	//表单验证
	BE98AD_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BE98AD_create_date").val() == "")    {        $("#BE98AD_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BE98AD_create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BE98AD_page_end();
}

//提交表单数据
function BE98AD_SubmitForm(){
	if(BE98AD_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_modbus_addr",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#BE98AD_modbus_gateway").val())                ,"param_value2": s_encode($("#BE98AD_com_port").val())                ,"param_value3": s_encode($("#BE98AD_modbus_addr").val())                ,"param_value4": s_encode($("#BE98AD_modbus_begin").val())                ,"param_value5": $("#BE98AD_modbus_length").val()                ,"param_value6": $("#BE98AD_element_id").val()                ,"param_value7": $("#BE98AD_protocol_id").val()                ,"param_value8": $("#BE98AD_create_date").val()                ,"param_value9": s_encode($("#BE98AD_s_desc").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BE98AD_get_N01_ins_t_modbus_addr");
	}
	else if(BE98AD_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_modbus_addr",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#BE98AD_modbus_gateway").val())                ,"param_value2": s_encode($("#BE98AD_com_port").val())                ,"param_value3": s_encode($("#BE98AD_modbus_addr").val())                ,"param_value4": s_encode($("#BE98AD_modbus_begin").val())                ,"param_value5": $("#BE98AD_modbus_length").val()                ,"param_value6": $("#BE98AD_element_id").val()                ,"param_value7": $("#BE98AD_protocol_id").val()                ,"param_value8": $("#BE98AD_create_date").val()                ,"param_value9": s_encode($("#BE98AD_s_desc").val())                ,"param_value10": $("#BE98AD_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BE98AD_get_N01_upd_t_modbus_addr");
	}
}

//vue回调
function BE98AD_t_modbus_addr_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#BE98AD_save_t_modbus_addr_Edit").click(function () {
	$("form[name='BE98AD_DataModal']").submit();
})

/*修改数据*/
function BE98AD_get_N01_upd_t_modbus_addr(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_modbus_addr) == true)
	{
		swal("修改数据成功!", "", "success");
		AE98AD_t_modbus_addr_query();
		BE98AD_clear_validate();
		layer.close(BE98AD_param["ly_index"]);
	}
}

/*添加数据*/
function BE98AD_get_N01_ins_t_modbus_addr(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_modbus_addr) == true)
	{
		swal("添加数据成功!", "", "success");
		AE98AD_t_modbus_addr_query();
		BE98AD_clear_validate();
		layer.close(BE98AD_param["ly_index"]);
	}
}

//取消编辑
$("#BE98AD_cancel_t_modbus_addr_Edit").click(function () {
	layer.close(BE98AD_param["ly_index"]);
	BE98AD_clear_validate();
	$("[id^='BE98AD_div']").hide();
})

//清除查找框
function BE98AD_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BE98AD_clear_validate(){
	$("#BE98AD_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BE98AD_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BE98AD_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BE98AD_clear_edit_info(){
	var inputs = $("#BE98AD_DataModal").find('input');
	var selects = $("#BE98AD_DataModal").find("select");
	var textareas = $("#BE98AD_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	BE98AD_init_t_modbus_addr();
}

//页面输入框赋值
function BE98AD_get_edit_info(){
	var rowData = $("#AE98AD_t_modbus_addr_Events").bootstrapTable('getData')[AE98AD_select_t_modbus_addr_rowId];
	var inputs = $("#BE98AD_DataModal").find('input');
	var selects = $("#BE98AD_DataModal").find("select");
	var textareas = $("#BE98AD_DataModal").find('textarea');
	$.each(selects, function (i, obj) {
		if(typeof(eval("AE98AD_ary_"+obj.id.toString().replace("BE98AD_",""))) == "object")
		{
			$.each(eval("AE98AD_ary_"+obj.id.toString().replace("BE98AD_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE98AD_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("BE98AD_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("AE98AD_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#BE98AD_DataModal").find('[id="'+"BE98AD_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE98AD_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE98AD_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//form验证
function BE98AD_checkFormInput() {
    BE98AD_validate = $("#BE98AD_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BE98AD_main_id: {}            ,BE98AD_modbus_gateway: {}            ,BE98AD_com_port: {}            ,BE98AD_modbus_addr: {}            ,BE98AD_modbus_begin: {}            ,BE98AD_modbus_length: {digits: true,required : true,maxlength:10}            ,BE98AD_element_id: {}            ,BE98AD_protocol_id: {}            ,BE98AD_create_date: {date: true,required : true,maxlength:19}            ,BE98AD_s_desc: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BE98AD_main_id: {}            ,BE98AD_modbus_gateway: {}            ,BE98AD_com_port: {}            ,BE98AD_modbus_addr: {}            ,BE98AD_modbus_begin: {}            ,BE98AD_modbus_length: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,BE98AD_element_id: {}            ,BE98AD_protocol_id: {}            ,BE98AD_create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BE98AD_s_desc: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BE98AD_SubmitForm();
        	return false;
        }
    })
}