var iTop = 0;
var ileft = 0;
var ly_index;

function biz_start(){
}

//编辑提交数据成功
function submit_success() {
    init();
    $("#doing").empty();
    $("#doing").attr("style", "display:none");
    swal("操作成功!", "", "success");
    $("a.ui-jqdialog-titlebar-close").click();
}

//后台用户管理
var mod_eova_user = [{
        label: '主键',
        name: 'MAIN_ID',
        width: '30px',
        index: 'MAIN_ID',
        editable: false,
        key: true,
        readOnly: true,
        editrules: {
            required: true
        }
    }, {
        label: '登录账号',
        name: 'login_id',
        width: '40px',
        editable: true,
        editrules: {
            required: true
        }
    }, {
        label: '登录密码',
        name: 'LOGIN_PWD',
        width: '40px',
        editable: true,
        editrules: {
            required: true
        },
        edittype: "password"
    }, {
        label: '用户角色',
        name: 'RID',
        width: '50px',
        editable: true,
        editrules: true,
        edittype: 'select',
        formatter: 'select',
        editable: true,
        editrules: true,
        editoptions: {
            value: {
                1: '开发者',
                2: '系统管理员',
                3: '运维人员',
                4: '普通用户'
            },
            defaultValue: 4
        },
        formatoptions: {
            value: {
                1: '开发者',
                2: '系统管理员',
                3: '运维人员',
                4: '普通用户'
            }
        }
    }, {
        label: '用户姓名',
        name: 'NICKNAME',
        width: '50px',
        editable: true,
        editrules: {
            required: true
        }
    }, {
        label: '备注',
        name: 'S_DESC',
        width: '50px',
        editable: true,
        editrules: true
    }, {
        label: '系统时间',
        name: 'CREATE_DATE',
        width: '50px',
        editable: true,
        editrules: true,
        readOnly: true,
        formatter: function (value, row) {
            return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
        },
        editoptions: {
            defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')
        }
    }
];

//获取后台用户管理
function get_T01_sel_eova_user() {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_eova_user",
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_eova_user",
        success: function (data2) {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

function T01_sel_eova_user(input) {
    data = input.T01_sel_eova_user;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
    $("#table_list_1").jqGrid({
        data: data,
        datatype: "local",
        height: "450",
        autowidth: true,
        shrinkToFit: true,
        rowNum: 10,
        rowList: [10, 20, 30],
        colModel: mod_eova_user,
        editurl: 'clientArray',
        altRows: true,
        pager: "#pager_list_1",
        viewrecords: true,
        caption: "后台用户管理列表"
    });
    //导航后面，增加增删改查按钮
    $("#table_list_1").jqGrid("navGrid", "#pager_list_1", {
        edit: true,
        add: true,
        del: true,
        search: true,
        view: false,
        position: "left",
        cloneToTop: false
    }, {
        beforeShowForm: function () {
            $('#login_id').attr('readOnly', true);
            $("#login_id").attr('disabled', "disabled");
            laydate.render({
                elem: '#CREATE_DATE',
                type: 'datetime'
            });
        },
        onclickSubmit: updateDbByMainId,
        closeAfterEdit: true
    }, {
        beforeShowForm: function () {
            laydate.render({
                elem: '#CREATE_DATE',
                type: 'datetime'
            });
        },
        onclickSubmit: insertDb,
        closeAfterAdd: true
    }, {
        top: iTop,
        left: ileft,
        onclickSubmit: delDbByMainId
    }, {
        closeAfterSearch: true
    }, {
        height: 200,
        reloadAfterSubmit: true
    });
    layer.close(ly_index);
}

//初始化
function init() {
    iTop = (winHeight - 300) / 2;
    ileft = (winWidth - 300) / 2;
    ly_index = layer.load();
    $.jgrid.defaults.styleUI = "Bootstrap";
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_eova_user",
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_eova_user",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

$(document).ready(function () {
    init_page();
    getWindowSize();
    init();
    $(window).bind("resize",
        function () {
        var width = $(".jqGrid_wrapper").width();
        $("#table_list_1").setGridWidth(width);
    });
});

function T01_ins_eova_user(input) {
    data = input.T01_ins_eova_user;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
    //$("#table_list_1").trigger("reloadGrid");
    layer.close(ly_index);
}

var insertDb = function () {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_ins_eova_user",
            session_id: session_id,
            param_value1: $("#login_id").val(),
            param_value2: $("#LOGIN_PWD").val(),
            param_value3: $("#rid").val(),
            param_value4: $("#nickname").val(),
            param_value5: $("#S_DESC").val(),
            param_value6: $("#CREATE_DATE").val()
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_ins_eova_user",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_upd_eova_user(input) {
    data = input.T01_upd_eova_user;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
    layer.close(ly_index);
}

var updateDbByMainId = function () {
    ly_index = layer.load();
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_upd_eova_user",
            session_id: session_id,
            param_value1: $("#login_id").val(),
            param_value2: $("#LOGIN_PWD").val(),
            param_value3: $("#rid").val(),
            param_value4: $("#nickname").val(),
            param_value5: $("#S_DESC").val(),
            param_value6: $("#CREATE_DATE").val(),
            param_value7: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_upd_eova_user",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_del_eova_user(input) {
    data = input.T01_del_eova_user;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
    layer.close(ly_index);
}

var delDbByMainId = function () {
    ly_index = layer.load();
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_del_eova_user",
            session_id: session_id,
            param_value1: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_del_eova_user",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};
