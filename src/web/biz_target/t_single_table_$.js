var type = "";

/*定义下拉框集合定义*/
/*declare select options begin*/

var ary_table_type = [{"MAIN_ID":"0","CN_NAME":"表"},{"MAIN_ID":"1","CN_NAME":"视图"}];

/*declare select options end*/

//业务逻辑数据开始
function biz_start() {
	layer.close(ly_index);
    /*biz begin*/
    init_t_single_table()	
    /*biz end*/
}

/*biz step begin*/

function format_TABLE_TYPE(value, row, index) {
    var objResult = value;
    for(i = 0; i < ary_TABLE_TYPE.length; i++) {
        var obj = ary_TABLE_TYPE[i];
        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {
            objResult = obj[GetLowUpp("cn_name")];
            break;
        }
    }
    return objResult;
}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function page_end(){
}


//页面初始化方法
function init_t_single_table() {
	type = getUrlParam("type");
	if(type == "add"){
		$("#main_id").parent().parent().parent().hide();
	}
	else if(type == "edit"){
	
	}
	
    if($("#table_type").is("select") && $("#table_type")[0].options.length == 0)
    {
        $.each(ary_table_type, function (i, obj) {
            addOptionValue("table_type", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);
        });
    }
    
	//表单验证
	checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#create_date").val() == "")    {        $("#create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
}

//提交表单数据
function SubmitForm(){
	if(type == "add"){
		var inputdata = {
				"param_name": "T01_ins_t_single_table",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#table_cn_name").val())                ,"param_value2": s_encode($("#table_en_name").val())                ,"param_value3": $("#create_date").val()                ,"param_value4": s_encode($("#s_desc").val())	
                ,"param_value5": s_encode($("#table_type").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_ins_t_single_table");
	}
	else if(type == "edit"){
		var inputdata = {
				"param_name": "T01_upd_t_single_table",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#table_cn_name").val())                ,"param_value2": s_encode($("#table_en_name").val())                ,"param_value3": $("#create_date").val()                ,"param_value4": s_encode($("#s_desc").val())
                ,"param_value5": s_encode($("#table_type").val())                ,"param_value6": $("#main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_upd_t_single_table");
	}
}

//以下为页面表单验证
$("#save_t_single_table_Edit").click(function () {
	$("form[name='DataModal']").submit();
})

/*修改数据*/
function get_T01_upd_t_single_table(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_upd_t_single_table) == true)
	{
        //init();
		parent.swal("修改数据成功!", "", "success");
		parent.t_single_table_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

/*添加数据*/
function get_T01_ins_t_single_table(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_ins_t_single_table) == true)
	{
		parent.swal("添加数据成功!", "", "success");
		parent.t_single_table_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

//取消编辑
$("#cancel_t_single_table_Edit").click(function () {
	var index = parent.layer.getFrameIndex(window.name);
	parent.layer.close(index);
})

function clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

$(document).ready(function () {
});

window.onpageshow = function (e) {
    if (e.persisted || (window.performance.navigation.type == 2)) {
        is_history_back = 1;
    } else {
        is_history_back = 0;
    }
    //页面初始化
    init_page();
};

//form验证
function checkFormInput() {
    $("#DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            main_id: {}            ,table_cn_name: {}            ,table_en_name: {}            ,create_date: {date: true,required : true,maxlength:19}            ,s_desc: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            main_id: {}            ,table_cn_name: {}            ,table_en_name: {}            ,create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,s_desc: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	SubmitForm();
        	return false;
        }
    })
}