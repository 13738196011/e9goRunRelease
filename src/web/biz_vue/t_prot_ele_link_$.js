//按钮事件新增或编辑
var BE339C_type = "";
//其他页面传到本页面参数
var BE339C_param = {};
//暂时没用
var BE339C_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BE339C_ary_prot_id = null;var BE339C_ary_element_id = null;
/*declare select options end*/

//业务逻辑数据开始
function BE339C_t_prot_ele_link_biz_start(inputdata) {
	BE339C_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_prot_ele_link$prot_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BE339C_get_N01_t_prot_ele_link$prot_id");	
    /*biz end*/
}

/*biz step begin*/
function BE339C_format_prot_id(value, row, index) {    var objResult = value;    for(i = 0; i < BE339C_ary_prot_id.length; i++) {        var obj = BE339C_ary_prot_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BE339C_format_element_id(value, row, index) {    var objResult = value;    for(i = 0; i < BE339C_ary_element_id.length; i++) {        var obj = BE339C_ary_element_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BE339C_get_N01_t_prot_ele_link$prot_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_prot_ele_link$prot_id) == false)        return false;    BE339C_ary_prot_id = input.N01_t_prot_ele_link$prot_id;    if($("#BE339C_prot_id").is("select") && $("#BE339C_prot_id")[0].options.length == 0)    {        $.each(BE339C_ary_prot_id, function (i, obj) {            addOptionValue("BE339C_prot_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    var inputdata = {        "param_name": "N01_t_prot_ele_link$element_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BE339C_get_N01_t_prot_ele_link$element_id");}function BE339C_get_N01_t_prot_ele_link$element_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_prot_ele_link$element_id) == false)        return false;    BE339C_ary_element_id = input.N01_t_prot_ele_link$element_id;    if($("#BE339C_element_id").is("select") && $("#BE339C_element_id")[0].options.length == 0)    {        $.each(BE339C_ary_element_id, function (i, obj) {            addOptionValue("BE339C_element_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    BE339C_init_t_prot_ele_link();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function BE339C_page_end(){
	if(BE339C_param["type"] == "edit"){
		BE339C_get_edit_info();
	}
}

//页面初始化方法
function BE339C_init_t_prot_ele_link() {
	//type = getUrlParam("type");
	if(BE339C_param["type"] == "add"){
		$("#BE339C_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BE339C_prot_id").val(BE339C_param["prot_id"]);        $("#BE339C_element_id").val(BE339C_param["element_id"]);		
		/*Get Find Select param end*/	

	}
	else if(BE339C_param["type"] == "edit"){
	
	}
	
	//表单验证
	BE339C_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BE339C_create_date").val() == "")    {        $("#BE339C_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BE339C_create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BE339C_page_end();
}

//提交表单数据
function BE339C_SubmitForm(){
	if(BE339C_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_prot_ele_link",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#BE339C_prot_id").val()                ,"param_value2": $("#BE339C_element_id").val()                ,"param_value3": $("#BE339C_create_date").val()                ,"param_value4": s_encode($("#BE339C_s_desc").val())                ,"param_value5": s_encode($("#BE339C_prot_up_limit").val())                ,"param_value6": s_encode($("#BE339C_prot_down_limit").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BE339C_get_N01_ins_t_prot_ele_link");
	}
	else if(BE339C_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_prot_ele_link",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#BE339C_prot_id").val()                ,"param_value2": $("#BE339C_element_id").val()                ,"param_value3": $("#BE339C_create_date").val()                ,"param_value4": s_encode($("#BE339C_s_desc").val())                ,"param_value5": s_encode($("#BE339C_prot_up_limit").val())                ,"param_value6": s_encode($("#BE339C_prot_down_limit").val())                ,"param_value7": $("#BE339C_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BE339C_get_N01_upd_t_prot_ele_link");
	}
}

//vue回调
function BE339C_t_prot_ele_link_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#BE339C_save_t_prot_ele_link_Edit").click(function () {
	$("form[name='BE339C_DataModal']").submit();
})

/*修改数据*/
function BE339C_get_N01_upd_t_prot_ele_link(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_prot_ele_link) == true)
	{
		swal("修改数据成功!", "", "success");
		AE339C_t_prot_ele_link_query();
		BE339C_clear_validate();
		layer.close(BE339C_param["ly_index"]);
	}
}

/*添加数据*/
function BE339C_get_N01_ins_t_prot_ele_link(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_prot_ele_link) == true)
	{
		swal("添加数据成功!", "", "success");
		AE339C_t_prot_ele_link_query();
		BE339C_clear_validate();
		layer.close(BE339C_param["ly_index"]);
	}
}

//取消编辑
$("#BE339C_cancel_t_prot_ele_link_Edit").click(function () {
	layer.close(BE339C_param["ly_index"]);
	BE339C_clear_validate();
	$("[id^='BE339C_div']").hide();
})

//清除查找框
function BE339C_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BE339C_clear_validate(){
	$("#BE339C_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BE339C_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BE339C_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BE339C_clear_edit_info(){
	var inputs = $("#BE339C_DataModal").find('input');
	var selects = $("#BE339C_DataModal").find("select");
	var textareas = $("#BE339C_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	BE339C_init_t_prot_ele_link();
}

//页面输入框赋值
function BE339C_get_edit_info(){
	var rowData = $("#AE339C_t_prot_ele_link_Events").bootstrapTable('getData')[AE339C_select_t_prot_ele_link_rowId];
	var inputs = $("#BE339C_DataModal").find('input');
	var selects = $("#BE339C_DataModal").find("select");
	var textareas = $("#BE339C_DataModal").find('textarea');
	$.each(selects, function (i, obj) {
		if(typeof(eval("AE339C_ary_"+obj.id.toString().replace("BE339C_",""))) == "object")
		{
			$.each(eval("AE339C_ary_"+obj.id.toString().replace("BE339C_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE339C_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("BE339C_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("AE339C_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#BE339C_DataModal").find('[id="'+"BE339C_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE339C_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE339C_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//form验证
function BE339C_checkFormInput() {
    BE339C_validate = $("#BE339C_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BE339C_main_id: {}            ,BE339C_prot_id: {}            ,BE339C_element_id: {}            ,BE339C_create_date: {date: true,required : true,maxlength:19}            ,BE339C_s_desc: {}            ,BE339C_prot_up_limit: {}            ,BE339C_prot_down_limit: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BE339C_main_id: {}            ,BE339C_prot_id: {}            ,BE339C_element_id: {}            ,BE339C_create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BE339C_s_desc: {}            ,BE339C_prot_up_limit: {}            ,BE339C_prot_down_limit: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BE339C_SubmitForm();
        	return false;
        }
    })
}