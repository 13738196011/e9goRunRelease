#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_code_element(object):
    attmain_id = -1
    attelement_code = ""
    attelement_name = ""
    attelement_val = ""
    attcreate_date = None
    atts_desc = ""
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")        