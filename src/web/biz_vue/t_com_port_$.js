//按钮事件新增或编辑
var B72A1E_type = "";
//其他页面传到本页面参数
var B72A1E_param = {};
//暂时没用
var B72A1E_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

//业务逻辑数据开始
function B72A1E_t_com_port_biz_start(inputdata) {
	B72A1E_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    B72A1E_init_t_com_port()	
    /*biz end*/
}

/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function B72A1E_page_end(){
	if(B72A1E_param["type"] == "edit"){
		B72A1E_get_edit_info();
	}
}

//页面初始化方法
function B72A1E_init_t_com_port() {
	//type = getUrlParam("type");
	if(B72A1E_param["type"] == "add"){
		$("#B72A1E_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
		
		/*Get Find Select param end*/	
	}
	else if(B72A1E_param["type"] == "edit"){
	
	}
	
	//表单验证
	B72A1E_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B72A1E_create_date").val() == "")    {        $("#B72A1E_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B72A1E_create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B72A1E_page_end();
}

//提交表单数据
function B72A1E_SubmitForm(){
	if(B72A1E_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_com_port",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#B72A1E_com_name").val())                ,"param_value2": $("#B72A1E_com_bot").val()                ,"param_value3": $("#B72A1E_com_data").val()                ,"param_value4": $("#B72A1E_com_ct").val()                ,"param_value5": s_encode($("#B72A1E_com_eflag").val())                ,"param_value6": s_encode($("#B72A1E_s_desc").val())                ,"param_value7": $("#B72A1E_create_date").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B72A1E_get_N01_ins_t_com_port");
	}
	else if(B72A1E_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_com_port",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#B72A1E_com_name").val())                ,"param_value2": $("#B72A1E_com_bot").val()                ,"param_value3": $("#B72A1E_com_data").val()                ,"param_value4": $("#B72A1E_com_ct").val()                ,"param_value5": s_encode($("#B72A1E_com_eflag").val())                ,"param_value6": s_encode($("#B72A1E_s_desc").val())                ,"param_value7": $("#B72A1E_create_date").val()                ,"param_value8": $("#B72A1E_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B72A1E_get_N01_upd_t_com_port");
	}
}

//vue回调
function B72A1E_t_com_port_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#B72A1E_save_t_com_port_Edit").click(function () {
	$("form[name='B72A1E_DataModal']").submit();
})

/*修改数据*/
function B72A1E_get_N01_upd_t_com_port(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_com_port) == true)
	{
		swal("修改数据成功!", "", "success");
		A72A1E_t_com_port_query();
		B72A1E_clear_validate();
		layer.close(B72A1E_param["ly_index"]);
	}
}

/*添加数据*/
function B72A1E_get_N01_ins_t_com_port(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_com_port) == true)
	{
		swal("添加数据成功!", "", "success");
		A72A1E_t_com_port_query();
		B72A1E_clear_validate();
		layer.close(B72A1E_param["ly_index"]);
	}
}

//取消编辑
$("#B72A1E_cancel_t_com_port_Edit").click(function () {
	layer.close(B72A1E_param["ly_index"]);
	B72A1E_clear_validate();
	$("[id^='B72A1E_div']").hide();
})

//清除查找框
function B72A1E_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B72A1E_clear_validate(){
	$("#B72A1E_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B72A1E_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B72A1E_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B72A1E_clear_edit_info(){
	var inputs = $("#B72A1E_DataModal").find('input');
	var selects = $("#B72A1E_DataModal").find("select");
	var textareas = $("#B72A1E_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	B72A1E_init_t_com_port();
}

//页面输入框赋值
function B72A1E_get_edit_info(){
	var rowData = $("#A72A1E_t_com_port_Events").bootstrapTable('getData')[A72A1E_select_t_com_port_rowId];
	var inputs = $("#B72A1E_DataModal").find('input');
	var selects = $("#B72A1E_DataModal").find("select");
	var textareas = $("#B72A1E_DataModal").find('textarea');
	$.each(selects, function (i, obj) {
		if(typeof(eval("A72A1E_ary_"+obj.id.toString().replace("B72A1E_",""))) == "object")
		{
			$.each(eval("A72A1E_ary_"+obj.id.toString().replace("B72A1E_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("B72A1E_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("B72A1E_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("A72A1E_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#B72A1E_DataModal").find('[id="'+"B72A1E_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("B72A1E_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("B72A1E_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//form验证
function B72A1E_checkFormInput() {
    B72A1E_validate = $("#B72A1E_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B72A1E_main_id: {}            ,B72A1E_com_name: {}            ,B72A1E_com_bot: {digits: true,required : true,maxlength:10}            ,B72A1E_com_data: {digits: true,required : true,maxlength:10}            ,B72A1E_com_ct: {digits: true,required : true,maxlength:10}            ,B72A1E_com_eflag: {}            ,B72A1E_s_desc: {}            ,B72A1E_create_date: {date: true,required : true,maxlength:19}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B72A1E_main_id: {}            ,B72A1E_com_name: {}            ,B72A1E_com_bot: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,B72A1E_com_data: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,B72A1E_com_ct: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,B72A1E_com_eflag: {}            ,B72A1E_s_desc: {}            ,B72A1E_create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B72A1E_SubmitForm();
        	return false;
        }
    })
}