﻿var iTop = 0;
var ileft = 0;
var ly_index;

//接口名称
var mod_t_page_item;
var options  = {};
var options_v2  = {};

function biz_start(){
}

function T01_sel_t_page_info(input)
{
	var data = input.T01_sel_t_page_info;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
	$.each(input.T01_sel_t_page_info,function(i, obj) {
		options[obj.MAIN_ID.toString()] = obj.PAGE_TITLE;
	});
		
	mod_t_page_item=[{label:'主键',name:'MAIN_ID',width:'30px',index:'MAIN_ID',editable: false,key: true,readOnly:true,editrules : { required: true}}
		,{label:'页面模板名称',name:'PAGE_ID',width:'50px',editable: true,readOnly:true,editrules:true,edittype:'select',formatter:'select',editoptions: {value:options}}
		,{label:'接口名称',name:'PROC_ID',width:'50px',editable: true,readOnly:true,editrules:true,edittype:'select',formatter:'select',editoptions: {value:options_v2}}
		,{label:'明细名称',name:'ITEM_NAME',width:'50px',editable:true,editrules:true}		
		,{label:'明细类型',name:'ITEM_TYPE',width:'50px',editable:true,editrules:true,edittype:'select',formatter:'select',
			editoptions: {value:{1:'表格模式',2:'列表模式',3:'曲线图',4:'柱状图'}},
			formatoptions:{value:{1:'表格模式',2:'列表模式',3:'曲线图',4:'柱状图'}}}
		,{label:'显示列数',name:'COLUMN_COUNT',width:'50px',editable:true,editrules:true}
		,{label:'系统时间',name:'CREATE_DATE',width:'50px',editable:true,editrules:true,readOnly:true,formatter:function(value,row){return new Date(value).Format('yyyy-MM-dd hh:mm:ss');},editoptions:{defaultValue:new Date().Format('yyyy-MM-dd hh:mm:ss')}}
		,{label:'备注',name:'S_DESC',width:'70px',editable:true,editrules:true}
		,{label:'页面对应输入参数',name:'URL_IN_PARAM',width:'50px',editable:true,editrules:true}		
	];
	layer.close(ly_index);
	init();
}

//获取接口名称结果
function T01_sel_t_proc_name(input)
{
	data = input.T01_sel_t_proc_name;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
	if(data[0].MAIN_ID < 72)
		data.splice(0,1);
	$.each(data,function(i, obj) {
		options_v2[obj.MAIN_ID.toString()] = obj.DB_ID+"--"+obj.INF_CN_NAME;
	});
	layer.close(ly_index);
	get_T01_sel_t_page_info();
}

//获取接口名称
function get_T01_sel_t_proc_name() {
	ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
		data:{
            param_name: "T01_sel_t_proc_name",
			session_id:session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_proc_name",
        success: function(data2) {
		},
        error: function() {
			layer.close(ly_index);
			swal({
				title: "告警",
				text: "网络异常或系统故障，请刷新页面！",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "刷新",
				closeOnConfirm: false
			},
			function() {
				window.location.reload();
			});
		},
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
		complete: function(XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

//获取APP应用模板
function get_T01_sel_t_page_info() {
	ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
		data:{
            param_name: "T01_sel_t_page_info",
			session_id:session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_page_info",
        success: function(data2) {
		},
        error: function() {
			layer.close(ly_index);
			swal({
				title: "告警",
				text: "网络异常或系统故障，请刷新页面！",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "刷新",
				closeOnConfirm: false
			},
			function() {
				window.location.reload();
			});
		},
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
		complete: function(XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

function T01_sel_t_page_item(input)
{
	data = input.T01_sel_t_page_item;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
	$("#table_list_1").jqGrid({
		data: data,
		datatype: "local",
		height: "450",
		autowidth: true,
		shrinkToFit: true,
		rowNum: 10,
		rowList: [10, 20, 30],
		colModel: mod_t_page_item,
		editurl: 'clientArray',
		altRows: true,
		pager: "#pager_list_1",
		viewrecords: true,
		caption: "APP应用模板明细列表"
	});
	//导航后面，增加增删改查按钮
	$("#table_list_1").jqGrid("navGrid", "#pager_list_1", {
		edit: true,
		add: true,
		del: true,
		search: true,
		view: false,
		position: "left",
		cloneToTop: false
	},
	{
		beforeShowForm: function() {
			$('#PROC_ID').attr('readOnly', true);			
			$("#PROC_ID").attr('disabled',"disabled");	
			$("#PAGE_ID").attr('disabled',"disabled");
			laydate.render({
				elem: '#CREATE_DATE',
				type: 'datetime'
			});
		},
		onclickSubmit: updateDbByMainId,
		closeAfterEdit: true
	},
	{
		beforeShowForm: function() {
			laydate.render({
				elem: '#CREATE_DATE',
				type: 'datetime'
			});
		},
		onclickSubmit: insertDb,
		closeAfterAdd: true
	},
	{
		top: iTop,
		left: ileft,
		onclickSubmit: delDbByMainId
	},
	{
		closeAfterSearch: true
	},
	{
		height: 200,
		reloadAfterSubmit: true
	});
	layer.close(ly_index);
}

//初始化
function init() {
	ly_index = layer.load();
	iTop = (winHeight - 300) / 2;
	ileft = (winWidth - 300) / 2;
    $.jgrid.defaults.styleUI = "Bootstrap";
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
		data:{
            param_name: "T01_sel_t_page_item",
			session_id:session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_page_item",
        success: function() {},
        error: function() {
			layer.close(ly_index);
			swal({
				title: "告警",
				text: "网络异常或系统故障，请刷新页面！",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "刷新",
				closeOnConfirm: false
			},
			function() {
				window.location.reload();
			});
		},
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
		complete: function(XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

$(document).ready(function() {
	init_page();
    getWindowSize();
	get_T01_sel_t_proc_name();
    $(window).bind("resize",
    function() {
        var width = $(".jqGrid_wrapper").width();
        $("#table_list_1").setGridWidth(width);
    });
});

function T01_ins_t_page_item(input)
{
	data = input.T01_ins_t_page_item;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
	layer.close(ly_index);
}

var insertDb = function() {
	ly_index = layer.load();
	$.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
		data:{
            param_name: "T01_ins_t_page_item",
			session_id:session_id,
			param_value1: $("#PAGE_ID").val(),
			param_value2: $("#PROC_ID").val(),
			param_value3: $("#ITEM_NAME").val(),
			param_value4: $("#ITEM_TYPE").val(),
			param_value5: $("#COLUMN_COUNT").val(),
			param_value6: $("#CREATE_DATE").val(),
			param_value7: $("#S_DESC").val(),
			param_value8: $("#URL_IN_PARAM").val()			
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_ins_t_page_item",
        success: function() {},
        error: function() {
			layer.close(ly_index);
			swal({
				title: "告警",
				text: "网络异常或系统故障，请刷新页面！",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "刷新",
				closeOnConfirm: false
			},
			function() {
				window.location.reload();
			});
		},
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
		complete: function(XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_upd_t_page_item(input)
{
	data = input.T01_upd_t_page_item;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
	layer.close(ly_index);
}

var updateDbByMainId = function() {
	ly_index = layer.load();
	var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
	var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
	$.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
		data:{
            param_name: "T01_upd_t_page_item",
			session_id:session_id,
			param_value1: $("#ITEM_NAME").val(),
			param_value2: $("#ITEM_TYPE").val(),
			param_value3: $("#COLUMN_COUNT").val(),
			param_value4: $("#CREATE_DATE").val(),
			param_value5: $("#S_DESC").val(),
			param_value6: $("#URL_IN_PARAM").val(),	
			param_value7: rowData.MAIN_ID			
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_upd_t_page_item",
        success: function() {},
        error: function() {
			layer.close(ly_index);
			swal({
				title: "告警",
				text: "网络异常或系统故障，请刷新页面！",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "刷新",
				closeOnConfirm: false
			},
			function() {
				window.location.reload();
			});
		},
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
		complete: function(XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_del_t_page_item(input)
{
	data = input.T01_del_t_page_item;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
	layer.close(ly_index);
	
	//重新设置jqgrid大小尺寸
	//getWindowSize();
	/*
	$(" #delmodgrid-table").css({
		"top": iTop,
		"left": ileft,
		"height": "140px",
		"width": "390px"
	});*/
}

var delDbByMainId = function() {	
	ly_index = layer.load();
	var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
	var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
	$.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
		data:{
            param_name: "T01_del_t_page_item",
			session_id:session_id,
			param_value1: rowData.MAIN_ID			
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_del_t_page_item",
        success: function() {},
        error: function() {
			layer.close(ly_index);
			swal({
				title: "告警",
				text: "网络异常或系统故障，请刷新页面！",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "刷新",
				closeOnConfirm: false
			},
			function() {
				window.location.reload();
			});
		},
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
		complete: function(XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};