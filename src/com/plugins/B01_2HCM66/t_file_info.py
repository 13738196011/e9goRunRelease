#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_file_info(object):
    attmain_id = -1
    attfile_type_id = 0
    attfile_name = ""
    attfile_url = ""
    attis_ava = 1
    attcreate_date = None
    atts_desc = ""
    lenfile_name = 500
    lenfile_url = 500
    lens_desc = 2000
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["file_type_id"] = "附件类别ID"  
        temHt["file_name"] = "附件中文名称"  
        temHt["file_url"] = "附件URL"
        temHt["is_ava"] = "是否启用 "        
        temHt["create_date"] = "系统时间"  
        temHt["s_desc"] = "系统备注"  
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["file_name"] = "附件名称"
        temHtQry["file_type_id"] = "附件类别"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["file_type_id"] = "select main_id,type_name as cn_name from t_file_type"
        temHtSelect["is_ava"] = [{"main_id":"1","cn_name":"启用"},{"main_id":"0","cn_name":"禁用"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn