//按钮事件新增或编辑
var B43B7C_type = "";
//其他页面传到本页面参数
var B43B7C_param = {};
//暂时没用
var B43B7C_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var B43B7C_ary_DB_ID = null;var B43B7C_ary_INF_TYPE = [{'MAIN_ID': '1', 'CN_NAME': '存储过程'}, {'MAIN_ID': '2', 'CN_NAME': 'SQL语句'}];var B43B7C_ary_IS_AUTHORITY = [{'MAIN_ID': '1', 'CN_NAME': '是'}, {'MAIN_ID': '0', 'CN_NAME': '否'}];
/*declare select options end*/

//主从表传递参数
function B43B7C_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function B43B7C_t_proc_name_biz_start(inputdata) {
	B43B7C_param = inputdata;
	layer.close(ly_index);
	B43B7C_param_set();
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_proc_name$DB_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B43B7C_get_N01_t_proc_name$DB_ID");	
    /*biz end*/
}

/*biz step begin*/
function B43B7C_format_DB_ID(value, row, index) {    var objResult = value;    for(i = 0; i < B43B7C_ary_DB_ID.length; i++) {        var obj = B43B7C_ary_DB_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B43B7C_format_INF_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < B43B7C_ary_INF_TYPE.length; i++) {        var obj = B43B7C_ary_INF_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B43B7C_format_IS_AUTHORITY(value, row, index) {    var objResult = value;    for(i = 0; i < B43B7C_ary_IS_AUTHORITY.length; i++) {        var obj = B43B7C_ary_IS_AUTHORITY[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B43B7C_get_N01_t_proc_name$DB_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_name$DB_ID) == false)        return false;    B43B7C_ary_DB_ID = input.N01_t_proc_name$DB_ID;    if($("#B43B7C_DB_ID").is("select") && $("#B43B7C_DB_ID")[0].options.length == 0)    {        $.each(B43B7C_ary_DB_ID, function (i, obj) {            addOptionValue("B43B7C_DB_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#B43B7C_INF_TYPE").is("select") && $("#B43B7C_INF_TYPE")[0].options.length == 0)    {        $.each(B43B7C_ary_INF_TYPE, function (i, obj) {            addOptionValue("B43B7C_INF_TYPE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#B43B7C_IS_AUTHORITY").is("select") && $("#B43B7C_IS_AUTHORITY")[0].options.length == 0)    {        $.each(B43B7C_ary_IS_AUTHORITY, function (i, obj) {            addOptionValue("B43B7C_IS_AUTHORITY", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    B43B7C_init_t_proc_name();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function B43B7C_page_end(){
	if(B43B7C_param["type"] == "edit"){
		B43B7C_get_edit_info();
	}
}

//页面初始化方法
function B43B7C_init_t_proc_name() {
	//type = getUrlParam("type");
	if(B43B7C_param["type"] == "add"){
		$("#B43B7C_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#B43B7C_INF_CN_NAME").val(B43B7C_param["INF_CN_NAME"]);        $("#B43B7C_INF_EN_NAME").val(B43B7C_param["INF_EN_NAME"]);        $("#B43B7C_IS_AUTHORITY").val(B43B7C_param["IS_AUTHORITY"]);		
		/*Get Find Select param end*/	
	}
	else if(B43B7C_param["type"] == "edit"){
	
	}
	
	//表单验证
	B43B7C_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B43B7C_CREATE_DATE").val() == "")    {        $("#B43B7C_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B43B7C_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B43B7C_page_end();
}

//提交表单数据
function B43B7C_SubmitForm(){
	if(B43B7C_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_proc_name",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#B43B7C_DB_ID").val()                ,"param_value2": s_encode($("#B43B7C_INF_CN_NAME").val())                ,"param_value3": s_encode($("#B43B7C_INF_EN_NAME").val())                ,"param_value4": s_encode($("#B43B7C_INF_EN_SQL").val())                ,"param_value5": $("#B43B7C_INF_TYPE").val()                ,"param_value6": s_encode($("#B43B7C_S_DESC").val())                ,"param_value7": $("#B43B7C_CREATE_DATE").val()                ,"param_value8": s_encode($("#B43B7C_REFLECT_IN_CLASS").val())                ,"param_value9": s_encode($("#B43B7C_REFLECT_OUT_CLASS").val())                ,"param_value10": $("#B43B7C_IS_AUTHORITY").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B43B7C_get_N01_ins_t_proc_name");
	}
	else if(B43B7C_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_proc_name",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#B43B7C_DB_ID").val()                ,"param_value2": s_encode($("#B43B7C_INF_CN_NAME").val())                ,"param_value3": s_encode($("#B43B7C_INF_EN_NAME").val())                ,"param_value4": s_encode($("#B43B7C_INF_EN_SQL").val())                ,"param_value5": $("#B43B7C_INF_TYPE").val()                ,"param_value6": s_encode($("#B43B7C_S_DESC").val())                ,"param_value7": $("#B43B7C_CREATE_DATE").val()                ,"param_value8": s_encode($("#B43B7C_REFLECT_IN_CLASS").val())                ,"param_value9": s_encode($("#B43B7C_REFLECT_OUT_CLASS").val())                ,"param_value10": $("#B43B7C_IS_AUTHORITY").val()                ,"param_value11": $("#B43B7C_MAIN_ID").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B43B7C_get_N01_upd_t_proc_name");
	}
}

//vue回调
function B43B7C_t_proc_name_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#B43B7C_save_t_proc_name_Edit").click(function () {
	$("form[name='B43B7C_DataModal']").submit();
})

/*修改数据*/
function B43B7C_get_N01_upd_t_proc_name(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_proc_name) == true)
	{
		swal("修改数据成功!", "", "success");
		A43B7C_t_proc_name_query();
		B43B7C_clear_validate();
		layer.close(B43B7C_param["ly_index"]);
	}
}

/*添加数据*/
function B43B7C_get_N01_ins_t_proc_name(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_proc_name) == true)
	{
		swal("添加数据成功!", "", "success");
		A43B7C_t_proc_name_query();
		B43B7C_clear_validate();
		layer.close(B43B7C_param["ly_index"]);
	}
}

//取消编辑
$("#B43B7C_cancel_t_proc_name_Edit").click(function () {
	layer.close(B43B7C_param["ly_index"]);
	B43B7C_clear_validate();
	$("[id^='B43B7C_div']").hide();
})

//清除查找框
function B43B7C_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B43B7C_clear_validate(){
	$("#B43B7C_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B43B7C_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B43B7C_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B43B7C_clear_edit_info(){
	var inputs = $("#B43B7C_DataModal").find('input');
	var selects = $("#B43B7C_DataModal").find("select");
	var textareas = $("#B43B7C_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	B43B7C_init_t_proc_name();
}

//页面输入框赋值
function B43B7C_get_edit_info(){
	var rowData = $("#A43B7C_t_proc_name_Events").bootstrapTable('getData')[A43B7C_select_t_proc_name_rowId];
	var inputs = $("#B43B7C_DataModal").find('input');
	var selects = $("#B43B7C_DataModal").find("select");
	var textareas = $("#B43B7C_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"A43B7C","B43B7C");
}

//form验证
function B43B7C_checkFormInput() {
    B43B7C_validate = $("#B43B7C_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B43B7C_MAIN_ID: {}            ,B43B7C_DB_ID: {}            ,B43B7C_INF_CN_NAME: {}            ,B43B7C_INF_EN_NAME: {}            ,B43B7C_INF_EN_SQL: {}            ,B43B7C_INF_TYPE: {}            ,B43B7C_S_DESC: {}            ,B43B7C_CREATE_DATE: {date: true,required : true,maxlength:19}            ,B43B7C_REFLECT_IN_CLASS: {}            ,B43B7C_REFLECT_OUT_CLASS: {}            ,B43B7C_IS_AUTHORITY: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B43B7C_MAIN_ID: {}            ,B43B7C_DB_ID: {}            ,B43B7C_INF_CN_NAME: {}            ,B43B7C_INF_EN_NAME: {}            ,B43B7C_INF_EN_SQL: {}            ,B43B7C_INF_TYPE: {}            ,B43B7C_S_DESC: {}            ,B43B7C_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B43B7C_REFLECT_IN_CLASS: {}            ,B43B7C_REFLECT_OUT_CLASS: {}            ,B43B7C_IS_AUTHORITY: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B43B7C_SubmitForm();
        	return false;
        }
    })
}