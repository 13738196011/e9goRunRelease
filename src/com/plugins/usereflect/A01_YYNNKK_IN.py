#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''
import json,os,importlib
from com.zxy.common import Com_Para
from com.zxy.db1.Db_Common1 import Db_Common1
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.plugins.usereflect.A01_STATICDB2_IN import A01_STATICDB2_IN
from com.plugins.B01_2HCM66.B01_2HCM66_Time import B01_2HCM66_Time
from com.zxy.business.Power_Control import Power_Control
from com.zxy.z_debug import z_debug
from com.zxy.common.Com_Fun import Com_Fun
class A01_YYNNKK_IN(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = 1
    def __init__(self):
        pass
    def init_start(self):
        try:        
            self.strContinue = 0
            temdbc2 = Db_Common2()
            bodc = Ope_DB_Cent()
            temStrSql = "select MAIN_ID,TABLE_CN_NAME,TABLE_EN_NAME,TABLE_TYPE,S_DESC from t_single_table where main_id = '"+self.param_value1+"'"
            temResultSet = temdbc2.Common_Sql(temStrSql)
            temListtable_info = bodc.Get_table_list_ByDB("com.plugins.B01_2HCM66.t_table_column", "2", " where table_id = '"+self.param_value1+"'")
            if len(temResultSet) > 0:
                temTableName = Com_Fun.NoNull(temResultSet[0][2])
                temTableType = Com_Fun.NoNull(temResultSet[0][3])
                temViewSql = Com_Fun.NoNull(temResultSet[0][4])
                if temTableType == "1":
                    b2t = B01_2HCM66_Time()
                    b2t.param_value9 = self.param_value2
                    b2t.Create_ViewModel_Py(temTableName,Com_Fun.NoNull(temResultSet[0][1]),temListtable_info,temViewSql)
                    self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"1\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"元数据表创建业务成功\"}]}"                
                    return None
                elif temTableName == "":
                    self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"元数据表创建业务失败，元数据表名不能为空\"}]}"
                    return None
                temCondAva = self.CondAva(self,temTableName,temListtable_info)
                if temCondAva != "":
                    self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"元数据表创建业务失败，"+temCondAva+"\"}]}"
                    return None
                else:
                    b2t = B01_2HCM66_Time()
                    b2t.param_value9 = self.param_value2
                    b2t.Create_SingleTbModel_Py(temTableName,Com_Fun.NoNull(temResultSet[0][1]),temListtable_info)
                self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"1\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"元数据表创建业务成功\"}]}"                
            else:
                self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"元数据表创建业务失败，元数据表记录不存在\"}]}"
                return None
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+repr(e)
            else:
                temLog = self.debug_info()+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"异常:"+repr(e)+"\"}]}"
        self.strContinue = 0
    def CondAva(self,inputTableName,inputListtable_info):
        b01_2h = B01_2HCM66_Time()
        if b01_2h.SindleTable_Py_Had(inputTableName) == True and 1 != 1:
            return "元数据表"+inputTableName+".py文件已经存在，请先执行重置业务"
        else:
            odc = Ope_DB_Cent()
            for temt_table_column in inputListtable_info:
                if temt_table_column.attCOLUMN_QRY == 3 and (temt_table_column.attCOLUMN_QRY_FORMAT == "" or temt_table_column.attCOLUMN_FIND_HTML == "" or temt_table_column.attCOLUMN_FIND_RETURN == ""):
                    return temt_table_column.attCOLUMN_CN_NAME+"["+temt_table_column.attCOLUMN_EN_NAME+"]下拉框或查找框数据集,查找框子页面,查找框返回值均不能为空"
                elif temt_table_column.attCOLUMN_QRY == 2 and temt_table_column.attCOLUMN_QRY_FORMAT == "":
                    return temt_table_column.attCOLUMN_CN_NAME+"["+temt_table_column.attCOLUMN_EN_NAME+"]下拉框或查找框数据集不能为空"
                elif temt_table_column.attCOLUMN_QRY_FORMAT != "" and temt_table_column.attCOLUMN_QRY_FORMAT.upper().find("SELECT ") == 0:
                    return ""
                elif temt_table_column.attCOLUMN_QRY_FORMAT != "":
                    try:
                        jsonObject = json.loads(temt_table_column.attCOLUMN_QRY_FORMAT)
                        return ""
                    except Exception as e:
                        if odc.get_t_proc_name_id(temt_table_column.attCOLUMN_QRY_FORMAT) == "":
                            return temt_table_column.attCOLUMN_CN_NAME+"["+temt_table_column.attCOLUMN_EN_NAME+"]下拉框或查找框数据集数据接口"+temt_table_column.attCOLUMN_QRY_FORMAT+"不存在"
            return ""