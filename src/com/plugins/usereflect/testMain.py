#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import OpenOPC
from ctypes import cdll
if __name__ == '__main__':
    opc = OpenOPC.open_client("192.168.1.173")#("Matrikon.OPC.Simulation.1","Group0")
    opc.connect('Matrikon.OPC.Simulation.1', 'localhost')
    taglist=['a01','Random.Int2','Random.Real4']
    opc_datas = opc.read(taglist)
    datas = [i[1] for i in opc_datas]
    opc_data = opc.read(taglist[0])
    data = opc_data[1]
    opc.close()
    print(data)
    p = '/opt/python3/lib/python3.6/lib-dynload/_sqlite3.cpython-36m-arm-linux-gnueabihf.so'
    f = cdll.LoadLibrary(p)
    print(f)
    if f is None:
        print("error")
    else:
        print("ok")
    print(f)        
