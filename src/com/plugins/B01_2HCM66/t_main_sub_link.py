#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_main_sub_link(object):
    attmain_id = -1
    attMAIN_TB_NAME = ""
    attSUB_TB_NAME = ""
    attMAIN_TITLE_COLUMN = ""
    lenMAIN_TB_NAME = 50
    lenSUB_TB_NAME = 50
    lenMAIN_TITLE_COLUMN = 50
    def __init__(self):
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["MAIN_TB_NAME"] = "主表名及字段"
        temHt["SUB_TB_NAME"] = "从表名及字段"
        temHt["MAIN_TITLE_COLUMN"] = "主表关联显示字段"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["MAIN_TB_NAME"] = "select b.MAIN_ID,a.TABLE_EN_NAME ||'=>' || b.COLUMN_EN_NAME as CN_NAME From t_single_table a join t_table_column b on a.main_id = b.table_id"
        temHtSelect["SUB_TB_NAME"] = "select b.MAIN_ID,a.TABLE_EN_NAME ||'=>' || b.COLUMN_EN_NAME as CN_NAME From t_single_table a join t_table_column b on a.main_id = b.table_id"
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        temHtFind["MAIN_TB_NAME"] = "V_VTC001.vue"
        temHtFind["SUB_TB_NAME"] = "V_VTC001.vue"
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        temHtFReturn["MAIN_TB_NAME"] = ["MAIN_ID","COLUMN_EN_NAME"]
        temHtFReturn["SUB_TB_NAME"] = ["MAIN_ID","COLUMN_EN_NAME"]
        return temHtFReturn
