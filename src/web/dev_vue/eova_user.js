//主从表tab选项卡标志位
var AF958F_Tab_Flag = -1;
//选择某一行
var AF958F_select_eova_user_rowId = "";
//按钮事件新增或编辑
var AF958F_type = "";
//其他页面传到本页面参数
var AF958F_param = {};
//table选中数据，如无则默认第一条
var AF958F_rowCheckData = null;

/*定义查询条件变量*/
/*declare query param begin*/
var AF958F_tem_login_id = "";var AF958F_tem_nickname = "";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var AF958F_ary_rid = [{'MAIN_ID': '1', 'CN_NAME': '超级管理员'}, {'MAIN_ID': '2', 'CN_NAME': '系统管理员'}, {'MAIN_ID': '3', 'CN_NAME': '运维人员'}, {'MAIN_ID': '4', 'CN_NAME': '普通用户'}];var AF958F_ary_S_STATE = [{'MAIN_ID': '0', 'CN_NAME': '启用'}, {'MAIN_ID': '1', 'CN_NAME': '禁用'}];
/*declare select options end*/

/*绑定show监听事件*/
if(AF958F_Tab_Flag == "-1"){
	var n = Get_RandomDiv("AF958F","");
	$(n).bind("show", function(objTag){
		AF958F_Adjust_Sub_Sequ();
		if(AF958F_Tab_Flag > 0)
			AF958F_adjust_tab();		
	});
}

//设置主从表div顺序
function AF958F_Adjust_Sub_Sequ(){
	var temSubDivs = $("#content-main").find("div[data-id]");
	var MainDiv = $(Get_RandomDiv("AF958F",""));
	for(i = 0; i < temSubDivs.length; i++) {
		if(AF958F_Is_Sub_Div(temSubDivs[i].id)){
			$(temSubDivs[i]).before($(MainDiv));
			break;
		}
	}
}

//主从表传递参数
function AF958F_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function AF958F_eova_user_biz_start(inputparam) {
	layer.close(ly_index);
	AF958F_param = inputparam;
	//主从表传递参数
	AF958F_param_set();	
    /*biz begin*/
    if($("#AF958F_qry_rid").is("select") && $("#AF958F_qry_rid")[0].options.length == 0)    {        $("#AF958F_qry_rid").append("<option value='-1'></option>")        $.each(AF958F_ary_rid, function (i, obj) {            addOptionValue("AF958F_qry_rid", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#AF958F_qry_S_STATE").is("select") && $("#AF958F_qry_S_STATE")[0].options.length == 0)    {        $("#AF958F_qry_S_STATE").append("<option value='-1'></option>")        $.each(AF958F_ary_S_STATE, function (i, obj) {            addOptionValue("AF958F_qry_S_STATE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    AF958F_init_eova_user()	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function AF958F_format_rid(value, row, index) {    var objResult = value;    for(i = 0; i < AF958F_ary_rid.length; i++) {        var obj = AF958F_ary_rid[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AF958F_format_S_STATE(value, row, index) {    var objResult = value;    for(i = 0; i < AF958F_ary_S_STATE.length; i++) {        var obj = AF958F_ary_S_STATE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function AF958F_page_end(){
	AF958F_adjust_tab();
}

//系统用户显示列定义
var AF958F_eova_user = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'ID',        sortable: true,        visible: false    },    {        title: '登录账号',        field: 'LOGIN_ID',        sortable: true        ,formatter: set_s_decode    },    {        title: '登录密码',        field: 'LOGIN_PWD',        sortable: true        ,formatter: set_pwd    },    {        title: '用户姓名',        field: 'NICKNAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '所属角色',        field: 'RID',        sortable: true        ,formatter: AF958F_format_rid    },    {        title: '是否禁用',        field: 'S_STATE',        sortable: true        ,formatter: AF958F_format_S_STATE    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function AF958F_init_eova_user() {
	$(window).resize(function () {
		  $('#AF958F_eova_user_Events').bootstrapTable('resetView');
	});
	//系统用户查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AF958F_btn_eova_user_query').click();
}

//查询接口
function AF958F_eova_user_query() {
    $('#AF958F_eova_user_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_eova_user",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(AF958F_tem_login_id)        ,"param_value2": s_encode(AF958F_tem_nickname)		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AF958F_get_N01_sel_eova_user");
}

//查询结果
function AF958F_get_N01_sel_eova_user(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_eova_user) == false)
        return false;
    AF958F_rowCheckData = null;
    //调整table各列宽度
    $.each(AF958F_eova_user, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_eova_user;
    if(s_data.length > 0)
    	AF958F_rowCheckData = s_data[0];    
    AF958F_select_eova_user_rowId = "";
    AF958F_Tab_Flag = 0;
    $('#AF958F_eova_user_Events').bootstrapTable('destroy');
    $("#AF958F_eova_user_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AF958F_eova_user_Events').find("tr.changeColor").removeClass('changeColor');
                AF958F_select_eova_user_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AF958F_eova_user_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AF958F_select_eova_user_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#AF958F_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AF958F_param.hasOwnProperty("target_name"))
			{
				$("#"+AF958F_param["target_id"]).val(eval("row."+AF958F_param["sourc_id"].toUpperCase()));
				$("#"+AF958F_param["target_name"]).val(s_decode(eval("row."+AF958F_param["sourc_name"].toUpperCase())));				
				layer.close(AF958F_param["ly_index"]);
			}
		},
        toolbar: "#AF958F_eova_user_Toolbar",
        columns: AF958F_eova_user,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        onPostBody:function(){
        	if(s_data.length != 0)
            	AF958F_page_end();
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AF958F_btn_eova_user_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    AF958F_tem_login_id = "";    AF958F_tem_nickname = "";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/

	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    if($("#AF958F_qry_rid").is("select") && $("#AF958F_qry_rid")[0].options.length == 0)    {        $("#AF958F_qry_rid").append("<option value='-1'></option>")        $.each(AF958F_ary_rid, function (i, obj) {            addOptionValue("AF958F_qry_rid", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#AF958F_qry_S_STATE").is("select") && $("#AF958F_qry_S_STATE")[0].options.length == 0)    {        $("#AF958F_qry_S_STATE").append("<option value='-1'></option>")        $.each(AF958F_ary_S_STATE, function (i, obj) {            addOptionValue("AF958F_qry_S_STATE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    AF958F_init_eova_user()	
    /*biz end*/
})

//查询按钮
$('#AF958F_btn_eova_user_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    AF958F_tem_login_id = $("#AF958F_qry_login_id").val();    AF958F_tem_nickname = $("#AF958F_qry_nickname").val();	
    /*set query param end*/
	AF958F_eova_user_query();
})

//vue回调
function AF958F_eova_user_call_vue(objResult){
	//选择某条记录或自动选择第一条
	if (AF958F_select_eova_user_rowId != "") 
		AF958F_rowCheckData = $("#AF958F_eova_user_Events").bootstrapTable('getData')[AF958F_select_eova_user_rowId];
	
	if(index_subhtml == "eova_user_$.vue")
	{
		var n = Get_RandomDiv("BF958F",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AF958F_type,"ly_index":index};
				if(AF958F_param.hasOwnProperty("hidden_find")){
					inputdata["hidden_find"] = "1";
				}				
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temlogin_id = $("#AF958F_qry_login_id").val();                if(temlogin_id != ""){                    inputdata["login_id"] = temlogin_id;                }                var temnickname = $("#AF958F_qry_nickname").val();                if(temnickname != ""){                    inputdata["nickname"] = temnickname;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("dev_vue/eova_user_$.js","BF958F_eova_user_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/

	/*get tab subvue end*/
}

//新增按钮
$("#AF958F_btn_eova_user_add").click(function () {
	AF958F_type = "add";
	index_subhtml = "eova_user_$.vue";
	if(loadHtmlSubVueFun("dev_vue/eova_user_$.vue","AF958F_eova_user_call_vue") == true){
		var n = Get_RandomDiv("BF958F","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BF958F_param["type"] = AF958F_type;
				BF958F_param["ly_index"]= index;
				if(AF958F_param.hasOwnProperty("hidden_find")){
					BF958F_param["hidden_find"] = "1";
				}
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temlogin_id = $("#AF958F_qry_login_id").val();                if(temlogin_id != ""){                    BF958F_param["login_id"] = temlogin_id;                }                var temnickname = $("#AF958F_qry_nickname").val();                if(temnickname != ""){                    BF958F_param["nickname"] = temnickname;                }				
				/*Send Two FindSelect param end*/

				BF958F_clear_edit_info();
			},
			end: function(){
				BF958F_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AF958F_btn_eova_user_edit").click(function () {
	if (AF958F_select_eova_user_rowId != "") {
		AF958F_type = "edit";
		index_subhtml = "eova_user_$.vue";
		if(loadHtmlSubVueFun("dev_vue/eova_user_$.vue","AF958F_eova_user_call_vue") == true){
			var n = Get_RandomDiv("BF958F","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BF958F_param["type"] = AF958F_type;
					BF958F_param["ly_index"] = index;
					if(AF958F_param.hasOwnProperty("hidden_find")){
						BF958F_param["hidden_find"] = "1";
					}
					BF958F_clear_edit_info();
					BF958F_get_edit_info();
					$("#BF958F_login_pwd").val("*");
				},
				end: function(){
					BF958F_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AF958F_btn_eova_user_delete').click(function () {
	//单行选择
	var rowData = $("#AF958F_eova_user_Events").bootstrapTable('getData')[AF958F_select_eova_user_rowId];
	//多行选择
	var rowDatas = AF958F_sel_row_eova_user();
	if (AF958F_select_eova_user_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.ID;
					var inputdata = {
						"param_name": "N01_del_eova_user",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AF958F_N01_del_eova_user");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["ID"];
				var inputdata = {
					"param_name": "N01_del_eova_user",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AF958F_N01_del_eova_user");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AF958F_N01_del_eova_user(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_eova_user) == true)
		AF958F_eova_user_query();
}

function set_pwd(value, row, index) {
    return "*";
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function AF958F_adjust_tab(){
	if(typeof($("#AF958F_TAB_MAIN")[0]) != "undefined" && $("#AF958F_TAB_MAIN")[0].length != 0){
		AF958F_Tab_Flag = 1;
		$(Get_RDivNoBuild("AF958F","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("AF958F","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("AF958F","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#AF958F_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/
//隐藏tab页选项卡function AF958F_hide_tab_fun(){    var n = null;}//判断是否sub子项div页面function AF958F_Is_Sub_Div(temDivId){    if(temDivId.indexOf("XX$TTT") == 0)        return false;    return false;}
/*Tab Click Fun End*/

function AF958F_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("dev_vue/"+inputUrl,"AF958F_eova_user_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (AF958F_select_eova_user_rowId != "") 
			rowData = $("#AF958F_eova_user_Events").bootstrapTable('getData')[AF958F_select_t_proc_name_rowId];
		else
			rowData = AF958F_rowCheckData;
		if(rowData != null){
			var temFlag = true;
			$.each(temPar, function (i, obj) {
				if(eval(inputrandom+"_param.hasOwnProperty(\""+obj.target_id+"\")") && eval(inputrandom+"_param[\""+obj.target_id+"\"]").toString() == rowData[obj.sourc_id].toString())
					temFlag = false;
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			if(temFlag){
				//传递子页面隐藏find功能
				eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
				//参数传递并赋值
				eval(inputrandom+"_param_set()");
				var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
				$("#"+inputrandom+"_btn_"+tbName+"_query").click();
			}
		}
		else{
			$.each(temPar, function (i, obj) {
				if(obj.sourc_id.toUpperCase() == "MAIN_ID")
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"-999\"");
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"\"");
			});
			//传递子页面隐藏find功能
			eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
			var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
			$("#"+inputrandom+"_btn_"+tbName+"_query").click();		
		}
	}
}

//清除 查找框
function AF958F_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AF958F_sel_row_eova_user(){
	//获得选中行
	var checkedbox= $("#AF958F_eova_user_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}