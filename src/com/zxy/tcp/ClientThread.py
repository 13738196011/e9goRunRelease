#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import datetime
import socket
import threading
import time
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.tcp.TcpClient import TcpClient
from com.zxy.z_debug import z_debug
class ClientThread(z_debug):
    idtTimeOut = 60
    sServerIP = "0.0.0.0"
    iPort = 6000
    connectionFlag = False
    def __init__(self, timeout, ServerIP, temPort):
        self.idtTimeOut = timeout
        self.sServerIP = ServerIP
        self.iPort = temPort
    def run(self):
        starttime = datetime.datetime.now()       
        endtime = datetime.datetime.now() + datetime.timedelta(seconds=self.idtTimeOut)        
        ckeys = self.sServerIP + "|" + str(self.iPort)
        tc = TcpClient(ckeys)
        t = None        
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((self.sServerIP, self.iPort))
            Com_Fun.SetHashTable(Com_Para.dClientThreadList, ckeys, s)            
            temResult = "@##ST=01&DATATIME=" + Com_Fun.GetTime("%Y%m%d%H%M%S") + "&CHILD_NODE="+ Com_Para.Child_Code + "&CHILD_NAME="+Com_Para.Child_Name+"##@"
            Com_Fun.SendSocket(temResult,s)            
            t = threading.Thread(target=tc.client_link, name="ClientTh" + ckeys)
            t.start()
        except IOError as e:
            temError = "repeat connect server error:"+self.sServerIP+" "+Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") + repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temError)
            uL.WriteLog()
        while True:
            starttime = datetime.datetime.now()
            try:
                if t is None and starttime <= endtime:
                    if Com_Fun.GetHashTableNone(Com_Para.dClientThreadList, ckeys) is not None:
                        t = threading.Thread(target=tc.client_link, name="ClientTh" + self.sServerIP)
                        t.start()
                    time.sleep(self.idtTimeOut/2)
                elif starttime >= endtime:
                    endtime = datetime.datetime.now() + datetime.timedelta(seconds=self.idtTimeOut)                
                    if Com_Fun.GetHashTableNone(Com_Para.dClientThreadList, ckeys) is None:
                        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                        s.connect((self.sServerIP, self.iPort))
                        Com_Fun.SetHashTable(Com_Para.dClientThreadList, s.getpeername()[0]+"|"+str(s.getpeername()[1]), s)
                        t = threading.Thread(target=tc.client_link, name="ClientTh" + self.sServerIP)
                        t.start()
                    time.sleep(self.idtTimeOut/2)
                elif (endtime - starttime).seconds > 10 * self.idtTimeOut:
                    endtime = datetime.datetime.now() + datetime.timedelta(seconds=self.idtTimeOut)
                else:
                    time.sleep(self.idtTimeOut/2)
            except IOError as e:
                temError = "repeat connect server error:"+self.sServerIP+" "+Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") + repr(e)
                uL = UsAdmin_Log(Com_Para.ApplicationPath, temError)
                uL.WriteLog()
    def list_run(self):
        starttime = datetime.datetime.now()       
        endtime = datetime.datetime.now() + datetime.timedelta(seconds=self.idtTimeOut)        
        ckeys = self.sServerIP + "|" + str(self.iPort)
        tc = TcpClient(ckeys)
        t = None        
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((self.sServerIP, self.iPort))
            Com_Fun.SetHashTable(Com_Para.dClientThreadList, ckeys, s)      
            t = threading.Thread(target=tc.client_link, name="ClientTh" + ckeys)
            t.start()
        except IOError as e:
            temError = "repeat connect server error:"+self.sServerIP+" "+Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") + repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temError)
            uL.WriteLog()   
        while True:
            starttime = datetime.datetime.now()
            try:
                if t is None and starttime <= endtime:
                    if Com_Fun.GetHashTableNone(Com_Para.dClientThreadList, ckeys) is not None:
                        t = threading.Thread(target=tc.client_link, name="ClientTh" + self.sServerIP)
                        t.start()
                    time.sleep(self.idtTimeOut/2)
                elif starttime >= endtime:
                    endtime = datetime.datetime.now() + datetime.timedelta(seconds=self.idtTimeOut)                
                    if Com_Fun.GetHashTableNone(Com_Para.dClientThreadList, ckeys) is None:
                        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                        s.connect((self.sServerIP, self.iPort))
                        Com_Fun.SetHashTable(Com_Para.dClientThreadList, s.getpeername()[0]+"|"+str(s.getpeername()[1]), s)
                        t = threading.Thread(target=tc.client_link, name="ClientTh" + self.sServerIP)
                        t.start()
                    time.sleep(self.idtTimeOut/2)
                elif (endtime - starttime).seconds > 10 * self.idtTimeOut:
                    endtime = datetime.datetime.now() + datetime.timedelta(seconds=self.idtTimeOut)
                else:
                    time.sleep(self.idtTimeOut/2)
            except IOError as e:
                temError = "repeat connect server error:"+self.sServerIP+" "+Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") + repr(e)
                uL = UsAdmin_Log(Com_Para.ApplicationPath, temError)
                uL.WriteLog()
