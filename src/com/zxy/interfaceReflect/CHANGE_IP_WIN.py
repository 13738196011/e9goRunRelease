#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
import pythoncom
import wmi
from com.zxy.z_debug import z_debug
class CHANGE_IP_WIN(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = "CHANGE_IP_WIN"
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None    
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    param_value11     = None  
    param_value12     = None
    param_value13     = None
    param_value14     = None
    param_value15     = None
    param_value16     = None
    param_value17     = None
    param_value18     = None
    param_value19     = None
    param_value20     = None
    strContinue      = "1"
    def __init__(self):
        pass
    def init_start(self):        
        if self.param_value6 == "1":
            self.get_ip_info(self)
        else: #self.param_value6 == "2":  
            self.c_ip1(self)
    def get_ip_info(self):
        try:
            pythoncom.CoInitialize()
            c = wmi.WMI()
            nic_configs = c.Win32_NetworkAdapterConfiguration(IPEnabled=True)
            if len(nic_configs) < 1:
                self.strResult = "{\""+self.param_name+"\": [{\""+Com_Fun.GetLowUpp("s_result")+"\": 0,\""+Com_Fun.GetLowUpp("error_desc")+"\": \"没有找到可用的网络适配器\",\""+Com_Fun.GetLowUpp("ip")+"\":\"\",\""+Com_Fun.GetLowUpp("netmask")+"\":\"\",\""+Com_Fun.GetLowUpp("gateway")+"\":\"\",\""+Com_Fun.GetLowUpp("dns")+"\":\"\"}]}"
                self.strContinue = "0"
                exit()
            objNicConfig = nic_configs[int(self.param_value5)-1]
            self.strResult = "{\""+self.param_name+"\": [{\""+Com_Fun.GetLowUpp("s_result")+"\": 1,\""+Com_Fun.GetLowUpp("error_desc")+"\": \"获取IP成功\""
            self.strResult = self.strResult +",\""+Com_Fun.GetLowUpp("ip")+"\":\""+objNicConfig.IPAddress[0]+"\""
            self.strResult = self.strResult +",\""+Com_Fun.GetLowUpp("netmask")+"\":\""+objNicConfig.IPSubnet[0]+"\""
            self.strResult = self.strResult +",\""+Com_Fun.GetLowUpp("gateway")+"\":\""+objNicConfig.DefaultIPGateway[0]+"\""
            self.strResult = self.strResult +",\""+Com_Fun.GetLowUpp("dns")+"\":\""+objNicConfig.DNSServerSearchOrder[0]+"\""
            self.strResult = self.strResult +",\""+Com_Fun.GetLowUpp("mac")+"\":\""+Com_Fun.GetMacAddr()+"\""
            self.strResult = self.strResult + "}]}"
            self.strContinue = "0"
        except Exception as e:
            self.strResult = "{\""+self.param_name+"\": [{\""+Com_Fun.GetLowUpp("s_result")+"\": 0,\""+Com_Fun.GetLowUpp("error_desc")+"\": \"获取IP失败"+Com_Fun.py_urlencode(repr(e))+"\",\""+Com_Fun.GetLowUpp("ip")+"\":\"\",\""+Com_Fun.GetLowUpp("netmask")+"\":\"\",\""+Com_Fun.GetLowUpp("gateway")+"\":\"\",\""+Com_Fun.GetLowUpp("dns")+"\":\"\",\""+Com_Fun.GetLowUpp("mac")+"\":\"\"}]}"
            self.strContinue = "0" 
        finally:
            pythoncom.CoUninitialize()
            pass    
    def c_ip1(self):
        try:
            pythoncom.CoInitialize()
            nic_configs = wmi.WMI().Win32_NetworkAdapterConfiguration(IPEnabled=True)
            if len(nic_configs) < 1:
                self.strResult = "{\""+self.param_name+"\": [{\""+Com_Fun.GetLowUpp("s_result")+"\": 0,\""+Com_Fun.GetLowUpp("error_desc")+"\": \"没有找到可用的网络适配器\",\""+Com_Fun.GetLowUpp("ip")+"\":\"\",\""+Com_Fun.GetLowUpp("netmask")+"\":\"\",\""+Com_Fun.GetLowUpp("gateway")+"\":\"\",\""+Com_Fun.GetLowUpp("dns")+"\":\"\",\""+Com_Fun.GetLowUpp("mac")+"\":\"\"}]}"
                self.strContinue = "0"
                exit()
            nic = nic_configs[int(self.param_value5)-1]
            ip = self.param_value1
            subnetmask = self.param_value2
            gateway = self.param_value3
            temdns = self.param_value4            
            nic.EnableStatic(IPAddress=[ip],SubnetMask=[subnetmask])
            nic.SetGateways(DefaultIPGateway=[gateway])
            nic.SetDNSServerSearchOrder(DNSServerSearchOrder=[temdns])   
            self.strResult = "{\""+self.param_name+"\": [{\""+Com_Fun.GetLowUpp("s_result")+"\": 1,\""+Com_Fun.GetLowUpp("error_desc")+"\": \"更改IP成功\",\""+Com_Fun.GetLowUpp("ip")+"\":\"\",\""+Com_Fun.GetLowUpp("netmask")+"\":\"\",\""+Com_Fun.GetLowUpp("gateway")+"\":\"\",\""+Com_Fun.GetLowUpp("dns")+"\":\"\",\""+Com_Fun.GetLowUpp("mac")+"\":\"\"}]}"               
            self.strContinue = "0"     
        except Exception as e:
            self.strResult = "{\""+self.param_name+"\": [{\""+Com_Fun.GetLowUpp("s_result")+"\": 0,\""+Com_Fun.GetLowUpp("error_desc")+"\": \"更改IP失败"+Com_Fun.py_urlencode(repr(e))+"\",\""+Com_Fun.GetLowUpp("ip")+"\":\"\",\""+Com_Fun.GetLowUpp("netmask")+"\":\"\",\""+Com_Fun.GetLowUpp("gateway")+"\":\"\",\""+Com_Fun.GetLowUpp("dns")+"\":\"\",\""+Com_Fun.GetLowUpp("mac")+"\":\"\"}]}"
            self.strContinue = "0"
        finally:            
            pythoncom.CoUninitialize()
            pass