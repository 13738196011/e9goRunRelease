//按钮事件新增或编辑
var BE35F9_type = "";
//其他页面传到本页面参数
var BE35F9_param = {};
//暂时没用
var BE35F9_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BE35F9_ary_file_id = null;
/*declare select options end*/

//业务逻辑数据开始
function BE35F9_t_carbon_info_biz_start(inputdata) {
	BE35F9_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_carbon_info$file_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BE35F9_get_N01_t_carbon_info$file_id");	
    /*biz end*/
}

/*biz step begin*/
function BE35F9_format_file_id(value, row, index) {    var objResult = value;    for(i = 0; i < BE35F9_ary_file_id.length; i++) {        var obj = BE35F9_ary_file_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BE35F9_get_N01_t_carbon_info$file_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_carbon_info$file_id) == false)        return false;    BE35F9_ary_file_id = input.N01_t_carbon_info$file_id;    if($("#BE35F9_file_id").is("select") && $("#BE35F9_file_id")[0].options.length == 0)    {        $.each(BE35F9_ary_file_id, function (i, obj) {            addOptionValue("BE35F9_file_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    BE35F9_init_t_carbon_info();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function BE35F9_file_id_cn_name_fun(){    index_subhtml = "t_file_info.vue"    random_subhtml = "AE32DA";    if(loadHtmlSubVueFun("biz_vue/t_file_info.vue","BE35F9_t_carbon_info_call_vue") == true){        var n = Get_RandomDiv("AE32DA","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#AE32DA_t_file_info_Events').bootstrapTable('resetView');                AE32DA_param["ly_index"] = index;                AE32DA_param["target_name"] = "BE35F9_find_file_id_cn_name"                AE32DA_param["target_id"] = "BE35F9_file_id"                AE32DA_param["sourc_id"] = "MAIN_ID"                AE32DA_param["sourc_name"] = "FILE_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function BE35F9_page_end(){
	if(BE35F9_param["type"] == "edit"){
		BE35F9_get_edit_info();
	}
}

//页面初始化方法
function BE35F9_init_t_carbon_info() {
	//type = getUrlParam("type");
	if(BE35F9_param["type"] == "add"){
		$("#BE35F9_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BE35F9_get_time").val(BE35F9_param["get_time"]);        $("#BE35F9_file_id").val(BE35F9_param["file_id"]);        $("#BE35F9_find_file_id_cn_name").val(BE35F9_param["file_id_cn_name"]);		
		/*Get Find Select param end*/	
	}
	else if(BE35F9_param["type"] == "edit"){
	
	}
	
	//表单验证
	BE35F9_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BE35F9_get_time").val() == "")    {        $("#BE35F9_get_time").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BE35F9_get_time',        type: 'datetime',        trigger: 'click'    });    if($("#BE35F9_create_date").val() == "")    {        $("#BE35F9_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BE35F9_create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BE35F9_page_end();
}

//提交表单数据
function BE35F9_SubmitForm(){
	if(BE35F9_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_carbon_info",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#BE35F9_file_id").val()                ,"param_value2": $("#BE35F9_get_time").val()                ,"param_value3": $("#BE35F9_coal_carbon").val()                ,"param_value4": $("#BE35F9_oil_carbon").val()                ,"param_value5": $("#BE35F9_gas_carbon").val()                ,"param_value6": $("#BE35F9_ele_carbon").val()                ,"param_value7": $("#BE35F9_hot_carbon").val()                ,"param_value8": $("#BE35F9_create_date").val()                ,"param_value9": s_encode($("#BE35F9_s_desc").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BE35F9_get_N01_ins_t_carbon_info");
	}
	else if(BE35F9_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_carbon_info",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#BE35F9_file_id").val()                ,"param_value2": $("#BE35F9_get_time").val()                ,"param_value3": $("#BE35F9_coal_carbon").val()                ,"param_value4": $("#BE35F9_oil_carbon").val()                ,"param_value5": $("#BE35F9_gas_carbon").val()                ,"param_value6": $("#BE35F9_ele_carbon").val()                ,"param_value7": $("#BE35F9_hot_carbon").val()                ,"param_value8": $("#BE35F9_create_date").val()                ,"param_value9": s_encode($("#BE35F9_s_desc").val())                ,"param_value10": $("#BE35F9_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BE35F9_get_N01_upd_t_carbon_info");
	}
}

//vue回调
function BE35F9_t_carbon_info_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_file_info.vue"){        var n = Get_RandomDiv("AE32DA",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":BE35F9_type,                    "ly_index":index,                    "target_name":"BE35F9_find_file_id_cn_name",                    "target_id":"BE35F9_find_file_id",                    "sourc_id":"MAIN_ID",                    "sourc_name":"FILE_NAME"                };                loadScript_hasparam("biz_vue/t_file_info.js","AE32DA_t_file_info_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//for表单提交
$("#BE35F9_save_t_carbon_info_Edit").click(function () {
	$("form[name='BE35F9_DataModal']").submit();
})

/*修改数据*/
function BE35F9_get_N01_upd_t_carbon_info(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_carbon_info) == true)
	{
		swal("修改数据成功!", "", "success");
		AE35F9_t_carbon_info_query();
		BE35F9_clear_validate();
		layer.close(BE35F9_param["ly_index"]);
	}
}

/*添加数据*/
function BE35F9_get_N01_ins_t_carbon_info(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_carbon_info) == true)
	{
		swal("添加数据成功!", "", "success");
		AE35F9_t_carbon_info_query();
		BE35F9_clear_validate();
		layer.close(BE35F9_param["ly_index"]);
	}
}

//取消编辑
$("#BE35F9_cancel_t_carbon_info_Edit").click(function () {
	layer.close(BE35F9_param["ly_index"]);
	BE35F9_clear_validate();
	$("[id^='BE35F9_div']").hide();
})

//清除查找框
function BE35F9_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BE35F9_clear_validate(){
	$("#BE35F9_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BE35F9_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BE35F9_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BE35F9_clear_edit_info(){
	var inputs = $("#BE35F9_DataModal").find('input');
	var selects = $("#BE35F9_DataModal").find("select");
	var textareas = $("#BE35F9_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
    BE35F9_clear_input_cn_name('BE35F9_find_file_id_cn_name','BE35F9_file_id')	
	/*input validate clear end*/
	BE35F9_init_t_carbon_info();
}

//页面输入框赋值
function BE35F9_get_edit_info(){
	var rowData = $("#AE35F9_t_carbon_info_Events").bootstrapTable('getData')[AE35F9_select_t_carbon_info_rowId];
	var inputs = $("#BE35F9_DataModal").find('input');
	var selects = $("#BE35F9_DataModal").find("select");
	var textareas = $("#BE35F9_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"AE35F9","BE35F9");
	
	/*
	$.each(selects, function (i, obj) {
		if(typeof(eval("AE35F9_ary_"+obj.id.toString().replace("BE35F9_",""))) == "object")
		{
			$.each(eval("AE35F9_ary_"+obj.id.toString().replace("BE35F9_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE35F9_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("BE35F9_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("AE35F9_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#BE35F9_DataModal").find('[id="'+"BE35F9_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE35F9_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE35F9_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});*/
}

//form验证
function BE35F9_checkFormInput() {
    BE35F9_validate = $("#BE35F9_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BE35F9_main_id: {}            ,BE35F9_file_id: {}            ,BE35F9_get_time: {date: true,required : true,maxlength:19}            ,BE35F9_coal_carbon: {number: true,required : true,maxlength:14}            ,BE35F9_oil_carbon: {number: true,required : true,maxlength:14}            ,BE35F9_gas_carbon: {number: true,required : true,maxlength:14}            ,BE35F9_ele_carbon: {number: true,required : true,maxlength:14}            ,BE35F9_hot_carbon: {number: true,required : true,maxlength:14}            ,BE35F9_create_date: {date: true,required : true,maxlength:19}            ,BE35F9_s_desc: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BE35F9_main_id: {}            ,BE35F9_file_id: {}            ,BE35F9_get_time: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BE35F9_coal_carbon: {number: "必须输入合法的小数",required : "必须输入合法的小数",maxlength:"长度不能超过14"}            ,BE35F9_oil_carbon: {number: "必须输入合法的小数",required : "必须输入合法的小数",maxlength:"长度不能超过14"}            ,BE35F9_gas_carbon: {number: "必须输入合法的小数",required : "必须输入合法的小数",maxlength:"长度不能超过14"}            ,BE35F9_ele_carbon: {number: "必须输入合法的小数",required : "必须输入合法的小数",maxlength:"长度不能超过14"}            ,BE35F9_hot_carbon: {number: "必须输入合法的小数",required : "必须输入合法的小数",maxlength:"长度不能超过14"}            ,BE35F9_create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BE35F9_s_desc: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BE35F9_SubmitForm();
        	return false;
        }
    })
}