#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_proc_return(object):
    attmain_id = -1
    attPROC_ID = 0
    attRETURN_NAME = ""
    attRETURN_TYPE = ""
    attS_DESC = ""
    attCREATE_DATE = None
    attIS_IMG = 0
    attIS_URLENCODE = 0
    attLIST_HIDDEN = 0
    lenRETURN_NAME = 50
    lenRETURN_TYPE = 50
    lenS_DESC = 500
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["PROC_ID"] = "接口名称"
        temHt["RETURN_NAME"] = "接口返回值名称"
        temHt["RETURN_TYPE"] = "返回值类型"
        temHt["S_DESC"] = "备注"
        temHt["CREATE_DATE"] = "系统时间"
        temHt["IS_IMG"] = "是否图片"
        temHt["IS_URLENCODE"] = "是否URL编码"
        temHt["LIST_HIDDEN"] = "扩展字段"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["PROC_ID"] = "N01_t_proc_inparam$PROC_ID"
        temHtSelect["RETURN_TYPE"] = "select id as MAIN_ID,cn as CN_NAME from t_proc_return_type"
        temHtSelect["IS_IMG"] = [{"MAIN_ID":"1","CN_NAME":"是"},{"MAIN_ID":"0","CN_NAME":"否"}]
        temHtSelect["IS_URLENCODE"] = [{"MAIN_ID":"1","CN_NAME":"是"},{"MAIN_ID":"0","CN_NAME":"否"}]
        temHtSelect["LIST_HIDDEN"] = [{"MAIN_ID":"1","CN_NAME":"是"},{"MAIN_ID":"0","CN_NAME":"否"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        temHtFind["PROC_ID"] = "t_proc_name.vue"
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        temHtFReturn["PROC_ID"] = ["MAIN_ID","INF_CN_NAME"]
        return temHtFReturn
