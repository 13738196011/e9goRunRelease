//按钮事件新增或编辑
var BE32DA_type = "";
//其他页面传到本页面参数
var BE32DA_param = {};
//暂时没用
var BE32DA_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BE32DA_ary_file_type_id = null;var BE32DA_ary_is_ava = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];
/*declare select options end*/

//业务逻辑数据开始
function BE32DA_t_file_info_biz_start(inputdata) {
	BE32DA_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_file_info$file_type_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BE32DA_get_N01_t_file_info$file_type_id");	
    /*biz end*/
}

/*biz step begin*/
function BE32DA_format_file_type_id(value, row, index) {    var objResult = value;    for(i = 0; i < BE32DA_ary_file_type_id.length; i++) {        var obj = BE32DA_ary_file_type_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BE32DA_format_is_ava(value, row, index) {    var objResult = value;    for(i = 0; i < BE32DA_ary_is_ava.length; i++) {        var obj = BE32DA_ary_is_ava[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BE32DA_get_N01_t_file_info$file_type_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_file_info$file_type_id) == false)        return false;    BE32DA_ary_file_type_id = input.N01_t_file_info$file_type_id;    if($("#BE32DA_file_type_id").is("select") && $("#BE32DA_file_type_id")[0].options.length == 0)    {        $.each(BE32DA_ary_file_type_id, function (i, obj) {            addOptionValue("BE32DA_file_type_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#BE32DA_is_ava").is("select") && $("#BE32DA_is_ava")[0].options.length == 0)    {        $.each(BE32DA_ary_is_ava, function (i, obj) {            addOptionValue("BE32DA_is_ava", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    BE32DA_init_t_file_info();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function BE32DA_page_end(){
	if(BE32DA_param["type"] == "edit"){
		BE32DA_get_edit_info();
	}
}

//页面初始化方法
function BE32DA_init_t_file_info() {
	//type = getUrlParam("type");
	if(BE32DA_param["type"] == "add"){
		$("#BE32DA_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BE32DA_file_name").val(BE32DA_param["file_name"]);        $("#BE32DA_file_type_id").val(BE32DA_param["file_type_id"]);		
		/*Get Find Select param end*/	
	}
	else if(BE32DA_param["type"] == "edit"){
	
	}
	
	//表单验证
	BE32DA_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BE32DA_create_date").val() == "")    {        $("#BE32DA_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BE32DA_create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BE32DA_page_end();
}

//提交表单数据
function BE32DA_SubmitForm(){
	if(BE32DA_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_file_info",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#BE32DA_file_type_id").val()                ,"param_value2": s_encode($("#BE32DA_file_name").val())                ,"param_value3": s_encode($("#BE32DA_file_url").val())                ,"param_value4": $("#BE32DA_is_ava").val()                ,"param_value5": $("#BE32DA_create_date").val()                ,"param_value6": s_encode($("#BE32DA_s_desc").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BE32DA_get_N01_ins_t_file_info");
	}
	else if(BE32DA_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_file_info",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#BE32DA_file_type_id").val()                ,"param_value2": s_encode($("#BE32DA_file_name").val())                ,"param_value3": s_encode($("#BE32DA_file_url").val())                ,"param_value4": $("#BE32DA_is_ava").val()                ,"param_value5": $("#BE32DA_create_date").val()                ,"param_value6": s_encode($("#BE32DA_s_desc").val())                ,"param_value7": $("#BE32DA_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BE32DA_get_N01_upd_t_file_info");
	}
}

//vue回调
function BE32DA_t_file_info_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#BE32DA_save_t_file_info_Edit").click(function () {
	$("form[name='BE32DA_DataModal']").submit();
})

/*修改数据*/
function BE32DA_get_N01_upd_t_file_info(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_file_info) == true)
	{
		swal("修改数据成功!", "", "success");
		AE32DA_t_file_info_query();
		BE32DA_clear_validate();
		layer.close(BE32DA_param["ly_index"]);
	}
}

/*添加数据*/
function BE32DA_get_N01_ins_t_file_info(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_file_info) == true)
	{
		swal("添加数据成功!", "", "success");
		AE32DA_t_file_info_query();
		BE32DA_clear_validate();
		layer.close(BE32DA_param["ly_index"]);
	}
}

//取消编辑
$("#BE32DA_cancel_t_file_info_Edit").click(function () {
	layer.close(BE32DA_param["ly_index"]);
	BE32DA_clear_validate();
	$("[id^='BE32DA_div']").hide();
})

//清除查找框
function BE32DA_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BE32DA_clear_validate(){
	$("#BE32DA_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BE32DA_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BE32DA_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BE32DA_clear_edit_info(){
	var inputs = $("#BE32DA_DataModal").find('input');
	var selects = $("#BE32DA_DataModal").find("select");
	var textareas = $("#BE32DA_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	BE32DA_init_t_file_info();
}

//页面输入框赋值
function BE32DA_get_edit_info(){
	var rowData = $("#AE32DA_t_file_info_Events").bootstrapTable('getData')[AE32DA_select_t_file_info_rowId];
	var inputs = $("#BE32DA_DataModal").find('input');
	var selects = $("#BE32DA_DataModal").find("select");
	var textareas = $("#BE32DA_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"AE32DA","BE32DA");
	
	/*
	$.each(selects, function (i, obj) {
		if(typeof(eval("AE32DA_ary_"+obj.id.toString().replace("BE32DA_",""))) == "object")
		{
			$.each(eval("AE32DA_ary_"+obj.id.toString().replace("BE32DA_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE32DA_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("BE32DA_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("AE32DA_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#BE32DA_DataModal").find('[id="'+"BE32DA_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE32DA_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE32DA_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});*/
}

//form验证
function BE32DA_checkFormInput() {
    BE32DA_validate = $("#BE32DA_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BE32DA_main_id: {}            ,BE32DA_file_type_id: {}            ,BE32DA_file_name: {}            ,BE32DA_file_url: {}            ,BE32DA_is_ava: {}            ,BE32DA_create_date: {date: true,required : true,maxlength:19}            ,BE32DA_s_desc: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BE32DA_main_id: {}            ,BE32DA_file_type_id: {}            ,BE32DA_file_name: {}            ,BE32DA_file_url: {}            ,BE32DA_is_ava: {}            ,BE32DA_create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BE32DA_s_desc: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BE32DA_SubmitForm();
        	return false;
        }
    })
}