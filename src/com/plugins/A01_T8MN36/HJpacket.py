#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import json
from builtins import str
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_IHGDW0 import A01_Para
from com.plugins.A01_IHGDW0.A01_Fun import A01_Fun
from com.zxy.z_debug import z_debug
class HJpacket(z_debug):
    attBeg = "##"    
    attQN = ""
    attST = A01_Fun.Get_ST()
    attCN = ""
    attPW = Com_Fun.GetHashTable(A01_Para.ht_t_param_value,"UP_PW")
    attMN = Com_Fun.GetHashTable(A01_Para.ht_t_param_value,"UP_MN")
    attFlag = ""
    attPNUM = ""
    attPNO = ""
    attCP = ""
    attLength = "0"
    attCRC = ""
    attEnd = "\r\n"
    def __init__(self):
        self.attPW = Com_Fun.GetHashTable(A01_Para.ht_t_param_value,"UP_PW")
        self.attMN = Com_Fun.GetHashTable(A01_Para.ht_t_param_value,"UP_MN")
        self.attST = A01_Fun.Get_ST()
        pass
    def clear_value(self):
        self.attBeg = ""
        self.attQN = ""
        self.attST = ""
        self.attCN = ""
        self.attFlag = ""
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = ""
        self.attCRC = ""
    def init_value(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attCN = ""
        self.attFlag = ""
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_1_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_1_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_2_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_2_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "1011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_2_4(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_3_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_3_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_4_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9013"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_5_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_5_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "1061"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_5_4(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_6_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_6_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_7_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_7_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "1063"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_7_4(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_8_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_8_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_9_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_9_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_10_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_10_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_11_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9013"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_12_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_12_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_13_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9013"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_14_1(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "2011"
        self.attFlag = "5"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_15_1(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "2021"
        self.attFlag = "5"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_16_1(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "2051"
        self.attFlag = "1"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_17_1(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "2061"
        self.attFlag = "1"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_18_1(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "2031"
        self.attFlag = "5"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_19_1(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "2041"
        self.attFlag = "5"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_20_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_20_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "2051"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_20_4(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_21_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""  
    def init_C_21_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "2061"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_21_4(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_22_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = "" 
    def init_C_22_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "2031"
        self.attFlag = "5"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = "" 
    def init_C_22_4(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_23_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""    
    def init_C_23_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "2041"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = "" 
    def init_C_23_4(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_24_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9042"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_29_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9014"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_30_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_30_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_31_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_31_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_32_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_32_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_33_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_33_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_34_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_34_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "3015"
        self.attFlag = "5"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_34_4(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_35_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = "" 
    def init_C_35_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_36_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_36_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "3017"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_36_4(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_37_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_37_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "3018"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_37_4(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_38_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_38_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "3019"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_38_4(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_39_1(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "3019"
        self.attFlag = "5"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_40_1(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "3020"
        self.attFlag = "5"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_41_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_41_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "3020"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_41_4(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_42_1(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "3020"
        self.attFlag = "5"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_43_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_43_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "3020"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_43_4(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""  
    def init_C_44_1(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "3020"
        self.attFlag = "5"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_45_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = ""
    def init_C_45_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = A01_Fun.Get_ST()
        self.attCN = "3020"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_C_45_4(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = ""  
    def init_C_46_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9011"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "QnRtn=1"
        self.attLength = "0"
        self.attCRC = "" 
    def init_C_46_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9012"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = "ExeRtn=1"
        self.attLength = "0"
        self.attCRC = "" 
    def MakeC14(self):
        temmonitor_flag_model = A01_Fun.Get_monitor_flag_real()
        temmonitor_data_model = A01_Fun.Get_monitor_data_real()    
        if temmonitor_data_model is not None and temmonitor_flag_model is not None:
            self.attCP = "DataTime="+Com_Fun.GetDateInput("%Y%m%d%H%M%S","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)+";"
            for tcount in A01_Para.ht_t_code_element:
                self.attCP = self.attCP + tcount.attelement_code2+"-Rtd="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                self.attCP = self.attCP + tcount.attelement_code2+"-Flag="+str(getattr(temmonitor_flag_model, tcount.attelement_val.lower()))+";"
            temSocketValue = self.GetSendPack()
            return temSocketValue
        else:
            return ""
    def MakeC14_SEC(self):
        temmonitor_flag_model = A01_Fun.Get_monitor_flag_real()
        temmonitor_data_model = A01_Fun.Get_monitor_data_real()
        if temmonitor_data_model is not None and temmonitor_flag_model is not None and temmonitor_data_model != "":
            self.attCP = "DataTime="+Com_Fun.GetDateInput("%Y%m%d%H%M%S","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)+";"
            for tcount in A01_Para.ht_t_code_element:
                self.attCP = self.attCP + tcount.attelement_code2+"-Rtd="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                self.attCP = self.attCP + tcount.attelement_code2+"-Flag="+str(getattr(temmonitor_flag_model, tcount.attelement_val.lower()))+";"
            return self.attCP
        else:
            return ""
    def MakeC16_SEC(self,temmonitor_data_model):        
        temmonitor_flag_model = A01_Fun.Get_monitor_flag_real()
        if temmonitor_data_model is not None and temmonitor_flag_model is not None:
            self.attCP = "DataTime="+Com_Fun.GetDateInput("%Y%m%d%H%M%S","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)+";"
            for tcount in A01_Para.ht_t_code_element:
                self.attCP = self.attCP + tcount.attelement_code2+"-Rtd="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                self.attCP = self.attCP + tcount.attelement_code2+"-Flag="+str(getattr(temmonitor_flag_model, tcount.attelement_val.lower()))+","
                self.attCP = self.attCP + tcount.attelement_code2+"-Min="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")[0])+","
                self.attCP = self.attCP + tcount.attelement_code2+"-Avg="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                self.attCP = self.attCP + tcount.attelement_code2+"-Max="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")[2])+","
                if self.attST == "31":
                    self.attCP = self.attCP + tcount.attelement_code2+"-ZsRtd="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")[3])+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-ZsMin="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")[4])+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-ZsAvg="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")[5])+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-ZsMax="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")[6])+","
                self.attCP = self.attCP + tcount.attelement_code2+"-Cou="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")[9])
                self.attCP = self.attCP +";"
            return self.attCP
        else:
            return ""
    def MakeC17_SEC(self,temmonitor_data_model,temmonitor_flag_model,temmonitor_max_model): 
        temmonitor_flag_model = A01_Fun.Get_monitor_flag_real()
        if temmonitor_data_model is not None:
            self.attCP = "DataTime="+Com_Fun.GetDateInput("%Y%m%d%H%M%S","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)+";"
            for tcount in A01_Para.ht_t_code_element:
                self.attCP = self.attCP + tcount.attelement_code2+"-Rtd="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                if temmonitor_flag_model is not None:
                    self.attCP = self.attCP + tcount.attelement_code2+"-Flag="+str(getattr(temmonitor_flag_model, tcount.attelement_val.lower()))+","
                else:
                    self.attCP = self.attCP + tcount.attelement_code2+"-Flag=N,"
                if temmonitor_max_model is not None:
                    self.attCP = self.attCP + tcount.attelement_code2+"-Min="+str(json.loads(getattr(temmonitor_max_model, tcount.attelement_val.lower()))[0])+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Avg="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Max="+str(json.loads(getattr(temmonitor_max_model, tcount.attelement_val.lower()))[2])+","
                    if self.attST == "31":
                        self.attCP = self.attCP + tcount.attelement_code2+"-ZsRtd="+str(json.loads(getattr(temmonitor_max_model, tcount.attelement_val.lower()))[3])+","
                        self.attCP = self.attCP + tcount.attelement_code2+"-ZsMin="+str(json.loads(getattr(temmonitor_max_model, tcount.attelement_val.lower()))[4])+","
                        self.attCP = self.attCP + tcount.attelement_code2+"-ZsAvg="+str(json.loads(getattr(temmonitor_max_model, tcount.attelement_val.lower()))[5])+","
                        self.attCP = self.attCP + tcount.attelement_code2+"-ZsMax="+str(json.loads(getattr(temmonitor_max_model, tcount.attelement_val.lower()))[6])+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Cou="+str(json.loads(getattr(temmonitor_max_model, tcount.attelement_val.lower()))[9])                    
                    self.attCP = self.attCP +";"
                else:    
                    self.attCP = self.attCP + tcount.attelement_code2+"-Min="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Avg="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Max="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    if self.attST == "31":
                        self.attCP = self.attCP + tcount.attelement_code2+"-ZsRtd="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                        self.attCP = self.attCP + tcount.attelement_code2+"-ZsMin="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                        self.attCP = self.attCP + tcount.attelement_code2+"-ZsAvg="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                        self.attCP = self.attCP + tcount.attelement_code2+"-ZsMax="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Cou="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))
                    self.attCP = self.attCP +";"
            return self.attCP
        else:
            return ""
    def MakeC17(self,temmonitor_data_model,temmonitor_flag_model,temmonitor_max_model): 
        temmonitor_flag_model = A01_Fun.Get_monitor_flag_real()
        if temmonitor_data_model is not None:
            self.attCP = "DataTime="+Com_Fun.GetDateInput("%Y%m%d%H%M%S","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)+";"
            for tcount in A01_Para.ht_t_code_element:
                self.attCP = self.attCP + tcount.attelement_code2+"-Rtd="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                if temmonitor_flag_model is not None:
                    self.attCP = self.attCP + tcount.attelement_code2+"-Flag="+str(getattr(temmonitor_flag_model, tcount.attelement_val.lower()))+","
                else:
                    self.attCP = self.attCP + tcount.attelement_code2+"-Flag=N,"
                if temmonitor_max_model is not None:
                    self.attCP = self.attCP + tcount.attelement_code2+"-Min="+str(json.loads(getattr(temmonitor_max_model, tcount.attelement_val.lower()))[0])+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Avg="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Max="+str(json.loads(getattr(temmonitor_max_model, tcount.attelement_val.lower()))[2])+","
                    if self.attST == "31":
                        self.attCP = self.attCP + tcount.attelement_code2+"-ZsRtd="+str(json.loads(getattr(temmonitor_max_model, tcount.attelement_val.lower()))[3])+","
                        self.attCP = self.attCP + tcount.attelement_code2+"-ZsMin="+str(json.loads(getattr(temmonitor_max_model, tcount.attelement_val.lower()))[4])+","
                        self.attCP = self.attCP + tcount.attelement_code2+"-ZsAvg="+str(json.loads(getattr(temmonitor_max_model, tcount.attelement_val.lower()))[5])+","
                        self.attCP = self.attCP + tcount.attelement_code2+"-ZsMax="+str(json.loads(getattr(temmonitor_max_model, tcount.attelement_val.lower()))[6])+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Cou="+str(json.loads(getattr(temmonitor_max_model, tcount.attelement_val.lower()))[9])                    
                    self.attCP = self.attCP +";"
                else:    
                    self.attCP = self.attCP + tcount.attelement_code2+"-Min="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Avg="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Max="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    if self.attST == "31":
                        self.attCP = self.attCP + tcount.attelement_code2+"-ZsRtd="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                        self.attCP = self.attCP + tcount.attelement_code2+"-ZsMin="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                        self.attCP = self.attCP + tcount.attelement_code2+"-ZsAvg="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                        self.attCP = self.attCP + tcount.attelement_code2+"-ZsMax="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Cou="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))
                    self.attCP = self.attCP +";"
            temSocketValue = self.GetSendPack()
            return temSocketValue
        else:
            return ""
    def MakeC18_SEC(self,temmonitor_data_model,temmonitor_flag_model,temmonitor_max_model): 
        if temmonitor_data_model is not None:
            self.attCP = "DataTime="+Com_Fun.GetDateInput("%Y%m%d%H%M%S","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)+";"
            for tcount in A01_Para.ht_t_code_element:
                if temmonitor_flag_model is not None:
                    self.attCP = self.attCP + tcount.attelement_code2+"-Flag="+str(getattr(temmonitor_flag_model, tcount.attelement_val.lower()))+","
                else:
                    self.attCP = self.attCP + tcount.attelement_code2+"-Flag=N,"
                if temmonitor_max_model is not None:
                    self.attCP = self.attCP + tcount.attelement_code2+"-Min="+str(json.loads(str(getattr(temmonitor_max_model, tcount.attelement_val.lower()+"_ary")))[0])+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Avg="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Max="+str(json.loads(str(getattr(temmonitor_max_model, tcount.attelement_val.lower()+"_ary")))[2])+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Cou="+str(json.loads(str(getattr(temmonitor_max_model, tcount.attelement_val.lower()+"_ary")))[9])                    
                    self.attCP = self.attCP +";"
                else:    
                    self.attCP = self.attCP + tcount.attelement_code2+"-Min="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Avg="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Max="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Cou="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))
                    self.attCP = self.attCP +";"
            return self.attCP
        else:
            return ""
    def MakeC18(self,temmonitor_data_model,temmonitor_flag_model,temmonitor_max_model):
        if temmonitor_data_model is not None:
            self.attCP = "DataTime="+Com_Fun.GetDateInput("%Y%m%d%H%M%S","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)+";"
            for tcount in A01_Para.ht_t_code_element:
                if temmonitor_flag_model is not None:
                    self.attCP = self.attCP + tcount.attelement_code2+"-Flag="+str(getattr(temmonitor_flag_model, tcount.attelement_val.lower()))+","
                else:
                    self.attCP = self.attCP + tcount.attelement_code2+"-Flag=N,"
                if temmonitor_max_model is not None:
                    self.attCP = self.attCP + tcount.attelement_code2+"-Min="+str(json.loads(str(getattr(temmonitor_max_model, tcount.attelement_val.lower()+"_ary")))[0])+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Avg="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Max="+str(json.loads(str(getattr(temmonitor_max_model, tcount.attelement_val.lower()+"_ary")))[2])+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Cou="+str(json.loads(str(getattr(temmonitor_max_model, tcount.attelement_val.lower()+"_ary")))[9])                    
                    self.attCP = self.attCP +";"
                else:    
                    self.attCP = self.attCP + tcount.attelement_code2+"-Min="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Avg="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Max="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-Cou="+str(getattr(temmonitor_max_model, tcount.attelement_val.lower()))
                    self.attCP = self.attCP +";"
            temSocketValue = self.GetSendPack()
            return temSocketValue
        else:
            return ""
    def MakeC16(self,temmonitor_data_model):        
        temmonitor_flag_model = A01_Fun.Get_monitor_flag_real()
        if temmonitor_data_model is not None and temmonitor_flag_model is not None:
            self.attCP = "DataTime="+Com_Fun.GetDateInput("%Y%m%d%H%M%S","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)+";"
            for tcount in A01_Para.ht_t_code_element:
                self.attCP = self.attCP + tcount.attelement_code2+"-Rtd="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                self.attCP = self.attCP + tcount.attelement_code2+"-Flag="+str(getattr(temmonitor_flag_model, tcount.attelement_val.lower()))+","
                self.attCP = self.attCP + tcount.attelement_code2+"-Min="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")[0])+","
                self.attCP = self.attCP + tcount.attelement_code2+"-Avg="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()))+","
                self.attCP = self.attCP + tcount.attelement_code2+"-Max="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")[2])+","
                if self.attST == "31":
                    self.attCP = self.attCP + tcount.attelement_code2+"-ZsRtd="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")[3])+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-ZsMin="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")[4])+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-ZsAvg="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")[5])+","
                    self.attCP = self.attCP + tcount.attelement_code2+"-ZsMax="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")[6])+","
                self.attCP = self.attCP + tcount.attelement_code2+"-Cou="+str(getattr(temmonitor_data_model, tcount.attelement_val.lower()+"_ary")[9])
                self.attCP = self.attCP +";"
            temSocketValue = self.GetSendPack()
            return temSocketValue
        else:
            return ""
    def GetSendPack_CP(self):
        if self.attQN == "":
            self.attQN = Com_Fun.GetTime("%Y%m%d%H%M%S%f")[:-3]
        temPacket = "QN="+self.attQN+";ST="+self.attST+";CN="+self.attCN+";PW="+self.attPW
        temPacket = temPacket +";MN="+self.attMN+";Flag="+self.attFlag+";CP=&&"+self.attCP+"&&" 
        temL = len(temPacket.encode(Com_Para.U_CODE))
        strL = str(temL)
        if len(strL) == 1 :
            self.attLength = "000"+strL
        elif len(strL) == 2:
            self.attLength = "00"+strL
        elif len(strL) == 3:
            self.attLength = "0"+strL
        elif len(strL) == 4:
            self.attLength = strL        
        self.attCRC = self.CheckCRC16Z_CP(temPacket).upper()
        return self.attBeg+self.attLength+temPacket+self.attCRC+self.attEnd   
    def GetSendPack_SEC(self,inputSecCP):
        if self.attQN == "":
            self.attQN = Com_Fun.GetTime("%Y%m%d%H%M%S%f")[:-3]
        temPacket = "QN="+self.attQN+";ST="+self.attST+";CN="+self.attCN+";PW="+self.attPW
        temPacket = temPacket +";MN="+self.attMN+";Flag="+self.attFlag+";CP=&&"+self.attCP+"&&"
        temPacketSec = "QN="+self.attQN+";ST="+self.attST+";CN="+self.attCN+";PW="+self.attPW
        temPacketSec = temPacketSec +";MN="+self.attMN+";Flag="+self.attFlag+";CP=&&"+inputSecCP+"&&" 
        temL = len(temPacket.encode(Com_Para.U_CODE))
        strL = str(temL)
        if len(strL) == 1 :
            self.attLength = "000"+strL
        elif len(strL) == 2:
            self.attLength = "00"+strL
        elif len(strL) == 3:
            self.attLength = "0"+strL
        elif len(strL) == 4:
            self.attLength = strL        
        self.attCRC = self.CheckCRC16Z_CP(temPacket).upper()
        return self.attBeg+self.attLength+temPacketSec+self.attCRC+self.attEnd 
    def GetSendPack(self):        
        if self.attQN == "":
            self.attQN = Com_Fun.GetTime("%Y%m%d%H%M%S%f")[:-3]
        temPacket = "QN="+self.attQN+";ST="+self.attST+";CN="+self.attCN+";PW="+self.attPW
        temPacket = temPacket +";MN="+self.attMN+";Flag="+self.attFlag+";CP=&&"+self.attCP+"&&"        
        temL = len(temPacket.encode(Com_Para.U_CODE))
        strL = str(temL)
        if len(strL) == 1 :
            self.attLength = "000"+strL
        elif len(strL) == 2:
            self.attLength = "00"+strL
        elif len(strL) == 3:
            self.attLength = "0"+strL
        elif len(strL) == 4:
            self.attLength = strL        
        self.attCRC = self.CheckCRC16Z(temPacket).upper()
        return self.attBeg+self.attLength+temPacket+self.attCRC+self.attEnd
    def GetHeartLoop(self):
        self.init_value()
        self.attQN = Com_Fun.GetTime("%Y%m%d%H%M%S%f")[:-3]
        temPacket = "QN="+self.attQN+";ST="+self.attST+";CN="+self.attCN+";PW="+self.attPW
        temPacket = temPacket +";MN="+self.attMN+";Flag="+self.attFlag+";CP=&&"+self.attCP+"&&"        
        temL = len(temPacket)
        strL = str(temL)
        if len(strL) == 1 :
            self.attLength = "000"+strL
        elif len(strL) == 2:
            self.attLength = "00"+strL
        elif len(strL) == 3:
            self.attLength = "0"+strL
        elif len(strL) == 4:
            self.attLength = strL        
        self.attCRC = self.CheckCRC16Z(temPacket).upper()
        return self.attBeg+self.attLength+temPacket+self.attCRC+self.attEnd
    def CalCRC16(self):
        temPacket = "QN="+self.attQN+";ST="+self.attST+";CN="+self.attCN+";PW="+self.attPW
        temPacket = temPacket +";MN="+self.attMN+";Flag="+self.attFlag+";CP=&&"+self.attCP+"&&"
        return self.CheckCRC16Z(temPacket).upper()
    def CheckCRC16Z_CP(self,inputData):
        usDataLen = b''
        usDataLen = bytes(inputData, encoding=Com_Para.U_CODE)
        crc_reg = 0xFFFF
        for i in usDataLen:
            crc_reg = ( crc_reg >> 8) ^ i
            for j in range(8):
                check = crc_reg & 0x0001
                crc_reg >>= 1
                if check == 0x0001 :
                    crc_reg ^= 0xA001
        temV = hex(crc_reg)[2:6]
        if len(temV) == 3:
            temV = "0"+temV 
        elif len(temV) == 2:
            temV = "00"+temV
        elif len(temV) == 1:
            temV = "000"+temV
        return temV
    def CheckCRC16Z(self,inputData):
        usDataLen = b''
        usDataLen = bytes(inputData, encoding=Com_Para.U_CODE)
        crc_reg = 0xFFFF
        for i in usDataLen:
            crc_reg = ( crc_reg >> 8) ^ i
            for j in range(8):
                check = crc_reg & 0x0001
                crc_reg >>= 1
                if check == 0x0001 :
                    crc_reg ^= 0xA001
        temV = hex(crc_reg)[2:6]
        if len(temV) == 3:
            temV = "0"+temV 
        elif len(temV) == 2:
            temV = "00"+temV
        elif len(temV) == 1:
            temV = "000"+temV
        return temV
    def CheckCRC16(self,inputData):
        temInit_msg = b''
        temInit_msg = bytes(inputData, encoding=Com_Para.U_CODE)
        crc_reg = 0xFFFF
        for temBt in temInit_msg:
            crc_reg = (crc_reg>>8) ^ temBt
            for i in range(8):
                check = crc_reg & 0x0001
                crc_reg >>= 1
                if check == 0x0001:
                    crc_reg ^= 0xA001
        temV = hex(crc_reg)[2:6]
        if len(temV) == 3:
            temV = "0"+temV 
        elif len(temV) == 2:
            temV = "00"+temV
        elif len(temV) == 1:
            temV = "000"+temV
        return temV