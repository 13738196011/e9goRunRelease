//主从表tab选项卡标志位
var A170C7_Tab_Flag = -1;
//选择某一行
var A170C7_select_t_proc_name_rowId = "";
//按钮事件新增或编辑
var A170C7_type = "";
//其他页面传到本页面参数
var A170C7_param = {};
//table选中数据，如无则默认第一条
var A170C7_rowCheckData = null;

/*定义查询条件变量*/
/*declare query param begin*/
var A170C7_tem_INF_CN_NAME = "";var A170C7_tem_INF_EN_NAME = "";var A170C7_tem_IS_AUTHORITY = "0";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var A170C7_ary_DB_ID = null;var A170C7_ary_INF_TYPE = [{'MAIN_ID': '1', 'CN_NAME': '存储过程'}, {'MAIN_ID': '2', 'CN_NAME': 'SQL语句'}];var A170C7_ary_IS_AUTHORITY = [{'MAIN_ID': '1', 'CN_NAME': '是'}, {'MAIN_ID': '0', 'CN_NAME': '否'}];
/*declare select options end*/

/*绑定show监听事件*/
if(A170C7_Tab_Flag == "-1"){
	var n = Get_RandomDiv("A170C7","");
	$(n).bind("show", function(objTag){
		A170C7_Adjust_Sub_Sequ();
		if(A170C7_Tab_Flag > 0)// && A170C7_Is_Sub_Div($(n)[0].id))
			A170C7_adjust_tab();
	});
}

//设置主从表div顺序
function A170C7_Adjust_Sub_Sequ(){
	var temSubDivs = $("#content-main").find("div[data-id]");
	var MainDiv = $(Get_RandomDiv("A170C7",""));
	for(i = 0; i < temSubDivs.length; i++) {
		if(A170C7_Is_Sub_Div(temSubDivs[i].id)){
			$(temSubDivs[i]).before($(MainDiv));
			break;
		}
	}
}
	
//主从表传递参数
function A170C7_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function A170C7_t_proc_name_biz_start(inputparam) {
	layer.close(ly_index);
	A170C7_param = inputparam;
	//主从表传递参数
	A170C7_param_set();	
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_proc_name$DB_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A170C7_get_N01_t_proc_name$DB_ID");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function A170C7_format_DB_ID(value, row, index) {    var objResult = value;    for(i = 0; i < A170C7_ary_DB_ID.length; i++) {        var obj = A170C7_ary_DB_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A170C7_format_INF_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < A170C7_ary_INF_TYPE.length; i++) {        var obj = A170C7_ary_INF_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A170C7_format_IS_AUTHORITY(value, row, index) {    var objResult = value;    for(i = 0; i < A170C7_ary_IS_AUTHORITY.length; i++) {        var obj = A170C7_ary_IS_AUTHORITY[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A170C7_get_N01_t_proc_name$DB_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_name$DB_ID) == false)        return false;    A170C7_ary_DB_ID = input.N01_t_proc_name$DB_ID;    $("#A170C7_qry_IS_AUTHORITY").append("<option value='-1'></option>")    $.each(A170C7_ary_IS_AUTHORITY, function (i, obj) {        addOptionValue("A170C7_qry_IS_AUTHORITY", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    A170C7_init_t_proc_name();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function A170C7_page_end(){
	A170C7_adjust_tab();
}

//接口名称2显示列定义
var A170C7_t_proc_name = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '数据源',        field: 'DB_ID',        sortable: true        ,formatter: A170C7_format_DB_ID    },    {        title: '接口中文名',        field: 'INF_CN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '接口英文名称',        field: 'INF_EN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: 'SQL语句或存储过程',        field: 'INF_EN_SQL',        sortable: true        ,formatter: set_s_decode    },    {        title: '类型',        field: 'INF_TYPE',        sortable: true        ,formatter: A170C7_format_INF_TYPE    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '输入参数拦截器类名',        field: 'REFLECT_IN_CLASS',        sortable: true        ,formatter: set_s_decode    },    {        title: '输出参数拦截器类名',        field: 'REFLECT_OUT_CLASS',        sortable: true        ,formatter: set_s_decode    },    {        title: '是否权限认证',        field: 'IS_AUTHORITY',        sortable: true        ,formatter: A170C7_format_IS_AUTHORITY    }	
	/*table column end*/
];

//页面初始化
function A170C7_init_t_proc_name() {
	$(window).resize(function () {
		  $('#A170C7_t_proc_name_Events').bootstrapTable('resetView');
	});
	//接口名称2查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#A170C7_btn_t_proc_name_query').click();
}

//查询接口
function A170C7_t_proc_name_query() {
    $('#A170C7_t_proc_name_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_proc_name",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(A170C7_tem_INF_CN_NAME)        ,"param_value2": s_encode(A170C7_tem_INF_EN_NAME)        ,"param_value3": A170C7_tem_IS_AUTHORITY        ,"param_value4": A170C7_tem_IS_AUTHORITY		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "A170C7_get_N01_sel_t_proc_name");
}

//查询结果
function A170C7_get_N01_sel_t_proc_name(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_proc_name) == false)
        return false;
    A170C7_rowCheckData = null;
    //调整table各列宽度
    $.each(A170C7_t_proc_name, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_t_proc_name;
    if(s_data.length > 0)
    	A170C7_rowCheckData = s_data[0];    
    A170C7_select_t_proc_name_rowId = "";
	A170C7_Tab_Flag = 0;
    $('#A170C7_t_proc_name_Events').bootstrapTable('destroy');
    $("#A170C7_t_proc_name_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#A170C7_t_proc_name_Events').find("tr.changeColor").removeClass('changeColor');
                A170C7_select_t_proc_name_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#A170C7_t_proc_name_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                A170C7_select_t_proc_name_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#A170C7_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(A170C7_param.hasOwnProperty("target_name"))
			{
				$("#"+A170C7_param["target_id"]).val(eval("row."+A170C7_param["sourc_id"].toUpperCase()));
				$("#"+A170C7_param["target_name"]).val(s_decode(eval("row."+A170C7_param["sourc_name"].toUpperCase())));				
				layer.close(A170C7_param["ly_index"]);
			}
		},
        toolbar: "#A170C7_t_proc_name_Toolbar",
        columns: A170C7_t_proc_name,
        data: s_data,
        pageNumber: 1,
        pageSize: 5, // 每页的记录行数（*）
        pageList: [5,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        onPostBody:function(){
        	if(s_data.length != 0)
            	A170C7_page_end();
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#A170C7_btn_t_proc_name_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    A170C7_tem_INF_CN_NAME = "";    A170C7_tem_INF_EN_NAME = "";    A170C7_tem_IS_AUTHORITY = "0";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    B170C7_ary_DB_ID = null;
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_proc_name$DB_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A170C7_get_N01_t_proc_name$DB_ID");	
    /*biz end*/
})

//查询按钮
$('#A170C7_btn_t_proc_name_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    A170C7_tem_INF_CN_NAME = $("#A170C7_qry_INF_CN_NAME").val();    A170C7_tem_INF_EN_NAME = $("#A170C7_qry_INF_EN_NAME").val();    A170C7_tem_IS_AUTHORITY = $("#A170C7_qry_IS_AUTHORITY").val();	
    /*set query param end*/
	A170C7_t_proc_name_query();
})

//vue回调
function A170C7_t_proc_name_call_vue(objResult){
	//选择某条记录或自动选择第一条
	if (A170C7_select_t_proc_name_rowId != "") 
		A170C7_rowCheckData = $("#A170C7_t_proc_name_Events").bootstrapTable('getData')[A170C7_select_t_proc_name_rowId];
	
	if(index_subhtml == "t_proc_name_$.vue")
	{
		var n = Get_RandomDiv("B170C7",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":A170C7_type,"ly_index":index};
				if(A170C7_param.hasOwnProperty("hidden_find")){
					inputdata["hidden_find"] = "1";
				}
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temINF_CN_NAME = $("#A170C7_qry_INF_CN_NAME").val();                if(temINF_CN_NAME != ""){                    inputdata["INF_CN_NAME"] = temINF_CN_NAME;                }                var temINF_EN_NAME = $("#A170C7_qry_INF_EN_NAME").val();                if(temINF_EN_NAME != ""){                    inputdata["INF_EN_NAME"] = temINF_EN_NAME;                }                var temIS_AUTHORITY = $("#A170C7_qry_IS_AUTHORITY").val();                if(temIS_AUTHORITY != ""){                    inputdata["IS_AUTHORITY"] = temIS_AUTHORITY;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_proc_name_$.js","B170C7_t_proc_name_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/
    else if(index_subhtml == "t_proc_inparam.vue"){        var n = Get_RandomDiv("A1F0D4",objResult);        $(n).show();        //传递参数        var inputdata = {"hidden_find":"1"};        if(A170C7_rowCheckData != null){            inputdata["PROC_ID_cn_name"] = A170C7_rowCheckData[GetLowUpp("INF_CN_NAME")];            inputdata["PROC_ID"] = A170C7_rowCheckData[GetLowUpp("MAIN_ID")];        }        else{            inputdata["PROC_ID_cn_name"] = "";            inputdata["PROC_ID"] = "-999";        }        loadScript_hasparam("biz_vue/t_proc_inparam.js","A1F0D4_t_proc_inparam_biz_start",inputdata);    }    else if(index_subhtml == "t_proc_return.vue"){        var n = Get_RandomDiv("A20D9D",objResult);        $(n).show();        //传递参数        var inputdata = {"hidden_find":"1"};        if(A170C7_rowCheckData != null){            inputdata["PROC_ID_cn_name"] = A170C7_rowCheckData[GetLowUpp("INF_CN_NAME")];            inputdata["PROC_ID"] = A170C7_rowCheckData[GetLowUpp("MAIN_ID")];        }        else{            inputdata["PROC_ID_cn_name"] = "";            inputdata["PROC_ID"] = "-999";        }        loadScript_hasparam("biz_vue/t_proc_return.js","A20D9D_t_proc_return_biz_start",inputdata);    }    else if(index_subhtml == "t_sub_power.vue"){        var n = Get_RandomDiv("A24E7B",objResult);        $(n).show();        //传递参数        var inputdata = {"hidden_find":"1"};        if(A170C7_rowCheckData != null){            inputdata["PROC_ID_cn_name"] = A170C7_rowCheckData[GetLowUpp("INF_CN_NAME")];            inputdata["PROC_ID"] = A170C7_rowCheckData[GetLowUpp("MAIN_ID")];        }        else{            inputdata["PROC_ID_cn_name"] = "";            inputdata["PROC_ID"] = "-999";        }        loadScript_hasparam("biz_vue/t_sub_power.js","A24E7B_t_sub_power_biz_start",inputdata);    }    else if(index_subhtml == "t_sub_userpower.vue"){        var n = Get_RandomDiv("A29E3D",objResult);        $(n).show();        //传递参数        var inputdata = {"hidden_find":"1"};        if(A170C7_rowCheckData != null){            inputdata["PROC_ID_cn_name"] = A170C7_rowCheckData[GetLowUpp("INF_CN_NAME")];            inputdata["PROC_ID"] = A170C7_rowCheckData[GetLowUpp("MAIN_ID")];        }        else{            inputdata["PROC_ID_cn_name"] = "";            inputdata["PROC_ID"] = "-999";        }        loadScript_hasparam("biz_vue/t_sub_userpower.js","A29E3D_t_sub_userpower_biz_start",inputdata);    }
	/*get tab subvue end*/
}

//新增按钮
$("#A170C7_btn_t_proc_name_add").click(function () {
	A170C7_type = "add";
	index_subhtml = "t_proc_name_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_proc_name_$.vue","A170C7_t_proc_name_call_vue") == true){
		var n = Get_RandomDiv("B170C7","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				B170C7_param["type"] = A170C7_type;
				B170C7_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temINF_CN_NAME = $("#A170C7_qry_INF_CN_NAME").val();                if(temINF_CN_NAME != ""){                    B170C7_param["INF_CN_NAME"] = temINF_CN_NAME;                }                var temINF_EN_NAME = $("#A170C7_qry_INF_EN_NAME").val();                if(temINF_EN_NAME != ""){                    B170C7_param["INF_EN_NAME"] = temINF_EN_NAME;                }                var temIS_AUTHORITY = $("#A170C7_qry_IS_AUTHORITY").val();                if(temIS_AUTHORITY != ""){                    B170C7_param["IS_AUTHORITY"] = temIS_AUTHORITY;                }				
				/*Send Two FindSelect param end*/

				B170C7_clear_edit_info();
			},
			end: function(){
				B170C7_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#A170C7_btn_t_proc_name_edit").click(function () {
	if (A170C7_select_t_proc_name_rowId != "") {
		A170C7_type = "edit";
		index_subhtml = "t_proc_name_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_proc_name_$.vue","A170C7_t_proc_name_call_vue") == true){
			var n = Get_RandomDiv("B170C7","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					B170C7_param["type"] = A170C7_type;
					B170C7_param["ly_index"] = index;
					B170C7_clear_edit_info();
					B170C7_get_edit_info();
				},
				end: function(){
					B170C7_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#A170C7_btn_t_proc_name_delete').click(function () {
	//单行选择
	var rowData = $("#A170C7_t_proc_name_Events").bootstrapTable('getData')[A170C7_select_t_proc_name_rowId];
	//多行选择
	var rowDatas = A170C7_sel_row_t_proc_name();
	if (A170C7_select_t_proc_name_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_proc_name",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "A170C7_N01_del_t_proc_name");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_proc_name",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "A170C7_N01_del_t_proc_name");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function A170C7_N01_del_t_proc_name(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_proc_name) == true)
		A170C7_t_proc_name_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function A170C7_adjust_tab(){
	if(typeof($("#A170C7_TAB_MAIN")[0]) != "undefined" && $("#A170C7_TAB_MAIN")[0].length != 0){
		A170C7_Tab_Flag = 0;
		$(Get_RDivNoBuild("A170C7","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("A170C7","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("A170C7","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#A170C7_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/
$("#A170C7_tab_1").click(function(){    A170C7_Tab_Flag = 1;    A170C7_hide_tab_fun();    //主子表参数传递    var temPar = [{"sourc_id":"MAIN_ID","target_id":"PROC_ID"},{"sourc_id":"INF_CN_NAME","target_id":"PROC_ID_cn_name"}];    A170C7_show_tab_fun("t_proc_inparam.vue","A1F0D4",temPar);});$("#A170C7_tab_2").click(function(){    A170C7_Tab_Flag = 2;    A170C7_hide_tab_fun();    //主子表参数传递    var temPar = [{"sourc_id":"MAIN_ID","target_id":"PROC_ID"},{"sourc_id":"INF_CN_NAME","target_id":"PROC_ID_cn_name"}];    A170C7_show_tab_fun("t_proc_return.vue","A20D9D",temPar);});$("#A170C7_tab_3").click(function(){    A170C7_Tab_Flag = 3;    A170C7_hide_tab_fun();    //主子表参数传递    var temPar = [{"sourc_id":"MAIN_ID","target_id":"PROC_ID"},{"sourc_id":"INF_CN_NAME","target_id":"PROC_ID_cn_name"}];    A170C7_show_tab_fun("t_sub_power.vue","A24E7B",temPar);});$("#A170C7_tab_4").click(function(){    A170C7_Tab_Flag = 4;    A170C7_hide_tab_fun();    //主子表参数传递    var temPar = [{"sourc_id":"MAIN_ID","target_id":"PROC_ID"},{"sourc_id":"INF_CN_NAME","target_id":"PROC_ID_cn_name"}];    A170C7_show_tab_fun("t_sub_userpower.vue","A29E3D",temPar);});//隐藏tab页选项卡function A170C7_hide_tab_fun(){    var n = null;    n = Get_RDivNoBuild("A1F0D4","");    $(n).hide();    n = Get_RDivNoBuild("A20D9D","");    $(n).hide();    n = Get_RDivNoBuild("A24E7B","");    $(n).hide();    n = Get_RDivNoBuild("A29E3D","");    $(n).hide();}

//判断是否sub子项div页面"
function A170C7_Is_Sub_Div(temDivId){
	if(temDivId.indexOf("XX$TTT") == 0)
		return false;
	else if(temDivId.indexOf("A1F0D4") == 0)
		return true;
	else if(temDivId.indexOf("A20D9D") == 0)
		return true;
	else if(temDivId.indexOf("A24E7B") == 0)
		return true;
	else if(temDivId.indexOf("A29E3D") == 0)
		return true;
}

/*Tab Click Fun End*/

function A170C7_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("biz_vue/"+inputUrl,"A170C7_t_proc_name_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (A170C7_select_t_proc_name_rowId != "") 
			rowData = $("#A170C7_t_proc_name_Events").bootstrapTable('getData')[A170C7_select_t_proc_name_rowId];
		else
			rowData = A170C7_rowCheckData;//$("#A170C7_t_proc_name_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			var temFlag = true;
			$.each(temPar, function (i, obj) {
				if(eval(inputrandom+"_param.hasOwnProperty(\""+obj.target_id+"\")") &&  eval(inputrandom+"_param[\""+obj.target_id+"\"]").toString() == rowData[obj.sourc_id].toString())
					temFlag = false;
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			if(temFlag){
				//传递子页面隐藏find功能
				eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
				//参数传递并赋值
				eval(inputrandom+"_param_set()");
				var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
				$("#"+inputrandom+"_btn_"+tbName+"_query").click();
			}
		}
		else{
			$.each(temPar, function (i, obj) {
				if(obj.sourc_id.toUpperCase() == "MAIN_ID")
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"-999\"");
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"\"");
			});
			//传递子页面隐藏find功能
			eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
			var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
			$("#"+inputrandom+"_btn_"+tbName+"_query").click();		
		}
	}
}

//清除 查找框
function A170C7_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function A170C7_sel_row_t_proc_name(){
	//获得选中行
	var checkedbox= $("#A170C7_t_proc_name_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}