#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import time,json,struct,urllib.parse
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_7GA08B.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_7GA08B import A01_Para
from com.plugins.A01_7GA08B.A01_7GA08B_PLC import A01_7GA08B_PLC
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.common import Com_Para
from com.zxy.z_debug import z_debug
class A01_7GA08B_Time(z_debug):
    strContinue      = "1"   
    strResult        = ""
    def __init__(self):
        pass
    def init_start(self):
        bodc = Bus_Ope_DB_Cent()
        bodc.init_page()    
        temt_station_info = bodc.Get_t_station_info()
        temdbc2 = Db_Common2()     
        temA01PLC = A01_7GA08B_PLC()
        for tsi in temt_station_info:
            tbMonth = "monitor_data_min"+Com_Fun.GetTime("%Y%m")
            get_time = Com_Fun.GetTime("%Y-%m-%d %H:%M:00")
            update_time = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
            strSqlInsert = "insert into "
            strSqlInsert2 = "insert into monitor_data_real "
            strSqlUpdate = "update monitor_data_real "
            strSqlValues = "values "
            inputValues = {}
            strSqlInsert = strSqlInsert + tbMonth+"(station_code,get_time,update_time"
            strSqlInsert2 = strSqlInsert2+"(station_code,get_time,update_time"
            strSqlUpdate = strSqlUpdate+"set get_time='"+get_time+"',update_time='"+update_time+"'"
            strSqlValues = strSqlValues+"('"+tsi.attstation_code+"','"+get_time+"','"+Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")+"'"
            inputValues["param_value1"] = tsi.attstation_code
            inputValues["param_value2"] = Com_Fun.py_urlencode(get_time)
            temt_plc_addr = bodc.Get_t_plc_addr_by_station_id(str(tsi.attmain_id))
            all_value = ""
            m_flag = 0
            iIndex = 0
            for tmda in temt_plc_addr:
                iIndex = iIndex + 1
                red = temA01PLC.get_data_plc(0,1,tmda.attplc_dbnumber,tmda.attplc_Start,tmda.attplc_size)
                data_val = -1
                if red is not None:
                    data_val = struct.unpack('!B',red)[0]
                all_value = all_value + str(data_val)               
                strSqlInsert = strSqlInsert +","+tmda.attelement_val
                strSqlInsert2 = strSqlInsert2 +","+tmda.attelement_val
                strSqlValues = strSqlValues +",'"+str(data_val)+"'"
                strSqlUpdate = strSqlUpdate +","+tmda.attelement_val+"='"+str(data_val)+"'"
                inputValues["param_value"+str(iIndex+2)] = str(data_val)
                time.sleep(0.5)
            for ti in range(iIndex+3,23):
                inputValues["param_value"+str(ti)] = ""
            inputUrl = "http://" + Com_Para.ServerIP + ":"+ str(Com_Para.ServerWebPort)+ "/CenterData/getdata.jsp?a=1" + A01_Para.strCon
            m_result = Com_Fun.PostHttp(inputUrl, inputValues)
            if m_result == "-1" or m_result == "":
                m_flag = 0
            else:    
                try:            
                    temJso = json.loads(m_result)
                    temJsoary = temJso["C01_T7H5R4"]
                    for temobj in temJsoary:
                        temSResult = temobj["s_result"]
                        if temSResult == 1:
                            m_flag = 1
                except Exception as e:
                    m_flag = 0
                    if str(type(self)) == "<class 'type'>":
                        self.debug_in(self,repr(e)+"===>"+m_result)#打印异常信息
                    else:
                        self.debug_in(repr(e)+"===>"+m_result)#打印异常信息                      
            if len(all_value) > 0:
                if bodc.Get_monitor_data_real_true(tsi.attstation_code) == True:
                    temdbc2.CommonExec_Sql(strSqlUpdate+" where station_code ='"+tsi.attstation_code+"'")
                else:
                    temdbc2.CommonExec_Sql(strSqlInsert2+",m_flag) "+strSqlValues+",'"+str(m_flag)+"')")
                temdbc2.CommonExec_Sql(strSqlInsert+",m_flag) "+strSqlValues+",'"+str(m_flag)+"')")