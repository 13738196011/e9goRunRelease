#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from builtins import str
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_IHGDW0 import A01_Para
from com.plugins.A01_IHGDW0.A01_Fun import A01_Fun
from com.zxy.z_debug import z_debug
class HJpacket(z_debug):
    attBeg = "##"    
    attQN = ""
    attST = "27"
    attCN = ""
    attPW = "123456"
    attMN = ""
    attFlag = ""
    attPNUM = ""
    attPNO = ""
    attCP = ""
    attLength = "0"
    attCRC = ""
    attEnd = "\r\n"
    def __init__(self):
        self.attMN = Com_Fun.GetHashTable(A01_Para.ht_t_param_value,"ZET_MN")
        pass
    def clear_value(self):
        self.attBeg = ""
        self.attQN = ""
        self.attST = ""
        self.attCN = ""
        self.attFlag = ""
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = ""
        self.attCRC = ""
    def init_value(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "27"
        self.attCN = ""
        self.attFlag = ""
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_B_3_1(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "27"
        self.attCN = "2011"
        self.attFlag = "5"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_B_3_2(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "91"
        self.attCN = "9014"
        self.attFlag = "4"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_B_3_3(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "27"
        self.attCN = "3020"
        self.attFlag = "5"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_B_3_4(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "27"
        self.attCN = "3020"
        self.attFlag = "5"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_B_3_5(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "27"
        self.attCN = "1012"
        self.attFlag = "5"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_B_3_6(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "27"
        self.attCN = "3011"
        self.attFlag = "5"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_B_3_7(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "27"
        self.attCN = "3100"
        self.attFlag = "5"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def init_B_3_8(self):
        self.attBeg = "##"    
        self.attQN = ""
        self.attST = "27"
        self.attCN = "3013"
        self.attFlag = "5"
        self.attPNUM = ""
        self.attPNO = ""
        self.attCP = ""
        self.attLength = "0"
        self.attCRC = ""
    def GetSendPack_CP(self):
        if self.attQN == "":
            self.attQN = Com_Fun.GetTime("%Y%m%d%H%M%S%f")[:-3]
        temPacket = "QN="+self.attQN+";ST="+self.attST+";CN="+self.attCN+";PW="+self.attPW
        temPacket = temPacket +";MN="+self.attMN+";Flag="+self.attFlag+";CP=&&"+self.attCP+"&&" 
        temL = len(temPacket.encode(Com_Para.U_CODE))
        strL = str(temL)
        if len(strL) == 1 :
            self.attLength = "000"+strL
        elif len(strL) == 2:
            self.attLength = "00"+strL
        elif len(strL) == 3:
            self.attLength = "0"+strL
        elif len(strL) == 4:
            self.attLength = strL        
        self.attCRC = self.CheckCRC16Z_CP(temPacket).upper()
        return self.attBeg+self.attLength+temPacket+self.attCRC+self.attEnd   
    def GetSendPack(self):        
        if self.attQN == "":
            self.attQN = Com_Fun.GetTime("%Y%m%d%H%M%S%f")[:-3]
        temPacket = "QN="+self.attQN+";ST="+self.attST+";CN="+self.attCN+";PW="+self.attPW
        temPacket = temPacket +";MN="+self.attMN+";Flag="+self.attFlag+";CP=&&"+self.attCP+"&&"        
        temL = len(temPacket.encode(Com_Para.U_CODE))
        strL = str(temL)
        if len(strL) == 1 :
            self.attLength = "000"+strL
        elif len(strL) == 2:
            self.attLength = "00"+strL
        elif len(strL) == 3:
            self.attLength = "0"+strL
        elif len(strL) == 4:
            self.attLength = strL        
        self.attCRC = self.CheckCRC16Z(temPacket).upper()
        return self.attBeg+self.attLength+temPacket+self.attCRC+self.attEnd
    def GetHeartLoop(self):
        self.init_value()
        self.attQN = Com_Fun.GetTime("%Y%m%d%H%M%S%f")[:-3]
        temPacket = "QN="+self.attQN+";ST="+self.attST+";CN="+self.attCN+";PW="+self.attPW
        temPacket = temPacket +";MN="+self.attMN+";Flag="+self.attFlag+";CP=&&"+self.attCP+"&&"        
        temL = len(temPacket)
        strL = str(temL)
        if len(strL) == 1 :
            self.attLength = "000"+strL
        elif len(strL) == 2:
            self.attLength = "00"+strL
        elif len(strL) == 3:
            self.attLength = "0"+strL
        elif len(strL) == 4:
            self.attLength = strL        
        self.attCRC = self.CheckCRC16Z(temPacket).upper()
        return self.attBeg+self.attLength+temPacket+self.attCRC+self.attEnd
    def CalCRC16(self):
        temPacket = "QN="+self.attQN+";ST="+self.attST+";CN="+self.attCN+";PW="+self.attPW
        temPacket = temPacket +";MN="+self.attMN+";Flag="+self.attFlag+";CP=&&"+self.attCP+"&&"
        return self.CheckCRC16Z(temPacket).upper()
    def CheckCRC16Z_CP(self,inputData):
        usDataLen = b''
        usDataLen = bytes(inputData, encoding=Com_Para.U_CODE)
        crc_reg = 0xFFFF
        for i in usDataLen:
            crc_reg = ( crc_reg >> 8) ^ i
            for j in range(8):
                check = crc_reg & 0x0001
                crc_reg >>= 1
                if check == 0x0001 :
                    crc_reg ^= 0xA001
        temV = hex(crc_reg)[2:6]
        if len(temV) == 3:
            temV = "0"+temV 
        elif len(temV) == 2:
            temV = "00"+temV
        elif len(temV) == 1:
            temV = "000"+temV
        return temV
    def CheckCRC16Z(self,inputData):
        usDataLen = b''
        usDataLen = bytes(inputData, encoding=Com_Para.U_CODE)
        crc_reg = 0xFFFF
        for i in usDataLen:
            crc_reg = ( crc_reg >> 8) ^ i
            for j in range(8):
                check = crc_reg & 0x0001
                crc_reg >>= 1
                if check == 0x0001 :
                    crc_reg ^= 0xA001
        temV = hex(crc_reg)[2:6]
        if len(temV) == 3:
            temV = "0"+temV 
        elif len(temV) == 2:
            temV = "00"+temV
        elif len(temV) == 1:
            temV = "000"+temV
        return temV
    def CheckCRC16(self,inputData):
        temInit_msg = b''
        temInit_msg = bytes(inputData, encoding=Com_Para.U_CODE)
        crc_reg = 0xFFFF
        for temBt in temInit_msg:
            crc_reg = (crc_reg>>8) ^ temBt
            for i in range(8):
                check = crc_reg & 0x0001
                crc_reg >>= 1
                if check == 0x0001:
                    crc_reg ^= 0xA001
        temV = hex(crc_reg)[2:6]
        if len(temV) == 3:
            temV = "0"+temV 
        elif len(temV) == 2:
            temV = "00"+temV
        elif len(temV) == 1:
            temV = "000"+temV
        return temV