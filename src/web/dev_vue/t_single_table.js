//主从表tab选项卡标志位
var A12594_Tab_Flag = -1;
//选择某一行
var A12594_select_t_single_table_rowId = "";
//按钮事件新增或编辑
var A12594_type = "";
//其他页面传到本页面参数
var A12594_param = {};
//table选中数据，如无则默认第一条
var A12594_rowCheckData = null;

/*定义查询条件变量*/
/*declare query param begin*/
var A12594_tem_TABLE_CN_NAME = "";var A12594_tem_TABLE_EN_NAME = "";var A12594_tem_TABLE_TYPE = "0";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var A12594_ary_TABLE_TYPE = [{'MAIN_ID': '0', 'CN_NAME': '表'}, {'MAIN_ID': '1', 'CN_NAME': '视图'}];
/*declare select options end*/

/*绑定show监听事件*/
if(A12594_Tab_Flag == "-1"){
	var n = Get_RandomDiv("A12594","");
	$(n).bind("show", function(objTag){
		A12594_Adjust_Sub_Sequ();
		if(A12594_Tab_Flag > 0)
			A12594_adjust_tab();		
	});
	$(n).bind("hide", function(objTag){
		A12594_hide_tab_fun();		
	});
}

//设置主从表div顺序
function A12594_Adjust_Sub_Sequ(){
	var temSubDivs = $("#content-main").find("div[data-id]");
	var MainDiv = $(Get_RandomDiv("A12594",""));
	for(i = 0; i < temSubDivs.length; i++) {
		if(A12594_Is_Sub_Div(temSubDivs[i].id)){
			$(temSubDivs[i]).before($(MainDiv));
			break;
		}
	}
}

//主从表传递参数
function A12594_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function A12594_t_single_table_biz_start(inputparam) {
	layer.close(ly_index);
	A12594_param = inputparam;
	//主从表传递参数
	A12594_param_set();	
    /*biz begin*/
    if($("#A12594_qry_TABLE_TYPE").is("select") && $("#A12594_qry_TABLE_TYPE")[0].options.length == 0)    {        $("#A12594_qry_TABLE_TYPE").append("<option value='-1'></option>")        $.each(A12594_ary_TABLE_TYPE, function (i, obj) {            addOptionValue("A12594_qry_TABLE_TYPE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    A12594_init_t_single_table()	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function A12594_format_TABLE_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < A12594_ary_TABLE_TYPE.length; i++) {        var obj = A12594_ary_TABLE_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function A12594_page_end(){
	A12594_adjust_tab();
}

//元数据表显示列定义
var A12594_t_single_table = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '表中文名',        field: 'TABLE_CN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '表英文名',        field: 'TABLE_EN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '视图类SQL语句或接口名',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '表或视图',        field: 'TABLE_TYPE',        sortable: true        ,formatter: A12594_format_TABLE_TYPE    }	
	/*table column end*/
];

//页面初始化
function A12594_init_t_single_table() {
	$(window).resize(function () {
		  $('#A12594_t_single_table_Events').bootstrapTable('resetView');
	});
	//元数据表查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#A12594_btn_t_single_table_query').click();
}

//查询接口
function A12594_t_single_table_query() {
    $('#A12594_t_single_table_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_single_table",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(A12594_tem_TABLE_CN_NAME)        ,"param_value2": s_encode(A12594_tem_TABLE_EN_NAME)        ,"param_value3": A12594_tem_TABLE_TYPE        ,"param_value4": A12594_tem_TABLE_TYPE		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "A12594_get_N01_sel_t_single_table");
}

//查询结果
function A12594_get_N01_sel_t_single_table(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_single_table) == false)
        return false;
    A12594_rowCheckData = null;
    //调整table各列宽度
    $.each(A12594_t_single_table, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_t_single_table;
    if(s_data.length > 0)
    	A12594_rowCheckData = s_data[0];    
    A12594_select_t_single_table_rowId = "";
    A12594_Tab_Flag = 0;
    $('#A12594_t_single_table_Events').bootstrapTable('destroy');
    $("#A12594_t_single_table_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#A12594_t_single_table_Events').find("tr.changeColor").removeClass('changeColor');
                A12594_select_t_single_table_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#A12594_t_single_table_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                A12594_select_t_single_table_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#A12594_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(A12594_param.hasOwnProperty("target_name"))
			{
				$("#"+A12594_param["target_id"]).val(eval("row."+A12594_param["sourc_id"].toUpperCase()));
				$("#"+A12594_param["target_name"]).val(s_decode(eval("row."+A12594_param["sourc_name"].toUpperCase())));				
				layer.close(A12594_param["ly_index"]);
			}
		},
        toolbar: "#A12594_t_single_table_Toolbar",
        columns: A12594_t_single_table,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        onPostBody:function(){
        	if(s_data.length != 0)
            	A12594_page_end();
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#A12594_btn_t_single_table_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    A12594_tem_TABLE_CN_NAME = "";    A12594_tem_TABLE_EN_NAME = "";    A12594_tem_TABLE_TYPE = "0";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/

	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    if($("#A12594_qry_TABLE_TYPE").is("select") && $("#A12594_qry_TABLE_TYPE")[0].options.length == 0)    {        $("#A12594_qry_TABLE_TYPE").append("<option value='-1'></option>")        $.each(A12594_ary_TABLE_TYPE, function (i, obj) {            addOptionValue("A12594_qry_TABLE_TYPE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    A12594_init_t_single_table()	
    /*biz end*/
})

//查询按钮
$('#A12594_btn_t_single_table_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    A12594_tem_TABLE_CN_NAME = $("#A12594_qry_TABLE_CN_NAME").val();    A12594_tem_TABLE_EN_NAME = $("#A12594_qry_TABLE_EN_NAME").val();    A12594_tem_TABLE_TYPE = $("#A12594_qry_TABLE_TYPE").val();	
    /*set query param end*/
	A12594_t_single_table_query();
})

//vue回调
function A12594_t_single_table_call_vue(objResult){
	//选择某条记录或自动选择第一条
	if (A12594_select_t_single_table_rowId != "") 
		A12594_rowCheckData = $("#A12594_t_single_table_Events").bootstrapTable('getData')[A12594_select_t_single_table_rowId];
	
	if(index_subhtml == "t_single_table_$.vue")
	{
		var n = Get_RandomDiv("B12594",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":A12594_type,"ly_index":index};
				if(A12594_param.hasOwnProperty("hidden_find")){
					inputdata["hidden_find"] = "1";
				}				
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temTABLE_CN_NAME = $("#A12594_qry_TABLE_CN_NAME").val();                if(temTABLE_CN_NAME != ""){                    inputdata["TABLE_CN_NAME"] = temTABLE_CN_NAME;                }                var temTABLE_EN_NAME = $("#A12594_qry_TABLE_EN_NAME").val();                if(temTABLE_EN_NAME != ""){                    inputdata["TABLE_EN_NAME"] = temTABLE_EN_NAME;                }                var temTABLE_TYPE = $("#A12594_qry_TABLE_TYPE").val();                if(temTABLE_TYPE != ""){                    inputdata["TABLE_TYPE"] = temTABLE_TYPE;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("dev_vue/t_single_table_$.js","B12594_t_single_table_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/
    else if(index_subhtml == "t_table_column.vue"){        var n = Get_RandomDiv("A567C2",objResult);        $(n).show();        //传递参数        var inputdata = {"hidden_find":"1"};        if(A12594_rowCheckData != null){            inputdata["TABLE_ID_cn_name"] = A12594_rowCheckData[GetLowUpp("TABLE_EN_NAME")];            inputdata["TABLE_ID"] = A12594_rowCheckData[GetLowUpp("MAIN_ID")];        }        else{            inputdata["TABLE_ID_cn_name"] = "";            inputdata["TABLE_ID"] = "-999";        }        loadScript_hasparam("dev_vue/t_table_column.js","A567C2_t_table_column_biz_start",inputdata);    }
	else if(index_subhtml == "t_main_sub_link.vue"){
        var n = Get_RandomDiv("A6FC61",objResult);
        $(n).show();
        //传递参数
        var inputdata = {"hidden_find":"1"};
        if(A12594_rowCheckData != null){
            inputdata["TABLE_ID_cn_name"] = A12594_rowCheckData[GetLowUpp("TABLE_EN_NAME")];
            inputdata["TABLE_ID"] = A12594_rowCheckData[GetLowUpp("MAIN_ID")];
        }
        else{
            inputdata["TABLE_ID_cn_name"] = "";
            inputdata["TABLE_ID"] = "-999";
        }
        loadScript_hasparam("dev_vue/t_main_sub_link.js","A6FC61_t_main_sub_link_biz_start",inputdata);
    }
	/*get tab subvue end*/
}

//新增按钮
$("#A12594_btn_t_single_table_add").click(function () {
	A12594_type = "add";
	index_subhtml = "t_single_table_$.vue";
	if(loadHtmlSubVueFun("dev_vue/t_single_table_$.vue","A12594_t_single_table_call_vue") == true){
		var n = Get_RandomDiv("B12594","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				B12594_param["type"] = A12594_type;
				B12594_param["ly_index"]= index;
				if(A12594_param.hasOwnProperty("hidden_find")){
					B12594_param["hidden_find"] = "1";
				}
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temTABLE_CN_NAME = $("#A12594_qry_TABLE_CN_NAME").val();                if(temTABLE_CN_NAME != ""){                    B12594_param["TABLE_CN_NAME"] = temTABLE_CN_NAME;                }                var temTABLE_EN_NAME = $("#A12594_qry_TABLE_EN_NAME").val();                if(temTABLE_EN_NAME != ""){                    B12594_param["TABLE_EN_NAME"] = temTABLE_EN_NAME;                }                var temTABLE_TYPE = $("#A12594_qry_TABLE_TYPE").val();                if(temTABLE_TYPE != ""){                    B12594_param["TABLE_TYPE"] = temTABLE_TYPE;                }				
				/*Send Two FindSelect param end*/

				B12594_clear_edit_info();
			},
			end: function(){
				B12594_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#A12594_btn_t_single_table_edit").click(function () {
	if (A12594_select_t_single_table_rowId != "") {
		A12594_type = "edit";
		index_subhtml = "t_single_table_$.vue";
		if(loadHtmlSubVueFun("dev_vue/t_single_table_$.vue","A12594_t_single_table_call_vue") == true){
			var n = Get_RandomDiv("B12594","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					B12594_param["type"] = A12594_type;
					B12594_param["ly_index"] = index;
					if(A12594_param.hasOwnProperty("hidden_find")){
						B12594_param["hidden_find"] = "1";
					}
					B12594_clear_edit_info();
					B12594_get_edit_info();
				},
				end: function(){
					B12594_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#A12594_btn_t_single_table_delete').click(function () {
	//单行选择
	var rowData = $("#A12594_t_single_table_Events").bootstrapTable('getData')[A12594_select_t_single_table_rowId];
	//多行选择
	var rowDatas = A12594_sel_row_t_single_table();
	if (A12594_select_t_single_table_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_single_table",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "A12594_N01_del_t_single_table");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_single_table",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "A12594_N01_del_t_single_table");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//创建生成
$('#A12594_btn_t_single_table_build').click(function () {
	if (A12594_select_t_single_table_rowId == "") {
		swal({
            title: "提示信息",
            text: "无法创建业务，请选择正确记录信息!"
        });
	}
	else{
		var rowData = $("#A12594_t_single_table_Events").bootstrapTable('getData')[A12594_select_t_single_table_rowId];
		var select_main_id = rowData["MAIN_ID"];
		var inputdata = {
			"param_name": "T01_YYNNKK",
			"session_id": session_id,
			"login_id": login_id,
			"param_value1": select_main_id,
			"param_value2": $("#file_folder").val()
		};
		ly_index = layer.load();
		get_ajax_baseurl(inputdata, "get_T01_YYNNKK");
	}
});

//重置业务
$('#A12594_btn_t_single_table_reset').click(function () {
	if (A12594_select_t_single_table_rowId == "") {
		swal({
            title: "提示信息",
            text: "无法重置业务，请选择正确记录信息!"
        });
	}
	else{
		swal({
				title: "告警",
				text: "确认要重置业务吗?所有数据及页面将删除且无法恢复!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "确定",
				closeOnConfirm: false
			},
			function () {
				var rowData = $("#A12594_t_single_table_Events").bootstrapTable('getData')[A12594_select_t_single_table_rowId];
				var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "T01_YY77HH",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "get_T01_YY77HH");
		});
	}
});

//重置业务结果
function get_T01_YY77HH(input){
	layer.close(ly_index);
	//操作失败
    if (Call_OpeResult(input.T01_YY77HH) == false)
        return false;    
}

//创建业务结果
function get_T01_YYNNKK(input){
	layer.close(ly_index);
	//操作失败
    if (Call_OpeResult(input.T01_YYNNKK) == false)
        return false;  
}

//删除结果
function A12594_N01_del_t_single_table(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_single_table) == true)
		A12594_t_single_table_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function A12594_adjust_tab(){
	if(typeof($("#A12594_TAB_MAIN")[0]) != "undefined" && $("#A12594_TAB_MAIN")[0].length != 0){
		A12594_Tab_Flag = 1;
		$(Get_RDivNoBuild("A12594","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("A12594","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("A12594","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#A12594_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/
$("#A12594_tab_1").click(function(){    A12594_Tab_Flag = 1;    A12594_hide_tab_fun();    //主子表参数传递    var temPar = [{"sourc_id":"MAIN_ID","target_id":"TABLE_ID"},{"sourc_id":"TABLE_EN_NAME","target_id":"TABLE_ID_cn_name"}];    A12594_show_tab_fun("t_table_column.vue","A567C2",temPar);});
$("#A12594_tab_2").click(function(){
    A12594_Tab_Flag = 1;
    A12594_hide_tab_fun();
    //主子表参数传递
    var temPar = [{"sourc_id":"MAIN_ID","target_id":"TABLE_ID"},{"sourc_id":"TABLE_EN_NAME","target_id":"TABLE_ID_cn_name"}];
    A12594_show_tab_fun("t_main_sub_link.vue","A6FC61",temPar);
});
//隐藏tab页选项卡function A12594_hide_tab_fun(){    var n = null;    n = Get_RDivNoBuild("A567C2","");    $(n).hide();
    n = Get_RDivNoBuild("A6FC61","");
    $(n).hide();}//判断是否sub子项div页面function A12594_Is_Sub_Div(temDivId){    if(temDivId.indexOf("XX$TTT") == 0)        return false;    else if(temDivId.indexOf("A567C2") == 0)        return true;
    else if(temDivId.indexOf("A6FC61") == 0)
        return true;    return false;}
/*Tab Click Fun End*/

function A12594_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("dev_vue/"+inputUrl,"A12594_t_single_table_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (A12594_select_t_single_table_rowId != "") 
			rowData = $("#A12594_t_single_table_Events").bootstrapTable('getData')[A12594_select_t_single_table_rowId];
		else
			rowData = A12594_rowCheckData;
		//$("#A12594_t_single_table_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			var temFlag = true;
			$.each(temPar, function (i, obj) {
				if(eval(inputrandom+"_param.hasOwnProperty(\""+obj.target_id+"\")") && eval(inputrandom+"_param[\""+obj.target_id+"\"]").toString() == rowData[obj.sourc_id].toString())
					temFlag = false;
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			if(temFlag){
				//传递子页面隐藏find功能
				eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
				//参数传递并赋值
				eval(inputrandom+"_param_set()");
				var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
				$("#"+inputrandom+"_btn_"+tbName+"_query").click();
			}
		}
		else{
			$.each(temPar, function (i, obj) {
				if(obj.sourc_id.toUpperCase() == "MAIN_ID")
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"-999\"");
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"\"");
			});
			//传递子页面隐藏find功能
			eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
			var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
			$("#"+inputrandom+"_btn_"+tbName+"_query").click();		
		}
	}
}

//清除 查找框
function A12594_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function A12594_sel_row_t_single_table(){
	//获得选中行
	var checkedbox= $("#A12594_t_single_table_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}