#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class v_prot_element(object):
    attprot_id = -1
    attprot_name = ""
    attmain_id = -1
    attelement_code = ""
    attelement_name = ""
    attelement_val = ""
    attelement_type = ""
    attelement_state = ""
    attcreate_date = None
    atts_desc = ""
    attdot_num = 0
    attup_level = ""
    attdown_level = ""
    attup_limit = ""
    attdown_limit = ""
    attcom_port = ""
    attprot_up_limit = ""
    attprot_down_limit = ""
    attelement_code2 = ""
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")