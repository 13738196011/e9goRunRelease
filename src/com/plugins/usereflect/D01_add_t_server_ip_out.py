#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.db_Self.Db_Common_Self import Db_Common_Self
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.z_debug import z_debug
class D01_add_t_server_ip_out(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = "1"
    def __init__(self):
        pass
    def init_start(self):
        temdbc2 = Db_Common2()        
        temdbc_self = Db_Common_Self()
        try:
            temStrSql = "select main_id,server_ip,server_port,server_name,up_protocol_id,create_date,s_desc from t_server_ip"
            temRs = temdbc2.Common_Sql(temStrSql)
            temServerIPList = ""
            iIndex = 0
            for temItem in temRs:
                if iIndex > 0:
                    temServerIPList = temServerIPList + ";"
                if Com_Fun.NoNull(temItem[1]) != "" and Com_Fun.ZeroNull(temItem[2]) > 0 :
                    temServerIPList = temServerIPList + str(temItem[1])+":"+ str(temItem[2])
                iIndex = iIndex + 1
            temStrSql = "update t_param_value set param_value = '"+temServerIPList+"' where param_key = 'ServerIPList'"
            temdbc_self.CommonExec_Sql(temStrSql)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
            return False
        temdbc_self = Db_Common_Self()
