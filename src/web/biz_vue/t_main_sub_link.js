//主从表tab选项卡标志位
var A83CFB_Tab_Flag = "-1";
//选择某一行
var A83CFB_select_t_main_sub_link_rowId = "";
//按钮事件新增或编辑
var A83CFB_type = "";
//其他页面传到本页面参数
var A83CFB_param = {};
//table选中数据，如无则默认第一条
var A83CFB_rowCheckData = null;

/*定义查询条件变量*/
/*declare query param begin*/
var A83CFB_tem_MAIN_TB_NAME = "";var A83CFB_tem_SUB_TB_NAME = "";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var A83CFB_ary_MAIN_TB_NAME = null;var A83CFB_ary_SUB_TB_NAME = null;
/*declare select options end*/

/*绑定show监听事件*/
if(A83CFB_Tab_Flag == "-1"){
	var n = Get_RandomDiv("A83CFB","");
	$(n).bind("show", function(objTag){
		if(A83CFB_Tab_Flag != "-1")
			A83CFB_adjust_tab();		
	});
}

//主从表传递参数
function A83CFB_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function A83CFB_t_main_sub_link_biz_start(inputparam) {
	layer.close(ly_index);
	A83CFB_param = inputparam;
	//主从表传递参数
	A83CFB_param_set();	
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_main_sub_link$MAIN_TB_NAME",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A83CFB_get_N01_t_main_sub_link$MAIN_TB_NAME");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function A83CFB_format_MAIN_TB_NAME(value, row, index) {    var objResult = value;    for(i = 0; i < A83CFB_ary_MAIN_TB_NAME.length; i++) {        var obj = A83CFB_ary_MAIN_TB_NAME[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A83CFB_format_SUB_TB_NAME(value, row, index) {    var objResult = value;    for(i = 0; i < A83CFB_ary_SUB_TB_NAME.length; i++) {        var obj = A83CFB_ary_SUB_TB_NAME[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A83CFB_get_N01_t_main_sub_link$MAIN_TB_NAME(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_main_sub_link$MAIN_TB_NAME) == false)        return false;    A83CFB_ary_MAIN_TB_NAME = input.N01_t_main_sub_link$MAIN_TB_NAME;    var inputdata = {        "param_name": "N01_t_main_sub_link$SUB_TB_NAME",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A83CFB_get_N01_t_main_sub_link$SUB_TB_NAME");}function A83CFB_get_N01_t_main_sub_link$SUB_TB_NAME(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_main_sub_link$SUB_TB_NAME) == false)        return false;    A83CFB_ary_SUB_TB_NAME = input.N01_t_main_sub_link$SUB_TB_NAME;    A83CFB_init_t_main_sub_link();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function A83CFB_MAIN_TB_NAME_cn_name_fun(){    index_subhtml = "V_VTC001.vue"    random_subhtml = "C60403";    if(loadHtmlSubVueFun("biz_vue/V_VTC001.vue","A83CFB_t_main_sub_link_call_vue") == true){        var n = Get_RandomDiv("C60403","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#C60403_V_VTC001_Events').bootstrapTable('resetView');                C60403_param["ly_index"] = index;                C60403_param["target_name"] = "A83CFB_find_MAIN_TB_NAME_cn_name"                C60403_param["target_id"] = "A83CFB_find_MAIN_TB_NAME"                C60403_param["sourc_id"] = "MAIN_ID"                C60403_param["sourc_name"] = "COLUMN_EN_NAME"            },            end: function(){                $(n).hide();            }        });     }}function A83CFB_SUB_TB_NAME_cn_name_fun(){    index_subhtml = "V_VTC001.vue"    random_subhtml = "C60403";    if(loadHtmlSubVueFun("biz_vue/V_VTC001.vue","A83CFB_t_main_sub_link_call_vue") == true){        var n = Get_RandomDiv("C60403","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#C60403_V_VTC001_Events').bootstrapTable('resetView');                C60403_param["ly_index"] = index;                C60403_param["target_name"] = "A83CFB_find_SUB_TB_NAME_cn_name"                C60403_param["target_id"] = "A83CFB_find_SUB_TB_NAME"                C60403_param["sourc_id"] = "MAIN_ID"                C60403_param["sourc_name"] = "COLUMN_EN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function A83CFB_page_end(){
	A83CFB_adjust_tab();
}

//主从关联表显示列定义
var A83CFB_t_main_sub_link = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '主表名及字段',        field: 'MAIN_TB_NAME',        sortable: true        ,formatter: A83CFB_format_MAIN_TB_NAME    },    {        title: '从表名及字段',        field: 'SUB_TB_NAME',        sortable: true        ,formatter: A83CFB_format_SUB_TB_NAME    },    {        title: '主表关联显示字段',        field: 'MAIN_TITLE_COLUMN',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function A83CFB_init_t_main_sub_link() {
	$(window).resize(function () {
		  $('#A83CFB_t_main_sub_link_Events').bootstrapTable('resetView');
	});
	//主从关联表查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#A83CFB_btn_t_main_sub_link_query').click();
}

//查询接口
function A83CFB_t_main_sub_link_query() {
    $('#A83CFB_t_main_sub_link_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_main_sub_link",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(A83CFB_tem_MAIN_TB_NAME)
        ,"param_value2": s_encode(A83CFB_tem_MAIN_TB_NAME)        ,"param_value3": s_encode(A83CFB_tem_SUB_TB_NAME)	
        ,"param_value4": s_encode(A83CFB_tem_SUB_TB_NAME)		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "A83CFB_get_N01_sel_t_main_sub_link");
}

//查询结果
function A83CFB_get_N01_sel_t_main_sub_link(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_main_sub_link) == false)
        return false;
    A83CFB_rowCheckData = null;
    //调整table各列宽度
    $.each(A83CFB_t_main_sub_link, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_t_main_sub_link;
    A83CFB_select_t_main_sub_link_rowId = "";
    $('#A83CFB_t_main_sub_link_Events').bootstrapTable('destroy');
    $("#A83CFB_t_main_sub_link_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#A83CFB_t_main_sub_link_Events').find("tr.changeColor").removeClass('changeColor');
                A83CFB_select_t_main_sub_link_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#A83CFB_t_main_sub_link_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                A83CFB_select_t_main_sub_link_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#A83CFB_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(A83CFB_param.hasOwnProperty("target_name"))
			{
				$("#"+A83CFB_param["target_id"]).val(eval("row."+A83CFB_param["sourc_id"].toUpperCase()));
				$("#"+A83CFB_param["target_name"]).val(s_decode(eval("row."+A83CFB_param["sourc_name"].toUpperCase())));				
				layer.close(A83CFB_param["ly_index"]);
			}
		},
        toolbar: "#A83CFB_t_main_sub_link_Toolbar",
        columns: A83CFB_t_main_sub_link,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        onPostBody:function(){
        	if(s_data.length != 0)
            	A83CFB_page_end();
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#A83CFB_btn_t_main_sub_link_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    A83CFB_tem_MAIN_TB_NAME = "";    A83CFB_tem_SUB_TB_NAME = "";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    B83CFB_ary_MAIN_TB_NAME = null;    B83CFB_ary_SUB_TB_NAME = null;    $("#A83CFB_find_MAIN_TB_NAME_cn_name").val("");    $("#A83CFB_find_MAIN_TB_NAME").val("-1");    $("#A83CFB_find_SUB_TB_NAME_cn_name").val("");    $("#A83CFB_find_SUB_TB_NAME").val("-1");
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_main_sub_link$MAIN_TB_NAME",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A83CFB_get_N01_t_main_sub_link$MAIN_TB_NAME");	
    /*biz end*/
})

//查询按钮
$('#A83CFB_btn_t_main_sub_link_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    A83CFB_tem_MAIN_TB_NAME = $("#A83CFB_find_MAIN_TB_NAME").val();    A83CFB_tem_SUB_TB_NAME = $("#A83CFB_find_SUB_TB_NAME").val();	
    /*set query param end*/
	A83CFB_t_main_sub_link_query();
})

//vue回调
function A83CFB_t_main_sub_link_call_vue(objResult){
	if(index_subhtml == "t_main_sub_link_$.vue")
	{
		var n = Get_RandomDiv("B83CFB",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":A83CFB_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temMAIN_TB_NAME = $("#A83CFB_find_MAIN_TB_NAME_cn_name").val();                if(temMAIN_TB_NAME != ""){                    inputdata["MAIN_TB_NAME_cn_name"] = temMAIN_TB_NAME;                    inputdata["MAIN_TB_NAME"] = $("#A83CFB_MAIN_TB_NAME").val();                }                var temSUB_TB_NAME = $("#A83CFB_find_SUB_TB_NAME_cn_name").val();                if(temSUB_TB_NAME != ""){                    inputdata["SUB_TB_NAME_cn_name"] = temSUB_TB_NAME;                    inputdata["SUB_TB_NAME"] = $("#A83CFB_SUB_TB_NAME").val();                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_main_sub_link_$.js","B83CFB_t_main_sub_link_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "V_VTC001.vue"){        var n = Get_RandomDiv("C60403",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":A83CFB_type,                    "ly_index":index,                    "target_name":"A83CFB_find_MAIN_TB_NAME_cn_name",                    "target_id":"A83CFB_find_MAIN_TB_NAME",                    "sourc_id":"MAIN_ID",                    "sourc_name":"COLUMN_EN_NAME"                };                loadScript_hasparam("biz_vue/V_VTC001.js","C60403_V_VTC001_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }    else if(index_subhtml == "V_VTC001.vue"){        var n = Get_RandomDiv("C60403",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":A83CFB_type,                    "ly_index":index,                    "target_name":"A83CFB_find_SUB_TB_NAME_cn_name",                    "target_id":"A83CFB_find_SUB_TB_NAME",                    "sourc_id":"MAIN_ID",                    "sourc_name":"COLUMN_EN_NAME"                };                loadScript_hasparam("biz_vue/V_VTC001.js","C60403_V_VTC001_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/

	/*get tab subvue end*/
}

//新增按钮
$("#A83CFB_btn_t_main_sub_link_add").click(function () {
	A83CFB_type = "add";
	index_subhtml = "t_main_sub_link_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_main_sub_link_$.vue","A83CFB_t_main_sub_link_call_vue") == true){
		var n = Get_RandomDiv("B83CFB","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				B83CFB_param["type"] = A83CFB_type;
				B83CFB_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temMAIN_TB_NAME = $("#A83CFB_find_MAIN_TB_NAME_cn_name").val();                if(temMAIN_TB_NAME != ""){                    B83CFB_param["MAIN_TB_NAME_cn_name"] = temMAIN_TB_NAME;                    B83CFB_param["MAIN_TB_NAME"] = $("#A83CFB_MAIN_TB_NAME").val();                }                var temSUB_TB_NAME = $("#A83CFB_find_SUB_TB_NAME_cn_name").val();                if(temSUB_TB_NAME != ""){                    B83CFB_param["SUB_TB_NAME_cn_name"] = temSUB_TB_NAME;                    B83CFB_param["SUB_TB_NAME"] = $("#A83CFB_SUB_TB_NAME").val();                }				
				/*Send Two FindSelect param end*/

				B83CFB_clear_edit_info();
			},
			end: function(){
				B83CFB_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#A83CFB_btn_t_main_sub_link_edit").click(function () {
	if (A83CFB_select_t_main_sub_link_rowId != "") {
		A83CFB_type = "edit";
		index_subhtml = "t_main_sub_link_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_main_sub_link_$.vue","A83CFB_t_main_sub_link_call_vue") == true){
			var n = Get_RandomDiv("B83CFB","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					B83CFB_param["type"] = A83CFB_type;
					B83CFB_param["ly_index"] = index;
					B83CFB_clear_edit_info();
					B83CFB_get_edit_info();
				},
				end: function(){
					B83CFB_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#A83CFB_btn_t_main_sub_link_delete').click(function () {
	//单行选择
	var rowData = $("#A83CFB_t_main_sub_link_Events").bootstrapTable('getData')[A83CFB_select_t_main_sub_link_rowId];
	//多行选择
	var rowDatas = A83CFB_sel_row_t_main_sub_link();
	if (A83CFB_select_t_main_sub_link_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_main_sub_link",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "A83CFB_N01_del_t_main_sub_link");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_main_sub_link",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "A83CFB_N01_del_t_main_sub_link");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function A83CFB_N01_del_t_main_sub_link(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_main_sub_link) == true)
		A83CFB_t_main_sub_link_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function A83CFB_adjust_tab(){
	if(typeof($("#A83CFB_TAB_MAIN")[0]) != "undefined" && $("#A83CFB_TAB_MAIN")[0].length != 0){
		A83CFB_Flag = "1";
		$(Get_RDivNoBuild("A83CFB","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("A83CFB","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("A83CFB","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#A83CFB_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/
//隐藏tab页选项卡function hide_tab_fun(){    var n = null;}
/*Tab Click Fun End*/

function A83CFB_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("biz_vue/"+inputUrl,"A83CFB_t_main_sub_link_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (A83CFB_select_t_main_sub_link_rowId != "") 
			rowData = $("#A83CFB_t_main_sub_link_Events").bootstrapTable('getData')[A83CFB_select_t_proc_name_rowId];
		else
			rowData = $("#A83CFB_t_main_sub_link_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			$.each(temPar, function (i, obj) {
				eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
		}
		var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
		$("#"+inputrandom+"_btn_"+tbName+"_query").click();
	}
}

//清除 查找框
function A83CFB_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function A83CFB_sel_row_t_main_sub_link(){
	//获得选中行
	var checkedbox= $("#A83CFB_t_main_sub_link_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}