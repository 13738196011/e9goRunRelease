#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_single_table(object):
    attmain_id = -1
    attTABLE_CN_NAME = ""
    attTABLE_EN_NAME = ""
    attCREATE_DATE = None
    attS_DESC = ""
    attTABLE_TYPE = 0
    lenTABLE_CN_NAME = 50
    lenTABLE_EN_NAME = 50
    lenS_DESC = 500
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["TABLE_CN_NAME"] = "表中文名"
        temHt["TABLE_EN_NAME"] = "表英文名"
        temHt["CREATE_DATE"] = "系统时间"
        temHt["S_DESC"] = "视图类SQL语句或接口名"
        temHt["TABLE_TYPE"] = "表或视图"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["TABLE_CN_NAME"] = "表中文名"
        temHtQry["TABLE_EN_NAME"] = "表英文名"
        temHtQry["TABLE_TYPE"] = "表或视图"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["TABLE_TYPE"] = [{"MAIN_ID":"0","CN_NAME":"表"},{"MAIN_ID":"1","CN_NAME":"视图"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
