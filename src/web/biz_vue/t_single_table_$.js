//按钮事件新增或编辑
var BE35BE_type = "";
//其他页面传到本页面参数
var BE35BE_param = {};
//暂时没用
var BE35BE_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

//业务逻辑数据开始
function BE35BE_t_single_table_biz_start(inputdata) {
	BE35BE_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    BE35BE_init_t_single_table()	
    /*biz end*/
}

/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function BE35BE_page_end(){
	if(BE35BE_param["type"] == "edit"){
		BE35BE_get_edit_info();
	}
}

//页面初始化方法
function BE35BE_init_t_single_table() {
	//type = getUrlParam("type");
	if(BE35BE_param["type"] == "add"){
		$("#BE35BE_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BE35BE_table_cn_name").val(BE35BE_param["table_cn_name"]);        $("#BE35BE_table_en_name").val(BE35BE_param["table_en_name"]);		
		/*Get Find Select param end*/	
	}
	else if(BE35BE_param["type"] == "edit"){
	
	}
	
	//表单验证
	BE35BE_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BE35BE_create_date").val() == "")    {        $("#BE35BE_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BE35BE_create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BE35BE_page_end();
}

//提交表单数据
function BE35BE_SubmitForm(){
	if(BE35BE_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_single_table",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#BE35BE_table_cn_name").val())                ,"param_value2": s_encode($("#BE35BE_table_en_name").val())                ,"param_value3": $("#BE35BE_create_date").val()                ,"param_value4": s_encode($("#BE35BE_s_desc").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BE35BE_get_N01_ins_t_single_table");
	}
	else if(BE35BE_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_single_table",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#BE35BE_table_cn_name").val())                ,"param_value2": s_encode($("#BE35BE_table_en_name").val())                ,"param_value3": $("#BE35BE_create_date").val()                ,"param_value4": s_encode($("#BE35BE_s_desc").val())                ,"param_value5": $("#BE35BE_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BE35BE_get_N01_upd_t_single_table");
	}
}

//vue回调
function BE35BE_t_single_table_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#BE35BE_save_t_single_table_Edit").click(function () {
	$("form[name='BE35BE_DataModal']").submit();
})

/*修改数据*/
function BE35BE_get_N01_upd_t_single_table(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_single_table) == true)
	{
		swal("修改数据成功!", "", "success");
		AE35BE_t_single_table_query();
		BE35BE_clear_validate();
		layer.close(BE35BE_param["ly_index"]);
	}
}

/*添加数据*/
function BE35BE_get_N01_ins_t_single_table(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_single_table) == true)
	{
		swal("添加数据成功!", "", "success");
		AE35BE_t_single_table_query();
		BE35BE_clear_validate();
		layer.close(BE35BE_param["ly_index"]);
	}
}

//取消编辑
$("#BE35BE_cancel_t_single_table_Edit").click(function () {
	layer.close(BE35BE_param["ly_index"]);
	BE35BE_clear_validate();
	$("[id^='BE35BE_div']").hide();
})

//清除查找框
function BE35BE_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BE35BE_clear_validate(){
	$("#BE35BE_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BE35BE_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BE35BE_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BE35BE_clear_edit_info(){
	var inputs = $("#BE35BE_DataModal").find('input');
	var selects = $("#BE35BE_DataModal").find("select");
	var textareas = $("#BE35BE_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	BE35BE_init_t_single_table();
}

//页面输入框赋值
function BE35BE_get_edit_info(){
	var rowData = $("#AE35BE_t_single_table_Events").bootstrapTable('getData')[AE35BE_select_t_single_table_rowId];
	var inputs = $("#BE35BE_DataModal").find('input');
	var selects = $("#BE35BE_DataModal").find("select");
	var textareas = $("#BE35BE_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"AE35BE","BE35BE");
	
	/*
	$.each(selects, function (i, obj) {
		if(typeof(eval("AE35BE_ary_"+obj.id.toString().replace("BE35BE_",""))) == "object")
		{
			$.each(eval("AE35BE_ary_"+obj.id.toString().replace("BE35BE_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE35BE_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("BE35BE_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("AE35BE_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#BE35BE_DataModal").find('[id="'+"BE35BE_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE35BE_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE35BE_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});*/
}

//form验证
function BE35BE_checkFormInput() {
    BE35BE_validate = $("#BE35BE_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BE35BE_main_id: {}            ,BE35BE_table_cn_name: {}            ,BE35BE_table_en_name: {}            ,BE35BE_create_date: {date: true,required : true,maxlength:19}            ,BE35BE_s_desc: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BE35BE_main_id: {}            ,BE35BE_table_cn_name: {}            ,BE35BE_table_en_name: {}            ,BE35BE_create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BE35BE_s_desc: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BE35BE_SubmitForm();
        	return false;
        }
    })
}