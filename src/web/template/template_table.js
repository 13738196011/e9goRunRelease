var iTop = 0;
var ileft = 0;

//业务逻辑数据开始
function biz_start() {
	layer.close(ly_index);
    /*biz begin*/
    getWindowSize();
	init();
    $(window).bind("resize",
        function (){
        var width = $(".jqGrid_wrapper").width();
        $("#template_table_list").setGridWidth(width);
    });
    /*biz end*/
}

/*biz step begin*/

/*biz step end*/

/*页面结束*/
function page_end(){
}

//{template_table}显示列定义
var template_table = [
	/*table column begin*/
	
	/*table column end*/
];

function T01_sel_template_table(input) {
	//查询失败
    if (Call_QryResult(input.T01_sel_template_table) == false)
        return false;

    var total = $("#template_table_list").jqGrid('getGridParam', 'records');
    if (typeof(total) == "undefined") {
    	$("#template_table_list").jqGrid({
        	data: input.T01_sel_template_table,
            datatype: "local",
            height: "450",
            autowidth: true,
            shrinkToFit: true,
            rowNum: 10,
            rowList: [10, 20, 30],
            colModel: template_table,
            editurl: 'clientArray',
            altRows: true,
            pager: "#template_table_div",
            viewrecords: true,
            caption: "{template_table}列表",
            //rownumbers:true,
            gridview: true
        });

        //导航后面，增加增删改查按钮
        $("#template_table_list").jqGrid("navGrid", "#template_table_div", {
            edit: true,
            add: true,
            del: true,
            search: true,
            view: false,
            position: "left",
            cloneToTop: false
        }, {
            beforeShowForm: function () {
            $('#MAIN_ID').attr('readOnly', true);
            /*datetime format begin*/
            
            /*datetime format end*/
        },
        onclickSubmit: updateDbByMainId,
            closeAfterEdit: true
        }, {
            beforeShowForm: function () {
            /*datetime format begin*/
            	
            /*datetime format end*/
        },
        onclickSubmit: insertDb,
            closeAfterAdd: true
        }, {
            top: iTop,
            left: ileft,
            onclickSubmit: delDbByMainId
        }, {
            closeAfterSearch: true
        }, {
            height: 200,
            reloadAfterSubmit: true
        });
    } else {
        $("#template_table_list").jqGrid("clearGridData");
        $("#template_table_list").jqGrid('setGridParam', {
            data: input.T01_sel_template_table
        }).trigger("reloadGrid");
    }
    layer.close(ly_index);
}

//初始化
function init() {
    ly_index = layer.load();
    iTop = (winHeight - 300) / 2;
    ileft = (winWidth - 300) / 2;
    $.jgrid.defaults.styleUI = "Bootstrap";    
    var inputdata = {
    	"param_name": "T01_sel_template_table",
    	"session_id": session_id,
        "login_id": login_id
    };
    get_ajax_baseurl(inputdata, "T01_sel_template_table");
}

$(document).ready(function () {
    //init_page();
});

function T01_ins_template_table(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_ins_template_table) == true)
        init();
}

var insertDb = function () {
    ly_index = layer.load();
    var inputdata = {
        	"param_name": "T01_ins_template_table",
        	"session_id": session_id,
            "login_id": login_id
            /*insert param begin*/
            
            /*insert param end*/
        };
    get_ajax_baseurl(inputdata, "T01_ins_template_table");
};

function T01_upd_template_table(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_upd_template_table) == true)
        init();
}

var updateDbByMainId = function () {
    var rowid = $("#template_table_list").jqGrid("getGridParam", "selrow");
    var rowMAIN_ID = $('#template_table_list').jqGrid('getRowData', rowid).MAIN_ID;
    if (rowid.indexOf("jqg") != -1 || rowMAIN_ID.indexOf("jqg") != -1) {
        swal({
            title: "提示信息",
            text: "无法修改缓存记录，请选择正确修改!"
        });
        return false;
    }
    ly_index = layer.load();
    var inputdata = {
        "param_name": "T01_upd_template_table",
        "session_id": session_id,
        "login_id": login_id
        /*update param begin*/
        
        /*update param end*/
    };
    get_ajax_baseurl(inputdata, "T01_upd_template_table");
};

function T01_del_template_table(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_del_template_table) == true)
        init();
}

var delDbByMainId = function () {
    var rowid = $("#template_table_list").jqGrid("getGridParam", "selrow");
    var rowMAIN_ID = $('#template_table_list').jqGrid('getRowData', rowid).MAIN_ID;
    if (rowid.indexOf("jqg") != -1 || rowMAIN_ID.indexOf("jqg") != -1) {
        swal({
            title: "提示信息",
            text: "无法删除缓存记录，请选择正确删除!"
        });
        return false;
    }
    ly_index = layer.load();
    var inputdata = {
            "param_name": "T01_del_template_table",
            "session_id": session_id,
            "login_id": login_id
            /*delete param begin*/
            
            /*delete param end*/
        };
    get_ajax_baseurl(inputdata, "T01_del_template_table");
};

$(document).ready(function () {
    //页面初始化
    //init_page();
});

window.onpageshow = function (e) {
    if (e.persisted || (window.performance.navigation.type == 2)) {
        is_history_back = 1;
    } else {
        is_history_back = 0;
    }
    //页面初始化
    init_page();
};