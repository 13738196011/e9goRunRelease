#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import time,json,importlib,binascii,datetime
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_IHGDW0.A01_Fun import A01_Fun
from com.plugins.A01_IHGDW0.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_IHGDW0 import A01_Para
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.comport.ComModBus import ComModBus
from com.plugins.A01_IHGDW0.monitor_data_model import monitor_data_model
from openpyxl.descriptors.base import Length
from com.plugins.usereflect.Hex_Release import Hex_Release
from com.zxy.z_debug import z_debug
class A01_905MFA_Time(z_debug):
    strContinue      = "1"   
    strResult        = ""
    def __init__(self):
        pass
    def init_start(self):
        try:
            temCom = None     
            temA01modbus = ComModBus()
            hex_r = Hex_Release()
            for temv_prot_element in A01_Para.ht_v_prot_element:                
                if temv_prot_element.attprot_name == "A01_00K4T3": 
                    for temt_modbus_addr in A01_Para.ht_t_modbus_addr:
                        if str(temv_prot_element.attmain_id) == str(temt_modbus_addr.attelement_id) and str(temv_prot_element.attprot_id) == str(temt_modbus_addr.attprotocol_id):                         
                            temCom = temt_modbus_addr.attcom_port
                            hex_r.rec_byte = temA01modbus.get_data_com_hex(temCom,"010600000001480A") #协议文档案例 0110120000010200015591                                                   
                            hex_r.A01_00K4T3(1)                            
                elif temv_prot_element.attprot_name == "A01_00T5L4": 
                    for temt_modbus_addr in A01_Para.ht_t_modbus_addr:
                        if str(temv_prot_element.attmain_id) == str(temt_modbus_addr.attelement_id) and str(temv_prot_element.attprot_id) == str(temt_modbus_addr.attprotocol_id):                         
                            temCom = temt_modbus_addr.attcom_port
                            hex_r.rec_byte = temA01modbus.get_data_com_hex(temCom,"01060110000089F3") #协议文档案例 01060110000089F3                                                   
                            hex_r.A01_00T5L4(1)                                            
                elif temv_prot_element.attprot_name == "A01_77H4UY": 
                    for temt_modbus_addr in A01_Para.ht_t_modbus_addr:
                        if str(temv_prot_element.attmain_id) == str(temt_modbus_addr.attelement_id) and str(temv_prot_element.attprot_id) == str(temt_modbus_addr.attprotocol_id):                         
                            temCom = temt_modbus_addr.attcom_port
                            hex_r.rec_byte = temA01modbus.get_data_com_hex(temCom,"0110120000010200015591")
                            hex_r.A01_77H4UY(1)                            
                elif temv_prot_element.attprot_name == "A01_V47TK1": 
                    for temt_modbus_addr in A01_Para.ht_t_modbus_addr:
                        if str(temv_prot_element.attmain_id) == str(temt_modbus_addr.attelement_id) and str(temv_prot_element.attprot_id) == str(temt_modbus_addr.attprotocol_id):                         
                            temCom = temt_modbus_addr.attcom_port
                            temByte = temA01modbus.set_data_rtu(temCom,4001, 1, 1)                            
                elif temv_prot_element.attprot_name == "A01_88H5UT": 
                    for temt_modbus_addr in A01_Para.ht_t_modbus_addr:
                        if str(temv_prot_element.attmain_id) == str(temt_modbus_addr.attelement_id) and str(temv_prot_element.attprot_id) == str(temt_modbus_addr.attprotocol_id):                         
                            temCom = temt_modbus_addr.attcom_port
                            hex_r.rec_byte = temA01modbus.get_data_com_hex(temCom,"0110120000010200015591")
                            hex_r.A01_88H5UT(1)
                elif temv_prot_element.attprot_name == "A01_99N4T3": 
                    for temt_modbus_addr in A01_Para.ht_t_modbus_addr:
                        if str(temv_prot_element.attmain_id) == str(temt_modbus_addr.attelement_id) and str(temv_prot_element.attprot_id) == str(temt_modbus_addr.attprotocol_id):                         
                            temCom = temt_modbus_addr.attcom_port
                            hex_r.rec_byte = temA01modbus.get_data_com_hex(temCom,"0110000000020200010001EBAF")
                            hex_r.error_desc = "TP做样结果:"+str(hex_r.rec_byte)
                elif temv_prot_element.attprot_name == "A01_11N55T": 
                    for temt_modbus_addr in A01_Para.ht_t_modbus_addr:
                        if str(temv_prot_element.attmain_id) == str(temt_modbus_addr.attelement_id) and str(temv_prot_element.attprot_id) == str(temt_modbus_addr.attprotocol_id):                         
                            temCom = temt_modbus_addr.attcom_port
                            hex_r.rec_byte = temA01modbus.get_data_com_hex(temCom,"0110120000010200015591")
                            hex_r.error_desc = "COD做样结果:"+str(hex_r.rec_byte)
                elif temv_prot_element.attprot_name == "A01_KKVVT3": 
                    for temt_modbus_addr in A01_Para.ht_t_modbus_addr:
                        if str(temv_prot_element.attmain_id) == str(temt_modbus_addr.attelement_id) and str(temv_prot_element.attprot_id) == str(temt_modbus_addr.attprotocol_id):                         
                            temCom = temt_modbus_addr.attcom_port
                            hex_r.rec_byte = temA01modbus.get_data_com_hex(temCom,"0110120000010200015591")
                            hex_r.error_desc = "氨氮做样结果:"+str(hex_r.rec_byte)
                elif temv_prot_element.attprot_name == "A01_00T6L5": 
                    for temt_modbus_addr in A01_Para.ht_t_modbus_addr:
                        if str(temv_prot_element.attmain_id) == str(temt_modbus_addr.attelement_id) and str(temv_prot_element.attprot_id) == str(temt_modbus_addr.attprotocol_id):                         
                            temCom = temt_modbus_addr.attcom_port
                            hex_r.rec_byte = temA01modbus.get_data_com_hex(temCom,"060600C80001C843")
                            hex_r.error_desc = "总铬做样结果:"+str(hex_r.rec_byte)
                elif temv_prot_element.attprot_name == "A01_MKK4A4":
                    for temt_modbus_addr in A01_Para.ht_t_modbus_addr:
                        if str(temv_prot_element.attmain_id) == str(temt_modbus_addr.attelement_id) and str(temv_prot_element.attprot_id) == str(temt_modbus_addr.attprotocol_id):                         
                            temCom = temt_modbus_addr.attcom_port
                            hex_r.rec_byte = temA01modbus.get_data_com_hex(temCom,"0110000000020200010001EBAF")
                            hex_r.error_desc = "TOC-4200做样结果:"+str(hex_r.rec_byte)
                    pass
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            pass