#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import json
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.plugins.A01_IHGDW0 import A01_Para
from com.zxy.z_debug import z_debug
from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.zxy.common.Com_Fun import Com_Fun
class A01_905MFA_CON_IN(z_debug):
    strContinue      = "1"   
    strResult        = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    def __init__(self):
        pass
    def init_start(self):
        temOpd = Ope_DB_Cent()
        if temOpd.Get_eova_user_rid(Com_Fun.getLogin_Id(self.session_id)) > 3:
            self.strResult = "{\""+self.param_name+"\": [{\""+Com_Fun.GetLowUpp("s_result")+"\": 0, \""+Com_Fun.GetLowUpp("error_desc")+"\": \"禁止操作，用户权限不足!\"}]}"
        else:
            jso = {}
            jsary1 = []
            for v_prot_ele in A01_Para.ht_v_prot_element:
                temjso1 = {}
                temFlag = False
                for A01_9ary in A01_Para.A01_905MFA_ARY:
                    if v_prot_ele.attprot_name == A01_9ary:
                        temFlag = True
                    if temFlag == True:
                        temjso1[Com_Fun.GetLowUpp("prot_name")] = v_prot_ele.attprot_name
                        temjso1[Com_Fun.GetLowUpp("element_name")] = v_prot_ele.attelement_name           
                        jsary1.append(temjso1)
                        break
            jso[self.param_name] = jsary1
            self.strResult = json.dumps(jso,ensure_ascii=False)
            self.strContinue = "0"  
