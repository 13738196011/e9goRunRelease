// 参数表t_param_value
var data1 = [];
// modbus地址表t_modbus_addr
var data2 = [];
// 上传平台表t_server_ip
var data3 = [];
// 上传协议表t_up_protocol
var data4 = [];
// 采集协议因子关联表t_prot_ele_link
var data5 = [];
// 采集协议表t_protocol
var data6 = [];
// 因子表数据t_code_element
var data7 = [];
// 串口设置t_com_port
var data8 = [];
var selectRowId1 = "";
var selectRowId2 = "";
var selectRowId3 = "";
var selectRowId4 = "";
var selectRowId5 = "";
var selectRowId6 = "";
var selectRowId7 = "";
var selectRowId8 = "";
var param_name1 = 'D01_query_t_param_value';
var param_name_add1 = 'D01_add_t_param_value';
var param_name_edit1 = 'D01_edit_t_param_value';
var param_name_delete1 = 'D01_delete_t_param_value';
var param_name2 = 'D01_query_t_modbus_addr';
var param_name_add2 = 'D01_add_t_modbus_addr';
var param_name_edit2 = 'D01_edit_t_modbus_addr';
var param_name_delete2 = 'D01_delete_t_modbus_addr';
var param_name3 = 'D01_query_t_server_ip';
var param_name_add3 = 'D01_add_t_server_ip';
var param_name_edit3 = 'D01_edit_t_server_ip';
var param_name_delete3 = 'D01_delete_t_server_ip';
var param_name4 = 'D01_query_t_up_protocol';
var param_name_add4 = 'D01_add_t_up_protocol';
var param_name_edit4 = 'D01_edit_t_up_protocol';
var param_name_delete4 = 'D01_delete_t_up_protocol';
var param_name5 = 'D01_query_t_prot_ele_link';
var param_name_add5 = 'D01_add_t_prot_ele_link';
var param_name_edit5 = 'D01_edit_t_prot_ele_link';
var param_name_delete5 = 'D01_delete_t_prot_ele_link';
var param_name6 = 'D01_query_t_protocol';
var param_name_add6 = 'D01_add_t_protocol';
var param_name_edit6 = 'D01_edit_t_protocol';
var param_name_delete6 = 'D01_delete_t_protocol';
var param_name7 = 'D01_query_t_code_element';
var param_name_add7 = 'D01_add_t_code_element';
var param_name_edit7 = 'D01_edit_t_code_element';
var param_name_delete7 = 'D01_delete_t_code_element';
var param_name8 = 'D01_select_t_com_port';
var param_name_add8 = 'D01_insert_t_com_port';
var param_name_edit8 = 'D01_update_t_com_port';
var param_name_delete8 = 'D01_delete_t_com_port';
var param_name_t_protocol_list = "D01_query_t_protocol_list";
var param_name_t_code_element_list = "D01_query_t_code_element_list";

var columns1 = [{
        title: '主键',
        field: 'main_id',
        sortable: true,
        visible: false
    }, {
        title: '参数key',
        field: 'param_key',
        sortable: true
    }, {
        title: '参数值',
        field: 'param_value',
        sortable: true
    }, {
        title: '参数描述',
        field: 'param_name',
        sortable: true
    }, {
        title: '系统时间',
        field: 'create_date',
        sortable: true
    }, {
        title: '备注',
        field: 's_desc',
        sortable: true
    }
];
var columns2 = [{
        title: '主键',
        field: 'main_id',
        sortable: true,
        visible: false
    }, {
        title: '设备地址',
        field: 'modbus_gateway',
        sortable: true
    }, {
        title: '串口号',
        field: 'com_port',
        sortable: true,
        formatter: get_t_com_port
    }, {
        title: '栈地址',
        field: 'modbus_addr',
        sortable: true
    }, {
        title: '起始位',
        field: 'modbus_begin',
        sortable: true
    }, {
        title: '长度',
        field: 'modbus_length',
        sortable: true
    }, {
        title: '因子名称',
        field: 'element_id',
        sortable: true,
        formatter: get_t_code_element
    }, {
        title: '采集协议模块',
        field: 'protocol_id',
        sortable: true,
        formatter: get_t_protocol
    }, {
        title: '系统时间',
        field: 'create_date',
        sortable: true,
        visible: false
    }, {
        title: '备注',
        field: 's_desc',
        sortable: true,
        visible: false
    }
];
var columns3 = [{
        title: '主键',
        field: 'main_id',
        sortable: true,
        visible: false
    }, {
        title: '上传平台IP',
        field: 'server_ip',
        sortable: true
    }, {
        title: '上传平台端口号',
        field: 'server_port',
        sortable: true
    }, {
        title: '上传协议名称',
        field: 'server_name',
        sortable: true
    }, {
        title: '上传协议模块',
        field: 'up_protocol_id',
        sortable: true,
        formatter: get_t_up_protocol
    }, {
        title: '系统时间',
        field: 'create_date',
        sortable: true
    }, {
        title: '上传标志位(不得超过整数10)',
        field: 's_desc',
        sortable: true,
        visible: false
    }
];
var columns4 = [{
        title: '主键',
        field: 'main_id',
        sortable: true,
        visible: false
    }, {
        title: '上传协议模块',
        field: 'prot_name',
        sortable: true
    }, {
        title: '上传协议名称',
        field: 'cn_name',
        sortable: true
    }, {
        title: '系统时间',
        field: 'create_date',
        sortable: true
    }, {
        title: '备注',
        field: 's_desc',
        sortable: true,
        visible: false
    }, {
        title: '状态',
        field: 's_state',
        sortable: true,
        formatter: get_s_state
    }
];
var columns5 = [{
        title: '主键',
        field: 'main_id',
        sortable: true,
        visible: false
    }, {
        title: '采集协议模块',
        field: 'prot_id',
        sortable: true,
        formatter: get_t_protocol
    }, {
        title: '因子名称',
        field: 'element_id',
        sortable: true,
        formatter: get_t_code_element
    }, {
        title: '仪器量程上限',
        field: 'prot_up_limit',
        sortable: true
    }, {
        title: '仪器量程下限',
        field: 'prot_down_limit',
        sortable: true
    }, {
        title: '系统时间',
        field: 'create_date',
        sortable: true
    }, {
        title: '备注',
        field: 's_desc',
        sortable: true,
        visible: false
    }
];
var columns6 = [{
        title: '主键',
        field: 'main_id',
        sortable: true,
        visible: false
    }, {
        title: '采集协议模块',
        field: 'prot_name',
        sortable: true
    }, {
        title: '采集协议名称',
        field: 'oem_name',
        sortable: true
    }, {
        title: '串口号',
        field: 'com_port',
        sortable: true,
        formatter: get_t_com_port
    }, {
        title: '系统时间',
        field: 'create_date',
        sortable: true
    }, {
        title: '备注',
        field: 's_desc',
        sortable: true,
        visible: false
    }, {
        title: '状态',
        field: 's_state',
        sortable: true,
        formatter: get_s_state
    }
];
var columns7 = [{
        title: '主键',
        field: 'main_id',
        sortable: true,
        visible: false
    }, {
        title: '因子编码',
        field: 'element_code',
        sortable: true
    }, {
        title: '因子名称',
        field: 'element_name',
        sortable: true
    }, {
        title: '存储位',
        field: 'element_val',
        sortable: true
    }, {
        title: '因子类型',
        field: 'element_type',
        sortable: true
    }, {
        title: '因子单位',
        field: 'element_unit',
        sortable: true
    }, {
        title: '小数点位',
        field: 'dot_num',
        sortable: true
    }, {
        title: '报警上限',
        field: 'up_level',
        sortable: true
    }, {
        title: '报警下限',
        field: 'down_level',
        sortable: true
    }, {
        title: '量程上限',
        field: 'up_limit',
        sortable: true
    }, {
        title: '量程下限',
        field: 'down_limit',
        sortable: true
    }, {
        title: '因子编码2',
        field: 'element_code2',
        sortable: true
    }, {
        title: '系统时间',
        field: 'create_date',
        sortable: true
    }, {
        title: '备注',
        field: 's_desc',
        sortable: true,
        visible: false
    }, {
        title: '状态',
        field: 's_state',
        sortable: true,
        formatter: get_s_state
    }
];
var columns8 = [{
        title: '主键',
        field: 'main_id',
        sortable: true,
        visible: false
    }, {
        title: '串口名称',
        field: 'com_name',
        sortable: true
    }, {
        title: '波特率',
        field: 'com_bot',
        sortable: true
    }, {
        title: '数据位',
        field: 'com_data',
        sortable: true
    }, {
        title: '奇偶校验',
        field: 'com_ct',
        sortable: true
    }, {
        title: '英文标签',
        field: 'com_eflag',
        sortable: true
    }, {
        title: '系统时间',
        field: 'create_date',
        sortable: true
    }, {
        title: '备注',
        field: 's_desc',
        sortable: true
    }
];

// 启用禁用
function get_s_state(value, row, index) {
    if (value == "1")
        return "启用";
    else if (value == "0")
        return "<font color='red'>禁用</font>";
}

function get_t_up_protocol(value, row, index) {
    objResult = value;
    for (i = 0; i < data4.length; i++) {
        obj = data4[i];
        if (obj.main_id.toString() == value.toString()) {
            objResult = obj.prot_name + "[" + obj.cn_name + "]";
            break;
        }
    }
    return objResult;
}

function get_t_protocol(value, row, index) {
    objResult = value;
    for (i = 0; i < data6.length; i++) {
        obj = data6[i];
        if (obj.main_id.toString() == value.toString()) {
            objResult = obj.prot_name + "[" + obj.oem_name + "]";
            break;
        }
    }
    return objResult;
}

function get_t_code_element(value, row, index) {
    objResult = value;
    for (i = 0; i < data7.length; i++) {
        obj = data7[i];
        if (obj.main_id.toString() == value.toString()) {
            objResult = obj.element_name;
            break;
        }
    }
    return objResult;
}

function get_t_com_port(value, row, index) {
    objResult = value;
    for (i = 0; i < data8.length; i++) {
        obj = data8[i];
        if (obj.com_eflag.toString() == value.toString()) {
            objResult = obj.com_eflag + "[" + obj.com_name + "]";
            break;
        }
    }
    return objResult;
}


function initTime() {
    $('#starttimeHour').datetimepicker({
        format: 'yyyy-mm-dd hh:00:00',
        startView: 2,
        todayHighlight: 1, // 今天高亮
        // initialDate:new Date(),
        autoclose: true,
        forceParse: 0, // 设置为0，时间不会跳转1899，会显示当前时间。
        language: 'zh-CN', // 显示中文
        minView: "day", // 设置只显示到月份
        todayBtn: true // 显示今日按钮
    });
    $('#endtimeHour').datetimepicker({
        format: 'yyyy-mm-dd hh:00:00',
        startView: 2,
        todayHighlight: 1, // 今天高亮
        // initialDate:new Date(),
        autoclose: true,
        forceParse: 0, // 设置为0，时间不会跳转1899，会显示当前时间。
        language: 'zh-CN', // 显示中文
        minView: "day", // 设置只显示到月份
        todayBtn: true // 显示今日按钮
    });
    $('#starttimeDay').datepicker({
        format: 'yyyy-mm-dd',
        startView: 0,
        maxViewMode: 0,
        minViewMode: 0,
        initialDate: new Date(),
        autoclose: true
    });
    $('#starttimeMonth').datepicker({
        format: 'yyyy-mm',
        startView: 1,
        maxViewMode: 1,
        minViewMode: 1,
        initialDate: '2019-01',
        autoclose: true
    });
    var today = new Date();
    today = today.getFullYear() + "-" + (today.getMonth() > 9 ? (today.getMonth() + 1) : "0" + (today.getMonth() + 1)) + "-" + (today.getDate() > 9 ? (today.getDate()) : "0" + (today.getDate()));
    var yesterday_time = (new Date).getTime() - 24 * 60 * 60 * 1000 * 365;
    var yesterday = new Date(yesterday_time);
    yesterday = yesterday.getFullYear() + "-" + (yesterday.getMonth() > 9 ? (yesterday.getMonth() + 1) : "0" + (yesterday.getMonth() + 1)) + "-" + (yesterday.getDate() > 9 ? (yesterday.getDate()) : "0" + (yesterday.getDate()));
    $("#starttimeHour").val(yesterday);
    $("#endtimeHour").val(today);
}

function initTable1() {
    $('#exampleTableEvents1').bootstrapTable('destroy');
    $("#exampleTableEvents1").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                // 已选中则移除 当前行的class='changeColor'
                // $($element).removeClass('changeColor');
                $('#exampleTableEvents1').find("tr.changeColor").removeClass('changeColor');
                selectRowId1 = "";
            } else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#exampleTableEvents1').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');
                　
                selectRowId1 = $element.attr('data-index');
            }
        },
        toolbar: "#exampleTableEvents1Toolbar",
        columns: columns1,
        data: data1,
        pageNumber: 1,
        pageSize: 30, // 每页的记录行数（*）
        pageList: [30, 100, 500, 2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
}

function s_param_name1(input){
    layer.close(ly_index);
	data1 = input[param_name1];
    initTable1();
}

function query1() {
    $('#exampleTableEvents1').bootstrapTable("removeAll");
    var inputdata = {
            "param_name": param_name1,
            "session_id": session_id,
            "login_id": login_id
        };
    get_ajax_baseurl(inputdata, "s_param_name1");
}

$('#btn_query1').click(function () {
    query1();
})

$("#cancelAdd1").on('click', function () {
    $("#addDataModal1").find('input').val('');
})

function s_get_param_name_add1(input)
{
    layer.close(ly_index);
	if (input[param_name_add1][0]["s_result"] == 1) {
        swal("新增成功");
        query1();
    } else if (input[param_name_add1][0]["s_result"] == 0) {
        swal("新增失败");
    }
}

$("#saveAdd1").on('click', function () {
    var param_key = $("#add1_param_key").val();
    var param_value = $("#add1_param_value").val();
    var param_name = $("#add1_param_name").val();
    var create_date = $("#add1_create_date").val();
    var s_desc = $("#add1_s_desc").val();
    if (typeof(data1) == "undefined") {
        data1 = [];
    }
    var inputdata = {
            "param_name": param_name_add1,
            "session_id": session_id,
            "login_id": login_id,
            "param_value1": param_key,
            "param_value2": param_value,
            "param_value3": param_name,
            "param_value4": create_date,
            "param_value5": s_desc
        };
    get_ajax_baseurl(inputdata, "s_get_param_name_add1");
})

$("#btn_add1").click(function () {
    $("#addData1").modal();
})

$('#btn_edit1').click(function () {
    if (selectRowId1 != "") {
        $("#editDataInfo1").modal();
        var rowData = $("#exampleTableEvents1").bootstrapTable('getData')[selectRowId1];
        var inputs = $("#editDataModal1").find('input');
        Object.keys(rowData).forEach(function (key) {
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].id == "edit1_" + key) {
                    $(inputs[i]).val(rowData[key]);
                }
            }
        });
    } else {
        alert('请选择数据行');
    }
})

function s_get_param_name_edit1(input) {
	layer.close(ly_index);
	if (input[param_name_edit1][0]["s_result"] == 1) {
        swal("修改成功");
        query1();
    } else if (input[param_name_edit1][0]["s_result"] == 0) {
        swal("修改失败");
    }
}

$("#saveEdit1").click(function () {
    var main_id = Number($("#edit1_main_id").val());
    var param_key = $("#edit1_param_key").val();
    var param_value = $("#edit1_param_value").val();
    var param_name = $("#edit1_param_name").val();
    var create_date = $("#edit1_create_date").val();
    var s_desc = $("#edit1_s_desc").val();
    
    var inputdata = {
            "param_name": param_name_edit1,
            "session_id": session_id,
            "login_id": login_id,
            "param_value1": param_key,
            "param_value2": param_value,
            "param_value3": param_name,
            "param_value4": create_date,
            "param_value5": s_desc,
            "param_value6": main_id
        };
        get_ajax_baseurl(inputdata, "s_get_param_name_edit1");
})

$("#cancelEdit1").click(function () {
    $("#editDataModal1").find('input').val('')
})

$('#btn_delete1').click(function () {
    if (selectRowId1 != "") {
        $("#deleteData1").modal();
    } else {
        alert('请选择数据行');
    }
})

function s_get_param_name_delete1(input) {
    layer.close(ly_index);
    if (input[param_name_delete1][0]["s_result"] == 1) {
        swal("删除成功");
        query1();
    } else if (input[param_name_delete1][0]["s_result"] == 0) {
        swal("删除失败");
    }
}

$('#delete1').click(function () {
    var rowData = $("#exampleTableEvents1").bootstrapTable('getData')[selectRowId1];
    var delete1_main_id = rowData["main_id"];
    
    var inputdata = {
            "param_name": param_name_delete1,
            "session_id": session_id,
            "login_id": login_id,
            "param_value1": delete1_main_id
        };
    get_ajax_baseurl(inputdata, "s_get_param_name_delete1");
})

$('#btn_upload1').click(function () {
    var postData = {};
    postData[file_name] = data;
    var formData = new FormData();
    formData.append(file_name + '.dat', JSON.stringify(postData));
    get_ajax_upload(formData, "get_upload_file");
})

function initTable2() {
    $('#exampleTableEvents2').bootstrapTable('destroy');
    $("#exampleTableEvents2").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#exampleTableEvents2').find("tr.changeColor").removeClass('changeColor');
                selectRowId2 = "";
            } else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#exampleTableEvents2').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');
                　
                selectRowId2 = $element.attr('data-index');
            }
        },
        toolbar: "#exampleTableEvents2Toolbar",
        columns: columns2,
        data: data2,
        pageNumber: 1,
        pageSize: 30, // 每页的记录行数（*）
        pageList: [30, 100, 500, 2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
}

function s_param_name2(input) {
    layer.close(ly_index);
    data2 = input[param_name2];
    initTable2();
}

function query2() {
    $('#exampleTableEvents2').bootstrapTable("removeAll");
    var inputdata = {
            "param_name": param_name2,
            "session_id": session_id,
            "login_id": login_id
        };
    get_ajax_baseurl(inputdata, "s_param_name2");
}

$('#btn_query2').click(function () {
    query2();
})

$("#cancelAdd2").on('click', function () {
    $("#addDataModal2").find('input').val('');
})

function s_param_name_add2(input) {
    layer.close(ly_index);
    if (input[param_name_add2][0]["s_result"] == 1) {
        swal("新增成功");
        query2();
    } else if (input[param_name_add2][0]["s_result"] == 0) {
        swal("新增失败");
    }
}

$("#saveAdd2").on('click', function () {
    var main_id = 0;
    var modbus_gateway = $("#add2_modbus_gateway").val();
	var com_port = ($("#add2_com_port").val() == null) ? ('') : ($("#add2_com_port").val()[0]);
    var modbus_addr = $("#add2_modbus_addr").val();
    var modbus_begin = $("#add2_modbus_begin").val();
    var modbus_length = $("#add2_modbus_length").val();
    var element_id = ($("#add2_element_id").val() == null) ? ('') : ($("#add2_element_id").val()[0]);
    var protocol_id = ($("#add2_protocol_id").val() == null) ? ('') : ($("#add2_protocol_id").val()[0]);
    var create_date = $("#add2_create_date").val();
    var s_desc = $("#add2_s_desc").val();
    if (typeof(data2) == "undefined") {
        data2 = [];
    }    
    var inputdata = {
            "param_name": param_name_add2,
            "session_id": session_id,
            "login_id": login_id,
            "param_value1": modbus_gateway,
            "param_value2": com_port,
            "param_value3": modbus_addr,
            "param_value4": modbus_begin,
            "param_value5": modbus_length,
            "param_value6": element_id,
            "param_value7": protocol_id,
            "param_value8": create_date,
            "param_value9": s_desc
        };
    get_ajax_baseurl(inputdata, "s_param_name_add2");
})

function s_param_name8_add2(input){
	layer.close(ly_index);
	data = input[param_name8];
	for (var i = 0; i < data.length; i++) {
		addOptionValue("add2_com_port", data[i].com_eflag, data[i].com_eflag + "[" + data[i].com_name + "]");
	}
	addOptionValue("add2_com_port", "", "");
}

function s_param_name_t_code_element_list_add2(input) {
    layer.close(ly_index);
    var data = input[param_name_t_code_element_list];
    for (var i = 0; i < data.length; i++) {
        addOptionValue("add2_element_id", data[i].main_id, data[i].element_name);
    }
	var inputdata = {
		        "param_name": param_name8,
		        "session_id": session_id,
		        "login_id": login_id
		    };
	get_ajax_baseurl(inputdata, "s_param_name8_add2");
}

function s_param_name_t_protocol_list_add2(input) {
    layer.close(ly_index);
    var data = input[param_name_t_protocol_list];
    for (var i = 0; i < data.length; i++) {
        addOptionValue("add2_protocol_id", data[i].main_id, data[i].oem_name);
    }
    var inputdata = {
	        "param_name": param_name_t_code_element_list,
	        "session_id": session_id,
	        "login_id": login_id
	    };
    get_ajax_baseurl(inputdata, "s_param_name_t_code_element_list_add2");    
}

$("#btn_add2").click(function () {
	var inputdata = {
		        "param_name": param_name_t_protocol_list,
		        "session_id": session_id,
		        "login_id": login_id
		    };
	get_ajax_baseurl(inputdata, "s_param_name_t_protocol_list_add2");
    $("#addData2").modal();
})

function s_param_name8_edit2(input){
	layer.close(ly_index);
	data = input[param_name8];
	for (var i = 0; i < data.length; i++) {
		addOptionValue("edit2_com_port", data[i].com_eflag, data[i].com_eflag + "[" + data[i].com_name + "]");
	}
	addOptionValue("edit2_com_port", "", "");
	var rowData = $("#exampleTableEvents2").bootstrapTable('getData')[selectRowId2];
	var inputs = $("#editDataModal2").find('input');
	var selects = $("#editDataModal2").find("select");
	Object.keys(rowData).forEach(function (key) {
		for (var i = 0; i < inputs.length; i++) {
			if (inputs[i].id == "edit2_" + key) {
				$(inputs[i]).val(rowData[key]);
			}
		}
		for (var j = 0; j < selects.length; j++) {
			if (selects[j].id == "edit2_" + key) {
				$(selects[j]).val(rowData[key]);
			}
		}
	});	
}

function s_param_name_t_code_element_list_edit2(input){
	layer.close(ly_index);
	var data = input[param_name_t_code_element_list];
	for (var i = 0; i < data.length; i++) {
		addOptionValue("edit2_element_id", data[i].main_id, data[i].element_name);
	}
	$("#editDataInfo2").modal();
	var inputdata = {
		        "param_name": param_name8,
		        "session_id": session_id,
		        "login_id": login_id
		    };
	get_ajax_baseurl(inputdata, "s_param_name8_edit2");
}

function s_param_name_t_protocol_list_edit2(input) {
    layer.close(ly_index);
    var data = input[param_name_t_protocol_list];
    for (var i = 0; i < data.length; i++) {
        addOptionValue("edit2_protocol_id", data[i].main_id, data[i].oem_name);
    }
	var inputdata = {
		        "param_name": param_name_t_code_element_list,
		        "session_id": session_id,
		        "login_id": login_id
		    };
	get_ajax_baseurl(inputdata, "s_param_name_t_code_element_list_edit2");
}

$('#btn_edit2').click(function () {
    if (selectRowId2 != "") {
    	var inputdata = {
    		        "param_name": param_name_t_protocol_list,
    		        "session_id": session_id,
    		        "login_id": login_id
    		    };
    	 get_ajax_baseurl(inputdata, "s_param_name_t_protocol_list_edit2");
    } else {
        alert('请选择数据行');
    }
})

function s_get_param_name_edit2(input) {
    layer.close(ly_index);
    if (input[param_name_edit2][0]["s_result"] == 1) {
        swal("修改成功");
        query2();
    } else if (input[param_name_edit2][0]["s_result"] == 0) {
        swal("修改失败");
    }
}

$("#saveEdit2").click(function () {
    var main_id = Number($("#edit2_main_id").val());
    var modbus_gateway = $("#edit2_modbus_gateway").val();
	var com_port = ($("#edit2_com_port").val() == null) ? ('') : ($("#edit2_com_port").val()[0]);
    var modbus_addr = $("#edit2_modbus_addr").val();
    var modbus_begin = $("#edit2_modbus_begin").val();
    var modbus_length = $("#edit2_modbus_length").val();
    var element_id = ($("#edit2_element_id").val() == null) ? ('') : ($("#edit2_element_id").val()[0]);
    var protocol_id = ($("#edit2_protocol_id").val() == null) ? ('') : ($("#edit2_protocol_id").val()[0]);
    var create_date = $("#edit2_create_date").val();
    var s_desc = $("#edit2_s_desc").val();
	var inputdata = {
        "param_name": param_name_edit2,
        "session_id": session_id,
        "login_id": login_id,
        "param_value1": modbus_gateway,
        "param_value2": com_port,
        "param_value3": modbus_addr,
        "param_value4": modbus_begin,
        "param_value5": modbus_length,
        "param_value6": element_id,
		"param_value7": protocol_id,
		"param_value8": create_date,
		"param_value9": s_desc,
		"param_value10": main_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name_edit2");
})

$("#cancelEdit2").click(function () {
    $("#editDataModal2").find('input').val('')
})

$('#btn_delete2').click(function () {
    if (selectRowId2 != "") {
        $("#deleteData2").modal();
    } else {
        alert('请选择数据行');
    }
})

function s_get_param_name_delete2(input) {
    layer.close(ly_index);
    if (input[param_name_delete2][0]["s_result"] == 1) {
        swal("删除成功");
        query2();
    } else if (input[param_name_delete2][0]["s_result"] == 0) {
        swal("删除失败");
    }
}

$('#delete2').click(function () {
    var rowData = $("#exampleTableEvents2").bootstrapTable('getData')[selectRowId2];
    var delete2_main_id = rowData["main_id"];
    var inputdata = {
        "param_name": param_name_delete2,
        "session_id": session_id,
        "login_id": login_id,
        "param_value1": delete2_main_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name_delete2");
})

$('#btn_upload2').click(function () {
    var postData = {};
    postData[file_name] = data;
    var formData = new FormData();
    formData.append(file_name + '.dat', JSON.stringify(postData));
    get_ajax_upload(formData, "get_upload_file");
})

function initTable3() {
    $('#exampleTableEvents3').bootstrapTable('destroy');
    $("#exampleTableEvents3").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                // 已选中则移除 当前行的class='changeColor'
                // $($element).removeClass('changeColor');
                $('#exampleTableEvents3').find("tr.changeColor").removeClass('changeColor');
                selectRowId3 = "";
            }
            　　　　　　else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#exampleTableEvents3').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');
                　
                selectRowId3 = $element.attr('data-index');
            }
        },
        toolbar: "#exampleTableEvents3Toolbar",
        columns: columns3,
        data: data3,
        pageNumber: 1,
        pageSize: 30, // 每页的记录行数（*）
        pageList: [30, 100, 500, 2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
}

function s_param_name3(input) {
    layer.close(ly_index);
    data3 = input[param_name3];
    initTable3();
}

function query3() {
    $('#exampleTableEvents3').bootstrapTable("removeAll");
	var inputdata = {
            "param_name": param_name3,
            "session_id": session_id,
            "login_id": login_id
        };
    get_ajax_baseurl(inputdata, "s_param_name3");
}

$('#btn_query3').click(function () {
    query3();
})

$("#cancelAdd3").on('click', function () {
    $("#addDataModal3").find('input').val('');
})

function s_param_name_add3(input) {
	layer.close(ly_index);
	if (input[param_name_add3][0]["s_result"] == 1) {
        swal("新增成功");
        query3();
    } else if (input[param_name_add3][0]["s_result"] == 0) {
        swal("新增失败");
    }
}

$("#saveAdd3").on('click', function () {
    var main_id = 0;
    var server_ip = $("#add3_server_ip").val();
    var server_port = $("#add3_server_port").val();
    var server_name = $("#add3_server_name").val();
    var up_protocol_id = ($("#add3_up_protocol_id").val() == null) ? ('') : ($("#add3_up_protocol_id").val()[0]);
    var create_date = $("#add3_create_date").val();
    var s_desc = $("#add3_s_desc").val();
    if (typeof(data3) == "undefined") {
        data3 = [];
    }
	var inputdata = {
		"param_name": param_name_add3,
		"session_id": session_id,
		"login_id": login_id,
		"param_value1": server_ip,
		"param_value2": server_port,
		"param_value3": server_name,
		"param_value4": up_protocol_id,
		"param_value5": create_date,
		"param_value6": s_desc
	}
	get_ajax_baseurl(inputdata, "s_param_name_add3");
})

function s_param_name4_add3(input){
	layer.close(ly_index);
	var data = input[param_name4];
	for (var i = 0; i < data.length; i++) {
		addOptionValue("add3_up_protocol_id", data[i].main_id, data[i].prot_name + "[" + data[i].cn_name + "]");
	}
}

$("#btn_add3").click(function () {
	var inputdata = {
		"param_name": param_name4,
		"session_id": session_id,
		"login_id": login_id
	}
	get_ajax_baseurl(inputdata, "s_param_name4_add3");
    $("#addData3").modal();
})

function s_param_name4_edit3(inuput){
	layer.close(ly_index);
	var data = inuput[param_name4];
	for (var i = 0; i < data.length; i++) {
		addOptionValue("edit3_up_protocol_id", data[i].main_id, data[i].prot_name + "[" + data[i].cn_name + "]");
	}
	$("#editDataInfo3").modal();
	var rowData = $("#exampleTableEvents3").bootstrapTable('getData')[selectRowId3];
	var inputs = $("#editDataModal3").find('input');
	var selects = $("#editDataModal3").find("select");
	Object.keys(rowData).forEach(function (key) {
		for (var i = 0; i < inputs.length; i++) {
			if (inputs[i].id == "edit3_" + key) {
				$(inputs[i]).val(rowData[key]);
			}
		}
		for (var j = 0; j < selects.length; j++) {
			if (selects[j].id == "edit3_" + key) {
				$(selects[j]).val(rowData[key]);
			}
		}
	});
}

$('#btn_edit3').click(function () {
    if (selectRowId3 != "") {
		var inputdata = {
			"param_name": param_name4,
			"session_id": session_id,
			"login_id": login_id
		};
		get_ajax_baseurl(inputdata, "s_param_name4_edit3");
    } else {
        alert('请选择数据行');
    }
})

function s_get_param_name_edit3(input) {
    layer.close(ly_index);
    if (input[param_name_edit3][0]["s_result"] == 1) {
        swal("修改成功");
        query3();
    } else if (input[param_name_edit3][0]["s_result"] == 0) {
        swal("修改失败");
    }
}

$("#saveEdit3").click(function () {
    var main_id = Number($("#edit3_main_id").val());
    var server_ip = $("#edit3_server_ip").val();
    var server_port = $("#edit3_server_port").val();
    var server_name = $("#edit3_server_name").val();
    var up_protocol_id = ($("#edit3_up_protocol_id").val() == null) ? ('') : ($("#edit3_up_protocol_id").val()[0]);
    var create_date = $("#edit3_create_date").val();
    var s_desc = $("#edit3_s_desc").val();
    var inputdata = {
        "param_name": param_name_edit3,
        "session_id": session_id,
        "login_id": login_id,
        "param_value1": server_ip,
        "param_value2": server_port,
        "param_value3": server_name,
        "param_value4": up_protocol_id,
        "param_value5": create_date,
        "param_value6": s_desc,
		"param_value7": main_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name_edit3");
})

$("#cancelEdit3").click(function () {
    $("#editDataModal3").find('input').val('')
})

$('#btn_delete3').click(function () {
    if (selectRowId3 != "") {
        $("#deleteData3").modal();
    } else {
        alert('请选择数据行');
    }
})

function s_get_param_name_delete3(input) {
    layer.close(ly_index);
    if (input[param_name_delete3][0]["s_result"] == 1) {
        swal("删除成功");
        query3();
    } else if (input[param_name_delete3][0]["s_result"] == 0) {
        swal("删除失败");
    }
}

$('#delete3').click(function () {
    var rowData = $("#exampleTableEvents3").bootstrapTable('getData')[selectRowId3];
    var delete3_main_id = rowData["main_id"];
	var inputdata = {
        "param_name": param_name_delete3,
        "session_id": session_id,
        "login_id": login_id,
        "param_value1": delete3_main_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name_delete3");
})

$('#btn_upload3').click(function () {
    var postData = {};
    postData[file_name] = data;
    var formData = new FormData();
    formData.append(file_name + '.dat', JSON.stringify(postData));
    get_ajax_upload(formData, "get_upload_file");
})

function initTable4() {
    $('#exampleTableEvents4').bootstrapTable('destroy');
    $("#exampleTableEvents4").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                // 已选中则移除 当前行的class='changeColor'
                // $($element).removeClass('changeColor');
                $('#exampleTableEvents4').find("tr.changeColor").removeClass('changeColor');
                selectRowId4 = "";
            }
            　　　　　　else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#exampleTableEvents4').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');
                　
                selectRowId4 = $element.attr('data-index');
            }
        },
        toolbar: "#exampleTableEvents4Toolbar",
        columns: columns4,
        data: data4,
        pageNumber: 1,
        pageSize: 30, // 每页的记录行数（*）
        pageList: [30, 100, 500, 2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
}

function s_param_name4(input) {
    layer.close(ly_index);
    data4 = input[param_name4];
    initTable4();
}

function query4() {
    $('#exampleTableEvents4').bootstrapTable("removeAll");
	var inputdata = {
            "param_name": param_name4,
            "session_id": session_id,
            "login_id": login_id
        };
    get_ajax_baseurl(inputdata, "s_param_name4");
}

$('#btn_query4').click(function () {
    query4();
})

$("#cancelAdd4").on('click', function () {
    $("#addDataModal4").find('input').val('');
})

function s_param_name_add4(input) {
	layer.close(ly_index);
	if (input[param_name_add4][0]["s_result"] == 1) {
        swal("新增成功");
        query4();
    } else if (input[param_name_add4][0]["s_result"] == 0) {
        swal("新增失败");
    }
}

$("#saveAdd4").on('click', function () {
    var main_id = 0;
    var prot_name = $("#add4_prot_name").val();
    var cn_name = $("#add4_cn_name").val();
    var create_date = $("#add4_create_date").val();
    var s_desc = $("#add4_s_desc").val();
    var s_state = $("#add4_s_state").val();
    if (typeof(data4) == "undefined") {
        data4 = [];
    }
    var inputdata = {
            "param_name": param_name_add4,
            "session_id": session_id,
            "login_id": login_id,
            "param_value1": prot_name,
            "param_value2": cn_name,
            "param_value3": create_date,
            "param_value4": s_desc,
            "param_value5": s_state
        };
    get_ajax_baseurl(inputdata, "s_param_name_add4");
})

$("#btn_add4").click(function () {
    $("#addData4").modal();
})

$('#btn_edit4').click(function () {
    if (selectRowId4 != "") {
        $("#editDataInfo4").modal();
        var rowData = $("#exampleTableEvents4").bootstrapTable('getData')[selectRowId4];
        var inputs = $("#editDataModal4").find('input');
        Object.keys(rowData).forEach(function (key) {
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].id == "edit4_" + key) {
                    $(inputs[i]).val(rowData[key]);
                }
            }
        });
    } else {
        alert('请选择数据行');
    }
})

function s_get_param_name_edit4(input) {
    layer.close(ly_index);
    if (input[param_name_edit4][0]["s_result"] == 1) {
        swal("修改成功");
        query4();
    } else if (input[param_name_edit4][0]["s_result"] == 0) {
        swal("修改失败");
    }
}

$("#saveEdit4").click(function () {
    var main_id = Number($("#edit4_main_id").val());
    var prot_name = $("#edit4_prot_name").val();
    var cn_name = $("#edit4_cn_name").val();
    var create_date = $("#edit4_create_date").val();
    var s_desc = $("#edit4_s_desc").val();
    var s_state = $("#edit4_s_state").val();
    var inputdata = {
        "param_name": param_name_edit4,
        "session_id": session_id,
        "login_id": login_id,
        "param_value1": prot_name,
        "param_value2": cn_name,
        "param_value3": create_date,
        "param_value4": s_desc,
        "param_value5": s_state,
        "param_value6": main_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name_edit4");
})

$("#cancelEdit4").click(function () {
    $("#editDataModal4").find('input').val('')
})

$('#btn_delete4').click(function () {
    if (selectRowId4 != "") {
        $("#deleteData4").modal();
    } else {
        alert('请选择数据行');
    }
})

function s_get_param_name_delete4(input) {
    layer.close(ly_index);
    if (input[param_name_delete4][0]["s_result"] == 1) {
        swal("删除成功");
        query4();
    } else if (input[param_name_delete4][0]["s_result"] == 0) {
        swal("删除失败");
    }
}

$('#delete4').click(function () {
    var rowData = $("#exampleTableEvents4").bootstrapTable('getData')[selectRowId4];
    var delete4_main_id = rowData["main_id"];
	var inputdata = {
        "param_name": param_name_delete4,
        "session_id": session_id,
        "login_id": login_id,
        "param_value1": delete4_main_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name_delete4");
})

$('#btn_upload4').click(function () {
    var postData = {};
    postData[file_name] = data;
    var formData = new FormData();
    formData.append(file_name + '.dat', JSON.stringify(postData));
    get_ajax_upload(formData, "get_upload_file");
})

function initTable5() {
    $('#exampleTableEvents5').bootstrapTable('destroy');
    $("#exampleTableEvents5").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                // 已选中则移除 当前行的class='changeColor'
                // $($element).removeClass('changeColor');
                $('#exampleTableEvents5').find("tr.changeColor").removeClass('changeColor');
                selectRowId5 = "";
            }
            　　　　　　else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#exampleTableEvents5').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');
                　
                selectRowId5 = $element.attr('data-index');
            }
        },
        toolbar: "#exampleTableEvents5Toolbar",
        columns: columns5,
        data: data5,
        pageNumber: 1,
        pageSize: 30, // 每页的记录行数（*）
        pageList: [30, 100, 500, 2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
}

function s_param_name5(input) {
	layer.close(ly_index);
	data5 = input[param_name5];
	initTable5();
}

function query5() {
    $('#exampleTableEvents5').bootstrapTable("removeAll");
	var inputdata = {
            "param_name": param_name5,
            "session_id": session_id,
            "login_id": login_id
        };
    get_ajax_baseurl(inputdata, "s_param_name5");
}

$('#btn_query5').click(function () {
    query5();
})

$("#cancelAdd5").on('click', function () {
    $("#addDataModal5").find('input').val('');
})

function s_param_name_add5(input){
	layer.close(ly_index);
	if (input[param_name_add5][0]["s_result"] == 1) {
        swal("新增成功");
        query5();
    } else if (input[param_name_add5][0]["s_result"] == 0) {
        swal("新增失败");
    }
}

$("#saveAdd5").on('click', function () {
    var prot_id = ($("#add5_prot_id").val() == null) ? ('') : ($("#add5_prot_id").val()[0]);
    var element_id = ($("#add5_element_id").val() == null) ? ('') : ($("#add5_element_id").val()[0]);
    var prot_up_limit = $("#add5_prot_up_limit").val();
    var prot_down_limit = $("#add5_prot_down_limit").val();
    var create_date = $("#add5_create_date").val();
    var s_desc = $("#add5_s_desc").val();
    if (typeof(data5) == "undefined") {
        data5 = [];
    }
	var inputdata = {
            "param_name": param_name_add5,
            "session_id": session_id,
            "login_id": login_id,
            "param_value1": prot_id,
            "param_value2": element_id,
            "param_value3": prot_up_limit,
            "param_value4": prot_down_limit,
            "param_value5": create_date,
            "param_value6": s_desc
        };
    get_ajax_baseurl(inputdata, "s_param_name_add5");
})

function s_param_name_t_code_element_list_add5(input){
	layer.close(ly_index);
	data = input[param_name_t_code_element_list];
	for (var i = 0; i < data.length; i++) {
		addOptionValue("add5_element_id", data[i].main_id, data[i].element_name);
	}
}

function s_param_name_t_protocol_list_add5(input){
    layer.close(ly_index);
    data = input[param_name_t_protocol_list];
	for (var i = 0; i < data.length; i++) {
		addOptionValue("add5_prot_id", data[i].main_id, data[i].oem_name);
	}
	var inputdata = {
		"param_name":param_name_t_code_element_list,
		"session_id": session_id,
		"login_id": login_id
	};
	get_ajax_baseurl(inputdata, "s_param_name_t_code_element_list_add5");
}

$("#btn_add5").click(function () {
	var inputdata = {
		"param_name": param_name_t_protocol_list,
			"session_id": session_id,
			"login_id": login_id
	}
	get_ajax_baseurl(inputdata, "s_param_name_t_protocol_list_add5");
    $("#addData5").modal();
})

function s_param_name_t_code_element_list_edit5(input){
    layer.close(ly_index);
    data = input[param_name_t_code_element_list];
	for (var i = 0; i < data.length; i++) {
		addOptionValue("edit5_element_id", data[i].main_id, data[i].element_name);
	}
	$("#editDataInfo5").modal();
	var rowData = $("#exampleTableEvents5").bootstrapTable('getData')[selectRowId5];
	var inputs = $("#editDataModal5").find('input');
	var selects = $("#editDataModal5").find("select");
	Object.keys(rowData).forEach(function (key) {
		for (var i = 0; i < inputs.length; i++) {
			if (inputs[i].id == "edit5_" + key) {
				$(inputs[i]).val(rowData[key]);
			}
		}
		for (var j = 0; j < selects.length; j++) {
			if (selects[j].id == "edit5_" + key) {
				$(selects[j]).val(rowData[key]);
			}
		}
	});
}

function s_param_name_t_protocol_list_edit5(input){
    layer.close(ly_index);
    data = input[param_name_t_protocol_list];
	for (var i = 0; i < data.length; i++) {
		addOptionValue("edit5_prot_id", data[i].main_id, data[i].oem_name);
	}
	var inputdata = {
		"param_name":param_name_t_code_element_list,
		"session_id": session_id,
		"login_id": login_id
	};
	get_ajax_baseurl(inputdata, "s_param_name_t_code_element_list_edit5");
}

$('#btn_edit5').click(function () {
    if (selectRowId5 != "") {
		var inputdata = {
			"param_name": param_name_t_protocol_list,
			"session_id": session_id,
			"login_id": login_id
		};
		get_ajax_baseurl(inputdata, "s_param_name_t_protocol_list_edit5");
	} else {
        alert('请选择数据行');
    }
})

function s_get_param_name_edit5(input) {
    layer.close(ly_index);
    if (input[param_name_edit5][0]["s_result"] == 1) {
        swal("修改成功");
        query5();
    } else if (input[param_name_edit5][0]["s_result"] == 0) {
        swal("修改失败");
    }
}

$("#saveEdit5").click(function () {
    var main_id = Number($("#edit5_main_id").val());
    var prot_id = ($("#edit5_prot_id").val() == null) ? ('') : ($("#edit5_prot_id").val()[0]);
    var element_id = ($("#edit5_element_id").val() == null) ? ('') : ($("#edit5_element_id").val()[0]);
    var prot_up_limit = $("#edit5_prot_up_limit").val();
    var prot_down_limit = $("#edit5_prot_down_limit").val();
    var create_date = $("#edit5_create_date").val();
    var s_desc = $("#edit5_s_desc").val();
    var inputdata = {
        "param_name": param_name_edit5,
        "session_id": session_id,
        "login_id": login_id,
        "param_value1": prot_id,
        "param_value2": element_id,
        "param_value3": prot_up_limit,
        "param_value4": prot_down_limit,
        "param_value5": create_date,
        "param_value6": s_desc,
        "param_value7": main_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name_edit5");
})

$("#cancelEdit5").click(function () {
    $("#editDataModal5").find('input').val('')
})

$('#btn_delete5').click(function () {
    if (selectRowId5 != "") {
        $("#deleteData5").modal();
    } else {
        alert('请选择数据行');
    }
})

function s_get_param_name_delete5(input) {
    layer.close(ly_index);
    if (input[param_name_delete5][0]["s_result"] == 1) {
        swal("删除成功");
        query5();
    } else if (input[param_name_delete5][0]["s_result"] == 0) {
        swal("删除失败");
    }
}

$('#delete5').click(function () {
    var rowData = $("#exampleTableEvents5").bootstrapTable('getData')[selectRowId5];
    var delete5_main_id = rowData["main_id"];
    var inputdata = {
        "param_name": param_name_delete5,
        "session_id": session_id,
        "login_id": login_id,
        "param_value1": delete5_main_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name_delete5");
})

$('#btn_upload5').click(function () {
    var postData = {};
    postData[file_name] = data;
    var formData = new FormData();
    formData.append(file_name + '.dat', JSON.stringify(postData));
    get_ajax_upload(formData, "get_upload_file");
})

function initTable6() {
    $('#exampleTableEvents6').bootstrapTable('destroy');
    $("#exampleTableEvents6").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                // 已选中则移除 当前行的class='changeColor'
                // $($element).removeClass('changeColor');
                $('#exampleTableEvents6').find("tr.changeColor").removeClass('changeColor');
                selectRowId6 = "";
            }
            　　　　　　else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#exampleTableEvents6').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                selectRowId6 = $element.attr('data-index');
            }
        },
        toolbar: "#exampleTableEvents6Toolbar",
        columns: columns6,
        data: data6,
        pageNumber: 1,
        pageSize: 30, // 每页的记录行数（*）
        pageList: [30, 100, 500, 2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
}

function s_param_name6(input) {
	layer.close(ly_index);
	data6 = input[param_name6];
	initTable6();
}

function query6() {
    $('#exampleTableEvents6').bootstrapTable("removeAll");
    var inputdata = {
            "param_name": param_name6,
            "session_id": session_id,
            "login_id": login_id
        };
    get_ajax_baseurl(inputdata, "s_param_name6");
}

$('#btn_query6').click(function () {
    query6();
})

$("#cancelAdd6").on('click', function () {
    $("#addDataModal6").find('input').val('');
})

function s_param_name_add6(input) {
	layer.close(ly_index);
	if (input[param_name_add6][0]["s_result"] == 1) {
        swal("新增成功");
        query6();
    } else if (input[param_name_add6][0]["s_result"] == 0) {
        swal("新增失败");
    }
    layer.close(ly_index);
}

$("#saveAdd6").on('click', function () {
    var prot_name = $("#add6_prot_name").val();
    var oem_name = $("#add6_oem_name").val();
    var com_port = ($("#add6_com_port").val() == null) ? ('') : ($("#add6_com_port").val()[0]);
    var create_date = $("#add6_create_date").val();
    var s_desc = $("#add6_s_desc").val();
    var s_state = $("#add6_s_state").val();
    if (typeof(data6) == "undefined") {
        data6 = [];
    }
    var inputdata = {
            "param_name": param_name_add6,
            "session_id": session_id,
            "login_id": login_id,
            "param_value1": prot_name,
            "param_value2": oem_name,
            "param_value3": com_port,
            "param_value4": create_date,
            "param_value5": s_desc,
            "param_value6": s_state
        };
    get_ajax_baseurl(inputdata, "s_param_name_add6");
})

function s_param_name8_add6(input){
	layer.close(ly_index);
	data = input[param_name8];
	for (var i = 0; i < data.length; i++) {
		addOptionValue("add6_com_port", data[i].com_eflag, data[i].com_eflag + "[" + data[i].com_name + "]");
	}       
	addOptionValue("add6_com_port", "","");
}

$("#btn_add6").click(function () {
    $("#addData6").modal();
	var inputdata = {
		        "param_name": param_name8,
		        "session_id": session_id,
		        "login_id": login_id
		    };
	get_ajax_baseurl(inputdata, "s_param_name8_add6");
})

function s_param_name8_edit6(input){
	layer.close(ly_index);
	data = input[param_name8];
	for (var i = 0; i < data.length; i++) {
		addOptionValue("edit6_com_port", data[i].com_eflag, data[i].com_eflag + "[" + data[i].com_name + "]");
	}
	addOptionValue("edit6_com_port", "", "");
	var rowData = $("#exampleTableEvents6").bootstrapTable('getData')[selectRowId6];
	var inputs = $("#editDataModal6").find('input');
	var selects = $("#editDataModal6").find("select");
	Object.keys(rowData).forEach(function (key) {
		for (var i = 0; i < inputs.length; i++) {
			if (inputs[i].id == "edit6_" + key) {
				$(inputs[i]).val(rowData[key]);
			}
		}
		for (var j = 0; j < selects.length; j++) {
			if (selects[j].id == "edit6_" + key) {
				$(selects[j]).val(rowData[key]);
			}
		}
	});
}

$('#btn_edit6').click(function () {
    if (selectRowId6 != "") {
        $("#editDataInfo6").modal();
		var inputdata = {
		        "param_name": param_name8,
		        "session_id": session_id,
		        "login_id": login_id
		    };
		get_ajax_baseurl(inputdata, "s_param_name8_edit6");
    } else {
        alert('请选择数据行');
    }
})

function s_get_param_name_edit6(input) {
    layer.close(ly_index);
    if (input[param_name_edit6][0]["s_result"] == 1) {
        swal("修改成功");
        query6();
    } else if (input[param_name_edit6][0]["s_result"] == 0) {
        swal("修改失败");
    }
}

$("#saveEdit6").click(function () {
    var main_id = Number($("#edit6_main_id").val());
    var prot_name = $("#edit6_prot_name").val();
	var oem_name = $("#edit6_oem_name").val();
	var com_port = ($("#edit6_com_port").val() == null) ? ('') : ($("#edit6_com_port").val()[0]);
	var create_date = $("#edit6_create_date").val();
	var s_desc = $("#edit6_s_desc").val();
	var s_state = $("#edit6_s_state").val();
    var inputdata = {
        "param_name": param_name_edit6,
        "session_id": session_id,
        "login_id": login_id,
        "param_value1": prot_name,
        "param_value2": oem_name,
        "param_value3": com_port,
        "param_value4": create_date,
        "param_value5": s_desc,
        "param_value6": s_state,
        "param_value7": main_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name_edit6");
})

$("#cancelEdit6").click(function () {
    $("#editDataModal6").find('input').val('')
})

$('#btn_delete6').click(function () {
    if (selectRowId6 != "") {
        $("#deleteData6").modal();
    } else {
        alert('请选择数据行');
    }
})

function s_get_param_name_delete6(input) {
    layer.close(ly_index);
    if (input[param_name_delete6][0]["s_result"] == 1) {
        swal("删除成功");
        query6();
    } else if (input[param_name_delete6][0]["s_result"] == 0) {
        swal("删除失败");
    }
}

$('#delete6').click(function () {
    var rowData = $("#exampleTableEvents6").bootstrapTable('getData')[selectRowId6];
    var delete6_main_id = rowData["main_id"];
    var inputdata = {
        "param_name": param_name_delete6,
        "session_id": session_id,
        "login_id": login_id,
        "param_value1": delete6_main_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name_delete6");
})

function initTable7() {
    $('#exampleTableEvents7').bootstrapTable('destroy');
    $("#exampleTableEvents7").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                // 已选中则移除 当前行的class='changeColor'
                // $($element).removeClass('changeColor');
                $('#exampleTableEvents7').find("tr.changeColor").removeClass('changeColor');
                selectRowId7 = "";
            }
            　　　　　　else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#exampleTableEvents7').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');
                　
                selectRowId7 = $element.attr('data-index');
            }
        },
        toolbar: "#exampleTableEvents7Toolbar",
        columns: columns7,
        data: data7,
        pageNumber: 1,
        pageSize: 30, // 每页的记录行数（*）
        pageList: [30, 100, 500, 2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
}

function s_param_name7(input) {
    layer.close(ly_index);
    data7 = input[param_name7];
    initTable7();
}

function query7() {
    $('#exampleTableEvents7').bootstrapTable("removeAll");
    var inputdata = {
        "param_name": param_name7,
        "session_id": session_id,
        "login_id": login_id
    };
    get_ajax_baseurl(inputdata, "s_param_name7");
}

$('#btn_query7').click(function () {
    query7();
})

$("#cancelAdd7").on('click', function () {
    $("#addDataModal7").find('input').val('');
})

function s_get_param_name_add7(input) {
    layer.close(ly_index);
    if (input[param_name_add7][0]["s_result"] == 1) {
        swal("新增成功");
        query7();
    } else if (input[param_name_add7][0]["s_result"] == 0) {
        swal("新增失败");
    }
}

$("#saveAdd7").on('click', function () {
    var inputdata = {
        "param_name": param_name_add7,
        "session_id": session_id,
        "login_id": login_id,
        "param_value1": $("#add7_element_code").val(),
        "param_value2": $("#add7_element_name").val(),
        "param_value3": $("#add7_element_val").val(),
        "param_value4": $("#add7_element_type").val(),
        "param_value5": $("#add7_element_unit").val(),
        "param_value6": $("#add7_dot_num").val(),
        "param_value7": $("#add7_up_level").val(),
        "param_value8": $("#add7_down_level").val(),
        "param_value9": $("#add7_up_limit").val(),
        "param_value10": $("#add7_down_limit").val(),
        "param_value11": $("#add7_element_code2").val(),
        "param_value12": $("#add7_create_date").val(),
        "param_value13": $("#add7_s_desc").val(),
        "param_value14": $("#add7_s_state").val()
    };
    get_ajax_baseurl(inputdata, "s_get_param_name_add7");
})

$("#btn_add7").click(function () {
    $("#addData7").modal();
})

$('#btn_edit7').click(function () {
    if (selectRowId7 != "") {
        $("#editDataInfo7").modal();
        var rowData = $("#exampleTableEvents7").bootstrapTable('getData')[selectRowId7];
        var inputs = $("#editDataModal7").find('input');
        Object.keys(rowData).forEach(function (key) {
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].id == "edit7_" + key) {
                    $(inputs[i]).val(rowData[key]);
                }
            }
        });
    } else {
        alert('请选择数据行');
    }
})

function s_get_param_name_edit7(input) {
    layer.close(ly_index);
    if (input[param_name_edit7][0]["s_result"] == 1) {
        swal("修改成功");
        query7();
    } else if (input[param_name_edit7][0]["s_result"] == 0) {
        swal("修改失败");
    }
}

$("#saveEdit7").click(function () {
    var inputdata = {
        "param_name": param_name_edit7,
        "session_id": session_id,
        "login_id": login_id,
        "param_value1": $("#edit7_element_code").val(),
        "param_value2": $("#edit7_element_name").val(),
        "param_value3": $("#edit7_element_val").val(),
        "param_value4": $("#edit7_element_type").val(),
        "param_value5": $("#edit7_element_unit").val(),
        "param_value6": $("#edit7_dot_num").val(),
        "param_value7": $("#edit7_up_level").val(),
        "param_value8": $("#edit7_down_level").val(),
        "param_value9": $("#edit7_up_limit").val(),
        "param_value10": $("#edit7_down_limit").val(),
        "param_value11": $("#edit7_element_code2").val(),
        "param_value12": $("#edit7_create_date").val(),
        "param_value13": $("#edit7_s_desc").val(),
        "param_value14": $("#edit7_s_state").val(),
        "param_value15": Number($("#edit7_main_id").val())
    };
    get_ajax_baseurl(inputdata, "s_get_param_name_edit7");
})

$("#cancelEdit7").click(function () {
    $("#editDataModal7").find('input').val('')
})

$('#btn_delete7').click(function () {
    if (selectRowId7 != "") {
        $("#deleteData7").modal();
    } else {
        alert('请选择数据行');
    }
})

function initTable8() {
    $('#exampleTableEvents8').bootstrapTable('destroy');
    $("#exampleTableEvents8").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                // 已选中则移除 当前行的class='changeColor'
                // $($element).removeClass('changeColor');
                $('#exampleTableEvents8').find("tr.changeColor").removeClass('changeColor');
                selectRowId8 = "";
            }
            　　　　　　else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#exampleTableEvents8').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');
                selectRowId8 = $element.attr('data-index');
            }
        },
        toolbar: "#exampleTableEvents8Toolbar",
        columns: columns8,
        data: data8,
        pageNumber: 1,
        pageSize: 30, // 每页的记录行数（*）
        pageList: [30, 100, 500, 2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
}

$('#btn_query8').click(function () {
    query8();
})

$("#cancelAdd8").on('click', function () {
    $("#addDataModal8").find('input').val('');
})

$("#btn_add8").click(function () {
    $("#addData8").modal();
})

$('#btn_edit8').click(function () {
    if (selectRowId8 != "") {
        $("#editDataInfo8").modal();
        var rowData = $("#exampleTableEvents8").bootstrapTable('getData')[selectRowId8];
        var inputs = $("#editDataModal8").find('input');
        Object.keys(rowData).forEach(function (key) {
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].id == "edit8_" + key) {
                    $(inputs[i]).val(rowData[key]);
                }
            }
        });
    } else {
        alert('请选择数据行');
    }
})

$("#cancelEdit8").click(function () {
    $("#editDataModal8").find('input').val('')
})

$('#btn_delete8').click(function () {
    if (selectRowId8 != "") {
        $("#deleteData8").modal();
    } else {
        swal('请选择数据行');
    }
})

function s_get_param_name_delete7(input) {
    layer.close(ly_index);
    if (input[param_name_delete7][0]["s_result"] == 1) {
        swal("删除成功");
        query7();
    } else if (input[param_name_delete7][0]["s_result"] == 0) {
        swal("删除失败");
    }
}

$('#delete7').click(function () {
    var rowData = $("#exampleTableEvents7").bootstrapTable('getData')[selectRowId7];
    var delete7_main_id = rowData["main_id"];
    var inputdata = {
        "param_name": param_name_delete7,
        "session_id": session_id,
        "login_id": login_id,
        "param_value1": delete7_main_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name_delete7");
})

$('#btn_upload6').click(function () {
    var postData = {};
    postData[file_name] = data;
    var formData = new FormData();
    formData.append(file_name + '.dat', JSON.stringify(postData));
    get_ajax_upload(formData, "get_upload_file");
})

$('#btn_upload7').click(function () {
    var postData = {};
    postData[file_name] = data;
    var formData = new FormData();
    formData.append(file_name + '.dat', JSON.stringify(postData));
    get_ajax_upload(formData, "get_upload_file");
})

function s_param_name8(input) {
    layer.close(ly_index);
    data8 = input[param_name8];
    initTable8();
}

function query8() {
    $('#exampleTableEvents8').bootstrapTable("removeAll");
    var inputdata = {
        "param_name": param_name8,
        "session_id": session_id,
        "login_id": login_id
    };
    get_ajax_baseurl(inputdata, "s_param_name8");
}

function s_get_param_name_add8(input) {
    layer.close(ly_index);
    if (input[param_name_add8][0]["s_result"] == 1) {
        swal("新增成功");
        query8();
    } else if (input[param_name_add8][0]["s_result"] == 0) {
        swal("新增失败");
    }
}

$("#saveAdd8").on('click', function () {
    var inputdata = {
        "param_name": param_name_add8,
        "session_id": session_id,
        "login_id": login_id,
        "param_value1": $("#add8_com_name").val(),
        "param_value2": $("#add8_com_bot").val(),
        "param_value3": $("#add8_com_data").val(),
        "param_value4": $("#add8_com_ct").val(),
        "param_value5": $("#add8_com_eflag").val(),
        "param_value6": $("#add8_s_desc").val(),
        "param_value7": $("#add8_create_date").val()
    };
    get_ajax_baseurl(inputdata, "s_get_param_name_add8");
})

function s_get_param_name_edit8(input) {
    layer.close(ly_index);
    if (input[param_name_edit8][0]["s_result"] == 1) {
        swal("修改成功");
        query8();
    } else if (input[param_name_edit8][0]["s_result"] == 0) {
        swal("修改失败");
    }
}

$("#saveEdit8").click(function () {
    var inputdata = {
        "param_name": param_name_edit8,
        "session_id": session_id,
        "login_id": login_id,
        "param_value1": $("#edit8_com_name").val(),
        "param_value2": $("#edit8_com_bot").val(),
        "param_value3": $("#edit8_com_data").val(),
        "param_value4": $("#edit8_com_ct").val(),
        "param_value5": $("#edit8_com_eflag").val(),
        "param_value6": $("#edit8_s_desc").val(),
        "param_value7": $("#edit8_create_date").val(),
        "param_value8": Number($("#edit8_main_id").val())
    };
    get_ajax_baseurl(inputdata, "s_get_param_name_edit8");
})

function s_get_param_name_delete8(input) {
    layer.close(ly_index);
    var data = input[param_name_delete8]
        if (data[0]["s_result"] == 1) {
            swal("删除成功");
            query8();
        } else if (data[0]["s_result"] == 0) {
            swal("删除失败");
        }
}

$('#delete8').click(function () {
    var rowData = $("#exampleTableEvents8").bootstrapTable('getData')[selectRowId8];
    var delete8_main_id = rowData["main_id"];
    var inputdata = {
        "param_name": param_name_delete8,
        "session_id": session_id,
        "login_id": login_id,
        "param_value1": delete8_main_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name_delete8");
})

function get_upload_file(input) {
    layer.close(ly_index);
}

$('#btn_upload8').click(function () {
    var postData = {};
    postData[file_name] = data;
    var formData = new FormData();
    formData.append(file_name + '.dat', JSON.stringify(postData));
    get_ajax_upload(formData, "get_upload_file");
})

function s_get_param_name5(input) {
    data5 = input[param_name5];
    initTable5();
    layer.close(ly_index);
}

function s_get_param_name2(input) {
    data2 = input[param_name2];
    initTable2();
    layer.close(ly_index);
    $('#exampleTableEvents5').bootstrapTable("removeAll");
    var inputdata = {
        "param_name": param_name5,
        "session_id": session_id,
        "login_id": login_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name5");
}

function query_tab2() {
    $('#exampleTableEvents2').bootstrapTable("removeAll");
    var inputdata = {
        "param_name": param_name2,
        "session_id": session_id,
        "login_id": login_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name2");
}

function s_get_param_name3(input) {
    data3 = input[param_name3];
    initTable3();
    layer.close(ly_index);
}

function s_get_param_name4(input) {
    data4 = input[param_name4];
    initTable4();
    layer.close(ly_index);
    $('#exampleTableEvents3').bootstrapTable("removeAll");
    var inputdata = {
        "param_name": param_name3,
        "session_id": session_id,
        "login_id": login_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name3");
}

function query_tab3() {
    $('#exampleTableEvents4').bootstrapTable("removeAll");
    var inputdata = {
        "param_name": param_name4,
        "session_id": session_id,
        "login_id": login_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name4");
}

function s_get_param_name7(input) {
    data7 = input[param_name7];
    initTable7();
    layer.close(ly_index);
}

function s_get_param_name6(input) {
    data6 = input[param_name6];
    initTable6();
    layer.close(ly_index);
    $('#exampleTableEvents7').bootstrapTable("removeAll");
    var inputdata = {
        "param_name": param_name7,
        "session_id": session_id,
        "login_id": login_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name7");
}

function query_tab4() {
    $('#exampleTableEvents6').bootstrapTable("removeAll");
    var inputdata = {
        "param_name": param_name6,
        "session_id": session_id,
        "login_id": login_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name6");
}

function s_get_param_name8(input) {
    data8 = input[param_name8];
    initTable8();
    layer.close(ly_index);
}

function s_get_param_name1(input) {
    data1 = input[param_name1];
    initTable1();
    layer.close(ly_index);
    $('#exampleTableEvents8').bootstrapTable("removeAll");
    var inputdata = {
        "param_name": param_name8,
        "session_id": session_id,
        "login_id": login_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name8");
}

function query_tab1() {
    $('#exampleTableEvents1').bootstrapTable("removeAll");
    var inputdata = {
        "param_name": param_name1,
        "session_id": session_id,
        "login_id": login_id
    };
    get_ajax_baseurl(inputdata, "s_get_param_name1");
}

$("#tab_1").click(function () {
    query_tab1();
});

$("#tab_2").click(function () {
    query_tab2();
});

$("#tab_3").click(function () {
    query_tab3();
});

$("#tab_4").click(function () {
    query_tab4();
});


function isExistOption(id, value) {
    var isExist = false;
    var count = $('#' + id).find('option').length;

    for (var i = 0; i < count; i++) {
        if ($('#' + id).get(0).options[i].value == value) {
            isExist = true;
            break;
        }
    }
    return isExist;
}

function addOptionValue(id, value, text) {
    if (!isExistOption(id, value)) {
        $('#' + id).append("<option value=" + value + ">" + text + "</option>");
    }
}

function delOptionValue(id, value) {
    if (isExistOption(id, value)) {
        $("#" + id + " option[value=" + value + "]").remove();
    }
}

// 页面初始化
function init_pgd() {
    $("#add1_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));
    $("#edit1_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));
    $("#add2_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));
    $("#edit2_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));
    $("#add3_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));
    $("#edit3_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));
    $("#add4_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));
    $("#edit4_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));
    $("#add5_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));
    $("#edit5_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));
    $("#add6_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));
    $("#edit6_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));
    $("#add7_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));
    $("#edit7_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));
    $("#add8_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));
    $("#edit8_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));

    laydate.render({
        elem: '#add1_create_date',
        type: 'datetime',
		trigger: 'click'
    });
    laydate.render({
        elem: '#edit1_create_date',
        type: 'datetime',
		trigger: 'click'
    });
    laydate.render({
        elem: '#add2_create_date',
        type: 'datetime',
		trigger: 'click'
    });
    laydate.render({
        elem: '#edit2_create_date',
        type: 'datetime',
		trigger: 'click'
    });
    laydate.render({
        elem: '#add3_create_date',
        type: 'datetime',
		trigger: 'click'
    });
    laydate.render({
        elem: '#edit3_create_date',
        type: 'datetime',
		trigger: 'click'
    });
    laydate.render({
        elem: '#add4_create_date',
        type: 'datetime',
		trigger: 'click'
    });
    laydate.render({
        elem: '#edit4_create_date',
        type: 'datetime',
		trigger: 'click'
    });
    laydate.render({
        elem: '#add5_create_date',
        type: 'datetime',
		trigger: 'click'
    });
    laydate.render({
        elem: '#edit5_create_date',
        type: 'datetime',
		trigger: 'click'
    });
    laydate.render({
        elem: '#add6_create_date',
        type: 'datetime',
		trigger: 'click'
    });
    laydate.render({
        elem: '#edit6_create_date',
        type: 'datetime',
		trigger: 'click'
    });
    laydate.render({
        elem: '#add7_create_date',
        type: 'datetime',
		trigger: 'click'
    });
    laydate.render({
        elem: '#edit7_create_date',
        type: 'datetime',
		trigger: 'click'
    });
    laydate.render({
        elem: '#add8_create_date',
        type: 'datetime',
		trigger: 'click'
    });
    laydate.render({
        elem: '#edit8_create_date',
        type: 'datetime',
		trigger: 'click'
    });
}

// 加载js
$(document).ready(function () {
	init_page();
	init_pgd();
    query_tab1();
	query_tab4();
})