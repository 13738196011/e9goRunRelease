//按钮事件新增或编辑
var BE35D9_type = "";
//其他页面传到本页面参数
var BE35D9_param = {};
//暂时没用
var BE35D9_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BE35D9_ary_table_id = null;var BE35D9_ary_column_type = [{'main_id': '1', 'cn_name': 'STRING'}, {'main_id': '2', 'cn_name': 'INT'}, {'main_id': '3', 'cn_name': 'FLOAT'}, {'main_id': '4', 'cn_name': 'DATE'}];var BE35D9_ary_column_qry = [{'main_id': '0', 'cn_name': ''}, {'main_id': '1', 'cn_name': '输入框'}, {'main_id': '2', 'cn_name': '下拉框'}, {'main_id': '3', 'cn_name': '查找框'}];
/*declare select options end*/

//业务逻辑数据开始
function BE35D9_t_table_column_biz_start(inputdata) {
	BE35D9_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_table_column$table_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BE35D9_get_N01_t_table_column$table_id");	
    /*biz end*/
}

/*biz step begin*/
function BE35D9_format_table_id(value, row, index) {    var objResult = value;    for(i = 0; i < BE35D9_ary_table_id.length; i++) {        var obj = BE35D9_ary_table_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BE35D9_format_column_type(value, row, index) {    var objResult = value;    for(i = 0; i < BE35D9_ary_column_type.length; i++) {        var obj = BE35D9_ary_column_type[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BE35D9_format_column_qry(value, row, index) {    var objResult = value;    for(i = 0; i < BE35D9_ary_column_qry.length; i++) {        var obj = BE35D9_ary_column_qry[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BE35D9_get_N01_t_table_column$table_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_table_column$table_id) == false)        return false;    BE35D9_ary_table_id = input.N01_t_table_column$table_id;    if($("#BE35D9_table_id").is("select") && $("#BE35D9_table_id")[0].options.length == 0)    {        $.each(BE35D9_ary_table_id, function (i, obj) {            addOptionValue("BE35D9_table_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#BE35D9_column_type").is("select") && $("#BE35D9_column_type")[0].options.length == 0)    {        $.each(BE35D9_ary_column_type, function (i, obj) {            addOptionValue("BE35D9_column_type", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#BE35D9_column_qry").is("select") && $("#BE35D9_column_qry")[0].options.length == 0)    {        $.each(BE35D9_ary_column_qry, function (i, obj) {            addOptionValue("BE35D9_column_qry", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    BE35D9_init_t_table_column();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function BE35D9_table_id_cn_name_fun(){    index_subhtml = "t_single_table.html"    random_subhtml = "XXXXXX";    if(loadHtmlSubVueFun("biz_vue/t_single_table.html","BE35D9_t_table_column_call_vue") == true){        var n = Get_RandomDiv("XXXXXX","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#XXXXXX_t_single_table.html_Events').bootstrapTable('resetView');                XXXXXX_param["ly_index"] = index;                XXXXXX_param["target_name"] = "BE35D9_find_table_id_cn_name"                XXXXXX_param["target_id"] = "BE35D9_table_id"                XXXXXX_param["sourc_id"] = "MAIN_ID"                XXXXXX_param["sourc_name"] = "TABLE_EN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function BE35D9_page_end(){
	if(BE35D9_param["type"] == "edit"){
		BE35D9_get_edit_info();
	}
}

//页面初始化方法
function BE35D9_init_t_table_column() {
	//type = getUrlParam("type");
	if(BE35D9_param["type"] == "add"){
		$("#BE35D9_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BE35D9_column_en_name").val(BE35D9_param["column_en_name"]);        $("#BE35D9_table_id").val(BE35D9_param["table_id"]);        $("#BE35D9_find_table_id_cn_name").val(BE35D9_param["table_id_cn_name"]);		
		/*Get Find Select param end*/	
	}
	else if(BE35D9_param["type"] == "edit"){
	
	}
	
	//表单验证
	BE35D9_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BE35D9_create_date").val() == "")    {        $("#BE35D9_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BE35D9_create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BE35D9_page_end();
}

//提交表单数据
function BE35D9_SubmitForm(){
	if(BE35D9_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_table_column",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#BE35D9_table_id").val()                ,"param_value2": s_encode($("#BE35D9_column_cn_name").val())                ,"param_value3": s_encode($("#BE35D9_column_en_name").val())                ,"param_value4": $("#BE35D9_column_type").val()                ,"param_value5": $("#BE35D9_column_length").val()                ,"param_value6": $("#BE35D9_column_qry").val()                ,"param_value7": s_encode($("#BE35D9_column_qry_format").val())                ,"param_value8": s_encode($("#BE35D9_column_find_html").val())                ,"param_value9": s_encode($("#BE35D9_column_find_return").val())                ,"param_value10": $("#BE35D9_create_date").val()                ,"param_value11": s_encode($("#BE35D9_s_desc").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BE35D9_get_N01_ins_t_table_column");
	}
	else if(BE35D9_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_table_column",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#BE35D9_table_id").val()                ,"param_value2": s_encode($("#BE35D9_column_cn_name").val())                ,"param_value3": s_encode($("#BE35D9_column_en_name").val())                ,"param_value4": $("#BE35D9_column_type").val()                ,"param_value5": $("#BE35D9_column_length").val()                ,"param_value6": $("#BE35D9_column_qry").val()                ,"param_value7": s_encode($("#BE35D9_column_qry_format").val())                ,"param_value8": s_encode($("#BE35D9_column_find_html").val())                ,"param_value9": s_encode($("#BE35D9_column_find_return").val())                ,"param_value10": $("#BE35D9_create_date").val()                ,"param_value11": s_encode($("#BE35D9_s_desc").val())                ,"param_value12": $("#BE35D9_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BE35D9_get_N01_upd_t_table_column");
	}
}

//vue回调
function BE35D9_t_table_column_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_single_table.html"){        var n = Get_RandomDiv("XXXXXX",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":BE35D9_type,                    "ly_index":index,                    "target_name":"BE35D9_find_table_id_cn_name",                    "target_id":"BE35D9_find_table_id",                    "sourc_id":"MAIN_ID",                    "sourc_name":"TABLE_EN_NAME"                };                loadScript_hasparam("biz_vue/t_single_table.html.js","XXXXXX_t_single_table.html_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//for表单提交
$("#BE35D9_save_t_table_column_Edit").click(function () {
	$("form[name='BE35D9_DataModal']").submit();
})

/*修改数据*/
function BE35D9_get_N01_upd_t_table_column(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_table_column) == true)
	{
		swal("修改数据成功!", "", "success");
		AE35D9_t_table_column_query();
		BE35D9_clear_validate();
		layer.close(BE35D9_param["ly_index"]);
	}
}

/*添加数据*/
function BE35D9_get_N01_ins_t_table_column(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_table_column) == true)
	{
		swal("添加数据成功!", "", "success");
		AE35D9_t_table_column_query();
		BE35D9_clear_validate();
		layer.close(BE35D9_param["ly_index"]);
	}
}

//取消编辑
$("#BE35D9_cancel_t_table_column_Edit").click(function () {
	layer.close(BE35D9_param["ly_index"]);
	BE35D9_clear_validate();
	$("[id^='BE35D9_div']").hide();
})

//清除查找框
function BE35D9_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BE35D9_clear_validate(){
	$("#BE35D9_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BE35D9_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BE35D9_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BE35D9_clear_edit_info(){
	var inputs = $("#BE35D9_DataModal").find('input');
	var selects = $("#BE35D9_DataModal").find("select");
	var textareas = $("#BE35D9_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
    BE35D9_clear_input_cn_name('BE35D9_find_table_id_cn_name','BE35D9_table_id')	
	/*input validate clear end*/
	BE35D9_init_t_table_column();
}

//页面输入框赋值
function BE35D9_get_edit_info(){
	var rowData = $("#AE35D9_t_table_column_Events").bootstrapTable('getData')[AE35D9_select_t_table_column_rowId];
	var inputs = $("#BE35D9_DataModal").find('input');
	var selects = $("#BE35D9_DataModal").find("select");
	var textareas = $("#BE35D9_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"AE35D9","BE35D9");
	
	/*
	$.each(selects, function (i, obj) {
		if(typeof(eval("AE35D9_ary_"+obj.id.toString().replace("BE35D9_",""))) == "object")
		{
			$.each(eval("AE35D9_ary_"+obj.id.toString().replace("BE35D9_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE35D9_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("BE35D9_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("AE35D9_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#BE35D9_DataModal").find('[id="'+"BE35D9_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE35D9_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE35D9_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});*/
}

//form验证
function BE35D9_checkFormInput() {
    BE35D9_validate = $("#BE35D9_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BE35D9_main_id: {}            ,BE35D9_table_id: {}            ,BE35D9_column_cn_name: {}            ,BE35D9_column_en_name: {}            ,BE35D9_column_type: {}            ,BE35D9_column_length: {digits: true,required : true,maxlength:10}            ,BE35D9_column_qry: {}            ,BE35D9_column_qry_format: {}            ,BE35D9_column_find_html: {}            ,BE35D9_column_find_return: {}            ,BE35D9_create_date: {date: true,required : true,maxlength:19}            ,BE35D9_s_desc: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BE35D9_main_id: {}            ,BE35D9_table_id: {}            ,BE35D9_column_cn_name: {}            ,BE35D9_column_en_name: {}            ,BE35D9_column_type: {}            ,BE35D9_column_length: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,BE35D9_column_qry: {}            ,BE35D9_column_qry_format: {}            ,BE35D9_column_find_html: {}            ,BE35D9_column_find_return: {}            ,BE35D9_create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BE35D9_s_desc: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BE35D9_SubmitForm();
        	return false;
        }
    })
}