//主从表tab选项卡标志位
var AF0359_Tab_Flag = -1;
//选择某一行
var AF0359_select_menu_content_rowId = "";
//按钮事件新增或编辑
var AF0359_type = "";
//其他页面传到本页面参数
var AF0359_param = {};
//table选中数据，如无则默认第一条
var AF0359_rowCheckData = null;

/*定义查询条件变量*/
/*declare query param begin*/
var AF0359_tem_MENU_ID = "-1";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var AF0359_ary_MENU_ID = null;var AF0359_ary_IS_AVA = [{'MAIN_ID': '1', 'CN_NAME': '启用'}, {'MAIN_ID': '0', 'CN_NAME': '禁用'}];var AF0359_ary_ROLE_ID = [{'MAIN_ID': '1', 'CN_NAME': '开发者'}, {'MAIN_ID': '2', 'CN_NAME': '系统管理员'}, {'MAIN_ID': '3', 'CN_NAME': '运维人员'}, {'MAIN_ID': '4', 'CN_NAME': '普通用户'}];
/*declare select options end*/

/*绑定show监听事件*/
if(AF0359_Tab_Flag == "-1"){
	var n = Get_RandomDiv("AF0359","");
	$(n).bind("show", function(objTag){
		AF0359_Adjust_Sub_Sequ();
		if(AF0359_Tab_Flag > 0)
			AF0359_adjust_tab();		
	});
}

//设置主从表div顺序
function AF0359_Adjust_Sub_Sequ(){
	var temSubDivs = $("#content-main").find("div[data-id]");
	var MainDiv = $(Get_RandomDiv("AF0359",""));
	for(i = 0; i < temSubDivs.length; i++) {
		if(AF0359_Is_Sub_Div(temSubDivs[i].id)){
			$(temSubDivs[i]).before($(MainDiv));
			break;
		}
	}
}

//主从表传递参数
function AF0359_param_set(){
	/*Main Subsuv Table Param Begin*/
    if(AF0359_param.hasOwnProperty("MENU_ID_cn_name"))        $("#AF0359_find_MENU_ID_cn_name").val(s_decode(AF0359_param["MENU_ID_cn_name"]));    if(AF0359_param.hasOwnProperty("MENU_ID"))        $("#AF0359_find_MENU_ID").val(AF0359_param["MENU_ID"]);    if(AF0359_param.hasOwnProperty("hidden_find")){        $("#AF0359_Ope_MENU_ID").hide();        $("#AF0359_Clear_MENU_ID").hide();    }	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function AF0359_menu_content_biz_start(inputparam) {
	layer.close(ly_index);
	AF0359_param = inputparam;
	//主从表传递参数
	AF0359_param_set();	
    /*biz begin*/
    var inputdata = {        "param_name": "N01_menu_content$MENU_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AF0359_get_N01_menu_content$MENU_ID");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function AF0359_format_MENU_ID(value, row, index) {    var objResult = value;    for(i = 0; i < AF0359_ary_MENU_ID.length; i++) {        var obj = AF0359_ary_MENU_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AF0359_format_IS_AVA(value, row, index) {    var objResult = value;    for(i = 0; i < AF0359_ary_IS_AVA.length; i++) {        var obj = AF0359_ary_IS_AVA[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AF0359_format_ROLE_ID(value, row, index) {    var objResult = "";    for(i = 0; i < AF0359_ary_ROLE_ID.length; i++) {        var obj = AF0359_ary_ROLE_ID[i];
        var value_s = s_decode(value.toString()).split(",");
        for(j = 0; j < value_s.length; j ++){
	        if (obj[GetLowUpp("MAIN_ID")].toString() == value_s[j].toString()) {
	        	if(objResult == "")
	        		objResult += obj[GetLowUpp("CN_NAME")];
	        	else
	        		objResult += ","+obj[GetLowUpp("CN_NAME")];
	        }
        }    }    return objResult;}function AF0359_get_N01_menu_content$MENU_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_menu_content$MENU_ID) == false)        return false;    AF0359_ary_MENU_ID = input.N01_menu_content$MENU_ID;    AF0359_init_menu_content();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function AF0359_MENU_ID_cn_name_fun(){    index_subhtml = "menu_info.vue"    random_subhtml = "A77565";    if(loadHtmlSubVueFun("dev_vue/menu_info.vue","AF0359_menu_content_call_vue") == true){        var n = Get_RandomDiv("A77565","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#A77565_menu_info_Events').bootstrapTable('resetView');                A77565_param["ly_index"] = index;                A77565_param["target_name"] = "AF0359_find_MENU_ID_cn_name"                A77565_param["target_id"] = "AF0359_find_MENU_ID"                A77565_param["sourc_id"] = "MAIN_ID"                A77565_param["sourc_name"] = "MENU_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function AF0359_page_end(){
	AF0359_adjust_tab();
}

//首页菜单显示列定义
var AF0359_menu_content = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '菜单url',        field: 'MENU_ID',        sortable: true        ,formatter: AF0359_format_MENU_ID    },    {        title: '是否启用',        field: 'IS_AVA',        sortable: true        ,formatter: AF0359_format_IS_AVA    },    {        title: '角色',        field: 'ROLE_ID',        sortable: true        ,formatter: AF0359_format_ROLE_ID    }	
	/*table column end*/
];

//页面初始化
function AF0359_init_menu_content() {
	$(window).resize(function () {
		  $('#AF0359_menu_content_Events').bootstrapTable('resetView');
	});
	//首页菜单查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AF0359_btn_menu_content_query').click();
}

//查询接口
function AF0359_menu_content_query() {
    $('#AF0359_menu_content_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_menu_content",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": AF0359_tem_MENU_ID        ,"param_value2": AF0359_tem_MENU_ID		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AF0359_get_N01_sel_menu_content");
}

//查询结果
function AF0359_get_N01_sel_menu_content(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_menu_content) == false)
        return false;
    AF0359_rowCheckData = null;
    //调整table各列宽度
    $.each(AF0359_menu_content, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_menu_content;
    if(s_data.length > 0)
    	AF0359_rowCheckData = s_data[0];    
    AF0359_select_menu_content_rowId = "";
    AF0359_Tab_Flag = 0;
    $('#AF0359_menu_content_Events').bootstrapTable('destroy');
    $("#AF0359_menu_content_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AF0359_menu_content_Events').find("tr.changeColor").removeClass('changeColor');
                AF0359_select_menu_content_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AF0359_menu_content_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AF0359_select_menu_content_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#AF0359_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AF0359_param.hasOwnProperty("target_name"))
			{
				$("#"+AF0359_param["target_id"]).val(eval("row."+AF0359_param["sourc_id"].toUpperCase()));
				$("#"+AF0359_param["target_name"]).val(s_decode(eval("row."+AF0359_param["sourc_name"].toUpperCase())));				
				layer.close(AF0359_param["ly_index"]);
			}
		},
        toolbar: "#AF0359_menu_content_Toolbar",
        columns: AF0359_menu_content,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        onPostBody:function(){
        	if(s_data.length != 0)
            	AF0359_page_end();
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AF0359_btn_menu_content_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    AF0359_tem_MENU_ID = "-1";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    BF0359_ary_MENU_ID = null;    $("#AF0359_find_MENU_ID_cn_name").val("");    $("#AF0359_find_MENU_ID").val("-1");
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_menu_content$MENU_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AF0359_get_N01_menu_content$MENU_ID");	
    /*biz end*/
})

//查询按钮
$('#AF0359_btn_menu_content_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    AF0359_tem_MENU_ID = $("#AF0359_find_MENU_ID").val();	
    /*set query param end*/
	AF0359_menu_content_query();
})

//vue回调
function AF0359_menu_content_call_vue(objResult){
	//选择某条记录或自动选择第一条
	if (AF0359_select_menu_content_rowId != "") 
		AF0359_rowCheckData = $("#AF0359_menu_content_Events").bootstrapTable('getData')[AF0359_select_menu_content_rowId];
	
	if(index_subhtml == "menu_content_$.vue")
	{
		var n = Get_RandomDiv("BF0359",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AF0359_type,"ly_index":index};
				if(AF0359_param.hasOwnProperty("hidden_find")){
					inputdata["hidden_find"] = "1";
				}				
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temMENU_ID = $("#AF0359_find_MENU_ID_cn_name").val();                if(temMENU_ID != ""){                    inputdata["MENU_ID_cn_name"] = temMENU_ID;                    inputdata["MENU_ID"] = $("#AF0359_MENU_ID").val();                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("dev_vue/menu_content_$.js","BF0359_menu_content_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "menu_info.vue"){        var n = Get_RandomDiv("A77565",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":AF0359_type,                    "ly_index":index,                    "target_name":"AF0359_find_MENU_ID_cn_name",                    "target_id":"AF0359_find_MENU_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"MENU_NAME"                };                loadScript_hasparam("dev_vue/menu_info.js","A77565_menu_info_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/

	/*get tab subvue end*/
}

//新增按钮
$("#AF0359_btn_menu_content_add").click(function () {
	AF0359_type = "add";
	index_subhtml = "menu_content_$.vue";
	if(loadHtmlSubVueFun("dev_vue/menu_content_$.vue","AF0359_menu_content_call_vue") == true){
		var n = Get_RandomDiv("BF0359","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BF0359_param["type"] = AF0359_type;
				BF0359_param["ly_index"]= index;
				if(AF0359_param.hasOwnProperty("hidden_find")){
					BF0359_param["hidden_find"] = "1";
				}
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temMENU_ID = $("#AF0359_find_MENU_ID_cn_name").val();                if(temMENU_ID != ""){                    BF0359_param["MENU_ID_cn_name"] = temMENU_ID;                    BF0359_param["MENU_ID"] = $("#AF0359_MENU_ID").val();                }				
				/*Send Two FindSelect param end*/

				BF0359_clear_edit_info();
			},
			end: function(){
				BF0359_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AF0359_btn_menu_content_edit").click(function () {
	if (AF0359_select_menu_content_rowId != "") {
		AF0359_type = "edit";
		index_subhtml = "menu_content_$.vue";
		if(loadHtmlSubVueFun("dev_vue/menu_content_$.vue","AF0359_menu_content_call_vue") == true){
			var n = Get_RandomDiv("BF0359","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BF0359_param["type"] = AF0359_type;
					BF0359_param["ly_index"] = index;
					if(AF0359_param.hasOwnProperty("hidden_find")){
						BF0359_param["hidden_find"] = "1";
					}
					BF0359_clear_edit_info();
					BF0359_get_edit_info();
				},
				end: function(){
					BF0359_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AF0359_btn_menu_content_delete').click(function () {
	//单行选择
	var rowData = $("#AF0359_menu_content_Events").bootstrapTable('getData')[AF0359_select_menu_content_rowId];
	//多行选择
	var rowDatas = AF0359_sel_row_menu_content();
	if (AF0359_select_menu_content_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_menu_content",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AF0359_N01_del_menu_content");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_menu_content",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AF0359_N01_del_menu_content");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AF0359_N01_del_menu_content(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_menu_content) == true)
		AF0359_menu_content_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function AF0359_adjust_tab(){
	if(typeof($("#AF0359_TAB_MAIN")[0]) != "undefined" && $("#AF0359_TAB_MAIN")[0].length != 0){
		AF0359_Tab_Flag = 1;
		$(Get_RDivNoBuild("AF0359","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("AF0359","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("AF0359","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#AF0359_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/
//隐藏tab页选项卡function AF0359_hide_tab_fun(){    var n = null;}//判断是否sub子项div页面function AF0359_Is_Sub_Div(temDivId){    if(temDivId.indexOf("XX$TTT") == 0)        return false;    return false;}
/*Tab Click Fun End*/

function AF0359_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("dev_vue/"+inputUrl,"AF0359_menu_content_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (AF0359_select_menu_content_rowId != "") 
			rowData = $("#AF0359_menu_content_Events").bootstrapTable('getData')[AF0359_select_menu_content_rowId];
		else
			rowData = AF0359_rowCheckData;
		//$("#AF0359_menu_content_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			var temFlag = true;
			$.each(temPar, function (i, obj) {
				if(eval(inputrandom+"_param.hasOwnProperty(\""+obj.target_id+"\")") && eval(inputrandom+"_param[\""+obj.target_id+"\"]").toString() == rowData[obj.sourc_id].toString())
					temFlag = false;
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			if(temFlag){
				//传递子页面隐藏find功能
				eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
				//参数传递并赋值
				eval(inputrandom+"_param_set()");
				var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
				$("#"+inputrandom+"_btn_"+tbName+"_query").click();
			}
		}
		else{
			$.each(temPar, function (i, obj) {
				if(obj.sourc_id.toUpperCase() == "MAIN_ID")
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"-999\"");
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"\"");
			});
			//传递子页面隐藏find功能
			eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
			var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
			$("#"+inputrandom+"_btn_"+tbName+"_query").click();		
		}
	}
}

//清除 查找框
function AF0359_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AF0359_sel_row_menu_content(){
	//获得选中行
	var checkedbox= $("#AF0359_menu_content_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}