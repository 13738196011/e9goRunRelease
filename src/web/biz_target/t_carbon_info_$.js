var type = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var ary_file_id = null;
/*declare select options end*/

//业务逻辑数据开始
function biz_start() {
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "T01_t_excel_data$file_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "get_T01_t_excel_data$file_id");	
    /*biz end*/
}

/*biz step begin*/
function format_file_id(value, row, index) {    var objResult = value;    for(i = 0; i < ary_file_id.length; i++) {        var obj = ary_file_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function get_T01_t_excel_data$file_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_t_excel_data$file_id) == false)        return false;    ary_file_id = input.T01_t_excel_data$file_id;    if($("#file_id").is("select") && $("#file_id")[0].options.length == 0)    {        $.each(ary_file_id, function (i, obj) {            addOptionValue("file_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    init_t_carbon_info();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function file_id_cn_name_fun(){    layer.open({        type: 2,        area: ['1000px', '570px'],        fixed: false, //不固定        maxmin: true,        content: "t_file_info.html?target_name=find_file_id_cn_name&target_id=file_id&sourc_id=MAIN_ID&sourc_name=FILE_NAME",        success: function(layero, index){            //var body = layer.getChildFrame('body',index);            //var main_id = $("#find_file_id").val();        }    });}
/*find qry fun end*/

/*页面结束*/
function page_end(){
}


//页面初始化方法
function init_t_carbon_info() {
	type = getUrlParam("type");
	if(type == "add"){
		$("#main_id").parent().parent().parent().hide();
	}
	else if(type == "edit"){
	
	}
	
	//表单验证
	checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#get_time").val() == "")    {        $("#get_time").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#get_time',        type: 'datetime',        trigger: 'click'    });    if($("#create_date").val() == "")    {        $("#create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
}

//提交表单数据
function SubmitForm(){
	if(type == "add"){
		var inputdata = {
				"param_name": "T01_ins_t_carbon_info",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#file_id").val()                ,"param_value2": $("#get_time").val()                ,"param_value3": $("#coal_carbon").val()                ,"param_value4": $("#oil_carbon").val()                ,"param_value5": $("#gas_carbon").val()                ,"param_value6": $("#ele_carbon").val()                ,"param_value7": $("#hot_carbon").val()                ,"param_value8": $("#create_date").val()                ,"param_value9": s_encode($("#s_desc").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_ins_t_carbon_info");
	}
	else if(type == "edit"){
		var inputdata = {
				"param_name": "T01_upd_t_carbon_info",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#file_id").val()                ,"param_value2": $("#get_time").val()                ,"param_value3": $("#coal_carbon").val()                ,"param_value4": $("#oil_carbon").val()                ,"param_value5": $("#gas_carbon").val()                ,"param_value6": $("#ele_carbon").val()                ,"param_value7": $("#hot_carbon").val()                ,"param_value8": $("#create_date").val()                ,"param_value9": s_encode($("#s_desc").val())                ,"param_value10": $("#main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_upd_t_carbon_info");
	}
}

//以下为页面表单验证
$("#save_t_carbon_info_Edit").click(function () {
	$("form[name='DataModal']").submit();
})

/*修改数据*/
function get_T01_upd_t_carbon_info(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_upd_t_carbon_info) == true)
	{
        //init();
		parent.swal("修改数据成功!", "", "success");
		parent.t_carbon_info_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

/*添加数据*/
function get_T01_ins_t_carbon_info(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_ins_t_carbon_info) == true)
	{
		parent.swal("添加数据成功!", "", "success");
		parent.t_carbon_info_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

//取消编辑
$("#cancel_t_carbon_info_Edit").click(function () {
	var index = parent.layer.getFrameIndex(window.name);
	parent.layer.close(index);
})

function clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

$(document).ready(function () {
});

window.onpageshow = function (e) {
    if (e.persisted || (window.performance.navigation.type == 2)) {
        is_history_back = 1;
    } else {
        is_history_back = 0;
    }
    //页面初始化
    init_page();
};

//form验证
function checkFormInput() {
    $("#DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            main_id: {}            ,file_id: {}            ,get_time: {date: true,required : true,maxlength:19}            ,coal_carbon: {number: true,required : true,maxlength:14}            ,oil_carbon: {number: true,required : true,maxlength:14}            ,gas_carbon: {number: true,required : true,maxlength:14}            ,ele_carbon: {number: true,required : true,maxlength:14}            ,hot_carbon: {number: true,required : true,maxlength:14}            ,create_date: {date: true,required : true,maxlength:19}            ,s_desc: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            main_id: {}            ,file_id: {}            ,get_time: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,coal_carbon: {number: "必须输入合法的小数",required : "必须输入合法的小数",maxlength:"长度不能超过14"}            ,oil_carbon: {number: "必须输入合法的小数",required : "必须输入合法的小数",maxlength:"长度不能超过14"}            ,gas_carbon: {number: "必须输入合法的小数",required : "必须输入合法的小数",maxlength:"长度不能超过14"}            ,ele_carbon: {number: "必须输入合法的小数",required : "必须输入合法的小数",maxlength:"长度不能超过14"}            ,hot_carbon: {number: "必须输入合法的小数",required : "必须输入合法的小数",maxlength:"长度不能超过14"}            ,create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,s_desc: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	SubmitForm();
        	return false;
        }
    })
}