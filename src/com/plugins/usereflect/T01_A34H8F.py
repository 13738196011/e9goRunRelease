#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
import modbus_tk.defines as cst
import modbus_tk.modbus_rtu as modbus_rtu
from com.zxy.z_debug import z_debug
class T01_A34H8F(z_debug):
    def __init__(self):
        pass
    def get_data_rtu(self):
        red = []
        alarm = ""
        try:
            com_at = Com_Fun.GetHashTable(Com_Para.htComPort,"A")
            master = modbus_rtu.RtuMaster(com_at.attSerial)                   
            master.set_timeout(5.0)
            master.set_verbose(True)
            red = master.execute(1, cst.READ_HOLDING_REGISTERS, 0, 2)  # 这里可以修改需要读取的功能码
            print(red)
            alarm = "正常"
            return list(red), alarm    
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
            alarm = (str(e))
            return red, alarm        