﻿var ly_index;

function biz_start(){
}

$(document).ready(function () {
    init_page();
    //init();
});

//业务逻辑数据开始
function biz_start() {    
    $("#btnFileUpload").click(function () {
    	var formData = new FormData($("#updForm").eq(0)[0]);
    	var param_v = "&login_id="+login_id;
    	if($("#file_set").val() == "")
    		swal("请选择正确附件!", "请选择正确附件", "warning");
    	else
    		get_ajax_upload(formData,param_v,"get_upload_file");
    });
    
    $("#btnClearFile").click(function () {
    	$("#upd_file_list").html("");
    });
}

function get_A01_INPROC(objTag){
	var objValue = objTag.A01_INPROC;
	if (Call_OpeResult(objValue) == true)
	{
	}
}

function get_upload_file(objTag){
	var objValue = objTag.A01_UpLoadFile;
	if (Call_OpeResult(objValue) == true)
	{
		$.each(objValue, function (i, obj) {
			var fileList = "<a href=\"../file/"+obj[GetLowUpp("newFileName")]+"\" target='_blank'>"+obj[GetLowUpp("oldFileName")]+"</a><br>";
			$("#upd_file_list").append(fileList);
			
			if(obj[GetLowUpp("oldFileName")].indexOf("不符合上传文件格式") == -1)
			{				
			    var inputdata = {
			    	"param_name": "A01_INPROC",
			        "session_id": session_id,
			        "login_id": login_id,
			        "param_value1": s_encode(obj[GetLowUpp("newFileName")])
			    };
			    get_ajax_baseurl(inputdata, "get_A01_INPROC");
			}
		});
	}
}