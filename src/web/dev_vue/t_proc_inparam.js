//主从表tab选项卡标志位
var A9FE70_Tab_Flag = -1;
//选择某一行
var A9FE70_select_t_proc_inparam_rowId = "";
//按钮事件新增或编辑
var A9FE70_type = "";
//其他页面传到本页面参数
var A9FE70_param = {};
//table选中数据，如无则默认第一条
var A9FE70_rowCheckData = null;

/*定义查询条件变量*/
/*declare query param begin*/
var A9FE70_tem_PROC_ID = "-1";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var A9FE70_ary_PROC_ID = null;var A9FE70_ary_PARAM_TYPE = null;var A9FE70_ary_IS_URLENCODE = [{'MAIN_ID': '1', 'CN_NAME': '是'}, {'MAIN_ID': '0', 'CN_NAME': '否'}];
/*declare select options end*/

/*绑定show监听事件*/
if(A9FE70_Tab_Flag == "-1"){
	var n = Get_RandomDiv("A9FE70","");
	$(n).bind("show", function(objTag){
		A9FE70_Adjust_Sub_Sequ();
		if(A9FE70_Tab_Flag > 0)
			A9FE70_adjust_tab();		
	});
}

//设置主从表div顺序
function A9FE70_Adjust_Sub_Sequ(){
	var temSubDivs = $("#content-main").find("div[data-id]");
	var MainDiv = $(Get_RandomDiv("A9FE70",""));
	for(i = 0; i < temSubDivs.length; i++) {
		if(A9FE70_Is_Sub_Div(temSubDivs[i].id)){
			$(temSubDivs[i]).before($(MainDiv));
			break;
		}
	}
}

//主从表传递参数
function A9FE70_param_set(){
	/*Main Subsuv Table Param Begin*/
    if(A9FE70_param.hasOwnProperty("PROC_ID_cn_name"))        $("#A9FE70_find_PROC_ID_cn_name").val(s_decode(A9FE70_param["PROC_ID_cn_name"]));    if(A9FE70_param.hasOwnProperty("PROC_ID"))        $("#A9FE70_find_PROC_ID").val(A9FE70_param["PROC_ID"]);    if(A9FE70_param.hasOwnProperty("hidden_find")){        $("#A9FE70_Ope_PROC_ID").hide();        $("#A9FE70_Clear_PROC_ID").hide();    }	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function A9FE70_t_proc_inparam_biz_start(inputparam) {
	layer.close(ly_index);
	A9FE70_param = inputparam;
	//主从表传递参数
	A9FE70_param_set();	
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_proc_inparam$PROC_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A9FE70_get_N01_t_proc_inparam$PROC_ID");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function A9FE70_format_PROC_ID(value, row, index) {    var objResult = value;    for(i = 0; i < A9FE70_ary_PROC_ID.length; i++) {        var obj = A9FE70_ary_PROC_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A9FE70_format_PARAM_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < A9FE70_ary_PARAM_TYPE.length; i++) {        var obj = A9FE70_ary_PARAM_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A9FE70_format_IS_URLENCODE(value, row, index) {    var objResult = value;    for(i = 0; i < A9FE70_ary_IS_URLENCODE.length; i++) {        var obj = A9FE70_ary_IS_URLENCODE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A9FE70_get_N01_t_proc_inparam$PROC_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_inparam$PROC_ID) == false)        return false;    A9FE70_ary_PROC_ID = input.N01_t_proc_inparam$PROC_ID;    var inputdata = {        "param_name": "N01_t_proc_inparam$PARAM_TYPE",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A9FE70_get_N01_t_proc_inparam$PARAM_TYPE");}function A9FE70_get_N01_t_proc_inparam$PARAM_TYPE(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_inparam$PARAM_TYPE) == false)        return false;    A9FE70_ary_PARAM_TYPE = input.N01_t_proc_inparam$PARAM_TYPE;    A9FE70_init_t_proc_inparam();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function A9FE70_PROC_ID_cn_name_fun(){    index_subhtml = "t_proc_name.vue"    random_subhtml = "";    if(loadHtmlSubVueFun("dev_vue/t_proc_name.vue","A9FE70_t_proc_inparam_call_vue") == true){        var n = Get_RandomDiv("","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#_t_proc_name_Events').bootstrapTable('resetView');                _param["ly_index"] = index;                _param["target_name"] = "A9FE70_find_PROC_ID_cn_name"                _param["target_id"] = "A9FE70_find_PROC_ID"                _param["sourc_id"] = "MAIN_ID"                _param["sourc_name"] = "INF_CN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function A9FE70_page_end(){
	A9FE70_adjust_tab();
}

//输入参数显示列定义
var A9FE70_t_proc_inparam = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '接口名称',        field: 'PROC_ID',        sortable: true        ,formatter: A9FE70_format_PROC_ID    },    {        title: '输入参数中文名称',        field: 'PARAM_CN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '输入参数英文名称',        field: 'PARAM_EN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '参数类型',        field: 'PARAM_TYPE',        sortable: true        ,formatter: A9FE70_format_PARAM_TYPE    },    {        title: '接口输入参数大小',        field: 'PARAM_SIZE',        sortable: true    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '是否URL编码',        field: 'IS_URLENCODE',        sortable: true        ,formatter: A9FE70_format_IS_URLENCODE    }	
	/*table column end*/
];

//页面初始化
function A9FE70_init_t_proc_inparam() {
	$(window).resize(function () {
		  $('#A9FE70_t_proc_inparam_Events').bootstrapTable('resetView');
	});
	//输入参数查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#A9FE70_btn_t_proc_inparam_query').click();
}

//查询接口
function A9FE70_t_proc_inparam_query() {
    $('#A9FE70_t_proc_inparam_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_proc_inparam",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": A9FE70_tem_PROC_ID        ,"param_value2": A9FE70_tem_PROC_ID		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "A9FE70_get_N01_sel_t_proc_inparam");
}

//查询结果
function A9FE70_get_N01_sel_t_proc_inparam(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_proc_inparam) == false)
        return false;
    A9FE70_rowCheckData = null;
    //调整table各列宽度
    $.each(A9FE70_t_proc_inparam, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_t_proc_inparam;
    if(s_data.length > 0)
    	A9FE70_rowCheckData = s_data[0];    
    A9FE70_select_t_proc_inparam_rowId = "";
    A9FE70_Tab_Flag = 0;
    $('#A9FE70_t_proc_inparam_Events').bootstrapTable('destroy');
    $("#A9FE70_t_proc_inparam_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#A9FE70_t_proc_inparam_Events').find("tr.changeColor").removeClass('changeColor');
                A9FE70_select_t_proc_inparam_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#A9FE70_t_proc_inparam_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                A9FE70_select_t_proc_inparam_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#A9FE70_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(A9FE70_param.hasOwnProperty("target_name"))
			{
				$("#"+A9FE70_param["target_id"]).val(eval("row."+A9FE70_param["sourc_id"].toUpperCase()));
				$("#"+A9FE70_param["target_name"]).val(s_decode(eval("row."+A9FE70_param["sourc_name"].toUpperCase())));				
				layer.close(A9FE70_param["ly_index"]);
			}
		},
        toolbar: "#A9FE70_t_proc_inparam_Toolbar",
        columns: A9FE70_t_proc_inparam,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        onPostBody:function(){
        	if(s_data.length != 0)
            	A9FE70_page_end();
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#A9FE70_btn_t_proc_inparam_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    A9FE70_tem_PROC_ID = "-1";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    B9FE70_ary_PROC_ID = null;    B9FE70_ary_PARAM_TYPE = null;    $("#A9FE70_find_PROC_ID_cn_name").val("");    $("#A9FE70_find_PROC_ID").val("-1");
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_proc_inparam$PROC_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A9FE70_get_N01_t_proc_inparam$PROC_ID");	
    /*biz end*/
})

//查询按钮
$('#A9FE70_btn_t_proc_inparam_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    A9FE70_tem_PROC_ID = $("#A9FE70_find_PROC_ID").val();	
    /*set query param end*/
	A9FE70_t_proc_inparam_query();
})

//vue回调
function A9FE70_t_proc_inparam_call_vue(objResult){
	//选择某条记录或自动选择第一条
	if (A9FE70_select_t_proc_inparam_rowId != "") 
		A9FE70_rowCheckData = $("#A9FE70_t_proc_inparam_Events").bootstrapTable('getData')[A9FE70_select_t_proc_inparam_rowId];
	
	if(index_subhtml == "t_proc_inparam_$.vue")
	{
		var n = Get_RandomDiv("B9FE70",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":A9FE70_type,"ly_index":index};
				if(A9FE70_param.hasOwnProperty("hidden_find")){
					inputdata["hidden_find"] = "1";
				}				
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temPROC_ID = $("#A9FE70_find_PROC_ID_cn_name").val();                if(temPROC_ID != ""){                    inputdata["PROC_ID_cn_name"] = temPROC_ID;                    inputdata["PROC_ID"] = $("#A9FE70_PROC_ID").val();                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("dev_vue/t_proc_inparam_$.js","B9FE70_t_proc_inparam_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_proc_name.vue"){        var n = Get_RandomDiv("",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":A9FE70_type,                    "ly_index":index,                    "target_name":"A9FE70_find_PROC_ID_cn_name",                    "target_id":"A9FE70_find_PROC_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"INF_CN_NAME"                };                loadScript_hasparam("dev_vue/t_proc_name.js","_t_proc_name_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/

	/*get tab subvue end*/
}

//新增按钮
$("#A9FE70_btn_t_proc_inparam_add").click(function () {
	A9FE70_type = "add";
	index_subhtml = "t_proc_inparam_$.vue";
	if(loadHtmlSubVueFun("dev_vue/t_proc_inparam_$.vue","A9FE70_t_proc_inparam_call_vue") == true){
		var n = Get_RandomDiv("B9FE70","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				B9FE70_param["type"] = A9FE70_type;
				B9FE70_param["ly_index"]= index;
				if(A9FE70_param.hasOwnProperty("hidden_find")){
					B9FE70_param["hidden_find"] = "1";
				}
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temPROC_ID = $("#A9FE70_find_PROC_ID_cn_name").val();                if(temPROC_ID != ""){                    B9FE70_param["PROC_ID_cn_name"] = temPROC_ID;                    B9FE70_param["PROC_ID"] = $("#A9FE70_PROC_ID").val();                }				
				/*Send Two FindSelect param end*/

				B9FE70_clear_edit_info();
			},
			end: function(){
				B9FE70_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#A9FE70_btn_t_proc_inparam_edit").click(function () {
	if (A9FE70_select_t_proc_inparam_rowId != "") {
		A9FE70_type = "edit";
		index_subhtml = "t_proc_inparam_$.vue";
		if(loadHtmlSubVueFun("dev_vue/t_proc_inparam_$.vue","A9FE70_t_proc_inparam_call_vue") == true){
			var n = Get_RandomDiv("B9FE70","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					B9FE70_param["type"] = A9FE70_type;
					B9FE70_param["ly_index"] = index;
					if(A9FE70_param.hasOwnProperty("hidden_find")){
						B9FE70_param["hidden_find"] = "1";
					}
					B9FE70_clear_edit_info();
					B9FE70_get_edit_info();
				},
				end: function(){
					B9FE70_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#A9FE70_btn_t_proc_inparam_delete').click(function () {
	//单行选择
	var rowData = $("#A9FE70_t_proc_inparam_Events").bootstrapTable('getData')[A9FE70_select_t_proc_inparam_rowId];
	//多行选择
	var rowDatas = A9FE70_sel_row_t_proc_inparam();
	if (A9FE70_select_t_proc_inparam_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_proc_inparam",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "A9FE70_N01_del_t_proc_inparam");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_proc_inparam",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "A9FE70_N01_del_t_proc_inparam");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function A9FE70_N01_del_t_proc_inparam(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_proc_inparam) == true)
		A9FE70_t_proc_inparam_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function A9FE70_adjust_tab(){
	if(typeof($("#A9FE70_TAB_MAIN")[0]) != "undefined" && $("#A9FE70_TAB_MAIN")[0].length != 0){
		A9FE70_Tab_Flag = 1;
		$(Get_RDivNoBuild("A9FE70","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("A9FE70","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("A9FE70","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#A9FE70_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/
//隐藏tab页选项卡function A9FE70_hide_tab_fun(){    var n = null;}//判断是否sub子项div页面function A9FE70_Is_Sub_Div(temDivId){    if(temDivId.indexOf("XX$TTT") == 0)        return false;    return false;}
/*Tab Click Fun End*/

function A9FE70_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("dev_vue/"+inputUrl,"A9FE70_t_proc_inparam_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (A9FE70_select_t_proc_inparam_rowId != "") 
			rowData = $("#A9FE70_t_proc_inparam_Events").bootstrapTable('getData')[A9FE70_select_t_proc_name_rowId];
		else
			rowData = A9FE70_rowCheckData;
		//$("#A9FE70_t_proc_inparam_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			var temFlag = true;
			$.each(temPar, function (i, obj) {
				if(eval(inputrandom+"_param.hasOwnProperty(\""+obj.target_id+"\")") && eval(inputrandom+"_param[\""+obj.target_id+"\"]").toString() == rowData[obj.sourc_id].toString())
					temFlag = false;
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			if(temFlag){
				//传递子页面隐藏find功能
				eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
				//参数传递并赋值
				eval(inputrandom+"_param_set()");
				var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
				$("#"+inputrandom+"_btn_"+tbName+"_query").click();
			}
		}
		else{
			$.each(temPar, function (i, obj) {
				if(obj.sourc_id.toUpperCase() == "MAIN_ID")
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"-999\"");
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"\"");
			});
			//传递子页面隐藏find功能
			eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
			var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
			$("#"+inputrandom+"_btn_"+tbName+"_query").click();		
		}
	}
}

//清除 查找框
function A9FE70_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function A9FE70_sel_row_t_proc_inparam(){
	//获得选中行
	var checkedbox= $("#A9FE70_t_proc_inparam_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}