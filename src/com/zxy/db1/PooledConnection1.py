#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from urllib.parse import unquote
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.z_debug import z_debug
class PooledConnection1(z_debug):
    attUpdtime = 0
    attB_nocursor = True
    attConnection = None
    attBusy = False
    attColumnNames = []
    def __init__(self, inputConn):
        self.attB_nocursor = True
        self.attConnection = inputConn
        self.attUpdtime = Com_Fun.getTimeLong()
    def executeQueryNoCommit(self, inputSql):
        temCursor = None
        temValues = None
        try:
            self.attUpdtime = Com_Fun.getTimeLong()        
            temCursor = self.attConnection.cursor()
            temCursor.execute(inputSql)
            temValues = temCursor.fetchall()
            self.attColumnNames = temCursor.description
            self.attConnection.commit()
        except Exception as e:
            temStr = "操作数据库出错[inputStrSql]" + inputSql + "\r\n" + repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temStr)
            uL.WriteLog()
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if not temCursor is None:
                temCursor.close()
        return temValues
    def executeQuery(self, inputSql):
        temCursor = None
        temValues = None
        try:
            self.attUpdtime = Com_Fun.getTimeLong()        
            temCursor = self.attConnection.cursor()
            temCursor.execute(inputSql)
            temValues = temCursor.fetchall()
            self.attColumnNames = temCursor.description
            self.attConnection.commit()
        except Exception as e:
            temStr = "操作数据库出错[inputStrSql]" + inputSql + "\r\n" + repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temStr)
            uL.WriteLog()
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if not temCursor is None:
                temCursor.close()
        return temValues
    def executeUpdate(self, inputSql):
        temResult = -1
        temCursor = None
        try:
            self.attUpdtime = Com_Fun.getTimeLong()        
            temCursor = self.attConnection.cursor()
            temCursor.execute(inputSql)
            self.attConnection.commit()
            temResult = temCursor.rowcount
        except Exception as e:
            temStr = "操作数据库出错[inputStrSql]" + inputSql + "\r\n" + repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temStr)
            uL.WriteLog()
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if not temCursor:
                temCursor.close()
        return temResult
    def ParamExecuteQuery(self,inputProName, inputParameters, inputParamTypes, inputParamOutName, inputParamOutType, inputStrSql):
        self.attUpdtime = Com_Fun.getTimeLong()
        temValues = None
        temCursor = self.attConnection.cursor()
        if len(inputParameters) == len(inputParamTypes) and len(inputParamOutName) == len(inputParamOutType):
            i = 0
            for temParamTypes in inputParamTypes:
                if temParamTypes == "LIST":
                    j = 0
                    temStr_V = ""
                    for tsv in temParamTypes.split(","):
                        if j != 0:
                            temStr_V += ","
                        temStr_V += "?"
                        j += 1                    
                    inputStrSql = inputStrSql.replace("@\\?",temStr_V,1)
                if temParamTypes.upper() == "STRING":
                    inputParameters[i] = inputParameters[i]#unquote(inputParameters[i],Com_Para.U_CODE)
                    pass
                elif temParamTypes.upper() == "INT":
                    inputParameters[i] = int(inputParameters[i])
                    pass
                elif temParamTypes.upper() == "FLOAT":
                    inputParameters[i] = float(inputParameters[i])
                    pass
                elif temParamTypes.upper() == "DATE":
                    inputParameters[i] = unquote(inputParameters[i].replace("+"," "),Com_Para.U_CODE)
                    pass
                elif temParamTypes.upper() == "LIST":
                    pass
                elif temParamTypes.upper() == "LIKESTRING":
                    inputParameters[i] = unquote(inputParameters[i],Com_Para.U_CODE)
                    pass
                i += 1
            if inputStrSql.upper().strip().find("INSERT INTO") == 0 or inputStrSql.upper().strip().find("UPDATE") == 0:
                iCount = temCursor.execute(inputStrSql,inputParameters)
                if iCount.rowcount != -1:
                    temCursor.execute("select '1' as 's_result','成功,"+str(iCount.rowcount)+"' as 'error_desc'")
                else:
                    temCursor.execute("select '0' as 's_result','失败' as 'error_desc'")
                temValues = temCursor.fetchall()
                self.attColumnNames = temCursor.description
                self.attConnection.commit()
            elif inputStrSql.upper().strip().find("DELETE") == 0:
                if inputStrSql.upper().strip().find(";") != -1:
                    iCount = None
                    for strSqls in inputStrSql.split(";"):             
                        iCount = temCursor.execute(strSqls)
                    if iCount.rowcount != -1:
                        temCursor.execute("select '1' as 's_result','成功' as 'error_desc'")
                    else:
                        temCursor.execute("select '0' as 's_result','失败' as 'error_desc'")
                    temValues = temCursor.fetchall()
                    self.attColumnNames = temCursor.description
                    self.attConnection.commit()
                else:
                    iCount = temCursor.execute(inputStrSql,inputParameters)
                    if iCount.rowcount != -1:
                        temCursor.execute("select '1' as 's_result','成功' as 'error_desc'")
                    else:
                        temCursor.execute("select '0' as 's_result','失败' as 'error_desc'")
                    temValues = temCursor.fetchall()
                    self.attColumnNames = temCursor.description
                    self.attConnection.commit()
            elif inputStrSql.upper().strip().find("SELECT") == 0:
                iCount = temCursor.execute(inputStrSql,inputParameters)
                temValues = temCursor.fetchall()
                self.attColumnNames = temCursor.description
                self.attConnection.commit()
        return temValues
