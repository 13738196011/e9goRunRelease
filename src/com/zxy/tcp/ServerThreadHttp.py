#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import socket,threading,time
from concurrent.futures import ThreadPoolExecutor,as_completed
from com.zxy.z_debug import z_debug
from com.zxy.tcp.ServerHandlerHttp import ServerHandlerHttp
from com.zxy.common import Com_Para
class ServerThreadHttp(z_debug):
    attStrValue = ""
    attStrNum = ""
    attPort = 0
    def __init__(self, inputStrValue,inputNum,inputPort):
        self.attStrNum = inputNum
        self.attStrValue = inputStrValue
        self.attPort = inputPort
    def run(self):
        temS = None
        try:
            temS = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            temS.bind(('0.0.0.0', self.attPort))
            temS.listen(400)        
            future_list = []
            while True:
                try:
                    sock,addr = temS.accept()
                    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                    temServerHH = ServerHandlerHttp()
                    temServerHH.attServSocket = sock
                    temServerHH.attStrIP = addr
                    t1 = threading.Thread(target=temServerHH.run, name="ServerHttpThread"+"_"+str(addr[0])+"_"+str(addr[1]))
                    t1.start()
                except Exception as en:
                    if str(type(self)) == "<class 'type'>":
                        self.debug_in(self,repr(en))#打印异常信息
                    else:
                        self.debug_in(repr(en))#打印异常信息
                    pass
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
            pass 