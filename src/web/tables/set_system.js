﻿var ly_index;
var sope_type = 0;

function biz_start(){
}

function init() {
    ly_index = layer.load();

    $("#btnGetIP").click(function () {
        ly_index = layer.load();
        sope_type = 1;        
        $("#ip_param_value1").val("");
        $("#ip_param_value2").val("");
        $("#ip_param_value3").val("");
        $("#ip_param_value4").val("");        
        set_ip(1);
    });
    
    $("#btnSetIP").click(function () {
        ly_index = layer.load();
        
        /*
        $("#ip_param_value1").val();
        $("#ip_param_value2").val();
        $("#ip_param_value3").val();
        $("#ip_param_value4").val();
        $("#ip_param_value5").val();
        */
        
        sope_type = 2;
        set_ip(2);
    });
    
    layer.close(ly_index);

    var inputdata = {
            "param_name": "A01_905MFA",
            "session_id": session_id,
            "login_id": login_id
    };
    ly_index = layer.load();
    get_ajax_baseurl(inputdata, "get_A01_905MFA");	
}


function get_A01_905MFA(input) {
    layer.close(ly_index);
    //查询失败
    if (Call_QryResult(input.A01_905MFA) == false)
        return false;
    if(input.A01_905MFA.length > 0)
    {
	    $.each(input.A01_905MFA, function (i, obj) {
	        addOptionValue("A01_905MFA_id", obj[GetLowUpp("prot_name")], obj[GetLowUpp("element_name")]);
	    });
    }
    else
    {
    	
    }    
}

//设备调试
function debug_cmd() {
    //iframe层-父子操作
    layer.open({
        type: 2,
        area: ['1100px', '600px'],
        fixed: false, //不固定
        maxmin: true,
        content: 'debug_cmd_name.html'
    });
};

function A01_RXOB6C(input){
	data = input.A01_RXOB6C;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
}

//设置设备系统时间
function set_dev_time(){
	$.ajax({
	    type: "POST",
	    async: false,
	    url: bizzUrl,
	    data: {
	        param_name: "A01_RXOB6C",
	        session_id: session_id,
	        param_value1:$("#cmdPwd").val(),
	        param_value2:"5",
	        param_value3:new Date().Format('yyyy-MM-dd hh:mm:ss')
	    },
	    //跨域请求的URL
	    dataType: "jsonp",
	    jsonp: "jsoncallback",
	    jsonpCallback: "A01_RXOB6C",
	    success: function (input) {
	    	data = input.A01_RXOB6C;
			layer.close(ly_index);
			//操作失败
			if (Call_OpeResult(data) == false)
				return false;
	    },
	    error: function () {
	        layer.close(ly_index);
	        swal({
	            title: "告警",
	            text: "设备重启中或网络异常，请刷新页面！",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "刷新",
	            closeOnConfirm: false
	        },
	            function () {
	            layer.close(ly_index);
	            window.location.reload();
	        })
	    },
	    // 请求完成后的回调函数 (请求成功或失败之后均调用)
	    complete: function (XMLHttpRequest, textStatus) {
	        $("#doing").empty();
	        $("#doing").attr("style", "display:none");
	    }
	});
}

//获取设备系统时间
function get_dev_time(){
	$.ajax({
	    type: "POST",
	    async: false,
	    url: bizzUrl,
	    data: {
	        param_name: "A01_RXOB6C",
	        session_id: session_id,
	        param_value1:"",
	        param_value2:"4",
		    param_value3:""
	    },
	    //跨域请求的URL
	    dataType: "jsonp",
	    jsonp: "jsoncallback",
	    //jsonpCallback: "A01_RXOB6C",
	    success: function (input) {
	    	data = input.A01_RXOB6C;
			layer.close(ly_index);
			//操作失败
			if (Call_OpeResult(data) == false)
				return false;
			else
	        	$("#dev_time").val(data2[0][GetLowUpp("DateTime")]);
	    },
	    error: function () {
	        layer.close(ly_index);
	        swal({
	            title: "告警",
	            text: "设备重启中或网络异常，请刷新页面！",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "刷新",
	            closeOnConfirm: false
	        },
	            function () {
	            layer.close(ly_index);
	            window.location.reload();
	        })
	    },
	    // 请求完成后的回调函数 (请求成功或失败之后均调用)
	    complete: function (XMLHttpRequest, textStatus) {
	        $("#doing").empty();
	        $("#doing").attr("style", "display:none");
	    }
	});
}

//重置网卡
function reset_neta(){
	$.ajax({
	    type: "POST",
	    async: false,
	    url: bizzUrl,
	    data: {
	        param_name: "A01_RXOB6C",
	        session_id: session_id,
	        param_value1:$("#cmdPwd").val(),
	        param_value2:"3",
		    param_value3:""
	    },
	    //跨域请求的URL
	    dataType: "jsonp",
	    jsonp: "jsoncallback",
	    jsonpCallback: "A01_RXOB6C",
	    success: function (input) {
	    	data = input.A01_RXOB6C;
			layer.close(ly_index);
			//操作失败
			if (Call_OpeResult(data) == false)
				return false;
	    },
	    error: function () {
	        layer.close(ly_index);
	        swal({
	            title: "告警",
	            text: "设备重启中或网络异常，请刷新页面！",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "刷新",
	            closeOnConfirm: false
	        },
	            function () {
	            layer.close(ly_index);
	            window.location.reload();
	        })
	    },
	    // 请求完成后的回调函数 (请求成功或失败之后均调用)
	    complete: function (XMLHttpRequest, textStatus) {
	        $("#doing").empty();
	        $("#doing").attr("style", "display:none");
	    }
	});
}

//程序重启
function prog_reboot(){
	$.ajax({
	    type: "POST",
	    async: false,
	    url: bizzUrl,
	    data: {
	        param_name: "A01_RXOB6C",
	        session_id: session_id,
	        param_value1:$("#cmdPwd").val(),
	        param_value2:"2",
		    param_value3:""
	    },
	    //跨域请求的URL
	    dataType: "jsonp",
	    jsonp: "jsoncallback",
	    jsonpCallback: "A01_RXOB6C",
	    success: function (input) {
	    	data = input.A01_RXOB6C;
			layer.close(ly_index);
			//操作失败
			if (Call_OpeResult(data) == false)
				return false;
	    },
	    error: function () {
	        layer.close(ly_index);
	        swal({
	            title: "告警",
	            text: "设备重启中或网络异常，请刷新页面！",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "刷新",
	            closeOnConfirm: false
	        },
	            function () {
	            layer.close(ly_index);
	            window.location.reload();
	        })
	    },
	    // 请求完成后的回调函数 (请求成功或失败之后均调用)
	    complete: function (XMLHttpRequest, textStatus) {
	        $("#doing").empty();
	        $("#doing").attr("style", "display:none");
	    }
	});
}

//开始做样
function Do_sample(){
    var inputdata = {
            "param_name": "A01_905MFA_DOSAMP",
            "session_id": session_id,
            "login_id": login_id,
            "param_value1": $("#cmdPwd").val(),
            "param_value2": $("#A01_905MFA_id").val()
    };
    ly_index = layer.load();
    get_ajax_baseurl(inputdata, "get_A01_905MFA_DOSAMP");	
}

//反控做样指令
function get_A01_905MFA_DOSAMP(input){
	layer.close(ly_index);
    //查询失败
    if (Call_OpeResult(input.A01_905MFA_DOSAMP) == false)
        return false;
}

//tnp做样
function tnp_sample(){
	$.ajax({
	    type: "POST",
	    async: false,
	    url: bizzUrl,
	    data: {
	        param_name: "A01_RXOB6C",
	        session_id: session_id,
	        param_value1:$("#cmdPwd").val(),
	        param_value2:"9",
		    param_value3:""
	    },
	    //跨域请求的URL
	    dataType: "jsonp",
	    jsonp: "jsoncallback",
	    jsonpCallback: "A01_RXOB6C",
	    success: function (input) {
	    },
	    error: function () {
	        layer.close(ly_index);
	        swal({
	            title: "告警",
	            text: "设备重启中或网络异常，请刷新页面！",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "刷新",
	            closeOnConfirm: false
	        },
	            function () {
	            layer.close(ly_index);
	            window.location.reload();
	        })
	    },
	    // 请求完成后的回调函数 (请求成功或失败之后均调用)
	    complete: function (XMLHttpRequest, textStatus) {
	        $("#doing").empty();
	        $("#doing").attr("style", "display:none");
	    }
	});
}

//nh3做样
function nh3_sample(){
	$.ajax({
	    type: "POST",
	    async: false,
	    url: bizzUrl,
	    data: {
	        param_name: "A01_RXOB6C",
	        session_id: session_id,
	        param_value1:$("#cmdPwd").val(),
	        param_value2:"8",
		    param_value3:""
	    },
	    //跨域请求的URL
	    dataType: "jsonp",
	    jsonp: "jsoncallback",
	    jsonpCallback: "A01_RXOB6C",
	    success: function (input) {
	    },
	    error: function () {
	        layer.close(ly_index);
	        swal({
	            title: "告警",
	            text: "设备重启中或网络异常，请刷新页面！",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "刷新",
	            closeOnConfirm: false
	        },
	            function () {
	            layer.close(ly_index);
	            window.location.reload();
	        })
	    },
	    // 请求完成后的回调函数 (请求成功或失败之后均调用)
	    complete: function (XMLHttpRequest, textStatus) {
	        $("#doing").empty();
	        $("#doing").attr("style", "display:none");
	    }
	});
}


//COD做样
function cod_sample(){
	$.ajax({
	    type: "POST",
	    async: false,
	    url: bizzUrl,
	    data: {
	        param_name: "A01_RXOB6C",
	        session_id: session_id,
	        param_value1:$("#cmdPwd").val(),
	        param_value2:"7",
		    param_value3:""
	    },
	    //跨域请求的URL
	    dataType: "jsonp",
	    jsonp: "jsoncallback",
	    jsonpCallback: "A01_RXOB6C",
	    success: function (input) {
	    	data = input.A01_RXOB6C;
			layer.close(ly_index);
			//操作失败
			if (Call_OpeResult(data) == false)
				return false;
	    },
	    error: function () {
	        layer.close(ly_index);
	        swal({
	            title: "告警",
	            text: "设备重启中或网络异常，请刷新页面！",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "刷新",
	            closeOnConfirm: false
	        },
	            function () {
	            layer.close(ly_index);
	            window.location.reload();
	        })
	    },
	    // 请求完成后的回调函数 (请求成功或失败之后均调用)
	    complete: function (XMLHttpRequest, textStatus) {
	        $("#doing").empty();
	        $("#doing").attr("style", "display:none");
	    }
	});
}

//重启设备
function reboot(){
	$.ajax({
	    type: "POST",
	    async: false,
	    url: bizzUrl,
	    data: {
	        param_name: "A01_RXOB6C",
	        session_id: session_id,
	        param_value1:$("#cmdPwd").val(),
	        param_value2:"6",
		    param_value3:""
	    },
	    //跨域请求的URL
	    dataType: "jsonp",
	    jsonp: "jsoncallback",
	    jsonpCallback: "A01_RXOB6C",
	    success: function (input) {
	    	data = input.A01_RXOB6C;
			layer.close(ly_index);
			//操作失败
			if (Call_OpeResult(data) == false)
				return false;
	    },
	    error: function () {
	        layer.close(ly_index);
	        swal({
	            title: "告警",
	            text: "设备重启中或网络异常，请刷新页面！",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "刷新",
	            closeOnConfirm: false
	        },
	            function () {
	            layer.close(ly_index);
	            window.location.reload();
	        })
	    },
	    // 请求完成后的回调函数 (请求成功或失败之后均调用)
	    complete: function (XMLHttpRequest, textStatus) {
	        $("#doing").empty();
	        $("#doing").attr("style", "display:none");
	    }
	});
}

function set_ip(ope_type)
{
	var sys_ck = $('input:radio[name="system_check"]:checked').val();
    param_name = "";
    if(sys_ck == "1")
    	param_name = "CHANGE_IP_WIN";
    else
    	param_name = "CHANGE_IP_LINUX";
    
    $.ajax({
    	type: "POST",
        async: false,
        url: baseUrl,
        data: {
        	param_name: param_name,
            session_id: session_id,
            param_value1: $("#ip_param_value1").val(),
            param_value2: $("#ip_param_value2").val(),
            param_value3: $("#ip_param_value3").val(),
            param_value4: $("#ip_param_value4").val(),
            param_value5: $("#ip_param_value5").val(),
            param_value6: ope_type,
            param_value7: $("#cmdPwd").val()
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: param_name,
        success: function () {;
            layer.close(ly_index);
        },
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {}
    });
}

function CHANGE_IP_LINUX(input) {
	data = input.CHANGE_IP_LINUX;
    
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
	else {
    	if(sope_type == 1)
    	{
    		$("#ip_param_value1").val(data[0][GetLowUpp("ip")]);
    		$("#ip_param_value2").val(data[0][GetLowUpp("netmask")]);
    		$("#ip_param_value3").val(data[0][GetLowUpp("gateway")]);
    		$("#ip_param_value4").val(data[0][GetLowUpp("dns")]);
    		$("#mac_addr").val(data[0][GetLowUpp("mac")]);
    	}
    }	
    layer.close(ly_index);
}

function CHANGE_IP_WIN(input) {
	data = input.CHANGE_IP_WIN;
    
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
	else {
    	if(sope_type == 1)
    	{
    		$("#ip_param_value1").val(data[0][GetLowUpp("ip")]);
    		$("#ip_param_value2").val(data[0][GetLowUpp("netmask")]);
    		$("#ip_param_value3").val(data[0][GetLowUpp("gateway")]);
    		$("#ip_param_value4").val(data[0][GetLowUpp("dns")]);
    		$("#mac_addr").val(data[0][GetLowUpp("mac")]);
    	}
    }	
    layer.close(ly_index);
}

$(document).ready(function () {
    init_page();
    init();
});
