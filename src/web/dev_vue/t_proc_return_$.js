//按钮事件新增或编辑
var BD21F1_type = "";
//其他页面传到本页面参数
var BD21F1_param = {};
//暂时没用
var BD21F1_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BD21F1_ary_PROC_ID = null;var BD21F1_ary_RETURN_TYPE = null;var BD21F1_ary_IS_IMG = [{'MAIN_ID': '1', 'CN_NAME': '是'}, {'MAIN_ID': '0', 'CN_NAME': '否'}];var BD21F1_ary_IS_URLENCODE = [{'MAIN_ID': '1', 'CN_NAME': '是'}, {'MAIN_ID': '0', 'CN_NAME': '否'}];var BD21F1_ary_LIST_HIDDEN = [{'MAIN_ID': '1', 'CN_NAME': '是'}, {'MAIN_ID': '0', 'CN_NAME': '否'}];
/*declare select options end*/

//主从表传递参数
function BD21F1_param_set(){
	/*Main Subsuv Table Param Begin*/
    if(BD21F1_param.hasOwnProperty("PROC_ID_cn_name"))        $("#BD21F1_find_PROC_ID_cn_name").val(s_decode(BD21F1_param["PROC_ID_cn_name"]));    if(BD21F1_param.hasOwnProperty("PROC_ID"))        $("#BD21F1_find_PROC_ID").val(BD21F1_param["PROC_ID"]);    if(BD21F1_param.hasOwnProperty("hidden_find")){        $("#BD21F1_Ope_PROC_ID").hide();        $("#BD21F1_Clear_PROC_ID").hide();    }	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function BD21F1_t_proc_return_biz_start(inputdata) {
	BD21F1_param = inputdata;
	layer.close(ly_index);
	BD21F1_param_set();
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_proc_inparam$PROC_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BD21F1_get_N01_t_proc_inparam$PROC_ID");	
    /*biz end*/
}

/*biz step begin*/
function BD21F1_format_PROC_ID(value, row, index) {    var objResult = value;    for(i = 0; i < BD21F1_ary_PROC_ID.length; i++) {        var obj = BD21F1_ary_PROC_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BD21F1_format_RETURN_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < BD21F1_ary_RETURN_TYPE.length; i++) {        var obj = BD21F1_ary_RETURN_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BD21F1_format_IS_IMG(value, row, index) {    var objResult = value;    for(i = 0; i < BD21F1_ary_IS_IMG.length; i++) {        var obj = BD21F1_ary_IS_IMG[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BD21F1_format_IS_URLENCODE(value, row, index) {    var objResult = value;    for(i = 0; i < BD21F1_ary_IS_URLENCODE.length; i++) {        var obj = BD21F1_ary_IS_URLENCODE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BD21F1_format_LIST_HIDDEN(value, row, index) {    var objResult = value;    for(i = 0; i < BD21F1_ary_LIST_HIDDEN.length; i++) {        var obj = BD21F1_ary_LIST_HIDDEN[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BD21F1_get_N01_t_proc_inparam$PROC_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_inparam$PROC_ID) == false)        return false;    BD21F1_ary_PROC_ID = input.N01_t_proc_inparam$PROC_ID;    if($("#BD21F1_PROC_ID").is("select") && $("#BD21F1_PROC_ID")[0].options.length == 0)    {        $.each(BD21F1_ary_PROC_ID, function (i, obj) {            addOptionValue("BD21F1_PROC_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    var inputdata = {        "param_name": "N01_t_proc_return$RETURN_TYPE",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BD21F1_get_N01_t_proc_return$RETURN_TYPE");}function BD21F1_get_N01_t_proc_return$RETURN_TYPE(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_return$RETURN_TYPE) == false)        return false;    BD21F1_ary_RETURN_TYPE = input.N01_t_proc_return$RETURN_TYPE;    if($("#BD21F1_RETURN_TYPE").is("select") && $("#BD21F1_RETURN_TYPE")[0].options.length == 0)    {        $.each(BD21F1_ary_RETURN_TYPE, function (i, obj) {            addOptionValue("BD21F1_RETURN_TYPE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#BD21F1_IS_IMG").is("select") && $("#BD21F1_IS_IMG")[0].options.length == 0)    {        $.each(BD21F1_ary_IS_IMG, function (i, obj) {            addOptionValue("BD21F1_IS_IMG", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#BD21F1_IS_URLENCODE").is("select") && $("#BD21F1_IS_URLENCODE")[0].options.length == 0)    {        $.each(BD21F1_ary_IS_URLENCODE, function (i, obj) {            addOptionValue("BD21F1_IS_URLENCODE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#BD21F1_LIST_HIDDEN").is("select") && $("#BD21F1_LIST_HIDDEN")[0].options.length == 0)    {        $.each(BD21F1_ary_LIST_HIDDEN, function (i, obj) {            addOptionValue("BD21F1_LIST_HIDDEN", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    BD21F1_init_t_proc_return();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function BD21F1_PROC_ID_cn_name_fun(){    index_subhtml = "t_proc_name.vue"    random_subhtml = "";    if(loadHtmlSubVueFun("dev_vue/t_proc_name.vue","BD21F1_t_proc_return_call_vue") == true){        var n = Get_RandomDiv("","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#_t_proc_name_Events').bootstrapTable('resetView');                _param["ly_index"] = index;                _param["target_name"] = "BD21F1_find_PROC_ID_cn_name"                _param["target_id"] = "BD21F1_PROC_ID"                _param["sourc_id"] = "MAIN_ID"                _param["sourc_name"] = "INF_CN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function BD21F1_page_end(){
	if(BD21F1_param["type"] == "edit"){
		BD21F1_get_edit_info();
	}
}

//页面初始化方法
function BD21F1_init_t_proc_return() {
	//type = getUrlParam("type");
	if(BD21F1_param["type"] == "add"){
		$("#BD21F1_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BD21F1_PROC_ID").val(BD21F1_param["PROC_ID"]);        $("#BD21F1_find_PROC_ID_cn_name").val(BD21F1_param["PROC_ID_cn_name"]);		
		/*Get Find Select param end*/	
	}
	else if(BD21F1_param["type"] == "edit"){
	
	}
	
	//表单验证
	BD21F1_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BD21F1_CREATE_DATE").val() == "")    {        $("#BD21F1_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BD21F1_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BD21F1_page_end();
}

//提交表单数据
function BD21F1_SubmitForm(){
	if(BD21F1_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_proc_return",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#BD21F1_PROC_ID").val()                ,"param_value2": s_encode($("#BD21F1_RETURN_NAME").val())                ,"param_value3": s_encode($("#BD21F1_RETURN_TYPE").val())                ,"param_value4": s_encode($("#BD21F1_S_DESC").val())                ,"param_value5": $("#BD21F1_CREATE_DATE").val()                ,"param_value6": $("#BD21F1_IS_IMG").val()                ,"param_value7": $("#BD21F1_IS_URLENCODE").val()                ,"param_value8": $("#BD21F1_LIST_HIDDEN").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BD21F1_get_N01_ins_t_proc_return");
	}
	else if(BD21F1_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_proc_return",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#BD21F1_PROC_ID").val()                ,"param_value2": s_encode($("#BD21F1_RETURN_NAME").val())                ,"param_value3": s_encode($("#BD21F1_RETURN_TYPE").val())                ,"param_value4": s_encode($("#BD21F1_S_DESC").val())                ,"param_value5": $("#BD21F1_CREATE_DATE").val()                ,"param_value6": $("#BD21F1_IS_IMG").val()                ,"param_value7": $("#BD21F1_IS_URLENCODE").val()                ,"param_value8": $("#BD21F1_LIST_HIDDEN").val()                ,"param_value9": $("#BD21F1_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BD21F1_get_N01_upd_t_proc_return");
	}
}

//vue回调
function BD21F1_t_proc_return_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_proc_name.vue"){        var n = Get_RandomDiv("",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":BD21F1_type,                    "ly_index":index,                    "target_name":"BD21F1_find_PROC_ID_cn_name",                    "target_id":"BD21F1_find_PROC_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"INF_CN_NAME"                };                loadScript_hasparam("dev_vue/t_proc_name.js","_t_proc_name_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//for表单提交
$("#BD21F1_save_t_proc_return_Edit").click(function () {
	$("form[name='BD21F1_DataModal']").submit();
})

/*修改数据*/
function BD21F1_get_N01_upd_t_proc_return(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_proc_return) == true)
	{
		swal("修改数据成功!", "", "success");
		AD21F1_t_proc_return_query();
		BD21F1_clear_validate();
		layer.close(BD21F1_param["ly_index"]);
	}
}

/*添加数据*/
function BD21F1_get_N01_ins_t_proc_return(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_proc_return) == true)
	{
		swal("添加数据成功!", "", "success");
		AD21F1_t_proc_return_query();
		BD21F1_clear_validate();
		layer.close(BD21F1_param["ly_index"]);
	}
}

//取消编辑
$("#BD21F1_cancel_t_proc_return_Edit").click(function () {
	layer.close(BD21F1_param["ly_index"]);
	BD21F1_clear_validate();
	$("[id^='BD21F1_div']").hide();
})

//清除查找框
function BD21F1_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BD21F1_clear_validate(){
	$("#BD21F1_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BD21F1_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BD21F1_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BD21F1_clear_edit_info(){
	var inputs = $("#BD21F1_DataModal").find('input');
	var selects = $("#BD21F1_DataModal").find("select");
	var textareas = $("#BD21F1_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
    BD21F1_clear_input_cn_name('BD21F1_find_PROC_ID_cn_name','BD21F1_PROC_ID')	
	/*input validate clear end*/
	BD21F1_init_t_proc_return();
}

//页面输入框赋值
function BD21F1_get_edit_info(){
	var rowData = $("#AD21F1_t_proc_return_Events").bootstrapTable('getData')[AD21F1_select_t_proc_return_rowId];
	var inputs = $("#BD21F1_DataModal").find('input');
	var selects = $("#BD21F1_DataModal").find("select");
	var textareas = $("#BD21F1_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"AD21F1","BD21F1");
}

//form验证
function BD21F1_checkFormInput() {
    BD21F1_validate = $("#BD21F1_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BD21F1_main_id: {}            ,BD21F1_PROC_ID: {}            ,BD21F1_RETURN_NAME: {}            ,BD21F1_RETURN_TYPE: {}            ,BD21F1_S_DESC: {}            ,BD21F1_CREATE_DATE: {date: true,required : true,maxlength:19}            ,BD21F1_IS_IMG: {}            ,BD21F1_IS_URLENCODE: {}            ,BD21F1_LIST_HIDDEN: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BD21F1_main_id: {}            ,BD21F1_PROC_ID: {}            ,BD21F1_RETURN_NAME: {}            ,BD21F1_RETURN_TYPE: {}            ,BD21F1_S_DESC: {}            ,BD21F1_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BD21F1_IS_IMG: {}            ,BD21F1_IS_URLENCODE: {}            ,BD21F1_LIST_HIDDEN: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BD21F1_SubmitForm();
        	return false;
        }
    })
}