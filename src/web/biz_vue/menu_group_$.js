//按钮事件新增或编辑
var B7A26C_type = "";
//其他页面传到本页面参数
var B7A26C_param = {};
//暂时没用
var B7A26C_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var B7A26C_ary_is_ava = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];var B7A26C_ary_role_id = [{'main_id': '1', 'cn_name': '开发者'}, {'main_id': '2', 'cn_name': '系统管理员'}, {'main_id': '3', 'cn_name': '运维人员'}, {'main_id': '4', 'cn_name': '普通用户'}];
/*declare select options end*/

//业务逻辑数据开始
function B7A26C_menu_group_biz_start(inputdata) {
	B7A26C_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    $.each(B7A26C_ary_is_ava, function (i, obj) {        addOptionValue("B7A26C_is_ava", obj[GetLowUpp("main_id")], objGetLowUpp("cn_name")]);    });    $.each(B7A26C_ary_role_id, function (i, obj) {        addOptionValue("B7A26C_role_id", obj[GetLowUpp("main_id")], objGetLowUpp("cn_name")]);    });    B7A26C_init_menu_group()	
    /*biz end*/
}

/*biz step begin*/
function B7A26C_format_is_ava(value, row, index) {    var objResult = value;    for(i = 0; i < B7A26C_ary_is_ava.length; i++) {        var obj = B7A26C_ary_is_ava[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B7A26C_format_role_id(value, row, index) {    var objResult = value;    for(i = 0; i < B7A26C_ary_role_id.length; i++) {        var obj = B7A26C_ary_role_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function B7A26C_page_end(){
	if(B7A26C_param["type"] == "edit"){
		B7A26C_get_edit_info();
	}
}

//页面初始化方法
function B7A26C_init_menu_group() {
	//type = getUrlParam("type");
	if(B7A26C_param["type"] == "add"){
		$("#B7A26C_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
		
		/*Get Find Select param end*/	
	}
	else if(B7A26C_param["type"] == "edit"){
	
	}
	
	//表单验证
	B7A26C_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B7A26C_create_date").val() == "")    {        $("#B7A26C_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B7A26C_create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B7A26C_page_end();
}

//提交表单数据
function B7A26C_SubmitForm(){
	if(B7A26C_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_menu_group",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#B7A26C_group_en_name").val())                ,"param_value2": s_encode($("#B7A26C_group_cn_name").val())                ,"param_value3": s_encode($("#B7A26C_group_icon").val())                ,"param_value4": $("#B7A26C_is_ava").val()                ,"param_value5": s_encode($("#B7A26C_role_id").val())                ,"param_value6": $("#B7A26C_create_date").val()                ,"param_value7": s_encode($("#B7A26C_s_desc").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B7A26C_get_N01_ins_menu_group");
	}
	else if(B7A26C_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_menu_group",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#B7A26C_group_en_name").val())                ,"param_value2": s_encode($("#B7A26C_group_cn_name").val())                ,"param_value3": s_encode($("#B7A26C_group_icon").val())                ,"param_value4": $("#B7A26C_is_ava").val()                ,"param_value5": s_encode($("#B7A26C_role_id").val())                ,"param_value6": $("#B7A26C_create_date").val()                ,"param_value7": s_encode($("#B7A26C_s_desc").val())                ,"param_value8": $("#B7A26C_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B7A26C_get_N01_upd_menu_group");
	}
}

//vue回调
function B7A26C_menu_group_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#B7A26C_save_menu_group_Edit").click(function () {
	$("form[name='B7A26C_DataModal']").submit();
})

/*修改数据*/
function B7A26C_get_N01_upd_menu_group(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_menu_group) == true)
	{
		swal("修改数据成功!", "", "success");
		A7A26C_menu_group_query();
		B7A26C_clear_validate();
		layer.close(B7A26C_param["ly_index"]);
	}
}

/*添加数据*/
function B7A26C_get_N01_ins_menu_group(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_menu_group) == true)
	{
		swal("添加数据成功!", "", "success");
		A7A26C_menu_group_query();
		B7A26C_clear_validate();
		layer.close(B7A26C_param["ly_index"]);
	}
}

//取消编辑
$("#B7A26C_cancel_menu_group_Edit").click(function () {
	layer.close(B7A26C_param["ly_index"]);
	B7A26C_clear_validate();
	$("[id^='B7A26C_div']").hide();
})

//清除查找框
function B7A26C_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B7A26C_clear_validate(){
	$("#B7A26C_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B7A26C_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B7A26C_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B7A26C_clear_edit_info(){
	var inputs = $("#B7A26C_DataModal").find('input');
	var selects = $("#B7A26C_DataModal").find("select");
	var textareas = $("#B7A26C_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	B7A26C_init_menu_group();
}

//页面输入框赋值
function B7A26C_get_edit_info(){
	var rowData = $("#A7A26C_menu_group_Events").bootstrapTable('getData')[A7A26C_select_menu_group_rowId];
	var inputs = $("#B7A26C_DataModal").find('input');
	var selects = $("#B7A26C_DataModal").find("select");
	var textareas = $("#B7A26C_DataModal").find('textarea');
	$.each(selects, function (i, obj) {
		if(typeof(eval("A7A26C_ary_"+obj.id.toString().replace("B7A26C_",""))) == "object")
		{
			$.each(eval("A7A26C_ary_"+obj.id.toString().replace("B7A26C_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("B7A26C_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("B7A26C_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("A7A26C_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#B7A26C_DataModal").find('[id="'+"B7A26C_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("B7A26C_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("B7A26C_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//form验证
function B7A26C_checkFormInput() {
    B7A26C_validate = $("#B7A26C_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B7A26C_main_id: {}            ,B7A26C_group_en_name: {}            ,B7A26C_group_cn_name: {}            ,B7A26C_group_icon: {}            ,B7A26C_is_ava: {}            ,B7A26C_role_id: {}            ,B7A26C_create_date: {date: true,required : true,maxlength:19}            ,B7A26C_s_desc: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B7A26C_main_id: {}            ,B7A26C_group_en_name: {}            ,B7A26C_group_cn_name: {}            ,B7A26C_group_icon: {}            ,B7A26C_is_ava: {}            ,B7A26C_role_id: {}            ,B7A26C_create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B7A26C_s_desc: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B7A26C_SubmitForm();
        	return false;
        }
    })
}