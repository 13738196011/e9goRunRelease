//按钮事件新增或编辑
var B2E07D_type = "";
//其他页面传到本页面参数
var B2E07D_param = {};
//暂时没用
var B2E07D_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var B2E07D_ary_PAGE_ID = null;var B2E07D_ary_PROC_ID = null;var B2E07D_ary_ITEM_TYPE = [{'MAIN_ID': '1', 'CN_NAME': '表格模式'}, {'MAIN_ID': '2', 'CN_NAME': '列表模式'}];
/*declare select options end*/

//业务逻辑数据开始
function B2E07D_t_page_item_biz_start(inputdata) {
	B2E07D_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_page_item$PAGE_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B2E07D_get_N01_t_page_item$PAGE_ID");	
    /*biz end*/
}

/*biz step begin*/
function B2E07D_format_PAGE_ID(value, row, index) {    var objResult = value;    for(i = 0; i < B2E07D_ary_PAGE_ID.length; i++) {        var obj = B2E07D_ary_PAGE_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B2E07D_format_PROC_ID(value, row, index) {    var objResult = value;    for(i = 0; i < B2E07D_ary_PROC_ID.length; i++) {        var obj = B2E07D_ary_PROC_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B2E07D_format_ITEM_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < B2E07D_ary_ITEM_TYPE.length; i++) {        var obj = B2E07D_ary_ITEM_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B2E07D_get_N01_t_page_item$PAGE_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_page_item$PAGE_ID) == false)        return false;    B2E07D_ary_PAGE_ID = input.N01_t_page_item$PAGE_ID;    if($("#B2E07D_PAGE_ID").is("select") && $("#B2E07D_PAGE_ID")[0].options.length == 0)    {        $.each(B2E07D_ary_PAGE_ID, function (i, obj) {            addOptionValue("B2E07D_PAGE_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    var inputdata = {        "param_name": "N01_t_proc_inparam$PROC_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B2E07D_get_N01_t_proc_inparam$PROC_ID");}function B2E07D_get_N01_t_proc_inparam$PROC_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_inparam$PROC_ID) == false)        return false;    B2E07D_ary_PROC_ID = input.N01_t_proc_inparam$PROC_ID;    if($("#B2E07D_PROC_ID").is("select") && $("#B2E07D_PROC_ID")[0].options.length == 0)    {        $.each(B2E07D_ary_PROC_ID, function (i, obj) {            addOptionValue("B2E07D_PROC_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#B2E07D_ITEM_TYPE").is("select") && $("#B2E07D_ITEM_TYPE")[0].options.length == 0)    {        $.each(B2E07D_ary_ITEM_TYPE, function (i, obj) {            addOptionValue("B2E07D_ITEM_TYPE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    B2E07D_init_t_page_item();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function B2E07D_PROC_ID_cn_name_fun(){    index_subhtml = "t_proc_name.vue"    random_subhtml = "A1BCF7";    if(loadHtmlSubVueFun("biz_vue/t_proc_name.vue","B2E07D_t_page_item_call_vue") == true){        var n = Get_RandomDiv("A1BCF7","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#A1BCF7_t_proc_name_Events').bootstrapTable('resetView');                A1BCF7_param["ly_index"] = index;                A1BCF7_param["target_name"] = "B2E07D_find_PROC_ID_cn_name"                A1BCF7_param["target_id"] = "B2E07D_PROC_ID"                A1BCF7_param["sourc_id"] = "MAIN_ID"                A1BCF7_param["sourc_name"] = "INF_CN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function B2E07D_page_end(){
	if(B2E07D_param["type"] == "edit"){
		B2E07D_get_edit_info();
	}
}

//页面初始化方法
function B2E07D_init_t_page_item() {
	//type = getUrlParam("type");
	if(B2E07D_param["type"] == "add"){
		$("#B2E07D_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#B2E07D_PAGE_ID").val(B2E07D_param["PAGE_ID"]);        $("#B2E07D_PROC_ID").val(B2E07D_param["PROC_ID"]);        $("#B2E07D_find_PROC_ID_cn_name").val(B2E07D_param["PROC_ID_cn_name"]);		
		/*Get Find Select param end*/
	
	}
	else if(B2E07D_param["type"] == "edit"){
	
	}
	
	//表单验证
	B2E07D_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B2E07D_CREATE_DATE").val() == "")    {        $("#B2E07D_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B2E07D_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B2E07D_page_end();
}

//提交表单数据
function B2E07D_SubmitForm(){
	if(B2E07D_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_page_item",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#B2E07D_PAGE_ID").val()                ,"param_value2": $("#B2E07D_PROC_ID").val()                ,"param_value3": s_encode($("#B2E07D_ITEM_NAME").val())                ,"param_value4": $("#B2E07D_ITEM_TYPE").val()                ,"param_value5": $("#B2E07D_COLUMN_COUNT").val()                ,"param_value6": $("#B2E07D_CREATE_DATE").val()                ,"param_value7": s_encode($("#B2E07D_S_DESC").val())                ,"param_value8": s_encode($("#B2E07D_URL_IN_PARAM").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B2E07D_get_N01_ins_t_page_item");
	}
	else if(B2E07D_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_page_item",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#B2E07D_PAGE_ID").val()                ,"param_value2": $("#B2E07D_PROC_ID").val()                ,"param_value3": s_encode($("#B2E07D_ITEM_NAME").val())                ,"param_value4": $("#B2E07D_ITEM_TYPE").val()                ,"param_value5": $("#B2E07D_COLUMN_COUNT").val()                ,"param_value6": $("#B2E07D_CREATE_DATE").val()                ,"param_value7": s_encode($("#B2E07D_S_DESC").val())                ,"param_value8": s_encode($("#B2E07D_URL_IN_PARAM").val())                ,"param_value9": $("#B2E07D_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B2E07D_get_N01_upd_t_page_item");
	}
}

//vue回调
function B2E07D_t_page_item_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_proc_name.vue"){        var n = Get_RandomDiv("A1BCF7",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":B2E07D_type,                    "ly_index":index,                    "target_name":"B2E07D_find_PROC_ID_cn_name",                    "target_id":"B2E07D_find_PROC_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"INF_CN_NAME"                };                loadScript_hasparam("biz_vue/t_proc_name.js","A1BCF7_t_proc_name_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//for表单提交
$("#B2E07D_save_t_page_item_Edit").click(function () {
	$("form[name='B2E07D_DataModal']").submit();
})

/*修改数据*/
function B2E07D_get_N01_upd_t_page_item(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_page_item) == true)
	{
		swal("修改数据成功!", "", "success");
		A2E07D_t_page_item_query();
		B2E07D_clear_validate();
		layer.close(B2E07D_param["ly_index"]);
	}
}

/*添加数据*/
function B2E07D_get_N01_ins_t_page_item(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_page_item) == true)
	{
		swal("添加数据成功!", "", "success");
		A2E07D_t_page_item_query();
		B2E07D_clear_validate();
		layer.close(B2E07D_param["ly_index"]);
	}
}

//取消编辑
$("#B2E07D_cancel_t_page_item_Edit").click(function () {
	layer.close(B2E07D_param["ly_index"]);
	B2E07D_clear_validate();
	$("[id^='B2E07D_div']").hide();
})

//清除查找框
function B2E07D_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B2E07D_clear_validate(){
	$("#B2E07D_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B2E07D_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B2E07D_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B2E07D_clear_edit_info(){
	var inputs = $("#B2E07D_DataModal").find('input');
	var selects = $("#B2E07D_DataModal").find("select");
	var textareas = $("#B2E07D_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
    B2E07D_clear_input_cn_name('B2E07D_find_PROC_ID_cn_name','B2E07D_PROC_ID')	
	/*input validate clear end*/
	B2E07D_init_t_page_item();
}

//页面输入框赋值
function B2E07D_get_edit_info(){
	var rowData = $("#A2E07D_t_page_item_Events").bootstrapTable('getData')[A2E07D_select_t_page_item_rowId];
	var inputs = $("#B2E07D_DataModal").find('input');
	var selects = $("#B2E07D_DataModal").find("select");
	var textareas = $("#B2E07D_DataModal").find('textarea');
	$.each(selects, function (i, obj) {
		if(typeof(eval("A2E07D_ary_"+obj.id.toString().replace("B2E07D_",""))) == "object")
		{
			$.each(eval("A2E07D_ary_"+obj.id.toString().replace("B2E07D_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("B2E07D_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("B2E07D_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("A2E07D_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#B2E07D_DataModal").find('[id="'+"B2E07D_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("B2E07D_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("B2E07D_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//form验证
function B2E07D_checkFormInput() {
    B2E07D_validate = $("#B2E07D_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B2E07D_main_id: {}            ,B2E07D_PAGE_ID: {}            ,B2E07D_PROC_ID: {}            ,B2E07D_ITEM_NAME: {}            ,B2E07D_ITEM_TYPE: {}            ,B2E07D_COLUMN_COUNT: {digits: true,required : true,maxlength:10}            ,B2E07D_CREATE_DATE: {date: true,required : true,maxlength:19}            ,B2E07D_S_DESC: {}            ,B2E07D_URL_IN_PARAM: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B2E07D_main_id: {}            ,B2E07D_PAGE_ID: {}            ,B2E07D_PROC_ID: {}            ,B2E07D_ITEM_NAME: {}            ,B2E07D_ITEM_TYPE: {}            ,B2E07D_COLUMN_COUNT: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,B2E07D_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B2E07D_S_DESC: {}            ,B2E07D_URL_IN_PARAM: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B2E07D_SubmitForm();
        	return false;
        }
    })
}