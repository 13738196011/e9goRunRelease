#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import os
import time
from com.zxy.z_debug import z_debug
class SetRaspberryPiWiFi(z_debug):
    def __init__(self, params):
        pass
    """
    设置WIFI信息
    """
    def update_wifi(self,ssid="",password=""):
        try:
            wifi_config=open('/etc/wpa_supplicant/wpa_supplicant.conf','w')
            wifi_config_info=(  'network={\n'+
                                'ssid="'+ssid+'"\n'
                                'key_mgmt=WPA-PSK\n'+
                                'psk="'+password+'"\n'+
                                'priority=100\n'+
                                '}\n')
            wifi_config.write(wifi_config_info)
            wifi_config.close()
            os.system('/etc/init.d/networking restart')
            os.system('ifup wlan0')
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
            time.sleep(0.1)
            return (False)