//选择某一行
var ADC767_select_t_protocol_rowId = "";
//按钮事件新增或编辑
var ADC767_type = "";
//其他页面传到本页面参数
var ADC767_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var ADC767_tem_prot_name = "";var ADC767_tem_oem_name = "";var ADC767_tem_s_state = "0";var ADC767_tem_com_port = "";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var ADC767_ary_s_state = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];var ADC767_ary_com_port = null;
/*declare select options end*/

//业务逻辑数据开始
function ADC767_t_protocol_biz_start(inputparam) {
	layer.close(ly_index);
	ADC767_param = inputparam;
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_protocol$com_port",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "ADC767_get_N01_t_protocol$com_port");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function ADC767_format_s_state(value, row, index) {    var objResult = value;    for(i = 0; i < ADC767_ary_s_state.length; i++) {        var obj = ADC767_ary_s_state[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function ADC767_format_com_port(value, row, index) {    var objResult = value;    for(i = 0; i < ADC767_ary_com_port.length; i++) {        var obj = ADC767_ary_com_port[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function ADC767_get_N01_t_protocol$com_port(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_protocol$com_port) == false)        return false;    ADC767_ary_com_port = input.N01_t_protocol$com_port;    $("#ADC767_qry_s_state").append("<option value='-1'></option>")    $.each(ADC767_ary_s_state, function (i, obj) {        addOptionValue("ADC767_qry_s_state", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    $("#ADC767_qry_com_port").append("<option value='-1'></option>")    $.each(ADC767_ary_com_port, function (i, obj) {        addOptionValue("ADC767_qry_com_port", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    ADC767_init_t_protocol();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function ADC767_page_end(){
}

//采集协议设置显示列定义
var ADC767_t_protocol = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '采集协议模块',        field: 'PROT_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '采集协议名称',        field: 'OEM_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '是否启用',        field: 'S_STATE',        sortable: true        ,formatter: ADC767_format_s_state    },    {        title: '串口号',        field: 'COM_PORT',        sortable: true        ,formatter: ADC767_format_com_port    }	
	/*table column end*/
];

//页面初始化
function ADC767_init_t_protocol() {
	$(window).resize(function () {
		  $('#ADC767_t_protocol_Events').bootstrapTable('resetView');
	});
	//采集协议设置查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#ADC767_btn_t_protocol_query').click();
}

//查询接口
function ADC767_t_protocol_query() {
    $('#ADC767_t_protocol_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_protocol",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(ADC767_tem_prot_name)        ,"param_value2": s_encode(ADC767_tem_oem_name)        ,"param_value3": ADC767_tem_s_state        ,"param_value4": ADC767_tem_s_state        ,"param_value5": s_encode(ADC767_tem_com_port)		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "ADC767_get_N01_sel_t_protocol");
}

//查询结果
function ADC767_get_N01_sel_t_protocol(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_protocol) == false)
        return false;    
	var s_data = input.N01_sel_t_protocol;
    ADC767_select_t_protocol_rowId = "";
    $('#ADC767_t_protocol_Events').bootstrapTable('destroy');
    $("#ADC767_t_protocol_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#ADC767_t_protocol_Events').find("tr.changeColor").removeClass('changeColor');
                ADC767_select_t_protocol_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#ADC767_t_protocol_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                ADC767_select_t_protocol_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(ADC767_param.hasOwnProperty("target_name"))
			{
				$("#"+ADC767_param["target_id"]).val(eval("row."+ADC767_param["sourc_id"].toUpperCase()));
				$("#"+ADC767_param["target_name"]).val(s_decode(eval("row."+ADC767_param["sourc_name"].toUpperCase())));				
				layer.close(ADC767_param["ly_index"]);
			}
		},
        toolbar: "#ADC767_t_protocol_Toolbar",
        columns: ADC767_t_protocol,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#ADC767_btn_t_protocol_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    ADC767_tem_prot_name = "";    ADC767_tem_oem_name = "";    ADC767_tem_s_state = "0";    ADC767_tem_com_port = "";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    BDC767_ary_com_port = null;
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_protocol$com_port",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "ADC767_get_N01_t_protocol$com_port");	
    /*biz end*/
})

//查询按钮
$('#ADC767_btn_t_protocol_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    ADC767_tem_prot_name = $("#ADC767_qry_prot_name").val();    ADC767_tem_oem_name = $("#ADC767_qry_oem_name").val();    ADC767_tem_s_state = $("#ADC767_qry_s_state").val();    ADC767_tem_com_port = $("#ADC767_qry_com_port").val();	
    /*set query param end*/
	ADC767_t_protocol_query();
})

//vue回调
function ADC767_t_protocol_call_vue(objResult){
	if(index_subhtml == "t_protocol_$.vue")
	{
		var n = Get_RandomDiv("BDC767",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":ADC767_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temprot_name = $("#ADC767_qry_prot_name").val();                if(temprot_name != ""){                    inputdata["prot_name"] = temprot_name;                }                var temoem_name = $("#ADC767_qry_oem_name").val();                if(temoem_name != ""){                    inputdata["oem_name"] = temoem_name;                }                var tems_state = $("#ADC767_qry_s_state").val();                if(tems_state != ""){                    inputdata["s_state"] = tems_state;                }                var temcom_port = $("#ADC767_qry_com_port").val();                if(temcom_port != ""){                    inputdata["com_port"] = temcom_port;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_protocol_$.js","BDC767_t_protocol_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//新增按钮
$("#ADC767_btn_t_protocol_add").click(function () {
	ADC767_type = "add";
	index_subhtml = "t_protocol_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_protocol_$.vue","ADC767_t_protocol_call_vue") == true){
		var n = Get_RandomDiv("BDC767","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BDC767_param["type"] = ADC767_type;
				BDC767_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temprot_name = $("#ADC767_qry_prot_name").val();                if(temprot_name != ""){                    BDC767_param["prot_name"] = temprot_name;                }                var temoem_name = $("#ADC767_qry_oem_name").val();                if(temoem_name != ""){                    BDC767_param["oem_name"] = temoem_name;                }                var tems_state = $("#ADC767_qry_s_state").val();                if(tems_state != ""){                    BDC767_param["s_state"] = tems_state;                }                var temcom_port = $("#ADC767_qry_com_port").val();                if(temcom_port != ""){                    BDC767_param["com_port"] = temcom_port;                }				
				/*Send Two FindSelect param end*/

				BDC767_clear_edit_info();
			},
			end: function(){
				BDC767_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#ADC767_btn_t_protocol_edit").click(function () {
	if (ADC767_select_t_protocol_rowId != "") {
		ADC767_type = "edit";
		index_subhtml = "t_protocol_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_protocol_$.vue","ADC767_t_protocol_call_vue") == true){
			var n = Get_RandomDiv("BDC767","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BDC767_param["type"] = ADC767_type;
					BDC767_param["ly_index"] = index;
					BDC767_clear_edit_info();
					BDC767_get_edit_info();
				},
				end: function(){
					BDC767_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#ADC767_btn_t_protocol_delete').click(function () {
	//单行选择
	var rowData = $("#ADC767_t_protocol_Events").bootstrapTable('getData')[ADC767_select_t_protocol_rowId];
	//多行选择
	var rowDatas = ADC767_sel_row_t_protocol();
	if (ADC767_select_t_protocol_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_protocol",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "ADC767_N01_del_t_protocol");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_protocol",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "ADC767_N01_del_t_protocol");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function ADC767_N01_del_t_protocol(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_protocol) == true)
		ADC767_t_protocol_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function ADC767_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function ADC767_sel_row_t_protocol(){
	//获得选中行
	var checkedbox= $("#ADC767_t_protocol_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}