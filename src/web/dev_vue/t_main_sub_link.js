//主从表tab选项卡标志位
var A6FC61_Tab_Flag = -1;
//选择某一行
var A6FC61_select_t_main_sub_link_rowId = "";
//按钮事件新增或编辑
var A6FC61_type = "";
//其他页面传到本页面参数
var A6FC61_param = {};
//table选中数据，如无则默认第一条
var A6FC61_rowCheckData = null;

/*定义查询条件变量*/
/*declare query param begin*/

var A6FC61_tem_TABLE_ID = "-1";var A6FC61_tem_MAIN_TB_NAME = "";var A6FC61_tem_SUB_TB_NAME = "";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var A6FC61_ary_MAIN_TB_NAME = null;var A6FC61_ary_SUB_TB_NAME = null;
/*declare select options end*/

/*绑定show监听事件*/
if(A6FC61_Tab_Flag == "-1"){
	var n = Get_RandomDiv("A6FC61","");
	$(n).bind("show", function(objTag){
		A6FC61_Adjust_Sub_Sequ();
		if(A6FC61_Tab_Flag > 0)
			A6FC61_adjust_tab();		
	});
}

//设置主从表div顺序
function A6FC61_Adjust_Sub_Sequ(){
	var temSubDivs = $("#content-main").find("div[data-id]");
	var MainDiv = $(Get_RandomDiv("A6FC61",""));
	for(i = 0; i < temSubDivs.length; i++) {
		if(A6FC61_Is_Sub_Div(temSubDivs[i].id)){
			$(temSubDivs[i]).before($(MainDiv));
			break;
		}
	}
}

//主从表传递参数
function A6FC61_param_set(){
	/*Main Subsuv Table Param Begin*/
    if(A6FC61_param.hasOwnProperty("MAIN_TB_NAME_cn_name"))        $("#A6FC61_find_MAIN_TB_NAME_cn_name").val(s_decode(A6FC61_param["MAIN_TB_NAME_cn_name"]));    if(A6FC61_param.hasOwnProperty("MAIN_TB_NAME"))        $("#A6FC61_find_MAIN_TB_NAME").val(A6FC61_param["MAIN_TB_NAME"]);    if(A6FC61_param.hasOwnProperty("hidden_find")){        $("#A6FC61_Ope_MAIN_TB_NAME").hide();        $("#A6FC61_Clear_MAIN_TB_NAME").hide();    }
	if(A6FC61_param.hasOwnProperty("TABLE_ID_cn_name"))
        $("#A6FC61_find_TABLE_ID_cn_name").val(A6FC61_param["TABLE_ID_cn_name"]);
	if(A6FC61_param.hasOwnProperty("TABLE_ID"))
        $("#A6FC61_find_TABLE_ID").val(A6FC61_param["TABLE_ID"]);	    if(A6FC61_param.hasOwnProperty("SUB_TB_NAME_cn_name"))        $("#A6FC61_find_SUB_TB_NAME_cn_name").val(s_decode(A6FC61_param["SUB_TB_NAME_cn_name"]));    if(A6FC61_param.hasOwnProperty("SUB_TB_NAME"))        $("#A6FC61_find_SUB_TB_NAME").val(A6FC61_param["SUB_TB_NAME"]);    if(A6FC61_param.hasOwnProperty("hidden_find")){        $("#A6FC61_Ope_SUB_TB_NAME").hide();        $("#A6FC61_Clear_SUB_TB_NAME").hide();    }	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function A6FC61_t_main_sub_link_biz_start(inputparam) {
	layer.close(ly_index);
	A6FC61_param = inputparam;
	//主从表传递参数
	A6FC61_param_set();	
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_main_sub_link$MAIN_TB_NAME",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A6FC61_get_N01_t_main_sub_link$MAIN_TB_NAME");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function A6FC61_format_MAIN_TB_NAME(value, row, index) {    var objResult = value;    for(i = 0; i < A6FC61_ary_MAIN_TB_NAME.length; i++) {        var obj = A6FC61_ary_MAIN_TB_NAME[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A6FC61_format_SUB_TB_NAME(value, row, index) {    var objResult = value;    for(i = 0; i < A6FC61_ary_SUB_TB_NAME.length; i++) {        var obj = A6FC61_ary_SUB_TB_NAME[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A6FC61_get_N01_t_main_sub_link$MAIN_TB_NAME(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_main_sub_link$MAIN_TB_NAME) == false)        return false;    A6FC61_ary_MAIN_TB_NAME = input.N01_t_main_sub_link$MAIN_TB_NAME;    var inputdata = {        "param_name": "N01_t_main_sub_link$SUB_TB_NAME",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A6FC61_get_N01_t_main_sub_link$SUB_TB_NAME");}function A6FC61_get_N01_t_main_sub_link$SUB_TB_NAME(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_main_sub_link$SUB_TB_NAME) == false)        return false;    A6FC61_ary_SUB_TB_NAME = input.N01_t_main_sub_link$SUB_TB_NAME;    A6FC61_init_t_main_sub_link();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function A6FC61_MAIN_TB_NAME_cn_name_fun(){    index_subhtml = "V_VTC001.vue"    random_subhtml = "CC0427";    if(loadHtmlSubVueFun("dev_vue/V_VTC001.vue","A6FC61_t_main_sub_link_call_vue") == true){        var n = Get_RandomDiv("CC0427","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#CC0427_V_VTC001_Events').bootstrapTable('resetView');                CC0427_param["ly_index"] = index;                CC0427_param["target_name"] = "A6FC61_find_MAIN_TB_NAME_cn_name"                CC0427_param["target_id"] = "A6FC61_find_MAIN_TB_NAME"                CC0427_param["sourc_id"] = "MAIN_ID"                CC0427_param["sourc_name"] = "COLUMN_EN_NAME"
				CC0427_param["TABLE_EN_NAME"] = $("#A6FC61_find_TABLE_ID_cn_name").val();	
				CC0427_param_set();
				$('#CC0427_btn_V_VTC001_query').click();				            },            end: function(){                $(n).hide();            }        });     }}function A6FC61_SUB_TB_NAME_cn_name_fun(){    index_subhtml = "V_VTC001.vue"    random_subhtml = "CC0427";    if(loadHtmlSubVueFun("dev_vue/V_VTC001.vue","A6FC61_t_main_sub_link_call_vue") == true){        var n = Get_RandomDiv("CC0427","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#CC0427_V_VTC001_Events').bootstrapTable('resetView');                CC0427_param["ly_index"] = index;                CC0427_param["target_name"] = "A6FC61_find_SUB_TB_NAME_cn_name";                CC0427_param["target_id"] = "A6FC61_find_SUB_TB_NAME";                CC0427_param["sourc_id"] = "MAIN_ID";                CC0427_param["sourc_name"] = "COLUMN_EN_NAME";
				CC0427_param["TABLE_EN_NAME"] = "";
				CC0427_param_set();
				$('#CC0427_btn_V_VTC001_query').click();            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function A6FC61_page_end(){
	A6FC61_adjust_tab();
}

//主从关联表显示列定义
var A6FC61_t_main_sub_link = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '主表名及字段',        field: 'MAIN_TB_NAME',        sortable: true        ,formatter: A6FC61_format_MAIN_TB_NAME    },    {        title: '从表名及字段',        field: 'SUB_TB_NAME',        sortable: true        ,formatter: A6FC61_format_SUB_TB_NAME    },    {        title: '主表关联显示字段',        field: 'MAIN_TITLE_COLUMN',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function A6FC61_init_t_main_sub_link() {
	$(window).resize(function () {
		  $('#A6FC61_t_main_sub_link_Events').bootstrapTable('resetView');
	});
	//主从关联表查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#A6FC61_btn_t_main_sub_link_query').click();
}

//查询接口
function A6FC61_t_main_sub_link_query() {
    $('#A6FC61_t_main_sub_link_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_main_sub_link",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(A6FC61_tem_MAIN_TB_NAME)
        ,"param_value2": A6FC61_tem_TABLE_ID        ,"param_value3": s_encode(A6FC61_tem_MAIN_TB_NAME)        ,"param_value4": s_encode(A6FC61_tem_SUB_TB_NAME)        ,"param_value5": s_encode(A6FC61_tem_SUB_TB_NAME)		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "A6FC61_get_N01_sel_t_main_sub_link");
}

//查询结果
function A6FC61_get_N01_sel_t_main_sub_link(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_main_sub_link) == false)
        return false;
    A6FC61_rowCheckData = null;
    //调整table各列宽度
    $.each(A6FC61_t_main_sub_link, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_t_main_sub_link;
    if(s_data.length > 0)
    	A6FC61_rowCheckData = s_data[0];    
    A6FC61_select_t_main_sub_link_rowId = "";
    A6FC61_Tab_Flag = 0;
    $('#A6FC61_t_main_sub_link_Events').bootstrapTable('destroy');
    $("#A6FC61_t_main_sub_link_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#A6FC61_t_main_sub_link_Events').find("tr.changeColor").removeClass('changeColor');
                A6FC61_select_t_main_sub_link_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#A6FC61_t_main_sub_link_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                A6FC61_select_t_main_sub_link_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#A6FC61_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(A6FC61_param.hasOwnProperty("target_name"))
			{
				$("#"+A6FC61_param["target_id"]).val(eval("row."+A6FC61_param["sourc_id"].toUpperCase()));
				$("#"+A6FC61_param["target_name"]).val(s_decode(eval("row."+A6FC61_param["sourc_name"].toUpperCase())));				
				layer.close(A6FC61_param["ly_index"]);
			}
		},
        toolbar: "#A6FC61_t_main_sub_link_Toolbar",
        columns: A6FC61_t_main_sub_link,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        onPostBody:function(){
        	if(s_data.length != 0)
            	A6FC61_page_end();
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#A6FC61_btn_t_main_sub_link_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/

    A6FC61_tem_MAIN_TB_NAME = "";    A6FC61_tem_SUB_TB_NAME = "";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    B6FC61_ary_MAIN_TB_NAME = null;    B6FC61_ary_SUB_TB_NAME = null;    $("#A6FC61_find_MAIN_TB_NAME_cn_name").val("");    $("#A6FC61_find_MAIN_TB_NAME").val("-1");    $("#A6FC61_find_SUB_TB_NAME_cn_name").val("");    $("#A6FC61_find_SUB_TB_NAME").val("-1");
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_main_sub_link$MAIN_TB_NAME",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A6FC61_get_N01_t_main_sub_link$MAIN_TB_NAME");	
    /*biz end*/
})

//查询按钮
$('#A6FC61_btn_t_main_sub_link_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    A6FC61_tem_MAIN_TB_NAME = $("#A6FC61_find_MAIN_TB_NAME").val();    A6FC61_tem_SUB_TB_NAME = $("#A6FC61_find_SUB_TB_NAME").val();	
	A6FC61_tem_TABLE_ID = $("#A6FC61_find_TABLE_ID").val();
    /*set query param end*/
	A6FC61_t_main_sub_link_query();
})

//vue回调
function A6FC61_t_main_sub_link_call_vue(objResult){
	//选择某条记录或自动选择第一条
	if (A6FC61_select_t_main_sub_link_rowId != "") 
		A6FC61_rowCheckData = $("#A6FC61_t_main_sub_link_Events").bootstrapTable('getData')[A6FC61_select_t_main_sub_link_rowId];
	
	if(index_subhtml == "t_main_sub_link_$.vue")
	{
		var n = Get_RandomDiv("B6FC61",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":A6FC61_type,"ly_index":index};
				if(A6FC61_param.hasOwnProperty("hidden_find")){
					inputdata["hidden_find"] = "1";
				}				
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temMAIN_TB_NAME = $("#A6FC61_find_MAIN_TB_NAME_cn_name").val();                if(temMAIN_TB_NAME != ""){                    inputdata["MAIN_TB_NAME_cn_name"] = temMAIN_TB_NAME;                    inputdata["MAIN_TB_NAME"] = $("#A6FC61_MAIN_TB_NAME").val();                }                var temSUB_TB_NAME = $("#A6FC61_find_SUB_TB_NAME_cn_name").val();                if(temSUB_TB_NAME != ""){                    inputdata["SUB_TB_NAME_cn_name"] = temSUB_TB_NAME;                    inputdata["SUB_TB_NAME"] = $("#A6FC61_SUB_TB_NAME").val();                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("dev_vue/t_main_sub_link_$.js","B6FC61_t_main_sub_link_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "V_VTC001.vue"){        var n = Get_RandomDiv("CC0427",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":A6FC61_type,                    "ly_index":index,                    "target_name":"A6FC61_find_MAIN_TB_NAME_cn_name",                    "target_id":"A6FC61_find_MAIN_TB_NAME",                    "sourc_id":"MAIN_ID",                    "sourc_name":"COLUMN_EN_NAME",
					"TABLE_EN_NAME":$("#A6FC61_find_TABLE_ID_cn_name").val()                };                loadScript_hasparam("dev_vue/V_VTC001.js","CC0427_V_VTC001_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }
	/*    else if(index_subhtml == "V_VTC001.vue"){        var n = Get_RandomDiv("CC0427",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":A6FC61_type,                    "ly_index":index,                    "target_name":"A6FC61_find_SUB_TB_NAME_cn_name",                    "target_id":"A6FC61_find_SUB_TB_NAME",                    "sourc_id":"MAIN_ID",                    "sourc_name":"COLUMN_EN_NAME"                };                loadScript_hasparam("dev_vue/V_VTC001.js","CC0427_V_VTC001_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }*/
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/

	/*get tab subvue end*/
}

//新增按钮
$("#A6FC61_btn_t_main_sub_link_add").click(function () {
	A6FC61_type = "add";
	index_subhtml = "t_main_sub_link_$.vue";
	if(loadHtmlSubVueFun("dev_vue/t_main_sub_link_$.vue","A6FC61_t_main_sub_link_call_vue") == true){
		var n = Get_RandomDiv("B6FC61","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				B6FC61_param["type"] = A6FC61_type;
				B6FC61_param["ly_index"]= index;
				if(A6FC61_param.hasOwnProperty("hidden_find")){
					B6FC61_param["hidden_find"] = "1";
				}
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temMAIN_TB_NAME = $("#A6FC61_find_MAIN_TB_NAME_cn_name").val();                if(temMAIN_TB_NAME != ""){                    B6FC61_param["MAIN_TB_NAME_cn_name"] = temMAIN_TB_NAME;                    B6FC61_param["MAIN_TB_NAME"] = $("#A6FC61_MAIN_TB_NAME").val();                }                var temSUB_TB_NAME = $("#A6FC61_find_SUB_TB_NAME_cn_name").val();                if(temSUB_TB_NAME != ""){                    B6FC61_param["SUB_TB_NAME_cn_name"] = temSUB_TB_NAME;                    B6FC61_param["SUB_TB_NAME"] = $("#A6FC61_SUB_TB_NAME").val();                }				
				/*Send Two FindSelect param end*/

				B6FC61_clear_edit_info();
			},
			end: function(){
				B6FC61_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#A6FC61_btn_t_main_sub_link_edit").click(function () {
	if (A6FC61_select_t_main_sub_link_rowId != "") {
		A6FC61_type = "edit";
		index_subhtml = "t_main_sub_link_$.vue";
		if(loadHtmlSubVueFun("dev_vue/t_main_sub_link_$.vue","A6FC61_t_main_sub_link_call_vue") == true){
			var n = Get_RandomDiv("B6FC61","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					B6FC61_param["type"] = A6FC61_type;
					B6FC61_param["ly_index"] = index;
					if(A6FC61_param.hasOwnProperty("hidden_find")){
						B6FC61_param["hidden_find"] = "1";
					}
					B6FC61_clear_edit_info();
					B6FC61_get_edit_info();
				},
				end: function(){
					B6FC61_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#A6FC61_btn_t_main_sub_link_delete').click(function () {
	//单行选择
	var rowData = $("#A6FC61_t_main_sub_link_Events").bootstrapTable('getData')[A6FC61_select_t_main_sub_link_rowId];
	//多行选择
	var rowDatas = A6FC61_sel_row_t_main_sub_link();
	if (A6FC61_select_t_main_sub_link_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_main_sub_link",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "A6FC61_N01_del_t_main_sub_link");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_main_sub_link",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "A6FC61_N01_del_t_main_sub_link");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function A6FC61_N01_del_t_main_sub_link(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_main_sub_link) == true)
		A6FC61_t_main_sub_link_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function A6FC61_adjust_tab(){
	if(typeof($("#A6FC61_TAB_MAIN")[0]) != "undefined" && $("#A6FC61_TAB_MAIN")[0].length != 0){
		A6FC61_Tab_Flag = 1;
		$(Get_RDivNoBuild("A6FC61","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("A6FC61","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("A6FC61","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#A6FC61_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/
//隐藏tab页选项卡function A6FC61_hide_tab_fun(){    var n = null;}//判断是否sub子项div页面function A6FC61_Is_Sub_Div(temDivId){    if(temDivId.indexOf("XX$TTT") == 0)        return false;    return false;}
/*Tab Click Fun End*/

function A6FC61_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("dev_vue/"+inputUrl,"A6FC61_t_main_sub_link_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (A6FC61_select_t_main_sub_link_rowId != "") 
			rowData = $("#A6FC61_t_main_sub_link_Events").bootstrapTable('getData')[A6FC61_select_t_proc_name_rowId];
		else
			rowData = A6FC61_rowCheckData;
		//$("#A6FC61_t_main_sub_link_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			var temFlag = true;
			$.each(temPar, function (i, obj) {
				if(eval(inputrandom+"_param.hasOwnProperty(\""+obj.target_id+"\")") && eval(inputrandom+"_param[\""+obj.target_id+"\"]").toString() == rowData[obj.sourc_id].toString())
					temFlag = false;
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			if(temFlag){
				//传递子页面隐藏find功能
				eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
				//参数传递并赋值
				eval(inputrandom+"_param_set()");
				var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
				$("#"+inputrandom+"_btn_"+tbName+"_query").click();
			}
		}
		else{
			$.each(temPar, function (i, obj) {
				if(obj.sourc_id.toUpperCase() == "MAIN_ID")
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"-999\"");
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"\"");
			});
			//传递子页面隐藏find功能
			eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
			var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
			$("#"+inputrandom+"_btn_"+tbName+"_query").click();		
		}
	}
}

//清除 查找框
function A6FC61_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function A6FC61_sel_row_t_main_sub_link(){
	//获得选中行
	var checkedbox= $("#A6FC61_t_main_sub_link_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}