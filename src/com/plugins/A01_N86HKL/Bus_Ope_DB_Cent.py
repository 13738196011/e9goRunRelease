#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import json, importlib, os,platform,datetime
from urllib.parse import unquote
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.db_Self.Db_Common_Self import Db_Common_Self
from com.plugins.A01_N86HKL.t_station_info import t_station_info
from com.plugins.A01_N86HKL.t_code_element import t_code_element
from com.plugins.A01_N86HKL.t_plc_addr import t_plc_addr
from com.plugins.A01_N86HKL.plc_addr_element import plc_addr_element
from com.plugins.A01_N86HKL.monitor_data import monitor_data
from com.plugins.A01_N86HKL import A01_Para
class Bus_Ope_DB_Cent(object):
    def __init__(self):
        pass
    def init_page(self):
        self.monitor_had(30)
        self.Get_plc_addr_element("1")
        if A01_Para.objPLC_addr != "" and A01_Para.objPLC_rack == -1:
            self.monitor_had(1)
            return 0        
        temStrSql = "select * from plc_address where main_id = 1"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        try:
            for temItem in temRs:
                A01_Para.objPLC_addr = Com_Fun.NoNull(temItem[1])
                A01_Para.objPLC_rack = Com_Fun.Str_To_Int(temItem[2])  
                A01_Para.objPLC_slot = Com_Fun.Str_To_Int(temItem[3])  
                A01_Para.objPLC_Port = Com_Fun.Str_To_Int(temItem[4])          
            self.monitor_had(1)
        except Exception as e:
            temStr = "init data error [Bus_Ope_DB_Cent.init_page]" + "\r\n" + repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temStr)
            uL.WriteLog()
        finally:
            pass
    def monitor_had(self,iNum):
        temdbc2 = Db_Common2()
        tbName = "data_min"+Com_Fun.DateTimeAdd(datetime.datetime.now(), "d",iNum).strftime('%Y%m')
        strSql = "select count(1) from "+tbName+" limit 1"
        temValue = temdbc2.Common_Sql(strSql)
        if temValue == None:
            strSql = "Create  TABLE "+tbName+"("
            strSql = strSql+"[main_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"
            strSql = strSql+",[cpu_time] datetime NOT NULL DEFAULT (datetime('now'))"
            strSql = strSql+",[automatic_on] varchar(50)"
            strSql = strSql+",[time_base] varchar(50)"
            strSql = strSql+",[door_closed] varchar(50)"
            strSql = strSql+",[extruder_on] varchar(50)"
            strSql = strSql+",[batch_counter] varchar(50)"
            strSql = strSql+",[cycle_time] varchar(50)"
            strSql = strSql+",[tank_weight] varchar(50)"
            strSql = strSql+",[cooling_water_temp] varchar(50)"
            strSql = strSql+",[air_pressure] varchar(50)"
            strSql = strSql+",[update_time] datetime DEFAULT (datetime('now'))"
            strSql = strSql+",[m_flag] int"
            strSql = strSql+",[s_type] varchar(10)"
            strSql = strSql+");"            
            temdbc2.CommonExec_Sql(strSql)
        if iNum == 30:
            return 0
    def Get_plc_addr_element(self,inputT_P_MAIN_ID):
        if len(A01_Para.objplc_addr_element) != 0:
            return None
        temStrSql = "select * from plc_addr_element where p_main_id = '"+inputT_P_MAIN_ID+"'"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        try:
            for temItem in temRs:
                templc_addr_element = plc_addr_element()
                templc_addr_element.attmain_id = Com_Fun.ZeroNull(temItem[0])
                templc_addr_element.attp_main_id = Com_Fun.ZeroNull(temItem[1])
                templc_addr_element.attelement_code = Com_Fun.NoNull(temItem[2])
                templc_addr_element.attdata_type = Com_Fun.NoNull(temItem[3])
                templc_addr_element.attdata_area = Com_Fun.NoNull(temItem[4])
                templc_addr_element.attdb_num = Com_Fun.NoNull(temItem[5])
                templc_addr_element.attaddress = Com_Fun.NoNull(temItem[6])
                templc_addr_element.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[7])                
                Com_Fun.SetHashTable(A01_Para.objplc_addr_element,templc_addr_element.attmain_id,templc_addr_element)                
        except Exception as es:
            temError = "get data error [Get_plc_addr_element]" + temStrSql +"\r\n"+repr(es)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temError)
            uL.WriteLog()
    def Get_monitor_data_minNoUpdate(self,tbName):
        temStrSql = "select * from "+tbName+" where m_flag = '0' order by main_id asc limit 5000"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temmonitor_data = monitor_data()
                vTime = ""
                temmonitor_data.attmain_id = Com_Fun.ZeroNull(temItem[0])
                temmonitor_data.attstation_code = Com_Fun.NoNull(temItem[1])
                temmonitor_data.attget_time = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[2])
                temmonitor_data.attval_01 = Com_Fun.NoNull(temItem[3])
                temmonitor_data.attval_02 = Com_Fun.NoNull(temItem[4])
                temmonitor_data.attval_03 = Com_Fun.NoNull(temItem[5])
                temmonitor_data.attval_04 = Com_Fun.NoNull(temItem[6])
                temmonitor_data.attval_05 = Com_Fun.NoNull(temItem[7])
                temmonitor_data.attval_06 = Com_Fun.NoNull(temItem[8])
                temmonitor_data.attval_07 = Com_Fun.NoNull(temItem[9])
                temmonitor_data.attval_08 = Com_Fun.NoNull(temItem[10])
                temmonitor_data.attval_09 = Com_Fun.NoNull(temItem[11])
                temmonitor_data.attval_10 = Com_Fun.NoNull(temItem[12])
                temmonitor_data.attval_11 = Com_Fun.NoNull(temItem[13])
                temmonitor_data.attval_12 = Com_Fun.NoNull(temItem[14])
                temmonitor_data.attval_13 = Com_Fun.NoNull(temItem[15])
                temmonitor_data.attval_14 = Com_Fun.NoNull(temItem[16])
                temmonitor_data.attval_15 = Com_Fun.NoNull(temItem[17])
                temmonitor_data.attval_16 = Com_Fun.NoNull(temItem[18])
                temmonitor_data.attval_17 = Com_Fun.NoNull(temItem[19])
                temmonitor_data.attval_18 = Com_Fun.NoNull(temItem[20])
                temmonitor_data.attval_19 = Com_Fun.NoNull(temItem[21])
                temmonitor_data.attval_20 = Com_Fun.NoNull(temItem[22])
                if len(temItem[23]) == 10:
                    vTime = " 00:00:00"
                temmonitor_data.attupdate_time = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[23]+vTime)
                temmonitor_data.m_flag = Com_Fun.NoNull(temItem[24])
                temList.append(temmonitor_data)
        except Exception as es:
            temError = "get data error [Get_monitor_data_minNoUpdate]" + temStrSql +"\r\n"+repr(es)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temError)
            uL.WriteLog()
        return temList
    def Ins_data_min(self,inputdata_min):
        temStrSql = "insert into data_min"+Com_Fun.GetDateInput("%Y%m","%Y-%m-%d %H:%M:%S",str(inputdata_min.attcpu_time))  #+Com_Fun.GetTime("%Y%m")
        temStrSql = temStrSql+"(cpu_time,automatic_on,time_base,door_closed,extruder_on,batch_counter,cycle_time,tank_weight,cooling_water_temp,air_pressure,update_time,m_flag,s_type) values("
        temStrSql = temStrSql+"'"+str(inputdata_min.attcpu_time)+"','"+str(inputdata_min.attautomatic_on)+"','"+str(inputdata_min.atttime_base)+"','"+str(inputdata_min.attdoor_closed)
        temStrSql = temStrSql+"','"+str(inputdata_min.attextruder_on)+"','"+str(inputdata_min.attbatch_counter)+"','"+str(inputdata_min.attcycle_time)+"','"+str(inputdata_min.atttank_weight)
        temStrSql = temStrSql+"','"+str(inputdata_min.attcooling_water_temp)+"','"+str(inputdata_min.attair_pressure)+"','"+Com_Fun.GetTimeDef()+"','0','"+inputdata_min.atts_type+"')"       
        temDb2 = Db_Common2()
        temDb2.CommonExec_Sql(temStrSql)
    def Get_t_station_info(self):
        temStrSql = "select * from t_station_info"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temt_station_info = t_station_info()
                vTime = ""
                temt_station_info.attmain_id = Com_Fun.ZeroNull(temItem[0])
                temt_station_info.attstation_code = Com_Fun.NoNull(temItem[1])
                temt_station_info.attstation_name = Com_Fun.NoNull(temItem[2])
                if len(temItem[3]) == 10:
                    vTime = " 00:00:00"
                temt_station_info.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[3]+vTime)
                temt_station_info.atts_desc = Com_Fun.NoNull(temItem[4])
                temList.append(temt_station_info)
        except Exception as es:
            temError = "get data error [Get_t_station_info]" + temStrSql +"\r\n"+repr(es)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temError)
            uL.WriteLog()
        return temList
    def Get_t_plc_addr_by_station_id(self,inputstation_id):
        temStrSql = "select * from t_plc_addr where station_id = '"+inputstation_id+"'"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temt_plc_addr = t_plc_addr()
                temt_plc_addr.attmain_id = Com_Fun.ZeroNull(temItem[0])
                temt_plc_addr.attstation_id = Com_Fun.ZeroNull(temItem[1])
                temt_plc_addr.attplc_area = Com_Fun.NoNull(temItem[2])
                temt_plc_addr.attplc_dbnumber = Com_Fun.ZeroNull(temItem[3])
                temt_plc_addr.attplc_Start = Com_Fun.ZeroNull(temItem[4])
                temt_plc_addr.attplc_size = Com_Fun.ZeroNull(temItem[5])
                temt_plc_addr.attelement_val = Com_Fun.NoNull(temItem[6])
                vTime = ""
                if len(temItem[7]) == 10:
                    vTime = " 00:00:00"
                temt_plc_addr.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[7]+vTime)
                temt_plc_addr.atts_desc = Com_Fun.NoNull(temItem[8])
                temList.append(temt_plc_addr)
        except Exception as es:
            temError = "get data error [Get_t_plc_addr_by_station_id]" + temStrSql +"\r\n"+repr(es)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temError)
            uL.WriteLog()
        return temList
    def Get_t_code_element(self):
        temStrSql = "select * from t_code_element"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temt_code_element = t_code_element()
                vTime = ""
                temt_code_element.attmain_id = Com_Fun.ZeroNull(temItem[0])
                temt_code_element.attelement_code = Com_Fun.NoNull(temItem[1])
                temt_code_element.attelement_name = Com_Fun.NoNull(temItem[2])
                temt_code_element.attelement_val = Com_Fun.NoNull(temItem[3])
                if len(temItem[4]) == 10:
                    vTime = " 00:00:00"
                temt_code_element.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[4]+vTime)
                temt_code_element.atts_desc = Com_Fun.NoNull(temItem[5])
                temList.append(temt_code_element)
        except Exception as es:
            temError = "get data error [Get_t_code_element]" + temStrSql +"\r\n"+repr(es)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temError)
            uL.WriteLog()
        return temList
    def Get_t_plc_addr(self):
        temStrSql = "select * from t_plc_addr"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temt_plc_addr = t_plc_addr()
                temt_plc_addr.attmain_id = Com_Fun.ZeroNull(temItem[0])
                temt_plc_addr.attstation_id = Com_Fun.ZeroNull(temItem[1])
                temt_plc_addr.attplc_area = Com_Fun.NoNull(temItem[2])
                temt_plc_addr.attplc_dbnumber = Com_Fun.ZeroNull(temItem[3])
                temt_plc_addr.attplc_Start = Com_Fun.ZeroNull(temItem[4])
                temt_plc_addr.attplc_size = Com_Fun.ZeroNull(temItem[5])
                temt_plc_addr.attelement_val = Com_Fun.NoNull(temItem[6])
                vTime = ""
                if len(temItem[7]) == 10:
                    vTime = " 00:00:00"
                temt_plc_addr.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[7]+vTime)
                temt_plc_addr.atts_desc = Com_Fun.NoNull(temItem[8])
                temList.append(temt_plc_addr)
        except Exception as es:
            temError = "get data error [Get_t_plc_addr]" + temStrSql +"\r\n"+repr(es)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temError)
            uL.WriteLog()
        return temList
