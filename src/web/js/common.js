//配置接口url
var baseUrl = "/CenterData?sub_code=8A0731CC39614C90A5D474BC17253713&sub_usercode=414A6DB3BBE6419DA3768E6E25127310";
//业务接口url
var bizzUrl = "/CenterData?sub_code=FF0731CC39614C90A5D474BC17253713&sub_usercode=114A6DB3BBE6419DA3768E6E25127310";
var session_id = "",login_id = "",nickname="",s_title = "";
var rid = 0,winWidth = 0, winHeight = 0,is_history_back = 0,iReturnUppLower=1;
var ly_index;
//路由设置
var htmlRoute = [];
//js或css资源加载
var JSCSSRes = null;

//读取模板vue对象内容
function ReadCommonRes(objResult){
	//回调call_vue函数
	if(call_vue_funcName == "")
		callbackFuction("index_call_vue", objResult);
	else
		callbackFuction(call_vue_funcName, objResult);
}

//读取css或js资源内容
function ReadJSCSSRes(objResult){
	//动态加载js或css
	JSCSSRes = objResult;
	
	$.each(JSCSSRes.CSS_RESOURCE, function (i, obj) {
		loadStyles(obj.css_url);
	});
	$.each(JSCSSRes.JS_RESOURCE, function (i, obj) {
		loadScript(obj.js_url);
	});
}

//获取root路径
function getRootPath() {
    var curWwwPath = window.document.location.href;
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0, pos);
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    if (baseUrl.indexOf("getdata.jsp") != -1)
        return (localhostPath + projectName);
    else
        return localhostPath;
}

//获取IP地址
function getIP_Url() {
    var a = window.location.href.replace("http://", "");
    return a.substring(0, a.indexOf("/")) + baseUrl.substring(0, baseUrl.indexOf("?"));
}

//页面初始化
function init_page() {
    //获取session_id
    session_id = localStorage.getItem('session_id');
    if (session_id == null)
        session_id = "";
    login_id = localStorage.getItem('login_id');
    if (login_id == null)
        login_id = "";
    nickname = localStorage.getItem('nickname');
    if (nickname == null)
        nickname = "";
    rid = localStorage.getItem('rid');
    if (rid == null)
        rid = "";
    if (s_title == null)
    	s_title = "";
    s_title = localStorage.getItem('s_title');
    $("title").html("欢迎使用"+s_title);
    //判断服务端session_id是否超时
    T01_SELSSS();
}

// 获取用户的session_id
function getUrlParam(k) {
    var regExp = new RegExp('([?]|&)' + k + '=([^&]*)(&|$)');
    var result = window.location.href.match(regExp);
    if (result) {
        return decodeURIComponent(result[2]);
    } else {
        return null;
    }
}

//获取当前页面名称
function getPageUrl(){
	 var str = window.location.pathname;
	 str = str.substr(str.lastIndexOf('/', str.lastIndexOf('/') - 1) + 1);
	 str = str.substr(0,str.lastIndexOf('.html', str.lastIndexOf('.html')));
	 str = str.replaceAll("/","_")+"_";
	 return str;
}

//大小写转换
function GetLowUpp(inputValue){
    if(iReturnUppLower == 1)
        return inputValue.toUpperCase();
    else if(iReturnUppLower == 2)
        return inputValue.toLowerCase();
    else
        return inputValue;
}

//用户登录信息结果
function T01_SELSSS_Result(input) {
	layer.close(ly_index);    
    var s_result = "";
    var error_desc = "";
    for (var key in input.T01_SELSSS[0]) {
        if (key == GetLowUpp('s_result')) {
            s_result = input.T01_SELSSS[0][GetLowUpp("s_result")];
            error_desc = input.T01_SELSSS[0][GetLowUpp("error_desc")];
        }
    }
    if (s_result != "1") {
        window.location.href = getRootPath() + "/login.html";
    } 
    else 
    {
        //函数回调
        biz_start();
    }
}

//获取用户登录信息
function T01_SELSSS() {
	var inputdata = {param_name:"T01_SELSSS",session_id: session_id};
	get_ajax_staticurl(inputdata,"T01_SELSSS_Result");
}

//时间格式化
Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

//时间格式化2
function dateFmt(fmt, date) { //author: meizz
    var o = {
        "M+": date.getMonth() + 1, //月份
        "d+": date.getDate(), //日
        "h+": date.getHours(), //小时
        "m+": date.getMinutes(), //分
        "s+": date.getSeconds(), //秒
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度
        "S": date.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

//特殊字符解析
function js_decode(s) {
	if(typeof(s) != "undefined" && s != "")
	{
	    while (s.indexOf('+') >= 0)
	        s = s.replace('+', '%20');
	}
    return s;
}

//编码
function s_encode(s_result) {
    //对应后台解码 python encodeURIComponent(s_result);//java   encodeURIComponent(s_result).replace(/%20/g,"+");
    return encodeURIComponent(s_result);
}

//解码
function s_decode(s_result) {
	var vR = js_decode(s_result);
	if(vR != "\%")
		return decodeURIComponent(vR);
	return vR;
}

//窗口大小
function getWindowSize() {
    //获取窗口宽度
    if (window.innerWidth) { //兼容火狐，谷歌,safari等浏览器
        winWidth = window.innerWidth;
    } else if ((document.body) && (document.body.clientWidth)) { //兼容IE浏览器
        winWidth = document.body.clientWidth;
    }

    //获取窗口高度
    if (window.innerHeight) {
        winHeight = window.innerHeight;
    } else if ((document.body) && (document.body.clientHeight)) {
        winHeight = document.body.clientHeight;
    }
}

//获取IP及端口
function getIP2() {
    var a = window.location.href.replace("http://", "");
    var a_ary = a.substring(0, a.indexOf("/")).split(':');
    return a_ary[0];
}

//GUID
function GUID() {
    this.date = new Date();
    /* 判断是否初始化过，如果初始化过以下代码，则以下代码将不再执行，实际中只执行一次 */
    if (typeof this.newGUID != 'function') {
        /* 生成GUID码 */
        GUID.prototype.newGUID = function () {
            this.date = new Date();
            var guidStr = '';
            sexadecimalDate = this.hexadecimal(this.getGUIDDate(), 16);
            sexadecimalTime = this.hexadecimal(this.getGUIDTime(), 16);
            for (var i = 0; i < 9; i++) {
                guidStr += Math.floor(Math.random() * 16).toString(16);
            }
            guidStr += sexadecimalDate;
            guidStr += sexadecimalTime;
            while (guidStr.length < 32) {
                guidStr += Math.floor(Math.random() * 16).toString(16);
            }
            return this.formatGUID(guidStr);
        }
        /* * 功能：获取当前日期的GUID格式，即8位数的日期：19700101 * 返回值：返回GUID日期格式的字条串 */
        GUID.prototype.getGUIDDate = function () {
            return this.date.getFullYear() + this.addZero(this.date.getMonth() + 1) + this.addZero(this.date.getDay());
        }
        /* * 功能：获取当前时间的GUID格式，即8位数的时间，包括毫秒，毫秒为2位数：12300933 * 返回值：返回GUID日期格式的字条串 */
        GUID.prototype.getGUIDTime = function () {
            return this.addZero(this.date.getHours()) + this.addZero(this.date.getMinutes()) + this.addZero(this.date.getSeconds()) + this.addZero(parseInt(this.date.getMilliseconds() / 10));
        }
        /* * 功能: 为一位数的正整数前面添加0，如果是可以转成非NaN数字的字符串也可以实现 * 参数: 参数表示准备再前面添加0的数字或可以转换成数字的字符串 * 返回值: 如果符合条件，返回添加0后的字条串类型，否则返回自身的字符串 */
        GUID.prototype.addZero = function (num) {
            if (Number(num).toString() != 'NaN' && num >= 0 && num < 10) {
                return '0' + Math.floor(num);
            } else {
                return num.toString();
            }
        }
        /*  * 功能：将y进制的数值，转换为x进制的数值 * 参数：第1个参数表示欲转换的数值；第2个参数表示欲转换的进制；第3个参数可选，表示当前的进制数，如不写则为10 * 返回值：返回转换后的字符串 */
        GUID.prototype.hexadecimal = function (num, x, y) {
            if (y != undefined) {
                return parseInt(num.toString(), y).toString(x);
            } else {
                return parseInt(num.toString()).toString(x);
            }
        }
        /* * 功能：格式化32位的字符串为GUID模式的字符串 * 参数：第1个参数表示32位的字符串 * 返回值：标准GUID格式的字符串 */
        GUID.prototype.formatGUID = function (guidStr) {
            var str1 = guidStr.slice(0, 8),
            str2 = guidStr.slice(8, 12),
            str3 = guidStr.slice(12, 16),
            str4 = guidStr.slice(16, 20),
            str5 = guidStr.slice(20);
            return guidStr.toUpperCase(); //(str1 + str2 + str3 + str4 + str5).toUpperCase();
        }
    }
}

//通用子页面输入框赋值
function Com_edit_info(rowData,inputs,selects,textareas,RDA,RBC){
	$.each(selects, function (i, obj) {
		if(typeof(eval(RDA+"_ary_"+obj.id.toString().replace(RBC+"_",""))) == "object")
		{
			$.each(eval(RDA+"_ary_"+obj.id.toString().replace(RBC+"_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == (RBC+"_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == (RBC+"_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval(RDA+"_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#"+RBC+"_DataModal").find('[id="'+RBC+"_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == (RBC+"_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == (RBC+"_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//时间或日期相加
function DateAdd(interval, number, date) {
    switch (interval) {
    case "y": {
            date.setFullYear(date.getFullYear() + number);
            return date;
            break;
        }
    case "q": {
            date.setMonth(date.getMonth() + number * 3);
            return date;
            break;
        }
    case "M": {
            date.setMonth(date.getMonth() + number);
            return date;
            break;
        }
    case "w": {
            date.setDate(date.getDate() + number * 7);
            return date;
            break;
        }
    case "d": {
            date.setDate(date.getDate() + number);
            return date;
            break;
        }
    case "H": {
            date.setHours(date.getHours() + number);
            return date;
            break;
        }
    case "m": {
            date.setMinutes(date.getMinutes() + number);
            return date;
            break;
        }
    case "s": {
            date.setSeconds(date.getSeconds() + number);
            return date;
            break;
        }
    default: {
            date.setDate(date.getDate() + number);
            return date;
            break;
        }
    }
}

//附件上传
function get_ajax_upload(formData,param_v, inputjsonpCallBack) {
        ly_index = layer.load();        
        $.ajax({
            url: bizzUrl+"&param_name=upLoadFile&session_id="+session_id+param_v,//"/CenterData/uploadFile?FileDate=" + file_time,
            type: "POST",
            data: formData,
            dataType: 'json',
            processData: false, // 不处理数据
            contentType: false, // 不设置内容类型
            enctype: 'multipart/form-data',
            success: function (input) {
            	layer.close(ly_index);
            	callbackFuction(inputjsonpCallBack, input);
            },
            error: function () {
                layer.close(ly_index);
                swal({
                    title: "告警",
                    text: "网络异常或系统故障，请刷新页面！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "刷新",
                    closeOnConfirm: false
                },
                    function () {
                    layer.close(ly_index);
                    window.location.reload();
                });
            },
            // 请求完成后的回调函数 (请求成功或失败之后均调用)
            complete: function (XMLHttpRequest, textStatus) {
                $("#doing").empty();
                $("#doing").attr("style", "display:none");
            }
        });
}

//标准静态配置库数据提交
function get_ajax_staticurl(inputData, inputjsonpCallBack) {
    if (is_history_back == 0) {
        ly_index = layer.load();
        $.ajax({
            type: "POST",
            async: false,
            url: baseUrl,
            data: inputData,
            //跨域请求的URL
            dataType: "jsonp",
            jsonp: "jsoncallback",
            jsonpCallback: inputjsonpCallBack,
            success: function (data) {
            },
            error: function () {
                layer.close(ly_index);
                swal({
                    title: "告警",
                    text: "网络异常或系统故障，请刷新页面！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "刷新",
                    closeOnConfirm: false
                },
                    function () {
                    layer.close(ly_index);
                    window.location.reload();
                });
            },
            // 请求完成后的回调函数 (请求成功或失败之后均调用)
            complete: function (XMLHttpRequest, textStatus) {
                $("#doing").empty();
                $("#doing").attr("style", "display:none");
            }
        });
    } 
    else if (is_history_back == 1) {
        var storage = localStorage.getItem(getPageUrl() + inputjsonpCallBack.toString());
        if (storage != null) {
            ly_index = layer.load();
            var data = JSON.parse(storage);
            callbackFuction(inputjsonpCallBack, data);
        }
    }
}

//标准业务库数据提交
function get_ajax_baseurl(inputData, inputjsonpCallBack) {
    if (is_history_back == 0) {
        ly_index = layer.load();
        $.ajax({
            type: "POST",
            async: false,
            url: bizzUrl,
            data: inputData,
            //跨域请求的URL
            dataType: "jsonp",
            jsonp: "jsoncallback",
            jsonpCallback: inputjsonpCallBack,
            success: function (data) {
            },
            error: function () {
                layer.close(ly_index);
                swal({
                    title: "告警",
                    text: "网络异常或系统故障，请刷新页面！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "刷新",
                    closeOnConfirm: false
                },
                    function () {
                    layer.close(ly_index);
                    window.location.reload();
                });
            },
            // 请求完成后的回调函数 (请求成功或失败之后均调用)
            complete: function (XMLHttpRequest, textStatus) {
                $("#doing").empty();
                $("#doing").attr("style", "display:none");
            }
        });
    } 
    else if (is_history_back == 1) {
        var storage = localStorage.getItem(getPageUrl() + inputjsonpCallBack.toString());
        if (storage != null) {
            ly_index = layer.load();
            var data = JSON.parse(storage);
            callbackFuction(inputjsonpCallBack, data);
        }
    }
}

//查询成功或失败
function Call_QryResult(inputValue){
	layer.close(ly_index);
	var s_result = "1";
    var error_desc = "";
    for (var key in inputValue[0]) {
        if (key == GetLowUpp('s_result')) {
            s_result = inputValue[0][GetLowUpp("s_result")];
            error_desc = inputValue[0][GetLowUpp("error_desc")];
        }
    }
    if (s_result == "0") {
		layer.close(ly_index);
        swal("数据读取失败!", "失败原因:" + error_desc, "warning");
		return false;
    }
    else
    	return true;
}

//操作成功或失败
function Call_OpeResult(inputValue){
	layer.close(ly_index);
	var s_result = "1";
    var error_desc = "";
    for (var key in inputValue[0]) {
        if (key == GetLowUpp('s_result')) {
            s_result = inputValue[0][GetLowUpp("s_result")];
            error_desc = inputValue[0][GetLowUpp("error_desc")];
        }
    }
    if (s_result == "0") {
        swal("数据读取失败!", "失败原因:" + error_desc, "warning");
		return false;
    }
    else
    {
    	swal("操作成功!", "", "success");
    	return true;
    }    
}

//回调函数
function callbackFuction(callback, data){
    eval(callback + "(data)");
}

//启用禁用
function get_s_state(value, row, index) {
    if (value == "1")
        return "启用";
    else if (value == "0")
        return "<font color='red'>禁用</font>";
}

//以下3个方法为列表处理通用方法
function isExistOption(id, value) {
    var isExist = false;
    var count = $('#' + id).find('option').length;
    for (var i = 0; i < count; i++) {
        if ($('#' + id).get(0).options[i].value == value) {
            isExist = true;
            break;
        }
    }
    return isExist;
}

//添加select中某个option
function addOptionValue(id, value, text) {
    if (!isExistOption(id, value)) {
        $('#' + id).append("<option value='" + value + "'>" + text + "</option>");
    }
}

//删除select中某个option
function delOptionValue(id, value) {
    if (isExistOption(id, value)) {
        $("#" + id + " option[value=" + value + "]").remove();
    }
}

//判断是否object
function isObj(object) {
    return object && typeof(object) == 'object' && Object.prototype.toString.call(object).toLowerCase() == "[object object]";
}
 
//判断是否array
function isArray(object) {
    return object && typeof(object) == 'object' && object.constructor == Array;
}

//获取长度
function getLength(object) {
    var count = 0;
    for(var i in object) 
    	count++;
    return count;
}

//比较objA 和objB 2个json对象
function CompareJsonObj(objA, objB) {
    if(!isObj(objA) || !isObj(objB)) return false; //判断类型是否正确
    if(getLength(objA) != getLength(objB)) return false; //判断长度是否一致
    return CompareObj(objA, objB, true); //默认为true
}

//循环递归比较objA 和objB 2个json对象
function CompareObj(objA, objB, flag) {
	if(getLength(objA) != getLength(objB)) return false;
    for(var key in objA) {
        if(!flag) //flag为false，则跳出整个循环
            break;
        if(!objB.hasOwnProperty(key)) {//是否有自身属性，而不是继承的属性
            flag = false;
            break;
        }
        if(!isArray(objA[key])) { //子级不是数组时,比较属性值        	
        	if (isObj(objA[key])) {
        		if (isObj(objB[key])) {
        			if(!flag) //这里跳出循环是为了不让递归继续
                        break;
        			flag = CompareObj(objA[key], objB[key], flag);
        		} else {
        			flag = false;
                    break;
        		}
        	} else {
        		if(String(objB[key]) != String(objA[key])) { //排除数字比较的类型差异
            		flag = false;
                    break;
                }
        	}
        } else {
            if(!isArray(objB[key])) {
                flag = false;
                break;
            }
            var oA = objA[key],
                oB = objB[key];
            if(oA.length != oB.length) {
                flag = false;
                break;
            }
            for(var k in oA) {
                if(!flag) //这里跳出循环是为了不让递归继续
                    break;
                flag = CompareObj(oA[k], oB[k], flag);
            }
        }
    }
    return flag;
}

// 动态加载css文件
function loadStyles(url) {
	var link = document.createElement("link");
	link.type = "text/css";
	link.rel = "stylesheet";
	link.href = url;
	document.getElementsByTagName("head")[0].appendChild(link);
}

//动态加载js脚本文件
function loadScript(url) {
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = url;
	document.body.appendChild(script);
}

//动态加载js脚本文件 无任何返回动作
function loadScript_nofun(url) {
	var temUrl = "";
	if(url.lastIndexOf(".vue") != -1)
		if(url.indexOf("?") == -1 && url.lastIndexOf(".vue") == url.length - 4)
			temUrl = "root_api/"+ url;
		else if(url.indexOf("?") != -1 && url.lastIndexOf(".vue") == url.indexOf("?") - 4)
			temUrl = "root_api/"+ url;
		else
			temUrl = url
	else
		temUrl = url;
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = temUrl;
	document.body.appendChild(script);
}

//返回vue文件或html文件名
function RetVueOrHtml(url){
	if(url.lastIndexOf(".vue") != -1)
		if(url.indexOf("?") == -1 && url.lastIndexOf(".vue") == url.length - 4)
			temUrl = "root_api/"+ url;
		else if(url.indexOf("?") != -1 && url.lastIndexOf(".vue") == url.indexOf("?") - 4)
			temUrl = "root_api/"+ url;
		else
			temUrl = url
	else
		temUrl = url;
}

//判断vue文件或html文件
function IsVueOrHml(url){
	if(url.lastIndexOf(".vue") != -1)
		return 1;
	else if(url.lastIndexOf(".html") != -1)
		return 2;
	else
		return 3;
}

//动态加载js脚本文件 含回调函数
function loadScript_noparam(url,callback) {
	var temUrl = "";
	if(url.lastIndexOf(".vue") != -1)
		if(url.indexOf("?") == -1 && url.lastIndexOf(".vue") == url.length - 4)
			temUrl = "root_api/"+ url;
		else if(url.indexOf("?") != -1 && url.lastIndexOf(".vue") == url.indexOf("?") - 4)
			temUrl = "root_api/"+ url;
		else
			temUrl = url
	else
		temUrl = url;	
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = temUrl;
	if (script.addEventListener) {
		script.addEventListener('load', function () {
			eval(callback + "()");
		 }, false);
	} else if (script.attachEvent) {
		script.attachEvent('onreadystatechange', function () {
			var target = window.event.srcElement;
			if (target.readyState == 'loaded') {
				eval(callback + "()");
			}
		});
	}
	document.body.appendChild(script);
}

//动态加载js脚本文件 含回调函数及参数
function loadScript_hasparam(url,callback,data) {
	var temUrl = "";
	if(url.lastIndexOf(".vue") != -1)
		if(url.indexOf("?") == -1 && url.lastIndexOf(".vue") == url.length - 4)
			temUrl = "root_api/"+ url;
		else if(url.indexOf("?") != -1 && url.lastIndexOf(".vue") == url.indexOf("?") - 4)
			temUrl = "root_api/"+ url;
		else
			temUrl = url
	else
		temUrl = url;	
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = url;
	if (script.addEventListener) {
		script.addEventListener('load', function () {
			callbackFuction(callback,data);
		 }, false);
	} else if (script.attachEvent) {
		script.attachEvent('onreadystatechange', function () {
			var target = window.event.srcElement;
			if (target.readyState == 'loaded') {
				callbackFuction(callback,data);
			}
		});
	}
	document.body.appendChild(script);
}

//加载子vue文件及回调函数
function loadHtmlSubVueFun(sub_vue,fun_name){
	var ahref = window.document.location.pathname.replace("/","")
	var temRoute = {"p_url":ahref,"c_url":sub_vue};
	var htmlSetHasSub = false;
	$.each(htmlRoute, function (i, obj) {
		if(CompareJsonObj(obj,temRoute) == true){
			htmlSetHasSub = true;
		}
	});
	if(htmlSetHasSub == false){
		//添加路由
    	htmlRoute.push(temRoute);		
		call_vue_funcName = fun_name;
		loadScript_nofun(sub_vue+"?callback=ReadCommonRes");
	}
	return htmlSetHasSub
}

//时间数据解码
function set_time_decode(value, row, index) {
    var timeResult = s_decode(value);
    return timeResult.replace("T"," ");
}

//加载子vue文件无回调函数
function loadHtmlSubVue(sub_vue){
	var ahref = window.document.location.pathname.replace("/","")
	var upd_pwd_temRoute = {"p_url":ahref,"c_url":index_subhtml};
	var htmlSetHasSub = false;
	$.each(htmlRoute, function (i, obj) {
		if(CompareJsonObj(obj,upd_pwd_temRoute) == true){
			htmlSetHasSub = true;
		}
	});
	if(htmlSetHasSub == false){
		//添加路由
    	htmlRoute.push(upd_pwd_temRoute);
		call_vue_funcName = "";
		loadScript_nofun(sub_vue+"?callback=ReadCommonRes");
	}
	return htmlSetHasSub
}