#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class T_PARAM_VALUE(object):
    attMAIN_ID = -1
    attPARAM_KEY = ""
    attPARAM_VLUE = ""
    attPARAM_NAME = ""
    attS_DESC = ""
    attCREATE_DATE = None
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")