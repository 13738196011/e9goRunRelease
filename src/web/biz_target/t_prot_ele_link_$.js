var type = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var ary_prot_id = null;var ary_element_id = null;
/*declare select options end*/

//业务逻辑数据开始
function biz_start() {
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "T01_t_prot_ele_link$prot_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "get_T01_t_prot_ele_link$prot_id");	
    /*biz end*/
}

/*biz step begin*/
function format_prot_id(value, row, index) {    var objResult = value;    for(i = 0; i < ary_prot_id.length; i++) {        var obj = ary_prot_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function format_element_id(value, row, index) {    var objResult = value;    for(i = 0; i < ary_element_id.length; i++) {        var obj = ary_element_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function get_T01_t_prot_ele_link$prot_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_t_prot_ele_link$prot_id) == false)        return false;    ary_prot_id = input.T01_t_prot_ele_link$prot_id;    if($("#prot_id").is("select") && $("#prot_id")[0].options.length == 0)    {        $.each(ary_prot_id, function (i, obj) {            addOptionValue("prot_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    var inputdata = {        "param_name": "T01_t_prot_ele_link$element_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "get_T01_t_prot_ele_link$element_id");}function get_T01_t_prot_ele_link$element_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_t_prot_ele_link$element_id) == false)        return false;    ary_element_id = input.T01_t_prot_ele_link$element_id;    if($("#element_id").is("select") && $("#element_id")[0].options.length == 0)    {        $.each(ary_element_id, function (i, obj) {            addOptionValue("element_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    init_t_prot_ele_link();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function page_end(){
}


//页面初始化方法
function init_t_prot_ele_link() {
	type = getUrlParam("type");
	if(type == "add"){
		$("#main_id").parent().parent().parent().hide();
	}
	else if(type == "edit"){
	
	}
	
	//表单验证
	checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#create_date").val() == "")    {        $("#create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
}

//提交表单数据
function SubmitForm(){
	if(type == "add"){
		var inputdata = {
				"param_name": "T01_ins_t_prot_ele_link",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#prot_id").val()                ,"param_value2": $("#element_id").val()                ,"param_value3": $("#create_date").val()                ,"param_value4": s_encode($("#s_desc").val())                ,"param_value5": s_encode($("#prot_up_limit").val())                ,"param_value6": s_encode($("#prot_down_limit").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_ins_t_prot_ele_link");
	}
	else if(type == "edit"){
		var inputdata = {
				"param_name": "T01_upd_t_prot_ele_link",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#prot_id").val()                ,"param_value2": $("#element_id").val()                ,"param_value3": $("#create_date").val()                ,"param_value4": s_encode($("#s_desc").val())                ,"param_value5": s_encode($("#prot_up_limit").val())                ,"param_value6": s_encode($("#prot_down_limit").val())                ,"param_value7": $("#main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_upd_t_prot_ele_link");
	}
}

//以下为页面表单验证
$("#save_t_prot_ele_link_Edit").click(function () {
	$("form[name='DataModal']").submit();
})

/*修改数据*/
function get_T01_upd_t_prot_ele_link(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_upd_t_prot_ele_link) == true)
	{
        //init();
		parent.swal("修改数据成功!", "", "success");
		parent.t_prot_ele_link_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

/*添加数据*/
function get_T01_ins_t_prot_ele_link(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_ins_t_prot_ele_link) == true)
	{
		parent.swal("添加数据成功!", "", "success");
		parent.t_prot_ele_link_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

//取消编辑
$("#cancel_t_prot_ele_link_Edit").click(function () {
	var index = parent.layer.getFrameIndex(window.name);
	parent.layer.close(index);
})

function clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

$(document).ready(function () {
});

window.onpageshow = function (e) {
    if (e.persisted || (window.performance.navigation.type == 2)) {
        is_history_back = 1;
    } else {
        is_history_back = 0;
    }
    //页面初始化
    init_page();
};

//form验证
function checkFormInput() {
    $("#DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            main_id: {}            ,prot_id: {}            ,element_id: {}            ,create_date: {date: true,required : true,maxlength:19}            ,s_desc: {}            ,prot_up_limit: {}            ,prot_down_limit: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            main_id: {}            ,prot_id: {}            ,element_id: {}            ,create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,s_desc: {}            ,prot_up_limit: {}            ,prot_down_limit: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	SubmitForm();
        	return false;
        }
    })
}