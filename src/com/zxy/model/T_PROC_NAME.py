#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class T_PROC_NAME(object):
    attMAIN_ID = -1
    attDB_ID = -1
    attINF_CN_NAME = ""
    attINF_EN_NAME = ""
    attINF_EN_SQL = ""
    attINF_TYPE = ""
    attS_DESC = ""
    attCREATE_DATE = None
    attREFLECT_IN_CLASS = ""
    attREFLECT_OUT_CLASS = ""
    attIS_AUTHORITY = 1
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
