#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import binascii,snap7,struct
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
import modbus_tk.defines as cst
import modbus_tk.modbus_rtu as modbus_rtu
import modbus_tk.modbus_tcp as modbus_tcp
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.plugins.A01_7GA08B import A01_Para
from com.zxy.z_debug import z_debug
class A01_7GA08B_PLC(z_debug):
    def __init__(self):
        pass
    def open_plc(self,inputPLCrack,inputPLCslot):
        try:
            if A01_Para.objPLC_tcp is None:
                A01_Para.objPLC_tcp = snap7.client.Client()
            if A01_Para.objPLC_tcp.get_connected() == False :
                A01_Para.objPLC_tcp.connect(A01_Para.objPLC_IP,inputPLCrack, inputPLCslot)#, tcpport=A01_Para.objPLC_Port)
            return True 
        except Exception as e:
            return False 
    def get_data_plc(self,inputPLCrack,inputPLCslot,inputDb_number,inputStart,inputSize):
        red = None
        try:
            if self.open_plc(inputPLCrack,inputPLCslot) == True:
                red= A01_Para.objPLC_tcp.db_read(inputDb_number,inputStart,inputSize)
            return red
        except Exception as e:
            return red
    def StrtoByesarray(self,strdata):
        strarry = strdata.split()
        list = []
        for itm in strarry:
            list.append(itm)
        return bytearray(list)
    def send_data_plc(self,inputPLCAddress,inputPLCrack,inputPLCslot,inputDb_number,inputStart,inputData):
        temResult = None
        try:
            if self.open_plc(inputPLCAddress,inputPLCrack,inputPLCslot) == True:
                temData = self.StrtoByesarray(inputData)
                temResult = A01_Para.objPLC_tcp.db_write(inputDb_number, inputStart, temData)
        except Exception as e:
            temResult = "-1"
        return temResult