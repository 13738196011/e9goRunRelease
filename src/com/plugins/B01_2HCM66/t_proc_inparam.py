#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_proc_inparam(object):
    attmain_id = -1
    attPROC_ID = 0
    attPARAM_CN_NAME = ""
    attPARAM_EN_NAME = ""
    attPARAM_TYPE = ""
    attPARAM_SIZE = 0
    attS_DESC = ""
    attCREATE_DATE = None
    attIS_URLENCODE = 0
    lenPARAM_CN_NAME = 50
    lenPARAM_EN_NAME = 50
    lenPARAM_TYPE = 50
    lenS_DESC = 500
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["PROC_ID"] = "接口名称"
        temHt["PARAM_CN_NAME"] = "输入参数中文名称"
        temHt["PARAM_EN_NAME"] = "输入参数英文名称"
        temHt["PARAM_TYPE"] = "参数类型"
        temHt["PARAM_SIZE"] = "接口输入参数大小"
        temHt["S_DESC"] = "备注"
        temHt["CREATE_DATE"] = "系统时间"
        temHt["IS_URLENCODE"] = "是否URL编码"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["PROC_ID"] = "select MAIN_ID,INF_CN_NAME as CN_NAME from t_proc_name"
        temHtSelect["PARAM_TYPE"] = "select id as MAIN_ID,cn as CN_NAME from t_proc_inparam_type"
        temHtSelect["IS_URLENCODE"] = [{"MAIN_ID":"1","CN_NAME":"是"},{"MAIN_ID":"0","CN_NAME":"否"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        temHtFind["PROC_ID"] = "t_proc_name.vue"
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        temHtFReturn["PROC_ID"] = ["MAIN_ID","INF_CN_NAME"]
        return temHtFReturn
