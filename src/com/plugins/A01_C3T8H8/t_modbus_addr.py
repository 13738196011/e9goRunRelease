#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_modbus_addr(object):
    attmain_id = -1
    attstation_id = -1
    attmodbus_gateway = ""
    attcom_port = ""
    attmodbus_addr = ""
    attmodbus_begin = ""
    attmodbus_length = 0
    attelement_val = ""
    attcreate_date = None
    atts_desc = ""
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")