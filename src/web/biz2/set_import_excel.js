﻿var ly_index;

$(document).ready(function () {
    init_page();
    //init();
});

//业务逻辑数据开始
function biz_start() {
	$("#date_begin").val(new Date().Format('yyyy-MM-dd 00:00:00'));
    $('#date_begin').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:ss',
        startView: 2,
        todayHighlight: 1, //今天高亮
        //initialDate:new Date(),
        autoclose: true,
        forceParse: 0, //设置为0，时间不会跳转1899，会显示当前时间。
        language: 'zh-CN', //显示中文
        minView: "day", //设置只显示到月份
        todayBtn: true //显示今日按钮
    });
    
    $("#btnFileUpload").click(function () {
    	var formData = new FormData($("#updForm").eq(0)[0]);
    	var param_v = "&login_id="+login_id;
    	if($("#file_set").val() == "")
    		swal("请选择正确附件!", "请选择正确附件", "warning");
    	else
    		get_ajax_upload(formData,param_v,"get_upload_file");
    });
    
    $("#btnClearFile").click(function () {
    	$("#upd_file_list").html("");
    });
    
    var inputdata = {
        "param_name": "T01_sel_t_file_type_ISAVA",
        "session_id": session_id,
        "login_id": login_id
    };
    get_ajax_baseurl(inputdata, "get_T01_sel_t_file_type_ISAVA");
}

function get_T01_sel_t_file_type_ISAVA(objTag){
	var objValue = objTag.T01_sel_t_file_type_ISAVA;
	if (Call_QryResult(objValue) == true)
	{
		$.each(objValue, function (i, obj) {
			var divOption = "<option value='"+obj.MAIN_ID+"'>"+obj.TYPE_NAME+"</option>";
			$("#file_type_id").append(divOption);
		});
	}
}

function get_A01_BRU8BU(objTag){
	var objValue = objTag.A01_BRU8BU;
	if (Call_OpeResult(objValue) == true)
	{
	}
}

function get_upload_file(objTag){
	var objValue = objTag.A01_UpLoadFile;
	if (Call_OpeResult(objValue) == true)
	{
		$.each(objValue, function (i, obj) {
			var fileList = "<a href=\"../file/"+obj.newFileName+"\" target='_blank'>"+obj.oldFileName+"</a><br>";
			$("#upd_file_list").append(fileList);
			
			if(obj.oldFileName.indexOf("不符合上传文件格式") == -1)
			{
			    var inputdata = {
			    	"param_name": "A01_BRU8BU",
			        "session_id": session_id,
			        "login_id": login_id,
			        "param_value1": $("#file_type_id").val(),
			        "param_value2": s_encode(obj.oldFileName),
			        "param_value3": s_encode(obj.newFileName),
			        "param_value4": $("[name=date_type]").val(),
			        "param_value5": $("#date_begin").val()
			    };
			    get_ajax_baseurl(inputdata, "get_A01_BRU8BU");
			}
		});
	}
}