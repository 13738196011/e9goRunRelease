//按钮事件新增或编辑
var B2321E_type = "";
//其他页面传到本页面参数
var B2321E_param = {};
//暂时没用
var B2321E_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

//业务逻辑数据开始
function B2321E_t_sub_sys_biz_start(inputdata) {
	B2321E_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    B2321E_init_t_sub_sys()	
    /*biz end*/
}

/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function B2321E_page_end(){
	if(B2321E_param["type"] == "edit"){
		B2321E_get_edit_info();
	}
}

//页面初始化方法
function B2321E_init_t_sub_sys() {
	//type = getUrlParam("type");
	if(B2321E_param["type"] == "add"){
		$("#B2321E_main_id").parent().parent().parent().hide();
		
		$("#B2321E_SUB_CODE").val(new GUID().newGUID());
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
		
		/*Get Find Select param end*/	
	}
	else if(B2321E_param["type"] == "edit"){
	
	}
	
	//表单验证
	B2321E_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B2321E_LIMIT_DATE").val() == "")    {        $("#B2321E_LIMIT_DATE").val(DateAdd("y",50,new Date()).Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B2321E_LIMIT_DATE',        type: 'datetime',        trigger: 'click'    });    if($("#B2321E_CREATE_DATE").val() == "")    {        $("#B2321E_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B2321E_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B2321E_page_end();
}

//提交表单数据
function B2321E_SubmitForm(){
	if(B2321E_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_sub_sys",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#B2321E_SUB_NAME").val())                ,"param_value2": s_encode($("#B2321E_SUB_CODE").val())                ,"param_value3": $("#B2321E_LIMIT_DATE").val()                ,"param_value4": s_encode($("#B2321E_S_DESC").val())                ,"param_value5": $("#B2321E_CREATE_DATE").val()                ,"param_value6": s_encode($("#B2321E_IP_ADDR").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B2321E_get_N01_ins_t_sub_sys");
	}
	else if(B2321E_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_sub_sys",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#B2321E_SUB_NAME").val())                ,"param_value2": s_encode($("#B2321E_SUB_CODE").val())                ,"param_value3": $("#B2321E_LIMIT_DATE").val()                ,"param_value4": s_encode($("#B2321E_S_DESC").val())                ,"param_value5": $("#B2321E_CREATE_DATE").val()                ,"param_value6": s_encode($("#B2321E_IP_ADDR").val())                ,"param_value7": $("#B2321E_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B2321E_get_N01_upd_t_sub_sys");
	}
}

//vue回调
function B2321E_t_sub_sys_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#B2321E_save_t_sub_sys_Edit").click(function () {
	$("form[name='B2321E_DataModal']").submit();
})

/*修改数据*/
function B2321E_get_N01_upd_t_sub_sys(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_sub_sys) == true)
	{
		swal("修改数据成功!", "", "success");
		A2321E_t_sub_sys_query();
		B2321E_clear_validate();
		layer.close(B2321E_param["ly_index"]);
	}
}

/*添加数据*/
function B2321E_get_N01_ins_t_sub_sys(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_sub_sys) == true)
	{
		swal("添加数据成功!", "", "success");
		A2321E_t_sub_sys_query();
		B2321E_clear_validate();
		layer.close(B2321E_param["ly_index"]);
	}
}

//取消编辑
$("#B2321E_cancel_t_sub_sys_Edit").click(function () {
	layer.close(B2321E_param["ly_index"]);
	B2321E_clear_validate();
	$("[id^='B2321E_div']").hide();
})

//清除查找框
function B2321E_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B2321E_clear_validate(){
	$("#B2321E_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B2321E_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B2321E_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B2321E_clear_edit_info(){
	var inputs = $("#B2321E_DataModal").find('input');
	var selects = $("#B2321E_DataModal").find("select");
	var textareas = $("#B2321E_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	B2321E_init_t_sub_sys();
}

//页面输入框赋值
function B2321E_get_edit_info(){
	var rowData = $("#A2321E_t_sub_sys_Events").bootstrapTable('getData')[A2321E_select_t_sub_sys_rowId];
	var inputs = $("#B2321E_DataModal").find('input');
	var selects = $("#B2321E_DataModal").find("select");
	var textareas = $("#B2321E_DataModal").find('textarea');
	$.each(selects, function (i, obj) {
		if(typeof(eval("A2321E_ary_"+obj.id.toString().replace("B2321E_",""))) == "object")
		{
			$.each(eval("A2321E_ary_"+obj.id.toString().replace("B2321E_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("B2321E_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("B2321E_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("A2321E_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#B2321E_DataModal").find('[id="'+"B2321E_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("B2321E_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("B2321E_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//form验证
function B2321E_checkFormInput() {
    B2321E_validate = $("#B2321E_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B2321E_main_id: {}            ,B2321E_SUB_NAME: {}            ,B2321E_SUB_CODE: {}            ,B2321E_LIMIT_DATE: {date: true,required : true,maxlength:19}            ,B2321E_S_DESC: {}            ,B2321E_CREATE_DATE: {date: true,required : true,maxlength:19}            ,B2321E_IP_ADDR: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B2321E_main_id: {}            ,B2321E_SUB_NAME: {}            ,B2321E_SUB_CODE: {}            ,B2321E_LIMIT_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B2321E_S_DESC: {}            ,B2321E_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B2321E_IP_ADDR: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B2321E_SubmitForm();
        	return false;
        }
    })
}