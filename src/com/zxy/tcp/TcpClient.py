#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import time
from com.zxy.z_debug import z_debug
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.business.Analyse_Data import Analyse_Data
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_6GG8T3 import A01_Para
class TcpClient(z_debug):
    def __init__(self, skey):
        self.skey = skey
        pass
    def client_link(self):
        sock = Com_Fun.GetHashTableNone(Com_Para.dClientThreadList,self.skey)
        while True:
            temInit_msg = b'' # 初始化流
            try:
                temInit_msg = sock.recv(1024*50)
                temValue = temInit_msg.decode(Com_Para.U_CODE)
                if temValue.replace("\r","").replace("\n","") == "Exit":
                    Com_Fun.RemoveHashTable(Com_Para.dClientThreadList,self.skey)
                    if sock is not None:
                        sock.close()
                    uL = UsAdmin_Log(Com_Para.ApplicationPath, Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") + "1 close server","NetWork")
                    uL.WriteLog()
                    break
                elif (temValue.find("##") == 0) and (temValue.rindex("##") == (len(temValue) - 2)):
                    A01_Para.objRand = temValue.replace("##","")
                elif len(temValue) > 0:
                    ad = Analyse_Data()
                    ad.SubAnalyseRecBytes(temValue,sock,False, sock.getpeername()[0],str(sock.getpeername()[1]))
                elif len(temInit_msg) == 0:
                    Com_Fun.RemoveHashTable(Com_Para.dClientThreadList,self.skey)
                    if sock is not None:
                        sock.close()
                    uL = UsAdmin_Log(Com_Para.ApplicationPath, Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") + "2 close server","NetWork")
                    uL.WriteLog()
                    break
                time.sleep(0.1)
            except Exception as e:
                Com_Fun.RemoveHashTable(Com_Para.dClientThreadList,self.skey)
                if sock is not None:
                    sock.close()
                temLog = ""
                if str(type(self)) == "<class 'type'>":
                    temLog = self.debug_info(self)+repr(e)
                else:
                    temLog = self.debug_info()+repr(e)
                uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
                uL.WriteLog()
                break
