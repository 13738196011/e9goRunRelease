//按钮事件新增或编辑
var BE31D6_type = "";
//其他页面传到本页面参数
var BE31D6_param = {};
//暂时没用
var BE31D6_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BE31D6_ary_is_ava = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];
/*declare select options end*/

//业务逻辑数据开始
function BE31D6_t_file_type_biz_start(inputdata) {
	BE31D6_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    $.each(BE31D6_ary_is_ava, function (i, obj) {        addOptionValue("BE31D6_is_ava", obj[GetLowUpp("main_id")], objGetLowUpp("cn_name")]);    });    BE31D6_init_t_file_type()	
    /*biz end*/
}

/*biz step begin*/
function BE31D6_format_is_ava(value, row, index) {    var objResult = value;    for(i = 0; i < BE31D6_ary_is_ava.length; i++) {        var obj = BE31D6_ary_is_ava[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function BE31D6_page_end(){
	if(BE31D6_param["type"] == "edit"){
		BE31D6_get_edit_info();
	}
}

//页面初始化方法
function BE31D6_init_t_file_type() {
	//type = getUrlParam("type");
	if(BE31D6_param["type"] == "add"){
		$("#BE31D6_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BE31D6_type_name").val(BE31D6_param["type_name"]);		
		/*Get Find Select param end*/	
	}
	else if(BE31D6_param["type"] == "edit"){
	
	}
	
	//表单验证
	BE31D6_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BE31D6_create_date").val() == "")    {        $("#BE31D6_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BE31D6_create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BE31D6_page_end();
}

//提交表单数据
function BE31D6_SubmitForm(){
	if(BE31D6_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_file_type",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#BE31D6_type_name").val())                ,"param_value2": $("#BE31D6_col_length").val()                ,"param_value3": $("#BE31D6_row_length").val()                ,"param_value4": $("#BE31D6_create_date").val()                ,"param_value5": $("#BE31D6_is_ava").val()                ,"param_value6": s_encode($("#BE31D6_s_desc").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BE31D6_get_N01_ins_t_file_type");
	}
	else if(BE31D6_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_file_type",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#BE31D6_type_name").val())                ,"param_value2": $("#BE31D6_col_length").val()                ,"param_value3": $("#BE31D6_row_length").val()                ,"param_value4": $("#BE31D6_create_date").val()                ,"param_value5": $("#BE31D6_is_ava").val()                ,"param_value6": s_encode($("#BE31D6_s_desc").val())                ,"param_value7": $("#BE31D6_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BE31D6_get_N01_upd_t_file_type");
	}
}

//vue回调
function BE31D6_t_file_type_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#BE31D6_save_t_file_type_Edit").click(function () {
	$("form[name='BE31D6_DataModal']").submit();
})

/*修改数据*/
function BE31D6_get_N01_upd_t_file_type(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_file_type) == true)
	{
		swal("修改数据成功!", "", "success");
		AE31D6_t_file_type_query();
		BE31D6_clear_validate();
		layer.close(BE31D6_param["ly_index"]);
	}
}

/*添加数据*/
function BE31D6_get_N01_ins_t_file_type(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_file_type) == true)
	{
		swal("添加数据成功!", "", "success");
		AE31D6_t_file_type_query();
		BE31D6_clear_validate();
		layer.close(BE31D6_param["ly_index"]);
	}
}

//取消编辑
$("#BE31D6_cancel_t_file_type_Edit").click(function () {
	layer.close(BE31D6_param["ly_index"]);
	BE31D6_clear_validate();
	$("[id^='BE31D6_div']").hide();
})

//清除查找框
function BE31D6_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BE31D6_clear_validate(){
	$("#BE31D6_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BE31D6_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BE31D6_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BE31D6_clear_edit_info(){
	var inputs = $("#BE31D6_DataModal").find('input');
	var selects = $("#BE31D6_DataModal").find("select");
	var textareas = $("#BE31D6_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	BE31D6_init_t_file_type();
}

//页面输入框赋值
function BE31D6_get_edit_info(){
	var rowData = $("#AE31D6_t_file_type_Events").bootstrapTable('getData')[AE31D6_select_t_file_type_rowId];
	var inputs = $("#BE31D6_DataModal").find('input');
	var selects = $("#BE31D6_DataModal").find("select");
	var textareas = $("#BE31D6_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"AE31D6","BE31D6");
	
	/*
	$.each(selects, function (i, obj) {
		if(typeof(eval("AE31D6_ary_"+obj.id.toString().replace("BE31D6_",""))) == "object")
		{
			$.each(eval("AE31D6_ary_"+obj.id.toString().replace("BE31D6_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE31D6_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("BE31D6_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("AE31D6_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#BE31D6_DataModal").find('[id="'+"BE31D6_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE31D6_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE31D6_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});*/
}

//form验证
function BE31D6_checkFormInput() {
    BE31D6_validate = $("#BE31D6_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BE31D6_main_id: {}            ,BE31D6_type_name: {}            ,BE31D6_col_length: {digits: true,required : true,maxlength:10}            ,BE31D6_row_length: {digits: true,required : true,maxlength:10}            ,BE31D6_create_date: {date: true,required : true,maxlength:19}            ,BE31D6_is_ava: {}            ,BE31D6_s_desc: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BE31D6_main_id: {}            ,BE31D6_type_name: {}            ,BE31D6_col_length: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,BE31D6_row_length: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,BE31D6_create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BE31D6_is_ava: {}            ,BE31D6_s_desc: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BE31D6_SubmitForm();
        	return false;
        }
    })
}