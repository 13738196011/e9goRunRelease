#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import inspect,datetime
class z_debug(object):
    def __init__(self):
        pass
    def debug_in(self,inputMsg):
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+"===>"+self.__class__.__name__+"."+inspect.stack()[1][3]+"==>"+inputMsg)
        pass
    def debug_info(self):
        return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+"===>"+self.__class__.__name__+"."+inspect.stack()[1][3]+"==>"