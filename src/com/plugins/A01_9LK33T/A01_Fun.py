#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.plugins.A01_IHGDW0 import A01_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.plugins.A01_IHGDW0.monitor_data_model import monitor_data_model
from com.plugins.A01_IHGDW0.monitor_flag_model import monitor_flag_model
from com.zxy.common import Com_Para
import os,struct,re
from com.zxy.z_debug import z_debug
class A01_Fun(z_debug):
    def __init__(self):
        pass
    @staticmethod
    def SaveDataFile(attFolder,attContent):
        if os.path.exists(attFolder) == False:
            os.mkdir(attFolder)
        temStrLogName = attFolder + '/' +Com_Fun.GetTime('%Y%m%d') + '.data'
        temFile = None
        try: 
            if os.path.exists(temStrLogName) == False :
                temFile = open(temStrLogName, 'a',encoding=Com_Para.U_CODE)
                temFile.write(attContent)
                temFile.close()
                temFile = None
            else:
                temFile = open(temStrLogName, 'a',encoding=Com_Para.U_CODE)
                temFile.write("\r\n"+","+attContent)
                temFile.close()
                temFile = None
        except Exception as e:
            print("A01_Fun.SaveFileDay:"+repr(e))
        finally:
            if temFile != None:
                temFile.close()
    @staticmethod
    def SendSocket(inputValue, inputSc):
        iReturn = -1
        iIndex = 0
        while iReturn <= 0 and iIndex < int(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "settime_rep")):
            try:
                iReturn = Com_Fun.SendSocket(inputValue, inputSc)
                if iReturn > 0:
                    uL = UsAdmin_Log(Com_Para.ApplicationPath, Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") + "=>"+inputValue,"SendUp"+inputSc.getpeername()[0]+"_"+str(inputSc.getpeername()[1])+"_")
                    uL.WriteLog()
            except Exception as e:
                pass
            iIndex = iIndex + 1
        return iReturn