//主从表tab选项卡标志位
var A567C2_Tab_Flag = -1;
//选择某一行
var A567C2_select_t_table_column_rowId = "";
//按钮事件新增或编辑
var A567C2_type = "";
//其他页面传到本页面参数
var A567C2_param = {};
//table选中数据，如无则默认第一条
var A567C2_rowCheckData = null;

/*定义查询条件变量*/
/*declare query param begin*/
var A567C2_tem_COLUMN_EN_NAME = "";var A567C2_tem_TABLE_ID = "-1";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var A567C2_ary_TABLE_ID = null;var A567C2_ary_COLUMN_TYPE = [{'MAIN_ID': '1', 'CN_NAME': 'STRING'}, {'MAIN_ID': '2', 'CN_NAME': 'INT'}, {'MAIN_ID': '3', 'CN_NAME': 'FLOAT'}, {'MAIN_ID': '4', 'CN_NAME': 'DATE'}];var A567C2_ary_COLUMN_QRY = [{'MAIN_ID': '0', 'CN_NAME': ''}, {'MAIN_ID': '1', 'CN_NAME': '输入框'}, {'MAIN_ID': '2', 'CN_NAME': '下拉框'}, {'MAIN_ID': '3', 'CN_NAME': '查找框'}];
/*declare select options end*/

/*绑定show监听事件*/
if(A567C2_Tab_Flag == "-1"){
	var n = Get_RandomDiv("A567C2","");
	$(n).bind("show", function(objTag){
		A567C2_Adjust_Sub_Sequ();
		if(A567C2_Tab_Flag > 0)
			A567C2_adjust_tab();		
	});
}

//设置主从表div顺序
function A567C2_Adjust_Sub_Sequ(){
	var temSubDivs = $("#content-main").find("div[data-id]");
	var MainDiv = $(Get_RandomDiv("A567C2",""));
	for(i = 0; i < temSubDivs.length; i++) {
		if(A567C2_Is_Sub_Div(temSubDivs[i].id)){
			$(temSubDivs[i]).before($(MainDiv));
			break;
		}
	}
}

//主从表传递参数
function A567C2_param_set(){
	/*Main Subsuv Table Param Begin*/
    if(A567C2_param.hasOwnProperty("TABLE_ID_cn_name"))        $("#A567C2_find_TABLE_ID_cn_name").val(s_decode(A567C2_param["TABLE_ID_cn_name"]));    if(A567C2_param.hasOwnProperty("TABLE_ID"))        $("#A567C2_find_TABLE_ID").val(A567C2_param["TABLE_ID"]);    if(A567C2_param.hasOwnProperty("hidden_find")){        $("#A567C2_Ope_TABLE_ID").hide();        $("#A567C2_Clear_TABLE_ID").hide();    }	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function A567C2_t_table_column_biz_start(inputparam) {
	layer.close(ly_index);
	A567C2_param = inputparam;
	//主从表传递参数
	A567C2_param_set();	
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_table_column$TABLE_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A567C2_get_N01_t_table_column$TABLE_ID");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function A567C2_format_TABLE_ID(value, row, index) {    var objResult = value;    for(i = 0; i < A567C2_ary_TABLE_ID.length; i++) {        var obj = A567C2_ary_TABLE_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A567C2_format_COLUMN_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < A567C2_ary_COLUMN_TYPE.length; i++) {        var obj = A567C2_ary_COLUMN_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A567C2_format_COLUMN_QRY(value, row, index) {    var objResult = value;    for(i = 0; i < A567C2_ary_COLUMN_QRY.length; i++) {        var obj = A567C2_ary_COLUMN_QRY[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A567C2_get_N01_t_table_column$TABLE_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_table_column$TABLE_ID) == false)        return false;    A567C2_ary_TABLE_ID = input.N01_t_table_column$TABLE_ID;    A567C2_init_t_table_column();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function A567C2_TABLE_ID_cn_name_fun(){    index_subhtml = "t_single_table.vue"    random_subhtml = "AEC35F";    if(loadHtmlSubVueFun("dev_vue/t_single_table.vue","A567C2_t_table_column_call_vue") == true){        var n = Get_RandomDiv("AEC35F","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#AEC35F_t_single_table_Events').bootstrapTable('resetView');                AEC35F_param["ly_index"] = index;                AEC35F_param["target_name"] = "A567C2_find_TABLE_ID_cn_name"                AEC35F_param["target_id"] = "A567C2_find_TABLE_ID"                AEC35F_param["sourc_id"] = "MAIN_ID"                AEC35F_param["sourc_name"] = "TABLE_EN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function A567C2_page_end(){
	A567C2_adjust_tab();
}

//表字段显示列定义
var A567C2_t_table_column = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '元数据表',        field: 'TABLE_ID',        sortable: true        ,formatter: A567C2_format_TABLE_ID    },    {        title: '字段中文名',        field: 'COLUMN_CN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '字段英文名',        field: 'COLUMN_EN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '字段类型',        field: 'COLUMN_TYPE',        sortable: true        ,formatter: A567C2_format_COLUMN_TYPE    },    {        title: '字段长度',        field: 'COLUMN_LENGTH',        sortable: true    },    {        title: '是否查询条件',        field: 'COLUMN_QRY',        sortable: true        ,formatter: A567C2_format_COLUMN_QRY    },    {        title: '下拉框或查找框数据集',        field: 'COLUMN_QRY_FORMAT',        sortable: true        ,formatter: set_s_decode    },    {        title: '查找框子页面',        field: 'COLUMN_FIND_HTML',        sortable: true        ,formatter: set_s_decode    },    {        title: '查找框返回字段',        field: 'COLUMN_FIND_RETURN',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '系统备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function A567C2_init_t_table_column() {
	$(window).resize(function () {
		  $('#A567C2_t_table_column_Events').bootstrapTable('resetView');
	});
	//表字段查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#A567C2_btn_t_table_column_query').click();
}

//查询接口
function A567C2_t_table_column_query() {
    $('#A567C2_t_table_column_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_table_column",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(A567C2_tem_COLUMN_EN_NAME)        ,"param_value2": A567C2_tem_TABLE_ID        ,"param_value3": A567C2_tem_TABLE_ID        ,"param_value4": A567C2_tem_TABLE_ID        ,"param_value5": A567C2_tem_TABLE_ID		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "A567C2_get_N01_sel_t_table_column");
}

//查询结果
function A567C2_get_N01_sel_t_table_column(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_table_column) == false)
        return false;
    A567C2_rowCheckData = null;
    //调整table各列宽度
    $.each(A567C2_t_table_column, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_t_table_column;
    if(s_data.length > 0)
    	A567C2_rowCheckData = s_data[0];    
    A567C2_select_t_table_column_rowId = "";
    A567C2_Tab_Flag = 0;
    $('#A567C2_t_table_column_Events').bootstrapTable('destroy');
    $("#A567C2_t_table_column_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#A567C2_t_table_column_Events').find("tr.changeColor").removeClass('changeColor');
                A567C2_select_t_table_column_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#A567C2_t_table_column_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                A567C2_select_t_table_column_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#A567C2_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(A567C2_param.hasOwnProperty("target_name"))
			{
				$("#"+A567C2_param["target_id"]).val(eval("row."+A567C2_param["sourc_id"].toUpperCase()));
				$("#"+A567C2_param["target_name"]).val(s_decode(eval("row."+A567C2_param["sourc_name"].toUpperCase())));				
				layer.close(A567C2_param["ly_index"]);
			}
		},
        toolbar: "#A567C2_t_table_column_Toolbar",
        columns: A567C2_t_table_column,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        onPostBody:function(){
        	if(s_data.length != 0)
            	A567C2_page_end();
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#A567C2_btn_t_table_column_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    A567C2_tem_COLUMN_EN_NAME = "";    A567C2_tem_TABLE_ID = "0";    A567C2_tem_TABLE_ID = "-1";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    B567C2_ary_TABLE_ID = null;    $("#A567C2_find_TABLE_ID_cn_name").val("");    $("#A567C2_find_TABLE_ID").val("-1");
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_table_column$TABLE_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A567C2_get_N01_t_table_column$TABLE_ID");	
    /*biz end*/
})

//查询按钮
$('#A567C2_btn_t_table_column_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    A567C2_tem_COLUMN_EN_NAME = $("#A567C2_qry_COLUMN_EN_NAME").val();    A567C2_tem_TABLE_ID = $("#A567C2_qry_TABLE_ID").val();    A567C2_tem_TABLE_ID = $("#A567C2_find_TABLE_ID").val();	
    /*set query param end*/
	A567C2_t_table_column_query();
})

//vue回调
function A567C2_t_table_column_call_vue(objResult){
	//选择某条记录或自动选择第一条
	if (A567C2_select_t_table_column_rowId != "") 
		A567C2_rowCheckData = $("#A567C2_t_table_column_Events").bootstrapTable('getData')[A567C2_select_t_table_column_rowId];
	
	if(index_subhtml == "t_table_column_$.vue")
	{
		var n = Get_RandomDiv("B567C2",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":A567C2_type,"ly_index":index};
				if(A567C2_param.hasOwnProperty("hidden_find")){
					inputdata["hidden_find"] = "1";
				}				
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temCOLUMN_EN_NAME = $("#A567C2_qry_COLUMN_EN_NAME").val();                if(temCOLUMN_EN_NAME != ""){                    inputdata["COLUMN_EN_NAME"] = temCOLUMN_EN_NAME;                }                var temTABLE_ID = $("#A567C2_qry_TABLE_ID").val();                if(temTABLE_ID != ""){                    inputdata["TABLE_ID"] = temTABLE_ID;                }                var temTABLE_ID = $("#A567C2_find_TABLE_ID_cn_name").val();                if(temTABLE_ID != ""){                    inputdata["TABLE_ID_cn_name"] = temTABLE_ID;                    inputdata["TABLE_ID"] = $("#A567C2_TABLE_ID").val();                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("dev_vue/t_table_column_$.js","B567C2_t_table_column_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_single_table.vue"){        var n = Get_RandomDiv("AEC35F",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":A567C2_type,                    "ly_index":index,                    "target_name":"A567C2_find_TABLE_ID_cn_name",                    "target_id":"A567C2_find_TABLE_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"TABLE_EN_NAME"                };                loadScript_hasparam("dev_vue/t_single_table.js","AEC35F_t_single_table_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/

	/*get tab subvue end*/
}

//新增按钮
$("#A567C2_btn_t_table_column_add").click(function () {
	A567C2_type = "add";
	index_subhtml = "t_table_column_$.vue";
	if(loadHtmlSubVueFun("dev_vue/t_table_column_$.vue","A567C2_t_table_column_call_vue") == true){
		var n = Get_RandomDiv("B567C2","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				B567C2_param["type"] = A567C2_type;
				B567C2_param["ly_index"]= index;
				if(A567C2_param.hasOwnProperty("hidden_find")){
					B567C2_param["hidden_find"] = "1";
				}
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temCOLUMN_EN_NAME = $("#A567C2_qry_COLUMN_EN_NAME").val();                if(temCOLUMN_EN_NAME != ""){                    B567C2_param["COLUMN_EN_NAME"] = temCOLUMN_EN_NAME;                }                var temTABLE_ID = $("#A567C2_qry_TABLE_ID").val();                if(temTABLE_ID != ""){                    B567C2_param["TABLE_ID"] = temTABLE_ID;                }                var temTABLE_ID = $("#A567C2_find_TABLE_ID_cn_name").val();                if(temTABLE_ID != ""){                    B567C2_param["TABLE_ID_cn_name"] = temTABLE_ID;                    B567C2_param["TABLE_ID"] = $("#A567C2_TABLE_ID").val();                }				
				/*Send Two FindSelect param end*/

				B567C2_clear_edit_info();
			},
			end: function(){
				B567C2_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#A567C2_btn_t_table_column_edit").click(function () {
	if (A567C2_select_t_table_column_rowId != "") {
		A567C2_type = "edit";
		index_subhtml = "t_table_column_$.vue";
		if(loadHtmlSubVueFun("dev_vue/t_table_column_$.vue","A567C2_t_table_column_call_vue") == true){
			var n = Get_RandomDiv("B567C2","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					B567C2_param["type"] = A567C2_type;
					B567C2_param["ly_index"] = index;
					if(A567C2_param.hasOwnProperty("hidden_find")){
						B567C2_param["hidden_find"] = "1";
					}
					B567C2_clear_edit_info();
					B567C2_get_edit_info();
				},
				end: function(){
					B567C2_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#A567C2_btn_t_table_column_delete').click(function () {
	//单行选择
	var rowData = $("#A567C2_t_table_column_Events").bootstrapTable('getData')[A567C2_select_t_table_column_rowId];
	//多行选择
	var rowDatas = A567C2_sel_row_t_table_column();
	if (A567C2_select_t_table_column_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_table_column",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "A567C2_N01_del_t_table_column");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_table_column",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "A567C2_N01_del_t_table_column");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function A567C2_N01_del_t_table_column(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_table_column) == true)
		A567C2_t_table_column_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function A567C2_adjust_tab(){
	if(typeof($("#A567C2_TAB_MAIN")[0]) != "undefined" && $("#A567C2_TAB_MAIN")[0].length != 0){
		A567C2_Tab_Flag = 1;
		$(Get_RDivNoBuild("A567C2","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("A567C2","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("A567C2","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#A567C2_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/
//隐藏tab页选项卡function A567C2_hide_tab_fun(){    var n = null;}//判断是否sub子项div页面function A567C2_Is_Sub_Div(temDivId){    if(temDivId.indexOf("XX$TTT") == 0)        return false;    return false;}
/*Tab Click Fun End*/

function A567C2_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("dev_vue/"+inputUrl,"A567C2_t_table_column_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (A567C2_select_t_table_column_rowId != "") 
			rowData = $("#A567C2_t_table_column_Events").bootstrapTable('getData')[A567C2_select_t_table_column_rowId];
		else
			rowData = A567C2_rowCheckData;
		//$("#A567C2_t_table_column_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			var temFlag = true;
			$.each(temPar, function (i, obj) {
				if(eval(inputrandom+"_param.hasOwnProperty(\""+obj.target_id+"\")") && eval(inputrandom+"_param[\""+obj.target_id+"\"]").toString() == rowData[obj.sourc_id].toString())
					temFlag = false;
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			if(temFlag){
				//传递子页面隐藏find功能
				eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
				//参数传递并赋值
				eval(inputrandom+"_param_set()");
				var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
				$("#"+inputrandom+"_btn_"+tbName+"_query").click();
			}
		}
		else{
			$.each(temPar, function (i, obj) {
				if(obj.sourc_id.toUpperCase() == "MAIN_ID")
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"-999\"");
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"\"");
			});
			//传递子页面隐藏find功能
			eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
			var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
			$("#"+inputrandom+"_btn_"+tbName+"_query").click();		
		}
	}
}

//清除 查找框
function A567C2_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function A567C2_sel_row_t_table_column(){
	//获得选中行
	var checkedbox= $("#A567C2_t_table_column_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}