#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import time,json,importlib
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_IHGDW0.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_IHGDW0 import A01_Para
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.comport.ComModBus import ComModBus
from com.plugins.A01_IHGDW0.monitor_data_model import monitor_data_model
from com.plugins.A01_LXNOQU.HJpacket import HJpacket
from com.plugins.A01_IHGDW0.A01_Fun import A01_Fun
from com.zxy.common.Aes_ECB import Aes_ECB
from com.zxy.z_debug import z_debug
class A01_R790KS_Time(z_debug):
    strContinue      = "1"   
    strResult        = ""
    strIP            = ""
    strPort          = ""
    strSend          = ""
    def __init__(self):
        pass
    def init_start(self):
        try:            
            if self.strResult.find("\\\\r\\\\n") != -1 and  self.strResult.find("\\\\r\\\\n") != len(self.strResult) - 4:
                strA = self.strResult.split("\\r\\n")
            elif self.strResult.find("\\r\\n") != -1 and  self.strResult.find("\\r\\n") != len(self.strResult) - 2:
                strA = self.strResult.split("\r\n")
            else:
                strA = self.strResult.split("\r\n")
            for temStrAryT in strA:
                if temStrAryT == "" :
                    continue
                temAry = temStrAryT.replace("\r","").replace("\n","").split("&&")
                temCRCInit = temStrAryT.replace("\r","").replace("\n","")                
                if len(temCRCInit) > 10 :
                    temCRCInit = temCRCInit[6:len(temCRCInit) - 4]
                initPacket = HJpacket()
                initPacket.clear_value()       
                temHJpacket = HJpacket()
                temHJpacket.clear_value()
                temHeadAry = temCRCInit.replace("##","").split(";")
                for hn in temHeadAry:
                    if hn.split("=")[0].find("QN") == 4:
                        temHJpacket.attQN = hn.split("=")[1]
                        temHJpacket.attLength = hn.split("=")[0][0:4]
                    elif hn.split("=")[0] == "ST":
                        temHJpacket.attST = hn.split("=")[1]
                    elif hn.split("=")[0] == "CN":
                        temHJpacket.attCN = hn.split("=")[1]
                    elif hn.split("=")[0] == "PW":
                        temHJpacket.attPW = hn.split("=")[1]
                    elif hn.split("=")[0] == "MN":
                        temHJpacket.attMN = hn.split("=")[1]
                    elif hn.split("=")[0] == "Flag":
                        temHJpacket.attFlag = hn.split("=")[1]
                temHJpacket.attCP = temAry[1]
                temHJpacket.attCRC = temAry[2]
                temCRC = temHJpacket.CheckCRC16Z(temCRCInit).upper()
                if temCRC != temHJpacket.attCRC:
                    uL = UsAdmin_Log(Com_Para.ApplicationPath, Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") + "接收数据包错误CRC校验失败："+self.strResult)
                    uL.WriteLog()
                elif temHJpacket.attST != A01_Fun.Get_ST() and temHJpacket.attST != "91":
                    uL = UsAdmin_Log(Com_Para.ApplicationPath, Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") + "接收数据包错误ST不一致："+self.strResult)
                    uL.WriteLog()
                elif temHJpacket.attPW != initPacket.attPW:
                    uL = UsAdmin_Log(Com_Para.ApplicationPath, Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") + "接收数据包错误PW不一致："+self.strResult)
                    uL.WriteLog()
                elif initPacket.attMN != temHJpacket.attMN :
                    uL = UsAdmin_Log(Com_Para.ApplicationPath, Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") + "接收数据包错误MN号不一致："+self.strResult)
                    uL.WriteLog()
                else:
                    self.ReleasePacket(temHJpacket)
        except Exception as e:        
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+self.strResult+"===>"+repr(e)
            else:
                temLog = self.debug_info()+self.strResult+"===>"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
    def ReleasePacket(self,inputHJpacket):
        temCPAry = inputHJpacket.attCP.split(";")
        bodc = Bus_Ope_DB_Cent()
        temHtCP = {}
        temHtDev = {}
        temInfoId = {}
        for cpinfo in temCPAry:
            if cpinfo.find(",") != -1:
                iIndex = 0
                for cpv in cpinfo.split(","):                    
                    if iIndex == 0:
                        Com_Fun.SetHashTable(temHtCP,cpv.split("=")[0],cpv.split("=")[1])
                    else:
                        if cpv.split("=")[0] == "InfoId":
                            Com_Fun.SetHashTable(temInfoId,cpv.split("=")[1],cpv.split("=")[1])
                        Com_Fun.SetHashTable(temHtDev,cpv.split("=")[0],cpv.split("=")[1])                        
                    iIndex = iIndex + 1
            elif len(cpinfo) > 1:
                Com_Fun.SetHashTable(temHtCP,cpinfo.split("=")[0],cpinfo.split("=")[1])
        temSock = None
        for temClientSocket in list(Com_Para.dClientThreadList.keys()):
            if temClientSocket == self.strIP+"|"+self.strPort:
                temSock = Com_Para.dClientThreadList[temClientSocket]
                break
        if inputHJpacket.attCN == "1000" and temSock is not None:
            try:
                temHJpacket = HJpacket()
                if Com_Fun.GetHashTable(temHtCP, "Overtime") != "":
                    pass
                if Com_Fun.GetHashTable(temHtCP, "ReCount") != "":
                    bodc.Upd_t_param_value("settime_rep", Com_Fun.GetHashTable(temHtCP, "ReCount"))
                temHJpacket.init_C_1_2()
                temHJpacket.attQN = inputHJpacket.attQN
                temSocketValue = temHJpacket.GetSendPack()
                A01_Fun.SendSocket(temSocketValue,Com_Para.dClientThreadList[temClientSocket])
                uL = UsAdmin_Log(Com_Para.ApplicationPath,temSocketValue,"SendPacket")
                uL.WriteLog()
                temHJpacket.init_C_1_3()
                temHJpacket.attQN = inputHJpacket.attQN
                temSocketValue = temHJpacket.GetSendPack()
                A01_Fun.SendSocket(temSocketValue,Com_Para.dClientThreadList[temClientSocket])
                uL = UsAdmin_Log(Com_Para.ApplicationPath,temSocketValue,"SendPacket")
                uL.WriteLog()
            except Exception as e:
                temLog = ""
                if str(type(self)) == "<class 'type'>":
                    temLog = self.debug_info(self)+repr(e)
                else:
                    temLog = self.debug_info()+repr(e)
                uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
                uL.WriteLog()
    def SendC14(self):
        temSock = None
        for temClientSocket in list(Com_Para.dClientThreadList.keys()):
            if temClientSocket == self.strIP+"|"+self.strPort:
                temSock = Com_Para.dClientThreadList[temClientSocket]
                break
        if temSock is not None:
            temHJpacket = HJpacket()
            temHJpacket.init_C_14_1()
            temSocketValue = temHJpacket.MakeC14_SEC()
            uL = UsAdmin_Log(Com_Para.ApplicationPath,"加密前明文SendC14==>"+temSocketValue,"SendPacket_A01_R790KS")
            uL.WriteLog()
            ae = Aes_ECB(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "token_key"))
            strResult = temHJpacket.GetSendPack_SEC(ae.AES_encrypt(temSocketValue)) 
            return A01_Fun.SendSocket(strResult,temSock)
    def SendC17(self,temmonitor_data_model,temmonitor_flag_model,temmonitor_max_model):
        temSock = None
        for temClientSocket in list(Com_Para.dClientThreadList.keys()):
            if temClientSocket == self.strIP+"|"+self.strPort:
                temSock = Com_Para.dClientThreadList[temClientSocket]
                break
        if temSock is not None:
            temHJpacket = HJpacket()
            temHJpacket.init_C_17_1()
            temSocketValue = temHJpacket.MakeC17_SEC(temmonitor_data_model,temmonitor_flag_model,temmonitor_max_model)
            uL = UsAdmin_Log(Com_Para.ApplicationPath,"加密前明文SendC17==>"+temSocketValue,"SendPacket_A01_R790KS")
            uL.WriteLog()
            ae = Aes_ECB(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "token_key"))
            strResult = temHJpacket.GetSendPack_SEC(ae.AES_encrypt(temSocketValue))            
            return A01_Fun.SendSocket(strResult,temSock)
        return -1
    def SendC18(self,temmonitor_data_model,temmonitor_flag_model,temmonitor_max_model):
        temSock = None
        for temClientSocket in list(Com_Para.dClientThreadList.keys()):
            if temClientSocket == self.strIP+"|"+self.strPort:
                temSock = Com_Para.dClientThreadList[temClientSocket]
                break
        if temSock is not None:
            temHJpacket = HJpacket()
            temHJpacket.init_C_18_1()
            temSocketValue = temHJpacket.MakeC18_SEC(temmonitor_data_model,temmonitor_flag_model,temmonitor_max_model)
            uL = UsAdmin_Log(Com_Para.ApplicationPath,"加密前明文SendC18==>"+temSocketValue,"SendPacket_A01_R790KS")
            uL.WriteLog()
            ae = Aes_ECB(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "token_key"))
            strResult = temHJpacket.GetSendPack_SEC(ae.AES_encrypt(temSocketValue));    
            return A01_Fun.SendSocket(strResult,temSock)
        return -1
    def SendC16(self,temmonitor_data_model):
        temSock = None
        for temClientSocket in list(Com_Para.dClientThreadList.keys()):
            if temClientSocket == self.strIP+"|"+self.strPort:
                temSock = Com_Para.dClientThreadList[temClientSocket]
                break
        if temSock is not None:
            temHJpacket = HJpacket()
            temHJpacket.init_C_16_1()
            temSocketValue = temHJpacket.MakeC16_SEC(temmonitor_data_model)
            uL = UsAdmin_Log(Com_Para.ApplicationPath,"加密前明文SendC16==>"+temSocketValue,"SendPacket_A01_R790KS")
            uL.WriteLog()
            ae = Aes_ECB(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "token_key"))
            strResult = temHJpacket.GetSendPack_SEC(ae.AES_encrypt(temSocketValue));
            return A01_Fun.SendSocket(strResult,temSock)
    def SendB3_1(self,temprot_name):
        temA01modbus = ComModBus()
        temHJpacket = HJpacket()
        temHJpacket.init_B_3_1()
        temSocketValue = temHJpacket.GetSendPack_CP()
        return temA01modbus.get_data_com_value(temprot_name, temSocketValue)
    def SendB3_3(self,temprot_name):
        temA01modbus = ComModBus()
        temHJpacket = HJpacket()
        temHJpacket.init_B_3_3()
        for temv_prot_element in A01_Para.ht_v_prot_element:
            if temv_prot_element.attprot_name == "A01_3W3XHW":
                temHJpacket.attCP = temHJpacket.attCP+"PolId="+temv_prot_element.attelement_code+";"
        temHJpacket.attCP = temHJpacket.attCP+"InfoId=i12001;"
        temSocketValue = temHJpacket.GetSendPack_CP()
        return temA01modbus.get_data_com_value(temprot_name, temSocketValue)
    def SendB3_4(self,temprot_name):
        temA01modbus = ComModBus()
        temHJpacket = HJpacket()
        temHJpacket.init_B_3_4()
        for temv_prot_element in A01_Para.ht_v_prot_element:
            if temv_prot_element.attprot_name == "A01_3W3XHW":
                temHJpacket.attCP = temHJpacket.attCP+"PolId="+temv_prot_element.attelement_code+";"
        temHJpacket.attCP = temHJpacket.attCP+"InfoId=i13001;"
        temSocketValue = temHJpacket.GetSendPack_CP()
        return temA01modbus.get_data_com_value(temprot_name, temSocketValue)
    def SendB3_5(self,temprot_name):
        temA01modbus = ComModBus()
        temHJpacket = HJpacket()
        temHJpacket.init_B_3_5()
        for temv_prot_element in A01_Para.ht_v_prot_element:
            if temv_prot_element.attprot_name == "A01_3W3XHW":
                temHJpacket.attCP = temHJpacket.attCP+"PolId="+temv_prot_element.attelement_code+";"
        temHJpacket.attCP = temHJpacket.attCP+"SystemTime="+Com_Fun.GetTime("%Y%m%d%H%M%S")+";"
        temSocketValue = temHJpacket.GetSendPack_CP()
        return temA01modbus.get_data_com_value(temprot_name, temSocketValue)
    def SendB3_6(self,temprot_name):
        temA01modbus = ComModBus()
        temHJpacket = HJpacket()
        temHJpacket.init_B_3_6()
        iIndex = 0
        for temv_prot_element in A01_Para.ht_v_prot_element:
            if temv_prot_element.attprot_name == "A01_3W3XHW":
                if iIndex  != 0:
                    temHJpacket.attCP = temHJpacket.attCP+";"
                temHJpacket.attCP = temHJpacket.attCP+"PolId="+temv_prot_element.attelement_code
                iIndex = 1
        temSocketValue = temHJpacket.GetSendPack_CP()
        return temA01modbus.get_data_com_value(temprot_name, temSocketValue)
    def SendB3_7(self,temprot_name):
        temA01modbus = ComModBus()
        temHJpacket = HJpacket()
        temHJpacket.init_B_3_7()
        iIndex = 0
        for temv_prot_element in A01_Para.ht_v_prot_element:
            if temv_prot_element.attprot_name == "A01_3W3XHW":
                if iIndex  != 0:
                    temHJpacket.attCP = temHJpacket.attCP+";"
                temHJpacket.attCP = temHJpacket.attCP+"PolId="+temv_prot_element.attelement_code
                iIndex = 1
        temSocketValue = temHJpacket.GetSendPack_CP()
        return temA01modbus.get_data_com_value(temprot_name, temSocketValue)
    def SendB3_8(self,temprot_name):
        temA01modbus = ComModBus()
        temHJpacket = HJpacket()
        temHJpacket.init_B_3_8()
        iIndex = 0
        for temv_prot_element in A01_Para.ht_v_prot_element:
            if temv_prot_element.attprot_name == "A01_3W3XHW":
                if iIndex  != 0:
                    temHJpacket.attCP = temHJpacket.attCP+";"
                temHJpacket.attCP = temHJpacket.attCP+"PolId="+temv_prot_element.attelement_code
                iIndex = 1
        temSocketValue = temHJpacket.GetSendPack_CP()
        return temA01modbus.get_data_com_value(temprot_name, temSocketValue)