//按钮事件新增或编辑
var B2BA1A_type = "";
//其他页面传到本页面参数
var B2BA1A_param = {};
//暂时没用
var B2BA1A_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

//业务逻辑数据开始
function B2BA1A_t_page_info_biz_start(inputdata) {
	B2BA1A_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    B2BA1A_init_t_page_info()	
    /*biz end*/
}

/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function B2BA1A_page_end(){
	if(B2BA1A_param["type"] == "edit"){
		B2BA1A_get_edit_info();
	}
}

//页面初始化方法
function B2BA1A_init_t_page_info() {
	//type = getUrlParam("type");
	if(B2BA1A_param["type"] == "add"){
		$("#B2BA1A_main_id").parent().parent().parent().hide();
		$("#B2BA1A_PAGE_CODE").val(new GUID().newGUID());
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
		
		/*Get Find Select param end*/	
	}
	else if(B2BA1A_param["type"] == "edit"){
	
	}
	
	//表单验证
	B2BA1A_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B2BA1A_CREATE_DATE").val() == "")    {        $("#B2BA1A_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B2BA1A_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B2BA1A_page_end();
}

//提交表单数据
function B2BA1A_SubmitForm(){
	if(B2BA1A_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_page_info",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#B2BA1A_PAGE_CODE").val())                ,"param_value2": s_encode($("#B2BA1A_PAGE_TITLE").val())                ,"param_value3": s_encode($("#B2BA1A_PAGE_URL").val())                ,"param_value4": $("#B2BA1A_CREATE_DATE").val()                ,"param_value5": s_encode($("#B2BA1A_S_DESC").val())                ,"param_value6": s_encode($("#B2BA1A_JS_URL").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B2BA1A_get_N01_ins_t_page_info");
	}
	else if(B2BA1A_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_page_info",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#B2BA1A_PAGE_CODE").val())                ,"param_value2": s_encode($("#B2BA1A_PAGE_TITLE").val())                ,"param_value3": s_encode($("#B2BA1A_PAGE_URL").val())                ,"param_value4": $("#B2BA1A_CREATE_DATE").val()                ,"param_value5": s_encode($("#B2BA1A_S_DESC").val())                ,"param_value6": s_encode($("#B2BA1A_JS_URL").val())                ,"param_value7": $("#B2BA1A_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B2BA1A_get_N01_upd_t_page_info");
	}
}

//vue回调
function B2BA1A_t_page_info_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#B2BA1A_save_t_page_info_Edit").click(function () {
	$("form[name='B2BA1A_DataModal']").submit();
})

/*修改数据*/
function B2BA1A_get_N01_upd_t_page_info(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_page_info) == true)
	{
		swal("修改数据成功!", "", "success");
		A2BA1A_t_page_info_query();
		B2BA1A_clear_validate();
		layer.close(B2BA1A_param["ly_index"]);
	}
}

/*添加数据*/
function B2BA1A_get_N01_ins_t_page_info(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_page_info) == true)
	{
		swal("添加数据成功!", "", "success");
		A2BA1A_t_page_info_query();
		B2BA1A_clear_validate();
		layer.close(B2BA1A_param["ly_index"]);
	}
}

//取消编辑
$("#B2BA1A_cancel_t_page_info_Edit").click(function () {
	layer.close(B2BA1A_param["ly_index"]);
	B2BA1A_clear_validate();
	$("[id^='B2BA1A_div']").hide();
})

//清除查找框
function B2BA1A_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B2BA1A_clear_validate(){
	$("#B2BA1A_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B2BA1A_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B2BA1A_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B2BA1A_clear_edit_info(){
	var inputs = $("#B2BA1A_DataModal").find('input');
	var selects = $("#B2BA1A_DataModal").find("select");
	var textareas = $("#B2BA1A_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	B2BA1A_init_t_page_info();
}

//页面输入框赋值
function B2BA1A_get_edit_info(){
	var rowData = $("#A2BA1A_t_page_info_Events").bootstrapTable('getData')[A2BA1A_select_t_page_info_rowId];
	var inputs = $("#B2BA1A_DataModal").find('input');
	var selects = $("#B2BA1A_DataModal").find("select");
	var textareas = $("#B2BA1A_DataModal").find('textarea');
	$.each(selects, function (i, obj) {
		if(typeof(eval("A2BA1A_ary_"+obj.id.toString().replace("B2BA1A_",""))) == "object")
		{
			$.each(eval("A2BA1A_ary_"+obj.id.toString().replace("B2BA1A_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("B2BA1A_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("B2BA1A_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("A2BA1A_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#B2BA1A_DataModal").find('[id="'+"B2BA1A_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("B2BA1A_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("B2BA1A_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//form验证
function B2BA1A_checkFormInput() {
    B2BA1A_validate = $("#B2BA1A_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B2BA1A_main_id: {}            ,B2BA1A_PAGE_CODE: {}            ,B2BA1A_PAGE_TITLE: {}            ,B2BA1A_PAGE_URL: {}            ,B2BA1A_CREATE_DATE: {date: true,required : true,maxlength:19}            ,B2BA1A_S_DESC: {}            ,B2BA1A_JS_URL: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B2BA1A_main_id: {}            ,B2BA1A_PAGE_CODE: {}            ,B2BA1A_PAGE_TITLE: {}            ,B2BA1A_PAGE_URL: {}            ,B2BA1A_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B2BA1A_S_DESC: {}            ,B2BA1A_JS_URL: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B2BA1A_SubmitForm();
        	return false;
        }
    })
}