//选择某一行
var select_t_modbus_addr_rowId = "";

/*定义查询条件变量*/
/*declare query param begin*/
var tem_com_port = "";var tem_element_id = "0";var tem_protocol_id = "0";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var ary_com_port = null;var ary_element_id = null;var ary_protocol_id = null;
/*declare select options end*/

//业务逻辑数据开始
function biz_start() {
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "T01_t_protocol$com_port",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "get_T01_t_protocol$com_port");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function format_com_port(value, row, index) {    var objResult = value;    for(i = 0; i < ary_com_port.length; i++) {        var obj = ary_com_port[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function format_element_id(value, row, index) {    var objResult = value;    for(i = 0; i < ary_element_id.length; i++) {        var obj = ary_element_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function format_protocol_id(value, row, index) {    var objResult = value;    for(i = 0; i < ary_protocol_id.length; i++) {        var obj = ary_protocol_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function get_T01_t_protocol$com_port(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_t_protocol$com_port) == false)        return false;    ary_com_port = input.T01_t_protocol$com_port;    var inputdata = {        "param_name": "T01_t_prot_ele_link$element_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "get_T01_t_prot_ele_link$element_id");}function get_T01_t_prot_ele_link$element_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_t_prot_ele_link$element_id) == false)        return false;    ary_element_id = input.T01_t_prot_ele_link$element_id;    var inputdata = {        "param_name": "T01_t_prot_ele_link$prot_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "get_T01_t_prot_ele_link$prot_id");}function get_T01_t_prot_ele_link$prot_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_t_prot_ele_link$prot_id) == false)        return false;    ary_protocol_id = input.T01_t_prot_ele_link$prot_id;    $("#qry_com_port").append("<option value=''></option>")    $.each(ary_com_port, function (i, obj) {        addOptionValue("qry_com_port", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    $("#qry_element_id").append("<option value='-1'></option>")    $.each(ary_element_id, function (i, obj) {        addOptionValue("qry_element_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    $("#qry_protocol_id").append("<option value='-1'></option>")    $.each(ary_protocol_id, function (i, obj) {        addOptionValue("qry_protocol_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    init_t_modbus_addr();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function page_end(){
}

//协议因子地址对照显示列定义
var t_modbus_addr = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '设备网关地址',        field: 'MODBUS_GATEWAY',        sortable: true        ,formatter: set_s_decode    },    {        title: '串口号',        field: 'COM_PORT',        sortable: true        ,formatter: format_com_port    },    {        title: '设备地址',        field: 'MODBUS_ADDR',        sortable: true        ,formatter: set_s_decode    },    {        title: '起始位或4017通道号',        field: 'MODBUS_BEGIN',        sortable: true        ,formatter: set_s_decode    },    {        title: '数据长度',        field: 'MODBUS_LENGTH',        sortable: true    },    {        title: '因子名称',        field: 'ELEMENT_ID',        sortable: true        ,formatter: format_element_id    },    {        title: '采集协议模块',        field: 'PROTOCOL_ID',        sortable: true        ,formatter: format_protocol_id    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true
        ,formatter: set_time_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function init_t_modbus_addr() {
	$(window).resize(function () {
		  $('#t_modbus_addr_Events').bootstrapTable('resetView');
	});
	//协议因子地址对照查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#btn_t_modbus_addr_query').click();
}

//查询接口
function t_modbus_addr_query() {
    $('#t_modbus_addr_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "T01_sel_t_modbus_addr",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(tem_com_port)        ,"param_value2": tem_element_id        ,"param_value3": tem_element_id        ,"param_value4": tem_protocol_id        ,"param_value5": tem_protocol_id		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "get_T01_sel_t_modbus_addr");
}

//查询结果
function get_T01_sel_t_modbus_addr(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.T01_sel_t_modbus_addr) == false)
        return false;    
	var s_data = input.T01_sel_t_modbus_addr;
    $('#t_modbus_addr_Events').bootstrapTable('destroy');
    $("#t_modbus_addr_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#t_modbus_addr_Events').find("tr.changeColor").removeClass('changeColor');
                select_t_modbus_addr_rowId = "";
            }
            　　　　　else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#t_modbus_addr_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                select_t_modbus_addr_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(getUrlParam("target_name") != null)
			{
				/*查找框返回值*/
				parent.$("#"+getUrlParam("target_id")).val(eval("row."+getUrlParam("sourc_id").toUpperCase()));
				parent.$("#"+getUrlParam("target_name")).val(eval("row."+getUrlParam("sourc_name").toUpperCase()));			
				var index = parent.layer.getFrameIndex(window.name);
				parent.layer.close(index);
			}
		},
        toolbar: "#t_modbus_addr_Toolbar",
        columns: t_modbus_addr,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [30, 100, 500, 2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//查询按钮
$('#btn_t_modbus_addr_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    tem_com_port = $("#qry_com_port").val();    tem_element_id = $("#qry_element_id").val();    tem_protocol_id = $("#qry_protocol_id").val();	
    /*set query param end*/
	t_modbus_addr_query();
})

//新增按钮
$("#btn_t_modbus_addr_add").click(function () {
	layer.open({
		type: 2,
        area: ['1100px', '600px'],
        fixed: false, //不固定
        maxmin: true,
        content: 't_modbus_addr_$.html?type=add',
        success: function(layero, index){
			var body = layer.getChildFrame('body',index);
			var inputs = body.contents().find("#DataModal").find('input');
        	/*新增按钮页面传父类ID或其他信息*/
        	/*add param value begin*/
        	
        	/*add param value end*/
			
//			$.each(inputs, function (i, obj) {
//				if (obj.id.toUpperCase() == "find_table_id_cn_name".toUpperCase()) {
//					$(obj).val($("#find_table_id_cn_name").val());
//				}
//				else if (("find_"+obj.id).toUpperCase() == "find_table_id".toUpperCase()){
//					$(obj).val($("#find_"+obj.id).val());
//				}
//			});
        }
    });
})

//编辑按钮
$("#btn_t_modbus_addr_edit").click(function () {
	if (select_t_modbus_addr_rowId != "") {
		layer.open({
			type: 2,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: 't_modbus_addr_$.html?type=edit',
			success: function(layero, index){
				var body = layer.getChildFrame('body',index);
				var rowData = $("#t_modbus_addr_Events").bootstrapTable('getData')[select_t_modbus_addr_rowId];
				var inputs = body.contents().find("#DataModal").find('input');
				var selects = body.contents().find("#DataModal").find("select");				
				$.each(selects, function (i, obj) {
					if(typeof(eval("ary_"+obj.id)) == "object")
					{
						$.each(eval("ary_"+obj.id), function (inx, obj_sub) {
							var div = "<option value='" + obj_sub[GetLowUpp("main_id")] + "'>" + obj_sub[GetLowUpp("cn_name")] + "</option>";
							$(obj).append(div);
						});
					}
				});
				
				Object.keys(rowData).forEach(function (key) {
					$.each(inputs, function (i, obj) {
						if (obj.id.toUpperCase() == key.toUpperCase()) {
							if(typeof(rowData[key]) == "string")
							{
								$(obj).val(s_decode(rowData[key]));
							}
							else
							{
								$(obj).val(rowData[key]);
							}
						}
						else if(obj.id.toUpperCase() == ("find_"+key+"_cn_name").toUpperCase()){
							$.each(eval("ary_"+key.toLowerCase()), function (jindex, obj2) {
								if(obj2[GetLowUpp("main_id")] == rowData[key]){
									body.contents().find("#DataModal").find('[id="'+("find_"+key.toLowerCase()+"_cn_name").toLowerCase()+'"]').val(obj2[GetLowUpp("cn_name")]);
								}
							});
						}
					});
					$.each(selects, function (i, obj) {
						if (obj.id.toUpperCase() == key.toUpperCase()) {
							$(obj).val(rowData[key]);
						}
					});
				});
			}
		});
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#btn_t_modbus_addr_delete').click(function () {
	//单行选择
	var rowData = $("#t_modbus_addr_Events").bootstrapTable('getData')[select_t_modbus_addr_rowId];
	//多行选择
	var rowDatas = sel_row_t_modbus_addr();
	if (select_t_modbus_addr_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "T01_del_t_modbus_addr",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "T01_del_t_modbus_addr");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "T01_del_t_modbus_addr",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "T01_del_t_modbus_addr");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function T01_del_t_modbus_addr(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.T01_del_t_modbus_addr) == true)
		t_modbus_addr_query();
}

//数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//清除 查找框
function clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function sel_row_t_modbus_addr(){
	//获得选中行
	var checkedbox= $("#t_modbus_addr_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}

$(document).ready(function () {
});

window.onpageshow = function (e) {
    if (e.persisted || (window.performance.navigation.type == 2)) {
        is_history_back = 1;
    } else {
        is_history_back = 0;
    }
    //页面初始化
    init_page();
};