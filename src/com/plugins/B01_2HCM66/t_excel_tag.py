#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_excel_tag(object):
    attmain_id = -1
    atttag_name = ""
    attfile_type_id = 0
    attrow_tag = ""
    attcolumn_tag = ""
    attcell_up = ""
    attcell_down = ""
    attcell_left = ""
    attcell_right = ""
    attcell_type = 0
    attcell_special = ""
    attis_ava = 1
    attcreate_date = None
    atts_desc = ""
    irow = -1
    jcell = -1
    lens_desc = 2000
    lenrow_tag = 200
    lencolumn_tag = 200
    lencell_up = 200
    lencell_down = 200
    lencell_left = 200
    lencell_right = 200
    lentag_name = 200
    lencell_special = 200
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["tag_name"] = "单元格标签名称"  
        temHt["file_type_id"] = "附件类别ID"  
        temHt["row_tag"] = "行起始标签" 
        temHt["column_tag"] = "列起始标签" 
        temHt["cell_up"] = "数据单元格上邻标签" 
        temHt["cell_down"] = "数据单元格下邻标签" 
        temHt["cell_left"] = "数据单元格左邻标签" 
        temHt["cell_right"] = "数据单元格右邻标签" 
        temHt["cell_type"] = "数据单元格类型" 
        temHt["cell_special"] = "数据特殊处理拦截器"
        temHt["is_ava"] = "是否启用 "        
        temHt["create_date"] = "系统时间"  
        temHt["s_desc"] = "系统备注"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["file_type_id"] = "附件类别"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["file_type_id"] = "select main_id,type_name as cn_name from t_file_type"
        temHtSelect["is_ava"] = [{"main_id":"1","cn_name":"启用"},{"main_id":"0","cn_name":"禁用"}]
        temHtSelect["cell_type"] = [{"main_id":"0","cn_name":"table格式"},{"main_id":"1","cn_name":"列表格式"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn