var iTop = 0;
var ileft = 0;
var ly_index;

function biz_start(){
}

//编辑提交数据成功
function submit_success() {
    init();
    $("#doing").empty();
    $("#doing").attr("style", "display:none");
    swal("操作成功!", "", "success");
    $("a.ui-jqdialog-titlebar-close").click();
}

var s_t_db_config = [{
        label: '主键',
        name: 'MAIN_ID',
        width: '30px',
        key: true,
        editable: true,
        editrules: true,
        edittype: 'select',
        formatter: 'select',
        editable: true,
        editrules: true,
        editoptions: {
            value: {
                2: '2',
                3: '3',
                4: '4',
                5: '5',
                6: '6',
                7: '7',
                8: '8',
                9: '9',
                10: '10'
            }
        },
        formatoptions: {
            value: {
                2: '2',
                3: '3',
                4: '4',
                5: '5',
                6: '6',
                7: '7',
                8: '8',
                9: '9',
                10: '10'
            }
        }
    }, {
        label: '子系统名称',
        name: 'DB_CN_NAME',
        width: '50px',
        editable: true,
        editrules: true
    }, {
        label: '子系统驱动',
        name: GetLowUpp('DB_DRiverClassName'),
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '子系统url',
        name: GetLowUpp('DB_url'),
        width: '70px',
        editable: true,
        editrules: {
            custom: true,
            custom_func: check_value_length
        }
    }, {
        label: '子系统登录账号',
        name: GetLowUpp('DB_username'),
        width: '50px',
        editable: true,
        editrules: {
            custom: true,
            custom_func: check_value_length
        }
    }, {
        label: '子系统登录密码',
        name: GetLowUpp('DB_password'),
        width: '50px',
        edittype: "password",
        editable: true,
        editrules: {
            custom: true,
            custom_func: check_value_length
        }
    }, {
        label: '子系统版本号 ',
        name: GetLowUpp('DB_version'),
        width: '50px',
        editable: true,
        editrules: true
    }, {
        label: '子系统编码 ',
        name: GetLowUpp('DB_code'),
        width: '50px',
        editable: true,
        editrules: true
    }, {
        label: '备注',
        name: 'S_DESC',
        width: '50px',
        editable: true,
        editrules: true
    }, {
        label: '系统时间',
        name: 'CREATE_DATE',
        width: '30px',
        editable: true,
        editrules: true,
        readOnly: true,
        formatter: function (value, row) {
            return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
        },
        editoptions: {
            defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')
        }
    }
];

function check_value_length(input_value, colname) {
    if (input_value.length < 2 || input_value == "*")
        return [false, colname + ":请输入正确的值"];
    else
        return [true, ""];
}

function T01_truncate_table(input) {
    data = input.T01_truncate_table;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
}

function A01_A1B2C3(input) {
    data = input.A01_A1B2C3;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
}

function get_init_page(input) {
    data = input.init_page;
	layer.close(ly_index);
    //查询失败
    if (Call_OpeResult(data) == false)
        return false;
}

function T01_sel_t_db_config(input) {
    data = input.T01_sel_t_db_config;	
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;	
	
    //$("#table_list_1").trigger("reloadGrid");	
    var total = $("#table_list_1").jqGrid('getGridParam', 'records')
        if (typeof(total) == "undefined") {
            $("#table_list_1").jqGrid({
                data: data,
                datatype: "local",
                height: "450",
                autowidth: true,
                shrinkToFit: true,
                rowNum: 10,
                rowList: [10, 20, 30],
                colModel: s_t_db_config,
                editurl: 'clientArray',
                altRows: true,
                pager: "#pager_list_1",
                viewrecords: true,
                caption: "子系统配置列表&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' id= 'del_truncate' value='清除冗余数据'>&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' id= 'get_initpage' value='读取全局变量信息'>&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' id= 'get_A01_A1B2C3' value='同步更新设备时间'>"
            });

            $("#del_truncate").click(function () {
                swal({
                    title: "告警",
                    text: "是否删除冗余数据",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确定",
                    closeOnConfirm: false
                },
                    function () {
                    ly_index = layer.load();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: baseUrl,
                        data: {
                            param_name: "T01_truncate_table",
                            session_id: session_id
                        },
                        //跨域请求的URL
                        dataType: "jsonp",
                        jsonp: "jsoncallback",
                        jsonpCallback: "T01_truncate_table",
                        success: function () {},
                        error: function () {
                            layer.close(ly_index);
                            swal({
                                title: "告警",
                                text: "网络异常或系统故障，请刷新页面！",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "刷新",
                                closeOnConfirm: false
                            },
                                function () {
                                layer.close(ly_index);
                                window.location.reload();
                            });
                        },
                        // 请求完成后的回调函数 (请求成功或失败之后均调用)
                        complete: function (XMLHttpRequest, textStatus) {
                            $("#doing").empty();
                            $("#doing").attr("style", "display:none");
                        }
                    });
                });
            });

            $("#get_initpage").click(function () {
                swal({
                    title: "告警",
                    text: "是否读取全局变量信息",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确定",
                    closeOnConfirm: false
                },
                    function () {
                    ly_index = layer.load();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: baseUrl,
                        data: {
                            param_name: "init_page",
                            session_id: session_id
                        },
                        //跨域请求的URL
                        dataType: "jsonp",
                        jsonp: "jsoncallback",
                        jsonpCallback: "get_init_page",
                        success: function () {},
                        error: function () {
                            layer.close(ly_index);
                            swal({
                                title: "告警",
                                text: "网络异常或系统故障，请刷新页面！",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "刷新",
                                closeOnConfirm: false
                            },
                                function () {
                                layer.close(ly_index);
                                window.location.reload();
                            });
                        },
                        // 请求完成后的回调函数 (请求成功或失败之后均调用)
                        complete: function (XMLHttpRequest, textStatus) {
                            $("#doing").empty();
                            $("#doing").attr("style", "display:none");
                        }
                    });
                });
            });

            $("#get_A01_A1B2C3").click(function () {
                swal({
                    title: "告警",
                    text: "是否同步更新设备时间",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确定",
                    closeOnConfirm: false
                },
                    function () {
                    ly_index = layer.load();
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: baseUrl,
                        data: {
                            param_name: "A01_A1B2C3",
                            session_id: session_id,
                            param_value1: "1",
                            param_value2: new Date().Format('yyyy-MM-dd hh:mm:ss'),
                            param_value3: ""
                        },
                        //跨域请求的URL
                        dataType: "jsonp",
                        jsonp: "jsoncallback",
                        jsonpCallback: "A01_A1B2C3",
                        success: function () {},
                        error: function () {
                            layer.close(ly_index);
                            swal({
                                title: "告警",
                                text: "网络异常或系统故障，请刷新页面！",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "刷新",
                                closeOnConfirm: false
                            },
                                function () {
                                layer.close(ly_index);
                                window.location.reload();
                            });
                        },
                        // 请求完成后的回调函数 (请求成功或失败之后均调用)
                        complete: function (XMLHttpRequest, textStatus) {
                            $("#doing").empty();
                            $("#doing").attr("style", "display:none");
                        }
                    });
                });
            });

            //导航后面，增加增删改查按钮
            $("#table_list_1").jqGrid("navGrid", "#pager_list_1", {
                edit: true,
                add: true,
                del: true,
                search: true,
                refresh: true,
                position: "left",
                cloneToTop: false
                //view: true,
            }, {
                beforeShowForm: function () {
                    $('#MAIN_ID').attr('readOnly', true);
                    $("#MAIN_ID").attr('disabled', "disabled");
                    laydate.render({
                        elem: '#CREATE_DATE',
                        type: 'datetime'
                    });
                },
                onclickSubmit: updateDbByMainId,
                closeAfterEdit: true,
                reloadAfterSubmit: true
            }, {
                beforeShowForm: function () {
                    var jsonDataOrderList1 = $("#table_list_1").jqGrid("getRowData");
                    var initMain_ID = 1;

                    $.each(jsonDataOrderList1, function (i, obj) {
                        if (obj.MAIN_ID > initMain_ID + 1) {
                            initMain_ID = initMain_ID + 1;
                            return false;
                        } else if (i != jsonDataOrderList1.length - 1) {
                            initMain_ID = initMain_ID + 1;
                        } else if (i == jsonDataOrderList1.length - 1) {
                            initMain_ID = initMain_ID + 2;
                        }
                    });
                    $('#MAIN_ID').attr('readOnly', true);
                    $("#MAIN_ID").attr('disabled', "disabled");
                    $("#MAIN_ID").find("option[value = '" + (initMain_ID).toString() + "']").attr("selected", "selected");
                    laydate.render({
                        elem: '#CREATE_DATE',
                        type: 'datetime'
                    });
                },
                beforeSubmit: insertDb
            }, {
                top: iTop,
                left: ileft,
                onclickSubmit: delDbByMainId,
                reloadAfterSubmit: true
            }, {
                closeAfterSearch: true
            });
        } else {
            $("#table_list_1").jqGrid("clearGridData");
            $("#table_list_1").jqGrid('setGridParam', {
                data: data
            }).trigger("reloadGrid");
        }
        layer.close(ly_index);
}

//初始化
function init() {
    iTop = (winHeight - 300) / 2;
    ileft = (winWidth - 300) / 2;
    ly_index = layer.load();
    $.jgrid.defaults.styleUI = "Bootstrap";
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_t_db_config",
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_db_config",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                layer.close(ly_index);
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

$(document).ready(function () {
    init_page();
    getWindowSize();
    init();
    $(window).bind("resize",
        function () {
        var width = $(".jqGrid_wrapper").width();
        $("#table_list_1").setGridWidth(width);
    });
});

function T01_ins_t_db_config(input) {
    data = input.T01_ins_t_db_config;
	layer.close(ly_index);
    //查询失败
    if (Call_OpeResult(data) == false)
        return false;
	else {
        submit_success();
    }
}

var insertDb = function () {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_ins_t_db_config",
            session_id: session_id,
            param_value1: $("#MAIN_ID").val(),
            param_value2: $("#DB_CN_NAME").val(),
            param_value3: $("#DB_DRiverClassName").val(),
            param_value4: s_encode($("#DB_url").val()),
            param_value5: $("#DB_username").val(),
            param_value6: $("#DB_password").val(),
            param_value7: $("#DB_version").val(),
            param_value8: $("#DB_code").val(),
            param_value9: $("#S_DESC").val(),
            param_value10: $("#CREATE_DATE").val()
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_ins_t_db_config",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_upd_t_db_config(input) {
    data = input.T01_upd_t_db_config;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
	else {
        $("#table_list_1").trigger("reloadGrid");
    }
    layer.close(ly_index);
}

var updateDbByMainId = function () {
    ly_index = layer.load();
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_upd_t_db_config",
            session_id: session_id,
            param_value1: $("#DB_CN_NAME").val(),
            param_value2: $("#DB_DRiverClassName").val(),
            param_value3: s_encode($("#DB_url").val()),
            param_value4: $("#DB_username").val(),
            param_value5: $("#DB_password").val(),
            param_value6: $("#DB_version").val(),
            param_value7: $("#DB_code").val(),
            param_value8: $("#S_DESC").val(),
            param_value9: $("#CREATE_DATE").val(),
            param_value10: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_upd_t_db_config",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                layer.close(ly_index);
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_del_t_db_config(input) {
    data = input.T01_del_t_db_config;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
	else {
        $("#table_list_1").trigger("reloadGrid");
    }
    layer.close(ly_index);

    //重新设置jqgrid大小尺寸
    //getWindowSize();
    /*
    $(" #delmodgrid-table").css({
    "top": iTop,
    "left": ileft,
    "height": "140px",
    "width": "390px"
    });*/
}

var delDbByMainId = function () {
    iResult = false;
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_del_t_db_config",
            session_id: session_id,
            param_value1: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_del_t_db_config",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                layer.close(ly_index);
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};
