#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''
import threading
objModbus_rtu = {}
ht_t_param_value = {}
ht_t_code_element = []
ht_t_modbus_addr = []
ht_t_protocol = []
ht_v_prot_element = []
ht_monitor_sec = {}
ht_monitor_flag_sec = {}
ht_v_server_protocol = []
allmonitor_data_min_last = None
allmonitor_flag_min_last = None
strCon = "&sub_code=8A0731CC39614C90A5D474BC17253713&sub_usercode=414A6DB3BBE6419DA3768E6E25127310&param_name="
objTimeFlag = True
objUpHourFlag = True
attLock = threading.Lock()
w000000time = None
w000000data = 0.0
codsampletime = None
objAB_Alarm_model = None
A01_905MFA_ARY = ["A01_00K4T3","A01_00T5L4","A01_77H4UY","A01_V47TK1","A01_88H5UT","A01_99N4T3","A01_11N55T","A01_KKVVT3","A01_00T6L5","A01_MKK4A4"]