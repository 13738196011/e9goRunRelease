//选择某一行
var AA9E13_select_menu_info_rowId = "";
//按钮事件新增或编辑
var AA9E13_type = "";
//其他页面传到本页面参数
var AA9E13_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var AA9E13_tem_group_id = "0";var AA9E13_tem_menu_name = "";var AA9E13_tem_is_ava = "0";var AA9E13_tem_role_id = "";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var AA9E13_ary_group_id = null;var AA9E13_ary_is_ava = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];var AA9E13_ary_role_id = [{'main_id': '1', 'cn_name': '开发者'}, {'main_id': '2', 'cn_name': '系统管理员'}, {'main_id': '3', 'cn_name': '运维人员'}, {'main_id': '4', 'cn_name': '普通用户'}];
/*declare select options end*/

//业务逻辑数据开始
function AA9E13_menu_info_biz_start(inputparam) {
	layer.close(ly_index);
	AA9E13_param = inputparam;
    /*biz begin*/
    var inputdata = {        "param_name": "N01_menu_info$group_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AA9E13_get_N01_menu_info$group_id");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function AA9E13_format_group_id(value, row, index) {    var objResult = value;    for(i = 0; i < AA9E13_ary_group_id.length; i++) {        var obj = AA9E13_ary_group_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AA9E13_format_is_ava(value, row, index) {    var objResult = value;    for(i = 0; i < AA9E13_ary_is_ava.length; i++) {        var obj = AA9E13_ary_is_ava[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AA9E13_format_role_id(value, row, index) {    var objResult = value;    for(i = 0; i < AA9E13_ary_role_id.length; i++) {        var obj = AA9E13_ary_role_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AA9E13_get_N01_menu_info$group_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_menu_info$group_id) == false)        return false;    AA9E13_ary_group_id = input.N01_menu_info$group_id;    $("#AA9E13_qry_group_id").append("<option value='-1'></option>")    $.each(AA9E13_ary_group_id, function (i, obj) {        addOptionValue("AA9E13_qry_group_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    $("#AA9E13_qry_is_ava").append("<option value='-1'></option>")    $.each(AA9E13_ary_is_ava, function (i, obj) {        addOptionValue("AA9E13_qry_is_ava", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    $("#AA9E13_qry_role_id").append("<option value='-1'></option>")    $.each(AA9E13_ary_role_id, function (i, obj) {        addOptionValue("AA9E13_qry_role_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    AA9E13_init_menu_info();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function AA9E13_page_end(){
}

//菜单页面显示列定义
var AA9E13_menu_info = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '菜单组',        field: 'GROUP_ID',        sortable: true        ,formatter: AA9E13_format_group_id    },    {        title: '菜单url',        field: 'MENU_URL',        sortable: true        ,formatter: set_s_decode    },    {        title: '菜单名称',        field: 'MENU_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '是否启用',        field: 'IS_AVA',        sortable: true        ,formatter: AA9E13_format_is_ava    },    {        title: '角色',        field: 'ROLE_ID',        sortable: true        ,formatter: AA9E13_format_role_id    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function AA9E13_init_menu_info() {
	$(window).resize(function () {
		  $('#AA9E13_menu_info_Events').bootstrapTable('resetView');
	});
	//菜单页面查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AA9E13_btn_menu_info_query').click();
}

//查询接口
function AA9E13_menu_info_query() {
    $('#AA9E13_menu_info_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_menu_info",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": AA9E13_tem_group_id        ,"param_value2": AA9E13_tem_group_id        ,"param_value3": s_encode(AA9E13_tem_menu_name)        ,"param_value4": AA9E13_tem_is_ava        ,"param_value5": AA9E13_tem_is_ava        ,"param_value6": s_encode(AA9E13_tem_role_id)		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AA9E13_get_N01_sel_menu_info");
}

//查询结果
function AA9E13_get_N01_sel_menu_info(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_menu_info) == false)
        return false;    
	var s_data = input.N01_sel_menu_info;
    AA9E13_select_menu_info_rowId = "";
    $('#AA9E13_menu_info_Events').bootstrapTable('destroy');
    $("#AA9E13_menu_info_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AA9E13_menu_info_Events').find("tr.changeColor").removeClass('changeColor');
                AA9E13_select_menu_info_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AA9E13_menu_info_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AA9E13_select_menu_info_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AA9E13_param.hasOwnProperty("target_name"))
			{
				$("#"+AA9E13_param["target_id"]).val(eval("row."+AA9E13_param["sourc_id"].toUpperCase()));
				$("#"+AA9E13_param["target_name"]).val(s_decode(eval("row."+AA9E13_param["sourc_name"].toUpperCase())));				
				layer.close(AA9E13_param["ly_index"]);
			}
		},
        toolbar: "#AA9E13_menu_info_Toolbar",
        columns: AA9E13_menu_info,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AA9E13_btn_menu_info_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    AA9E13_tem_group_id = "0";    AA9E13_tem_menu_name = "";    AA9E13_tem_is_ava = "0";    AA9E13_tem_role_id = "";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    BA9E13_ary_group_id = null;
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_menu_info$group_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AA9E13_get_N01_menu_info$group_id");	
    /*biz end*/
})

//查询按钮
$('#AA9E13_btn_menu_info_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    AA9E13_tem_group_id = $("#AA9E13_qry_group_id").val();    AA9E13_tem_menu_name = $("#AA9E13_qry_menu_name").val();    AA9E13_tem_is_ava = $("#AA9E13_qry_is_ava").val();    AA9E13_tem_role_id = $("#AA9E13_qry_role_id").val();	
    /*set query param end*/
	AA9E13_menu_info_query();
})

//vue回调
function AA9E13_menu_info_call_vue(objResult){
	if(index_subhtml == "menu_info_$.vue")
	{
		var n = Get_RandomDiv("BA9E13",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AA9E13_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temgroup_id = $("#AA9E13_qry_group_id").val();                if(temgroup_id != ""){                    inputdata["group_id"] = temgroup_id;                }                var temmenu_name = $("#AA9E13_qry_menu_name").val();                if(temmenu_name != ""){                    inputdata["menu_name"] = temmenu_name;                }                var temis_ava = $("#AA9E13_qry_is_ava").val();                if(temis_ava != ""){                    inputdata["is_ava"] = temis_ava;                }                var temrole_id = $("#AA9E13_qry_role_id").val();                if(temrole_id != ""){                    inputdata["role_id"] = temrole_id;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/menu_info_$.js","BA9E13_menu_info_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//新增按钮
$("#AA9E13_btn_menu_info_add").click(function () {
	AA9E13_type = "add";
	index_subhtml = "menu_info_$.vue";
	if(loadHtmlSubVueFun("biz_vue/menu_info_$.vue","AA9E13_menu_info_call_vue") == true){
		var n = Get_RandomDiv("BA9E13","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BA9E13_param["type"] = AA9E13_type;
				BA9E13_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temgroup_id = $("#AA9E13_qry_group_id").val();                if(temgroup_id != ""){                    BA9E13_param["group_id"] = temgroup_id;                }                var temmenu_name = $("#AA9E13_qry_menu_name").val();                if(temmenu_name != ""){                    BA9E13_param["menu_name"] = temmenu_name;                }                var temis_ava = $("#AA9E13_qry_is_ava").val();                if(temis_ava != ""){                    BA9E13_param["is_ava"] = temis_ava;                }                var temrole_id = $("#AA9E13_qry_role_id").val();                if(temrole_id != ""){                    BA9E13_param["role_id"] = temrole_id;                }				
				/*Send Two FindSelect param end*/

				BA9E13_clear_edit_info();
			},
			end: function(){
				BA9E13_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AA9E13_btn_menu_info_edit").click(function () {
	if (AA9E13_select_menu_info_rowId != "") {
		AA9E13_type = "edit";
		index_subhtml = "menu_info_$.vue";
		if(loadHtmlSubVueFun("biz_vue/menu_info_$.vue","AA9E13_menu_info_call_vue") == true){
			var n = Get_RandomDiv("BA9E13","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BA9E13_param["type"] = AA9E13_type;
					BA9E13_param["ly_index"] = index;
					BA9E13_clear_edit_info();
					BA9E13_get_edit_info();
				},
				end: function(){
					BA9E13_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AA9E13_btn_menu_info_delete').click(function () {
	//单行选择
	var rowData = $("#AA9E13_menu_info_Events").bootstrapTable('getData')[AA9E13_select_menu_info_rowId];
	//多行选择
	var rowDatas = AA9E13_sel_row_menu_info();
	if (AA9E13_select_menu_info_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_menu_info",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AA9E13_N01_del_menu_info");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_menu_info",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AA9E13_N01_del_menu_info");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AA9E13_N01_del_menu_info(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_menu_info) == true)
		AA9E13_menu_info_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function AA9E13_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AA9E13_sel_row_menu_info(){
	//获得选中行
	var checkedbox= $("#AA9E13_menu_info_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}