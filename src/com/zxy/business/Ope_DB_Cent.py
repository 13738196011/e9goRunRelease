#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import json, importlib, os,platform,copy
from urllib.parse import unquote
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.db1.Db_Common1 import Db_Common1
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.db3.Db_Common3 import Db_Common3
from com.zxy.db_Self.Db_Common_Self import Db_Common_Self
from com.zxy.model.T_DB_CONFIG import T_DB_CONFIG
from com.zxy.model.T_PROC_INPARAM import T_PROC_INPARAM
from com.zxy.model.T_PROC_NAME import T_PROC_NAME
from com.zxy.model.T_PROC_OUTPARAM import T_PROC_OUTPARAM
from com.zxy.model.T_PROC_RETURN import T_PROC_RETURN
from com.zxy.model.T_SUB_POWER import T_SUB_POWER
from com.zxy.model.T_SUB_SYS import T_SUB_SYS
from com.zxy.model.T_SUB_USER import T_SUB_USER
from com.zxy.model.T_SUB_USERPOWER import T_SUB_USERPOWER
from com.zxy.model.cal_fun import cal_fun
from com.zxy.model.cal_run import cal_run
from com.zxy.model.cal_run_param import cal_run_param
from com.zxy.model.cal_run_step import cal_run_step
from com.zxy.model.cal_variables import cal_variables
from com.zxy.model.table_info import table_info
from com.zxy.model.v_main_sub_link import v_main_sub_link
from com.zxy.z_debug import z_debug
class Ope_DB_Cent(z_debug):
    attICount = 0
    attS_result = 1
    def __init__(self):
        pass
    def AddMenuInfo(self,inputmenu_url,inputmenu_name):
        temDb_dbc = Db_Common2()
        temStrSql = "select main_id from menu_info where menu_url = '"+inputmenu_url+"'"
        temRs = temDb_dbc.Common_Sql(temStrSql)
        if len(temRs) == 0:
            temStrSql = "insert into menu_info(group_id,menu_url,menu_name,is_ava,role_id,create_date) " 
            temStrValue = " values('3','"+inputmenu_url+"','"+inputmenu_name+"','1','1,2,3,4','"+Com_Fun.GetTimeDef()+"')"
            temDb_dbc.CommonExec_Sql(temStrSql+temStrValue)
    def ResultSetToJson(self,input_sql):
        temDbSelf = Db_Common_Self()
        temRs = temDbSelf.Common_Sql(input_sql)
        temColumnNames = temDbSelf.attColumnNames        
        jsary1 = []
        try:
            for temItem in temRs:
                temjso1 = {}
                for i in range(0,len(temColumnNames)):
                    if temItem[i] != "null":
                        temjso1[Com_Fun.GetLowUpp(temColumnNames[i][0])] = temItem[i]
                    else:
                        temjso1[Com_Fun.GetLowUpp(temColumnNames[i][0])] = ""
                jsary1.append(temjso1)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+input_sql+"==>"+repr(e)
                self.debug_in(self,input_sql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info(self)+input_sql+"==>"+repr(e)
                self.debug_in(input_sql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return jsary1
    def get_t_proc_name_id(self,temproc_name):
        temDbSelf = Db_Common_Self()
        input_sql = "select * from t_proc_name where inf_en_name = '"+temproc_name+"'"
        temRs = temDbSelf.Common_Sql(input_sql)
        try:
            for temItem in temRs:
                return str(temItem[0])
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+input_sql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+input_sql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return "";
    def Ins_Data_JSON(self,inputjsob,inputTable_name,inputPROC_ID):
        temSqlIns = "insert into "+inputTable_name+"("
        temstrSqlValues = " values("
        temParameters = []
        temParamTypes = []
        temParamOutName = []
        temParamOutType = []
        iIndex = 0
        try:
            for key in inputjsob:
                temvalue = str(inputjsob[key]);
                if str(key).lower() != "main_id":
                    if str(key).lower() == "proc_id":
                        temvalue = inputPROC_ID
                    elif temvalue == "null":
                        temvalue = ""
                    elif temvalue == "None":
                        temvalue = ""
                    if iIndex == 0:
                        temSqlIns = temSqlIns + str(key).lower()
                        temstrSqlValues = temstrSqlValues +"?"
                    else:
                        temSqlIns = temSqlIns+","+str(key).lower()
                        temstrSqlValues = temstrSqlValues +",?"
                    temParameters.append(temvalue)
                    temParamTypes.append("STRING")
                    iIndex = iIndex + 1
            temDb_self = Db_Common_Self()
            temTpn = T_PROC_NAME()
            temTpn.attINF_EN_SQL = temSqlIns+") "+temstrSqlValues+")"        
            temDb_self.Common_Sql_Proc("Ins_Data_JSON",temParameters,temParamTypes,temParamOutName,temParamOutType,temTpn)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temSqlIns+") "+temstrSqlValues+")" +"\r\n"+repr(e)
                self.debug_in(self,temSqlIns+") "+temstrSqlValues+")" +"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temSqlIns+") "+temstrSqlValues+")" +"\r\n"+repr(e)
                self.debug_in(temSqlIns+") "+temstrSqlValues+")" +"==>"+repr(e))#打印异常信息、
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
    def SetInterSession(self):          
        temT_PROC_NAME = self.Get_T_PROC_NAME_LIST()
        for tpn in temT_PROC_NAME:
            if tpn.attIS_AUTHORITY == 1:
                Com_Fun.SetHashTable(Com_Para.htInterSession,tpn.attINF_EN_NAME,1)
    def GetMainSubInfo(self,inputtable_name,inputFileFolder):
        temListv_main_sub_link = []
        temDb2 = Db_Common2()
        temSql = "select a.MAIN_ID,d.TABLE_EN_NAME as MAIN_TABLE_EN_NAME,d.TABLE_CN_NAME as MAIN_TABLE_CN_NAME, b.COLUMN_EN_NAME as MAIN_COLUMN_EN_NAME, "
        temSql = temSql + "b.COLUMN_CN_NAME as MAIN_COLUMN_CN_NAME,e.TABLE_EN_NAME as SUB_TABLE_EN_NAME,e.TABLE_CN_NAME as SUB_TABLE_CN_NAME, "
        temSql = temSql + "c.COLUMN_EN_NAME as SUB_COLUMN_EN_NAME,c.COLUMN_CN_NAME as SUB_COLUMN_CN_NAME,f.S_DESC as SUB_RANDOM,a.MAIN_TITLE_COLUMN "
        temSql = temSql + "From t_main_sub_link a join  t_table_column b on a.MAIN_TB_NAME = b.main_id  "
        temSql = temSql + "join t_table_column c on a.SUB_TB_NAME = c.MAIN_ID join  t_single_table d on b.table_id = d.MAIN_ID "
        temSql = temSql + "join  t_single_table e on c.table_id = e.MAIN_ID "
        temSql = temSql + "join  menu_info f  on   f.menu_url like '"+inputFileFolder+"%' || e.TABLE_EN_NAME || '.vue%' where d.TABLE_EN_NAME = '"+inputtable_name+"'"
        temRs = temDb2.Common_Sql(temSql)
        try:
            for temItem in temRs:
                temvms = v_main_sub_link()
                temvms.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temvms.attMAIN_TABLE_EN_NAME = Com_Fun.NoNull(temItem[1])
                temvms.attMAIN_TABLE_CN_NAME = Com_Fun.NoNull(temItem[2])
                temvms.attMAIN_COLUMN_EN_NAME = Com_Fun.NoNull(temItem[3])
                temvms.attMAIN_COLUMN_CN_NAME = Com_Fun.NoNull(temItem[4])
                temvms.attSUB_TABLE_EN_NAME = Com_Fun.NoNull(temItem[5])
                temvms.attSUB_TABLE_CN_NAME = Com_Fun.NoNull(temItem[6])
                temvms.attSUB_COLUMN_EN_NAME = Com_Fun.NoNull(temItem[7])
                temvms.attSUB_COLUMN_CN_NAME = Com_Fun.NoNull(temItem[8])   
                temvms.attSUB_RANDOM = Com_Fun.NoNull(temItem[9])   
                temvms.attMAIN_TITLE_COLUMN = Com_Fun.NoNull(temItem[10])                  
                temListv_main_sub_link.append(temvms)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temSql+"==>"+repr(e)
                self.debug_in(self,temSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temSql+"==>"+repr(e)
                self.debug_in(temSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temListv_main_sub_link  
    def Create_StaticView_Interface(self,inputDB_ID,inputTABLE_NAME,inputCN_NAME,inputPID,inputTN01):       
        temAryCP = inputTABLE_NAME.split(".")
        if inputTABLE_NAME != "" and len(temAryCP) >= 1:
            temTABLE_NAME = temAryCP[len(temAryCP) - 1]
        temAtts = []
        temLen = {}
        temAttCN = {}
        temQryAtt = {}
        temSelectAtt = {}
        temFindAtt = {}
        objN = None
        objC = importlib.import_module(inputTABLE_NAME)  
        objN = getattr(objC,temTABLE_NAME)
        for att in objN.__dict__:
            if str(att).find("att") == 0:
                temAtts.append(str(att)[3:len(str(att))])
            elif str(att).find("len") == 0:
                temLen[str(att)[3:len(str(att))]] = str(getattr(objN,att))
        fun_us = getattr(objN,"Get_Att_CN")    #利用反射调用对象中的属性func_name
        temAttCN = fun_us(objN)        
        fun_us = getattr(objN,"Qry_Att_CN")
        temQryAtt = fun_us(objN)               
        fun_us = getattr(objN,"Select_Att_CN")
        temSelectAtt = fun_us(objN)           
        fun_us = getattr(objN,"Find_Att_CN")
        temFindAtt = fun_us(objN)
        for key in list(temSelectAtt.keys()):
            temV = Com_Fun.GetHashTable(temSelectAtt, key)
            temT_PROC_NAME = self.Get_T_PROC_NAME3(inputTN01+"_"+temTABLE_NAME+"$"+key,inputDB_ID)
            if isinstance(temV,str) and temT_PROC_NAME.attMAIN_ID == -1 and temV.lower().find("select ") == 0:
                T01_sel_DBN = T_PROC_NAME()         
                T01_sel_DBN.attINF_CN_NAME = "获取下拉框"+temTABLE_NAME+"$"+key
                T01_sel_DBN.attINF_EN_NAME = inputTN01+"_"+temTABLE_NAME+"$"+key            
                T01_sel_DBN.attINF_TYPE = 2    
                T01_sel_DBN.attCREATE_DATE = Com_Fun.GetTimeDef()                
                T01_sel_DBN.attREFLECT_IN_CLASS = ""#"com.zxy.interfaceReflect.A01_DevGrants"                
                T01_sel_DBN.attIS_AUTHORITY = "1"
                T01_sel_DBN.attINF_EN_SQL = temV
                T01_sel_DBN.attDB_ID = int(inputDB_ID)
                temTpi = T_PROC_INPARAM()                  
                temTpr = T_PROC_RETURN()
                temTsp = T_SUB_POWER()
                temTsp.attCREATE_DATE = Com_Fun.GetTimeDef()
                temTsp.attFIRST_DATE = Com_Fun.GetTimeDef()
                temTsp.attLIMIT_DATE = "2050-01-01"
                temTsp.attLIMIT_NUMBER = 10000
                temTsp.attLIMIT_TAG = 3
                temTsp.attSUB_ID = int(inputDB_ID)
                temTsp.attUSE_LIMIT = 0           
                temTssp = T_SUB_USERPOWER()            
                temTssp.attCREATE_DATE = Com_Fun.GetTimeDef()
                temTssp.attFIRST_DATE = Com_Fun.GetTimeDef()
                temTssp.attLIMIT_DATE = "2050-01-01"
                temTssp.attLIMIT_NUMBER = 10000
                temTssp.attLIMIT_TAG = 3
                temTssp.attSUB_USER_ID = int(inputDB_ID)
                temTssp.attUSE_LIMIT = 0
                temPROC_ID = self.Ins_metadata("t_proc_name",T01_sel_DBN)                    
                temTpi.attPROC_ID = temPROC_ID
                temTpr.attPROC_ID = temPROC_ID
                temTsp.attPROC_ID = temPROC_ID
                temTssp.attPROC_ID = temPROC_ID
                temTpr.attRETURN_NAME = "main_id"
                temTpr.attRETURN_TYPE = "STRING"
                self.Ins_metadata("t_proc_return",temTpr)
                temTpr.attRETURN_NAME = "cn_name"
                temTpr.attRETURN_TYPE = "STRING"
                self.Ins_metadata("t_proc_return",temTpr)
                temTsp.attPROC_ID = temPROC_ID
                self.Ins_metadata("t_sub_power",temTsp)
                temTssp.attPROC_ID = temPROC_ID
                self.Ins_metadata("t_sub_userpower",temTssp)
    def Create_SView_Interface(self,inputDB_ID,temView_NAME, inputCN_NAME,temListtable_info,inputTN01,inputViewSql):
        temAryCP = temView_NAME.split(".")
        if temView_NAME != "" and len(temAryCP) >= 1:
            inputView_NAME = temAryCP[len(temAryCP) - 1]
        if inputViewSql.upper().find("SELECT ") == 0:
            temT_PROC_NAME = self.Get_T_PROC_NAME3(inputTN01+"_sel_"+inputView_NAME,inputDB_ID)     
            if temT_PROC_NAME.attMAIN_ID == -1:
                temAtts = []
                temLen = {}
                temAttCN = {}
                temQryAtt = {}
                temSelectAtt = {}
                temFindAtt = {}
                objN = None
                objC = importlib.import_module(temView_NAME)  
                objN = getattr(objC,inputView_NAME)
                for att in objN.__dict__:
                    if str(att).find("att") == 0:
                        temAtts.append(str(att)[3:len(str(att))])
                    elif str(att).find("len") == 0:
                        temLen[str(att)[3:len(str(att))]] = str(getattr(objN,att))        
                fun_us = getattr(objN,"Get_Att_CN")    #利用反射调用对象中的属性func_name
                temAttCN = fun_us(objN)        
                fun_us = getattr(objN,"Qry_Att_CN")
                temQryAtt = fun_us(objN)               
                fun_us = getattr(objN,"Select_Att_CN")
                temSelectAtt = fun_us(objN)           
                fun_us = getattr(objN,"Find_Att_CN")
                temFindAtt = fun_us(objN)
                T01_sel_DBN = T_PROC_NAME()
                T01_sel_DBN.attDB_ID = int(inputDB_ID)
                T01_sel_DBN.attINF_CN_NAME = "获取所有"+inputCN_NAME            
                T01_sel_DBN.attINF_EN_NAME = inputTN01+"_sel_"+inputView_NAME
                T01_sel_DBN.attINF_TYPE = 2
                T01_sel_DBN.attCREATE_DATE = Com_Fun.GetTimeDef()
                T01_sel_DBN.attIS_AUTHORITY = "1"
                T01_sel_DBN.attINF_EN_SQL = inputViewSql
                temPROC_ID = self.Ins_metadata("t_proc_name",T01_sel_DBN)
                temTpi = T_PROC_INPARAM()
                temTpi.attPROC_ID = temPROC_ID
                temTpi.attCREATE_DATE = Com_Fun.GetTimeDef()
                for key in list(temQryAtt.keys()):
                    if isinstance(getattr(objN,"att"+key),str):
                        temTpi.attIS_URLENCODE = 1
                        temTpi.attPARAM_CN_NAME = key
                        temTpi.attPARAM_EN_NAME = key
                        temTpi.attPARAM_TYPE = "STRING"
                        temTpi.attPARAM_SIZE = 500           
                        self.Ins_metadata("t_proc_inparam",temTpi)
                        pass
                    elif isinstance(getattr(objN,"att"+key),int):
                        temTpi.attIS_URLENCODE = 0
                        temTpi.attPARAM_CN_NAME = key
                        temTpi.attPARAM_EN_NAME = key
                        temTpi.attPARAM_TYPE = "INT"
                        temTpi.attPARAM_SIZE = 50           
                        self.Ins_metadata("t_proc_inparam",temTpi)                    
                        temTpi.attIS_URLENCODE = 0
                        temTpi.attPARAM_CN_NAME = key
                        temTpi.attPARAM_EN_NAME = key
                        temTpi.attPARAM_TYPE = "INT"
                        temTpi.attPARAM_SIZE = 50 
                        self.Ins_metadata("t_proc_inparam",temTpi) 
                        pass
                    elif isinstance(getattr(objN,"att"+key),float):                    
                        temTpi.attIS_URLENCODE = 0
                        temTpi.attPARAM_CN_NAME = key
                        temTpi.attPARAM_EN_NAME = key
                        temTpi.attPARAM_TYPE = "FLOAT"
                        temTpi.attPARAM_SIZE = 50           
                        self.Ins_metadata("t_proc_inparam",temTpi)  
                        pass
                    elif getattr(objN,"att"+key) is None:
                        temTpi.attIS_URLENCODE = 1
                        temTpi.attPARAM_CN_NAME = "qry_begin_"+key
                        temTpi.attPARAM_EN_NAME = "qry_begin_"+key
                        temTpi.attPARAM_TYPE = "DATE"
                        temTpi.attPARAM_SIZE = 50           
                        self.Ins_metadata("t_proc_inparam",temTpi)
                        temTpi.attIS_URLENCODE = 1
                        temTpi.attPARAM_CN_NAME = "qry_end_"+key
                        temTpi.attPARAM_EN_NAME = "qry_end_"+key
                        temTpi.attPARAM_TYPE = "DATE"
                        temTpi.attPARAM_SIZE = 50           
                        self.Ins_metadata("t_proc_inparam",temTpi)
                for key in list(temFindAtt.keys()):
                    if isinstance(getattr(objN,"att"+key),str):
                        temTpi.attIS_URLENCODE = 1
                        temTpi.attPARAM_CN_NAME = key
                        temTpi.attPARAM_EN_NAME = key
                        temTpi.attPARAM_TYPE = "STRING"
                        temTpi.attPARAM_SIZE = 500           
                        self.Ins_metadata("t_proc_inparam",temTpi)
                        pass
                    elif isinstance(getattr(objN,"att"+key),int):
                        temTpi.attIS_URLENCODE = 0
                        temTpi.attPARAM_CN_NAME = key
                        temTpi.attPARAM_EN_NAME = key
                        temTpi.attPARAM_TYPE = "INT"
                        temTpi.attPARAM_SIZE = 50           
                        self.Ins_metadata("t_proc_inparam",temTpi) 
                        temTpi.attIS_URLENCODE = 0
                        temTpi.attPARAM_CN_NAME = key
                        temTpi.attPARAM_EN_NAME = key
                        temTpi.attPARAM_TYPE = "INT"
                        temTpi.attPARAM_SIZE = 50 
                        self.Ins_metadata("t_proc_inparam",temTpi) 
                        pass
                    elif isinstance(getattr(objN,"att"+key),float):                    
                        temTpi.attIS_URLENCODE = 0
                        temTpi.attPARAM_CN_NAME = key
                        temTpi.attPARAM_EN_NAME = key
                        temTpi.attPARAM_TYPE = "FLOAT"
                        temTpi.attPARAM_SIZE = 50           
                        self.Ins_metadata("t_proc_inparam",temTpi)  
                        pass
                    elif getattr(objN,"att"+key) is None:
                        temTpi.attIS_URLENCODE = 1
                        temTpi.attPARAM_CN_NAME = "qry_begin_"+key
                        temTpi.attPARAM_EN_NAME = "qry_begin_"+key
                        temTpi.attPARAM_TYPE = "DATE"
                        temTpi.attPARAM_SIZE = 50           
                        self.Ins_metadata("t_proc_inparam",temTpi)
                        temTpi.attIS_URLENCODE = 1
                        temTpi.attPARAM_CN_NAME = "qry_end_"+key
                        temTpi.attPARAM_EN_NAME = "qry_end_"+key
                        temTpi.attPARAM_TYPE = "DATE"
                        temTpi.attPARAM_SIZE = 50           
                        self.Ins_metadata("t_proc_inparam",temTpi)            
                temTpr = T_PROC_RETURN()
                temTpr.attPROC_ID = temPROC_ID
                temTpr.attCREATE_DATE = Com_Fun.GetTimeDef()
                temTpr.attIS_IMG = 0
                temTpr.attIS_URLENCODE = 1
                temTpr.attRETURN_TYPE = "STRING"
                for tti in temListtable_info:
                    temTpr.attRETURN_NAME = tti.attcolumn_en_name.upper()
                    self.Ins_metadata("t_proc_return",temTpr)                
                temTsp = T_SUB_POWER()
                temTsp.attCREATE_DATE = Com_Fun.GetTimeDef()
                temTsp.attFIRST_DATE = Com_Fun.GetTimeDef()
                temTsp.attLIMIT_DATE = "2050-01-01"
                temTsp.attLIMIT_NUMBER = 10000
                temTsp.attLIMIT_TAG = 3
                temTsp.attPROC_ID = temPROC_ID
                temTsp.attSUB_ID = int(inputDB_ID)
                temTsp.attUSE_LIMIT = 0
                self.Ins_metadata("t_sub_power",temTsp)                    
                temTssp = T_SUB_USERPOWER()
                temTssp.attCREATE_DATE = Com_Fun.GetTimeDef()
                temTssp.attFIRST_DATE = Com_Fun.GetTimeDef()
                temTssp.attLIMIT_DATE = "2050-01-01"
                temTssp.attLIMIT_NUMBER = 10000
                temTssp.attLIMIT_TAG = 3
                temTssp.attPROC_ID = temPROC_ID
                temTssp.attSUB_USER_ID = int(inputDB_ID)
                temTssp.attUSE_LIMIT = 0
                self.Ins_metadata("t_sub_userpower",temTssp)            
            return "-1"
        elif inputViewSql != "":
            temT_PROC_NAME = self.Get_T_PROC_NAME3(inputViewSql,inputDB_ID)
            return str(temT_PROC_NAME.attMAIN_ID)
        return "-1"
    def Create_StaticDB_Interface(self,inputTABLE_NAME,inputDB_ID,inputCN_NAME,inputPID_Column,inputIntTABLE_NAME,inputTN01):
        temtable_info = self.Get_Sqlite_table_info_ByDB(inputTABLE_NAME,inputDB_ID)
        temT_PROC_NAME = self.Get_T_PROC_NAME3(inputTN01+"_sel_"+inputTABLE_NAME,inputDB_ID)        
        if temT_PROC_NAME.attMAIN_ID == -1:
            temAtts = []
            temLen = {}
            temAttCN = {}
            temQryAtt = {}
            temSelectAtt = {}
            temFindAtt = {}
            objN = None
            objC = importlib.import_module(inputIntTABLE_NAME)  
            objN = getattr(objC,inputTABLE_NAME)
            for att in objN.__dict__:
                if str(att).find("att") == 0:
                    temAtts.append(str(att)[3:len(str(att))])
                elif str(att).find("len") == 0:
                    temLen[str(att)[3:len(str(att))]] = str(getattr(objN,att))        
            fun_us = getattr(objN,"Get_Att_CN")    #利用反射调用对象中的属性func_name
            temAttCN = fun_us(objN)        
            fun_us = getattr(objN,"Qry_Att_CN")
            temQryAtt = fun_us(objN)               
            fun_us = getattr(objN,"Select_Att_CN")
            temSelectAtt = fun_us(objN)           
            fun_us = getattr(objN,"Find_Att_CN")
            temFindAtt = fun_us(objN)
            T01_sel_DBN_ByID = T_PROC_NAME()
            T01_sel_DBN_ByPID = T_PROC_NAME()
            T01_sel_DBN = T_PROC_NAME()
            T01_ins_DBN = T_PROC_NAME()
            T01_upd_DBN = T_PROC_NAME()
            T01_del_DBN_ByID = T_PROC_NAME()
            T01_sel_DBN_ByID.attDB_ID = int(inputDB_ID)
            T01_sel_DBN_ByPID.attDB_ID = int(inputDB_ID)
            T01_sel_DBN.attDB_ID = int(inputDB_ID)
            T01_ins_DBN.attDB_ID = int(inputDB_ID)
            T01_upd_DBN.attDB_ID = int(inputDB_ID)
            T01_del_DBN_ByID.attDB_ID = int(inputDB_ID)
            T01_sel_DBN_ByID.attINF_CN_NAME = "根据ID获取"+inputCN_NAME
            T01_sel_DBN_ByPID.attINF_CN_NAME = "根据父ID获取"+inputCN_NAME
            T01_sel_DBN.attINF_CN_NAME = "获取所有"+inputCN_NAME
            T01_ins_DBN.attINF_CN_NAME = "添加"+inputCN_NAME
            T01_upd_DBN.attINF_CN_NAME = "根据ID修改"+inputCN_NAME
            T01_del_DBN_ByID.attINF_CN_NAME = "根据ID删除"+inputCN_NAME
            T01_sel_DBN_ByID.attINF_EN_NAME = inputTN01+"_sel_"+inputTABLE_NAME+"_ByID"
            T01_sel_DBN_ByPID.attINF_EN_NAME = inputTN01+"_sel_"+inputTABLE_NAME+"_ByPID"
            T01_sel_DBN.attINF_EN_NAME = inputTN01+"_sel_"+inputTABLE_NAME
            T01_ins_DBN.attINF_EN_NAME = inputTN01+"_ins_"+inputTABLE_NAME
            T01_upd_DBN.attINF_EN_NAME = inputTN01+"_upd_"+inputTABLE_NAME
            T01_del_DBN_ByID.attINF_EN_NAME = inputTN01+"_del_"+inputTABLE_NAME
            T01_sel_DBN_ByID.attINF_TYPE = 2
            T01_sel_DBN_ByPID.attINF_TYPE = 2
            T01_sel_DBN.attINF_TYPE = 2
            T01_ins_DBN.attINF_TYPE = 2
            T01_upd_DBN.attINF_TYPE = 2
            T01_del_DBN_ByID.attINF_TYPE = 2
            T01_sel_DBN_ByID.attCREATE_DATE = Com_Fun.GetTimeDef()
            T01_sel_DBN_ByPID.attCREATE_DATE = Com_Fun.GetTimeDef()
            T01_sel_DBN.attCREATE_DATE = Com_Fun.GetTimeDef()
            T01_ins_DBN.attCREATE_DATE = Com_Fun.GetTimeDef()
            T01_upd_DBN.attCREATE_DATE = Com_Fun.GetTimeDef()
            T01_del_DBN_ByID.attCREATE_DATE = Com_Fun.GetTimeDef()
            T01_sel_DBN_ByID.attREFLECT_IN_CLASS = ""#"com.zxy.interfaceReflect.A01_DevGrants"
            T01_sel_DBN_ByPID.attREFLECT_IN_CLASS = ""#"com.zxy.interfaceReflect.A01_DevGrants"
            T01_sel_DBN.attREFLECT_IN_CLASS = ""#"com.zxy.interfaceReflect.A01_DevGrants"
            T01_ins_DBN.attREFLECT_IN_CLASS = ""#"com.zxy.interfaceReflect.A01_DevGrants"
            T01_upd_DBN.attREFLECT_IN_CLASS = ""#"com.zxy.interfaceReflect.A01_DevGrants"
            T01_del_DBN_ByID.attREFLECT_IN_CLASS = ""#"com.zxy.interfaceReflect.A01_DevGrants"
            T01_sel_DBN_ByID.attIS_AUTHORITY = "1"
            T01_sel_DBN_ByPID.attIS_AUTHORITY = "1"
            T01_sel_DBN.attIS_AUTHORITY = "1"
            T01_ins_DBN.attIS_AUTHORITY = "1"
            T01_upd_DBN.attIS_AUTHORITY = "1"
            T01_del_DBN_ByID.attIS_AUTHORITY = "1"
            T01_sel_DBN_ByID.attINF_EN_SQL = "select "
            T01_sel_DBN_ByPID.attINF_EN_SQL = "select "
            T01_sel_DBN.attINF_EN_SQL = "select "
            T01_ins_DBN.attINF_EN_SQL = "insert into "+inputTABLE_NAME+"("
            ins_value = "values("
            T01_upd_DBN.attINF_EN_SQL = "update "+inputTABLE_NAME+" set "
            T01_del_DBN_ByID.attINF_EN_SQL = "delete from "+inputTABLE_NAME
            iFalg = False
            for tti in temtable_info:
                if tti.attname == "MAIN_ID":
                    T01_sel_DBN_ByID.attINF_EN_SQL = T01_sel_DBN_ByID.attINF_EN_SQL + tti.attname
                    T01_sel_DBN_ByPID.attINF_EN_SQL = T01_sel_DBN_ByPID.attINF_EN_SQL + tti.attname
                    T01_sel_DBN.attINF_EN_SQL = T01_sel_DBN.attINF_EN_SQL + tti.attname
                else:
                    T01_sel_DBN_ByID.attINF_EN_SQL = T01_sel_DBN_ByID.attINF_EN_SQL + "," + tti.attname
                    T01_sel_DBN_ByPID.attINF_EN_SQL = T01_sel_DBN_ByPID.attINF_EN_SQL+ "," + tti.attname
                    T01_sel_DBN.attINF_EN_SQL = T01_sel_DBN.attINF_EN_SQL+ "," + tti.attname
                    if iFalg == False:
                        iFalg = True
                        T01_ins_DBN.attINF_EN_SQL = T01_ins_DBN.attINF_EN_SQL+  tti.attname
                        ins_value = ins_value+"?"
                        T01_upd_DBN.attINF_EN_SQL = T01_upd_DBN.attINF_EN_SQL+ " " + tti.attname+"=?" 
                    else:
                        T01_ins_DBN.attINF_EN_SQL = T01_ins_DBN.attINF_EN_SQL+ "," + tti.attname
                        ins_value = ins_value+",?"
                        T01_upd_DBN.attINF_EN_SQL = T01_upd_DBN.attINF_EN_SQL+ "," + tti.attname+"=?" 
            T01_sel_DBN_ByID.attINF_EN_SQL = T01_sel_DBN_ByID.attINF_EN_SQL + " from "+inputTABLE_NAME+" where MAIN_ID=?"
            T01_sel_DBN_ByPID.attINF_EN_SQL = T01_sel_DBN_ByPID.attINF_EN_SQL + " from "+inputTABLE_NAME+" where "+inputPID_Column+"=?"
            T01_sel_DBN.attINF_EN_SQL = T01_sel_DBN.attINF_EN_SQL + " from "+inputTABLE_NAME
            T01_ins_DBN.attINF_EN_SQL = T01_ins_DBN.attINF_EN_SQL +") "+ins_value+")"
            T01_upd_DBN.attINF_EN_SQL = T01_upd_DBN.attINF_EN_SQL +" where MAIN_ID=?"
            T01_del_DBN_ByID.attINF_EN_SQL = T01_del_DBN_ByID.attINF_EN_SQL + " where MAIN_ID =?"
            T01_sel_DBN.attINF_EN_SQL = T01_sel_DBN.attINF_EN_SQL +" where 1=1 "
            for key in list(temQryAtt.keys()):
                if isinstance(getattr(objN,"att"+key),str):
                    T01_sel_DBN.attINF_EN_SQL = T01_sel_DBN.attINF_EN_SQL +" and "+key+" like \'%\'||?||\'%\'" 
                    pass
                elif isinstance(getattr(objN,"att"+key),int):
                    T01_sel_DBN.attINF_EN_SQL = T01_sel_DBN.attINF_EN_SQL + " and (case when -1=? then 1=1 else "+key+" = ? end)"
                    pass
                elif isinstance(getattr(objN,"att"+key),float):
                    T01_sel_DBN.attINF_EN_SQL = T01_sel_DBN.attINF_EN_SQL +" and "+key+" = ?" 
                    pass
                elif getattr(objN,"att"+key) is None:
                    T01_sel_DBN.attINF_EN_SQL = T01_sel_DBN.attINF_EN_SQL +" and "+key+" between ? and ?"
            for key in list(temFindAtt.keys()):
                if isinstance(getattr(objN,"att"+key),str):
                    T01_sel_DBN.attINF_EN_SQL = T01_sel_DBN.attINF_EN_SQL +" and "+key+" like \'%\'||?||\'%\'" 
                    pass
                elif isinstance(getattr(objN,"att"+key),int):
                    T01_sel_DBN.attINF_EN_SQL = T01_sel_DBN.attINF_EN_SQL + " and (case when -1=? then 1=1 else "+key+" = ? end)"
                    pass
                elif isinstance(getattr(objN,"att"+key),float):
                    T01_sel_DBN.attINF_EN_SQL = T01_sel_DBN.attINF_EN_SQL +" and "+key+" = ?" 
                    pass
                elif getattr(objN,"att"+key) is None:
                    T01_sel_DBN.attINF_EN_SQL = T01_sel_DBN.attINF_EN_SQL +" and "+key+" between ? and ?"
            temPROC_ID = self.Ins_metadata("t_proc_name",T01_sel_DBN_ByID)            
            temTpi = T_PROC_INPARAM()
            temTpi.attPROC_ID = temPROC_ID
            temTpi.attCREATE_DATE = Com_Fun.GetTimeDef()
            temTpi.attIS_URLENCODE = 0
            temTpi.attPARAM_CN_NAME = "in_MAIN_ID"
            temTpi.attPARAM_EN_NAME = "in_MAIN_ID"
            temTpi.attPARAM_TYPE = "INT"
            temTpi.attPARAM_SIZE = 50           
            self.Ins_metadata("t_proc_inparam",temTpi)            
            temTpr = T_PROC_RETURN()
            temTpr.attPROC_ID = temPROC_ID
            temTpr.attCREATE_DATE = Com_Fun.GetTimeDef()
            temTpr.attIS_IMG = 0
            temTpr.attIS_URLENCODE = 0
            temTpr.attPROC_ID = temPROC_ID
            temTpr.attRETURN_TYPE = "STRING"
            for tti in temtable_info:
                temTpr.attRETURN_NAME = tti.attname.upper()
                self.Ins_metadata("t_proc_return",temTpr)
            temTsp = T_SUB_POWER()
            temTsp.attCREATE_DATE = Com_Fun.GetTimeDef()
            temTsp.attFIRST_DATE = Com_Fun.GetTimeDef()
            temTsp.attLIMIT_DATE = "2050-01-01"
            temTsp.attLIMIT_NUMBER = 10000
            temTsp.attLIMIT_TAG = 3
            temTsp.attPROC_ID = temPROC_ID
            temTsp.attSUB_ID = int(inputDB_ID)
            temTsp.attUSE_LIMIT = 0
            self.Ins_metadata("t_sub_power",temTsp)            
            temTssp = T_SUB_USERPOWER()
            temTssp.attCREATE_DATE = Com_Fun.GetTimeDef()
            temTssp.attFIRST_DATE = Com_Fun.GetTimeDef()
            temTssp.attLIMIT_DATE = "2050-01-01"
            temTssp.attLIMIT_NUMBER = 10000
            temTssp.attLIMIT_TAG = 3
            temTssp.attPROC_ID = temPROC_ID
            temTssp.attSUB_USER_ID = int(inputDB_ID)
            temTssp.attUSE_LIMIT = 0
            self.Ins_metadata("t_sub_userpower",temTssp)            
            if inputPID_Column != "":
                temPROC_ID = self.Ins_metadata("t_proc_name",T01_sel_DBN_ByPID)
                temTpi.attPROC_ID = temPROC_ID
                temTpi.attPARAM_CN_NAME = "in_PROC_ID"
                temTpi.attPARAM_EN_NAME = "in_PROC_ID"
                temTpi.attPARAM_TYPE = "INT"
                temTpi.attPARAM_SIZE = 50           
                self.Ins_metadata("t_proc_inparam",temTpi)
                temTpr.attPROC_ID = temPROC_ID
                for tti in temtable_info:
                    temTpr.attRETURN_NAME = tti.attname
                    if tti.atttype.upper().find("INT") == 0:
                        temTpr.attRETURN_TYPE = "INT"
                    elif tti.atttype.upper().find("FLOAT") == 0:
                        temTpr.attRETURN_TYPE = "FLOAT"
                    elif tti.atttype.upper().find("VARCHAR") == 0:
                        temTpr.attPARAM_TYPE = "STRING"
                        temTpr.attIS_URLENCODE = 1
                    elif tti.atttype.upper().find("DATETIME") == 0:
                        temTpr.attPARAM_TYPE = "DATE"
                    self.Ins_metadata("t_proc_return",temTpr)
                temTsp.attPROC_ID = temPROC_ID
                self.Ins_metadata("t_sub_power",temTsp)
                temTssp.attPROC_ID = temPROC_ID
                self.Ins_metadata("t_sub_userpower",temTssp)
            temPROC_ID = self.Ins_metadata("t_proc_name",T01_sel_DBN)
            temTpi = T_PROC_INPARAM()
            temTpi.attPROC_ID = temPROC_ID
            temTpi.attCREATE_DATE = Com_Fun.GetTimeDef()
            for key in list(temQryAtt.keys()):
                if isinstance(getattr(objN,"att"+key),str):
                    temTpi.attIS_URLENCODE = 1
                    temTpi.attPARAM_CN_NAME = key
                    temTpi.attPARAM_EN_NAME = key
                    temTpi.attPARAM_TYPE = "STRING"
                    temTpi.attPARAM_SIZE = 500           
                    self.Ins_metadata("t_proc_inparam",temTpi)
                    pass
                elif isinstance(getattr(objN,"att"+key),int):
                    temTpi.attIS_URLENCODE = 0
                    temTpi.attPARAM_CN_NAME = key
                    temTpi.attPARAM_EN_NAME = key
                    temTpi.attPARAM_TYPE = "INT"
                    temTpi.attPARAM_SIZE = 50           
                    self.Ins_metadata("t_proc_inparam",temTpi) 
                    temTpi.attIS_URLENCODE = 0
                    temTpi.attPARAM_CN_NAME = key
                    temTpi.attPARAM_EN_NAME = key
                    temTpi.attPARAM_TYPE = "INT"
                    temTpi.attPARAM_SIZE = 50 
                    self.Ins_metadata("t_proc_inparam",temTpi) 
                    pass
                elif isinstance(getattr(objN,"att"+key),float):                    
                    temTpi.attIS_URLENCODE = 0
                    temTpi.attPARAM_CN_NAME = key
                    temTpi.attPARAM_EN_NAME = key
                    temTpi.attPARAM_TYPE = "FLOAT"
                    temTpi.attPARAM_SIZE = 50           
                    self.Ins_metadata("t_proc_inparam",temTpi)  
                    pass
                elif getattr(objN,"att"+key) is None:
                    temTpi.attIS_URLENCODE = 1
                    temTpi.attPARAM_CN_NAME = "qry_begin_"+key
                    temTpi.attPARAM_EN_NAME = "qry_begin_"+key
                    temTpi.attPARAM_TYPE = "DATE"
                    temTpi.attPARAM_SIZE = 50           
                    self.Ins_metadata("t_proc_inparam",temTpi)
                    temTpi.attIS_URLENCODE = 1
                    temTpi.attPARAM_CN_NAME = "qry_end_"+key
                    temTpi.attPARAM_EN_NAME = "qry_end_"+key
                    temTpi.attPARAM_TYPE = "DATE"
                    temTpi.attPARAM_SIZE = 50           
                    self.Ins_metadata("t_proc_inparam",temTpi)
            for key in list(temFindAtt.keys()):
                if isinstance(getattr(objN,"att"+key),str):
                    temTpi.attIS_URLENCODE = 1
                    temTpi.attPARAM_CN_NAME = key
                    temTpi.attPARAM_EN_NAME = key
                    temTpi.attPARAM_TYPE = "STRING"
                    temTpi.attPARAM_SIZE = 500           
                    self.Ins_metadata("t_proc_inparam",temTpi)
                    pass
                elif isinstance(getattr(objN,"att"+key),int):
                    temTpi.attIS_URLENCODE = 0
                    temTpi.attPARAM_CN_NAME = key
                    temTpi.attPARAM_EN_NAME = key
                    temTpi.attPARAM_TYPE = "INT"
                    temTpi.attPARAM_SIZE = 50           
                    self.Ins_metadata("t_proc_inparam",temTpi) 
                    temTpi.attIS_URLENCODE = 0
                    temTpi.attPARAM_CN_NAME = key
                    temTpi.attPARAM_EN_NAME = key
                    temTpi.attPARAM_TYPE = "INT"
                    temTpi.attPARAM_SIZE = 50 
                    self.Ins_metadata("t_proc_inparam",temTpi) 
                    pass
                elif isinstance(getattr(objN,"att"+key),float):                    
                    temTpi.attIS_URLENCODE = 0
                    temTpi.attPARAM_CN_NAME = key
                    temTpi.attPARAM_EN_NAME = key
                    temTpi.attPARAM_TYPE = "FLOAT"
                    temTpi.attPARAM_SIZE = 50           
                    self.Ins_metadata("t_proc_inparam",temTpi)  
                    pass
                elif getattr(objN,"att"+key) is None:
                    temTpi.attIS_URLENCODE = 1
                    temTpi.attPARAM_CN_NAME = "qry_begin_"+key
                    temTpi.attPARAM_EN_NAME = "qry_begin_"+key
                    temTpi.attPARAM_TYPE = "DATE"
                    temTpi.attPARAM_SIZE = 50           
                    self.Ins_metadata("t_proc_inparam",temTpi)
                    temTpi.attIS_URLENCODE = 1
                    temTpi.attPARAM_CN_NAME = "qry_end_"+key
                    temTpi.attPARAM_EN_NAME = "qry_end_"+key
                    temTpi.attPARAM_TYPE = "DATE"
                    temTpi.attPARAM_SIZE = 50           
                    self.Ins_metadata("t_proc_inparam",temTpi)
            temTpi.attPROC_ID = temPROC_ID
            temTpr.attPROC_ID = temPROC_ID
            for tti in temtable_info:
                temTpr.attRETURN_NAME = tti.attname
                if tti.atttype.upper().find("INT") == 0:
                    temTpr.attRETURN_TYPE = "INT"
                elif tti.atttype.upper().find("FLOAT") == 0:
                    temTpr.attRETURN_TYPE = "FLOAT"
                elif tti.atttype.upper().find("VARCHAR") == 0:
                    temTpr.attRETURN_TYPE = "STRING"
                    temTpr.attIS_URLENCODE = 1
                elif tti.atttype.upper().find("DATETIME") == 0:
                    temTpr.attRETURN_TYPE = "STRING"
                self.Ins_metadata("t_proc_return",temTpr)            
            temTsp.attPROC_ID = temPROC_ID
            self.Ins_metadata("t_sub_power",temTsp)
            temTssp.attPROC_ID = temPROC_ID
            self.Ins_metadata("t_sub_userpower",temTssp)
            temPROC_ID = self.Ins_metadata("t_proc_name",T01_ins_DBN)
            temTpi.attPROC_ID = temPROC_ID
            for tti in temtable_info:
                if tti.attname == "MAIN_ID":
                    pass
                else:
                    temTpi.attPARAM_CN_NAME = "in_"+tti.attname
                    temTpi.attPARAM_EN_NAME = "in_"+tti.attname
                    if tti.atttype.upper().find("INT") == 0:
                        temTpi.attPARAM_TYPE = "INT"
                        temTpi.attPARAM_SIZE = 50
                    elif tti.atttype.upper().find("FLOAT") == 0:
                        temTpi.attPARAM_TYPE = "FLOAT"
                        temTpi.attPARAM_SIZE = 50
                    elif tti.atttype.upper().find("VARCHAR") == 0:
                        temTpi.attIS_URLENCODE = 1
                        temTpi.attPARAM_TYPE = "STRING"
                        temTpi.attPARAM_SIZE = int(tti.atttype.upper().replace("VARCHAR(","").replace(")",""))
                    elif tti.atttype.upper().find("DATETIME") == 0:
                        temTpi.attPARAM_TYPE = "DATE"
                        temTpi.attPARAM_SIZE = 50
                        temTpi.attIS_URLENCODE = 1
                    self.Ins_metadata("t_proc_inparam",temTpi)
            temTpr.attPROC_ID = temPROC_ID
            temTpr.attRETURN_NAME = "s_result"
            temTpr.attRETURN_TYPE = "INT"
            self.Ins_metadata("t_proc_return",temTpr)
            temTpr.attRETURN_NAME = "error_desc"
            temTpr.attRETURN_TYPE = "STRING"
            self.Ins_metadata("t_proc_return",temTpr)            
            temTsp.attPROC_ID = temPROC_ID
            self.Ins_metadata("t_sub_power",temTsp)            
            temTssp.attPROC_ID = temPROC_ID
            self.Ins_metadata("t_sub_userpower",temTssp)
            temPROC_ID = self.Ins_metadata("t_proc_name",T01_upd_DBN)
            temTpi.attPROC_ID = temPROC_ID
            for tti in temtable_info:
                if tti.attname == "MAIN_ID":
                    pass
                else:
                    temTpi.attPARAM_CN_NAME = "in_"+tti.attname
                    temTpi.attPARAM_EN_NAME = "in_"+tti.attname
                    if tti.atttype.upper().find("INT") == 0:
                        temTpi.attPARAM_TYPE = "INT"
                        temTpi.attPARAM_SIZE = 50
                    elif tti.atttype.upper().find("FLOAT") == 0:
                        temTpi.attPARAM_TYPE = "FLOAT"
                        temTpi.attPARAM_SIZE = 50
                    elif tti.atttype.upper().find("VARCHAR") == 0:
                        temTpi.attIS_URLENCODE = 1
                        temTpi.attPARAM_TYPE = "STRING"
                        temTpi.attPARAM_SIZE = int(tti.atttype.upper().replace("VARCHAR(","").replace(")",""))
                    elif tti.atttype.upper().find("DATETIME") == 0:
                        temTpi.attPARAM_TYPE = "DATE"
                        temTpi.attPARAM_SIZE = 50
                        temTpi.attIS_URLENCODE = 1
                    self.Ins_metadata("t_proc_inparam",temTpi)
            temTpi.attPARAM_CN_NAME = "in_MAIN_ID"
            temTpi.attPARAM_EN_NAME = "in_MAIN_ID"
            temTpi.attPARAM_TYPE = "INT"
            temTpi.attPARAM_SIZE = 50           
            self.Ins_metadata("t_proc_inparam",temTpi)
            temTpr.attPROC_ID = temPROC_ID
            temTpr.attRETURN_NAME = "s_result"
            temTpr.attRETURN_TYPE = "INT"
            self.Ins_metadata("t_proc_return",temTpr)
            temTpr.attRETURN_NAME = "error_desc"
            temTpr.attRETURN_TYPE = "STRING"
            self.Ins_metadata("t_proc_return",temTpr)
            temTsp.attPROC_ID = temPROC_ID
            self.Ins_metadata("t_sub_power",temTsp)
            temTssp.attPROC_ID = temPROC_ID
            self.Ins_metadata("t_sub_userpower",temTssp)
            temPROC_ID = self.Ins_metadata("t_proc_name",T01_del_DBN_ByID)
            temTpi.attPROC_ID = temPROC_ID
            temTpi.attPARAM_CN_NAME = "in_MAIN_ID"
            temTpi.attPARAM_EN_NAME = "in_MAIN_ID"
            temTpi.attPARAM_TYPE = "INT"
            temTpi.attPARAM_SIZE = 50           
            self.Ins_metadata("t_proc_inparam",temTpi)
            temTpr.attPROC_ID = temPROC_ID
            temTpr.attRETURN_NAME = "s_result"
            temTpr.attRETURN_TYPE = "INT"
            self.Ins_metadata("t_proc_return",temTpr)
            temTpr.attRETURN_NAME = "error_desc"
            temTpr.attRETURN_TYPE = "STRING"
            self.Ins_metadata("t_proc_return",temTpr)
            temTsp.attPROC_ID = temPROC_ID
            self.Ins_metadata("t_sub_power",temTsp)
            temTssp.attPROC_ID = temPROC_ID
            self.Ins_metadata("t_sub_userpower",temTssp)
    def Ins_metadata(self,inputT_TB_NAME,objTag):
        temtable_info = self.Get_Sqlite_table_info(inputT_TB_NAME)        
        try:
            temParameters = []
            temParamTypes = []
            temParamOutName = []
            temParamOutType = []
            temStrSql = "insert into "+inputT_TB_NAME+"(S_DESC" 
            temStrValue = "values(?"
            temParameters.append("")
            temParamTypes.append("STRING")       
            for tti in temtable_info:
                if tti.attname != "MAIN_ID" and tti.attname != "S_DESC":
                    temStrSql = temStrSql + ","+tti.attname
                    temStrValue = temStrValue+",?" 
                    temParameters.append(str(getattr(objTag, "att"+tti.attname.upper())))
                    temParamTypes.append("STRING")      
            temSqlIns = temStrSql+") "+temStrValue+")"    
            temDb_self = Db_Common1()
            temTpn = T_PROC_NAME()
            temTpn.attINF_EN_SQL = temSqlIns        
            temDb_self.Common_Sql_Proc("Ins_metadata",temParameters,temParamTypes,temParamOutName,temParamOutType,temTpn)
            return temDb_self.attlastrowid
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temSqlIns +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temSqlIns +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
            return -1
    def Get_Center_Data_Sel(self,input_type,input_sql):
        temRs = None
        temColumnNames = None
        if input_type == "110":
            temDbSelf = Db_Common_Self()
            temRs = temDbSelf.Common_Sql(input_sql)
            temColumnNames = temDbSelf.attColumnNames
        elif input_type == "111":
            temDbSelf = Db_Common_Self()
            temRs = temDbSelf.CommonExec_Sql(input_sql)
            temColumnNames = temDbSelf.attColumnNames
        elif input_type == "120":
            temDb2 = Db_Common2()
            temRs = temDb2.Common_Sql(input_sql)
            temColumnNames = temDb2.attColumnNames
        elif input_type == "121":
            temDb2 = Db_Common2()
            temRs = temDb2.CommonExec_Sql(input_sql)
            temColumnNames = temDb2.attColumnNames
        temResult = ""
        try:
            for temColumn in temColumnNames:
                temResult = temResult+temColumn[0]+"    "
            temResult = temResult + "\r"
            for temItem in temRs:
                for temVal in temItem:
                    temR = str(temVal)
                    temResult = temResult+temR+"    "
                temResult = temResult + "\r"
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+input_sql+"==>"+repr(e)
                self.debug_in(self,input_sql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info(self)+input_sql+"==>"+repr(e)
                self.debug_in(input_sql+"==>"+repr(e))#打印异常信息、
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temResult
    def Create_View4(self,inputDB_ID,inputView_NAME,inputCN_NAME,temListtable_info,inputViewSql):
        temAryCP = inputView_NAME.split(".")
        if inputView_NAME != "" and len(temAryCP) >= 1:
            inputView_NAME = temAryCP[len(temAryCP) - 1]
        self.Create_SView_Interface(inputView_NAME,inputDB_ID, inputCN_NAME,temListtable_info,"N01",inputViewSql)
    def Create_Table3(self,inputDB_ID,inputTABLE_NAME,inputCN_NAME,inputPID_Column):
        temAryCP = inputTABLE_NAME.split(".")
        if inputTABLE_NAME != "" and len(temAryCP) >= 1:
            temTABLE_NAME = temAryCP[len(temAryCP) - 1]
            temAtts = []
            temLen = {}          
            temListtable_info = self.Get_Sqlite_table_info_ByDB(temTABLE_NAME,inputDB_ID)
            if temListtable_info is not None and len(temListtable_info) == 0:
                try:
                    objC = importlib.import_module(inputTABLE_NAME)  
                    objN = getattr(objC,temTABLE_NAME)
                    for att in objN.__dict__:
                        if str(att).find("att") == 0:
                            temAtts.append(str(att)[3:len(str(att))])
                        elif str(att).find("len") == 0:
                            temLen[str(att)[3:len(str(att))]] = str(getattr(objN,att))
                except Exception as e:    
                    if str(type(self)) == "<class 'type'>":
                        self.debug_in(self,inputTABLE_NAME+"==>"+repr(e))#打印异常信息
                    else:
                        self.debug_in(inputTABLE_NAME+"==>"+repr(e))#打印异常信息
                finally:
                    pass
                if len(temAtts) > 0:
                    temSql = "CREATE TABLE IF NOT EXISTS "+temTABLE_NAME+"( "
                    temIndex = 0
                    for temColumn in temAtts:
                        if temIndex > 0:
                            temSql = temSql +","
                        temIndex = temIndex + 1
                        if temColumn.upper() == "MAIN_ID":
                            temSql = temSql + " ["+temColumn+"] integer PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL"
                        elif getattr(objN,"att"+temColumn) is None:
                            temSql = temSql + " ["+temColumn+"] datetime DEFAULT (datetime('now'))"
                        elif isinstance(getattr(objN,"att"+temColumn),int):
                            temSql = temSql + " ["+temColumn+"] int DEFAULT 0"
                        elif isinstance(getattr(objN,"att"+temColumn),float):
                            temSql = temSql + " ["+temColumn+"] float DEFAULT 0.0"
                        elif isinstance(getattr(objN,"att"+temColumn),str):
                            temSql = temSql + " ["+temColumn+"] varchar("+temLen[temColumn]+") DEFAULT NULL"    
                    temSql = temSql +  ");"
                    self.Run_Sql_Sqlite_ByDB(inputDB_ID,temSql);
        self.Create_StaticDB_Interface(temTABLE_NAME,inputDB_ID, inputCN_NAME,inputPID_Column,inputTABLE_NAME,"N01")
    def Create_Table(self,inputDB_ID,inputTABLE_NAME,inputCN_NAME,inputPID_Column):
        temAryCP = inputTABLE_NAME.split(".")
        if inputTABLE_NAME != "" and len(temAryCP) >= 1:
            temTABLE_NAME = temAryCP[len(temAryCP) - 1]
            temAtts = []
            temLen = {}          
            temListtable_info = self.Get_Sqlite_table_info_ByDB(temTABLE_NAME,inputDB_ID)
            if temListtable_info is not None and len(temListtable_info) == 0:
                try:
                    objC = importlib.import_module(inputTABLE_NAME)  
                    objN = getattr(objC,temTABLE_NAME)
                    for att in objN.__dict__:
                        if str(att).find("att") == 0:
                            temAtts.append(str(att)[3:len(str(att))])
                        elif str(att).find("len") == 0:
                            temLen[str(att)[3:len(str(att))]] = str(getattr(objN,att))
                except Exception as e:    
                    if str(type(self)) == "<class 'type'>":
                        self.debug_in(self,inputTABLE_NAME+"==>"+repr(e))#打印异常信息
                    else:
                        self.debug_in(inputTABLE_NAME+"==>"+repr(e))#打印异常信息
                finally:
                    pass
                if len(temAtts) > 0:
                    temSql = "CREATE TABLE IF NOT EXISTS "+temTABLE_NAME+"( "
                    temIndex = 0
                    for temColumn in temAtts:
                        if temIndex > 0:
                            temSql = temSql +","
                        temIndex = temIndex + 1
                        if temColumn.upper() == "MAIN_ID":
                            temSql = temSql + " ["+temColumn+"] integer PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL"
                        elif getattr(objN,"att"+temColumn) is None:
                            temSql = temSql + " ["+temColumn+"] datetime DEFAULT (datetime('now'))"
                        elif isinstance(getattr(objN,"att"+temColumn),int):
                            temSql = temSql + " ["+temColumn+"] int DEFAULT 0"
                        elif isinstance(getattr(objN,"att"+temColumn),float):
                            temSql = temSql + " ["+temColumn+"] float DEFAULT 0.0"
                        elif isinstance(getattr(objN,"att"+temColumn),str):
                            temSql = temSql + " ["+temColumn+"] varchar("+temLen[temColumn]+") DEFAULT NULL"    
                    temSql = temSql +  ");"
                    self.Run_Sql_Sqlite_ByDB(inputDB_ID,temSql);
        self.Create_StaticDB_Interface(temTABLE_NAME,inputDB_ID, inputCN_NAME,inputPID_Column,inputTABLE_NAME,"T01")
    def Ins_metadata_ByDB_NoInc(self,inputT_TB_NAME,objTag,inputDB_ID,inputNoInclude):
        temDb_dbc = None
        if inputDB_ID == "0":
            temDb_dbc = Db_Common_Self()
        elif inputDB_ID == "1":
            temDb_dbc = Db_Common1()
        elif inputDB_ID == "2":
            temDb_dbc = Db_Common2()
        elif inputDB_ID == "3":
            temDb_dbc = Db_Common3()
        temtable_info = self.Get_Sqlite_table_info_ByDB(inputT_TB_NAME,inputDB_ID)        
        temStrSql = "insert into "+inputT_TB_NAME+"(S_DESC,CREATE_DATE" 
        temStrValue = "values('','"+Com_Fun.GetTimeDef()+"'"
        temResult = False
        try:             
            for tti in temtable_info:
                temV = str(getattr(objTag, "att"+tti.attname.upper()))
                if tti.attname.upper() != "MAIN_ID" and tti.attname.upper() != "S_DESC" and tti.attname.upper() != "CREATE_DATE":
                    temStrSql = temStrSql + ","+tti.attname
                    if str(getattr(objTag, "att"+tti.attname.lower())) != "" and tti.attname.lower() not in inputNoInclude:
                        temResult = True
                    temStrValue = temStrValue+",'"+str(getattr(objTag, "att"+tti.attname.upper()))+"'"
        except Exception as e:
            for tti in temtable_info:
                if tti.attname.upper() != "MAIN_ID" and tti.attname.upper() != "S_DESC" and tti.attname.upper() != "CREATE_DATE":
                    if str(getattr(objTag, "att"+tti.attname.lower())) != "" and tti.attname.lower() not in inputNoInclude:
                        temResult = True
                    temStrSql = temStrSql + ","+tti.attname
                    temStrValue = temStrValue+",'"+str(getattr(objTag, "att"+tti.attname.lower()))+"'"
        if temResult == False:
            return -1
        return temDb_dbc.CommonExec_Sql(temStrSql+") "+temStrValue+")")
    def Ins_metadata_ByDB(self,inputT_TB_NAME,objTag,inputDB_ID):
        temDb_dbc = None
        if inputDB_ID == "0":
            temDb_dbc = Db_Common_Self()
        elif inputDB_ID == "1":
            temDb_dbc = Db_Common1()
        elif inputDB_ID == "2":
            temDb_dbc = Db_Common2()
        elif inputDB_ID == "3":
            temDb_dbc = Db_Common3()
        temtable_info = self.Get_Sqlite_table_info_ByDB(inputT_TB_NAME,inputDB_ID)        
        temStrSql = "insert into "+inputT_TB_NAME+"(S_DESC,CREATE_DATE" 
        temStrValue = "values('','"+Com_Fun.GetTimeDef()+"'"
        temResult = False
        try:             
            for tti in temtable_info:
                temV = str(getattr(objTag, "att"+tti.attname.upper()))
                if tti.attname.upper() != "MAIN_ID" and tti.attname.upper() != "S_DESC"  and tti.attname.upper() != "CREATE_DATE":
                    temStrSql = temStrSql + ","+tti.attname
                    if str(getattr(objTag, "att"+tti.attname.lower())) != "":
                        temResult = True
                    temStrValue = temStrValue+",'"+str(getattr(objTag, "att"+tti.attname.upper()))+"'"
        except Exception as e:
            for tti in temtable_info:
                if tti.attname.upper() != "MAIN_ID" and tti.attname.upper() != "S_DESC"  and tti.attname.upper() != "CREATE_DATE":
                    if str(getattr(objTag, "att"+tti.attname.lower())) != "":
                        temResult = True
                    temStrSql = temStrSql + ","+tti.attname
                    temStrValue = temStrValue+",'"+str(getattr(objTag, "att"+tti.attname.lower()))+"'"
        if temResult == False:
            return -1
        return temDb_dbc.CommonExec_Sql(temStrSql+") "+temStrValue+")")
    def Run_Sql_Sqlite_ByDB(self,inputDB_ID,inputSql):
        temDb_dbc = None
        if inputDB_ID == "0":
            temDb_dbc = Db_Common_Self()
        elif inputDB_ID == "1":
            temDb_dbc = Db_Common1()
        elif inputDB_ID == "2":
            temDb_dbc = Db_Common2()
        elif inputDB_ID == "3":
            temDb_dbc = Db_Common3()
        try:
            temRs = temDb_dbc.CommonExec_Sql(inputSql)
        except Exception as e:
            temLog = ""              
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+inputSql+"==>"+repr(e)
                self.debug_in(self,inputSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+inputSql+"==>"+repr(e)
                self.debug_in(inputSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temRs
    def Get_table_list_ByDB(self,inputTABLE_NAME,inputDB_ID,inputWhere):
        temListtable_info = []
        temAryCP = inputTABLE_NAME.split(".")
        if inputTABLE_NAME != "" and len(temAryCP) >= 1:
            temTABLE_NAME = temAryCP[len(temAryCP) - 1]            
            temStrSql = "select * from "+temTABLE_NAME+" "+inputWhere
            temDb_dbc = None
            if inputDB_ID == "0":
                temDb_dbc = Db_Common_Self()
            elif inputDB_ID == "1":
                temDb_dbc = Db_Common1()
            elif inputDB_ID == "2":
                temDb_dbc = Db_Common2()
            elif inputDB_ID == "3":
                temDb_dbc = Db_Common3()
            try:
                if temDb_dbc is not None:
                    temRs = temDb_dbc.Common_Sql(temStrSql)
                    objModule = importlib.import_module(inputTABLE_NAME)  
                    objModClass = getattr(objModule,temTABLE_NAME)
                    for temItem in temRs:
                        objN = None
                        objN = objModClass()
                        jIndex = 0
                        for colName in temDb_dbc.attColumnNames:
                            if isinstance(getattr(objN,"att"+colName[0].lower()),int):
                                setattr(objN,"att"+colName[0].lower(),Com_Fun.ZeroNull(temItem[jIndex]))                                
                            elif isinstance(getattr(objN,"att"+colName[0].lower()),float):
                                setattr(objN,"att"+colName[0].lower(),Com_Fun.ZeroNull(temItem[jIndex]))                                
                            elif isinstance(getattr(objN,"att"+colName[0].lower()),str):
                                setattr(objN,"att"+colName[0].lower(),Com_Fun.NoNull(temItem[jIndex]))                           
                            else:
                                setattr(objN,"att"+colName[0].lower(),Com_Fun.NoNull(temItem[jIndex]))
                            jIndex = jIndex + 1
                        temListtable_info.append(objN)
            except Exception as ee: 
                try:
                    if temDb_dbc is not None:
                        temListtable_info = []
                        temRs = temDb_dbc.Common_Sql(temStrSql)
                        objModule = importlib.import_module(inputTABLE_NAME)  
                        objModClass = getattr(objModule,temTABLE_NAME)
                        for temItem in temRs:
                            objN = None
                            objN = objModClass()
                            jIndex = 0
                            for colName in temDb_dbc.attColumnNames:
                                if isinstance(getattr(objN,"att"+colName[0].upper()),int):
                                    setattr(objN,"att"+colName[0].upper(),Com_Fun.ZeroNull(temItem[jIndex]))                                
                                elif isinstance(getattr(objN,"att"+colName[0].upper()),float):
                                    setattr(objN,"att"+colName[0].upper(),Com_Fun.ZeroNull(temItem[jIndex]))                                
                                elif isinstance(getattr(objN,"att"+colName[0].upper()),str):
                                    setattr(objN,"att"+colName[0].upper(),Com_Fun.NoNull(temItem[jIndex]))                           
                                else:
                                    setattr(objN,"att"+colName[0].upper(),Com_Fun.NoNull(temItem[jIndex]))
                                jIndex = jIndex + 1
                            temListtable_info.append(objN)
                except Exception as e:   
                    temLog = ""
                    if str(type(self)) == "<class 'type'>":
                        temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                        self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
                    else:
                        temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                        self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
                    uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
                    uL.WriteLog()
        return temListtable_info
    def Get_Sqlite_table_info_ByDB(self,inputTABLE_NAME,inputDB_ID):
        temListtable_info = []
        temStrSql = "PRAGMA table_info(["+inputTABLE_NAME+"])"
        temDb_dbc = None
        if inputDB_ID == "0":
            temDb_dbc = Db_Common_Self()
        elif inputDB_ID == "1":
            temDb_dbc = Db_Common1()
        elif inputDB_ID == "2":
            temDb_dbc = Db_Common2()
        elif inputDB_ID == "3":
            temDb_dbc = Db_Common3()
        try:
            temRs = temDb_dbc.Common_SqlNoCommit(temStrSql)
            for temItem in temRs:
                temtable_info = table_info()
                temtable_info.attcid = Com_Fun.ZeroNull(temItem[0])
                temtable_info.attname = Com_Fun.NoNull(temItem[1]).upper()
                temtable_info.atttype = Com_Fun.NoNull(temItem[2])
                temtable_info.attnotnull = Com_Fun.ZeroNull(temItem[3])
                temtable_info.attdflt_value = Com_Fun.NoNull(temItem[4])
                temtable_info.attpk = Com_Fun.ZeroNull(temItem[5])
                temListtable_info.append(temtable_info)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temListtable_info
    def Get_Sqlite_table_info(self,inputTABLE_NAME):
        temListtable_info = []
        temStrSql = "PRAGMA table_info(["+inputTABLE_NAME+"])"
        temDb_self = Db_Common_Self()
        temRs = temDb_self.Common_SqlNoCommit(temStrSql)        
        try:
            for temItem in temRs:
                temtable_info = table_info()
                temtable_info.attcid = Com_Fun.ZeroNull(temItem[0])
                temtable_info.attname = Com_Fun.NoNull(temItem[1]).upper()
                temtable_info.atttype = Com_Fun.NoNull(temItem[2])
                temtable_info.attnotnull = Com_Fun.ZeroNull(temItem[3])
                temtable_info.attdflt_value = Com_Fun.NoNull(temItem[4])
                temtable_info.attpk = Com_Fun.ZeroNull(temItem[5])
                temListtable_info.append(temtable_info)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temListtable_info
    def Get_T_DB_CONFIG_ByID(self,inputMAIN_ID):
        temStrSql = "select * from t_db_config where MAIN_ID = '" + str(inputMAIN_ID) + "'"
        temDb_self = Db_Common_Self()
        temRs = temDb_self.Common_Sql(temStrSql)
        temT_DB_CONFIG = T_DB_CONFIG()
        try:
            for temItem in temRs:
                vTime = ""
                temT_DB_CONFIG.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temT_DB_CONFIG.attDB_CN_NAME = Com_Fun.NoNull(temItem[1])
                temT_DB_CONFIG.attDB_DriverClassName = Com_Fun.NoNull(temItem[2])
                temT_DB_CONFIG.attDB_url = Com_Fun.NoNull(temItem[3])
                temT_DB_CONFIG.attDB_username = Com_Fun.NoNull(temItem[4])
                temT_DB_CONFIG.attDB_password = Com_Fun.NoNull(temItem[5])
                temT_DB_CONFIG.attDB_version = Com_Fun.NoNull(temItem[6])
                temT_DB_CONFIG.attDB_Code = Com_Fun.NoNull(temItem[7])
                temT_DB_CONFIG.attS_DESC = Com_Fun.NoNull(temItem[8])
                if len(str(temItem[9])) == 10:
                    vTime = " 00:00:00"
                temT_DB_CONFIG.attCREATE_DATE = Com_Fun.GetStrTimeInput("%Y-%m-%d %H:%M:%S", str(temItem[9])+vTime)
                break
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temT_DB_CONFIG
    def Get_T_DB_CONFIG(self):
        temListT_DB_CONFIG = []
        temStrSql = "select * from t_db_config"
        temDb_self = Db_Common_Self()
        temRs = temDb_self.Common_Sql(temStrSql)
        try:
            for temItem in temRs:
                vTime = ""
                temT_DB_CONFIG = T_DB_CONFIG()
                temT_DB_CONFIG.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temT_DB_CONFIG.attDB_CN_NAME = Com_Fun.NoNull(temItem[1])
                temT_DB_CONFIG.attDB_DriverClassName = Com_Fun.NoNull(temItem[2])
                temT_DB_CONFIG.attDB_url = Com_Fun.NoNull(temItem[3])
                temT_DB_CONFIG.attDB_username = Com_Fun.NoNull(temItem[4])
                temT_DB_CONFIG.attDB_password = Com_Fun.NoNull(temItem[5])
                temT_DB_CONFIG.attDB_version = Com_Fun.NoNull(temItem[6])
                temT_DB_CONFIG.attDB_Code = Com_Fun.NoNull(temItem[7])
                temT_DB_CONFIG.attS_DESC = Com_Fun.NoNull(temItem[8])
                if len(str(temItem[9])) == 10:
                    vTime = " 00:00:00"
                temT_DB_CONFIG.attCREATE_DATE = Com_Fun.GetStrTimeInput("%Y-%m-%d %H:%M:%S", str(temItem[9])+vTime)
                temListT_DB_CONFIG.append(temT_DB_CONFIG)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temListT_DB_CONFIG  
    def Get_T_SUB_SYS(self, inputSUB_CODE):
        temTss = T_SUB_SYS()
        temStrSql = "select * from t_sub_sys where SUB_CODE ='" + inputSUB_CODE + "'"
        temDb_self = Db_Common_Self()
        temRs = temDb_self.Common_Sql(temStrSql)
        try:
            for temItem in temRs:                
                vTime = ""
                temTss.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temTss.attSUB_NAME = Com_Fun.NoNull(temItem[1])
                temTss.attSUB_CODE = Com_Fun.NoNull(temItem[2])
                if len(str(temItem[3])) == 10:
                    vTime = " 00:00:00"
                temTss.attLIMIT_DATE = Com_Fun.GetDateInput("%Y-%m-%d", "%Y-%m-%d %H:%M:%S", str(temItem[3])+vTime)
                vTime = ""
                temTss.attS_DESC = Com_Fun.NoNull(temItem[4])
                if len(str(temItem[5])) == 10:
                    vTime = " 00:00:00"
                temTss.attCREATE_DATE = Com_Fun.GetStrTimeInput("%Y-%m-%d %H:%M:%S", str(temItem[5])+vTime)
                vTime = ""
                temTss.attIP_ADDR = Com_Fun.NoNull(temItem[6])
                break
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temTss
    def Get_T_PROC_NAME_LIST(self):
        temListT_PROC_NAME = []
        temStrSql = "select * from t_proc_name"            
        temDb_self = Db_Common_Self()
        try:
            temRs = temDb_self.Common_Sql(temStrSql)
            for temItem in temRs:    
                temTpn = T_PROC_NAME()
                temTpn.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temTpn.attDB_ID = Com_Fun.ZeroNull(temItem[1])
                temTpn.attINF_CN_NAME = Com_Fun.NoNull(temItem[2])
                temTpn.attINF_EN_NAME = Com_Fun.NoNull(temItem[3])
                temTpn.attINF_EN_SQL = Com_Fun.NoNull(temItem[4])
                temTpn.attINF_TYPE = Com_Fun.ZeroNull(temItem[5])
                temTpn.attS_DESC = Com_Fun.NoNull(temItem[6])
                temTpn.attREFLECT_IN_CLASS = Com_Fun.NoNull(temItem[8])
                temTpn.attREFLECT_OUT_CLASS = Com_Fun.NoNull(temItem[9])
                if len(temItem) > 10:
                    temTpn.attIS_AUTHORITY = Com_Fun.ZeroNull(temItem[10])
                temListT_PROC_NAME.append(temTpn)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temListT_PROC_NAME 
    def Get_T_PROC_NAME3(self, inputINF_EN_NAME, inputSUB_ID):
        temTpn = T_PROC_NAME()
        temStrSql = ""
        if inputINF_EN_NAME != "":
            temStrSql = "select * from T_PROC_NAME where INF_EN_NAME = '" + inputINF_EN_NAME + "'"
        else:
            temStrSql = "select * from t_proc_name"            
        temDb_self = Db_Common_Self()
        try:
            temRs = temDb_self.Common_Sql(temStrSql)
            for temItem in temRs:    
                temTpn.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temTpn.attDB_ID = Com_Fun.ZeroNull(temItem[1])
                temTpn.attINF_CN_NAME = Com_Fun.NoNull(temItem[2])
                temTpn.attINF_EN_NAME = Com_Fun.NoNull(temItem[3])
                temTpn.attINF_EN_SQL = Com_Fun.NoNull(temItem[4])
                temTpn.attINF_TYPE = Com_Fun.ZeroNull(temItem[5])
                temTpn.attS_DESC = Com_Fun.NoNull(temItem[6])
                temTpn.attREFLECT_IN_CLASS = Com_Fun.NoNull(temItem[8])
                temTpn.attREFLECT_OUT_CLASS = Com_Fun.NoNull(temItem[9])
                if len(temItem) > 10:
                    temTpn.attIS_AUTHORITY = Com_Fun.ZeroNull(temItem[10])
                break
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temTpn
    def Get_T_PROC_NAME(self, inputINF_EN_NAME, inputSUB_ID):
        temTpn = T_PROC_NAME()
        temStrSql = ""
        if inputINF_EN_NAME != "" and  inputSUB_ID != -1:
            temStrSql = "select * from T_PROC_NAME where INF_EN_NAME = '" + inputINF_EN_NAME + "' and MAIN_ID in (select PROC_ID from t_sub_power where SUB_ID = '" + str(inputSUB_ID) + "')"
        elif inputINF_EN_NAME != "":
            temStrSql = "select * from T_PROC_NAME where INF_EN_NAME = '" + inputINF_EN_NAME + "'"
        else:
            temStrSql = "select * from t_proc_name"            
        temDb_self = Db_Common_Self()
        try:
            temRs = temDb_self.Common_Sql(temStrSql)
            for temItem in temRs:    
                temTpn.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temTpn.attDB_ID = Com_Fun.ZeroNull(temItem[1])
                temTpn.attINF_CN_NAME = Com_Fun.NoNull(temItem[2])
                temTpn.attINF_EN_NAME = Com_Fun.NoNull(temItem[3])
                temTpn.attINF_EN_SQL = Com_Fun.NoNull(temItem[4])
                temTpn.attINF_TYPE = Com_Fun.ZeroNull(temItem[5])
                temTpn.attS_DESC = Com_Fun.NoNull(temItem[6])
                temTpn.attREFLECT_IN_CLASS = Com_Fun.NoNull(temItem[8])
                temTpn.attREFLECT_OUT_CLASS = Com_Fun.NoNull(temItem[9])
                if len(temItem) > 10:
                    temTpn.attIS_AUTHORITY = Com_Fun.ZeroNull(temItem[10])
                break
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temTpn    
    def Get_eova_user_rid(self, inputLogin_id):
        temResult= -1
        temStrSql = "select rid from eova_user where id = '" + str(inputLogin_id)+"'"
        temDb_self = Db_Common_Self()
        try:
            temRs = temDb_self.Common_Sql(temStrSql)
            for temItem in temRs:
                temResult = Com_Fun.ZeroNull(temItem[0])
                break
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temResult
    def Get_T_PROC_INPARAM(self, inputPROC_ID):
        td = []
        temStrSql = "select * from t_proc_inparam where PROC_ID = " + str(inputPROC_ID)
        temDb_self = Db_Common_Self()
        try:
            temRs = temDb_self.Common_Sql(temStrSql)
            for temItem in temRs:
                tdc = T_PROC_INPARAM()
                tdc.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                tdc.attPROC_ID = Com_Fun.ZeroNull(temItem[1])
                tdc.attPARAM_CN_NAME = Com_Fun.NoNull(temItem[2])
                tdc.attPARAM_EN_NAME = Com_Fun.NoNull(temItem[3])
                tdc.attPARAM_TYPE = Com_Fun.NoNull(temItem[4])
                tdc.attPARAM_SIZE = Com_Fun.ZeroNull(temItem[5])
                tdc.attIS_URLENCODE = Com_Fun.ZeroNull(temItem[8])
                td.append(tdc)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return td
    def Get_T_PROC_OUTPARAM(self, inputPROC_ID):
        td = []
        temStrSql = "select * from t_proc_outparam where PROC_ID = " + str(inputPROC_ID)
        temDb_self = Db_Common_Self()
        try:
            temRs = temDb_self.Common_Sql(temStrSql)
            for temItem in temRs:
                tdc = T_PROC_OUTPARAM()
                tdc.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                tdc.attPROC_ID = Com_Fun.ZeroNull(temItem[1])
                tdc.attPARAM_CN_NAME = Com_Fun.NoNull(temItem[2])
                tdc.attPARAM_EN_NAME = Com_Fun.NoNull(temItem[3])
                tdc.attPARAM_TYPE = Com_Fun.NoNull(temItem[4])
                tdc.attPARAM_SIZE = Com_Fun.ZeroNull(temItem[5])
                tdc.attIS_IMG = Com_Fun.ZeroNull(temItem[8])
                tdc.attIS_URLENCODE = Com_Fun.ZeroNull(temItem[9])
                td.append(tdc)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return td
    def Get_T_PROC_RETURN(self, inputPROC_ID):
        td = []
        temStrSql = "select * from t_proc_return where PROC_ID = " + str(inputPROC_ID)
        temDb_self = Db_Common_Self()
        try:
            temRs = temDb_self.Common_Sql(temStrSql)
            for temItem in temRs:
                tdc = T_PROC_RETURN()
                tdc.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                tdc.attPROC_ID = Com_Fun.ZeroNull(temItem[1])
                tdc.attRETURN_NAME = Com_Fun.NoNull(temItem[2])
                tdc.attRETURN_TYPE = Com_Fun.NoNull(temItem[3])
                tdc.attS_DESC = Com_Fun.NoNull(temItem[4])
                tdc.attIS_IMG = Com_Fun.ZeroNull(temItem[6])
                tdc.attIS_URLENCODE = Com_Fun.ZeroNull(temItem[7])
                td.append(tdc)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return td
    def Get_T_SUB_POWER(self, inputSUB_ID, inputPROC_ID):
        temTsp = T_SUB_POWER()
        temStrSql = "select * from t_sub_power where SUB_ID ='" + str(inputSUB_ID) + "' and PROC_ID = '" + str(inputPROC_ID) + "'"
        temDb_self = Db_Common_Self()
        try:
            temRs = temDb_self.Common_Sql(temStrSql)
            for temItem in temRs:
                vTime = ""
                temTsp.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temTsp.attSUB_ID = Com_Fun.ZeroNull(temItem[1])
                temTsp.attPROC_ID = Com_Fun.ZeroNull(temItem[2])
                if len(str(temItem[3])) == 10:
                    vTime = " 00:00:00"
                temTsp.attLIMIT_DATE = Com_Fun.GetDateInput("%Y-%m-%d", "%Y-%m-%d %H:%M:%S", str(temItem[3])+vTime)
                vTime = ""
                temTsp.attUSE_LIMIT = Com_Fun.ZeroNull(temItem[4])
                temTsp.attLIMIT_NUMBER = Com_Fun.ZeroNull(temItem[5])
                temTsp.attLIMIT_TAG = Com_Fun.ZeroNull(temItem[6])
                if len(str(temItem[7])) == 10:
                    vTime = " 00:00:00"
                temTsp.attFIRST_DATE = Com_Fun.GetDateInput("%Y-%m-%d", "%Y-%m-%d %H:%M:%S", str(temItem[7])+vTime)
                vTime = ""
                temTsp.attS_DESC = Com_Fun.NoNull(temItem[8])
                if len(str(temItem[9])) == 10:
                    vTime = " 00:00:00"
                temTsp.attCREATE_DATE = Com_Fun.GetStrTimeInput("%Y-%m-%d %H:%M:%S", str(temItem[9])+vTime)
                vTime = ""
                break
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temTsp
    def Get_T_SUB_USER(self, inputSub_id, inputSub_usercode):
        temTsp = T_SUB_USER()
        temStrSql = "select * from t_sub_user where SUB_USERCODE ='" + str(inputSub_usercode) + "' and SUB_ID = '" + str(inputSub_id) + "'"
        temDb_self = Db_Common_Self()
        try:
            temRs = temDb_self.Common_Sql(temStrSql)
            for temItem in temRs:
                vTime = ""
                temTsp.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temTsp.attSUB_ID = Com_Fun.ZeroNull(temItem[1])
                temTsp.attSUB_USERNAME = Com_Fun.NoNull(temItem[2])
                temTsp.attSUB_USERCODE = Com_Fun.NoNull(temItem[3])
                if len(str(temItem[4])) == 10:
                    vTime = " 00:00:00"
                temTsp.attLIMIT_DATE = Com_Fun.GetDateInput("%Y-%m-%d", "%Y-%m-%d %H:%M:%S", str(temItem[4])+vTime)
                vTime = ""
                temTsp.attS_DESC = Com_Fun.NoNull(temItem[5])
                if len(str(temItem[6])) == 10:
                    vTime = " 00:00:00"
                temTsp.attCREATE_DATE = Com_Fun.GetStrTimeInput("%Y-%m-%d %H:%M:%S", str(temItem[6])+vTime)
                vTime = ""
                break
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temTsp
    def Get_T_SUB_USERPOWER(self, inputSub_user_id, inputProc_id):
        temTsp = T_SUB_USERPOWER()
        temStrSql = "select * from t_sub_userpower where SUB_USER_ID ='"+ str(inputSub_user_id) + "' and PROC_ID = '" + str(inputProc_id) + "'"
        temDb_self = Db_Common_Self()
        try:
            temRs = temDb_self.Common_Sql(temStrSql)
            for temItem in temRs:
                vTime = ""
                temTsp.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temTsp.attSUB_USER_ID = Com_Fun.ZeroNull(temItem[1])
                temTsp.attPROC_ID = Com_Fun.ZeroNull(temItem[2])
                if len(str(temItem[3])) == 10:
                    vTime = " 00:00:00"
                temTsp.attLIMIT_DATE = Com_Fun.GetDateInput("%Y-%m-%d", "%Y-%m-%d %H:%M:%S", str(temItem[3])+vTime)
                vTime = ""
                temTsp.attUSE_LIMIT = Com_Fun.ZeroNull(temItem[4])
                temTsp.attLIMIT_NUMBER = Com_Fun.ZeroNull(temItem[5])
                temTsp.attLIMIT_TAG = Com_Fun.ZeroNull(temItem[6])
                if len(str(temItem[7])) == 10:
                    vTime = " 00:00:00"
                temTsp.attFIRST_DATE = Com_Fun.GetDateInput("%Y-%m-%d", "%Y-%m-%d %H:%M:%S", str(temItem[7])+vTime)
                vTime = ""
                temTsp.attS_DESC = Com_Fun.NoNull(temItem[8])
                if len(str(temItem[9])) == 10:
                    vTime = " 00:00:00"
                temTsp.attCREATE_DATE = Com_Fun.GetStrTimeInput("%Y-%m-%d %H:%M:%S", str(temItem[9])+vTime)
                vTime = ""
                break
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temTsp
    def Init_Load(self):
        temStrSql = "select * from t_param_value"
        temDb_self = Db_Common_Self()
        temRs = temDb_self.Common_Sql(temStrSql)
        try:
            for temItem in temRs:
                if temItem[1] == "AccSocketPort":
                    Com_Para.AccSocketPort = Com_Fun.Str_To_Int(temItem[2])
                elif temItem[1] == "Inf_Name":
                    Com_Para.Inf_Name = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "ServerIP":
                    Com_Para.ServerIP = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "ServerPort":
                    Com_Para.ServerPort = Com_Fun.Str_To_Int(temItem[2])
                elif temItem[1] == "Child_Code":
                    Com_Para.Child_Code = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "Child_Name":
                    Com_Para.Child_Name = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "If_Child_Device":
                    Com_Para.If_Child_Device = Com_Fun.Str_To_Int(temItem[2])
                elif temItem[1] == "If_Parent_Device":
                    Com_Para.If_Parent_Device = Com_Fun.Str_To_Int(temItem[2])
                elif temItem[1] == "TimeCount":
                    Com_Para.TimeCount = Com_Fun.Str_To_Int(temItem[2])
                elif temItem[1] == "HTTP_Path":
                    Com_Para.HTTP_Path = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "Login_Session":
                    Com_Para.Login_Session = Com_Fun.Str_To_Int(temItem[2])
                elif temItem[1] == "Is_UserLogin":
                    Com_Para.Is_UserLogin = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "InterSession":
                    Com_Para.InterSession = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "APP_ID":
                    Com_Para.APP_ID = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "U_CODE":
                    Com_Para.U_CODE = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "SessionTimeOut":
                    Com_Para.SessionTimeOut = Com_Fun.Str_To_Int(temItem[2])
                elif temItem[1] == "ShowSqlError":
                    Com_Para.ShowSqlError = Com_Fun.Str_To_Int(temItem[2])
                elif temItem[1] == "strSessionAva":
                    Com_Para.strSessionAva = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "ServerREFLECT_IN_CLASS":
                    Com_Para.ServerREFLECT_IN_CLASS = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "ClientREFLECT_IN_CLASS":
                    Com_Para.ClientREFLECT_IN_CLASS = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "TimeREFLECT_IN_CLASS":
                    Com_Para.TimeREFLECT_IN_CLASS = Com_Fun.NoNull(temItem[2])     
                elif temItem[1] == "CustTimeTaskNum" :  
                    Com_Para.CustTimeTaskNum = Com_Fun.NoNull(temItem[2])                
                elif temItem[1] == "CustTimeREFLECT_IN_CLASS" :  
                    Com_Para.CustTimeREFLECT_IN_CLASS = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "ServerIPList":
                    Com_Para.ServerIPList = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "ComPortList":
                    Com_Para.ComPortList = Com_Fun.NoNull(temItem[2])                  
                elif temItem[1] == "urlPath" :  
                    Com_Para.urlPath = Com_Fun.NoNull(temItem[2]) 
                elif temItem[1] == "ServerWebPort" :  
                    Com_Para.ServerWebPort = Com_Fun.Str_To_Int(temItem[2])   
                elif temItem[1] == "MQTT_host" :  
                    Com_Para.MQTT_host = Com_Fun.NoNull(temItem[2])    
                elif temItem[1] == "MQTT_username" :  
                    Com_Para.MQTT_username = Com_Fun.NoNull(temItem[2])  
                elif temItem[1] == "MQTT_pwd" :  
                    Com_Para.MQTT_pwd = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "MQTT_topic_pull" :  
                    Com_Para.MQTT_topic_pull = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "MQTT_topic_get" :  
                    Com_Para.MQTT_topic_get = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "MQTTREFLECT_IN_CLASS" :  
                    Com_Para.MQTTREFLECT_IN_CLASS = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "iReturnUppLower":
                    Com_Para.iReturnUppLower = Com_Fun.Str_To_Int(temItem[2])
            if Com_Para.Child_Code == "":
                Com_Para.Child_Code = Com_Fun.Get_New_GUID().upper().replace("-", "")
                temStrSql = "update t_param_value set PARAM_VALUE = '" + Com_Para.Child_Code + "' where PARAM_KEY = 'Child_Code'"
                temDb_self.CommonExec_Sql(temStrSql)#temIResult = 
            if Com_Para.strSessionAva == "":
                Com_Para.strSessionAva = Com_Fun.setSconnections()
                temStrSql = "update t_param_value set PARAM_VALUE = '" + Com_Para.strSessionAva + "' where PARAM_KEY = 'strSessionAva'"
                temDb_self.CommonExec_Sql(temStrSql)#temIResult = 
            if Com_Para.Child_Code == "2E71E37154914FA8A60A8BD72577F9F7" or Com_Para.Child_Code == "":
                Com_Para.Child_Code = Com_Fun.Get_New_GUID().upper().replace("-", "")
                self.Change_Child_Code(Com_Para.Child_Code)
            Com_Para.htInterSession.clear()
            Com_Fun.GetInterSession("")
            self.SetInterSession()
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        finally:
            pass
    def Change_Child_Code(self, inputChild_Code):
        temStrSql = "select * from t_param_value where PARAM_KEY = 'Child_Code'"
        temDb_self = Db_Common_Self()
        temRs = temDb_self.Common_Sql(temStrSql)
        if len(temRs) > 0:
            temStrSql = "update t_param_value set PARAM_VALUE='" + inputChild_Code + "' where PARAM_KEY = 'Child_Code'"
        else:
            temStrSql = "insert into t_param_value(PARAM_KEY,PARAM_VALUE,PARAM_NAME,S_DESC,CREATE_DATE) " + " values('Child_Code','" + inputChild_Code + "','本设备唯一编码','',sysdate())"
        temDb_self.CommonExec_Sql(temStrSql)#temIResult = 
    def Ope_Data_Number(self,inputTsp, inputTsup, inputProc_id):
        bResult = True
        temDb_self = Db_Common_Self()
        temStrSql = ""
        try:
            strDtDate = Com_Fun.GetTime("%Y-%m-%d")
            if inputTsup.attMAIN_ID != -1:
                temStrForatS = "%Y-%m-%d" #%Y-%m-%d %H:%M:%S
                if inputTsup.attLIMIT_TAG == 1:
                    temStrForatS = "%Y"
                elif inputTsup.attLIMIT_TAG == 2:
                    temStrForatS = "%Y-%m"
                elif inputTsup.attLIMIT_TAG == 3:
                    temStrForatS = "%Y-%m-%d"
                if Com_Fun.GetLong(temStrForatS,Com_Fun.GetTime(temStrForatS)) == Com_Fun.GetLong(temStrForatS,inputTsup.attFIRST_DATE):
                    temStrSql = "update t_sub_userpower set USE_LIMIT = USE_LIMIT + 1 where MAIN_ID = '"+ str(inputTsup.attMAIN_ID) + "'"
                else:
                    temStrSql = "update t_sub_userpower set USE_LIMIT = 1,FIRST_DATE = '"+ Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")+ "' where MAIN_ID = '" + str(inputTsup.attMAIN_ID) + "'"
                temDb_self.CommonExec_Sql(temStrSql)
            elif inputTsup.attMAIN_ID == -1 and inputTsp.attMAIN_ID != -1:
                temStrForatS = "%Y-%m-%d" #%Y-%m-%d %H:%M:%S
                if inputTsup.attLIMIT_TAG == 1:
                    temStrForatS = "%Y"
                elif inputTsup.attLIMIT_TAG == 2:
                    temStrForatS = "%Y-%m"
                elif inputTsup.attLIMIT_TAG == 3:
                    temStrForatS = "%Y-%m-%d"                   
                if Com_Fun.GetLong(temStrForatS,Com_Fun.GetTime(temStrForatS)) == Com_Fun.GetLong(temStrForatS,inputTsp.attFIRST_DATE):
                    temStrSql = "update t_sub_power set USE_LIMIT = USE_LIMIT + 1 where MAIN_ID = '"+ str(inputTsp.attMAIN_ID) + "'"
                else:
                    temStrSql = "update t_sub_power set USE_LIMIT = 1,FIRST_DATE = '"+ Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")+ "' where MAIN_ID = '"+ str(inputTsp.attMAIN_ID)+ "'"
                temDb_self.CommonExec_Sql(temStrSql)
            temStrSql = "select * from t_proc_log where SUB_USER_ID = '"+ str(inputTsup.attSUB_USER_ID) + "'  and PROC_ID = '" + str(inputProc_id)+ "' and SYS_DATE = '" + strDtDate + "'"
            temRs = temDb_self.Common_Sql(temStrSql)
            if len(temRs) > 0:
                temStrSql = "update t_proc_log set S_COUNT = S_COUNT+1 where SUB_USER_ID = '"+ str(inputTsup.attSUB_USER_ID) + "'  and PROC_ID = '"+ str(inputProc_id)+ "' and SYS_DATE = '" + strDtDate + "'"
            else:
                temStrSql = "insert into t_proc_log(SUB_USER_ID,PROC_ID,S_COUNT,SYS_DATE,S_DESC,CREATE_DATE) "+ " values('"+ str(inputTsup.attSUB_USER_ID)+ "','"+ str(inputProc_id)+ "','1','" + strDtDate + "','','" + strDtDate + "')"
            temDb_self.CommonExec_Sql(temStrSql)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return bResult
    def Get_Log(self,inputParam_name,param_path,inputSession_id):
        if self.Get_eova_user_rid(Com_Fun.getLogin_Id(inputSession_id)) != 1:
            return "{\""+inputParam_name+"\": [{\""+Com_Fun.GetLowUpp("s_result")+"\": 0, \""+Com_Fun.GetLowUpp("error_desc")+"\": \"禁止操作，用户权限不足!\"}]}"
        temLogPath = Com_Para.ApplicationPath+'/log/'+param_path
        jso = {}
        jsary = []
        iIndex = 1              
        for parent,dirnames,filenames in os.walk(temLogPath,  followlinks=True):
            for dirname in dirnames:
                temjso = {}
                temjso[Com_Fun.GetLowUpp("MAIN_ID")] = str(iIndex)
                temjso[Com_Fun.GetLowUpp("file_type")] = "文件夹"
                temjso[Com_Fun.GetLowUpp("file_name")] = dirname
                iIndex = iIndex + 1
                jsary.append(temjso)
            for filename in filenames:                
                temjso = {}
                temjso[Com_Fun.GetLowUpp("MAIN_ID")] = str(iIndex)
                temjso[Com_Fun.GetLowUpp("file_type")] = "日志文件"
                temjso[Com_Fun.GetLowUpp("file_name")] = filename
                iIndex = iIndex + 1
                jsary.append(temjso)
        jso[inputParam_name] = jsary
        temResult = json.dumps(jso,ensure_ascii=False)
        return temResult
    def Cal_Data(self, inputPcl, inputSub_code, inputParam_name, inputAryParamValue, inputStrIP,inputSession_id,inputHtParam):
        temResult = ""
        if inputParam_name == "init_page":
            self.Init_Load()
            return "{\"" + inputParam_name + "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"1\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"读取缓存信息成功!\"}]}"
        elif inputParam_name == "init_log":
            if len(inputAryParamValue) > 0:
                return self.Get_Log(inputParam_name,inputAryParamValue[0],inputSession_id)
            else:
                return self.Get_Log(inputParam_name,"",inputSession_id)
        elif inputPcl.attError != "":
            self.attS_result = 0
            temResult = inputPcl.attError
            return temResult        
        temTpinpas = self.Get_T_PROC_INPARAM(inputPcl.attTpn.attMAIN_ID)
        temTpoutpas = self.Get_T_PROC_OUTPARAM(inputPcl.attTpn.attMAIN_ID)
        temTprtns = self.Get_T_PROC_RETURN(inputPcl.attTpn.attMAIN_ID)
        temParameters = []
        temParamTypes = []
        temParamOutName = []
        temParamOutType = []
        i = 0
        if len(inputAryParamValue) > 0:
            for tpi in temTpinpas:
                temStrTem = inputAryParamValue[i]
                if tpi.attIS_URLENCODE == 1 and temStrTem != "":
                    try:
                        temParameters.append(unquote(temStrTem,Com_Para.U_CODE))
                    except:
                        pass
                else:
                    temParameters.append(temStrTem)
                temParamTypes.append(tpi.attPARAM_TYPE)
                i = i + 1
        elif inputHtParam is not None:
            for tpi in temTpinpas:
                strTem = ""
                if Com_Fun.GetHashTableNone(inputHtParam,tpi.attPARAM_EN_NAME) is not None:
                    strTem = str(Com_Fun.GetHashTableNone(inputHtParam,tpi.attPARAM_EN_NAME))
                    if tpi.attIS_URLENCODE == 1 and strTem != "":
                        try:
                            temParameters.append(unquote(strTem,Com_Para.U_CODE))
                        except:
                            pass
                    else:
                        temParameters.append(strTem)
                    temParamTypes.append(tpi.attPARAM_TYPE)
        if inputPcl.attTpn.attREFLECT_IN_CLASS != "":
            try:
                objC = importlib.import_module(inputPcl.attTpn.attREFLECT_IN_CLASS)  #对模块进行导入                
                objName = inputPcl.attTpn.attREFLECT_IN_CLASS.split(".")
                objN = getattr(objC,objName[len(objName) - 1])
                i = 0
                for tpi in temTpinpas:
                    temStrTem = inputAryParamValue[i]
                    if tpi.attIS_URLENCODE == 1 and temStrTem != "":
                        try:
                            temStrTem = unquote(inputAryParamValue[i],Com_Para.U_CODE)
                        except:
                            pass
                    else:
                        temStrTem = inputAryParamValue[i]
                    if hasattr(objN,"param_value"+str(i+1)):
                        setattr(objN,"param_value"+str(i+1),temStrTem)
                    i = i + 1
                setattr(objN,"param_name",inputParam_name)
                setattr(objN,"session_id",inputSession_id)
                setattr(objN,"strContinue",1)
                setattr(objN,"strResult","")
                fun_us = getattr(objN,"init_start")    #利用反射调用对象中的属性func_name
                fun_us(objN)
                temContinue = getattr(objN,"strContinue")                
                temStrResult = getattr(objN,"strResult")
                if str(temContinue) == "0":
                    return temStrResult   
                else:
                    i = 0
                    for tpi in temTpinpas:
                        if hasattr(objN,"param_value"+str(i+1)):
                            try:
                                inputAryParamValue[i] = getattr(objN,"param_value"+str(i+1))
                                temParameters[i] = getattr(objN,"param_value"+str(i+1))
                                i = i + 1
                            except Exception as e:
                                pass
                        else:
                            break
            except Exception as e:
                if str(type(self)) == "<class 'type'>":
                    self.debug_in(self,inputPcl.attTpn.attREFLECT_IN_CLASS+"==>"+repr(e))#打印异常信息
                else:
                    self.debug_in(inputPcl.attTpn.attREFLECT_IN_CLASS+"==>"+repr(e))#打印异常信息
            finally:
                pass            
        i = 0
        for tpi in temTpoutpas:
            temParamOutName.append(tpi.attPARAM_CN_NAME)
            temParamOutType.append(tpi.attPARAM_TYPE)        
        temRs = None
        dbc1 = None
        temSqlException = ""
        temColumnNames = []
        temTdc = self.Get_T_DB_CONFIG_ByID(inputPcl.attTpn.attDB_ID)
        if temTdc.attMAIN_ID == 1:
            dbc1 = Db_Common1()
            temRs = dbc1.Common_Sql_Proc(inputPcl.attTpn.attINF_EN_NAME,temParameters,temParamTypes,temParamOutName,temParamOutType,inputPcl.attTpn)
            temSqlException = dbc1.attSqlException
            temColumnNames = dbc1.attColumnNames
        elif temTdc.attMAIN_ID == 2:
            dbc2 = Db_Common2()
            temRs = dbc2.Common_Sql_Proc(inputPcl.attTpn.attINF_EN_NAME,temParameters,temParamTypes,temParamOutName,temParamOutType,inputPcl.attTpn)
            temSqlException = dbc2.attSqlException
            temColumnNames = dbc2.attColumnNames        
        elif temTdc.attMAIN_ID == 3:
            dbc3 = Db_Common3()
            temRs = dbc3.Common_Sql_Proc(inputPcl.attTpn.attINF_EN_NAME,temParameters,temParamTypes,temParamOutName,temParamOutType,inputPcl.attTpn)
            temSqlException = dbc3.attSqlException
            temColumnNames = dbc3.attColumnNames
        try:
            jso = {}
            jsary = []
            iIndex = 0
            for temItem in temRs:
                temjso = {}
                jIndex = 0
                for columnname in temColumnNames:
                    if iIndex == 0 and  Com_Para.Is_UserLogin != ""  and Com_Para.Is_UserLogin.strip().find("[" +inputParam_name + "]") != -1:
                        if len(columnname) > 0 and columnname[0] == "s_result" and Com_Fun.NoNull(temItem[jIndex]) == "1":
                            temjso[Com_Fun.GetLowUpp("session_id")] = inputSession_id
                        elif len(columnname) > 0 and columnname[0] == "s_result" and Com_Fun.NoNull(temItem[jIndex]) != "1":
                            temjso[Com_Fun.GetLowUpp("session_id")] = ""
                    temLogName = Com_Fun.GetImgPath(inputParam_name)
                    for tpi in temTprtns:
                        if tpi.attRETURN_NAME.upper() == columnname[0].upper():
                            if tpi.attRETURN_TYPE.upper() == "STRING":
                                temjso[Com_Fun.GetLowUpp(tpi.attRETURN_NAME)] = Com_Fun.ReplacePath(temLogName,Com_Fun.NoNull(temItem[jIndex]),tpi.attIS_IMG,tpi.attIS_URLENCODE)
                                break
                            elif tpi.attRETURN_TYPE.upper() == "INT":
                                temjso[Com_Fun.GetLowUpp(tpi.attRETURN_NAME)] = Com_Fun.ZeroNull(temItem[jIndex])
                                break
                            elif tpi.attRETURN_TYPE.upper() == "FLOAT":
                                temjso[Com_Fun.GetLowUpp(tpi.attRETURN_NAME)] = float(temItem[jIndex])
                                break
                            elif tpi.attRETURN_TYPE.upper() == "DATE":
                                strTemDate = ""
                                try:
                                    vTime = ""
                                    if len(str(temItem[jIndex])) == 10:
                                        vTime = " 00:00:00"
                                    strTemDate = Com_Fun.GetStrTimeInput("%Y-%m-%d %H:%M:%S",temItem[jIndex]+vTime)
                                except:
                                    pass
                                temjso[Com_Fun.GetLowUpp(tpi.attRETURN_NAME)] = strTemDate
                    jIndex += 1
                jsary.append(temjso)
                iIndex += 1
            if  len(jsary) == 0 and temSqlException != "":
                temjso = {}
                temjso[Com_Fun.GetLowUpp("s_result")] = "0"
                if Com_Para.ShowSqlError == 1:
                    temjso[Com_Fun.GetLowUpp("error_desc")] = "失败," + temSqlException
                else:
                    temjso[Com_Fun.GetLowUpp("error_desc")] = "失败,数据库操作出错请查看程序错误日志文件"
                jsary.append(temjso)            
            jso[inputPcl.attTpn.attINF_EN_NAME] = jsary
            temResult = json.dumps(jso,ensure_ascii=False)
            if inputSession_id != "":
                if Com_Para.Is_UserLogin != "" and Com_Para.Is_UserLogin.strip().find("[" + inputParam_name + "]") != -1:
                    Com_Fun.SetSession(inputSession_id,temResult.replace(inputParam_name,"SESSION_INFO"))
                else:
                    Com_Fun.SetSession(inputSession_id,"")        
            self.Ope_Data_Number(inputPcl.attTsp,inputPcl.attTsup,inputPcl.attTpn.attMAIN_ID)
        except Exception as e:
            temResult = "{\""+ inputParam_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"执行该接口出错(Cal_Data)SQLException:"+ repr(e) + "\"}]}"
            self.attS_result = 0
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+ "sub_code="+ inputSub_code+ "&param_name="+ inputParam_name+ " " + repr(e)
                self.debug_in(self,repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+ "sub_code="+ inputSub_code+ "&param_name="+ inputParam_name+ " " + repr(e)
                self.debug_in(repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath,temLog)
            uL.WriteLog()
        finally:
            pass
        if inputPcl.attTpn.attREFLECT_OUT_CLASS != "":
            try:
                objC = importlib.import_module(inputPcl.attTpn.attREFLECT_OUT_CLASS)  #对模块进行导入                
                objName = inputPcl.attTpn.attREFLECT_OUT_CLASS.split(".")
                objN = getattr(objC,objName[len(objName) - 1])
                if hasattr(objN,"strResult"):
                    setattr(objN,"strResult",temResult)
                    setattr(objN,"param_name",inputParam_name)
                    setattr(objN,"session_id",inputSession_id)
                    fun_us = getattr(objN,"init_start")    #利用反射调用对象m中的属性func_name
                    fun_us(objN)
                    temStrResult = getattr(objN,"strResult")
                    temContinue = getattr(objN,"strContinue")
                    if str(temContinue) == "0":
                        return temStrResult                    
            except Exception as e:                
                if str(type(self)) == "<class 'type'>":
                    self.debug_in(self,inputPcl.attTpn.attREFLECT_IN_CLASS+"=3=>"+repr(e))#打印异常信息
                else:
                    self.debug_in(inputPcl.attTpn.attREFLECT_IN_CLASS+"=3=>"+repr(e))#打印异常信息
            finally:
                pass
        return temResult
    def Get_cal_variables(self):
        temStrSql = "select * from cal_variables"
        temDb_self = Db_Common_Self()
        try:
            temRs = temDb_self.Common_Sql(temStrSql)
            for temItem in temRs:
                temCV = cal_variables()
                vTime = ""                
                temCV.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temCV.attVAR_NAME = Com_Fun.NoNull(temItem[1])
                temCV.attVAR_KEY = Com_Fun.NoNull(temItem[2])
                temCV.attVAR_TYPE = Com_Fun.NoNull(temItem[3])
                temCV.attVAR_DESC = Com_Fun.NoNull(temItem[4])
                if len(str(temItem[5])) == 10:
                    vTime = " 00:00:00"
                temCV.attCREATE_DATE = Com_Fun.GetStrTimeInput("%Y-%m-%d %H:%M:%S", str(temItem[5])+vTime)
                temCV.attS_DESC = Com_Fun.NoNull(temItem[6])                
                Com_Fun.SetHashTable(Com_Para.ht_cal_variables,temCV.attMAIN_ID,temCV)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
    def Get_cal_fun(self):
        temStrSql = "select * from cal_fun"
        temDb_self = Db_Common_Self()
        try:
            temRs = temDb_self.Common_Sql(temStrSql)
            for temItem in temRs:
                temCF = cal_fun()
                temCF.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temCF.attFUN_NAME = Com_Fun.NoNull(temItem[1])
                temCF.attFUN_DESC = Com_Fun.NoNull(temItem[2])
                vTime = ""       
                if len(str(temItem[3])) == 10:
                    vTime = " 00:00:00"
                temCF.attCREATE_DATE = Com_Fun.GetStrTimeInput("%Y-%m-%d %H:%M:%S", str(temItem[3])+vTime)
                temCF.attS_DESC = Com_Fun.NoNull(temItem[4])
                Com_Fun.SetHashTable(Com_Para.ht_cal_fun,temCF.attMAIN_ID,temCF)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
    def Get_cal_run(self):
        temStrSql = "select * from cal_run"
        temDb_self = Db_Common_Self()
        try:
            temRs = temDb_self.Common_Sql(temStrSql)
            for temItem in temRs:
                temCR = cal_run()
                temCR.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temCR.attRUN_CODE = Com_Fun.NoNull(temItem[1])
                temCR.attFUN_ID = Com_Fun.ZeroNull(temItem[2])
                temCR.attRUN_DESC = Com_Fun.NoNull(temItem[3])
                vTime = ""       
                if len(str(temItem[4])) == 10:
                    vTime = " 00:00:00"
                temCR.attCREATE_DATE = Com_Fun.GetStrTimeInput("%Y-%m-%d %H:%M:%S", str(temItem[4])+vTime)
                temCR.attS_DESC = Com_Fun.NoNull(temItem[5])
                Com_Fun.SetHashTable(Com_Para.ht_cal_run,temCR.attRUN_CODE,temCR)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
    def Get_cal_run_param(self):
        Com_Para.list_cal_run_param.clear()
        temStrSql = "select * from cal_run_param"
        temDb_self = Db_Common_Self()
        try:
            temRs = temDb_self.Common_Sql(temStrSql)
            for temItem in temRs:
                temCRP = cal_run_param()
                temCRP.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temCRP.attRUN_ID = Com_Fun.ZeroNull(temItem[1])
                temCRP.attVARI_ID = Com_Fun.ZeroNull(temItem[2])
                vTime = ""       
                if len(str(temItem[3])) == 10:
                    vTime = " 00:00:00"
                temCRP.attCREATE_DATE = Com_Fun.GetStrTimeInput("%Y-%m-%d %H:%M:%S", str(temItem[3])+vTime)
                temCRP.attS_DESC = Com_Fun.NoNull(temItem[4])
                Com_Para.list_cal_run_param.append(temCRP)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
    def Get_cal_run_step(self):
        Com_Para.list_cal_run_step.clear()
        temStrSql = "select * from cal_run_step"
        temDb_self = Db_Common_Self()
        try:
            temRs = temDb_self.Common_Sql(temStrSql)
            for temItem in temRs:
                temCRS = cal_run_step()
                temCRS.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temCRS.attCAL_STEP = Com_Fun.NoNull(temItem[1])
                temCRS.attSETP_DESC = Com_Fun.NoNull(temItem[2])
                vTime = ""       
                if len(str(temItem[3])) == 10:
                    vTime = " 00:00:00"
                temCRS.attCREATE_DATE = Com_Fun.GetStrTimeInput("%Y-%m-%d %H:%M:%S", str(temItem[3])+vTime)
                temCRS.attS_DESC = Com_Fun.NoNull(temItem[4])
                Com_Para.list_cal_run_step.append(temCRS)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql+"==>"+repr(e)
                self.debug_in(self,temStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+temStrSql+"==>"+repr(e)
                self.debug_in(temStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()