//主从表tab选项卡标志位
var AE16A0_Tab_Flag = -1;
//选择某一行
var AE16A0_select_t_sub_sys_rowId = "";
//按钮事件新增或编辑
var AE16A0_type = "";
//其他页面传到本页面参数
var AE16A0_param = {};
//table选中数据，如无则默认第一条
var AE16A0_rowCheckData = null;

/*定义查询条件变量*/
/*declare query param begin*/

/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

/*绑定show监听事件*/
if(AE16A0_Tab_Flag == "-1"){
	var AE16A0_n = Get_RandomDiv("AE16A0","");
	$(AE16A0_n).bind("show", function(objTag){
		AE16A0_Adjust_Sub_Sequ();
		if(AE16A0_Tab_Flag > 0)
			AE16A0_adjust_tab();		
	});
	$(AE16A0_n).bind("hide", function(objTag){
		AE16A0_hide_tab_fun();		
	});
}

//设置主从表div顺序
function AE16A0_Adjust_Sub_Sequ(){
	var temSubDivs = $("#content-main").find("div[data-id]");
	var MainDiv = $(Get_RandomDiv("AE16A0",""));
	for(i = 0; i < temSubDivs.length; i++) {
		if(AE16A0_Is_Sub_Div(temSubDivs[i].id)){
			$(temSubDivs[i]).before($(MainDiv));
			break;
		}
	}
}

//主从表传递参数
function AE16A0_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function AE16A0_t_sub_sys_biz_start(inputparam) {
	layer.close(ly_index);
	AE16A0_param = inputparam;
	//主从表传递参数
	AE16A0_param_set();	
    /*biz begin*/
    AE16A0_init_t_sub_sys()	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function AE16A0_page_end(){
	AE16A0_adjust_tab();
}

//应用平台信息显示列定义
var AE16A0_t_sub_sys = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '应用平台名称',        field: 'SUB_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '应用平台授权码',        field: 'SUB_CODE',        sortable: true        ,formatter: set_s_decode    },    {        title: '使用有效期',        field: 'LIMIT_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: 'IP白名单',        field: 'IP_ADDR',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function AE16A0_init_t_sub_sys() {
	$(window).resize(function () {
		  $('#AE16A0_t_sub_sys_Events').bootstrapTable('resetView');
	});
	//应用平台信息查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AE16A0_btn_t_sub_sys_query').click();
}

//查询接口
function AE16A0_t_sub_sys_query() {
    $('#AE16A0_t_sub_sys_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_sub_sys",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AE16A0_get_N01_sel_t_sub_sys");
}

//查询结果
function AE16A0_get_N01_sel_t_sub_sys(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_sub_sys) == false)
        return false;
    AE16A0_rowCheckData = null;
    //调整table各列宽度
    $.each(AE16A0_t_sub_sys, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_t_sub_sys;
    if(s_data.length > 0)
    	AE16A0_rowCheckData = s_data[0];    
    AE16A0_select_t_sub_sys_rowId = "";
    AE16A0_Tab_Flag = 0;
    $('#AE16A0_t_sub_sys_Events').bootstrapTable('destroy');
    $("#AE16A0_t_sub_sys_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AE16A0_t_sub_sys_Events').find("tr.changeColor").removeClass('changeColor');
                AE16A0_select_t_sub_sys_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AE16A0_t_sub_sys_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AE16A0_select_t_sub_sys_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#AE16A0_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AE16A0_param.hasOwnProperty("target_name"))
			{
				$("#"+AE16A0_param["target_id"]).val(eval("row."+AE16A0_param["sourc_id"].toUpperCase()));
				$("#"+AE16A0_param["target_name"]).val(s_decode(eval("row."+AE16A0_param["sourc_name"].toUpperCase())));				
				layer.close(AE16A0_param["ly_index"]);
			}
		},
        toolbar: "#AE16A0_t_sub_sys_Toolbar",
        columns: AE16A0_t_sub_sys,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        onPostBody:function(){
        	if(s_data.length != 0)
            	AE16A0_page_end();
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AE16A0_btn_t_sub_sys_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/

	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/

	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    AE16A0_init_t_sub_sys()	
    /*biz end*/
})

//查询按钮
$('#AE16A0_btn_t_sub_sys_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
	
    /*set query param end*/
	AE16A0_t_sub_sys_query();
})

//vue回调
function AE16A0_t_sub_sys_call_vue(objResult){
	//选择某条记录或自动选择第一条
	if (AE16A0_select_t_sub_sys_rowId != "") 
		AE16A0_rowCheckData = $("#AE16A0_t_sub_sys_Events").bootstrapTable('getData')[AE16A0_select_t_sub_sys_rowId];
	
	if(index_subhtml == "t_sub_sys_$.vue")
	{
		var n = Get_RandomDiv("BE16A0",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AE16A0_type,"ly_index":index};
				if(AE16A0_param.hasOwnProperty("hidden_find")){
					inputdata["hidden_find"] = "1";
				}				
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("dev_vue/t_sub_sys_$.js","BE16A0_t_sub_sys_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/

	/*get tab subvue end*/
}

//新增按钮
$("#AE16A0_btn_t_sub_sys_add").click(function () {
	AE16A0_type = "add";
	index_subhtml = "t_sub_sys_$.vue";
	if(loadHtmlSubVueFun("dev_vue/t_sub_sys_$.vue","AE16A0_t_sub_sys_call_vue") == true){
		var n = Get_RandomDiv("BE16A0","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BE16A0_param["type"] = AE16A0_type;
				BE16A0_param["ly_index"]= index;
				if(AE16A0_param.hasOwnProperty("hidden_find")){
					BE16A0_param["hidden_find"] = "1";
				}
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
				
				/*Send Two FindSelect param end*/

				BE16A0_clear_edit_info();
			},
			end: function(){
				BE16A0_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AE16A0_btn_t_sub_sys_edit").click(function () {
	if (AE16A0_select_t_sub_sys_rowId != "") {
		AE16A0_type = "edit";
		index_subhtml = "t_sub_sys_$.vue";
		if(loadHtmlSubVueFun("dev_vue/t_sub_sys_$.vue","AE16A0_t_sub_sys_call_vue") == true){
			var n = Get_RandomDiv("BE16A0","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BE16A0_param["type"] = AE16A0_type;
					BE16A0_param["ly_index"] = index;
					if(AE16A0_param.hasOwnProperty("hidden_find")){
						BE16A0_param["hidden_find"] = "1";
					}
					BE16A0_clear_edit_info();
					BE16A0_get_edit_info();
				},
				end: function(){
					BE16A0_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AE16A0_btn_t_sub_sys_delete').click(function () {
	//单行选择
	var rowData = $("#AE16A0_t_sub_sys_Events").bootstrapTable('getData')[AE16A0_select_t_sub_sys_rowId];
	//多行选择
	var rowDatas = AE16A0_sel_row_t_sub_sys();
	if (AE16A0_select_t_sub_sys_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_sub_sys",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AE16A0_N01_del_t_sub_sys");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_sub_sys",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AE16A0_N01_del_t_sub_sys");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AE16A0_N01_del_t_sub_sys(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_sub_sys) == true)
		AE16A0_t_sub_sys_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function AE16A0_adjust_tab(){
	if(typeof($("#AE16A0_TAB_MAIN")[0]) != "undefined" && $("#AE16A0_TAB_MAIN")[0].length != 0){
		AE16A0_Tab_Flag = 1;
		$(Get_RDivNoBuild("AE16A0","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("AE16A0","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("AE16A0","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#AE16A0_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/
//隐藏tab页选项卡function AE16A0_hide_tab_fun(){    var n = null;}//判断是否sub子项div页面function AE16A0_Is_Sub_Div(temDivId){    if(temDivId.indexOf("XX$TTT") == 0)        return false;    return false;}
/*Tab Click Fun End*/

function AE16A0_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("dev_vue/"+inputUrl,"AE16A0_t_sub_sys_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (AE16A0_select_t_sub_sys_rowId != "") 
			rowData = $("#AE16A0_t_sub_sys_Events").bootstrapTable('getData')[AE16A0_select_t_proc_name_rowId];
		else
			rowData = AE16A0_rowCheckData;
		//$("#AE16A0_t_sub_sys_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			var temFlag = true;
			$.each(temPar, function (i, obj) {
				if(eval(inputrandom+"_param.hasOwnProperty(\""+obj.target_id+"\")") && eval(inputrandom+"_param[\""+obj.target_id+"\"]").toString() == rowData[obj.sourc_id].toString())
					temFlag = false;
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			if(temFlag){
				//传递子页面隐藏find功能
				eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
				//参数传递并赋值
				eval(inputrandom+"_param_set()");
				var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
				$("#"+inputrandom+"_btn_"+tbName+"_query").click();
			}
		}
		else{
			$.each(temPar, function (i, obj) {
				if(obj.sourc_id.toUpperCase() == "MAIN_ID")
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"-999\"");
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"\"");
			});
			//传递子页面隐藏find功能
			eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
			var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
			$("#"+inputrandom+"_btn_"+tbName+"_query").click();		
		}
	}
}

//清除 查找框
function AE16A0_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AE16A0_sel_row_t_sub_sys(){
	//获得选中行
	var checkedbox= $("#AE16A0_t_sub_sys_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}