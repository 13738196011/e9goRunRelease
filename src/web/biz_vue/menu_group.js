//选择某一行
var A7A26C_select_menu_group_rowId = "";
//按钮事件新增或编辑
var A7A26C_type = "";
//其他页面传到本页面参数
var A7A26C_param = {};

/*定义查询条件变量*/
/*declare query param begin*/

/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var A7A26C_ary_is_ava = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];var A7A26C_ary_role_id = [{'main_id': '1', 'cn_name': '开发者'}, {'main_id': '2', 'cn_name': '系统管理员'}, {'main_id': '3', 'cn_name': '运维人员'}, {'main_id': '4', 'cn_name': '普通用户'}];
/*declare select options end*/

//业务逻辑数据开始
function A7A26C_menu_group_biz_start(inputparam) {
	layer.close(ly_index);
	A7A26C_param = inputparam;
    /*biz begin*/
    if($("#A7A26C_qry_is_ava").is("select") && $("#A7A26C_qry_is_ava")[0].options.length == 0)    {        $("#A7A26C_qry_is_ava").append("<option value='-1'></option>")        $.each(A7A26C_ary_is_ava, function (i, obj) {            addOptionValue("A7A26C_qry_is_ava", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#A7A26C_qry_role_id").is("select") && $("#A7A26C_qry_role_id")[0].options.length == 0)    {        $("#A7A26C_qry_role_id").append("<option value='-1'></option>")        $.each(A7A26C_ary_role_id, function (i, obj) {            addOptionValue("A7A26C_qry_role_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    A7A26C_init_menu_group()	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function A7A26C_format_is_ava(value, row, index) {    var objResult = value;    for(i = 0; i < A7A26C_ary_is_ava.length; i++) {        var obj = A7A26C_ary_is_ava[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A7A26C_format_role_id(value, row, index) {    var objResult = value;    for(i = 0; i < A7A26C_ary_role_id.length; i++) {        var obj = A7A26C_ary_role_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function A7A26C_page_end(){
}

//菜单组显示列定义
var A7A26C_menu_group = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '菜单组英文名',        field: 'GROUP_EN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '菜单组中文名',        field: 'GROUP_CN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '菜单组图标',        field: 'GROUP_ICON',        sortable: true        ,formatter: set_s_decode    },    {        title: '是否启用',        field: 'IS_AVA',        sortable: true        ,formatter: A7A26C_format_is_ava    },    {        title: '角色',        field: 'ROLE_ID',        sortable: true        ,formatter: A7A26C_format_role_id    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function A7A26C_init_menu_group() {
	$(window).resize(function () {
		  $('#A7A26C_menu_group_Events').bootstrapTable('resetView');
	});
	//菜单组查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#A7A26C_btn_menu_group_query').click();
}

//查询接口
function A7A26C_menu_group_query() {
    $('#A7A26C_menu_group_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_menu_group",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "A7A26C_get_N01_sel_menu_group");
}

//查询结果
function A7A26C_get_N01_sel_menu_group(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_menu_group) == false)
        return false;    
	var s_data = input.N01_sel_menu_group;
    A7A26C_select_menu_group_rowId = "";
    $('#A7A26C_menu_group_Events').bootstrapTable('destroy');
    $("#A7A26C_menu_group_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#A7A26C_menu_group_Events').find("tr.changeColor").removeClass('changeColor');
                A7A26C_select_menu_group_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#A7A26C_menu_group_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                A7A26C_select_menu_group_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(A7A26C_param.hasOwnProperty("target_name"))
			{
				$("#"+A7A26C_param["target_id"]).val(eval("row."+A7A26C_param["sourc_id"].toUpperCase()));
				$("#"+A7A26C_param["target_name"]).val(s_decode(eval("row."+A7A26C_param["sourc_name"].toUpperCase())));				
				layer.close(A7A26C_param["ly_index"]);
			}
		},
        toolbar: "#A7A26C_menu_group_Toolbar",
        columns: A7A26C_menu_group,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#A7A26C_btn_menu_group_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/

	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/

	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    if($("#A7A26C_qry_is_ava").is("select") && $("#A7A26C_qry_is_ava")[0].options.length == 0)    {        $("#A7A26C_qry_is_ava").append("<option value='-1'></option>")        $.each(A7A26C_ary_is_ava, function (i, obj) {            addOptionValue("A7A26C_qry_is_ava", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#A7A26C_qry_role_id").is("select") && $("#A7A26C_qry_role_id")[0].options.length == 0)    {        $("#A7A26C_qry_role_id").append("<option value='-1'></option>")        $.each(A7A26C_ary_role_id, function (i, obj) {            addOptionValue("A7A26C_qry_role_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    A7A26C_init_menu_group()	
    /*biz end*/
})

//查询按钮
$('#A7A26C_btn_menu_group_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
	
    /*set query param end*/
	A7A26C_menu_group_query();
})

//vue回调
function A7A26C_menu_group_call_vue(objResult){
	if(index_subhtml == "menu_group_$.vue")
	{
		var n = Get_RandomDiv("B7A26C",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":A7A26C_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/menu_group_$.js","B7A26C_menu_group_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//新增按钮
$("#A7A26C_btn_menu_group_add").click(function () {
	A7A26C_type = "add";
	index_subhtml = "menu_group_$.vue";
	if(loadHtmlSubVueFun("biz_vue/menu_group_$.vue","A7A26C_menu_group_call_vue") == true){
		var n = Get_RandomDiv("B7A26C","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				B7A26C_param["type"] = A7A26C_type;
				B7A26C_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
				
				/*Send Two FindSelect param end*/

				B7A26C_clear_edit_info();
			},
			end: function(){
				B7A26C_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#A7A26C_btn_menu_group_edit").click(function () {
	if (A7A26C_select_menu_group_rowId != "") {
		A7A26C_type = "edit";
		index_subhtml = "menu_group_$.vue";
		if(loadHtmlSubVueFun("biz_vue/menu_group_$.vue","A7A26C_menu_group_call_vue") == true){
			var n = Get_RandomDiv("B7A26C","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					B7A26C_param["type"] = A7A26C_type;
					B7A26C_param["ly_index"] = index;
					B7A26C_clear_edit_info();
					B7A26C_get_edit_info();
				},
				end: function(){
					B7A26C_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#A7A26C_btn_menu_group_delete').click(function () {
	//单行选择
	var rowData = $("#A7A26C_menu_group_Events").bootstrapTable('getData')[A7A26C_select_menu_group_rowId];
	//多行选择
	var rowDatas = A7A26C_sel_row_menu_group();
	if (A7A26C_select_menu_group_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_menu_group",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "A7A26C_N01_del_menu_group");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_menu_group",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "A7A26C_N01_del_menu_group");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function A7A26C_N01_del_menu_group(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_menu_group) == true)
		A7A26C_menu_group_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function A7A26C_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function A7A26C_sel_row_menu_group(){
	//获得选中行
	var checkedbox= $("#A7A26C_menu_group_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}