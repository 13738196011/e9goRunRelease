#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import json
from com.plugins.A01_IHGDW0.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_IHGDW0 import A01_Para
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun 
from com.zxy.z_debug import z_debug
class Qry_Monitor_Data_Min(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = "0"
    strCmdValue      = ""
    def __init__(self):
        pass
    def init_start(self):        
        try:
            bodc = Bus_Ope_DB_Cent()
            temRs = bodc.Qry_data(self.param_name,self.param_value1,self.param_value2,self.param_value3,self.param_value4,self.param_value5)
            jso = {}
            jsary = []
            iIndex = 0
            for temItem in temRs:
                temjso = {}
                jIndex = 1
                temjso[Com_Fun.GetLowUpp("get_time")] = str(temItem[0])
                for temElement in A01_Para.ht_t_code_element:
                    temjso[Com_Fun.GetLowUpp(temElement.attelement_val)] = str(temItem[jIndex])                                
                    jIndex += 1
                jsary.append(temjso)
                iIndex += 1    
            jso[self.param_name] = jsary
            self.strContinue = "0"
            self.strResult = json.dumps(jso,ensure_ascii=False)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+repr(e)
            else:
                temLog = self.debug_info()+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        finally:
            pass