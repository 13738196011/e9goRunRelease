//按钮事件新增或编辑
var B77565_type = "";
//其他页面传到本页面参数
var B77565_param = {};
//暂时没用
var B77565_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var B77565_ary_GROUP_ID = null;var B77565_ary_IS_AVA = [{'MAIN_ID': '1', 'CN_NAME': '启用'}, {'MAIN_ID': '0', 'CN_NAME': '禁用'}];var B77565_ary_ROLE_ID = [{'MAIN_ID': '1', 'CN_NAME': '开发者'}, {'MAIN_ID': '2', 'CN_NAME': '系统管理员'}, {'MAIN_ID': '3', 'CN_NAME': '运维人员'}, {'MAIN_ID': '4', 'CN_NAME': '普通用户'}];
/*declare select options end*/

//主从表传递参数
function B77565_param_set(){
	/*Main Subsuv Table Param Begin*/
    if(B77565_param.hasOwnProperty("GROUP_ID_cn_name"))        $("#B77565_find_GROUP_ID_cn_name").val(s_decode(B77565_param["GROUP_ID_cn_name"]));    if(B77565_param.hasOwnProperty("GROUP_ID"))        $("#B77565_find_GROUP_ID").val(B77565_param["GROUP_ID"]);    if(B77565_param.hasOwnProperty("hidden_find")){        $("#B77565_Ope_GROUP_ID").hide();        $("#B77565_Clear_GROUP_ID").hide();    }	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function B77565_menu_info_biz_start(inputdata) {
	B77565_param = inputdata;
	layer.close(ly_index);
	B77565_param_set();
    /*biz begin*/
    var inputdata = {        "param_name": "N01_menu_info$GROUP_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B77565_get_N01_menu_info$GROUP_ID");	
    /*biz end*/
}

/*biz step begin*/
function B77565_format_GROUP_ID(value, row, index) {    var objResult = value;    for(i = 0; i < B77565_ary_GROUP_ID.length; i++) {        var obj = B77565_ary_GROUP_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B77565_format_IS_AVA(value, row, index) {    var objResult = value;    for(i = 0; i < B77565_ary_IS_AVA.length; i++) {        var obj = B77565_ary_IS_AVA[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B77565_format_ROLE_ID(value, row, index) {    var objResult = value;    for(i = 0; i < B77565_ary_ROLE_ID.length; i++) {        var obj = B77565_ary_ROLE_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B77565_get_N01_menu_info$GROUP_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_menu_info$GROUP_ID) == false)        return false;    B77565_ary_GROUP_ID = input.N01_menu_info$GROUP_ID;    if($("#B77565_GROUP_ID").is("select") && $("#B77565_GROUP_ID")[0].options.length == 0)    {        $.each(B77565_ary_GROUP_ID, function (i, obj) {            addOptionValue("B77565_GROUP_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#B77565_IS_AVA").is("select") && $("#B77565_IS_AVA")[0].options.length == 0)    {        $.each(B77565_ary_IS_AVA, function (i, obj) {            addOptionValue("B77565_IS_AVA", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#B77565_ROLE_ID").is("select") && $("#B77565_ROLE_ID")[0].options.length == 0)    {        $.each(B77565_ary_ROLE_ID, function (i, obj) {            addOptionValue("B77565_ROLE_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    B77565_init_menu_info();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function B77565_GROUP_ID_cn_name_fun(){    index_subhtml = "menu_group.vue"    random_subhtml = "";    if(loadHtmlSubVueFun("dev_vue/menu_group.vue","B77565_menu_info_call_vue") == true){        var n = Get_RandomDiv("","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#_menu_group_Events').bootstrapTable('resetView');                _param["ly_index"] = index;                _param["target_name"] = "B77565_find_GROUP_ID_cn_name"                _param["target_id"] = "B77565_GROUP_ID"                _param["sourc_id"] = "MAIN_ID"                _param["sourc_name"] = "GROUP_CN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function B77565_page_end(){
	$('#B77565_ROLE_ID').selectpicker('refresh'); 
	$('#B77565_ROLE_ID').selectpicker('show');
	if(B77565_param["type"] == "edit"){
		B77565_get_edit_info();
	}
}

//页面初始化方法
function B77565_init_menu_info() {
	//type = getUrlParam("type");
	if(B77565_param["type"] == "add"){
		$("#B77565_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#B77565_MENU_NAME").val(B77565_param["MENU_NAME"]);        $("#B77565_IS_AVA").val(B77565_param["IS_AVA"]);        $("#B77565_ROLE_ID").val(B77565_param["ROLE_ID"]);        $("#B77565_GROUP_ID").val(B77565_param["GROUP_ID"]);        $("#B77565_find_GROUP_ID_cn_name").val(B77565_param["GROUP_ID_cn_name"]);		
		/*Get Find Select param end*/	
	}
	else if(B77565_param["type"] == "edit"){
	
	}
	
	//表单验证
	B77565_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B77565_CREATE_DATE").val() == "")    {        $("#B77565_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B77565_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B77565_page_end();
}

//提交表单数据
function B77565_SubmitForm(){
	if(B77565_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_menu_info",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#B77565_GROUP_ID").val()                ,"param_value2": s_encode($("#B77565_MENU_URL").val())                ,"param_value3": s_encode($("#B77565_MENU_NAME").val())                ,"param_value4": $("#B77565_IS_AVA").val()                ,"param_value5": s_encode($("#B77565_ROLE_ID").val())                ,"param_value6": $("#B77565_CREATE_DATE").val()                ,"param_value7": s_encode($("#B77565_S_DESC").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B77565_get_N01_ins_menu_info");
	}
	else if(B77565_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_menu_info",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#B77565_GROUP_ID").val()                ,"param_value2": s_encode($("#B77565_MENU_URL").val())                ,"param_value3": s_encode($("#B77565_MENU_NAME").val())                ,"param_value4": $("#B77565_IS_AVA").val()                ,"param_value5": s_encode($("#B77565_ROLE_ID").val())                ,"param_value6": $("#B77565_CREATE_DATE").val()                ,"param_value7": s_encode($("#B77565_S_DESC").val())                ,"param_value8": $("#B77565_MAIN_ID").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B77565_get_N01_upd_menu_info");
	}
}

//vue回调
function B77565_menu_info_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "menu_group.vue"){        var n = Get_RandomDiv("",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":B77565_type,                    "ly_index":index,                    "target_name":"B77565_find_GROUP_ID_cn_name",                    "target_id":"B77565_find_GROUP_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"GROUP_CN_NAME"                };                loadScript_hasparam("dev_vue/menu_group.js","_menu_group_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//for表单提交
$("#B77565_save_menu_info_Edit").click(function () {
	$("form[name='B77565_DataModal']").submit();
})

/*修改数据*/
function B77565_get_N01_upd_menu_info(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_menu_info) == true)
	{
		swal("修改数据成功!", "", "success");
		A77565_menu_info_query();
		B77565_clear_validate();
		layer.close(B77565_param["ly_index"]);
	}
}

/*添加数据*/
function B77565_get_N01_ins_menu_info(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_menu_info) == true)
	{
		swal("添加数据成功!", "", "success");
		A77565_menu_info_query();
		B77565_clear_validate();
		layer.close(B77565_param["ly_index"]);
	}
}

//取消编辑
$("#B77565_cancel_menu_info_Edit").click(function () {
	layer.close(B77565_param["ly_index"]);
	B77565_clear_validate();
	$("[id^='B77565_div']").hide();
})

//清除查找框
function B77565_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B77565_clear_validate(){
	$("#B77565_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B77565_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B77565_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B77565_clear_edit_info(){
	var inputs = $("#B77565_DataModal").find('input');
	var selects = $("#B77565_DataModal").find("select");
	var textareas = $("#B77565_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
    B77565_clear_input_cn_name('B77565_find_GROUP_ID_cn_name','B77565_GROUP_ID')	
	/*input validate clear end*/
	B77565_init_menu_info();
}

//页面输入框赋值
function B77565_get_edit_info(){
	//$('#B77565_ROLE_ID').selectpicker('refresh'); 
	//$('#B77565_ROLE_ID').selectpicker('show');
	var rowData = $("#A77565_menu_info_Events").bootstrapTable('getData')[A77565_select_menu_info_rowId];
	var inputs = $("#B77565_DataModal").find('input');
	var selects = $("#B77565_DataModal").find("select");
	var textareas = $("#B77565_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"A77565","B77565");
	
	//多选框赋值
	$.each(selects, function (i, obj) {
		if (obj.id.toUpperCase() == "B77565_ROLE_ID") {
			var arr = s_decode(rowData["ROLE_ID"]).split(',');
			$(obj).selectpicker('val', arr);
		}
	});
}

//form验证
function B77565_checkFormInput() {
    B77565_validate = $("#B77565_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B77565_main_id: {}            ,B77565_GROUP_ID: {}            ,B77565_MENU_URL: {}            ,B77565_MENU_NAME: {}            ,B77565_IS_AVA: {}            ,B77565_ROLE_ID: {}            ,B77565_CREATE_DATE: {date: true,required : true,maxlength:19}            ,B77565_S_DESC: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B77565_main_id: {}            ,B77565_GROUP_ID: {}            ,B77565_MENU_URL: {}            ,B77565_MENU_NAME: {}            ,B77565_IS_AVA: {}            ,B77565_ROLE_ID: {}            ,B77565_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B77565_S_DESC: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B77565_SubmitForm();
        	return false;
        }
    })
}