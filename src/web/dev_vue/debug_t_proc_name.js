//业务逻辑数据开始
function AH5T84_debug_biz_start(inputparam) {
	layer.close(ly_index);
	AH5T84_Clear_Inpurt();
    /*biz begin*/
	if (A43B7C_select_t_proc_name_rowId != ""){
		var A43B7C_rowCheckData = $("#A43B7C_t_proc_name_Events").bootstrapTable('getData')[A43B7C_select_t_proc_name_rowId];
		$("#AH5T84_param_name").val(A43B7C_rowCheckData.INF_EN_NAME);
	}
	if (AA1D24_select_t_sub_power_rowId != ""){
		var AA1D24_rowCheckData = $("#AA1D24_t_sub_power_Events").bootstrapTable('getData')[AA1D24_select_t_sub_power_rowId];
		$("#AH5T84_sub_code").val(AH5T84_get_format_SUB_ID(AA1D24_rowCheckData.SUB_ID));
	}
	if (AD0AFB_select_t_sub_userpower_rowId != ""){
		var AD0AFB_rowCheckData = $("#AD0AFB_t_sub_userpower_Events").bootstrapTable('getData')[AD0AFB_select_t_sub_userpower_rowId];
		$("#AH5T84_sub_usercode").val(AH5T84_get_format_SUB_USER_ID(AD0AFB_rowCheckData.SUB_USER_ID));
	}
	var A9FE70_rowCheckData = $("#A9FE70_t_proc_inparam_Events").bootstrapTable('getData');
    for (var i = 1; i <= A9FE70_rowCheckData.length; i++) {
        var input_prarm_value = "<div class=\"form-group\">"
        	 + "<label class='col-sm-3 control-label'>输入参数" + i.toString() + "(param_value" + i.toString() + "=)</label>"
             + "<div class='col-sm-8'>"
             + "<input id='AH5T84_param_value" + i.toString() + "' placeholder='请输入" + s_decode(A9FE70_rowCheckData[i - 1].PARAM_CN_NAME) + "' name='AH5T84_param_value" + i.toString() + "' type='text' class='form-control' >"
             + "</div></div>";
        $("#AH5T84_param_values").append(input_prarm_value);
    }
}

function AH5T84_Clear_Inpurt(){
	$("#AH5T84_param_values").html("");
	$("#AH5T84_sub_usercode").val("");
	$("#AH5T84_sub_code").val("");
	$("#AH5T84_t_proc_name_url").val("");
	$("#AH5T84_debug_result").val("");
	$("#AH5T84_view_source").val("");
}

function AH5T84_get_format_SUB_ID(value) {
    var objResult = value;
    for(i = 0; i < AA1D24_ary_SUB_ID.length; i++) {
        var obj = AA1D24_ary_SUB_ID[i];
        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {
            objResult = obj[GetLowUpp("cn_name")];
            break;
        }
    }
    return objResult.substring(6,objResult.length);
}

function AH5T84_get_format_SUB_USER_ID(value) {
    var objResult = value;
    for(i = 0; i < AD0AFB_ary_SUB_USER_ID.length; i++) {
        var obj = AD0AFB_ary_SUB_USER_ID[i];
        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {
            objResult = obj[GetLowUpp("cn_name")];
            break;
        }
    }
    return objResult.substring(6,objResult.length);
}

$("#AH5T84_btnDebug").click(function () {
    $("#AH5T84_debug_result").val("");
    ly_index = layer.load();
    var show_url = "http://" + getIP_Url() + "?sub_code=" + $("#AH5T84_sub_code").val() + "&sub_usercode=" + $("#AH5T84_sub_usercode").val()+"&session_id="+session_id;
    var rooturl = show_url + "&param_name=" + $("#AH5T84_param_name").val();
    var source_code = "<script type=\"text/javascript\">\r\n"
         + "	$.ajax({\r\n"
         + "		type: \"POST\",\r\n"
         + "		async: false,\r\n"
         + "		url: \"" + show_url + "\",\r\n"
         + "		data:{param_name:\"" + $("#AH5T84_param_name").val() + "\"\r\n";    
    var inputs = $("#AH5T84_param_values").find('input');
    for (var i = 1; i <= inputs.length; i++) {
        rooturl += "&param_value" + i.toString() + "=" + $(inputs[i-1]).val();
        source_code += "			,param_value" + i.toString() + ":\"" + $(inputs[i-1]).val() + "\"\r\n"
    }
    source_code += "},\r\n"
     + "		//跨域请求的URL\r\n"
     + "		dataType: \"jsonp\",\r\n"
     + "		jsonp: \"jsoncallback\",\r\n"
     + "		jsonpCallback: \"data_result\",\r\n"
     + "		success: function(data) {\r\n"
     + "		},\r\n"
     + "		error: function() {\r\n"
     + "		if(confirm(\"网络故障，请刷新网络\"))\r\n"
     + "			window.location.reload();\r\n"
     + "		},\r\n"
     + "		// 请求完成后的回调函数 (请求成功或失败之后均调用)\r\n"
     + "		complete: function(XMLHttpRequest, textStatus) {\r\n"
     + "		}\r\n"
     + "	});\r\n"
     + "\r\n"
     + "	function data_result(input_data)\r\n"
     + "	{\r\n"
     + "		//alert(input_data);\r\n"
     + "		alert(JSON.stringify(input_data));\r\n"
     + "	}\r\n"
     + "</script>";
    $("#AH5T84_t_proc_name_url").val(rooturl);
    $("#AH5T84_view_source").val(source_code);
    inputData = {};
    $.ajax({
        type: "POST",
        async: false,
        url: rooturl,
        data: inputData,
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        //jsonpCallback: inputjsonpCallBack,
        success: function (data) {
        	AF958F_get_btnDebug(data);
        },
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                layer.close(ly_index);
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
    
//    var inputdata = {
//    		"param_name": $("#AH5T84_param_name").val(),
//    		"session_id": session_id,
//    		"login_id": login_id
//    		/*传递查询条件变量*/
//            /*get query param begin*/
//            /*get query param end*/
//    };
//    for (var i = 1; i <= inputs.length; i++) {
//    	inputdata["param_value" + i.toString()] = $(inputs[i-1]).val();
//    }
//    if($("#AH5T84_db_config").val() == "1")
//    	get_ajax_staticurl(inputdata, "AF958F_get_btnDebug");
//    else
//    	get_ajax_baseurl(inputdata, "AF958F_get_btnDebug");
});

//调试结果
function AF958F_get_btnDebug(input) {
	layer.close(ly_index);
	//查询失败
	if (Call_QryResult(input[$("#AH5T84_param_name").val()]) == false)
		return false;
	$("#AH5T84_debug_result").val(JSON.stringify(input));
}