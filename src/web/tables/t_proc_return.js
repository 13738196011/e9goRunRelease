﻿var iTop = 0;
var ileft = 0;
var ly_index;

//接口名称
var mod_t_proc_return;

function biz_start(){
}

function T01_sel_t_proc_name(input) {
    var options = {};
    data = input.T01_sel_t_proc_name;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
    if (data[0].MAIN_ID < 72){
        data.splice(0, 1);
	}
    $.each(data, function (i, obj) {
        options[obj.MAIN_ID.toString()] = obj.DB_ID + "--" + obj.INF_CN_NAME;
    });

    mod_t_proc_return = [{
            label: '主键',
            name: 'MAIN_ID',
            width: '30px',
            index: 'MAIN_ID',
            editable: false,
            key: true,
            readOnly: true,
            editrules: {
                required: true
            }
        }, {
            label: '接口名称',
            name: 'PROC_ID',
            width: '70px',
            editable: true,
            readOnly: true,
            editrules: true,
            edittype: 'select',
            formatter: 'select',
            editoptions: {
                value: options
            }
        }, {
            label: '接口返回值名称',
            name: 'RETURN_NAME',
            width: '50px',
            editable: true,
            editrules: true
        }, {
            label: '接口返回值类型',
            name: 'RETURN_TYPE',
            width: '50px',
            edittype: 'select',
            formatter: 'select',
            editable: true,
            editrules: true,
            editoptions: {
                value: {
                    INT: 'INT',
                    STRING: 'STRING',
                    FLOAT: 'FLOAT',
                    DATE: 'DATE'
                }
            }
        }, {
            label: '备注',
            name: 'S_DESC',
            width: '60px',
            editable: true,
            editrules: true
        }, {
            label: '是否图片或路径',
            name: 'IS_IMG',
            width: '40px',
            edittype: 'select',
            formatter: 'select',
            editable: true,
            editrules: true,
            editoptions: {
                value: {
                    0: '否',
                    1: '是'
                }
            }
        }, {
            label: '是否URL编码',
            name: 'IS_URLENCODE',
            width: '40px',
            edittype: 'select',
            formatter: 'select',
            editable: true,
            editrules: true,
            editoptions: {
                value: {
                    0: '否',
                    1: '是'
                }
            }
        }, {
            label: '是否隐藏List列',
            name: 'LIST_HIDDEN',
            width: '40px',
            edittype: 'select',
            formatter: 'select',
            editable: true,
            editrules: true,
            editoptions: {
                value: {
                    0: '否',
                    1: '是'
                }
            }
        }, {
            label: '系统时间',
            name: 'CREATE_DATE',
            width: '50px',
            editable: true,
            editrules: true,
            readOnly: true,
            formatter: function (value, row) {
                return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
            },
            editoptions: {
                defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')
            }
        }
    ];
    layer.close(ly_index);
    init();
}

//获取接口名称
function get_T01_sel_t_proc_name() {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_t_proc_name",
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_proc_name",
        success: function (data2) {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

function T01_sel_t_proc_return(input) {
    data = input.T01_sel_t_proc_return;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
    $("#table_list_1").jqGrid({
        data: data,
        datatype: "local",
        height: "450",
        autowidth: true,
        shrinkToFit: true,
        rowNum: 10,
        rowList: [10, 20, 30],
        colModel: mod_t_proc_return,
        editurl: 'clientArray',
        altRows: true,
        pager: "#pager_list_1",
        viewrecords: true,
        caption: "接口返回值列表"
    });
    //导航后面，增加增删改查按钮
    $("#table_list_1").jqGrid("navGrid", "#pager_list_1", {
        edit: true,
        add: true,
        del: true,
        search: true,
        view: false,
        position: "left",
        cloneToTop: false
    }, {
        beforeShowForm: function () {
            $('#PROC_ID').attr('readOnly', true);
            $("#PROC_ID").attr('disabled', "disabled");
            laydate.render({
                elem: '#CREATE_DATE',
                type: 'datetime'
            });
        },
        onclickSubmit: updateDbByMainId,
        closeAfterEdit: true
    }, {
        beforeShowForm: function () {
            laydate.render({
                elem: '#CREATE_DATE',
                type: 'datetime'
            });
        },
        onclickSubmit: insertDb,
        closeAfterAdd: true
    }, {
        top: iTop,
        left: ileft,
        onclickSubmit: delDbByMainId
    }, {
        closeAfterSearch: true
    }, {
        height: 200,
        reloadAfterSubmit: true
    });
    layer.close(ly_index);
}

//初始化
function init() {
    iTop = (winHeight - 300) / 2;
    ileft = (winWidth - 300) / 2;
    ly_index = layer.load();
    $.jgrid.defaults.styleUI = "Bootstrap";
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_t_proc_return",
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_proc_return",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

$(document).ready(function () {
    init_page();
    getWindowSize();
    get_T01_sel_t_proc_name();
    $(window).bind("resize",
        function () {
        var width = $(".jqGrid_wrapper").width();
        $("#table_list_1").setGridWidth(width);
    });
});

function T01_ins_t_proc_return(input) {
    data = input.T01_ins_t_proc_return;
	layer.close(ly_index);
    //查询失败
    if (Call_OpeResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
    layer.close(ly_index);
}

var insertDb = function () {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_ins_t_proc_return",
            session_id: session_id,
            param_value1: $("#PROC_ID").val(),
            param_value2: $("#RETURN_NAME").val(),
            param_value3: $("#RETURN_TYPE").val(),
            param_value4: $("#S_DESC").val(),
            param_value5: $("#CREATE_DATE").val(),
            param_value6: $("#IS_IMG").val(),
            param_value7: $("#IS_URLENCODE").val(),
            param_value8: $("#LIST_HIDDEN").val()
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_ins_t_proc_return",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_upd_t_proc_return(input) {
    data = input.T01_upd_t_proc_return;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
}

var updateDbByMainId = function () {
    ly_index = layer.load();
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_upd_t_proc_return",
            session_id: session_id,
            param_value1: $("#RETURN_NAME").val(),
            param_value2: $("#RETURN_TYPE").val(),
            param_value3: $("#S_DESC").val(),
            param_value4: $("#CREATE_DATE").val(),
            param_value5: $("#IS_IMG").val(),
            param_value6: $("#IS_URLENCODE").val(),
            param_value7: $("#LIST_HIDDEN").val(),
            param_value8: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_upd_t_proc_return",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_del_t_proc_return(input) {
    data = input.T01_del_t_proc_return;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
    layer.close(ly_index);

    //重新设置jqgrid大小尺寸
    //getWindowSize();
    /*
    $(" #delmodgrid-table").css({
    "top": iTop,
    "left": ileft,
    "height": "140px",
    "width": "390px"
    });*/
}

var delDbByMainId = function () {
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_del_t_proc_return",
            session_id: session_id,
            param_value1: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_del_t_proc_return",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};
