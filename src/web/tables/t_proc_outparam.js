﻿var iTop = 0;
var ileft = 0;
var ly_index;

//接口名称
var mod_t_proc_outparam;

function biz_start(){
}

function T01_sel_t_proc_name(input) {
    var options = {};
    data = input.T01_sel_t_proc_name;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
    if (data[0].MAIN_ID < 72){
        data.splice(0, 1);
	}
    $.each(data, function (i, obj) {
        options[obj.MAIN_ID.toString()] = obj.DB_ID + "--" + obj.INF_CN_NAME;
    });

    mod_t_proc_outparam = [{
            label: '主键',
            name: 'MAIN_ID',
            width: '30px',
            index: 'MAIN_ID',
            editable: false,
            key: true,
            readOnly: true,
            editrules: {
                required: true
            }
        }, {
            label: '接口名称',
            name: 'PROC_ID',
            width: '70px',
            editable: true,
            readOnly: true,
            editrules: true,
            edittype: 'select',
            formatter: 'select',
            editoptions: {
                value: options
            }
        }, {
            label: '接口输出参数中文名称',
            name: 'PARAM_CN_NAME',
            width: '50px',
            editable: true,
            editrules: true
        }, {
            label: '接口输出参数英文名称',
            name: 'PARAM_EN_NAME',
            width: '50px',
            editable: true,
            editrules: true
        }, {
            label: '接口输出参数值类型',
            name: 'PARAM_TYPE',
            width: '40px',
            editable: true,
            editrules: true,
            edittype: 'select',
            formatter: 'select',
            editoptions: {
                value: {
                    INT: 'INT',
                    STRING: 'STRING',
                    FLOAT: 'FLOAT',
                    DATE: 'DATE',
                    CURSOR: 'CURSOR'
                }
            },
            formatoptions: {
                value: {
                    INT: 'INT',
                    STRING: 'STRING',
                    FLOAT: 'FLOAT',
                    DATE: 'DATE',
                    CURSOR: 'CURSOR'
                }
            }
        }, {
            label: '接口输出参数大小',
            name: 'PARAM_SIZE',
            width: '40px',
            editable: true,
            editrules: true,
            editoptions: {
                defaultValue: 50
            }
        }, {
            label: '备注',
            name: 'S_DESC',
            width: '70px',
            editable: true,
            editrules: true
        }, {
            label: '系统时间',
            name: 'CREATE_DATE',
            width: '70px',
            editable: true,
            editrules: true,
            readOnly: true,
            formatter: function (value, row) {
                return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
            },
            editoptions: {
                defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')
            }
        }
    ];
    layer.close(ly_index);
    init();
}

//获取接口名称
function get_T01_sel_t_proc_name() {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_t_proc_name",
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_proc_name",
        success: function (data2) {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            })
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

function T01_sel_t_proc_outparam(input) {
    data = input.T01_sel_t_proc_outparam;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
    $("#table_list_1").jqGrid({
        data: data,
        datatype: "local",
        height: "450",
        autowidth: true,
        shrinkToFit: true,
        rowNum: 10,
        rowList: [10, 20, 30],
        colModel: mod_t_proc_outparam,
        editurl: 'clientArray',
        altRows: true,
        pager: "#pager_list_1",
        viewrecords: true,
        caption: "输出参数列表"
    });
    //导航后面，增加增删改查按钮
    $("#table_list_1").jqGrid("navGrid", "#pager_list_1", {
        edit: true,
        add: true,
        del: true,
        search: true,
        view: false,
        position: "left",
        cloneToTop: false
    }, {
        beforeShowForm: function () {
            $('#PROC_ID').attr('readOnly', true);
            $("#PROC_ID").attr('disabled', "disabled");
            laydate.render({
                elem: '#CREATE_DATE',
                type: 'datetime'
            });
        },
        onclickSubmit: updateDbByMainId,
        closeAfterEdit: true
    }, {
        beforeShowForm: function () {
            laydate.render({
                elem: '#CREATE_DATE',
                type: 'datetime'
            });
        },
        onclickSubmit: insertDb,
        closeAfterAdd: true
    }, {
        top: iTop,
        left: ileft,
        onclickSubmit: delDbByMainId
    }, {
        closeAfterSearch: true
    }, {
        height: 200,
        reloadAfterSubmit: true
    });
    layer.close(ly_index);
}

//初始化
function init() {
    ly_index = layer.load();
    iTop = (winHeight - 300) / 2;
    ileft = (winWidth - 300) / 2;
    $.jgrid.defaults.styleUI = "Bootstrap";
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_t_proc_outparam",
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_proc_outparam",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            })
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

$(document).ready(function () {
    init_page();
    getWindowSize();
    get_T01_sel_t_proc_name();
    $(window).bind("resize",
        function () {
        var width = $(".jqGrid_wrapper").width();
        $("#table_list_1").setGridWidth(width);
    });
});

function T01_ins_t_proc_outparam(input) {
    data = input.T01_ins_t_proc_outparam;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
    layer.close(ly_index);
}

var insertDb = function () {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_ins_t_proc_outparam",
            session_id: session_id,
            param_value1: $("#PROC_ID").val(),
            param_value2: $("#PARAM_CN_NAME").val(),
            param_value3: $("#PARAM_EN_NAME").val(),
            param_value4: $("#PARAM_TYPE").val(),
            param_value5: $("#PARAM_SIZE").val(),
            param_value6: $("#S_DESC").val(),
            param_value7: $("#CREATE_DATE").val()
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_ins_t_proc_outparam",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_upd_t_proc_outparam(input) {
    data = input.T01_upd_t_proc_outparam;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
    layer.close(ly_index);
}

var updateDbByMainId = function () {
    ly_index = layer.load();
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_upd_t_proc_outparam",
            session_id: session_id,
            param_value1: $("#PARAM_CN_NAME").val(),
            param_value2: $("#PARAM_EN_NAME").val(),
            param_value3: $("#PARAM_TYPE").val(),
            param_value4: $("#PARAM_SIZE").val(),
            param_value5: $("#S_DESC").val(),
            param_value6: $("#CREATE_DATE").val(),
            param_value7: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_upd_t_proc_outparam",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_del_t_proc_outparam(input) {
    data = input.T01_del_t_proc_outparam;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
    layer.close(ly_index);

    //重新设置jqgrid大小尺寸
    //getWindowSize();
    /*
    $(" #delmodgrid-table").css({
    "top": iTop,
    "left": ileft,
    "height": "140px",
    "width": "390px"
    });*/
}

var delDbByMainId = function () {
    ly_index = layer.load();
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_del_t_proc_outparam",
            session_id: session_id,
            param_value1: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_del_t_proc_outparam",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};