//主从表tab选项卡标志位
var AA1D24_Tab_Flag = -1;
//选择某一行
var AA1D24_select_t_sub_power_rowId = "";
//按钮事件新增或编辑
var AA1D24_type = "";
//其他页面传到本页面参数
var AA1D24_param = {};
//table选中数据，如无则默认第一条
var AA1D24_rowCheckData = null;

/*定义查询条件变量*/
/*declare query param begin*/
var AA1D24_tem_SUB_ID = "0";var AA1D24_tem_PROC_ID = "-1";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var AA1D24_ary_SUB_ID = null;var AA1D24_ary_PROC_ID = null;var AA1D24_ary_LIMIT_TAG = [{'MAIN_ID': '1', 'CN_NAME': '年'}, {'MAIN_ID': '2', 'CN_NAME': '月'}, {'MAIN_ID': '3', 'CN_NAME': '日'}];
/*declare select options end*/

/*绑定show监听事件*/
if(AA1D24_Tab_Flag == "-1"){
	var n = Get_RandomDiv("AA1D24","");
	$(n).bind("show", function(objTag){
		AA1D24_Adjust_Sub_Sequ();
		if(AA1D24_Tab_Flag > 0)
			AA1D24_adjust_tab();		
	});
}

//设置主从表div顺序
function AA1D24_Adjust_Sub_Sequ(){
	var temSubDivs = $("#content-main").find("div[data-id]");
	var MainDiv = $(Get_RandomDiv("AA1D24",""));
	for(i = 0; i < temSubDivs.length; i++) {
		if(AA1D24_Is_Sub_Div(temSubDivs[i].id)){
			$(temSubDivs[i]).before($(MainDiv));
			break;
		}
	}
}

//主从表传递参数
function AA1D24_param_set(){
	/*Main Subsuv Table Param Begin*/
    if(AA1D24_param.hasOwnProperty("PROC_ID_cn_name"))        $("#AA1D24_find_PROC_ID_cn_name").val(s_decode(AA1D24_param["PROC_ID_cn_name"]));    if(AA1D24_param.hasOwnProperty("PROC_ID"))        $("#AA1D24_find_PROC_ID").val(AA1D24_param["PROC_ID"]);    if(AA1D24_param.hasOwnProperty("hidden_find")){        $("#AA1D24_Ope_PROC_ID").hide();        $("#AA1D24_Clear_PROC_ID").hide();    }	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function AA1D24_t_sub_power_biz_start(inputparam) {
	layer.close(ly_index);
	AA1D24_param = inputparam;
	//主从表传递参数
	AA1D24_param_set();	
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_sub_power$SUB_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AA1D24_get_N01_t_sub_power$SUB_ID");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function AA1D24_format_SUB_ID(value, row, index) {    var objResult = value;    for(i = 0; i < AA1D24_ary_SUB_ID.length; i++) {        var obj = AA1D24_ary_SUB_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AA1D24_format_PROC_ID(value, row, index) {    var objResult = value;    for(i = 0; i < AA1D24_ary_PROC_ID.length; i++) {        var obj = AA1D24_ary_PROC_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AA1D24_format_LIMIT_TAG(value, row, index) {    var objResult = value;    for(i = 0; i < AA1D24_ary_LIMIT_TAG.length; i++) {        var obj = AA1D24_ary_LIMIT_TAG[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AA1D24_get_N01_t_sub_power$SUB_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_sub_power$SUB_ID) == false)        return false;    AA1D24_ary_SUB_ID = input.N01_t_sub_power$SUB_ID;    var inputdata = {        "param_name": "N01_t_proc_inparam$PROC_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AA1D24_get_N01_t_proc_inparam$PROC_ID");}function AA1D24_get_N01_t_proc_inparam$PROC_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_inparam$PROC_ID) == false)        return false;    AA1D24_ary_PROC_ID = input.N01_t_proc_inparam$PROC_ID;    $("#AA1D24_qry_SUB_ID").append("<option value='-1'></option>")    $.each(AA1D24_ary_SUB_ID, function (i, obj) {        addOptionValue("AA1D24_qry_SUB_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    AA1D24_init_t_sub_power();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function AA1D24_PROC_ID_cn_name_fun(){    index_subhtml = "t_proc_name.vue"    random_subhtml = "";    if(loadHtmlSubVueFun("dev_vue/t_proc_name.vue","AA1D24_t_sub_power_call_vue") == true){        var n = Get_RandomDiv("","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#_t_proc_name_Events').bootstrapTable('resetView');                _param["ly_index"] = index;                _param["target_name"] = "AA1D24_find_PROC_ID_cn_name"                _param["target_id"] = "AA1D24_find_PROC_ID"                _param["sourc_id"] = "MAIN_ID"                _param["sourc_name"] = "INF_CN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function AA1D24_page_end(){
	AA1D24_adjust_tab();
}

//应用平台授权显示列定义
var AA1D24_t_sub_power = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '应用平台信息',        field: 'SUB_ID',        sortable: true        ,formatter: AA1D24_format_SUB_ID    },    {        title: '接口名称',        field: 'PROC_ID',        sortable: true        ,formatter: AA1D24_format_PROC_ID    },    {        title: '使用有效期',        field: 'LIMIT_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '已调用接口次数',        field: 'USE_LIMIT',        sortable: true    },    {        title: '接口次数限制',        field: 'LIMIT_NUMBER',        sortable: true    },    {        title: '次数限制类型',        field: 'LIMIT_TAG',        sortable: true        ,formatter: AA1D24_format_LIMIT_TAG    },    {        title: '初次使用时间',        field: 'FIRST_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    }	
	/*table column end*/
];

//页面初始化
function AA1D24_init_t_sub_power() {
	$(window).resize(function () {
		  $('#AA1D24_t_sub_power_Events').bootstrapTable('resetView');
	});
	//应用平台授权查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AA1D24_btn_t_sub_power_query').click();
}

//查询接口
function AA1D24_t_sub_power_query() {
    $('#AA1D24_t_sub_power_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_sub_power",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": AA1D24_tem_SUB_ID        ,"param_value2": AA1D24_tem_SUB_ID        ,"param_value3": AA1D24_tem_PROC_ID        ,"param_value4": AA1D24_tem_PROC_ID		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AA1D24_get_N01_sel_t_sub_power");
}

//查询结果
function AA1D24_get_N01_sel_t_sub_power(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_sub_power) == false)
        return false;
    AA1D24_rowCheckData = null;
    //调整table各列宽度
    $.each(AA1D24_t_sub_power, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_t_sub_power;
    if(s_data.length > 0)
    	AA1D24_rowCheckData = s_data[0];    
    AA1D24_select_t_sub_power_rowId = "";
    AA1D24_Tab_Flag = 0;
    $('#AA1D24_t_sub_power_Events').bootstrapTable('destroy');
    $("#AA1D24_t_sub_power_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AA1D24_t_sub_power_Events').find("tr.changeColor").removeClass('changeColor');
                AA1D24_select_t_sub_power_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AA1D24_t_sub_power_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AA1D24_select_t_sub_power_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#AA1D24_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AA1D24_param.hasOwnProperty("target_name"))
			{
				$("#"+AA1D24_param["target_id"]).val(eval("row."+AA1D24_param["sourc_id"].toUpperCase()));
				$("#"+AA1D24_param["target_name"]).val(s_decode(eval("row."+AA1D24_param["sourc_name"].toUpperCase())));				
				layer.close(AA1D24_param["ly_index"]);
			}
		},
        toolbar: "#AA1D24_t_sub_power_Toolbar",
        columns: AA1D24_t_sub_power,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        onPostBody:function(){
        	if(s_data.length != 0)
            	AA1D24_page_end();
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AA1D24_btn_t_sub_power_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    AA1D24_tem_SUB_ID = "0";    AA1D24_tem_PROC_ID = "-1";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    BA1D24_ary_SUB_ID = null;    BA1D24_ary_PROC_ID = null;    $("#AA1D24_find_PROC_ID_cn_name").val("");    $("#AA1D24_find_PROC_ID").val("-1");
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_sub_power$SUB_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AA1D24_get_N01_t_sub_power$SUB_ID");	
    /*biz end*/
})

//查询按钮
$('#AA1D24_btn_t_sub_power_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    AA1D24_tem_SUB_ID = $("#AA1D24_qry_SUB_ID").val();    AA1D24_tem_PROC_ID = $("#AA1D24_find_PROC_ID").val();	
    /*set query param end*/
	AA1D24_t_sub_power_query();
})

//vue回调
function AA1D24_t_sub_power_call_vue(objResult){
	//选择某条记录或自动选择第一条
	if (AA1D24_select_t_sub_power_rowId != "") 
		AA1D24_rowCheckData = $("#AA1D24_t_sub_power_Events").bootstrapTable('getData')[AA1D24_select_t_sub_power_rowId];
	
	if(index_subhtml == "t_sub_power_$.vue")
	{
		var n = Get_RandomDiv("BA1D24",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AA1D24_type,"ly_index":index};
				if(AA1D24_param.hasOwnProperty("hidden_find")){
					inputdata["hidden_find"] = "1";
				}				
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temSUB_ID = $("#AA1D24_qry_SUB_ID").val();                if(temSUB_ID != ""){                    inputdata["SUB_ID"] = temSUB_ID;                }                var temPROC_ID = $("#AA1D24_find_PROC_ID_cn_name").val();                if(temPROC_ID != ""){                    inputdata["PROC_ID_cn_name"] = temPROC_ID;                    inputdata["PROC_ID"] = $("#AA1D24_PROC_ID").val();                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("dev_vue/t_sub_power_$.js","BA1D24_t_sub_power_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_proc_name.vue"){        var n = Get_RandomDiv("",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":AA1D24_type,                    "ly_index":index,                    "target_name":"AA1D24_find_PROC_ID_cn_name",                    "target_id":"AA1D24_find_PROC_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"INF_CN_NAME"                };                loadScript_hasparam("dev_vue/t_proc_name.js","_t_proc_name_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/

	/*get tab subvue end*/
}

//新增按钮
$("#AA1D24_btn_t_sub_power_add").click(function () {
	AA1D24_type = "add";
	index_subhtml = "t_sub_power_$.vue";
	if(loadHtmlSubVueFun("dev_vue/t_sub_power_$.vue","AA1D24_t_sub_power_call_vue") == true){
		var n = Get_RandomDiv("BA1D24","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BA1D24_param["type"] = AA1D24_type;
				BA1D24_param["ly_index"]= index;
				if(AA1D24_param.hasOwnProperty("hidden_find")){
					BA1D24_param["hidden_find"] = "1";
				}
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temSUB_ID = $("#AA1D24_qry_SUB_ID").val();                if(temSUB_ID != ""){                    BA1D24_param["SUB_ID"] = temSUB_ID;                }                var temPROC_ID = $("#AA1D24_find_PROC_ID_cn_name").val();                if(temPROC_ID != ""){                    BA1D24_param["PROC_ID_cn_name"] = temPROC_ID;                    BA1D24_param["PROC_ID"] = $("#AA1D24_PROC_ID").val();                }				
				/*Send Two FindSelect param end*/

				BA1D24_clear_edit_info();
			},
			end: function(){
				BA1D24_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AA1D24_btn_t_sub_power_edit").click(function () {
	if (AA1D24_select_t_sub_power_rowId != "") {
		AA1D24_type = "edit";
		index_subhtml = "t_sub_power_$.vue";
		if(loadHtmlSubVueFun("dev_vue/t_sub_power_$.vue","AA1D24_t_sub_power_call_vue") == true){
			var n = Get_RandomDiv("BA1D24","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BA1D24_param["type"] = AA1D24_type;
					BA1D24_param["ly_index"] = index;
					if(AA1D24_param.hasOwnProperty("hidden_find")){
						BA1D24_param["hidden_find"] = "1";
					}
					BA1D24_clear_edit_info();
					BA1D24_get_edit_info();
				},
				end: function(){
					BA1D24_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AA1D24_btn_t_sub_power_delete').click(function () {
	//单行选择
	var rowData = $("#AA1D24_t_sub_power_Events").bootstrapTable('getData')[AA1D24_select_t_sub_power_rowId];
	//多行选择
	var rowDatas = AA1D24_sel_row_t_sub_power();
	if (AA1D24_select_t_sub_power_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_sub_power",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AA1D24_N01_del_t_sub_power");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_sub_power",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AA1D24_N01_del_t_sub_power");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AA1D24_N01_del_t_sub_power(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_sub_power) == true)
		AA1D24_t_sub_power_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function AA1D24_adjust_tab(){
	if(typeof($("#AA1D24_TAB_MAIN")[0]) != "undefined" && $("#AA1D24_TAB_MAIN")[0].length != 0){
		AA1D24_Tab_Flag = 1;
		$(Get_RDivNoBuild("AA1D24","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("AA1D24","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("AA1D24","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#AA1D24_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/
//隐藏tab页选项卡function AA1D24_hide_tab_fun(){    var n = null;}//判断是否sub子项div页面function AA1D24_Is_Sub_Div(temDivId){    if(temDivId.indexOf("XX$TTT") == 0)        return false;    return false;}
/*Tab Click Fun End*/

function AA1D24_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("dev_vue/"+inputUrl,"AA1D24_t_sub_power_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (AA1D24_select_t_sub_power_rowId != "") 
			rowData = $("#AA1D24_t_sub_power_Events").bootstrapTable('getData')[AA1D24_select_t_sub_power_rowId];
		else
			rowData = AA1D24_rowCheckData;
		//$("#AA1D24_t_sub_power_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			var temFlag = true;
			$.each(temPar, function (i, obj) {
				if(eval(inputrandom+"_param.hasOwnProperty(\""+obj.target_id+"\")") && eval(inputrandom+"_param[\""+obj.target_id+"\"]").toString() == rowData[obj.sourc_id].toString())
					temFlag = false;
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			if(temFlag){
				//传递子页面隐藏find功能
				eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
				//参数传递并赋值
				eval(inputrandom+"_param_set()");
				var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
				$("#"+inputrandom+"_btn_"+tbName+"_query").click();
			}
		}
		else{
			$.each(temPar, function (i, obj) {
				if(obj.sourc_id.toUpperCase() == "MAIN_ID")
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"-999\"");
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"\"");
			});
			//传递子页面隐藏find功能
			eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
			var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
			$("#"+inputrandom+"_btn_"+tbName+"_query").click();		
		}
	}
}

//清除 查找框
function AA1D24_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AA1D24_sel_row_t_sub_power(){
	//获得选中行
	var checkedbox= $("#AA1D24_t_sub_power_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}