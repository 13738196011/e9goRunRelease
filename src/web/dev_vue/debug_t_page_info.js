//业务逻辑数据开始
function A33541_debug_biz_start(inputparam) {
	layer.close(ly_index);
	A33541_Clear_Inpurt();
    /*biz begin*/
	$("#A33541_param_name").val("APP_USER_ZXY");
	if (A013B7_select_t_sub_user_rowId != ""){
		var A013B7_rowCheckData = $("#A013B7_t_sub_user_Events").bootstrapTable('getData')[A013B7_select_t_sub_user_rowId];
		$("#A33541_sub_usercode").val(A013B7_rowCheckData.SUB_USERCODE);
		$("#A33541_sub_code").val(A33541_get_format_SUB_ID(A013B7_rowCheckData.SUB_ID));
	}
	
	if (AD67FA_select_t_page_info_rowId != ""){
		var AD67FA_rowCheckData = $("#AD67FA_t_page_info_Events").bootstrapTable('getData')[AD67FA_select_t_page_info_rowId];
		$("#A33541_param_value1").val(AD67FA_rowCheckData.PAGE_CODE);
	}
}

function A33541_Clear_Inpurt(){
	$("#A33541_sub_usercode").val("");
	$("#A33541_sub_code").val("");
	$("#A33541_param_value1").val("");
	$("#A33541_t_proc_name_url").val("");
	$("#A33541_debug_result").val("");
	$("#A33541_view_source").val("");
}

function A33541_get_format_SUB_ID(value) {
    var objResult = value;
    for(i = 0; i < A013B7_ary_SUB_ID.length; i++) {
        var obj = A013B7_ary_SUB_ID[i];
        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {
            objResult = obj[GetLowUpp("cn_name")];
            break;
        }
    }
    return objResult.substring(6,objResult.length);
}

$("#A33541_btnDebug").click(function () {
    ly_index = layer.load();
    var show_url = "http://" + getIP_Url() + "?sub_code=" + $("#A33541_sub_code").val() + "&sub_usercode=" + $("#A33541_sub_usercode").val()+"&session_id="+session_id;
    var rooturl = show_url + "&param_name=" + $("#A33541_param_name").val();
    var source_code = "<script type=\"text/javascript\">\r\n"
         + "	$.ajax({\r\n"
         + "		type: \"POST\",\r\n"
         + "		async: false,\r\n"
         + "		url: \"" + show_url + "\",\r\n"
         + "		data:{param_name:\"" + $("#A33541_param_name").val() + "\"\r\n";   
    rooturl += "&param_value" + i.toString() + "=" + $("#A33541_param_value1").val();
    source_code += "			,param_value" + i.toString() + ":\"" + $("#A33541_param_value1").val() + "\"\r\n"
    source_code += "},\r\n"
     + "		//跨域请求的URL\r\n"
     + "		dataType: \"jsonp\",\r\n"
     + "		jsonp: \"jsoncallback\",\r\n"
     + "		jsonpCallback: \"data_result\",\r\n"
     + "		success: function(data) {\r\n"
     + "		},\r\n"
     + "		error: function() {\r\n"
     + "		if(confirm(\"网络故障，请刷新网络\"))\r\n"
     + "			window.location.reload();\r\n"
     + "		},\r\n"
     + "		// 请求完成后的回调函数 (请求成功或失败之后均调用)\r\n"
     + "		complete: function(XMLHttpRequest, textStatus) {\r\n"
     + "		}\r\n"
     + "	});\r\n"
     + "\r\n"
     + "	function data_result(input_data)\r\n"
     + "	{\r\n"
     + "		//alert(input_data);\r\n"
     + "		alert(JSON.stringify(input_data));\r\n"
     + "	}\r\n"
     + "</script>";
    $("#A33541_t_proc_name_url").val(rooturl);
    $("#A33541_view_source").val(source_code);
    inputData = {};
    $.ajax({
        type: "POST",
        async: false,
        url: rooturl,
        data: inputData,
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        //jsonpCallback: inputjsonpCallBack,
        success: function (data) {
        	A33541_get_btnDebug(data);
        },
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                layer.close(ly_index);
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
});

//调试结果
function A33541_get_btnDebug(input) {
	layer.close(ly_index);
	//查询失败
	if (Call_QryResult(input[$("#A33541_param_name").val()]) == false)
		return false;
	$("#A33541_debug_result").val(JSON.stringify(input));
}