#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.z_debug import z_debug
class Cal_Auto_Run(z_debug):    
    def __init__(self):
        pass
    @staticmethod
    def init_data():        
        opd = Ope_DB_Cent()
        opd.Get_cal_variables()
        opd.Get_cal_fun()
        opd.Get_cal_run()
        opd.Get_cal_run_param()
        opd.Get_cal_run_step()
        if Com_Para.ht_cond__real_value == None :
            Com_Para.ht_cond__real_value = {}
    @staticmethod
    def RunCal(inputStrExp, inputRc):
        temRunCode = inputStrExp.replace(" ","").replace("(","").replace(")","").replace("|","&").split("&&")
        remRunValue = []
        iIndex = 0
        for strRunCode in temRunCode:
            temc = Com_Fun.GetHashTable(Com_Para.ht_cal_run,strRunCode)
            temf = Com_Fun.GetHashTable(Com_Para.ht_cal_fun,temc.attFUN_ID)
            lst_var = []
            for crp in Com_Para.list_cal_run_param:
                if crp.attRUN_ID == temc.attMAIN_ID:
                    lst_var.append(Com_Fun.GetHashTable(Com_Para.ht_cal_variables,crp.attVARI_ID))
            objV = {}
            try:
                for crp in lst_var:
                    objV[crp.attVAR_NAME] = getattr(inputRc,crp.attVAR_NAME)                
                fun_us = getattr(inputRc,temf.attFUN_NAME)
                remRunValue.append(fun_us(*objV))
            except Exception as e:
                print("RunCal cal error:"+repr(e))
            finally:
                pass
            iIndex = iIndex + 1
        temResult = Cal_Auto_Run.Cal_Exp(inputStrExp,temRunCode,remRunValue)
        return temResult
    @staticmethod
    def Cal_Exp(inputexpression, inputstrText, inputbValue):
        iIndex = 0
        for temText in inputstrText:
            inputexpression = inputexpression.replace(temText,str(inputbValue[iIndex]))
            iIndex = iIndex + 1
        inputexpression = inputexpression.replace("&&","and").replace("||","or")
        if inputexpression.find(".") != -1 :
            return None
        elif inputexpression.find("*") != -1 :
            return None
        elif inputexpression.find('"') != -1 :
            return None
        elif inputexpression.find("-") != -1 :
            return None
        elif inputexpression.find("\'") != -1 :
            return None
        elif inputexpression.find("'") != -1 :
            return None
        elif inputexpression.find("\"") != -1 :
            return None
        else:
            bResult = eval(inputexpression)
            return bResult
