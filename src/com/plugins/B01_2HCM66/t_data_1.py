#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_data_1(object):
    attmain_id = -1
    attfile_id = 0
    attget_time = ""
    attval_01 = ""
    attval_02 = ""
    attval_03 = ""
    attval_04 = ""
    attval_05 = ""
    attval_06 = ""
    attval_07 = ""
    attval_08 = ""
    attval_09 = ""
    attval_10 = ""
    attcreate_date = None
    atts_desc = ""
    lenget_time = 500
    lenval_01 = 500
    lenval_02 = 500
    lenval_03 = 500
    lenval_04 = 500
    lenval_05 = 500
    lenval_06 = 500
    lenval_07 = 500
    lenval_08 = 500
    lenval_09 = 500
    lenval_10 = 500
    lens_desc = 2000
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["file_id"] = "附件信息"
        temHt["get_time"] = "日期"
        temHt["val_01"] = "纯水（吨）楼顶P2"
        temHt["val_02"] = "纯水（吨）楼顶P3"
        temHt["val_03"] = "纯水（吨）新系统"
        temHt["val_04"] = "纯水（吨）老Ⅰ"
        temHt["val_05"] = "纯水（吨）老Ⅱ"
        temHt["val_06"] = "纯水（吨）老Ⅲ"
        temHt["val_07"] = "纯水（吨）P3"
        temHt["val_08"] = "电（度）G673线"
        temHt["val_09"] = "电（度）G672线"
        temHt["val_10"] = "废水（吨）"
        temHt["create_date"] = "系统时间"
        temHt["s_desc"] = "系统备注"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["file_id"] = "附件信息" 
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["file_id"] = "select main_id,file_name as cn_name from t_file_info" 
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
