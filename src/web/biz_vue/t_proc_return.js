//选择某一行
var A20D9D_select_t_proc_return_rowId = "";
//按钮事件新增或编辑
var A20D9D_type = "";
//其他页面传到本页面参数
var A20D9D_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var A20D9D_tem_PROC_ID = "-1";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var A20D9D_ary_PROC_ID = null;var A20D9D_ary_RETURN_TYPE = null;var A20D9D_ary_IS_IMG = [{'MAIN_ID': '1', 'CN_NAME': '是'}, {'MAIN_ID': '0', 'CN_NAME': '否'}];var A20D9D_ary_IS_URLENCODE = [{'MAIN_ID': '1', 'CN_NAME': '是'}, {'MAIN_ID': '0', 'CN_NAME': '否'}];var A20D9D_ary_LIST_HIDDEN = [{'MAIN_ID': '1', 'CN_NAME': '是'}, {'MAIN_ID': '0', 'CN_NAME': '否'}];
/*declare select options end*/

//主从表传递参数
function A20D9D_param_set(){
	/*Main Subsuv Table Param Begin*/
	if(A20D9D_param.hasOwnProperty("PROC_ID_cn_name"))
		$("#A20D9D_find_PROC_ID_cn_name").val(s_decode(A20D9D_param["PROC_ID_cn_name"]));
	if(A20D9D_param.hasOwnProperty("PROC_ID"))
		$("#A20D9D_find_PROC_ID").val(A20D9D_param["PROC_ID"]);
	if(A20D9D_param.hasOwnProperty("hidden_find")){
		$("#A20D9D_Ope_PROC_ID").hide();
		$("#A20D9D_Clear_PROC_ID").hide();
	}
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function A20D9D_t_proc_return_biz_start(inputparam) {
	layer.close(ly_index);
	A20D9D_param = inputparam;
	A20D9D_param_set();
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_proc_inparam$PROC_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A20D9D_get_N01_t_proc_inparam$PROC_ID");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function A20D9D_format_PROC_ID(value, row, index) {    var objResult = value;    for(i = 0; i < A20D9D_ary_PROC_ID.length; i++) {        var obj = A20D9D_ary_PROC_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A20D9D_format_RETURN_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < A20D9D_ary_RETURN_TYPE.length; i++) {        var obj = A20D9D_ary_RETURN_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A20D9D_format_IS_IMG(value, row, index) {    var objResult = value;    for(i = 0; i < A20D9D_ary_IS_IMG.length; i++) {        var obj = A20D9D_ary_IS_IMG[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A20D9D_format_IS_URLENCODE(value, row, index) {    var objResult = value;    for(i = 0; i < A20D9D_ary_IS_URLENCODE.length; i++) {        var obj = A20D9D_ary_IS_URLENCODE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A20D9D_format_LIST_HIDDEN(value, row, index) {    var objResult = value;    for(i = 0; i < A20D9D_ary_LIST_HIDDEN.length; i++) {        var obj = A20D9D_ary_LIST_HIDDEN[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A20D9D_get_N01_t_proc_inparam$PROC_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_inparam$PROC_ID) == false)        return false;    A20D9D_ary_PROC_ID = input.N01_t_proc_inparam$PROC_ID;    var inputdata = {        "param_name": "N01_t_proc_return$RETURN_TYPE",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A20D9D_get_N01_t_proc_return$RETURN_TYPE");}function A20D9D_get_N01_t_proc_return$RETURN_TYPE(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_return$RETURN_TYPE) == false)        return false;    A20D9D_ary_RETURN_TYPE = input.N01_t_proc_return$RETURN_TYPE;    A20D9D_init_t_proc_return();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function A20D9D_PROC_ID_cn_name_fun(){    index_subhtml = "t_proc_name.vue"    random_subhtml = "A1BCF7";    if(loadHtmlSubVueFun("biz_vue/t_proc_name.vue","A20D9D_t_proc_return_call_vue") == true){        var n = Get_RandomDiv("A1BCF7","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#A1BCF7_t_proc_name_Events').bootstrapTable('resetView');                A1BCF7_param["ly_index"] = index;                A1BCF7_param["target_name"] = "A20D9D_find_PROC_ID_cn_name"                A1BCF7_param["target_id"] = "A20D9D_find_PROC_ID"                A1BCF7_param["sourc_id"] = "MAIN_ID"                A1BCF7_param["sourc_name"] = "INF_CN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function A20D9D_page_end(){
}

//接口返回值显示列定义
var A20D9D_t_proc_return = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '接口名称',        field: 'PROC_ID',        sortable: true        ,formatter: A20D9D_format_PROC_ID    },    {        title: '接口返回值名称',        field: 'RETURN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '返回值类型',        field: 'RETURN_TYPE',        sortable: true        ,formatter: A20D9D_format_RETURN_TYPE    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '是否图片',        field: 'IS_IMG',        sortable: true        ,formatter: A20D9D_format_IS_IMG    },    {        title: '是否URL编码',        field: 'IS_URLENCODE',        sortable: true        ,formatter: A20D9D_format_IS_URLENCODE    },    {        title: '扩展字段',        field: 'LIST_HIDDEN',        sortable: true        ,formatter: A20D9D_format_LIST_HIDDEN    }	
	/*table column end*/
];

//页面初始化
function A20D9D_init_t_proc_return() {
	$(window).resize(function () {
		  $('#A20D9D_t_proc_return_Events').bootstrapTable('resetView');
	});
	//接口返回值查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#A20D9D_btn_t_proc_return_query').click();
}

//查询接口
function A20D9D_t_proc_return_query() {
    $('#A20D9D_t_proc_return_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_proc_return",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": A20D9D_tem_PROC_ID        ,"param_value2": A20D9D_tem_PROC_ID		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "A20D9D_get_N01_sel_t_proc_return");
}

//查询结果
function A20D9D_get_N01_sel_t_proc_return(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_proc_return) == false)
        return false;    
	var s_data = input.N01_sel_t_proc_return;
    A20D9D_select_t_proc_return_rowId = "";
    $('#A20D9D_t_proc_return_Events').bootstrapTable('destroy');
    $("#A20D9D_t_proc_return_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#A20D9D_t_proc_return_Events').find("tr.changeColor").removeClass('changeColor');
                A20D9D_select_t_proc_return_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#A20D9D_t_proc_return_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                A20D9D_select_t_proc_return_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(A20D9D_param.hasOwnProperty("target_name"))
			{
				$("#"+A20D9D_param["target_id"]).val(eval("row."+A20D9D_param["sourc_id"].toUpperCase()));
				$("#"+A20D9D_param["target_name"]).val(s_decode(eval("row."+A20D9D_param["sourc_name"].toUpperCase())));				
				layer.close(A20D9D_param["ly_index"]);
			}
		},
        toolbar: "#A20D9D_t_proc_return_Toolbar",
        columns: A20D9D_t_proc_return,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#A20D9D_btn_t_proc_return_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    A20D9D_tem_PROC_ID = "-1";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    B20D9D_ary_PROC_ID = null;    B20D9D_ary_RETURN_TYPE = null;    $("#A20D9D_find_PROC_ID_cn_name").val("");    $("#A20D9D_find_PROC_ID").val("-1");
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_proc_inparam$PROC_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A20D9D_get_N01_t_proc_inparam$PROC_ID");	
    /*biz end*/
})

//查询按钮
$('#A20D9D_btn_t_proc_return_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    A20D9D_tem_PROC_ID = $("#A20D9D_find_PROC_ID").val();	
    /*set query param end*/
	A20D9D_t_proc_return_query();
})

//vue回调
function A20D9D_t_proc_return_call_vue(objResult){
	if(index_subhtml == "t_proc_return_$.vue")
	{
		var n = Get_RandomDiv("B20D9D",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":A20D9D_type,"ly_index":index};
				if(A20D9D_param.hasOwnProperty("hidden_find")){
					inputdata["hidden_find"] = "1";
				}
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temPROC_ID = $("#A20D9D_find_PROC_ID_cn_name").val();                if(temPROC_ID != ""){                    inputdata["PROC_ID_cn_name"] = temPROC_ID;                    inputdata["PROC_ID"] = $("#A20D9D_find_PROC_ID").val();                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_proc_return_$.js","B20D9D_t_proc_return_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_proc_name.vue"){        var n = Get_RandomDiv("A1BCF7",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":A20D9D_type,                    "ly_index":index,                    "target_name":"A20D9D_find_PROC_ID_cn_name",                    "target_id":"A20D9D_find_PROC_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"INF_CN_NAME"                };                loadScript_hasparam("biz_vue/t_proc_name.js","A1BCF7_t_proc_name_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//新增按钮
$("#A20D9D_btn_t_proc_return_add").click(function () {
	A20D9D_type = "add";
	index_subhtml = "t_proc_return_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_proc_return_$.vue","A20D9D_t_proc_return_call_vue") == true){
		var n = Get_RandomDiv("B20D9D","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				B20D9D_param["type"] = A20D9D_type;
				B20D9D_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temPROC_ID = $("#A20D9D_find_PROC_ID_cn_name").val();                if(temPROC_ID != ""){                    B20D9D_param["PROC_ID_cn_name"] = temPROC_ID;                    B20D9D_param["PROC_ID"] = $("#A20D9D_find_PROC_ID").val();
					if(A20D9D_param.hasOwnProperty("hidden_find")){
						B20D9D_param["hidden_find"] = "1";
					}                }				
				/*Send Two FindSelect param end*/

				B20D9D_clear_edit_info();
			},
			end: function(){
				B20D9D_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#A20D9D_btn_t_proc_return_edit").click(function () {
	if (A20D9D_select_t_proc_return_rowId != "") {
		A20D9D_type = "edit";
		index_subhtml = "t_proc_return_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_proc_return_$.vue","A20D9D_t_proc_return_call_vue") == true){
			var n = Get_RandomDiv("B20D9D","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					B20D9D_param["type"] = A20D9D_type;
					B20D9D_param["ly_index"] = index;
					if(A20D9D_param.hasOwnProperty("hidden_find")){
						B20D9D_param["hidden_find"] = "1";
					}
					B20D9D_clear_edit_info();
					B20D9D_get_edit_info();
				},
				end: function(){
					B20D9D_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#A20D9D_btn_t_proc_return_delete').click(function () {
	//单行选择
	var rowData = $("#A20D9D_t_proc_return_Events").bootstrapTable('getData')[A20D9D_select_t_proc_return_rowId];
	//多行选择
	var rowDatas = A20D9D_sel_row_t_proc_return();
	if (A20D9D_select_t_proc_return_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_proc_return",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "A20D9D_N01_del_t_proc_return");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_proc_return",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "A20D9D_N01_del_t_proc_return");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function A20D9D_N01_del_t_proc_return(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_proc_return) == true)
		A20D9D_t_proc_return_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function A20D9D_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function A20D9D_sel_row_t_proc_return(){
	//获得选中行
	var checkedbox= $("#A20D9D_t_proc_return_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}