#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.db_Self.Db_Common_Self import Db_Common_Self
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.z_debug import z_debug
class D01_t_com_port_reflect_out(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = "1"
    def __init__(self):
        pass
    def init_start(self):
        temdbc2 = Db_Common2()        
        temdbc_self = Db_Common_Self()
        try:
            temStrSql = "select main_id,com_name,com_bot,com_data,com_ct,com_eflag,s_desc,create_date from t_com_port"
            temRs = temdbc2.Common_Sql(temStrSql)
            temComPortList = ""
            iIndex = 0
            for temItem in temRs:
                if iIndex > 0:
                    temComPortList = temComPortList + ";"
                if Com_Fun.NoNull(temItem[1]) != "" and Com_Fun.ZeroNull(temItem[2]) > 0 and Com_Fun.ZeroNull(temItem[3]) > -1 and Com_Fun.ZeroNull(temItem[4]) > -1 and Com_Fun.NoNull(temItem[5]) != "":
                    temComPortList = temComPortList + str(temItem[1])+","+ str(temItem[2])+","+ str(temItem[3])+","+ str(temItem[4])+","+ str(temItem[5])
                iIndex = iIndex + 1
            temStrSql = "update t_param_value set param_value = '"+temComPortList+"' where param_key = 'ComPortList'"
            temdbc_self.CommonExec_Sql(temStrSql)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
            return False