#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_param_value(object):
    attmain_id = -1
    attparam_key = ""
    attparam_value = ""
    attparam_name = ""
    atts_desc = ""
    attcreate_date = None
    lenparam_key = 200
    lenparam_value = 500
    lenparam_name = 500
    lens_desc = 500
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["param_key"] = "参数key"
        temHt["param_value"] = "参数值"
        temHt["param_name"] = "参数描述"
        temHt["s_desc"] = "备注"
        temHt["create_date"] = "系统时间"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["param_key"] = "参数key"
        temHtQry["param_name"] = "参数描述"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
