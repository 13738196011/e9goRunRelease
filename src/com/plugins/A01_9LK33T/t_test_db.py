#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_tet_db(object):
    attMAIN_ID = -1
    attPROC_ID = -1
    attTEST_NAME = ""
    attMN_CODE = -1
    attLONGITUDE = 0.0
    attLATITUDE = 0.0
    attS_STATE = 0
    attCREATE_DATE = None
    attS_DESC = ""
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")