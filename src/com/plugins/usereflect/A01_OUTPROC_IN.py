#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''
import json
from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.zxy.z_debug import z_debug
class A01_OUTPROC_IN(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = 1
    def __init__(self):
        pass
    def init_start(self):
        jsoR = {}        
        jso = {}
        odc = Ope_DB_Cent()
        temSql = "select * from t_proc_name where main_id in ('" + self.param_value1.replace(",","','") + "')"
        jso["t_proc_name"] = odc.ResultSetToJson(temSql)
        temSql = "select * from t_proc_inparam where proc_id in ('"+ self.param_value1.replace(",","','") + "')";
        jso["t_proc_inparam"] = odc.ResultSetToJson(temSql)
        temSql = "select * from t_proc_return where proc_id in ('"+ self.param_value1.replace(",","','") + "')";
        jso["t_proc_return"] = odc.ResultSetToJson(temSql)
        temSql = "select * from t_proc_outparam where proc_id in ('"+ self.param_value1.replace(",","','") + "')";
        jso["t_proc_outparam"] = odc.ResultSetToJson(temSql)
        temSql = "select * from t_sub_power where proc_id in ('"+ self.param_value1.replace(",","','") + "')";
        jso["t_sub_power"] = odc.ResultSetToJson(temSql)
        temSql = "select * from t_sub_userpower where proc_id in ('"+ self.param_value1.replace(",","','") + "')";
        jso["t_sub_userpower"] = odc.ResultSetToJson(temSql)
        jsoR[self.param_name] = jso        
        self.strResult = json.dumps(jsoR,ensure_ascii=False)
        self.strContinue = 0