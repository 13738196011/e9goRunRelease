#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_code_element(object):
    attMAIN_ID = -1
    attELEMENT_CODE = ""
    attELEMENT_NAME = ""
    attELEMENT_VAL = ""
    attELEMENT_TYPE = ""
    attELEMENT_UNIT = ""
    attDTO_NUM = ""
    attELEMENT_CODE2 = ""
    attS_STATE = 0
    attCREATE_DATE = None
    attS_DESC = ""
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")