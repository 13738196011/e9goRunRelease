#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.db3.DBManager3 import DBManager3
from com.zxy.z_debug import z_debug
class Db_Common3(z_debug):    
    attSqlException = ""
    attRs_out = None
    attConn_a = None
    attColumnNames = []
    def __init__(self):
        pass
    def Common_SqlNoCommit(self, inputStrSql):
        temRs = None
        try:
            temDs = DBManager3()
            self.attConn_a = temDs.getConnection()
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock3.acquire()
            temRs = self.attConn_a.executeQueryNoCommit(inputStrSql)         
        except Exception as e:
            temStr = "操作数据库出错[inputStrSql]" + inputStrSql + "\r\n" + repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temStr)
            uL.WriteLog()
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            temDs.returnConnection(self.attConn_a.attConnection)
            self.Close_Conn()
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock3.release()
        return temRs
    def Common_Sql(self, inputStrSql):
        temRs = None
        try:
            temDs = DBManager3()
            self.attConn_a = temDs.getConnection()
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock3.acquire()
            temRs = self.attConn_a.executeQuery(inputStrSql)
            self.attColumnNames = self.attConn_a.attColumnNames        
        except Exception as e:
            temStr = "操作数据库出错[inputStrSql]" + inputStrSql + "\r\n" + repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temStr)
            uL.WriteLog()
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            temDs.returnConnection(self.attConn_a.attConnection)
            self.Close_Conn()
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock3.release()
        return temRs
    def CommonExec_Sql(self, inputStrSql):
        temIResult = -1
        try:
            temDs = DBManager3()
            self.attConn_a = temDs.getConnection()
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock3.acquire()      
            temIResult = self.attConn_a.executeUpdate(inputStrSql)  
        except Exception as e:
            temStr = "操作数据库出错[inputStrSql]" + inputStrSql + "\r\n" + repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temStr)
            uL.WriteLog()
            temIResult = -1
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            temDs.returnConnection(self.attConn_a.attConnection)
            self.Close_Conn()
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock3.release()
        return temIResult
    def Common_Sql_Proc(self,inputProName, inputParameters, inputParamTypes, inputParamOutName, inputParamOutType, inputTrn):
        try:    
            temDs = DBManager3()
            self.attConn_a = temDs.getConnection()        
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock3.acquire()
            if inputTrn.attINF_TYPE == "1":
                self.attRs_out = self.attConn_a.ParamExecuteQuery(inputProName,inputParameters,inputParamTypes,inputParamOutName,inputParamOutType)
            else:
                self.attRs_out = self.attConn_a.ParamExecuteQuery(inputProName,inputParameters,inputParamTypes,inputParamOutName,inputParamOutType,inputTrn.attINF_EN_SQL)
            self.attColumnNames = self.attConn_a.attColumnNames
        except Exception as e:
            self.attSqlException = "数据库操作出错请查看程序错误日志文件:" + inputProName + " "+ repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, self.attSqlException)
            uL.WriteLog()
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            temDs.returnConnection(self.attConn_a.attConnection)
            self.Close_Conn()
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock3.release()
        return self.attRs_out
    def Close_Conn(self):
        if not self.attConn_a.attConnection is None:
            pass
