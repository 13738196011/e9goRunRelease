#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_file_expand(object):
    attmain_id = -1
    attfile_id = 0
    attdate_type = 0
    attdate_begin = None
    attdate_end = None
    attis_ava = 1
    attcreate_date = None
    atts_desc = ""
    lentype_name = 500
    lens_desc = 2000
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["file_id"] = "附件信息"  
        temHt["date_type"] = "数据时间维度"  
        temHt["date_begin"] = "时间起" 
        temHt["date_end"] = "时间止"
        temHt["is_ava"] = "是否启用 "        
        temHt["create_date"] = "系统时间"  
        temHt["s_desc"] = "系统备注" 
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["date_begin"] = "业务"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["file_id"] = "select main_id,file_name as cn_name from t_file_info"  
        temHtSelect["is_ava"] = [{"main_id":"1","cn_name":"启用"},{"main_id":"0","cn_name":"禁用"}]
        temHtSelect["date_type"] = [{"main_id":"1","cn_name":"年"},{"main_id":"2","cn_name":"月"},{"main_id":"3","cn_name":"日"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn