#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import sys,csv,re,xlrd,os,importlib
import collections as col
from openpyxl import load_workbook    
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.plugins.B01_2HCM66.t_excel_tag import t_excel_tag
from com.plugins.B01_2HCM66.t_excel_cell import t_excel_cell
from com.plugins.B01_2HCM66.t_excel_data import t_excel_data
from com.plugins.usereflect.A01_STATICDB4_IN import A01_STATICDB4_IN
from com.plugins.usereflect.A01_STATICDB3_IN import A01_STATICDB3_IN
from com.zxy.z_debug import z_debug
class B01_2HCM66_Time(z_debug):
    strContinue      = "1"   
    strResult        = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = 1
    def __init__(self):
        pass
    def init_start(self):
        pass
    def Create_Table(self,objt_excel_tags,inputtype_name):
        htTable = {}
        htCNTable = {}
        temColumns = None
        temCNColumns = None
        temfile_type_id = -100
        for t_excel_tags in objt_excel_tags:
            if t_excel_tags.attcell_type == 1 and t_excel_tags.atts_desc != "":
                if t_excel_tags.attfile_type_id != temfile_type_id:
                    if temColumns is not None and len(temColumns) > 0:
                        htTable[t_excel_tags.attfile_type_id] = temColumns
                        htCNTable[t_excel_tags.attfile_type_id] = temCNColumns
                    temColumns = []
                    temCNColumns = []
                    temfile_type_id = t_excel_tags.attfile_type_id                
                temColumns.append(t_excel_tags.atts_desc)            
                temCNColumns.append(t_excel_tags.atttag_name)
        if temColumns is not None and len(temColumns) > 0:
            htTable[t_excel_tags.attfile_type_id] = temColumns
            htCNTable[t_excel_tags.attfile_type_id] = temCNColumns       
        for key in list(htTable.keys()):
            self.Create_Model_Py(key,htTable[key],htCNTable[key],inputtype_name)
            bIndex = __file__.rindex(Com_Para.zxyPath+"com"+Com_Para.zxyPath)
            eIndex = __file__.rindex(Com_Para.zxyPath)
            temreflect = __file__[bIndex + 1:eIndex].replace(Com_Para.zxyPath,".") 
            a01_sdb = A01_STATICDB3_IN()
            a01_sdb.param_name = "A01_STATICDBI"
            a01_sdb.param_value1 = "2"
            a01_sdb.param_value2 = temreflect+".t_data_"+str(key)
            a01_sdb.param_value3 = inputtype_name
            a01_sdb.param_value4 = ""
            a01_sdb.param_value10 = "1"
            a01_sdb.init_start()
            odc = Ope_DB_Cent()
            odc.AddMenuInfo("biz_target/t_data_"+str(key)+".html", inputtype_name)      
    def Delete_MOdel_Py(self,temKey): 
        sIndex = __file__.rindex(Com_Para.zxyPath)
        temPathFolder = __file__[0:sIndex]
        temFile = None     
        try: 
            tempTargetPath = Com_Para.ApplicationPath +Com_Para.zxyPath+ "web"+Com_Para.zxyPath+ "biz_target"
            if os.path.exists(temPathFolder+Com_Para.zxyPath+"t_data_"+str(temKey)+".py") == True :
                os.remove(temPathFolder+Com_Para.zxyPath+"t_data_"+str(temKey)+".py")
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+"t_data_"+str(temKey)+".html") == True:
                os.remove(tempTargetPath+Com_Para.zxyPath+"t_data_"+str(temKey)+".html")
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+"t_data_"+str(temKey)+".js") == True:
                os.remove(tempTargetPath+Com_Para.zxyPath+"t_data_"+str(temKey)+".js")                
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if temFile != None:
                temFile.close()
    def SindleTable_Py_Had(self,inputTableName):
        sIndex = __file__.rindex(Com_Para.zxyPath)
        temPathFolder = __file__[0:sIndex]
        return os.path.exists(temPathFolder+Com_Para.zxyPath+str(inputTableName)+".py")
    def Delete_SingleTable_Py(self,inputTableName): 
        sIndex = __file__.rindex(Com_Para.zxyPath)
        temPathFolder = __file__[0:sIndex]
        temFile = None
        try: 
            tempTargetPath = Com_Para.ApplicationPath +Com_Para.zxyPath+ "web"+Com_Para.zxyPath+ "biz_target"
            if os.path.exists(temPathFolder+Com_Para.zxyPath+str(inputTableName)+".py") == True :
                os.remove(temPathFolder+Com_Para.zxyPath+inputTableName+".py")
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+str(inputTableName)+".html") == True:
                os.remove(tempTargetPath+Com_Para.zxyPath+str(inputTableName)+".html")
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+str(inputTableName)+".js") == True:
                os.remove(tempTargetPath+Com_Para.zxyPath+str(inputTableName)+".js") 
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+str(inputTableName)+"_$.html") == True:
                os.remove(tempTargetPath+Com_Para.zxyPath+str(inputTableName)+"_$.html")
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+str(inputTableName)+"_$.js") == True:
                os.remove(tempTargetPath+Com_Para.zxyPath+str(inputTableName)+"_$.js") 
            tempTargetPath = Com_Para.ApplicationPath +Com_Para.zxyPath+ "web"+Com_Para.zxyPath+ "biz_vue"
            if os.path.exists(temPathFolder+Com_Para.zxyPath+str(inputTableName)+".py") == True :
                os.remove(temPathFolder+Com_Para.zxyPath+inputTableName+".py")
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+str(inputTableName)+".vue") == True:
                os.remove(tempTargetPath+Com_Para.zxyPath+str(inputTableName)+".vue")
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+str(inputTableName)+".js") == True:
                os.remove(tempTargetPath+Com_Para.zxyPath+str(inputTableName)+".js") 
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+str(inputTableName)+"_$.vue") == True:
                os.remove(tempTargetPath+Com_Para.zxyPath+str(inputTableName)+"_$.vue")
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+str(inputTableName)+"_$.js") == True:
                os.remove(tempTargetPath+Com_Para.zxyPath+str(inputTableName)+"_$.js")
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if temFile != None:
                temFile.close()
    def Create_ViewModel_Py(self,temViewName,temViewCnName,inputListtable_info,inputViewSql):        
        sIndex = __file__.rindex(Com_Para.zxyPath)
        temPathFolder = __file__[0:sIndex]
        temFile = None     
        try: 
            if os.path.exists(temPathFolder+Com_Para.zxyPath+temViewName+".py") == False :
                temFile = open(temPathFolder+Com_Para.zxyPath+temViewName+".py", 'w',encoding=Com_Para.U_CODE)
                temFile.write("#! python3\n")
                temFile.write("# -*- coding: utf-8 -\n")
                temFile.write("'''\n")
                temFile.write("Created on 2020年05月10日\n")
                temFile.write("@author: auto\n")
                temFile.write("'''\n")
                temFile.write("\n")
                temFile.write("from com.zxy.common.Com_Fun import Com_Fun\n")
                temFile.write("\n")
                temFile.write("#excel数据内容\n")
                temFile.write("class "+temViewName+"(object):\n")
                temFile.write("    #主键\n")
                temFile.write("    attmain_id = -1\n")
                for temt_table_column in inputListtable_info:
                    if temt_table_column.attCOLUMN_TYPE == 1:
                        temFile.write("    att"+temt_table_column.attCOLUMN_EN_NAME+" = \"\"\n")
                    elif temt_table_column.attCOLUMN_TYPE == 2:
                        temFile.write("    att"+temt_table_column.attCOLUMN_EN_NAME+" = 0\n")
                    elif temt_table_column.attCOLUMN_TYPE == 3:
                        temFile.write("    att"+temt_table_column.attCOLUMN_EN_NAME+" = 0.0\n")
                    elif temt_table_column.attCOLUMN_TYPE == 4:
                        temFile.write("    att"+temt_table_column.attCOLUMN_EN_NAME+" = None\n")
                temFile.write("    #字段长度\n")
                for temt_table_column in inputListtable_info:
                    if temt_table_column.attCOLUMN_TYPE == 1:
                        temFile.write("    len"+temt_table_column.attCOLUMN_EN_NAME+" = "+str(temt_table_column.attCOLUMN_LENGTH)+"\n")
                temFile.write("\n")
                temFile.write("    def __init__(self):\n")
                for temt_table_column in inputListtable_info:
                    if temt_table_column.attCOLUMN_TYPE == 4:
                        temFile.write("        self.att"+temt_table_column.attCOLUMN_EN_NAME+" = Com_Fun.GetTime(\"%Y-%m-%d %H:%M:%S\")\n")
                temFile.write("        pass\n")
                temFile.write("\n")
                temFile.write("    def Get_Att_CN(self):\n")
                temFile.write("        temHt = {}\n")
                temFile.write("        temHt[\"main_id\"] = \"主键\"\n")
                for temt_table_column in inputListtable_info:
                    temFile.write("        temHt[\""+temt_table_column.attCOLUMN_EN_NAME+"\"] = \""+temt_table_column.attCOLUMN_CN_NAME+"\"\n")       
                temFile.write("        return temHt\n")
                temFile.write("\n")
                temFile.write("    #查询条件\n")
                temFile.write("    def Qry_Att_CN(self):\n")
                temFile.write("        temHtQry = {}\n")
                for temt_table_column in inputListtable_info:
                    if temt_table_column.attCOLUMN_QRY == 1 or temt_table_column.attCOLUMN_QRY == 2:
                        temFile.write("        temHtQry[\""+temt_table_column.attCOLUMN_EN_NAME+"\"] = \""+temt_table_column.attCOLUMN_CN_NAME+"\"\n")
                temFile.write("        return temHtQry\n")
                temFile.write("\n")
                temFile.write("    #下拉框\n")
                temFile.write("    def Select_Att_CN(self):\n")
                temFile.write("        temHtSelect = {}\n")
                for temt_table_column in inputListtable_info:
                    if temt_table_column.attCOLUMN_QRY_FORMAT.replace(" ","").find("[{") == 0:
                        temFile.write("        temHtSelect[\""+temt_table_column.attCOLUMN_EN_NAME+"\"] = "+temt_table_column.attCOLUMN_QRY_FORMAT+"\n")       
                    elif temt_table_column.attCOLUMN_QRY_FORMAT.replace(" ","") != "":
                        temFile.write("        temHtSelect[\""+temt_table_column.attCOLUMN_EN_NAME+"\"] = \""+temt_table_column.attCOLUMN_QRY_FORMAT+"\"\n")                        
                temFile.write("        return temHtSelect\n")
                temFile.write("\n")
                temFile.write("    #查找框\n")
                temFile.write("    def Find_Att_CN(self):\n")
                temFile.write("        temHtFind = {}\n")
                for temt_table_column in inputListtable_info:
                    if temt_table_column.attCOLUMN_QRY == 3:
                        temFile.write("        temHtFind[\""+temt_table_column.attCOLUMN_EN_NAME+"\"] = \""+temt_table_column.attCOLUMN_FIND_HTML+"\"\n")
                temFile.write("        return temHtFind\n")
                temFile.write("\n")
                temFile.write("    #查找框返回值\n")
                temFile.write("    def FReturn_Att_CN(self):\n")
                temFile.write("        temHtFReturn = {}\n")
                for temt_table_column in inputListtable_info:
                    if temt_table_column.attCOLUMN_QRY == 3:
                        temFile.write("        temHtFReturn[\""+temt_table_column.attCOLUMN_EN_NAME+"\"] = "+temt_table_column.attCOLUMN_FIND_RETURN+"\n")
                temFile.write("        return temHtFReturn\n")    
                temFile.close()
                bIndex = __file__.rindex(Com_Para.zxyPath+"com"+Com_Para.zxyPath)
                eIndex = __file__.rindex(Com_Para.zxyPath)
                temreflect = __file__[bIndex + 1:eIndex].replace(Com_Para.zxyPath,".") 
            else:                                
                bIndex = __file__.rindex(Com_Para.zxyPath+"com"+Com_Para.zxyPath)
                eIndex = __file__.rindex(Com_Para.zxyPath)
                temreflect = __file__[bIndex + 1:eIndex].replace(Com_Para.zxyPath,".")
            a01_sdb = A01_STATICDB4_IN()
            a01_sdb.param_name = "A01_STATICDBI"
            a01_sdb.param_value1 = "2"
            a01_sdb.param_value2 = temreflect+"."+temViewName
            a01_sdb.param_value3 = temViewCnName
            a01_sdb.param_value4 = inputListtable_info
            a01_sdb.param_value5 = inputViewSql  
            a01_sdb.param_value9 = self.param_value9    
            a01_sdb.param_value10 = "1"
            a01_sdb.init_start()    
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if temFile != None:
                temFile.close()
    def Create_SingleTbModel_Py(self,temTableName,temTableCnName,inputListtable_info):
        sIndex = __file__.rindex(Com_Para.zxyPath)
        temPathFolder = __file__[0:sIndex]
        temFile = None     
        try: 
            if os.path.exists(temPathFolder+Com_Para.zxyPath+temTableName+".py") == False :
                temFile = open(temPathFolder+Com_Para.zxyPath+temTableName+".py", 'w',encoding=Com_Para.U_CODE)
                temFile.write("#! python3\n")
                temFile.write("# -*- coding: utf-8 -\n")
                temFile.write("'''\n")
                temFile.write("Created on 2020年05月10日\n")
                temFile.write("@author: auto\n")
                temFile.write("'''\n")
                temFile.write("\n")
                temFile.write("from com.zxy.common.Com_Fun import Com_Fun\n")
                temFile.write("\n")
                temFile.write("#excel数据内容\n")
                temFile.write("class "+temTableName+"(object):\n")
                temFile.write("    #主键\n")
                temFile.write("    attMAIN_ID = -1\n")
                for temt_table_column in inputListtable_info:
                    if temt_table_column.attCOLUMN_EN_NAME.upper() == "MAIN_ID":
                        pass
                    elif temt_table_column.attCOLUMN_TYPE == 1:
                        temFile.write("    att"+temt_table_column.attCOLUMN_EN_NAME+" = \"\"\n")
                    elif temt_table_column.attCOLUMN_TYPE == 2:
                        temFile.write("    att"+temt_table_column.attCOLUMN_EN_NAME+" = 0\n")
                    elif temt_table_column.attCOLUMN_TYPE == 3:
                        temFile.write("    att"+temt_table_column.attCOLUMN_EN_NAME+" = 0.0\n")
                    elif temt_table_column.attCOLUMN_TYPE == 4:
                        temFile.write("    att"+temt_table_column.attCOLUMN_EN_NAME+" = None\n")
                temFile.write("    #字段长度\n")
                for temt_table_column in inputListtable_info:
                    if temt_table_column.attCOLUMN_TYPE == 1:
                        temFile.write("    len"+temt_table_column.attCOLUMN_EN_NAME+" = "+str(temt_table_column.attCOLUMN_LENGTH)+"\n")
                temFile.write("\n")
                temFile.write("    def __init__(self):\n")
                for temt_table_column in inputListtable_info:
                    if temt_table_column.attCOLUMN_TYPE == 4:
                        temFile.write("        self.att"+temt_table_column.attCOLUMN_EN_NAME+" = Com_Fun.GetTime(\"%Y-%m-%d %H:%M:%S\")\n")
                temFile.write("        pass\n")
                temFile.write("\n")
                temFile.write("    def Get_Att_CN(self):\n")
                temFile.write("        temHt = {}\n")
                temFile.write("        temHt[\"MAIN_ID\"] = \"主键\"\n")
                for temt_table_column in inputListtable_info:
                    if temt_table_column.attCOLUMN_EN_NAME.upper() == "MAIN_ID":
                        pass
                    else:
                        temFile.write("        temHt[\""+temt_table_column.attCOLUMN_EN_NAME+"\"] = \""+temt_table_column.attCOLUMN_CN_NAME+"\"\n")       
                temFile.write("        return temHt\n")
                temFile.write("\n")
                temFile.write("    #查询条件\n")
                temFile.write("    def Qry_Att_CN(self):\n")
                temFile.write("        temHtQry = {}\n")
                for temt_table_column in inputListtable_info:
                    if temt_table_column.attCOLUMN_QRY == 1 or temt_table_column.attCOLUMN_QRY == 2:
                        temFile.write("        temHtQry[\""+temt_table_column.attCOLUMN_EN_NAME+"\"] = \""+temt_table_column.attCOLUMN_CN_NAME+"\"\n")
                temFile.write("        return temHtQry\n")
                temFile.write("\n")
                temFile.write("    #下拉框\n")
                temFile.write("    def Select_Att_CN(self):\n")
                temFile.write("        temHtSelect = {}\n")
                for temt_table_column in inputListtable_info:
                    if temt_table_column.attCOLUMN_QRY_FORMAT.replace(" ","").find("[{") == 0:
                        temFile.write("        temHtSelect[\""+temt_table_column.attCOLUMN_EN_NAME+"\"] = "+temt_table_column.attCOLUMN_QRY_FORMAT+"\n")       
                    elif temt_table_column.attCOLUMN_QRY_FORMAT.replace(" ","") != "":
                        temFile.write("        temHtSelect[\""+temt_table_column.attCOLUMN_EN_NAME+"\"] = \""+temt_table_column.attCOLUMN_QRY_FORMAT+"\"\n")                        
                temFile.write("        return temHtSelect\n")
                temFile.write("\n")
                temFile.write("    #查找框\n")
                temFile.write("    def Find_Att_CN(self):\n")
                temFile.write("        temHtFind = {}\n")
                for temt_table_column in inputListtable_info:
                    if temt_table_column.attCOLUMN_QRY == 3:
                        temFile.write("        temHtFind[\""+temt_table_column.attCOLUMN_EN_NAME+"\"] = \""+temt_table_column.attCOLUMN_FIND_HTML+"\"\n")
                temFile.write("        return temHtFind\n")
                temFile.write("\n")
                temFile.write("    #查找框返回值\n")
                temFile.write("    def FReturn_Att_CN(self):\n")
                temFile.write("        temHtFReturn = {}\n")
                for temt_table_column in inputListtable_info:
                    if temt_table_column.attCOLUMN_QRY == 3:
                        temFile.write("        temHtFReturn[\""+temt_table_column.attCOLUMN_EN_NAME+"\"] = "+temt_table_column.attCOLUMN_FIND_RETURN+"\n")
                temFile.write("        return temHtFReturn\n")    
                temFile.close()
                bIndex = __file__.rindex(Com_Para.zxyPath+"com"+Com_Para.zxyPath)
                eIndex = __file__.rindex(Com_Para.zxyPath)
                temreflect = __file__[bIndex + 1:eIndex].replace(Com_Para.zxyPath,".") 
                a01_sdb = A01_STATICDB3_IN()
                a01_sdb.param_name = "A01_STATICDBI"
                a01_sdb.param_value1 = "2"
                a01_sdb.param_value2 = temreflect+"."+temTableName
                a01_sdb.param_value3 = temTableCnName
                a01_sdb.param_value4 = ""
                a01_sdb.param_value9 = self.param_value9
                a01_sdb.param_value10 = "1"
                a01_sdb.init_start()
            else:                                
                bIndex = __file__.rindex(Com_Para.zxyPath+"com"+Com_Para.zxyPath)
                eIndex = __file__.rindex(Com_Para.zxyPath)
                temreflect = __file__[bIndex + 1:eIndex].replace(Com_Para.zxyPath,".")
                a01_sdb = A01_STATICDB3_IN()
                a01_sdb.param_name = "A01_STATICDBI"
                a01_sdb.param_value1 = "2"
                a01_sdb.param_value2 = temreflect+"."+temTableName
                a01_sdb.param_value3 = temTableCnName
                a01_sdb.param_value4 = ""
                a01_sdb.param_value9 = self.param_value9
                a01_sdb.param_value10 = "1"
                a01_sdb.init_start()            
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if temFile != None:
                temFile.close()
    def Create_Model_Py(self,temKey,temColumns,temCNColumns,inputtype_name):        
        sIndex = __file__.rindex(Com_Para.zxyPath)
        temPathFolder = __file__[0:sIndex]
        temFile = None     
        try: 
            if os.path.exists(temPathFolder+Com_Para.zxyPath+"t_data_"+str(temKey)+".py") == False :
                temFile = open(temPathFolder+Com_Para.zxyPath+"t_data_"+str(temKey)+".py", 'w',encoding=Com_Para.U_CODE)
                temFile.write("#! python3\n")
                temFile.write("# -*- coding: utf-8 -\n")
                temFile.write("'''\n")
                temFile.write("Created on 2020年05月10日\n")
                temFile.write("@author: auto\n")
                temFile.write("'''\n")
                temFile.write("\n")
                temFile.write("from com.zxy.common.Com_Fun import Com_Fun\n")
                temFile.write("\n")
                temFile.write("#excel数据内容\n")
                temFile.write("class t_data_"+str(temKey)+"(object):\n")
                temFile.write("    #主键\n")
                temFile.write("    attmain_id = -1\n")
                temFile.write("    #附件ID\n")
                temFile.write("    attfile_id = 0\n")
                for col in temColumns:
                    temFile.write("    att"+col+" = \"\"\n")
                temFile.write("    #系统时间\n")
                temFile.write("    attcreate_date = None\n")
                temFile.write("    #备注\n")
                temFile.write("    atts_desc = \"\"\n")
                temFile.write("    #字段长度\n")
                for col in temColumns:
                    temFile.write("    len"+col+" = 500\n")
                temFile.write("    lens_desc = 2000\n")
                temFile.write("\n")
                temFile.write("    def __init__(self):\n")
                temFile.write("        self.attcreate_date = Com_Fun.GetTime(\"%Y-%m-%d %H:%M:%S\")\n")
                temFile.write("        pass")
                temFile.write("\n")
                temFile.write("    def Get_Att_CN(self):\n")
                temFile.write("        temHt = {}\n")
                temFile.write("        temHt[\"main_id\"] = \"主键\"\n")
                temFile.write("        temHt[\"file_id\"] = \"附件信息\"\n")
                iIndex = 0              
                for col in temCNColumns:
                    temFile.write("        temHt[\""+temColumns[iIndex]+"\"] = \""+col+"\"\n")
                    iIndex = iIndex + 1
                temFile.write("        temHt[\"create_date\"] = \"系统时间\"\n")
                temFile.write("        temHt[\"s_desc\"] = \"系统备注\"\n")
                temFile.write("        return temHt\n")
                temFile.write("\n")
                temFile.write("    #查询条件\n")
                temFile.write("    def Qry_Att_CN(self):\n")
                temFile.write("        temHtQry = {}\n")
                temFile.write("        temHtQry[\"file_id\"] = \"附件信息\" \n")
                temFile.write("        return temHtQry\n")
                temFile.write("\n")
                temFile.write("    #下拉框\n")
                temFile.write("    def Select_Att_CN(self):\n")
                temFile.write("        temHtSelect = {}\n")
                temFile.write("        temHtSelect[\"file_id\"] = \"select main_id,file_name as cn_name from t_file_info\" \n") 
                temFile.write("        return temHtSelect\n")
                temFile.write("\n")
                temFile.write("    #查找框\n")
                temFile.write("    def Find_Att_CN(self):\n")
                temFile.write("        temHtFind = {}\n")
                temFile.write("        #temHtFind[\"file_id\"] = \"业务\"\n")
                temFile.write("        return temHtFind\n")
                temFile.write("\n")
                temFile.write("    #查找框返回值\n")
                temFile.write("    def FReturn_Att_CN(self):\n")
                temFile.write("        temHtFReturn = {}\n")
                temFile.write("        return temHtFReturn\n")
                temFile.close()
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if temFile != None:
                temFile.close()
    def ListOver(self,temtable_info,objTag):
        temResult = False                
        try:             
            for tti in temtable_info:
                temV = str(getattr(objTag, "att"+tti.attname.upper()))
                if tti.attname.upper() != "MAIN_ID" and tti.attname.upper() != "S_DESC":
                    if str(getattr(objTag, "att"+tti.attname.lower())) != "":
                        temResult = True
                        break
        except Exception as ee:
            for tti in temtable_info:
                if tti.attname.upper() != "MAIN_ID" and tti.attname.upper() != "S_DESC":
                    if str(getattr(objTag, "att"+tti.attname.lower())) != "":
                        temResult = True
                        break                    
        return temResult
    def run(self):
        bFlag = True
        bodc = Ope_DB_Cent()
        objt_file_types = bodc.Get_table_list_ByDB("com.plugins.B01_2HCM66.t_file_type","2","where main_id = '"+self.param_value1+"'")
        objt_excel_tags = bodc.Get_table_list_ByDB("com.plugins.B01_2HCM66.t_excel_tag","2","where file_type_id = '"+self.param_value1+"'")
        objt_file_infos = bodc.Get_table_list_ByDB("com.plugins.B01_2HCM66.t_file_info","2","where main_id='"+self.param_value6+"'")
        objt_file_expands = bodc.Get_table_list_ByDB("com.plugins.B01_2HCM66.t_file_expand","2","where file_id='"+self.param_value6+"'")
        self.Create_Table(objt_excel_tags,objt_file_types[0].atttype_name)        
        excelFileName = Com_Para.ApplicationPath+Com_Para.zxyPath+"web"+Com_Para.zxyPath+"file"+Com_Para.zxyPath+self.param_value3
        if excelFileName.find(".") != -1 and excelFileName.split(".")[1].lower() == "xls":
            ws = xlrd.open_workbook(excelFileName)
            sheet = ws.sheet_by_index(0)
            iIndex = 0;
            temTED = t_excel_data()
            temTED.attfile_id = int(self.param_value6)
            objModClass = None
            objN = None
            listName = "" 
            temtable_info = None          
            for i_row in range(sheet.nrows):
                temExcelCell = t_excel_cell()
                if objN is not None:
                    bodc.Ins_metadata_ByDB_NoInc(listName,objN,"2",["file_id"])
                if objModClass is not None:         
                    objN = objModClass()
                jIndex = 0
                for j_cell in range(sheet.ncols):
                    j_cell_value = Com_Fun.NoNull(str(sheet.cell_value(rowx=i_row,colx=j_cell)))
                    if Com_Fun.isVaildDate(j_cell_value) == False:
                        j_cell_value = j_cell_value.replace(" ","").replace("\n","")
                    if j_cell_value != "":
                        if j_cell_value == "能源合计":
                            vg = ""
                        tagIndex = self.setExcelTag(j_cell_value,i_row,j_cell,objt_excel_tags)                        
                        if tagIndex != -1 and objt_excel_tags[tagIndex].attcell_up != "":
                            j_cell_valueT = sheet.cell_value(rowx=i_row+1,colx=j_cell)
                            if j_cell_valueT != "":
                                temTED.attexcel_tag_id = objt_excel_tags[tagIndex].attmain_id
                                temTED.attdata_value = j_cell_valueT                            
                                bodc.Ins_metadata_ByDB("t_excel_data",temTED,"2")
                                objt_excel_tags.pop(tagIndex)                          
                                temExcelCell = t_excel_cell()
                            objModClass = None
                            objN = None
                        elif tagIndex != -1 and objt_excel_tags[tagIndex].attcell_down != "":
                            j_cell_valueT = sheet.cell_value(rowx=i_row-1,colx=j_cell)
                            if j_cell_valueT != "":
                                temTED.attexcel_tag_id = objt_excel_tags[tagIndex].attmain_id
                                temTED.attdata_value = j_cell_valueT                            
                                bodc.Ins_metadata_ByDB("t_excel_data",temTED,"2")
                                objt_excel_tags.pop(tagIndex)
                                temExcelCell = t_excel_cell()
                            objModClass = None
                            objN = None
                        elif tagIndex != -1 and objt_excel_tags[tagIndex].attcell_left != "":
                            temExcelCell.attcell_left = j_cell_value
                            temExcelCell.attt_excel_tag = objt_excel_tags[tagIndex]
                            objModClass = None
                            objN = None
                        elif tagIndex != -1 and objt_excel_tags[tagIndex].attcell_right != "":
                            temExcelCell.attcell_right = j_cell_value
                            temExcelCell.attt_excel_tag = objt_excel_tags[tagIndex]
                            if temExcelCell.attcell_value != "":
                                temTED.attexcel_tag_id = objt_excel_tags[tagIndex].attmain_id
                                temTED.attdata_value = temExcelCell.attcell_value                            
                                bodc.Ins_metadata_ByDB("t_excel_data",temTED,"2")
                                objt_excel_tags.pop(tagIndex)
                                temExcelCell = t_excel_cell() 
                            objModClass = None
                            objN = None                        
                        elif tagIndex != -1 and objt_excel_tags[tagIndex].attcell_special != "":
                            temTED.attexcel_tag_id = objt_excel_tags[tagIndex].attmain_id
                            temTED.attdata_value = j_cell_value                            
                            bodc.Ins_metadata_ByDB("t_excel_data",temTED,"2")
                            objt_excel_tags.pop(tagIndex)
                            temExcelCell = t_excel_cell()
                            objModClass = None
                            objN = None
                        elif tagIndex == -1 and temExcelCell.attcell_left != "":
                            temTED.attexcel_tag_id = temExcelCell.attt_excel_tag.attmain_id
                            temTED.attdata_value = j_cell_value                            
                            bodc.Ins_metadata_ByDB("t_excel_data",temTED,"2")
                            objt_excel_tags.remove(temExcelCell.attt_excel_tag)
                            temExcelCell = t_excel_cell()
                            objModClass = None
                            objN = None
                        elif tagIndex == -1 and temExcelCell.attt_excel_tag is not None:    
                            temExcelCell.attcell_value = temExcelCell.attcell_value  + j_cell_value 
                            objModClass = None
                            objN = None 
                        elif tagIndex == -1:
                            for lint_excel_tag in objt_excel_tags:#range(len(objt_excel_tags)):
                                if lint_excel_tag.attcell_type == 0 and lint_excel_tag.irow == iIndex and lint_excel_tag.jcell == jIndex:
                                    temTED.attexcel_tag_id = lint_excel_tag.attmain_id
                                    temTED.attdata_value = j_cell_value                           
                                    bodc.Ins_metadata_ByDB("t_excel_data",temTED,"2")
                                    objt_excel_tags.remove(lint_excel_tag)
                                    temExcelCell = t_excel_cell()
                                    break
                                elif self.param_value10 == 1 and lint_excel_tag.attcell_type == 1 and lint_excel_tag.jcell == jIndex:
                                    if objModClass is None:
                                        bIndex = __file__.rindex(Com_Para.zxyPath+"com"+Com_Para.zxyPath)
                                        eIndex = __file__.rindex(Com_Para.zxyPath)
                                        temreflect = __file__[bIndex + 1:eIndex].replace(Com_Para.zxyPath,".")+".t_data_"+str(lint_excel_tag.attfile_type_id)
                                        listName = "t_data_"+str(lint_excel_tag.attfile_type_id)
                                        objModule = importlib.import_module(temreflect)  
                                        objModClass = getattr(objModule,listName)
                                        objN = objModClass()
                                        temtable_info = bodc.Get_Sqlite_table_info_ByDB(listName,"2")                                      
                                    setattr(objN,"attfile_id",Com_Fun.ZeroNull(self.param_value6))                                                                            
                                    setattr(objN,"att"+lint_excel_tag.atts_desc.lower(),Com_Fun.NoNull(j_cell_value))
                                    break
                            temExcelCell.attcell_value = temExcelCell.attcell_value  + j_cell_value                            
                    jIndex = jIndex + 1
                    if len(objt_file_types) > 0 and jIndex > int(objt_file_types[0].attcol_length):
                        break
                if len(objt_file_types) > 0 and jIndex > int(objt_file_types[0].attrow_length) and int(objt_file_types[0].attrow_length) > 0:
                    break
                elif listName != "" and objN is not None and self.ListOver(temtable_info,objN) == False:
                    break
                iIndex = iIndex + 1
            if objN is not None:
                bodc.Ins_metadata_ByDB_NoInc(listName,objN,"2",["file_id"])           
        elif excelFileName.find(".") != -1 and excelFileName.split(".")[1].lower() == "xlsx":
            ws = load_workbook(excelFileName)                        
            sheet = ws.worksheets[0]
            iIndex = 0;
            temTED = t_excel_data()
            temTED.attfile_id = int(self.param_value6)
            objModClass = None
            objN = None
            listName = ""            
            for i_row in sheet.rows:
                temExcelCell = t_excel_cell()
                if objN is not None and objN.attfile_id > 0:
                    bodc.Ins_metadata_ByDB_NoInc(listName,objN,"2",["file_id"])
                if objModClass is not None:         
                    objN = objModClass()
                jIndex = 0
                for j_cell in i_row:
                    j_cell_value = Com_Fun.NoNull(j_cell.value)
                    if Com_Fun.isVaildDate(j_cell_value) == False:
                        j_cell_value = j_cell_value.replace(" ","").replace("\n","")
                    if j_cell_value != "":
                        tagIndex = self.setExcelTag(j_cell_value,iIndex,jIndex,objt_excel_tags)                        
                        if tagIndex != -1 and objt_excel_tags[tagIndex].attcell_up != "":                            
                            j_cell_valueT = Com_Fun.NoNull(sheet[j_cell.row+1][j_cell.column-1].value)#sheet.cell_value(rowx=iIndex+1,colx=jIndex)
                            if j_cell_valueT != "":
                                temTED.attexcel_tag_id = objt_excel_tags[tagIndex].attmain_id
                                temTED.attdata_value = j_cell_valueT                            
                                bodc.Ins_metadata_ByDB("t_excel_data",temTED,"2")
                                objt_excel_tags.pop(tagIndex)                          
                                temExcelCell = t_excel_cell()
                            objModClass = None
                            objN = None 
                        elif tagIndex != -1 and objt_excel_tags[tagIndex].attcell_down != "":
                            j_cell_valueT = Com_Fun.NoNull(sheet[j_cell.row-1][j_cell.column-1].value)#sheet.cell_value(rowx=iIndex-1,colx=jIndex)
                            if j_cell_valueT != "":
                                temTED.attexcel_tag_id = objt_excel_tags[tagIndex].attmain_id
                                temTED.attdata_value = j_cell_valueT                            
                                bodc.Ins_metadata_ByDB("t_excel_data",temTED,"2")
                                objt_excel_tags.pop(tagIndex)
                                temExcelCell = t_excel_cell()
                            objModClass = None
                            objN = None 
                        elif tagIndex != -1 and objt_excel_tags[tagIndex].attcell_left != "":
                            temExcelCell.attcell_left = j_cell_value
                            temExcelCell.attt_excel_tag = objt_excel_tags[tagIndex]
                            objModClass = None
                            objN = None 
                        elif tagIndex != -1 and objt_excel_tags[tagIndex].attcell_right != "":
                            temExcelCell.attcell_right = j_cell_value
                            temExcelCell.attt_excel_tag = objt_excel_tags[tagIndex]
                            if temExcelCell.attcell_value != "":
                                temTED.attexcel_tag_id = objt_excel_tags[tagIndex].attmain_id
                                temTED.attdata_value = temExcelCell.attcell_value                            
                                bodc.Ins_metadata_ByDB("t_excel_data",temTED,"2")
                                objt_excel_tags.pop(tagIndex)
                                temExcelCell = t_excel_cell() 
                            objModClass = None
                            objN = None                         
                        elif tagIndex != -1 and objt_excel_tags[tagIndex].attcell_special != "":
                            temTED.attexcel_tag_id = objt_excel_tags[tagIndex].attmain_id
                            temTED.attdata_value = j_cell_value                            
                            bodc.Ins_metadata_ByDB("t_excel_data",temTED,"2")
                            objt_excel_tags.pop(tagIndex)
                            temExcelCell = t_excel_cell()
                            objModClass = None
                            objN = None 
                        elif tagIndex == -1 and temExcelCell.attcell_left != "":
                            temTED.attexcel_tag_id = temExcelCell.attt_excel_tag.attmain_id
                            temTED.attdata_value = j_cell_value                            
                            bodc.Ins_metadata_ByDB("t_excel_data",temTED,"2")
                            objt_excel_tags.remove(temExcelCell.attt_excel_tag)
                            temExcelCell = t_excel_cell()
                            objModClass = None
                            objN = None 
                        elif tagIndex == -1 and temExcelCell.attt_excel_tag is not None:    
                            temExcelCell.attcell_value = temExcelCell.attcell_value  + j_cell_value  
                            objModClass = None
                            objN = None 
                        elif tagIndex == -1:
                            for lint_excel_tag in objt_excel_tags:#range(len(objt_excel_tags)):
                                if lint_excel_tag.attcell_type == 0 and lint_excel_tag.irow == iIndex and lint_excel_tag.jcell == jIndex:
                                    temTED.attexcel_tag_id = lint_excel_tag.attmain_id
                                    temTED.attdata_value = j_cell_value                           
                                    bodc.Ins_metadata_ByDB("t_excel_data",temTED,"2")
                                    objt_excel_tags.remove(lint_excel_tag)
                                    temExcelCell = t_excel_cell()
                                    break
                                elif self.param_value10 == 1 and lint_excel_tag.attcell_type == 1 and lint_excel_tag.jcell == jIndex:
                                    if objModClass is None:
                                        bIndex = __file__.rindex(Com_Para.zxyPath+"com"+Com_Para.zxyPath)
                                        eIndex = __file__.rindex(Com_Para.zxyPath)
                                        temreflect = __file__[bIndex + 1:eIndex].replace(Com_Para.zxyPath,".")+".t_data_"+str(lint_excel_tag.attfile_type_id)
                                        listName = "t_data_"+str(lint_excel_tag.attfile_type_id)
                                        objModule = importlib.import_module(temreflect)  
                                        objModClass = getattr(objModule,listName)
                                        objN = objModClass()  
                                        temtable_info = bodc.Get_Sqlite_table_info_ByDB(listName,"2")                                        
                                    setattr(objN,"attfile_id",Com_Fun.ZeroNull(self.param_value6))                                                                            
                                    setattr(objN,"att"+lint_excel_tag.atts_desc.lower(),Com_Fun.NoNull(j_cell_value))
                                    break
                            temExcelCell.attcell_value = temExcelCell.attcell_value  + j_cell_value
                    jIndex = jIndex + 1
                    if len(objt_file_types) > 0 and jIndex > int(objt_file_types[0].attcol_length):
                        break
                if len(objt_file_types) > 0 and jIndex > int(objt_file_types[0].attrow_length) and int(objt_file_types[0].attrow_length) > 0:
                    break
                elif listName != "" and objN is not None and self.ListOver(temtable_info,objN) == False:
                    break
                iIndex = iIndex + 1
            if objN is not None and objN.attfile_id > 0:
                bodc.Ins_metadata_ByDB_NoInc(listName,objN,"2",["file_id"]) 
        if str(type(self)) == "<class 'type'>":
            self.debug_in(self,"excel数据导入完成")#打印异常信息
        else:
            self.debug_in("excel数据导入完成")#打印异常信息
        if bFlag:
            self.strResult = "{\""+self.param_name+"\":[{\"s_result\":\"1\",\"error_desc\":\"数据导入成功\"}]}"
        else:
            self.strResult = "{\""+self.param_name+"\":[{\"s_result\":\"0\",\"error_desc\":\"数据导入失败\"}]}"
        self.strContinue = 0
    def setExcelTag(self,inputTagName,i_row,j_cell,objt_excel_tags):
        iResult = -1
        for i in range(len(objt_excel_tags)):
            t_excel_tag = objt_excel_tags[i]
            if (inputTagName.strip() == t_excel_tag.atttag_name or inputTagName.strip() == t_excel_tag.attcell_up or inputTagName.strip() == t_excel_tag.attcell_down or inputTagName.strip() == t_excel_tag.attcell_left or inputTagName.strip() == t_excel_tag.attcell_right)and t_excel_tag.attcell_type == 0:
                if t_excel_tag.jcell == -1:
                    t_excel_tag.irow = i_row
                    t_excel_tag.jcell = j_cell
                    self.param_value10 = None
                return i
            elif t_excel_tag.attcell_special.strip() != "" and inputTagName.strip().find(t_excel_tag.atttag_name.replace(" ","")) == 0:
                t_excel_tag.irow = i_row
                t_excel_tag.jcell = j_cell
                self.param_value10 = None
                return i
            if inputTagName.strip() == t_excel_tag.attrow_tag.strip():
                t_excel_tag.irow = i_row
                self.param_value10 = None
                iResult = 0
            if inputTagName.strip() == t_excel_tag.attcolumn_tag.strip():
                t_excel_tag.jcell = j_cell
                if t_excel_tag.attcell_type == 1:
                    self.param_value10 = 1
                    iResult = 0
                    return iResult
                else:
                    self.param_value10 = None
                    iResult = 0
        return iResult      