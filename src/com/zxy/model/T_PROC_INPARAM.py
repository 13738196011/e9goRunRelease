#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class T_PROC_INPARAM(object):
    attMAIN_ID            = -1
    attPROC_ID            = -1
    attPARAM_CN_NAME    = ""
    attPARAM_EN_NAME    = ""
    attPARAM_TYPE        = ""
    attPARAM_SIZE        = 100
    attS_DESC            = ""
    attCREATE_DATE        = None
    attIS_URLENCODE    = 0
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")