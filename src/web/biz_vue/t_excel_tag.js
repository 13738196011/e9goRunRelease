//选择某一行
var AE32BB_select_t_excel_tag_rowId = "";
//按钮事件新增或编辑
var AE32BB_type = "";
//其他页面传到本页面参数
var AE32BB_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var AE32BB_tem_file_type_id = "0";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var AE32BB_ary_file_type_id = null;var AE32BB_ary_is_ava = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];var AE32BB_ary_cell_type = [{'main_id': '0', 'cn_name': 'table格式'}, {'main_id': '1', 'cn_name': '列表格式'}];
/*declare select options end*/

//业务逻辑数据开始
function AE32BB_t_excel_tag_biz_start(inputparam) {
	layer.close(ly_index);
	AE32BB_param = inputparam;
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_excel_tag$file_type_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE32BB_get_N01_t_excel_tag$file_type_id");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function AE32BB_format_file_type_id(value, row, index) {    var objResult = value;    for(i = 0; i < AE32BB_ary_file_type_id.length; i++) {        var obj = AE32BB_ary_file_type_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AE32BB_format_is_ava(value, row, index) {    var objResult = value;    for(i = 0; i < AE32BB_ary_is_ava.length; i++) {        var obj = AE32BB_ary_is_ava[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AE32BB_format_cell_type(value, row, index) {    var objResult = value;    for(i = 0; i < AE32BB_ary_cell_type.length; i++) {        var obj = AE32BB_ary_cell_type[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AE32BB_get_N01_t_excel_tag$file_type_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_excel_tag$file_type_id) == false)        return false;    AE32BB_ary_file_type_id = input.N01_t_excel_tag$file_type_id;    $("#AE32BB_qry_file_type_id").append("<option value='-1'></option>")    $.each(AE32BB_ary_file_type_id, function (i, obj) {        addOptionValue("AE32BB_qry_file_type_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    AE32BB_init_t_excel_tag();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function AE32BB_page_end(){
}

//excel标签表显示列定义
var AE32BB_t_excel_tag = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '单元格标签名称',        field: 'TAG_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '附件类别ID',        field: 'FILE_TYPE_ID',        sortable: true        ,formatter: AE32BB_format_file_type_id    },    {        title: '行起始标签',        field: 'ROW_TAG',        sortable: true        ,formatter: set_s_decode    },    {        title: '列起始标签',        field: 'COLUMN_TAG',        sortable: true        ,formatter: set_s_decode    },    {        title: '数据单元格上邻标签',        field: 'CELL_UP',        sortable: true        ,formatter: set_s_decode    },    {        title: '数据单元格下邻标签',        field: 'CELL_DOWN',        sortable: true        ,formatter: set_s_decode    },    {        title: '数据单元格左邻标签',        field: 'CELL_LEFT',        sortable: true        ,formatter: set_s_decode    },    {        title: '数据单元格右邻标签',        field: 'CELL_RIGHT',        sortable: true        ,formatter: set_s_decode    },    {        title: '数据单元格类型',        field: 'CELL_TYPE',        sortable: true        ,formatter: AE32BB_format_cell_type    },    {        title: '数据特殊处理拦截器',        field: 'CELL_SPECIAL',        sortable: true        ,formatter: set_s_decode    },    {        title: '是否启用 ',        field: 'IS_AVA',        sortable: true        ,formatter: AE32BB_format_is_ava    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '系统备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function AE32BB_init_t_excel_tag() {
	$(window).resize(function () {
		  $('#AE32BB_t_excel_tag_Events').bootstrapTable('resetView');
	});
	//excel标签表查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AE32BB_btn_t_excel_tag_query').click();
}

//查询接口
function AE32BB_t_excel_tag_query() {
    $('#AE32BB_t_excel_tag_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_excel_tag",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": AE32BB_tem_file_type_id        ,"param_value2": AE32BB_tem_file_type_id		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AE32BB_get_N01_sel_t_excel_tag");
}

//查询结果
function AE32BB_get_N01_sel_t_excel_tag(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_excel_tag) == false)
        return false;    
	var s_data = input.N01_sel_t_excel_tag;
    AE32BB_select_t_excel_tag_rowId = "";
    $('#AE32BB_t_excel_tag_Events').bootstrapTable('destroy');
    $("#AE32BB_t_excel_tag_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AE32BB_t_excel_tag_Events').find("tr.changeColor").removeClass('changeColor');
                AE32BB_select_t_excel_tag_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AE32BB_t_excel_tag_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AE32BB_select_t_excel_tag_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AE32BB_param.hasOwnProperty("target_name"))
			{
				$("#"+AE32BB_param["target_id"]).val(eval("row."+AE32BB_param["sourc_id"].toUpperCase()));
				$("#"+AE32BB_param["target_name"]).val(s_decode(eval("row."+AE32BB_param["sourc_name"].toUpperCase())));				
				layer.close(AE32BB_param["ly_index"]);
			}
		},
        toolbar: "#AE32BB_t_excel_tag_Toolbar",
        columns: AE32BB_t_excel_tag,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AE32BB_btn_t_excel_tag_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    AE32BB_tem_file_type_id = "0";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    BE32BB_ary_file_type_id = null;
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_excel_tag$file_type_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE32BB_get_N01_t_excel_tag$file_type_id");	
    /*biz end*/
})

//查询按钮
$('#AE32BB_btn_t_excel_tag_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    AE32BB_tem_file_type_id = $("#AE32BB_qry_file_type_id").val();	
    /*set query param end*/
	AE32BB_t_excel_tag_query();
})

//vue回调
function AE32BB_t_excel_tag_call_vue(objResult){
	if(index_subhtml == "t_excel_tag_$.vue")
	{
		var n = Get_RandomDiv("BE32BB",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AE32BB_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temfile_type_id = $("#AE32BB_qry_file_type_id").val();                if(temfile_type_id != ""){                    inputdata["file_type_id"] = temfile_type_id;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_excel_tag_$.js","BE32BB_t_excel_tag_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//新增按钮
$("#AE32BB_btn_t_excel_tag_add").click(function () {
	AE32BB_type = "add";
	index_subhtml = "t_excel_tag_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_excel_tag_$.vue","AE32BB_t_excel_tag_call_vue") == true){
		var n = Get_RandomDiv("BE32BB","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BE32BB_param["type"] = AE32BB_type;
				BE32BB_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temfile_type_id = $("#AE32BB_qry_file_type_id").val();                if(temfile_type_id != ""){                    BE32BB_param["file_type_id"] = temfile_type_id;                }				
				/*Send Two FindSelect param end*/

				BE32BB_clear_edit_info();
			},
			end: function(){
				BE32BB_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AE32BB_btn_t_excel_tag_edit").click(function () {
	if (AE32BB_select_t_excel_tag_rowId != "") {
		AE32BB_type = "edit";
		index_subhtml = "t_excel_tag_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_excel_tag_$.vue","AE32BB_t_excel_tag_call_vue") == true){
			var n = Get_RandomDiv("BE32BB","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BE32BB_param["type"] = AE32BB_type;
					BE32BB_param["ly_index"] = index;
					BE32BB_clear_edit_info();
					BE32BB_get_edit_info();
				},
				end: function(){
					BE32BB_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AE32BB_btn_t_excel_tag_delete').click(function () {
	//单行选择
	var rowData = $("#AE32BB_t_excel_tag_Events").bootstrapTable('getData')[AE32BB_select_t_excel_tag_rowId];
	//多行选择
	var rowDatas = AE32BB_sel_row_t_excel_tag();
	if (AE32BB_select_t_excel_tag_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_excel_tag",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AE32BB_N01_del_t_excel_tag");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_excel_tag",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AE32BB_N01_del_t_excel_tag");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AE32BB_N01_del_t_excel_tag(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_excel_tag) == true)
		AE32BB_t_excel_tag_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function AE32BB_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AE32BB_sel_row_t_excel_tag(){
	//获得选中行
	var checkedbox= $("#AE32BB_t_excel_tag_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}