//选择某一行
var select_t_excel_tag_rowId = "";

/*定义查询条件变量*/
/*declare query param begin*/
var tem_file_type_id = "0";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var ary_file_type_id = null;var ary_is_ava = [{'MAIN_ID': '1', 'CN_NAME': '启用'}, {'MAIN_ID': '0', 'CN_NAME': '禁用'}];var ary_cell_type = [{'MAIN_ID': '0', 'CN_NAME': 'table格式'}, {'MAIN_ID': '1', 'CN_NAME': '列表格式'}];
/*declare select options end*/

//业务逻辑数据开始
function biz_start() {
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "T01_t_excel_tag$file_type_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "get_T01_t_excel_tag$file_type_id");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function format_file_type_id(value, row, index) {    var objResult = value;    for(i = 0; i < ary_file_type_id.length; i++) {        var obj = ary_file_type_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function format_is_ava(value, row, index) {    var objResult = value;    for(i = 0; i < ary_is_ava.length; i++) {        var obj = ary_is_ava[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function format_cell_type(value, row, index) {    var objResult = value;    for(i = 0; i < ary_cell_type.length; i++) {        var obj = ary_cell_type[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function get_T01_t_excel_tag$file_type_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_t_excel_tag$file_type_id) == false)        return false;    ary_file_type_id = input.T01_t_excel_tag$file_type_id;
    $("#qry_file_type_id").append("<option value='-1'></option>");    $.each(ary_file_type_id, function (i, obj) {        addOptionValue("qry_file_type_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    init_t_excel_tag();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function page_end(){
}

//excel标签表显示列定义
var t_excel_tag = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '单元格标签名称',        field: 'TAG_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '附件类别ID',        field: 'FILE_TYPE_ID',        sortable: true        ,formatter: format_file_type_id    },    {        title: '行起始标签',        field: 'ROW_TAG',        sortable: true        ,formatter: set_s_decode    },    {        title: '列起始标签',        field: 'COLUMN_TAG',        sortable: true        ,formatter: set_s_decode    },    {        title: '数据单元格上邻标签',        field: 'CELL_UP',        sortable: true        ,formatter: set_s_decode    },    {        title: '数据单元格下邻标签',        field: 'CELL_DOWN',        sortable: true        ,formatter: set_s_decode    },    {        title: '数据单元格左邻标签',        field: 'CELL_LEFT',        sortable: true        ,formatter: set_s_decode    },    {        title: '数据单元格右邻标签',        field: 'CELL_RIGHT',        sortable: true        ,formatter: set_s_decode    },    {        title: '数据单元格类型',        field: 'CELL_TYPE',        sortable: true        ,formatter: format_cell_type    },    {        title: '数据特殊处理拦截器',        field: 'CELL_SPECIAL',        sortable: true        ,formatter: set_s_decode    },    {        title: '是否启用 ',        field: 'IS_AVA',        sortable: true        ,formatter: format_is_ava    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true
        ,formatter: set_time_decode    },    {        title: '系统备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function init_t_excel_tag() {
	$(window).resize(function () {
		  $('#t_excel_tag_Events').bootstrapTable('resetView');
	});
	//excel标签表查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#btn_t_excel_tag_query').click();
}

//查询接口
function t_excel_tag_query() {
    $('#t_excel_tag_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "T01_sel_t_excel_tag",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": tem_file_type_id        ,"param_value2": tem_file_type_id		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "get_T01_sel_t_excel_tag");
}

//查询结果
function get_T01_sel_t_excel_tag(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.T01_sel_t_excel_tag) == false)
        return false;    
	var s_data = input.T01_sel_t_excel_tag;
    $('#t_excel_tag_Events').bootstrapTable('destroy');
    $("#t_excel_tag_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#t_excel_tag_Events').find("tr.changeColor").removeClass('changeColor');
                select_t_excel_tag_rowId = "";
            }
            　　　　　else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#t_excel_tag_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                select_t_excel_tag_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(getUrlParam("target_name") != null)
			{
				/*查找框返回值*/
				parent.$("#"+getUrlParam("target_id")).val(eval("row."+getUrlParam("sourc_id").toUpperCase()));
				parent.$("#"+getUrlParam("target_name")).val(eval("row."+getUrlParam("sourc_name").toUpperCase()));			
				var index = parent.layer.getFrameIndex(window.name);
				parent.layer.close(index);
			}
		},
        toolbar: "#t_excel_tag_Toolbar",
        columns: t_excel_tag,
        data: s_data,
        pageNumber: 1,
        pageSize: 30, // 每页的记录行数（*）
        pageList: [30, 100, 500, 2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//查询按钮
$('#btn_t_excel_tag_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    tem_file_type_id = $("#qry_file_type_id").val();	
    /*set query param end*/
	t_excel_tag_query();
})

//新增按钮
$("#btn_t_excel_tag_add").click(function () {
	layer.open({
		type: 2,
        area: ['1100px', '600px'],
        fixed: false, //不固定
        maxmin: true,
        content: 't_excel_tag_$.html?type=add',
        success: function(layero, index){
			var body = layer.getChildFrame('body',index);
			var inputs = body.contents().find("#DataModal").find('input');
        	/*新增按钮页面传父类ID或其他信息*/
        	/*add param value begin*/
        	
        	/*add param value end*/
			
//			$.each(inputs, function (i, obj) {
//				if (obj.id.toUpperCase() == "find_table_id_cn_name".toUpperCase()) {
//					$(obj).val($("#find_table_id_cn_name").val());
//				}
//				else if (("find_"+obj.id).toUpperCase() == "find_table_id".toUpperCase()){
//					$(obj).val($("#find_"+obj.id).val());
//				}
//			});
        }
    });
})

//编辑按钮
$("#btn_t_excel_tag_edit").click(function () {
	if (select_t_excel_tag_rowId != "") {
		layer.open({
			type: 2,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: 't_excel_tag_$.html?type=edit',
			success: function(layero, index){
				var body = layer.getChildFrame('body',index);
				var rowData = $("#t_excel_tag_Events").bootstrapTable('getData')[select_t_excel_tag_rowId];
				var inputs = body.contents().find("#DataModal").find('input');
				var selects = body.contents().find("#DataModal").find("select");				
				$.each(selects, function (i, obj) {
					if(typeof(eval("ary_"+obj.id)) == "object")
					{
						$.each(eval("ary_"+obj.id), function (inx, obj_sub) {
							var div = "<option value='" + obj_sub[GetLowUpp("main_id")] + "'>" + obj_sub[GetLowUpp("cn_name")] + "</option>";
							$(obj).append(div);
						});
					}
				});
				
				Object.keys(rowData).forEach(function (key) {
					$.each(inputs, function (i, obj) {
						if (obj.id.toUpperCase() == key.toUpperCase()) {
							if(typeof(rowData[key]) == "string")
							{
								$(obj).val(s_decode(rowData[key]));
							}
							else
							{
								$(obj).val(rowData[key]);
							}
						}
						else if(obj.id.toUpperCase() == ("find_"+key+"_cn_name").toUpperCase()){
							$.each(eval("ary_"+key.toLowerCase()), function (jindex, obj2) {
								if(obj2[GetLowUpp("main_id")] == rowData[key]){
									body.contents().find("#DataModal").find('[id="'+("find_"+key.toLowerCase()+"_cn_name").toLowerCase()+'"]').val(obj2[GetLowUpp("cn_name")]);
								}
							});
						}
					});
					$.each(selects, function (i, obj) {
						if (obj.id.toUpperCase() == key.toUpperCase()) {
							$(obj).val(rowData[key]);
						}
					});
				});
			}
		});
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#btn_t_excel_tag_delete').click(function () {
	//单行选择
	var rowData = $("#t_excel_tag_Events").bootstrapTable('getData')[select_t_excel_tag_rowId];
	//多行选择
	var rowDatas = sel_row_t_excel_tag();
	if (select_t_excel_tag_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "T01_del_t_excel_tag",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "T01_del_t_excel_tag");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "T01_del_t_excel_tag",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "T01_del_t_excel_tag");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function T01_del_t_excel_tag(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.T01_del_t_excel_tag) == true)
		t_excel_tag_query();
}

//数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//清除 查找框
function clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function sel_row_t_excel_tag(){
	//获得选中行
	var checkedbox= $("#t_excel_tag_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}

$(document).ready(function () {
});

window.onpageshow = function (e) {
    if (e.persisted || (window.performance.navigation.type == 2)) {
        is_history_back = 1;
    } else {
        is_history_back = 0;
    }
    //页面初始化
    init_page();
};