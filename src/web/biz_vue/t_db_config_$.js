//按钮事件新增或编辑
var BF1166_type = "";
//其他页面传到本页面参数
var BF1166_param = {};
//暂时没用
var BF1166_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

//业务逻辑数据开始
function BF1166_t_db_config_biz_start(inputdata) {
	BF1166_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    BF1166_init_t_db_config()	
    /*biz end*/
}

/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function BF1166_page_end(){
	if(BF1166_param["type"] == "edit"){
		BF1166_get_edit_info();
	}
}

//页面初始化方法
function BF1166_init_t_db_config() {
	//type = getUrlParam("type");
	if(BF1166_param["type"] == "add"){
		$("#BF1166_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
		
		/*Get Find Select param end*/	
	}
	else if(BF1166_param["type"] == "edit"){
	
	}
	
	//表单验证
	BF1166_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BF1166_CREATE_DATE").val() == "")    {        $("#BF1166_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BF1166_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BF1166_page_end();
}

//提交表单数据
function BF1166_SubmitForm(){
	if(BF1166_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_db_config",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#BF1166_DB_CN_NAME").val())                ,"param_value2": s_encode($("#BF1166_DB_DRiverClassName").val())                ,"param_value3": s_encode($("#BF1166_DB_url").val())                ,"param_value4": s_encode($("#BF1166_DB_username").val())                ,"param_value5": s_encode($("#BF1166_DB_password").val())                ,"param_value6": s_encode($("#BF1166_DB_version").val())                ,"param_value7": s_encode($("#BF1166_DB_code").val())                ,"param_value8": s_encode($("#BF1166_S_DESC").val())                ,"param_value9": $("#BF1166_CREATE_DATE").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BF1166_get_N01_ins_t_db_config");
	}
	else if(BF1166_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_db_config",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#BF1166_DB_CN_NAME").val())                ,"param_value2": s_encode($("#BF1166_DB_DRiverClassName").val())                ,"param_value3": s_encode($("#BF1166_DB_url").val())                ,"param_value4": s_encode($("#BF1166_DB_username").val())                ,"param_value5": s_encode($("#BF1166_DB_password").val())                ,"param_value6": s_encode($("#BF1166_DB_version").val())                ,"param_value7": s_encode($("#BF1166_DB_code").val())                ,"param_value8": s_encode($("#BF1166_S_DESC").val())                ,"param_value9": $("#BF1166_CREATE_DATE").val()                ,"param_value10": $("#BF1166_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BF1166_get_N01_upd_t_db_config");
	}
}

//vue回调
function BF1166_t_db_config_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#BF1166_save_t_db_config_Edit").click(function () {
	$("form[name='BF1166_DataModal']").submit();
})

/*修改数据*/
function BF1166_get_N01_upd_t_db_config(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_db_config) == true)
	{
		swal("修改数据成功!", "", "success");
		AF1166_t_db_config_query();
		BF1166_clear_validate();
		layer.close(BF1166_param["ly_index"]);
	}
}

/*添加数据*/
function BF1166_get_N01_ins_t_db_config(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_db_config) == true)
	{
		swal("添加数据成功!", "", "success");
		AF1166_t_db_config_query();
		BF1166_clear_validate();
		layer.close(BF1166_param["ly_index"]);
	}
}

//取消编辑
$("#BF1166_cancel_t_db_config_Edit").click(function () {
	layer.close(BF1166_param["ly_index"]);
	BF1166_clear_validate();
	$("[id^='BF1166_div']").hide();
})

//清除查找框
function BF1166_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BF1166_clear_validate(){
	$("#BF1166_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BF1166_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BF1166_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BF1166_clear_edit_info(){
	var inputs = $("#BF1166_DataModal").find('input');
	var selects = $("#BF1166_DataModal").find("select");
	var textareas = $("#BF1166_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	BF1166_init_t_db_config();
}

//页面输入框赋值
function BF1166_get_edit_info(){
	var rowData = $("#AF1166_t_db_config_Events").bootstrapTable('getData')[AF1166_select_t_db_config_rowId];
	var inputs = $("#BF1166_DataModal").find('input');
	var selects = $("#BF1166_DataModal").find("select");
	var textareas = $("#BF1166_DataModal").find('textarea');
	$.each(selects, function (i, obj) {
		if(typeof(eval("AF1166_ary_"+obj.id.toString().replace("BF1166_",""))) == "object")
		{
			$.each(eval("AF1166_ary_"+obj.id.toString().replace("BF1166_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("BF1166_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("BF1166_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("AF1166_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#BF1166_DataModal").find('[id="'+"BF1166_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("BF1166_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("BF1166_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//form验证
function BF1166_checkFormInput() {
    BF1166_validate = $("#BF1166_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BF1166_main_id: {}            ,BF1166_DB_CN_NAME: {}            ,BF1166_DB_DRiverClassName: {}            ,BF1166_DB_url: {}            ,BF1166_DB_username: {}            ,BF1166_DB_password: {}            ,BF1166_DB_version: {}            ,BF1166_DB_code: {}            ,BF1166_S_DESC: {}            ,BF1166_CREATE_DATE: {date: true,required : true,maxlength:19}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BF1166_main_id: {}            ,BF1166_DB_CN_NAME: {}            ,BF1166_DB_DRiverClassName: {}            ,BF1166_DB_url: {}            ,BF1166_DB_username: {}            ,BF1166_DB_password: {}            ,BF1166_DB_version: {}            ,BF1166_DB_code: {}            ,BF1166_S_DESC: {}            ,BF1166_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BF1166_SubmitForm();
        	return false;
        }
    })
}