//按钮事件新增或编辑
var BD6233_type = "";
//其他页面传到本页面参数
var BD6233_param = {};
//暂时没用
var BD6233_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BD6233_ary_element_type = [{'main_id': '1', 'cn_name': '污染源水'}, {'main_id': '2', 'cn_name': '污染源气'}];var BD6233_ary_s_state = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];
/*declare select options end*/

//业务逻辑数据开始
function BD6233_t_code_element_biz_start(inputdata) {
	BD6233_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    $.each(BD6233_ary_element_type, function (i, obj) {        addOptionValue("BD6233_element_type", obj[GetLowUpp("main_id")], objGetLowUpp("cn_name")]);    });    $.each(BD6233_ary_s_state, function (i, obj) {        addOptionValue("BD6233_s_state", obj[GetLowUpp("main_id")], objGetLowUpp("cn_name")]);    });    BD6233_init_t_code_element()	
    /*biz end*/
}

/*biz step begin*/
function BD6233_format_element_type(value, row, index) {    var objResult = value;    for(i = 0; i < BD6233_ary_element_type.length; i++) {        var obj = BD6233_ary_element_type[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BD6233_format_s_state(value, row, index) {    var objResult = value;    for(i = 0; i < BD6233_ary_s_state.length; i++) {        var obj = BD6233_ary_s_state[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function BD6233_page_end(){
	if(BD6233_param["type"] == "edit"){
		BD6233_get_edit_info();
	}
}

//页面初始化方法
function BD6233_init_t_code_element() {
	//type = getUrlParam("type");
	if(BD6233_param["type"] == "add"){
		$("#BD6233_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BD6233_element_name").val(BD6233_param["element_name"]);        $("#BD6233_element_type").val(BD6233_param["element_type"]);        $("#BD6233_s_state").val(BD6233_param["s_state"]);		
		/*Get Find Select param end*/	
	
	}
	else if(BD6233_param["type"] == "edit"){
	
	}
	
	//表单验证
	BD6233_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BD6233_create_date").val() == "")    {        $("#BD6233_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BD6233_create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BD6233_page_end();
}

//提交表单数据
function BD6233_SubmitForm(){
	if(BD6233_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_code_element",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#BD6233_element_code").val())                ,"param_value2": s_encode($("#BD6233_element_name").val())                ,"param_value3": s_encode($("#BD6233_element_val").val())                ,"param_value4": $("#BD6233_element_type").val()                ,"param_value5": s_encode($("#BD6233_element_unit").val())                ,"param_value6": $("#BD6233_s_state").val()                ,"param_value7": $("#BD6233_create_date").val()                ,"param_value8": s_encode($("#BD6233_s_desc").val())                ,"param_value9": $("#BD6233_dot_num").val()                ,"param_value10": s_encode($("#BD6233_up_level").val())                ,"param_value11": s_encode($("#BD6233_down_level").val())                ,"param_value12": s_encode($("#BD6233_up_limit").val())                ,"param_value13": s_encode($("#BD6233_down_limit").val())                ,"param_value14": s_encode($("#BD6233_element_code2").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BD6233_get_N01_ins_t_code_element");
	}
	else if(BD6233_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_code_element",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#BD6233_element_code").val())                ,"param_value2": s_encode($("#BD6233_element_name").val())                ,"param_value3": s_encode($("#BD6233_element_val").val())                ,"param_value4": $("#BD6233_element_type").val()                ,"param_value5": s_encode($("#BD6233_element_unit").val())                ,"param_value6": $("#BD6233_s_state").val()                ,"param_value7": $("#BD6233_create_date").val()                ,"param_value8": s_encode($("#BD6233_s_desc").val())                ,"param_value9": $("#BD6233_dot_num").val()                ,"param_value10": s_encode($("#BD6233_up_level").val())                ,"param_value11": s_encode($("#BD6233_down_level").val())                ,"param_value12": s_encode($("#BD6233_up_limit").val())                ,"param_value13": s_encode($("#BD6233_down_limit").val())                ,"param_value14": s_encode($("#BD6233_element_code2").val())                ,"param_value15": $("#BD6233_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BD6233_get_N01_upd_t_code_element");
	}
}

//vue回调
function BD6233_t_code_element_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#BD6233_save_t_code_element_Edit").click(function () {
	$("form[name='BD6233_DataModal']").submit();
})

/*修改数据*/
function BD6233_get_N01_upd_t_code_element(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_code_element) == true)
	{
		swal("修改数据成功!", "", "success");
		AD6233_t_code_element_query();
		BD6233_clear_validate();
		layer.close(BD6233_param["ly_index"]);
	}
}

/*添加数据*/
function BD6233_get_N01_ins_t_code_element(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_code_element) == true)
	{
		swal("添加数据成功!", "", "success");
		AD6233_t_code_element_query();
		BD6233_clear_validate();
		layer.close(BD6233_param["ly_index"]);
	}
}

//取消编辑
$("#BD6233_cancel_t_code_element_Edit").click(function () {
	layer.close(BD6233_param["ly_index"]);
	BD6233_clear_validate();
	$("[id^='BD6233_div']").hide();
})

//清除查找框
function BD6233_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BD6233_clear_validate(){
	$("#BD6233_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BD6233_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BD6233_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BD6233_clear_edit_info(){
	var inputs = $("#BD6233_DataModal").find('input');
	var selects = $("#BD6233_DataModal").find("select");
	var textareas = $("#BD6233_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	BD6233_init_t_code_element();
}

//页面输入框赋值
function BD6233_get_edit_info(){
	var rowData = $("#AD6233_t_code_element_Events").bootstrapTable('getData')[AD6233_select_t_code_element_rowId];
	var inputs = $("#BD6233_DataModal").find('input');
	var selects = $("#BD6233_DataModal").find("select");
	var textareas = $("#BD6233_DataModal").find('textarea');
	$.each(selects, function (i, obj) {
		if(typeof(eval("AD6233_ary_"+obj.id.toString().replace("BD6233_",""))) == "object")
		{
			$.each(eval("AD6233_ary_"+obj.id.toString().replace("BD6233_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("BD6233_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("BD6233_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("AD6233_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#BD6233_DataModal").find('[id="'+"BD6233_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("BD6233_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("BD6233_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//form验证
function BD6233_checkFormInput() {
    BD6233_validate = $("#BD6233_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BD6233_main_id: {}            ,BD6233_element_code: {}            ,BD6233_element_name: {}            ,BD6233_element_val: {}            ,BD6233_element_type: {}            ,BD6233_element_unit: {}            ,BD6233_s_state: {}            ,BD6233_create_date: {date: true,required : true,maxlength:19}            ,BD6233_s_desc: {}            ,BD6233_dot_num: {digits: true,required : true,maxlength:10}            ,BD6233_up_level: {}            ,BD6233_down_level: {}            ,BD6233_up_limit: {}            ,BD6233_down_limit: {}            ,BD6233_element_code2: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BD6233_main_id: {}            ,BD6233_element_code: {}            ,BD6233_element_name: {}            ,BD6233_element_val: {}            ,BD6233_element_type: {}            ,BD6233_element_unit: {}            ,BD6233_s_state: {}            ,BD6233_create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BD6233_s_desc: {}            ,BD6233_dot_num: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,BD6233_up_level: {}            ,BD6233_down_level: {}            ,BD6233_up_limit: {}            ,BD6233_down_limit: {}            ,BD6233_element_code2: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BD6233_SubmitForm();
        	return false;
        }
    })
}