//按钮事件新增或编辑
var B013B7_type = "";
//其他页面传到本页面参数
var B013B7_param = {};
//暂时没用
var B013B7_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var B013B7_ary_SUB_ID = null;
/*declare select options end*/

//主从表传递参数
function B013B7_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function B013B7_t_sub_user_biz_start(inputdata) {
	B013B7_param = inputdata;
	layer.close(ly_index);
	B013B7_param_set();
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_sub_power$SUB_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B013B7_get_N01_t_sub_power$SUB_ID");	
    /*biz end*/
}

/*biz step begin*/
function B013B7_format_SUB_ID(value, row, index) {    var objResult = value;    for(i = 0; i < B013B7_ary_SUB_ID.length; i++) {        var obj = B013B7_ary_SUB_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B013B7_get_N01_t_sub_power$SUB_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_sub_power$SUB_ID) == false)        return false;    B013B7_ary_SUB_ID = input.N01_t_sub_power$SUB_ID;    if($("#B013B7_SUB_ID").is("select") && $("#B013B7_SUB_ID")[0].options.length == 0)    {        $.each(B013B7_ary_SUB_ID, function (i, obj) {            addOptionValue("B013B7_SUB_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    B013B7_init_t_sub_user();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function B013B7_page_end(){
	if(B013B7_param["type"] == "edit"){
		B013B7_get_edit_info();
	}
}

//页面初始化方法
function B013B7_init_t_sub_user() {
	//type = getUrlParam("type");
	if(B013B7_param["type"] == "add"){
		$("#B013B7_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
		
		/*Get Find Select param end*/	
	}
	else if(B013B7_param["type"] == "edit"){
	
	}
	
	//表单验证
	B013B7_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B013B7_LIMIT_DATE").val() == "")    {        $("#B013B7_LIMIT_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B013B7_LIMIT_DATE',        type: 'datetime',        trigger: 'click'    });    if($("#B013B7_CREATE_DATE").val() == "")    {        $("#B013B7_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B013B7_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B013B7_page_end();
}

//提交表单数据
function B013B7_SubmitForm(){
	if(B013B7_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_sub_user",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#B013B7_SUB_ID").val()                ,"param_value2": s_encode($("#B013B7_SUB_USERNAME").val())                ,"param_value3": s_encode($("#B013B7_SUB_USERCODE").val())                ,"param_value4": $("#B013B7_LIMIT_DATE").val()                ,"param_value5": s_encode($("#B013B7_S_DESC").val())                ,"param_value6": $("#B013B7_CREATE_DATE").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B013B7_get_N01_ins_t_sub_user");
	}
	else if(B013B7_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_sub_user",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#B013B7_SUB_ID").val()                ,"param_value2": s_encode($("#B013B7_SUB_USERNAME").val())                ,"param_value3": s_encode($("#B013B7_SUB_USERCODE").val())                ,"param_value4": $("#B013B7_LIMIT_DATE").val()                ,"param_value5": s_encode($("#B013B7_S_DESC").val())                ,"param_value6": $("#B013B7_CREATE_DATE").val()                ,"param_value7": $("#B013B7_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B013B7_get_N01_upd_t_sub_user");
	}
}

//vue回调
function B013B7_t_sub_user_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#B013B7_save_t_sub_user_Edit").click(function () {
	$("form[name='B013B7_DataModal']").submit();
})

/*修改数据*/
function B013B7_get_N01_upd_t_sub_user(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_sub_user) == true)
	{
		swal("修改数据成功!", "", "success");
		A013B7_t_sub_user_query();
		B013B7_clear_validate();
		layer.close(B013B7_param["ly_index"]);
	}
}

/*添加数据*/
function B013B7_get_N01_ins_t_sub_user(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_sub_user) == true)
	{
		swal("添加数据成功!", "", "success");
		A013B7_t_sub_user_query();
		B013B7_clear_validate();
		layer.close(B013B7_param["ly_index"]);
	}
}

//取消编辑
$("#B013B7_cancel_t_sub_user_Edit").click(function () {
	layer.close(B013B7_param["ly_index"]);
	B013B7_clear_validate();
	$("[id^='B013B7_div']").hide();
})

//清除查找框
function B013B7_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B013B7_clear_validate(){
	$("#B013B7_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B013B7_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B013B7_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B013B7_clear_edit_info(){
	var inputs = $("#B013B7_DataModal").find('input');
	var selects = $("#B013B7_DataModal").find("select");
	var textareas = $("#B013B7_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	B013B7_init_t_sub_user();
}

//页面输入框赋值
function B013B7_get_edit_info(){
	var rowData = $("#A013B7_t_sub_user_Events").bootstrapTable('getData')[A013B7_select_t_sub_user_rowId];
	var inputs = $("#B013B7_DataModal").find('input');
	var selects = $("#B013B7_DataModal").find("select");
	var textareas = $("#B013B7_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"A013B7","B013B7");
}

//form验证
function B013B7_checkFormInput() {
    B013B7_validate = $("#B013B7_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B013B7_main_id: {}            ,B013B7_SUB_ID: {}            ,B013B7_SUB_USERNAME: {}            ,B013B7_SUB_USERCODE: {}            ,B013B7_LIMIT_DATE: {date: true,required : true,maxlength:19}            ,B013B7_S_DESC: {}            ,B013B7_CREATE_DATE: {date: true,required : true,maxlength:19}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B013B7_main_id: {}            ,B013B7_SUB_ID: {}            ,B013B7_SUB_USERNAME: {}            ,B013B7_SUB_USERCODE: {}            ,B013B7_LIMIT_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B013B7_S_DESC: {}            ,B013B7_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B013B7_SubmitForm();
        	return false;
        }
    })
}