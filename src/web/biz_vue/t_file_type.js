//选择某一行
var AE31D6_select_t_file_type_rowId = "";
//按钮事件新增或编辑
var AE31D6_type = "";
//其他页面传到本页面参数
var AE31D6_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var AE31D6_tem_type_name = "";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var AE31D6_ary_is_ava = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];
/*declare select options end*/

//业务逻辑数据开始
function AE31D6_t_file_type_biz_start(inputparam) {
	layer.close(ly_index);
	AE31D6_param = inputparam;
    /*biz begin*/
    if($("#AE31D6_qry_is_ava").is("select") && $("#AE31D6_qry_is_ava")[0].options.length == 0)    {        $("#AE31D6_qry_is_ava").append("<option value='-1'></option>")        $.each(AE31D6_ary_is_ava, function (i, obj) {            addOptionValue("AE31D6_qry_is_ava", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    AE31D6_init_t_file_type()	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function AE31D6_format_is_ava(value, row, index) {    var objResult = value;    for(i = 0; i < AE31D6_ary_is_ava.length; i++) {        var obj = AE31D6_ary_is_ava[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function AE31D6_page_end(){
}

//文件附件类别显示列定义
var AE31D6_t_file_type = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '附件类别名称',        field: 'TYPE_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '最大列',        field: 'COL_LENGTH',        sortable: true    },    {        title: '最大行',        field: 'ROW_LENGTH',        sortable: true    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '是否启用 ',        field: 'IS_AVA',        sortable: true        ,formatter: AE31D6_format_is_ava    },    {        title: '系统备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function AE31D6_init_t_file_type() {
	$(window).resize(function () {
		  $('#AE31D6_t_file_type_Events').bootstrapTable('resetView');
	});
	//文件附件类别查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AE31D6_btn_t_file_type_query').click();
}

//查询接口
function AE31D6_t_file_type_query() {
    $('#AE31D6_t_file_type_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_file_type",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(AE31D6_tem_type_name)		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AE31D6_get_N01_sel_t_file_type");
}

//查询结果
function AE31D6_get_N01_sel_t_file_type(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_file_type) == false)
        return false;    
	var s_data = input.N01_sel_t_file_type;
    AE31D6_select_t_file_type_rowId = "";
    $('#AE31D6_t_file_type_Events').bootstrapTable('destroy');
    $("#AE31D6_t_file_type_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AE31D6_t_file_type_Events').find("tr.changeColor").removeClass('changeColor');
                AE31D6_select_t_file_type_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AE31D6_t_file_type_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AE31D6_select_t_file_type_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AE31D6_param.hasOwnProperty("target_name"))
			{
				$("#"+AE31D6_param["target_id"]).val(eval("row."+AE31D6_param["sourc_id"].toUpperCase()));
				$("#"+AE31D6_param["target_name"]).val(s_decode(eval("row."+AE31D6_param["sourc_name"].toUpperCase())));				
				layer.close(AE31D6_param["ly_index"]);
			}
		},
        toolbar: "#AE31D6_t_file_type_Toolbar",
        columns: AE31D6_t_file_type,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AE31D6_btn_t_file_type_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    AE31D6_tem_type_name = "";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/

	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    if($("#AE31D6_qry_is_ava").is("select") && $("#AE31D6_qry_is_ava")[0].options.length == 0)    {        $("#AE31D6_qry_is_ava").append("<option value='-1'></option>")        $.each(AE31D6_ary_is_ava, function (i, obj) {            addOptionValue("AE31D6_qry_is_ava", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    AE31D6_init_t_file_type()	
    /*biz end*/
})

//查询按钮
$('#AE31D6_btn_t_file_type_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    AE31D6_tem_type_name = $("#AE31D6_qry_type_name").val();	
    /*set query param end*/
	AE31D6_t_file_type_query();
})

//vue回调
function AE31D6_t_file_type_call_vue(objResult){
	if(index_subhtml == "t_file_type_$.vue")
	{
		var n = Get_RandomDiv("BE31D6",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AE31D6_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temtype_name = $("#AE31D6_qry_type_name").val();                if(temtype_name != ""){                    inputdata["type_name"] = temtype_name;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_file_type_$.js","BE31D6_t_file_type_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//新增按钮
$("#AE31D6_btn_t_file_type_add").click(function () {
	AE31D6_type = "add";
	index_subhtml = "t_file_type_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_file_type_$.vue","AE31D6_t_file_type_call_vue") == true){
		var n = Get_RandomDiv("BE31D6","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BE31D6_param["type"] = AE31D6_type;
				BE31D6_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temtype_name = $("#AE31D6_qry_type_name").val();                if(temtype_name != ""){                    BE31D6_param["type_name"] = temtype_name;                }				
				/*Send Two FindSelect param end*/

				BE31D6_clear_edit_info();
			},
			end: function(){
				BE31D6_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AE31D6_btn_t_file_type_edit").click(function () {
	if (AE31D6_select_t_file_type_rowId != "") {
		AE31D6_type = "edit";
		index_subhtml = "t_file_type_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_file_type_$.vue","AE31D6_t_file_type_call_vue") == true){
			var n = Get_RandomDiv("BE31D6","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BE31D6_param["type"] = AE31D6_type;
					BE31D6_param["ly_index"] = index;
					BE31D6_clear_edit_info();
					BE31D6_get_edit_info();
				},
				end: function(){
					BE31D6_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AE31D6_btn_t_file_type_delete').click(function () {
	//单行选择
	var rowData = $("#AE31D6_t_file_type_Events").bootstrapTable('getData')[AE31D6_select_t_file_type_rowId];
	//多行选择
	var rowDatas = AE31D6_sel_row_t_file_type();
	if (AE31D6_select_t_file_type_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_file_type",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AE31D6_N01_del_t_file_type");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_file_type",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AE31D6_N01_del_t_file_type");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AE31D6_N01_del_t_file_type(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_file_type) == true)
		AE31D6_t_file_type_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function AE31D6_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AE31D6_sel_row_t_file_type(){
	//获得选中行
	var checkedbox= $("#AE31D6_t_file_type_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}