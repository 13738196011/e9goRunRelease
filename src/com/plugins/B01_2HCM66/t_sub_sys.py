#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_sub_sys(object):
    attmain_id = -1
    attSUB_NAME = ""
    attSUB_CODE = ""
    attLIMIT_DATE = None
    attS_DESC = ""
    attCREATE_DATE = None
    attIP_ADDR = ""
    lenSUB_NAME = 50
    lenSUB_CODE = 50
    lenS_DESC = 50
    lenIP_ADDR = 500
    def __init__(self):
        self.attLIMIT_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["SUB_NAME"] = "应用平台名称"
        temHt["SUB_CODE"] = "应用平台授权码"
        temHt["LIMIT_DATE"] = "使用有效期"
        temHt["S_DESC"] = "备注"
        temHt["CREATE_DATE"] = "系统时间"
        temHt["IP_ADDR"] = "IP白名单"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
