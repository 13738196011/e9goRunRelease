//按钮事件新增或编辑
var B170C7_type = "";
//其他页面传到本页面参数
var B170C7_param = {};
//暂时没用
var B170C7_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var B170C7_ary_DB_ID = null;var B170C7_ary_INF_TYPE = [{'MAIN_ID': '1', 'CN_NAME': '存储过程'}, {'MAIN_ID': '2', 'CN_NAME': 'SQL语句'}];var B170C7_ary_IS_AUTHORITY = [{'MAIN_ID': '1', 'CN_NAME': '是'}, {'MAIN_ID': '0', 'CN_NAME': '否'}];
/*declare select options end*/

//业务逻辑数据开始
function B170C7_t_proc_name_biz_start(inputdata) {
	B170C7_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_proc_name$DB_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "B170C7_get_N01_t_proc_name$DB_ID");	
    /*biz end*/
}

/*biz step begin*/
function B170C7_format_DB_ID(value, row, index) {    var objResult = value;    for(i = 0; i < B170C7_ary_DB_ID.length; i++) {        var obj = B170C7_ary_DB_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B170C7_format_INF_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < B170C7_ary_INF_TYPE.length; i++) {        var obj = B170C7_ary_INF_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B170C7_format_IS_AUTHORITY(value, row, index) {    var objResult = value;    for(i = 0; i < B170C7_ary_IS_AUTHORITY.length; i++) {        var obj = B170C7_ary_IS_AUTHORITY[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B170C7_get_N01_t_proc_name$DB_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_name$DB_ID) == false)        return false;    B170C7_ary_DB_ID = input.N01_t_proc_name$DB_ID;    if($("#B170C7_DB_ID").is("select") && $("#B170C7_DB_ID")[0].options.length == 0)    {        $.each(B170C7_ary_DB_ID, function (i, obj) {            addOptionValue("B170C7_DB_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#B170C7_INF_TYPE").is("select") && $("#B170C7_INF_TYPE")[0].options.length == 0)    {        $.each(B170C7_ary_INF_TYPE, function (i, obj) {            addOptionValue("B170C7_INF_TYPE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#B170C7_IS_AUTHORITY").is("select") && $("#B170C7_IS_AUTHORITY")[0].options.length == 0)    {        $.each(B170C7_ary_IS_AUTHORITY, function (i, obj) {            addOptionValue("B170C7_IS_AUTHORITY", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    B170C7_init_t_proc_name();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function B170C7_page_end(){
	if(B170C7_param["type"] == "edit"){
		B170C7_get_edit_info();
	}
}

//页面初始化方法
function B170C7_init_t_proc_name() {
	//type = getUrlParam("type");
	if(B170C7_param["type"] == "add"){
		$("#B170C7_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#B170C7_INF_CN_NAME").val(B170C7_param["INF_CN_NAME"]);        $("#B170C7_INF_EN_NAME").val(B170C7_param["INF_EN_NAME"]);        $("#B170C7_IS_AUTHORITY").val(B170C7_param["IS_AUTHORITY"]);		
		/*Get Find Select param end*/	
	}
	else if(B170C7_param["type"] == "edit"){
	
	}
	
	//表单验证
	B170C7_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B170C7_CREATE_DATE").val() == "")    {        $("#B170C7_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B170C7_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B170C7_page_end();
}

//提交表单数据
function B170C7_SubmitForm(){
	if(B170C7_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_proc_name",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#B170C7_DB_ID").val()                ,"param_value2": s_encode($("#B170C7_INF_CN_NAME").val())                ,"param_value3": s_encode($("#B170C7_INF_EN_NAME").val())                ,"param_value4": s_encode($("#B170C7_INF_EN_SQL").val())                ,"param_value5": $("#B170C7_INF_TYPE").val()                ,"param_value6": s_encode($("#B170C7_S_DESC").val())                ,"param_value7": $("#B170C7_CREATE_DATE").val()                ,"param_value8": s_encode($("#B170C7_REFLECT_IN_CLASS").val())                ,"param_value9": s_encode($("#B170C7_REFLECT_OUT_CLASS").val())                ,"param_value10": $("#B170C7_IS_AUTHORITY").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B170C7_get_N01_ins_t_proc_name");
	}
	else if(B170C7_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_proc_name",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#B170C7_DB_ID").val()                ,"param_value2": s_encode($("#B170C7_INF_CN_NAME").val())                ,"param_value3": s_encode($("#B170C7_INF_EN_NAME").val())                ,"param_value4": s_encode($("#B170C7_INF_EN_SQL").val())                ,"param_value5": $("#B170C7_INF_TYPE").val()                ,"param_value6": s_encode($("#B170C7_S_DESC").val())                ,"param_value7": $("#B170C7_CREATE_DATE").val()                ,"param_value8": s_encode($("#B170C7_REFLECT_IN_CLASS").val())                ,"param_value9": s_encode($("#B170C7_REFLECT_OUT_CLASS").val())                ,"param_value10": $("#B170C7_IS_AUTHORITY").val()                ,"param_value11": $("#B170C7_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B170C7_get_N01_upd_t_proc_name");
	}
}

//vue回调
function B170C7_t_proc_name_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#B170C7_save_t_proc_name_Edit").click(function () {
	$("form[name='B170C7_DataModal']").submit();
})

/*修改数据*/
function B170C7_get_N01_upd_t_proc_name(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_proc_name) == true)
	{
		swal("修改数据成功!", "", "success");
		A170C7_t_proc_name_query();
		B170C7_clear_validate();
		layer.close(B170C7_param["ly_index"]);
	}
}

/*添加数据*/
function B170C7_get_N01_ins_t_proc_name(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_proc_name) == true)
	{
		swal("添加数据成功!", "", "success");
		A170C7_t_proc_name_query();
		B170C7_clear_validate();
		layer.close(B170C7_param["ly_index"]);
	}
}

//取消编辑
$("#B170C7_cancel_t_proc_name_Edit").click(function () {
	layer.close(B170C7_param["ly_index"]);
	B170C7_clear_validate();
	$("[id^='B170C7_div']").hide();
})

//清除查找框
function B170C7_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B170C7_clear_validate(){
	$("#B170C7_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B170C7_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B170C7_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B170C7_clear_edit_info(){
	var inputs = $("#B170C7_DataModal").find('input');
	var selects = $("#B170C7_DataModal").find("select");
	var textareas = $("#B170C7_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	B170C7_init_t_proc_name();
}

//页面输入框赋值
function B170C7_get_edit_info(){
	var rowData = $("#A170C7_t_proc_name_Events").bootstrapTable('getData')[A170C7_select_t_proc_name_rowId];
	var inputs = $("#B170C7_DataModal").find('input');
	var selects = $("#B170C7_DataModal").find("select");
	var textareas = $("#B170C7_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"A170C7","B170C7");
	
	/*
	$.each(selects, function (i, obj) {
		if(typeof(eval("A170C7_ary_"+obj.id.toString().replace("B170C7_",""))) == "object")
		{
			$.each(eval("A170C7_ary_"+obj.id.toString().replace("B170C7_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("B170C7_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("B170C7_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("A170C7_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#B170C7_DataModal").find('[id="'+"B170C7_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("B170C7_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("B170C7_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});*/
}

//form验证
function B170C7_checkFormInput() {
    B170C7_validate = $("#B170C7_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B170C7_main_id: {}            ,B170C7_DB_ID: {}            ,B170C7_INF_CN_NAME: {}            ,B170C7_INF_EN_NAME: {}            ,B170C7_INF_EN_SQL: {}            ,B170C7_INF_TYPE: {}            ,B170C7_S_DESC: {}            ,B170C7_CREATE_DATE: {date: true,required : true,maxlength:19}            ,B170C7_REFLECT_IN_CLASS: {}            ,B170C7_REFLECT_OUT_CLASS: {}            ,B170C7_IS_AUTHORITY: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B170C7_main_id: {}            ,B170C7_DB_ID: {}            ,B170C7_INF_CN_NAME: {}            ,B170C7_INF_EN_NAME: {}            ,B170C7_INF_EN_SQL: {}            ,B170C7_INF_TYPE: {}            ,B170C7_S_DESC: {}            ,B170C7_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B170C7_REFLECT_IN_CLASS: {}            ,B170C7_REFLECT_OUT_CLASS: {}            ,B170C7_IS_AUTHORITY: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B170C7_SubmitForm();
        	return false;
        }
    })
}