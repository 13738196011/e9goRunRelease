#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import time,json
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_N86HKL.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_N86HKL import A01_Para
from com.plugins.A01_N86HKL.A01_N86HKL_PLC import A01_N86HKL_PLC
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.plugins.A01_N86HKL.data_min import data_min
from com.plugins.A01_N86TYV.A01_N86TYV_Time import A01_N86TYV_Time
from com.zxy.db2.Db_Common2 import Db_Common2
from com.plugins.A01_N86HKL.Set_Upload_Kg import Set_Upload_Kg
class A01_N86HKL_Time(object):
    strContinue      = "1"   
    strResult        = ""
    def __init__(self):
        pass
    def init_start(self):
        try:
            if A01_Para.objTimeFlag == True :
                A01_Para.objTimeFlag = False
                bodc = Bus_Ope_DB_Cent()
                bodc.init_page()
                tem_data_min = data_min()
                temA01PLC = A01_N86HKL_PLC()
                if temA01PLC.open_plc(A01_Para.objPLC_rack,A01_Para.objPLC_slot) == True :         
                    tem_data_min.attcpu_time = A01_Para.objPLC_tcp.get_plc_datetime()             
                    tem_data_min.attautomatic_on = temA01PLC.single_signal_read(Com_Fun.GetHashTable(A01_Para.objplc_addr_element,1))
                    tem_data_min.atttime_base = temA01PLC.single_signal_read(Com_Fun.GetHashTable(A01_Para.objplc_addr_element,2))
                    tem_data_min.attdoor_closed = temA01PLC.single_signal_read(Com_Fun.GetHashTable(A01_Para.objplc_addr_element,3))
                    tem_data_min.attextruder_on = temA01PLC.single_signal_read(Com_Fun.GetHashTable(A01_Para.objplc_addr_element,4))
                    tem_data_min.attbatch_counter = temA01PLC.single_signal_read(Com_Fun.GetHashTable(A01_Para.objplc_addr_element,5))
                    tem_data_min.attcycle_time = temA01PLC.single_signal_read(Com_Fun.GetHashTable(A01_Para.objplc_addr_element,6))
                    tem_data_min.atttank_weight = temA01PLC.single_signal_read(Com_Fun.GetHashTable(A01_Para.objplc_addr_element,7))
                    tem_data_min.attcooling_water_temp = temA01PLC.single_signal_read(Com_Fun.GetHashTable(A01_Para.objplc_addr_element,8))
                    tem_data_min.attair_pressure = temA01PLC.single_signal_read(Com_Fun.GetHashTable(A01_Para.objplc_addr_element,9))            
                    tem_data_min.atts_type = "1" 
                    if str(tem_data_min.attautomatic_on) == "False":
                        tem_data_min.atttank_weight = 0
                    print("plc 1 cpu time:"+str(tem_data_min.attcpu_time)+" now time:"+Com_Fun.GetTimeDef())
                    bodc.Ins_data_min(tem_data_min)
                A01_Para.objTimeFlag = True
        except Exception as e:
            print("A01_N86HKL_Time.init_start 2 error:"+repr(e)+" "+Com_Fun.GetTimeDef())
        finally:
            pass
