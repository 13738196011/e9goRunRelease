var iTop = 0;
var ileft = 0;
var ly_index;

function biz_start(){
}

//综合应用平台信息
//系统参数
var mod_t_param_value = [{
        label: '主键',
        name: 'MAIN_ID',
        width: '30px',
        index: 'MAIN_ID',
        editable: false,
        key: true,
        readOnly: true,
        editrules: {
            required: true
        }
    }, {
        label: '参数key',
        name: 'PARAM_KEY',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '参数value',
        name: 'PARAM_VALUE',
        width: '70px',
        editable: true,
        editrules: true,
        edittype: 'textarea',
        editoptions: {
            rows: "8",
            cols: "45"
        }
    }, {
        label: '参数名称',
        name: 'PARAM_NAME',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '备注',
        name: 'S_DESC',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '系统时间',
        name: 'CREATE_DATE',
        width: '70px',
        editable: true,
        editrules: true,
        readOnly: true,
        formatter: function (value, row) {
            return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
        },
        editoptions: {
            defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')
        }
    }
];

function T01_sel_t_param_value(input) {
    data = input.T01_sel_t_param_value;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
    $("#table_list_1").jqGrid({
        data: data,
        datatype: "local",
        height: "450",
        autowidth: true,
        shrinkToFit: true,
        rowNum: 10,
        rowList: [10, 20, 30],
        colModel: mod_t_param_value,
        editurl: 'clientArray',
        altRows: true,
        pager: "#pager_list_1",
        viewrecords: true,
        caption: "应用平台授权列表"
    });
    //导航后面，增加增删改查按钮
    $("#table_list_1").jqGrid("navGrid", "#pager_list_1", {
        edit: true,
        add: true,
        del: false,
        search: true,
        view: false,
        position: "left",
        cloneToTop: false
    }, {
        beforeShowForm: function () {
            laydate.render({
                elem: '#CREATE_DATE',
                type: 'datetime'
            });
        },
        onclickSubmit: updateDbByMainId,
        closeAfterEdit: true
    }, {
        beforeShowForm: function () {
            laydate.render({
                elem: '#CREATE_DATE',
                type: 'datetime'
            });
        },
        onclickSubmit: insertDb,
        closeAfterAdd: true
    },
        /*{
        top: iTop,
        left: ileft,
        onclickSubmit: delDbByMainId
        },*/
    {
        closeAfterSearch: true
    }, {
        height: 200,
        reloadAfterSubmit: true
    });
}

//初始化
function init() {
    iTop = (winHeight - 300) / 2;
    ileft = (winWidth - 300) / 2;
    $.jgrid.defaults.styleUI = "Bootstrap";
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_t_param_value",
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_param_value",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

$(document).ready(function () {
    init_page();
    getWindowSize();
    init();
    $(window).bind("resize",
        function () {
        var width = $(".jqGrid_wrapper").width();
        $("#table_list_1").setGridWidth(width);
    });
});

function T01_ins_t_param_value(input) {
    data = input.T01_ins_t_param_value;
	layer.close(ly_index);
    //查询失败
    if (Call_OpeResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
}

var insertDb = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_ins_t_param_value",
            session_id: session_id,
            param_value1: $("#PARAM_KEY").val(),
            param_value2: $("#PARAM_VALUE").val(),
            param_value3: $("#PARAM_NAME").val(),
            param_value4: $("#S_DESC").val(),
            param_value5: $("#CREATE_DATE").val()
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_ins_t_param_value",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_upd_t_param_value(input) {
    data = input.T01_upd_t_param_value;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
}

var updateDbByMainId = function () {
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_upd_t_param_value",
            session_id: session_id,
            param_value1: $("#PARAM_KEY").val(),
            param_value2: $("#PARAM_VALUE").val(),
            param_value3: $("#PARAM_NAME").val(),
            param_value4: $("#S_DESC").val(),
            param_value5: $("#CREATE_DATE").val(),
            param_value6: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_upd_t_param_value",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_del_t_param_value(input) {
    data = input.T01_del_t_param_value;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");

    //重新设置jqgrid大小尺寸
    //getWindowSize();
    /*
    $(" #delmodgrid-table").css({
    "top": iTop,
    "left": ileft,
    "height": "140px",
    "width": "390px"
    });*/
}

var delDbByMainId = function () {
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_del_t_param_value",
            session_id: session_id,
            param_value1: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_del_t_param_value",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};