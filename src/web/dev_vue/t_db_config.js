//主从表tab选项卡标志位
var AF8DA3_Tab_Flag = -1;
//选择某一行
var AF8DA3_select_t_db_config_rowId = "";
//按钮事件新增或编辑
var AF8DA3_type = "";
//其他页面传到本页面参数
var AF8DA3_param = {};
//table选中数据，如无则默认第一条
var AF8DA3_rowCheckData = null;

/*定义查询条件变量*/
/*declare query param begin*/

/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

/*绑定show监听事件*/
if(AF8DA3_Tab_Flag == "-1"){
	var n = Get_RandomDiv("AF8DA3","");
	$(n).bind("show", function(objTag){
		AF8DA3_Adjust_Sub_Sequ();
		if(AF8DA3_Tab_Flag > 0)
			AF8DA3_adjust_tab();		
	});
}

//设置主从表div顺序
function AF8DA3_Adjust_Sub_Sequ(){
	var temSubDivs = $("#content-main").find("div[data-id]");
	var MainDiv = $(Get_RandomDiv("AF8DA3",""));
	for(i = 0; i < temSubDivs.length; i++) {
		if(AF8DA3_Is_Sub_Div(temSubDivs[i].id)){
			$(temSubDivs[i]).before($(MainDiv));
			break;
		}
	}
}

//主从表传递参数
function AF8DA3_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function AF8DA3_t_db_config_biz_start(inputparam) {
	layer.close(ly_index);
	AF8DA3_param = inputparam;
	//主从表传递参数
	AF8DA3_param_set();	
    /*biz begin*/
    AF8DA3_init_t_db_config()	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function AF8DA3_page_end(){
	AF8DA3_adjust_tab();
}

//数据源配置2显示列定义
var AF8DA3_t_db_config = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '子系统名称',        field: 'DB_CN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '子系统驱动',        field: 'DB_DRIVERCLASSNAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '子系统url',        field: 'DB_URL',        sortable: true        ,formatter: set_s_decode    },    {        title: '子系统登录账号',        field: 'DB_USERNAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '子系统登录密码',        field: 'DB_PASSWORD',        sortable: true        ,formatter: set_s_decode    },    {        title: '子系统版本',        field: 'DB_VERSION',        sortable: true        ,formatter: set_s_decode    },    {        title: '子系统编码',        field: 'DB_CODE',        sortable: true        ,formatter: set_s_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    }	
	/*table column end*/
];

//页面初始化
function AF8DA3_init_t_db_config() {
	$(window).resize(function () {
		  $('#AF8DA3_t_db_config_Events').bootstrapTable('resetView');
	});
	//数据源配置2查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AF8DA3_btn_t_db_config_query').click();
}

//查询接口
function AF8DA3_t_db_config_query() {
    $('#AF8DA3_t_db_config_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_db_config",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AF8DA3_get_N01_sel_t_db_config");
}

//查询结果
function AF8DA3_get_N01_sel_t_db_config(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_db_config) == false)
        return false;
    AF8DA3_rowCheckData = null;
    //调整table各列宽度
    $.each(AF8DA3_t_db_config, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_t_db_config;
    if(s_data.length > 0)
    	AF8DA3_rowCheckData = s_data[0];    
    AF8DA3_select_t_db_config_rowId = "";
    AF8DA3_Tab_Flag = 0;
    $('#AF8DA3_t_db_config_Events').bootstrapTable('destroy');
    $("#AF8DA3_t_db_config_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AF8DA3_t_db_config_Events').find("tr.changeColor").removeClass('changeColor');
                AF8DA3_select_t_db_config_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AF8DA3_t_db_config_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AF8DA3_select_t_db_config_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#AF8DA3_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AF8DA3_param.hasOwnProperty("target_name"))
			{
				$("#"+AF8DA3_param["target_id"]).val(eval("row."+AF8DA3_param["sourc_id"].toUpperCase()));
				$("#"+AF8DA3_param["target_name"]).val(s_decode(eval("row."+AF8DA3_param["sourc_name"].toUpperCase())));				
				layer.close(AF8DA3_param["ly_index"]);
			}
		},
        toolbar: "#AF8DA3_t_db_config_Toolbar",
        columns: AF8DA3_t_db_config,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        onPostBody:function(){
        	if(s_data.length != 0)
            	AF8DA3_page_end();
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AF8DA3_btn_t_db_config_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/

	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/

	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    AF8DA3_init_t_db_config()	
    /*biz end*/
})

//查询按钮
$('#AF8DA3_btn_t_db_config_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
	
    /*set query param end*/
	AF8DA3_t_db_config_query();
})

//vue回调
function AF8DA3_t_db_config_call_vue(objResult){
	//选择某条记录或自动选择第一条
	if (AF8DA3_select_t_db_config_rowId != "") 
		AF8DA3_rowCheckData = $("#AF8DA3_t_db_config_Events").bootstrapTable('getData')[AF8DA3_select_t_db_config_rowId];
	
	if(index_subhtml == "t_db_config_$.vue")
	{
		var n = Get_RandomDiv("BF8DA3",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AF8DA3_type,"ly_index":index};
				if(AF8DA3_param.hasOwnProperty("hidden_find")){
					inputdata["hidden_find"] = "1";
				}				
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("dev_vue/t_db_config_$.js","BF8DA3_t_db_config_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/

	/*get tab subvue end*/
}

//新增按钮
$("#AF8DA3_btn_t_db_config_add").click(function () {
	AF8DA3_type = "add";
	index_subhtml = "t_db_config_$.vue";
	if(loadHtmlSubVueFun("dev_vue/t_db_config_$.vue","AF8DA3_t_db_config_call_vue") == true){
		var n = Get_RandomDiv("BF8DA3","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BF8DA3_param["type"] = AF8DA3_type;
				BF8DA3_param["ly_index"]= index;
				if(AF8DA3_param.hasOwnProperty("hidden_find")){
					BF8DA3_param["hidden_find"] = "1";
				}
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
				
				/*Send Two FindSelect param end*/

				BF8DA3_clear_edit_info();
			},
			end: function(){
				BF8DA3_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AF8DA3_btn_t_db_config_edit").click(function () {
	if (AF8DA3_select_t_db_config_rowId != "") {
		AF8DA3_type = "edit";
		index_subhtml = "t_db_config_$.vue";
		if(loadHtmlSubVueFun("dev_vue/t_db_config_$.vue","AF8DA3_t_db_config_call_vue") == true){
			var n = Get_RandomDiv("BF8DA3","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BF8DA3_param["type"] = AF8DA3_type;
					BF8DA3_param["ly_index"] = index;
					if(AF8DA3_param.hasOwnProperty("hidden_find")){
						BF8DA3_param["hidden_find"] = "1";
					}
					BF8DA3_clear_edit_info();
					BF8DA3_get_edit_info();
				},
				end: function(){
					BF8DA3_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AF8DA3_btn_t_db_config_delete').click(function () {
	//单行选择
	var rowData = $("#AF8DA3_t_db_config_Events").bootstrapTable('getData')[AF8DA3_select_t_db_config_rowId];
	//多行选择
	var rowDatas = AF8DA3_sel_row_t_db_config();
	if (AF8DA3_select_t_db_config_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_db_config",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AF8DA3_N01_del_t_db_config");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_db_config",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AF8DA3_N01_del_t_db_config");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AF8DA3_N01_del_t_db_config(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_db_config) == true)
		AF8DA3_t_db_config_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function AF8DA3_adjust_tab(){
	if(typeof($("#AF8DA3_TAB_MAIN")[0]) != "undefined" && $("#AF8DA3_TAB_MAIN")[0].length != 0){
		AF8DA3_Tab_Flag = 1;
		$(Get_RDivNoBuild("AF8DA3","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("AF8DA3","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("AF8DA3","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#AF8DA3_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/
//隐藏tab页选项卡function AF8DA3_hide_tab_fun(){    var n = null;}//判断是否sub子项div页面function AF8DA3_Is_Sub_Div(temDivId){    if(temDivId.indexOf("XX$TTT") == 0)        return false;    return false;}
/*Tab Click Fun End*/

function AF8DA3_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("dev_vue/"+inputUrl,"AF8DA3_t_db_config_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (AF8DA3_select_t_db_config_rowId != "") 
			rowData = $("#AF8DA3_t_db_config_Events").bootstrapTable('getData')[AF8DA3_select_t_proc_name_rowId];
		else
			rowData = AF8DA3_rowCheckData;
		//$("#AF8DA3_t_db_config_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			var temFlag = true;
			$.each(temPar, function (i, obj) {
				if(eval(inputrandom+"_param.hasOwnProperty(\""+obj.target_id+"\")") && eval(inputrandom+"_param[\""+obj.target_id+"\"]").toString() == rowData[obj.sourc_id].toString())
					temFlag = false;
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			if(temFlag){
				//传递子页面隐藏find功能
				eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
				//参数传递并赋值
				eval(inputrandom+"_param_set()");
				var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
				$("#"+inputrandom+"_btn_"+tbName+"_query").click();
			}
		}
		else{
			$.each(temPar, function (i, obj) {
				if(obj.sourc_id.toUpperCase() == "MAIN_ID")
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"-999\"");
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"\"");
			});
			//传递子页面隐藏find功能
			eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
			var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
			$("#"+inputrandom+"_btn_"+tbName+"_query").click();		
		}
	}
}

//清除 查找框
function AF8DA3_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AF8DA3_sel_row_t_db_config(){
	//获得选中行
	var checkedbox= $("#AF8DA3_t_db_config_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}

//清除冗余数据
$("#del_truncate").click(function () {
    swal({
        title: "告警",
        text: "是否删除冗余数据",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "确定",
        closeOnConfirm: false
    	},
        function () {
        ly_index = layer.load();
        var inputdata = {
        	"param_name": "T01_truncate_table",
        	"session_id": session_id,
        	"login_id": login_id
        };
        ly_index = layer.load();
        get_ajax_staticurl(inputdata, "get_T01_truncate_table");
    });
});

//读取全局变量
$("#get_initpage").click(function () {
    swal({
        title: "告警",
        text: "是否读取全局变量信息",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "确定",
        closeOnConfirm: false
    	},
        function () {
        ly_index = layer.load();
        var inputdata = {
        	"param_name": "init_page",
        	"session_id": session_id,
        	"login_id": login_id
        };
        ly_index = layer.load();
        get_ajax_staticurl(inputdata, "get_tem_init_page");
    });
});

//操作结果
function get_tem_init_page(input){
	layer.close(ly_index);
	//查询失败
    if (Call_OpeResult(input.init_page) == false)
        return false;  
}

//操作结果
function get_T01_truncate_table(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_OpeResult(input.T01_truncate_table) == false)
        return false;  
}