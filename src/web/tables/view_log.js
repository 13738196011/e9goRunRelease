﻿var iTop = 0;
var ileft = 0;
var ly_index;
var param_path = "";

function biz_start(){
}

//接口名称
var mod_init_log = [{
        label: '序号',
        name: 'MAIN_ID',
        width: '30px',
        index: 'MAIN_ID',
        editable: false,
        key: true,
        readOnly: true,
        editrules: {
            required: true
        }
    }, {
        label: '类型',
        name: GetLowUpp('file_type'),
        width: '70px',
        editable: false,
        readOnly: true
    }, {
        label: '名称',
        name: GetLowUpp('file_name'),
        width: '50px',
        editable: false,
        readOnly: true
    }
];

function init_log(input) {
    data = input.init_log;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
    $("#table_list_1").jqGrid({
        data: data,
        datatype: "local",
        height: "450",
        autowidth: true,
        shrinkToFit: true,
        rowNum: 10,
        rowList: [10, 20, 30],
        colModel: mod_init_log,
        editurl: 'clientArray',
        altRows: true,
        pager: "#pager_list_1",
        viewrecords: true,
        caption: "日志列表&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' id= 'debug_cmd_name' value='设备调试'>",
        onSelectRow: function (rowid) {
            file_type = $('#table_list_1').jqGrid('getRowData', rowid)[GetLowUpp("file_type")];
            file_name = $('#table_list_1').jqGrid('getRowData', rowid)[GetLowUpp("file_name")];
            if (file_type == "日志文件")
                //iframe层-父子操作
                layer.open({
                    type: 2,
                    area: ['1100px', '600px'],
                    fixed: false, //不固定
                    maxmin: true,
                    content: '../log/' + file_name+'?session_id='+session_id
                });
            else
                swal("提醒", "请选择日志文件", "success");
        }
    });

    //设备调试
    $("#debug_cmd_name").click(function () {
        //iframe层-父子操作
        layer.open({
            type: 2,
            area: ['1100px', '600px'],
            fixed: false, //不固定
            maxmin: true,
            content: 'debug_cmd_name.html'
        });
    });

    //导航后面，增加增删改查按钮
    $("#table_list_1").jqGrid("navGrid", "#pager_list_1", {
        edit: false,
        add: false,
        del: false,
        search: false,
        view: false,
        position: "left",
        cloneToTop: false,
    }, {
        closeAfterSearch: true
    }, {
        height: 200,
        reloadAfterSubmit: true
    });
    layer.close(ly_index);
}

//初始化
function init() {
    iTop = (winHeight - 300) / 2;
    ileft = (winWidth - 300) / 2;
    ly_index = layer.load();
    $.jgrid.defaults.styleUI = "Bootstrap";
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "init_log",
            param_value1: param_path,
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "init_log",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

$(document).ready(function () {
    init_page();
    getWindowSize();
    init();
    $(window).bind("resize",
        function () {
        var width = $(".jqGrid_wrapper").width();
        $("#table_list_1").setGridWidth(width);
    });
});
