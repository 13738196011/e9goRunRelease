//按钮事件新增或编辑
var {$RBC$}_type = "";
//其他页面传到本页面参数
var {$RBC$}_param = {};
//暂时没用
var {$RBC$}_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

//主从表传递参数
function {$RBC$}_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function {$RBC$}_{template_table_EN}_biz_start(inputdata) {
	{$RBC$}_param = inputdata;
	layer.close(ly_index);
	{$RBC$}_param_set();
    /*biz begin*/
	
    /*biz end*/
}

/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function {$RBC$}_page_end(){
	if({$RBC$}_param["type"] == "edit"){
		{$RBC$}_get_edit_info();
	}
}

//页面初始化方法
function {$RBC$}_init_{template_table_EN}() {
	//type = getUrlParam("type");
	if({$RBC$}_param["type"] == "add"){
		$("#{$RBC$}_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
		
		/*Get Find Select param end*/	
	}
	else if({$RBC$}_param["type"] == "edit"){
	
	}
	
	//表单验证
	{$RBC$}_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
	
    /*form datetime init end*/
	
	{$RBC$}_page_end();
}

//提交表单数据
function {$RBC$}_SubmitForm(){
	if({$RBC$}_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_{template_table_EN}",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "{$RBC$}_get_N01_ins_{template_table_EN}");
	}
	else if({$RBC$}_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_{template_table_EN}",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "{$RBC$}_get_N01_upd_{template_table_EN}");
	}
}

//vue回调
function {$RBC$}_{template_table_EN}_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#{$RBC$}_save_{template_table_EN}_Edit").click(function () {
	$("form[name='{$RBC$}_DataModal']").submit();
})

/*修改数据*/
function {$RBC$}_get_N01_upd_{template_table_EN}(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_{template_table_EN}) == true)
	{
		swal("修改数据成功!", "", "success");
		{$RDA$}_{template_table_EN}_query();
		{$RBC$}_clear_validate();
		layer.close({$RBC$}_param["ly_index"]);
	}
}

/*添加数据*/
function {$RBC$}_get_N01_ins_{template_table_EN}(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_{template_table_EN}) == true)
	{
		swal("添加数据成功!", "", "success");
		{$RDA$}_{template_table_EN}_query();
		{$RBC$}_clear_validate();
		layer.close({$RBC$}_param["ly_index"]);
	}
}

//取消编辑
$("#{$RBC$}_cancel_{template_table_EN}_Edit").click(function () {
	layer.close({$RBC$}_param["ly_index"]);
	{$RBC$}_clear_validate();
	$("[id^='{$RBC$}_div']").hide();
})

//清除查找框
function {$RBC$}_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function {$RBC$}_clear_validate(){
	$("#{$RBC$}_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#{$RBC$}_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#{$RBC$}_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function {$RBC$}_clear_edit_info(){
	var inputs = $("#{$RBC$}_DataModal").find('input');
	var selects = $("#{$RBC$}_DataModal").find("select");
	var textareas = $("#{$RBC$}_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	{$RBC$}_init_{template_table_EN}();
}

//页面输入框赋值
function {$RBC$}_get_edit_info(){
	var rowData = $("#{$RDA$}_{template_table_EN}_Events").bootstrapTable('getData')[{$RDA$}_select_{template_table_EN}_rowId];
	var inputs = $("#{$RBC$}_DataModal").find('input');
	var selects = $("#{$RBC$}_DataModal").find("select");
	var textareas = $("#{$RBC$}_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"{$RDA$}","{$RBC$}");
}

//form验证
function {$RBC$}_checkFormInput() {
    {$RBC$}_validate = $("#{$RBC$}_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	{$RBC$}_SubmitForm();
        	return false;
        }
    })
}