#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class menu_info(object):
    attMAIN_ID = -1
    attGROUP_ID = 0
    attMENU_URL = ""
    attMENU_NAME = ""
    attIS_AVA = 0
    attROLE_ID = ""
    attCREATE_DATE = None
    attS_DESC = ""
    lenMENU_URL = 50
    lenMENU_NAME = 50
    lenROLE_ID = 500
    lenS_DESC = 500
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["MAIN_ID"] = "主键"
        temHt["GROUP_ID"] = "菜单组"
        temHt["MENU_URL"] = "菜单url"
        temHt["MENU_NAME"] = "菜单名称"
        temHt["IS_AVA"] = "是否启用"
        temHt["ROLE_ID"] = "角色"
        temHt["CREATE_DATE"] = "系统时间"
        temHt["S_DESC"] = "备注"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["MENU_NAME"] = "菜单名称"
        temHtQry["IS_AVA"] = "是否启用"
        temHtQry["ROLE_ID"] = "角色"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["GROUP_ID"] = "select MAIN_ID,group_cn_name || group_en_name as CN_NAME from menu_group"
        temHtSelect["IS_AVA"] = [{"MAIN_ID":"1","CN_NAME":"启用"},{"MAIN_ID":"0","CN_NAME":"禁用"}]
        temHtSelect["ROLE_ID"] = [{"MAIN_ID":"1","CN_NAME":"开发者"},{"MAIN_ID":"2","CN_NAME":"系统管理员"},{"MAIN_ID":"3","CN_NAME":"运维人员"},{"MAIN_ID":"4","CN_NAME":"普通用户"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        temHtFind["GROUP_ID"] = "menu_group.vue"
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        temHtFReturn["GROUP_ID"] = ["MAIN_ID","GROUP_CN_NAME"]
        return temHtFReturn
