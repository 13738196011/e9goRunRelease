#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_db_config(object):
    attmain_id = -1
    attDB_CN_NAME = ""
    attDB_DRiverClassName = ""
    attDB_url = ""
    attDB_username = ""
    attDB_password = ""
    attDB_version = ""
    attDB_code = ""
    attS_DESC = ""
    attCREATE_DATE = None
    lenDB_CN_NAME = 50
    lenDB_DRiverClassName = 50
    lenDB_url = 200
    lenDB_username = 50
    lenDB_password = 50
    lenDB_version = 50
    lenDB_code = 50
    lenS_DESC = 50
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["DB_CN_NAME"] = "子系统名称"
        temHt["DB_DRiverClassName"] = "子系统驱动"
        temHt["DB_url"] = "子系统url"
        temHt["DB_username"] = "子系统登录账号"
        temHt["DB_password"] = "子系统登录密码"
        temHt["DB_version"] = "子系统版本"
        temHt["DB_code"] = "子系统编码"
        temHt["S_DESC"] = "备注"
        temHt["CREATE_DATE"] = "系统时间"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
