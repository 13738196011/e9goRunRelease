var type = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var ary_DB_ID = null;var ary_INF_TYPE = [{'main_id': '1', 'cn_name': '存储过程'}, {'main_id': '2', 'cn_name': 'SQL语句'}];var ary_IS_AUTHORITY = [{'main_id': '1', 'cn_name': '是'}, {'main_id': '0', 'cn_name': '否'}];
/*declare select options end*/

//业务逻辑数据开始
function biz_start() {
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "T01_t_proc_name$DB_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "get_T01_t_proc_name$DB_ID");	
    /*biz end*/
}

/*biz step begin*/
function format_DB_ID(value, row, index) {    var objResult = value;    for(i = 0; i < ary_DB_ID.length; i++) {        var obj = ary_DB_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function format_INF_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < ary_INF_TYPE.length; i++) {        var obj = ary_INF_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function format_IS_AUTHORITY(value, row, index) {    var objResult = value;    for(i = 0; i < ary_IS_AUTHORITY.length; i++) {        var obj = ary_IS_AUTHORITY[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function get_T01_t_proc_name$DB_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_t_proc_name$DB_ID) == false)        return false;    ary_DB_ID = input.T01_t_proc_name$DB_ID;    if($("#DB_ID").is("select") && $("#DB_ID")[0].options.length == 0)    {        $.each(ary_DB_ID, function (i, obj) {            addOptionValue("DB_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#INF_TYPE").is("select") && $("#INF_TYPE")[0].options.length == 0)    {        $.each(ary_INF_TYPE, function (i, obj) {            addOptionValue("INF_TYPE", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#IS_AUTHORITY").is("select") && $("#IS_AUTHORITY")[0].options.length == 0)    {        $.each(ary_IS_AUTHORITY, function (i, obj) {            addOptionValue("IS_AUTHORITY", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    init_t_proc_name();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function page_end(){
}


//页面初始化方法
function init_t_proc_name() {
	type = getUrlParam("type");
	if(type == "add"){
		$("#main_id").parent().parent().parent().hide();
	}
	else if(type == "edit"){
	
	}
	
	//表单验证
	checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#CREATE_DATE").val() == "")    {        $("#CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
}

//提交表单数据
function SubmitForm(){
	if(type == "add"){
		var inputdata = {
				"param_name": "T01_ins_t_proc_name",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#DB_ID").val()                ,"param_value2": s_encode($("#INF_CN_NAME").val())                ,"param_value3": s_encode($("#INF_EN_NAME").val())                ,"param_value4": s_encode($("#INF_EN_SQL").val())                ,"param_value5": $("#INF_TYPE").val()                ,"param_value6": s_encode($("#S_DESC").val())                ,"param_value7": $("#CREATE_DATE").val()                ,"param_value8": s_encode($("#REFLECT_IN_CLASS").val())                ,"param_value9": s_encode($("#REFLECT_OUT_CLASS").val())                ,"param_value10": $("#IS_AUTHORITY").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_ins_t_proc_name");
	}
	else if(type == "edit"){
		var inputdata = {
				"param_name": "T01_upd_t_proc_name",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#DB_ID").val()                ,"param_value2": s_encode($("#INF_CN_NAME").val())                ,"param_value3": s_encode($("#INF_EN_NAME").val())                ,"param_value4": s_encode($("#INF_EN_SQL").val())                ,"param_value5": $("#INF_TYPE").val()                ,"param_value6": s_encode($("#S_DESC").val())                ,"param_value7": $("#CREATE_DATE").val()                ,"param_value8": s_encode($("#REFLECT_IN_CLASS").val())                ,"param_value9": s_encode($("#REFLECT_OUT_CLASS").val())                ,"param_value10": $("#IS_AUTHORITY").val()                ,"param_value11": $("#main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_upd_t_proc_name");
	}
}

//以下为页面表单验证
$("#save_t_proc_name_Edit").click(function () {
	$("form[name='DataModal']").submit();
})

/*修改数据*/
function get_T01_upd_t_proc_name(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_upd_t_proc_name) == true)
	{
        //init();
		parent.swal("修改数据成功!", "", "success");
		parent.t_proc_name_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

/*添加数据*/
function get_T01_ins_t_proc_name(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_ins_t_proc_name) == true)
	{
		parent.swal("添加数据成功!", "", "success");
		parent.t_proc_name_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

//取消编辑
$("#cancel_t_proc_name_Edit").click(function () {
	var index = parent.layer.getFrameIndex(window.name);
	parent.layer.close(index);
})

function clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

$(document).ready(function () {
});

window.onpageshow = function (e) {
    if (e.persisted || (window.performance.navigation.type == 2)) {
        is_history_back = 1;
    } else {
        is_history_back = 0;
    }
    //页面初始化
    init_page();
};

//form验证
function checkFormInput() {
    $("#DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            main_id: {}            ,DB_ID: {}            ,INF_CN_NAME: {}            ,INF_EN_NAME: {}            ,INF_EN_SQL: {}            ,INF_TYPE: {}            ,S_DESC: {}            ,CREATE_DATE: {date: true,required : true,maxlength:19}            ,REFLECT_IN_CLASS: {}            ,REFLECT_OUT_CLASS: {}            ,IS_AUTHORITY: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            main_id: {}            ,DB_ID: {}            ,INF_CN_NAME: {}            ,INF_EN_NAME: {}            ,INF_EN_SQL: {}            ,INF_TYPE: {}            ,S_DESC: {}            ,CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,REFLECT_IN_CLASS: {}            ,REFLECT_OUT_CLASS: {}            ,IS_AUTHORITY: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	SubmitForm();
        	return false;
        }
    })
}