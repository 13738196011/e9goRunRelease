//按钮事件新增或编辑
var BE339B_type = "";
//其他页面传到本页面参数
var BE339B_param = {};
//暂时没用
var BE339B_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BE339B_ary_file_id = null;var BE339B_ary_is_ava = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];var BE339B_ary_date_type = [{'main_id': '1', 'cn_name': '年'}, {'main_id': '2', 'cn_name': '月'}, {'main_id': '3', 'cn_name': '日'}];
/*declare select options end*/

//业务逻辑数据开始
function BE339B_t_file_expand_biz_start(inputdata) {
	BE339B_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_file_expand$file_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BE339B_get_N01_t_file_expand$file_id");	
    /*biz end*/
}

/*biz step begin*/
function BE339B_format_file_id(value, row, index) {    var objResult = value;    for(i = 0; i < BE339B_ary_file_id.length; i++) {        var obj = BE339B_ary_file_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BE339B_format_is_ava(value, row, index) {    var objResult = value;    for(i = 0; i < BE339B_ary_is_ava.length; i++) {        var obj = BE339B_ary_is_ava[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BE339B_format_date_type(value, row, index) {    var objResult = value;    for(i = 0; i < BE339B_ary_date_type.length; i++) {        var obj = BE339B_ary_date_type[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BE339B_get_N01_t_file_expand$file_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_file_expand$file_id) == false)        return false;    BE339B_ary_file_id = input.N01_t_file_expand$file_id;    if($("#BE339B_file_id").is("select") && $("#BE339B_file_id")[0].options.length == 0)    {        $.each(BE339B_ary_file_id, function (i, obj) {            addOptionValue("BE339B_file_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#BE339B_is_ava").is("select") && $("#BE339B_is_ava")[0].options.length == 0)    {        $.each(BE339B_ary_is_ava, function (i, obj) {            addOptionValue("BE339B_is_ava", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#BE339B_date_type").is("select") && $("#BE339B_date_type")[0].options.length == 0)    {        $.each(BE339B_ary_date_type, function (i, obj) {            addOptionValue("BE339B_date_type", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    BE339B_init_t_file_expand();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function BE339B_page_end(){
	if(BE339B_param["type"] == "edit"){
		BE339B_get_edit_info();
	}
}

//页面初始化方法
function BE339B_init_t_file_expand() {
	//type = getUrlParam("type");
	if(BE339B_param["type"] == "add"){
		$("#BE339B_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BE339B_date_begin").val(BE339B_param["date_begin"]);		
		/*Get Find Select param end*/	
	}
	else if(BE339B_param["type"] == "edit"){
	
	}
	
	//表单验证
	BE339B_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BE339B_date_begin").val() == "")    {        $("#BE339B_date_begin").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BE339B_date_begin',        type: 'datetime',        trigger: 'click'    });    if($("#BE339B_date_end").val() == "")    {        $("#BE339B_date_end").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BE339B_date_end',        type: 'datetime',        trigger: 'click'    });    if($("#BE339B_create_date").val() == "")    {        $("#BE339B_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BE339B_create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BE339B_page_end();
}

//提交表单数据
function BE339B_SubmitForm(){
	if(BE339B_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_file_expand",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#BE339B_file_id").val()                ,"param_value2": $("#BE339B_date_type").val()                ,"param_value3": $("#BE339B_date_begin").val()                ,"param_value4": $("#BE339B_date_end").val()                ,"param_value5": $("#BE339B_is_ava").val()                ,"param_value6": $("#BE339B_create_date").val()                ,"param_value7": s_encode($("#BE339B_s_desc").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BE339B_get_N01_ins_t_file_expand");
	}
	else if(BE339B_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_file_expand",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#BE339B_file_id").val()                ,"param_value2": $("#BE339B_date_type").val()                ,"param_value3": $("#BE339B_date_begin").val()                ,"param_value4": $("#BE339B_date_end").val()                ,"param_value5": $("#BE339B_is_ava").val()                ,"param_value6": $("#BE339B_create_date").val()                ,"param_value7": s_encode($("#BE339B_s_desc").val())                ,"param_value8": $("#BE339B_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BE339B_get_N01_upd_t_file_expand");
	}
}

//vue回调
function BE339B_t_file_expand_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#BE339B_save_t_file_expand_Edit").click(function () {
	$("form[name='BE339B_DataModal']").submit();
})

/*修改数据*/
function BE339B_get_N01_upd_t_file_expand(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_file_expand) == true)
	{
		swal("修改数据成功!", "", "success");
		AE339B_t_file_expand_query();
		BE339B_clear_validate();
		layer.close(BE339B_param["ly_index"]);
	}
}

/*添加数据*/
function BE339B_get_N01_ins_t_file_expand(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_file_expand) == true)
	{
		swal("添加数据成功!", "", "success");
		AE339B_t_file_expand_query();
		BE339B_clear_validate();
		layer.close(BE339B_param["ly_index"]);
	}
}

//取消编辑
$("#BE339B_cancel_t_file_expand_Edit").click(function () {
	layer.close(BE339B_param["ly_index"]);
	BE339B_clear_validate();
	$("[id^='BE339B_div']").hide();
})

//清除查找框
function BE339B_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BE339B_clear_validate(){
	$("#BE339B_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BE339B_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BE339B_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BE339B_clear_edit_info(){
	var inputs = $("#BE339B_DataModal").find('input');
	var selects = $("#BE339B_DataModal").find("select");
	var textareas = $("#BE339B_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	BE339B_init_t_file_expand();
}

//页面输入框赋值
function BE339B_get_edit_info(){
	var rowData = $("#AE339B_t_file_expand_Events").bootstrapTable('getData')[AE339B_select_t_file_expand_rowId];
	var inputs = $("#BE339B_DataModal").find('input');
	var selects = $("#BE339B_DataModal").find("select");
	var textareas = $("#BE339B_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"AE339B","BE339B");
	
	/*
	$.each(selects, function (i, obj) {
		if(typeof(eval("AE339B_ary_"+obj.id.toString().replace("BE339B_",""))) == "object")
		{
			$.each(eval("AE339B_ary_"+obj.id.toString().replace("BE339B_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE339B_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("BE339B_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("AE339B_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#BE339B_DataModal").find('[id="'+"BE339B_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE339B_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE339B_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});*/
}

//form验证
function BE339B_checkFormInput() {
    BE339B_validate = $("#BE339B_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BE339B_main_id: {}            ,BE339B_file_id: {}            ,BE339B_date_type: {}            ,BE339B_date_begin: {date: true,required : true,maxlength:19}            ,BE339B_date_end: {date: true,required : true,maxlength:19}            ,BE339B_is_ava: {}            ,BE339B_create_date: {date: true,required : true,maxlength:19}            ,BE339B_s_desc: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BE339B_main_id: {}            ,BE339B_file_id: {}            ,BE339B_date_type: {}            ,BE339B_date_begin: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BE339B_date_end: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BE339B_is_ava: {}            ,BE339B_create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BE339B_s_desc: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BE339B_SubmitForm();
        	return false;
        }
    })
}