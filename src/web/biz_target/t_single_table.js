//选择某一行
var select_t_single_table_rowId = "";

/*定义查询条件变量*/
/*declare query param begin*/
var tem_table_cn_name = "";var tem_table_en_name = "";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var ary_table_type = [{"MAIN_ID":"0","CN_NAME":"表"},{"MAIN_ID":"1","CN_NAME":"视图"}];

/*declare select options end*/

//业务逻辑数据开始
function biz_start() {
	layer.close(ly_index);
    /*biz begin*/
    init_t_single_table()	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/

function format_TABLE_TYPE(value, row, index) {
    var objResult = value;
    for(i = 0; i < ary_table_type.length; i++) {
        var obj = ary_table_type[i];
        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {
            objResult = obj[GetLowUpp("cn_name")];
            break;
        }
    }
    return objResult;
}

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function page_end(){
}

//元数据表管理显示列定义
var t_single_table = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '表中文名',        field: 'TABLE_CN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '表英文名',        field: 'TABLE_EN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true
        ,formatter: set_time_decode    },    {        title: '系统备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },
    {
        title: '表或视图',
        field: 'TABLE_TYPE',
        sortable: true
        ,formatter: format_TABLE_TYPE
    }
	/*table column end*/
];

//页面初始化
function init_t_single_table() {
	$(window).resize(function () {
		  $('#t_single_table_Events').bootstrapTable('resetView');
	});
	//元数据表管理查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#btn_t_single_table_query').click();
}

//查询接口
function t_single_table_query() {
    $('#t_single_table_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "T01_sel_t_single_table",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(tem_table_cn_name)        ,"param_value2": s_encode(tem_table_en_name)		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "get_T01_sel_t_single_table");
}

//查询结果
function get_T01_sel_t_single_table(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.T01_sel_t_single_table) == false)
        return false;
    $.each(t_single_table, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
	var s_data = input.T01_sel_t_single_table;
    $('#t_single_table_Events').bootstrapTable('destroy');
    $("#t_single_table_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#t_single_table_Events').find("tr.changeColor").removeClass('changeColor');
                select_t_single_table_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#t_single_table_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                select_t_single_table_rowId = $element.attr('data-index');
				//查找子表信息
				var rowData = $("#t_single_table_Events").bootstrapTable('getData')[select_t_single_table_rowId];
				$("#find_table_id_cn_name").val(rowData["TABLE_EN_NAME"]);
				$("#find_table_id").val(rowData["MAIN_ID"]);
				$('#btn_t_table_column_query').click();
            }
        },
		onDblClickRow: function (row){
			if(getUrlParam("target_name") != null)
			{
				/*查找框返回值*/
				parent.$("#"+getUrlParam("target_id")).val(eval("row."+getUrlParam("sourc_id").toUpperCase()));
				parent.$("#"+getUrlParam("target_name")).val(eval("row."+getUrlParam("sourc_name").toUpperCase()));			
				var index = parent.layer.getFrameIndex(window.name);
				parent.layer.close(index);
			}
		},
        toolbar: "#t_single_table_Toolbar",
        columns: t_single_table,
        data: s_data,
        pageNumber: 1,
        pageSize: 5, // 每页的记录行数（*）
        pageList: [30, 100, 500, 2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    $("#t_table_column_tab_1").click();
    
	t_table_column_biz_start();
}

//重置业务结果
function get_T01_YY77HH(input){
	layer.close(ly_index);
	//操作失败
    if (Call_OpeResult(input.T01_YY77HH) == false)
        return false;    
}

//创建业务结果
function get_T01_YYNNKK(input){
	layer.close(ly_index);
	//操作失败
    if (Call_OpeResult(input.T01_YYNNKK) == false)
        return false;  
}

//重置业务
$('#btn_t_single_table_reset').click(function () {
	if (select_t_single_table_rowId == "") {
		swal({
            title: "提示信息",
            text: "无法重置业务，请选择正确记录信息!"
        });
	}
	else{
		swal({
				title: "告警",
				text: "确认要重置业务吗?所有数据及页面将删除且无法恢复!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "确定",
				closeOnConfirm: false
			},
			function () {
				var rowData = $("#t_single_table_Events").bootstrapTable('getData')[select_t_single_table_rowId];
				var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "T01_YY77HH",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "get_T01_YY77HH");
		});
	}
});

//创建生成
$('#btn_t_single_table_build').click(function () {
	if (select_t_single_table_rowId == "") {
		swal({
            title: "提示信息",
            text: "无法创建业务，请选择正确记录信息!"
        });
	}
	else{
		var rowData = $("#t_single_table_Events").bootstrapTable('getData')[select_t_single_table_rowId];
		var select_main_id = rowData["MAIN_ID"];
		var inputdata = {
			"param_name": "T01_YYNNKK",
			"session_id": session_id,
			"login_id": login_id,
			"param_value1": select_main_id,
			"param_value2": $("#file_folder").val()
		};
		ly_index = layer.load();
		get_ajax_baseurl(inputdata, "get_T01_YYNNKK");
	}
});

//查询按钮
$('#btn_t_single_table_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    tem_table_cn_name = $("#qry_table_cn_name").val();    tem_table_en_name = $("#qry_table_en_name").val();	
    /*set query param end*/
	t_single_table_query();
})

//新增按钮
$("#btn_t_single_table_add").click(function () {
	layer.open({
		type: 2,
        area: ['1100px', '600px'],
        fixed: false, //不固定
        maxmin: true,
        content: 't_single_table_$.html?type=add'
    });
})

//编辑按钮
$("#btn_t_single_table_edit").click(function () {
	if (select_t_single_table_rowId != "") {
		layer.open({
			type: 2,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: 't_single_table_$.html?type=edit',
			success: function(layero, index){
				var body = layer.getChildFrame('body',index);
				var rowData = $("#t_single_table_Events").bootstrapTable('getData')[select_t_single_table_rowId];
				var inputs = body.contents().find("#DataModal").find('input');
				var selects = body.contents().find("#DataModal").find("select");				
				var textareas = body.contents().find("#DataModal").find('textarea');
				$.each(selects, function (i, obj) {
					if(typeof(eval("ary_"+obj.id)) == "object")
					{
						$.each(eval("ary_"+obj.id), function (inx, obj_sub) {
							var div = "<option value='" + obj_sub[GetLowUpp("main_id")] + "'>" + obj_sub[GetLowUpp("cn_name")] + "</option>";
							$(obj).append(div);
						});
					}
				});
				
				Object.keys(rowData).forEach(function (key) {
					$.each(inputs, function (i, obj) {
						if (obj.id.toUpperCase() == key.toUpperCase()) {
							if(typeof(rowData[key]) == "string")
							{
								$(obj).val(s_decode(rowData[key]));
							}
							else
							{
								$(obj).val(rowData[key]);
							}
						}
						else if(obj.id.toUpperCase() == ("find_"+key+"_cn_name").toUpperCase()){
							$.each(eval("ary_"+key.toLowerCase()), function (jindex, obj2) {
								if(obj2[GetLowUpp("main_id")] == rowData[key]){
									body.contents().find("#DataModal").find('[id="'+("find_"+key.toLowerCase()+"_cn_name").toLowerCase()+'"]').val(obj2[GetLowUpp("cn_name")]);
								}
							});
						}
					});
					$.each(selects, function (i, obj) {
						if (obj.id.toUpperCase() == key.toUpperCase()) {
							$(obj).val(rowData[key]);
						}
					});
					$.each(textareas, function (i, obj) {
						if (obj.id.toUpperCase() == key.toUpperCase()) {
							$(obj).val(s_decode(rowData[key]));
						}
					});
				});
			}
		});
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#btn_t_single_table_delete').click(function () {
	var rowData = $("#t_single_table_Events").bootstrapTable('getData')[select_t_single_table_rowId];		
	var rowDatas = sel_row_t_single_table();
    if (select_t_single_table_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
			if(rowDatas.length > 0){
				$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "T01_del_t_single_table",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "T01_del_t_single_table");
				});
			}
			else{
				var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "T01_del_t_single_table",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "T01_del_t_single_table");
			}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function T01_del_t_single_table(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.T01_del_t_single_table) == true)
		t_single_table_query();
}

//数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//清除 查找框
function clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function sel_row_t_single_table(){
	//获得选中行
	var checkedbox= $("#t_single_table_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);	 
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	return jsonObject;
}

$(document).ready(function () {
});

window.onpageshow = function (e) {
    if (e.persisted || (window.performance.navigation.type == 2)) {
        is_history_back = 1;
    } else {
        is_history_back = 0;
    }
    //页面初始化
    init_page();
};