﻿var iTop = 0;
var ileft = 0;
var ly_index;

function biz_start(){
}

// 综合应用平台信息
var cal_variables = [{
        label: '主键',
        name: 'MAIN_ID',
        width: '30px',
        index: 'MAIN_ID',
        editable: false,
        key: true,
        readOnly: true,
        editrules: {
            required: true
        }
    }, {
        label: '变量英文定义',
        name: 'VAR_NAME',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '变量属性',
        name: 'VAR_KEY',
        width: '70px',
        editable: true,
        editrules: true,
    },{
        label: '变量分类',
        name: 'VAR_TYPE',
        width: '70px',
        editable: true,
        editrules: true,
    },{
        label: '变量描述',
        name: 'VAR_DESC',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '系统时间',
        name: 'CREATE_DATE',
        width: '70px',
        editable: true,
        editrules: true,
        readOnly: true,
        formatter: function (value, row) {
            return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
        },
        editoptions: {
            defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')
        }
    },{
        label: '备注',
        name: 'S_DESC',
        width: '70px',
        editable: true,
        editrules: true
    }
];

function T01_sel_cal_variables(input) {
    data = input.T01_sel_cal_variables;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
    var total = $("#table_list_1").jqGrid('getGridParam', 'records')
        if (typeof(total) == "undefined") {
            $("#table_list_1").jqGrid({
                data: data,
                datatype: "local",
                height: "450",
                autowidth: true,
                shrinkToFit: true,
                rowNum: 10,
                rowList: [10, 20, 30],
                colModel: cal_variables,
                editurl: 'clientArray',
                altRows: true,
                pager: "#pager_list_1",
                viewrecords: true,
                caption: "变量定义表",
                // rownumbers:true,
                gridview: true
            });

            // 导航后面，增加增删改查按钮
            $("#table_list_1").jqGrid("navGrid", "#pager_list_1", {
                edit: true,
                add: true,
                del: true,
                search: true,
                view: false,
                position: "left",
                cloneToTop: false
            }, {
                beforeShowForm: function () {
                    $('#MAIN_ID').attr('readOnly', true);
                    laydate.render({
                        elem: '#CREATE_DATE',
                        type: 'datetime'
                    });
                },
                onclickSubmit: updateDbByMainId,
                closeAfterEdit: true
            }, {
                beforeShowForm: function () {
                    laydate.render({
                        elem: '#CREATE_DATE',
                        type: 'datetime'
                    });
                },
                onclickSubmit: insertDb,
                closeAfterAdd: true
            }, {
                top: iTop,
                left: ileft,
                onclickSubmit: delDbByMainId
            }, {
                closeAfterSearch: true
            }, {
                height: 200,
                reloadAfterSubmit: true
            });
        } else {
            $("#table_list_1").jqGrid("clearGridData");
            $("#table_list_1").jqGrid('setGridParam', {
                data: data
            }).trigger("reloadGrid");
        }
        layer.close(ly_index);
}

// 初始化
function init() {
    ly_index = layer.load();
    iTop = (winHeight - 300) / 2;
    ileft = (winWidth - 300) / 2;
    $.jgrid.defaults.styleUI = "Bootstrap";
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_cal_variables",
            session_id: session_id
        },
        // 跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_cal_variables",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

$(document).ready(function () {
    init_page();
    getWindowSize();
    init();
    $(window).bind("resize",
        function () {
        var width = $(".jqGrid_wrapper").width();
        $("#table_list_1").setGridWidth(width);
    });
});

function T01_ins_cal_variables(input) {
    data = input.T01_ins_cal_variables;
	layer.close(ly_index);
    //查询失败
    if (Call_OpeResult(data) == false)
        return false;
    init();
}

var insertDb = function () {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_ins_cal_variables",
            session_id: session_id,
            param_value1: $("#VAR_NAME").val(),
            param_value2: $("#VAR_KEY").val(),
            param_value3: $("#VAR_TYPE").val(),
            param_value4: $("#VAR_DESC").val(),
            param_value5: $("#CREATE_DATE").val(),
            param_value6: $("#S_DESC").val()
        },
        // 跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_ins_cal_variables",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_upd_cal_variables(input) {
    data = input.T01_upd_cal_variables;
	layer.close(ly_index);
    //查询失败
    if (Call_OpeResult(data) == false)
        return false;
    init();
}

var updateDbByMainId = function () {
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid).MAIN_ID;
    if (rowid.indexOf("jqg") != -1 || rowData.indexOf("jqg") != -1) {
        swal({
            title: "提示信息",
            text: "无法修改缓存记录，请选择正确修改!"
        });
        return false;
    }
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_upd_cal_variables",
            session_id: session_id,
            param_value1: $("#VAR_NAME").val(),
            param_value2: $("#VAR_KEY").val(),
            param_value3: $("#VAR_TYPE").val(),
            param_value4: $("#VAR_DESC").val(),
            param_value5: $("#CREATE_DATE").val(),
            param_value6: $("#S_DESC").val(),
            param_value7: rowData
        },
        // 跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_upd_cal_variables",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_del_cal_variables(input) {
    data = input.T01_del_cal_variables;
	layer.close(ly_index);
    //操作失败
    if (Call_OpeResult(data) == false)
        return false;
    layer.close(ly_index);
    init();
}

var delDbByMainId = function () {
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid).MAIN_ID;
    if (rowid.indexOf("jqg") != -1 || rowData.indexOf("jqg") != -1) {
        swal({
            title: "提示信息",
            text: "无法删除缓存记录，请选择正确删除!"
        });
        return false;
    }
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_del_cal_variables",
            session_id: session_id,
            param_value1: rowData
        },
        // 跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_del_cal_variables",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};