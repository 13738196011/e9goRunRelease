#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import importlib
from com.zxy.business.Query_Data import Query_Data
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.model.Return_Value import Return_Value
from com.zxy.tcp.ServerHandlerHttp import ServerHandlerHttp
from com.zxy.z_debug import z_debug
class Analyse_Data(z_debug):
    attTemChildNode = ""
    def __init__(self):
        pass
    def GetPackageInfo(self,inputStrInfo):
        ht = None
        for temStrs in inputStrInfo.replace("@##","").replace("##@","").split("&"):
            Com_Fun.SetHashTable(ht,temStrs.split("=")[0],temStrs.split("=")[1])
        return ht
    def SubAnalyseRecBytes(self,inputStrResult, inputMServSocket, inputIServ, inputStrIP,inputStrPort):
        if inputIServ and Com_Para.ServerREFLECT_IN_CLASS != "":
            try:
                objC = importlib.import_module(Com_Para.ServerREFLECT_IN_CLASS)  #对模块进行导入                
                objName = Com_Para.ServerREFLECT_IN_CLASS.split(".")
                objN = getattr(objC,objName[len(objName) - 1])
                if hasattr(objN,"strResult"):
                    setattr(objN,"strResult",inputStrResult)
                    setattr(objN,"strIP",inputStrIP)
                    setattr(objN,"strPort",inputStrPort)
                    fun_us = getattr(objN,"init_start")
                    fun_us(objN)
                    temResult = getattr(objN,"strResult")
                    temSend = getattr(objN,"strSend")
                    temContinue = getattr(objN,"strContinue")
                    if temSend != "":
                        Com_Fun.SendSocket(temSend,inputMServSocket)
                    if temContinue == "0":
                        return ""
            except Exception as e:
                if str(type(self)) == "<class 'type'>":
                    self.debug_in(self,Com_Para.ServerREFLECT_IN_CLASS+"=1=>"+repr(e))#打印异常信息
                else:
                    self.debug_in(Com_Para.ServerREFLECT_IN_CLASS+"=1=>"+repr(e))#打印异常信息
            finally:
                pass
        elif not inputIServ and Com_Para.ClientREFLECT_IN_CLASS != "":
            try:
                objC = importlib.import_module(Com_Para.ClientREFLECT_IN_CLASS)  #对模块进行导入                
                objName = Com_Para.ClientREFLECT_IN_CLASS.split(".")
                objN = getattr(objC,objName[len(objName) - 1])
                if hasattr(objN,"strResult"):
                    setattr(objN,"strResult",inputStrResult)
                    setattr(objN,"strIP",inputStrIP)
                    setattr(objN,"strPort",inputStrPort)
                    fun_us = getattr(objN,"init_start")
                    fun_us(objN)
                    temResult = getattr(objN,"strResult")
                    temSend = getattr(objN,"strSend")
                    temContinue = getattr(objN,"strContinue")
                    if temSend != "":
                        Com_Fun.SendSocket(temSend,inputMServSocket)
                    if temContinue == "0":
                        return ""
            except Exception as e:
                if str(type(self)) == "<class 'type'>":
                    self.debug_in(self,Com_Para.ServerREFLECT_IN_CLASS+"=2=>"+repr(e))#打印异常信息
                else:
                    self.debug_in(Com_Para.ServerREFLECT_IN_CLASS+"=2=>"+repr(e))#打印异常信息
            finally:
                pass
        elif inputIServ:    
            temStrUrl = ""            
            if self.strResult.find("\\\\r\\\\n") != -1 and  self.strResult.find("\\\\r\\\\n") != len(self.strResult) - 4:
                strA = self.strResult.split("\\r\\n")
            elif self.strResult.find("\\r\\n") != -1 and  self.strResult.find("\\r\\n") != len(self.strResult) - 2:
                strA = self.strResult.split("\r\n")
            else:
                strA = self.strResult.split("\r\n")
            for temStrAryT in strA:
                if temStrAryT == "" :
                    continue
                temStrAryT = temStrAryT.replace("\r","").replace("\n","")            
                if len(temStrAryT) > 3 and temStrAryT.find("@##") == 0 and temStrAryT.find("##@") == len(temStrAryT) - 3:
                    if temStrAryT.find("@##ST=04") == 0:
                        temSHH = ServerHandlerHttp()
                        attReturn_Value = Return_Value()
                        temGd = Query_Data(attReturn_Value)
                        temStrUrl = temStrAryT.replace("@##","").replace("##@","")
                        temResult = temSHH.Center_Data_Rel(temStrUrl,"","",temGd,"",inputMServSocket.getpeername()[0])                        
                        temResult = "@##ST=03&CHILD_NODE=" + Com_Para.Child_Code+ "&VALUE=" + temResult + "##@\r\n"
                        Com_Fun.SendSocket(temResult,inputMServSocket)
                elif len(temStrAryT) > 3 and temStrAryT.find("@##") == 0 and temStrAryT.find("##@") == -1:
                    temStrUrl = temStrAryT
                elif len(temStrAryT) > 3 and temStrAryT.find("@##") == -1 and temStrAryT.find("##@") == len(temStrAryT) - 3:
                    temStrUrl += temStrAryT
                    if temStrUrl.find("@##ST=04") == 0:
                        temSHH = ServerHandlerHttp()
                        attReturn_Value = Return_Value()
                        temGd = Query_Data(attReturn_Value)
                        temStrUrl = temStrUrl.replace("@##","").replace("##@","")
                        temResult = temSHH.Center_Data_Rel(temStrUrl,"","",temGd,"",inputMServSocket.getpeername()[0])
                        temResult = "@##ST=03&CHILD_NODE=" + Com_Para.Child_Code+ "&VALUE=" + temResult + "##@\r\n"
                        Com_Fun.SendSocket(temResult,inputMServSocket)
                elif temStrAryT != "" and temStrAryT.find("@##") == -1 and temStrAryT.find("##@") == -1:
                    temStrUrl += temStrAryT
        elif not inputIServ:
            temStrUrl = ""                    
            if self.strResult.find("\\\\r\\\\n") != -1 and  self.strResult.find("\\\\r\\\\n") != len(self.strResult) - 4:
                strA = self.strResult.split("\\r\\n")
            elif self.strResult.find("\\r\\n") != -1 and  self.strResult.find("\\r\\n") != len(self.strResult) - 2:
                strA = self.strResult.split("\r\n")
            else:
                strA = self.strResult.split("\r\n")
            for temStrAryT in strA:
                if temStrAryT == "" :
                    continue
                temStrAryT = temStrAryT.replace("\r","").replace("\n","")  
                if len(temStrAryT) > 3 and temStrAryT.find("@##") == 0 and temStrAryT.find("##@") == len(temStrAryT) - 3:
                    if temStrAryT.find("@##ST=02") == 0:
                        temSHH = ServerHandlerHttp()
                        attReturn_Value = Return_Value()
                        temGd = Query_Data(attReturn_Value)
                        temStrUrl = temStrAryT.replace("@##","").replace("##@","")
                        temResult = temSHH.Center_Data_Rel(temStrUrl,"","",temGd,"",inputMServSocket.getpeername()[0])
                        temResult = "@##ST=03&CHILD_NODE=" + Com_Para.Child_Code+ "&VALUE=" + temResult + "##@\r\n"
                        Com_Fun.SendSocket(temResult,inputMServSocket)                    
                elif len(temStrAryT) > 3 and temStrAryT.find("@##") == 0 and temStrAryT.find("##@") == -1:
                    temStrUrl = temStrAryT
                elif len(temStrAryT) > 3 and temStrAryT.find("@##") == -1 and temStrAryT.find("##@") == len(temStrAryT) - 3:
                    temStrUrl += temStrAryT
                    if temStrAryT.find("@##ST=02") == 0:
                        temSHH = ServerHandlerHttp()
                        attReturn_Value = Return_Value()
                        temGd = Query_Data(attReturn_Value)
                        temStrUrl = temStrAryT.replace("@##","").replace("##@","")
                        temResult = temSHH.Center_Data_Rel(temStrUrl,"","",temGd,"",inputMServSocket.getpeername()[0])
                        temResult = "@##ST=03&CHILD_NODE=" + Com_Para.Child_Code+ "&VALUE=" + temResult + "##@\r\n"
                        Com_Fun.SendSocket(temResult,inputMServSocket)
                elif temStrAryT != "" and temStrAryT.find("@##") == -1 and temStrAryT.find("##@") == -1:
                    temStrUrl += temStrAryT          