#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import json, importlib, os,platform,datetime
from urllib.parse import unquote
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.db_Self.Db_Common_Self import Db_Common_Self
from com.plugins.A01_6GG8T3.t_station_info import t_station_info
from com.plugins.A01_6GG8T3.t_modbus_addr import t_modbus_addr
from com.plugins.A01_6GG8T3.t_code_element import t_code_element
from com.plugins.A01_6GG8T3.monitor_data import monitor_data
from com.plugins.A01_6GG8T3 import A01_Para
from com.zxy.z_debug import z_debug
class Bus_Ope_DB_Cent(z_debug):
    def __init__(self):
        pass
    def init_page(self):    
        self.monitor_had(30)
        if A01_Para.objModbus_IP != "" and A01_Para.objModbus_Port == -1:
            self.monitor_had(1)
            return 0
        temStrSql = "select * from t_param_value where PARAM_KEY = 'ModBus_Tcp_Server' or PARAM_KEY = 'ModBus_Tcp_Port'"
        temDb_self = Db_Common_Self()
        temRs = temDb_self.Common_Sql(temStrSql)
        try:
            for temItem in temRs:
                if temItem[1] == "ModBus_Tcp_Server":
                    A01_Para.objModbus_IP = Com_Fun.NoNull(temItem[2])
                elif temItem[1] == "ModBus_Tcp_Port":
                    A01_Para.objModbus_Port = Com_Fun.Str_To_Int(temItem[2])            
            self.monitor_had(1)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+repr(e)
            else:
                temLog = self.debug_info()+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        finally:
            pass
    def monitor_had(self,iNum):
        temdbc2 = Db_Common2()
        tbName = "monitor_data_min"+Com_Fun.DateTimeAdd(datetime.datetime.now(), "d",iNum).strftime('%Y%m')
        strSql = "select count(1) from "+tbName+" limit 1"
        temValue = temdbc2.Common_Sql(strSql)
        if temValue == None:
            strSql = "Create  TABLE "+tbName+"("
            strSql = strSql+"[main_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"
            strSql = strSql+",[station_code] varchar(14) NOT NULL"
            strSql = strSql+",[get_time] datetime NOT NULL DEFAULT (datetime('now'))"
            strSql = strSql+",[val_01] varchar(100)"
            strSql = strSql+",[val_02] varchar(100)"
            strSql = strSql+",[val_03] varchar(100)"
            strSql = strSql+",[val_04] varchar(100)"
            strSql = strSql+",[val_05] varchar(100)"
            strSql = strSql+",[val_06] varchar(100)"
            strSql = strSql+",[val_07] varchar(100)"
            strSql = strSql+",[val_08] varchar(100)"
            strSql = strSql+",[val_09] varchar(100)"
            strSql = strSql+",[val_10] varchar(100)"
            strSql = strSql+",[val_11] varchar(100)"
            strSql = strSql+",[val_12] varchar(100)"
            strSql = strSql+",[val_13] varchar(100)"
            strSql = strSql+",[val_14] varchar(100)"
            strSql = strSql+",[val_15] varchar(100)"
            strSql = strSql+",[val_16] varchar(100)"
            strSql = strSql+",[val_17] varchar(100)"
            strSql = strSql+",[val_18] varchar(100)"
            strSql = strSql+",[val_19] varchar(100)"
            strSql = strSql+",[val_20] varchar(100)"
            strSql = strSql+",[update_time] datetime DEFAULT (datetime('now'))"
            strSql = strSql+",[m_flag] int"
            strSql = strSql+");"
            temdbc2.CommonExec_Sql(strSql)
        if iNum == 30:
            return 0
        temMonitor_data = self.Get_monitor_data_minNoUpdate(tbName)
        for tsi in temMonitor_data:
            inputValues = {}
            inputValues["param_value1"] = tsi.attstation_code
            inputValues["param_value2"] = tsi.attget_time
            inputValues["param_value3"] = tsi.attval_01
            inputValues["param_value4"] = tsi.attval_02
            inputValues["param_value5"] = tsi.attval_03
            inputValues["param_value6"] = tsi.attval_04
            inputValues["param_value7"] = tsi.attval_05
            inputValues["param_value8"] = tsi.attval_06
            inputValues["param_value9"] = tsi.attval_07
            inputValues["param_value10"] = tsi.attval_08
            inputValues["param_value11"] = tsi.attval_09
            inputValues["param_value12"] = tsi.attval_10
            inputValues["param_value13"] = tsi.attval_11
            inputValues["param_value14"] = tsi.attval_12
            inputValues["param_value15"] = tsi.attval_13
            inputValues["param_value16"] = tsi.attval_14
            inputValues["param_value17"] = tsi.attval_15
            inputValues["param_value18"] = tsi.attval_16
            inputValues["param_value19"] = tsi.attval_17
            inputValues["param_value20"] = tsi.attval_18
            inputValues["param_value21"] = tsi.attval_19
            inputValues["param_value22"] = tsi.attval_20
            inputUrl = "http://localhost:8080/CenterData/getdata.jsp?a=1" + A01_Para.strCon
            m_result = Com_Fun.PostHttp(inputUrl, inputValues)
            if m_result == "-1" or m_result == "":
                break
            else:
                try:
                    temJso = json.loads(m_result)
                    temJsoary = temJso["C01_T7H5R4"]
                    for temobj in temJsoary:
                        temSResult = temobj["s_result"]
                        if temSResult == 1:
                            temUpdstrSql = "update "+tbName+" set m_flag = '1' where main_id='"+str(tsi.attmain_id)+"'"
                            temdbc2.CommonExec_Sql(temUpdstrSql)
                            break
                except Exception as e:
                    if str(type(self)) == "<class 'type'>":
                        self.debug_in(self,repr(e)+"===>"+m_result)#打印异常信息
                    else:
                        self.debug_in(repr(e)+"===>"+m_result)#打印异常信息
    def Get_monitor_data_minNoUpdate(self,tbName):
        temStrSql = "select * from "+tbName+" where m_flag = '0' order by main_id asc limit 5000"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temmonitor_data = monitor_data()
                vTime = ""
                temmonitor_data.attmain_id = Com_Fun.ZeroNull(temItem[0])
                temmonitor_data.attstation_code = Com_Fun.NoNull(temItem[1])
                if len(temItem[2]) == 10:
                    vTime = " 00:00:00"
                temmonitor_data.attget_time = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[2]+vTime)
                temmonitor_data.attval_01 = Com_Fun.NoNull(temItem[3])
                temmonitor_data.attval_02 = Com_Fun.NoNull(temItem[4])
                temmonitor_data.attval_03 = Com_Fun.NoNull(temItem[5])
                temmonitor_data.attval_04 = Com_Fun.NoNull(temItem[6])
                temmonitor_data.attval_05 = Com_Fun.NoNull(temItem[7])
                temmonitor_data.attval_06 = Com_Fun.NoNull(temItem[8])
                temmonitor_data.attval_07 = Com_Fun.NoNull(temItem[9])
                temmonitor_data.attval_08 = Com_Fun.NoNull(temItem[10])
                temmonitor_data.attval_09 = Com_Fun.NoNull(temItem[11])
                temmonitor_data.attval_10 = Com_Fun.NoNull(temItem[12])
                temmonitor_data.attval_11 = Com_Fun.NoNull(temItem[13])
                temmonitor_data.attval_12 = Com_Fun.NoNull(temItem[14])
                temmonitor_data.attval_13 = Com_Fun.NoNull(temItem[15])
                temmonitor_data.attval_14 = Com_Fun.NoNull(temItem[16])
                temmonitor_data.attval_15 = Com_Fun.NoNull(temItem[17])
                temmonitor_data.attval_16 = Com_Fun.NoNull(temItem[18])
                temmonitor_data.attval_17 = Com_Fun.NoNull(temItem[19])
                temmonitor_data.attval_18 = Com_Fun.NoNull(temItem[20])
                temmonitor_data.attval_19 = Com_Fun.NoNull(temItem[21])
                temmonitor_data.attval_20 = Com_Fun.NoNull(temItem[22])
                if len(temItem[23]) == 10:
                    vTime = " 00:00:00"
                temmonitor_data.attupdate_time = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[23]+vTime)
                temmonitor_data.m_flag = Com_Fun.NoNull(temItem[24])
                temList.append(temmonitor_data)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+ temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+ temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList    
    def Get_monitor_data_min202005(self):
        temStrSql = "select * from monitor_data_min202005  where main_id < 870"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temmonitor_data = monitor_data()
                vTime = ""
                temmonitor_data.attmain_id = Com_Fun.ZeroNull(temItem[0])
                temmonitor_data.attstation_code = Com_Fun.NoNull(temItem[1])
                if len(temItem[2]) == 10:
                    vTime = " 00:00:00"
                temmonitor_data.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[2]+vTime)
                temmonitor_data.attval_01 = Com_Fun.NoNull(temItem[3])
                temmonitor_data.attval_02 = Com_Fun.NoNull(temItem[4])
                temmonitor_data.attval_03 = Com_Fun.NoNull(temItem[5])
                temmonitor_data.attval_04 = Com_Fun.NoNull(temItem[6])
                temmonitor_data.attval_05 = Com_Fun.NoNull(temItem[7])
                temmonitor_data.attval_06 = Com_Fun.NoNull(temItem[8])
                temmonitor_data.attval_07 = Com_Fun.NoNull(temItem[9])
                temmonitor_data.attval_08 = Com_Fun.NoNull(temItem[10])
                temmonitor_data.attval_09 = Com_Fun.NoNull(temItem[11])
                temmonitor_data.attval_10 = Com_Fun.NoNull(temItem[12])
                temmonitor_data.attval_11 = Com_Fun.NoNull(temItem[13])
                temmonitor_data.attval_12 = Com_Fun.NoNull(temItem[14])
                temmonitor_data.attval_13 = Com_Fun.NoNull(temItem[15])
                temmonitor_data.attval_14 = Com_Fun.NoNull(temItem[16])
                temmonitor_data.attval_15 = Com_Fun.NoNull(temItem[17])
                temmonitor_data.attval_16 = Com_Fun.NoNull(temItem[18])
                temmonitor_data.attval_17 = Com_Fun.NoNull(temItem[19])
                temmonitor_data.attval_18 = Com_Fun.NoNull(temItem[20])
                temmonitor_data.attval_19 = Com_Fun.NoNull(temItem[21])
                temmonitor_data.attval_20 = Com_Fun.NoNull(temItem[22])
                if len(temItem[23]) == 10:
                    vTime = " 00:00:00"
                temmonitor_data.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[23]+vTime)
                temmonitor_data.m_flag = Com_Fun.NoNull(temItem[24])
                temList.append(temmonitor_data)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def Get_t_station_info(self):
        temStrSql = "select * from t_station_info"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temt_station_info = t_station_info()
                vTime = ""
                temt_station_info.attmain_id = Com_Fun.ZeroNull(temItem[0])
                temt_station_info.attstation_code = Com_Fun.NoNull(temItem[1])
                temt_station_info.attstation_name = Com_Fun.NoNull(temItem[2])
                if len(temItem[3]) == 10:
                    vTime = " 00:00:00"
                temt_station_info.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[3]+vTime)
                temt_station_info.atts_desc = Com_Fun.NoNull(temItem[4])
                temList.append(temt_station_info)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def Get_t_modbus_addr(self):
        temStrSql = "select * from t_modbus_addr"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temt_modbus_addr = t_modbus_addr()
                vTime = ""
                temt_modbus_addr.attmain_id = Com_Fun.ZeroNull(temItem[0])
                temt_modbus_addr.attstation_id = Com_Fun.ZeroNull(temItem[1])
                temt_modbus_addr.attmodbus_gateway = Com_Fun.NoNull(temItem[2])
                temt_modbus_addr.attcom_port = Com_Fun.NoNull(temItem[3])
                temt_modbus_addr.attmodbus_addr = Com_Fun.ZeroNull(temItem[4])
                temt_modbus_addr.attmodbus_begin = Com_Fun.ZeroNull(temItem[5])
                temt_modbus_addr.attmodbus_length = Com_Fun.ZeroNull(temItem[6])
                temt_modbus_addr.attelement_val = Com_Fun.NoNull(temItem[7])
                if len(temItem[8]) == 10:
                    vTime = " 00:00:00"
                temt_modbus_addr.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[8]+vTime)
                temt_modbus_addr.atts_desc = Com_Fun.NoNull(temItem[9])
                temList.append(temt_modbus_addr)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def Get_t_modbus_addr_by_station_id(self,inputstation_id):
        temStrSql = "select * from t_modbus_addr where station_id = '"+inputstation_id+"'"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temt_modbus_addr = t_modbus_addr()
                vTime = ""
                temt_modbus_addr.attmain_id = Com_Fun.ZeroNull(temItem[0])
                temt_modbus_addr.attstation_id = Com_Fun.ZeroNull(temItem[1])
                temt_modbus_addr.attmodbus_gateway = Com_Fun.NoNull(temItem[2])
                temt_modbus_addr.attcom_port = Com_Fun.NoNull(temItem[3])
                temt_modbus_addr.attmodbus_addr = Com_Fun.ZeroNull(temItem[4])
                temt_modbus_addr.attmodbus_begin = Com_Fun.ZeroNull(temItem[5])
                temt_modbus_addr.attmodbus_length = Com_Fun.ZeroNull(temItem[6])
                temt_modbus_addr.attelement_val = Com_Fun.NoNull(temItem[7])
                if len(temItem[8]) == 10:
                    vTime = " 00:00:00"
                temt_modbus_addr.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[8]+vTime)
                temt_modbus_addr.atts_desc = Com_Fun.NoNull(temItem[9])
                temList.append(temt_modbus_addr)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def Get_t_code_element(self):
        temStrSql = "select * from t_code_element"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temt_code_element = t_code_element()
                vTime = ""
                temt_code_element.attmain_id = Com_Fun.ZeroNull(temItem[0])
                temt_code_element.attelement_code = Com_Fun.NoNull(temItem[1])
                temt_code_element.attelement_name = Com_Fun.NoNull(temItem[2])
                temt_code_element.attelement_val = Com_Fun.NoNull(temItem[3])
                if len(temItem[4]) == 10:
                    vTime = " 00:00:00"
                temt_code_element.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[4]+vTime)
                temt_code_element.atts_desc = Com_Fun.NoNull(temItem[5])
                temList.append(temt_code_element)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def Get_monitor_data_real_true(self,inputstation_code):
        temStrSql = "select * from monitor_data_real where station_code = '"+inputstation_code+"'"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        if len(temRs) == 0:
            return False
        else:
            return True