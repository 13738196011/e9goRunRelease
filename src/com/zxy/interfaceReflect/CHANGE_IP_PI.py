#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import socket
import struct
from fcntl import ioctl
from com.zxy.z_debug import z_debug
class CHANGE_IP_PI(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    param_value11     = None  
    param_value12     = None
    param_value13     = None
    param_value14     = None
    param_value15     = None
    param_value16     = None
    param_value17     = None
    param_value18     = None
    param_value19     = None
    param_value20     = None
    strContinue      = "1"
    def __init__(self, params):
        pass
    def init_start(self):       
        self.c_ip2()
    def c_ip2(self):
        sock=socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        try:
            ifreq = struct.pack('16sH2s4s8s', 'eth0', socket.AF_INET, '\x00' * 2, socket.inet_aton(self.param_value1), '\x00' * 8)
            ioctl(sock.fileno(), 0x8916, ifreq)
            ifreq = struct.pack('16sH2s4s8s', 'eth0', socket.AF_INET, '\x00' * 2, socket.inet_aton(self.param_value2), '\x00' * 8)
            ioctl(sock.fileno(), 0x891c, ifreq)         
            ip=ioctl(sock.fileno(),0x8915,struct.pack('64s','eth0'))
            ip=socket.inet_ntoa(ip[20:24])
            print(ip)         
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
