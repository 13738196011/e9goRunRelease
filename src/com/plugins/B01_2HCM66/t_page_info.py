#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_page_info(object):
    attmain_id = -1
    attPAGE_CODE = ""
    attPAGE_TITLE = ""
    attPAGE_URL = ""
    attCREATE_DATE = None
    attS_DESC = ""
    attJS_URL = ""
    lenPAGE_CODE = 50
    lenPAGE_TITLE = 200
    lenPAGE_URL = 200
    lenS_DESC = 500
    lenJS_URL = 500
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["PAGE_CODE"] = "模板编号"
        temHt["PAGE_TITLE"] = "模板描述"
        temHt["PAGE_URL"] = "扩展字段"
        temHt["CREATE_DATE"] = "系统时间"
        temHt["S_DESC"] = "备注"
        temHt["JS_URL"] = "扩展字段2"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
