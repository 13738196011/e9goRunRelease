#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_station_ele_link(object):
    attMAIN_ID = -1
    attSTATION_CODE = ""
    attELEMENT_ID = -1
    attUP_LEVEL = ""
    attDOWN_LEVEL = ""
    attUP_LIMIT = ""
    attDOWN_LIMIT = ""
    attS_STATE = 0
    attCREATE_DATE = None
    attS_DESC = "",
    attELEMENT_CODE = ""
    attELEMENT_VAL= ""
    attDTO_NUM= ""
    attELEMENT_NAME= ""
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")