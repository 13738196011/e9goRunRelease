#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import threading
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.db1.Db_Common1 import Db_Common1
from com.zxy.model.T_PROC_NAME import T_PROC_NAME
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.business.Power_Control import Power_Control
from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.plugins.B01_2HCM66.B01_2HCM66_Time import B01_2HCM66_Time
from com.zxy.z_debug import z_debug
class A01_DATART_IN(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    temSqlException  = ""
    strContinue      = 0
    def __init__(self):
        pass
    def init_start(self):
        self.strContinue = 0
        bodc = Ope_DB_Cent()
        temFile = None
        temTableName = "t_data_"+str(self.param_value1)
        temtable_info = bodc.Get_Sqlite_table_info_ByDB(temTableName,"2")
        try: 
            if len(temtable_info) > 0 or 1==1: 
                temdbc2 = Db_Common2()   
                temdbc1 = Db_Common1()   
                strSql = "drop table "+temTableName
                temdbc2.CommonExec_Sql(strSql)            
                strSql = "delete from t_file_info where file_type_id = '"+self.param_value1+"'"
                temdbc2.CommonExec_Sql(strSql)            
                strSql = "delete from t_excel_data where file_id not in (select main_id from t_file_info)"
                temdbc2.CommonExec_Sql(strSql)
                strSql = "delete from t_excel_tag where file_type_id = '"+self.param_value1+"'"
                temdbc2.CommonExec_Sql(strSql)     
                strSql = "delete from t_file_expand where file_id not in (select main_id from t_file_info)"
                temdbc2.CommonExec_Sql(strSql) 
                strSql = "delete from t_proc_name where INF_EN_NAME in ('T01_sel_t_data_"+self.param_value1+"_ByID','T01_sel_t_data_"+self.param_value1+"','T01_ins_t_data_"+self.param_value1+"','T01_upd_t_data_"+self.param_value1+"','T01_del_t_data_"+self.param_value1+"')"
                temdbc1.CommonExec_Sql(strSql)
                strSql = "delete from menu_info where menu_url = 'biz_target/t_data_"+self.param_value1+".html'"
                temdbc2.CommonExec_Sql(strSql) 
                temPcl = Power_Control(Com_Para.sub_code,Com_Para.sub_usercode,"T01_truncate_table","")  
                bodc.Cal_Data(temPcl,Com_Para.sub_code,"T01_truncate_table",[],"","",{})        
            b2t = B01_2HCM66_Time()
            b2t.Delete_MOdel_Py(self.param_value1)
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"1\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"清除重置成功\"}]}"        
        except Exception as e:
            self.strResult = "{\""+self.param_name+"\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"清除重置操作失败："+repr(e)+"\"}]}"
        finally:
            if temFile != None:
                temFile.close()