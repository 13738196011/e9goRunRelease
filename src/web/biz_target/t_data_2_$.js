var type = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var ary_file_id = null;
/*declare select options end*/

//业务逻辑数据开始
function biz_start() {
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "T01_t_data_2$file_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "get_T01_t_data_2$file_id");	
    /*biz end*/
}

/*biz step begin*/
function format_file_id(value, row, index) {    var objResult = value;    for(i = 0; i < ary_file_id.length; i++) {        var obj = ary_file_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function get_T01_t_data_2$file_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.T01_t_data_2$file_id) == false)        return false;    ary_file_id = input.T01_t_data_2$file_id;    if($("#file_id").is("select") && $("#file_id")[0].options.length == 0)    {        $.each(ary_file_id, function (i, obj) {            addOptionValue("file_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    init_t_data_2();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function page_end(){
}


//页面初始化方法
function init_t_data_2() {
	type = getUrlParam("type");
	if(type == "add"){
		$("#main_id").parent().parent().parent().hide();
	}
	else if(type == "edit"){
	
	}
	
	//表单验证
	checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#create_date").val() == "")    {        $("#create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
}

//提交表单数据
function SubmitForm(){
	if(type == "add"){
		var inputdata = {
				"param_name": "T01_ins_t_data_2",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#file_id").val()                ,"param_value2": s_encode($("#val_01").val())                ,"param_value3": s_encode($("#val_02").val())                ,"param_value4": s_encode($("#val_03").val())                ,"param_value5": s_encode($("#val_04").val())                ,"param_value6": s_encode($("#val_05").val())                ,"param_value7": s_encode($("#val_06").val())                ,"param_value8": s_encode($("#val_07").val())                ,"param_value9": s_encode($("#val_08").val())                ,"param_value10": s_encode($("#val_09").val())                ,"param_value11": s_encode($("#val_10").val())                ,"param_value12": s_encode($("#val_11").val())                ,"param_value13": s_encode($("#val_12").val())                ,"param_value14": s_encode($("#val_13").val())                ,"param_value15": $("#create_date").val()                ,"param_value16": s_encode($("#s_desc").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_ins_t_data_2");
	}
	else if(type == "edit"){
		var inputdata = {
				"param_name": "T01_upd_t_data_2",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#file_id").val()                ,"param_value2": s_encode($("#val_01").val())                ,"param_value3": s_encode($("#val_02").val())                ,"param_value4": s_encode($("#val_03").val())                ,"param_value5": s_encode($("#val_04").val())                ,"param_value6": s_encode($("#val_05").val())                ,"param_value7": s_encode($("#val_06").val())                ,"param_value8": s_encode($("#val_07").val())                ,"param_value9": s_encode($("#val_08").val())                ,"param_value10": s_encode($("#val_09").val())                ,"param_value11": s_encode($("#val_10").val())                ,"param_value12": s_encode($("#val_11").val())                ,"param_value13": s_encode($("#val_12").val())                ,"param_value14": s_encode($("#val_13").val())                ,"param_value15": $("#create_date").val()                ,"param_value16": s_encode($("#s_desc").val())                ,"param_value17": $("#main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_upd_t_data_2");
	}
}

//以下为页面表单验证
$("#save_t_data_2_Edit").click(function () {
	$("form[name='DataModal']").submit();
})

/*修改数据*/
function get_T01_upd_t_data_2(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_upd_t_data_2) == true)
	{
        //init();
		parent.swal("修改数据成功!", "", "success");
		parent.t_data_2_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

/*添加数据*/
function get_T01_ins_t_data_2(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_ins_t_data_2) == true)
	{
		parent.swal("添加数据成功!", "", "success");
		parent.t_data_2_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

//取消编辑
$("#cancel_t_data_2_Edit").click(function () {
	var index = parent.layer.getFrameIndex(window.name);
	parent.layer.close(index);
})

function clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

$(document).ready(function () {
});

window.onpageshow = function (e) {
    if (e.persisted || (window.performance.navigation.type == 2)) {
        is_history_back = 1;
    } else {
        is_history_back = 0;
    }
    //页面初始化
    init_page();
};

//form验证
function checkFormInput() {
    $("#DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            main_id: {}            ,file_id: {}            ,val_01: {}            ,val_02: {}            ,val_03: {}            ,val_04: {}            ,val_05: {}            ,val_06: {}            ,val_07: {}            ,val_08: {}            ,val_09: {}            ,val_10: {}            ,val_11: {}            ,val_12: {}            ,val_13: {}            ,create_date: {date: true,required : true,maxlength:19}            ,s_desc: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            main_id: {}            ,file_id: {}            ,val_01: {}            ,val_02: {}            ,val_03: {}            ,val_04: {}            ,val_05: {}            ,val_06: {}            ,val_07: {}            ,val_08: {}            ,val_09: {}            ,val_10: {}            ,val_11: {}            ,val_12: {}            ,val_13: {}            ,create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,s_desc: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	SubmitForm();
        	return false;
        }
    })
}