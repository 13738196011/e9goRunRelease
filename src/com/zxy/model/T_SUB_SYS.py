#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class T_SUB_SYS(object):
    attMAIN_ID = -1
    attSUB_NAME = ""
    attSUB_CODE = ""
    attLIMIT_DATE = None
    attS_DESC = ""
    attCREATE_DATE = None
    attIP_ADDR = ""
    def __init__(self):
        self.attLIMIT_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
