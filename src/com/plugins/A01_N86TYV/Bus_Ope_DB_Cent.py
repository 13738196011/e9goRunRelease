#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import json, importlib, os,platform,datetime
from urllib.parse import unquote
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.db_Self.Db_Common_Self import Db_Common_Self
from com.plugins.A01_N86TYV.t_code_element import t_code_element
from com.plugins.A01_N86TYV.t_plc_addr import t_plc_addr
from com.plugins.A01_N86TYV.plc_addr_element import plc_addr_element
from com.plugins.A01_N86TYV import A01_Para
class Bus_Ope_DB_Cent(object):
    def __init__(self):
        pass
    def init_page(self):
        try:
            self.Get_plc_addr_element("2")
            if A01_Para.objPLC_addr != "" and A01_Para.objPLC_rack == -1:
                return 0        
            temStrSql = "select * from plc_address where main_id = 2"
            temDb2 = Db_Common2()
            temRs = temDb2.Common_Sql(temStrSql)
            for temItem in temRs:
                A01_Para.objPLC_addr = Com_Fun.NoNull(temItem[1])
                A01_Para.objPLC_rack = Com_Fun.Str_To_Int(temItem[2])  
                A01_Para.objPLC_slot = Com_Fun.Str_To_Int(temItem[3])  
                A01_Para.objPLC_Port = Com_Fun.Str_To_Int(temItem[4])
        except Exception as e:
            temStr = "init data error [A01_N86TYV.Bus_Ope_DB_Cent.init_page]" + "\r\n" + repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temStr)
            uL.WriteLog()
        finally:
            pass
    def Get_plc_addr_element(self,inputT_P_MAIN_ID):
        try:
            if len(A01_Para.objplc_addr_element) != 0:
                return None
            temStrSql = "select * from plc_addr_element where p_main_id = '"+inputT_P_MAIN_ID+"'"
            temDb2 = Db_Common2()
            temRs = temDb2.Common_Sql(temStrSql)
            for temItem in temRs:
                templc_addr_element = plc_addr_element()
                templc_addr_element.attmain_id = Com_Fun.ZeroNull(temItem[0])
                templc_addr_element.attp_main_id = Com_Fun.ZeroNull(temItem[1])
                templc_addr_element.attelement_code = Com_Fun.NoNull(temItem[2])
                templc_addr_element.attdata_type = Com_Fun.NoNull(temItem[3])
                templc_addr_element.attdata_area = Com_Fun.NoNull(temItem[4])
                templc_addr_element.attdb_num = Com_Fun.NoNull(temItem[5])
                templc_addr_element.attaddress = Com_Fun.NoNull(temItem[6])
                templc_addr_element.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[7])                
                Com_Fun.SetHashTable(A01_Para.objplc_addr_element,templc_addr_element.attmain_id,templc_addr_element)                
        except Exception as es:
            temError = "get data error [A01_N86TYV.Bus_Ope_DB_Cent.Get_plc_addr_element]" + temStrSql +"\r\n"+repr(es)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temError)
            uL.WriteLog()
    def Ins_data_min(self,inputdata_min):
        temStrSql = "insert into data_min"+Com_Fun.GetDateInput("%Y%m","%Y-%m-%d %H:%M:%S",str(inputdata_min.attcpu_time)) #+Com_Fun.GetTime("%Y%m")
        temStrSql = temStrSql+"(cpu_time,automatic_on,time_base,door_closed,extruder_on,batch_counter,cycle_time,tank_weight,cooling_water_temp,air_pressure,update_time,m_flag,s_type) values("
        temStrSql = temStrSql+"'"+str(inputdata_min.attcpu_time)+"','"+str(inputdata_min.attautomatic_on)+"','"+str(inputdata_min.atttime_base)+"','"+str(inputdata_min.attdoor_closed)
        temStrSql = temStrSql+"','"+str(inputdata_min.attextruder_on)+"','"+str(inputdata_min.attbatch_counter)+"','"+str(inputdata_min.attcycle_time)+"','"+str(inputdata_min.atttank_weight)
        temStrSql = temStrSql+"','"+str(inputdata_min.attcooling_water_temp)+"','"+str(inputdata_min.attair_pressure)+"','"+Com_Fun.GetTimeDef()+"','0','"+inputdata_min.atts_type+"')"       
        temDb2 = Db_Common2()
        temDb2.CommonExec_Sql(temStrSql)
    def Get_t_code_element(self):
        try:
            temStrSql = "select * from t_code_element"
            temDb2 = Db_Common2()
            temRs = temDb2.Common_Sql(temStrSql)
            temList = []
            for temItem in temRs:
                temt_code_element = t_code_element()
                vTime = ""
                temt_code_element.attmain_id = Com_Fun.ZeroNull(temItem[0])
                temt_code_element.attelement_code = Com_Fun.NoNull(temItem[1])
                temt_code_element.attelement_name = Com_Fun.NoNull(temItem[2])
                temt_code_element.attelement_val = Com_Fun.NoNull(temItem[3])
                if len(temItem[4]) == 10:
                    vTime = " 00:00:00"
                temt_code_element.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[4]+vTime)
                temt_code_element.atts_desc = Com_Fun.NoNull(temItem[5])
                temList.append(temt_code_element)
        except Exception as es:
            temError = "get data error [A01_N86TYV.Bus_Ope_DB_Cent.Get_t_code_element]" + temStrSql +"\r\n"+repr(es)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temError)
            uL.WriteLog()
        return temList
    def Get_t_plc_addr(self):
        try:
            temStrSql = "select * from t_plc_addr"
            temDb2 = Db_Common2()
            temRs = temDb2.Common_Sql(temStrSql)
            temList = []
            for temItem in temRs:
                temt_plc_addr = t_plc_addr()
                temt_plc_addr.attmain_id = Com_Fun.ZeroNull(temItem[0])
                temt_plc_addr.attstation_id = Com_Fun.ZeroNull(temItem[1])
                temt_plc_addr.attplc_area = Com_Fun.NoNull(temItem[2])
                temt_plc_addr.attplc_dbnumber = Com_Fun.ZeroNull(temItem[3])
                temt_plc_addr.attplc_Start = Com_Fun.ZeroNull(temItem[4])
                temt_plc_addr.attplc_size = Com_Fun.ZeroNull(temItem[5])
                temt_plc_addr.attelement_val = Com_Fun.NoNull(temItem[6])
                vTime = ""
                if len(temItem[7]) == 10:
                    vTime = " 00:00:00"
                temt_plc_addr.attcreate_date = Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S", temItem[7]+vTime)
                temt_plc_addr.atts_desc = Com_Fun.NoNull(temItem[8])
                temList.append(temt_plc_addr)
        except Exception as es:
            temError = "get data error [A01_N86TYV.Bus_Ope_DB_Cent.Get_t_plc_addr]" + temStrSql +"\r\n"+repr(es)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temError)
            uL.WriteLog()
        return temList
