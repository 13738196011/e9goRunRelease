#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_data_2(object):
    attmain_id = -1
    attfile_id = 0
    attval_01 = ""
    attval_02 = ""
    attval_03 = ""
    attval_04 = ""
    attval_05 = ""
    attval_06 = ""
    attval_07 = ""
    attval_08 = ""
    attval_09 = ""
    attval_10 = ""
    attval_11 = ""
    attval_12 = ""
    attval_13 = ""
    attcreate_date = None
    atts_desc = ""
    lenval_01 = 500
    lenval_02 = 500
    lenval_03 = 500
    lenval_04 = 500
    lenval_05 = 500
    lenval_06 = 500
    lenval_07 = 500
    lenval_08 = 500
    lenval_09 = 500
    lenval_10 = 500
    lenval_11 = 500
    lenval_12 = 500
    lenval_13 = 500
    lens_desc = 2000
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["file_id"] = "附件信息"
        temHt["val_01"] = "能源名称"
        temHt["val_02"] = "计量单位"
        temHt["val_03"] = "代码"
        temHt["val_04"] = "年初库存量"
        temHt["val_05"] = "购进量"
        temHt["val_06"] = "购自省外"
        temHt["val_07"] = "购进金额（千元）"
        temHt["val_08"] = "工业生产消费量"
        temHt["val_09"] = "用于原材料"
        temHt["val_10"] = "运输工具消费"
        temHt["val_11"] = "期末库存量"
        temHt["val_12"] = "采用折标系数"
        temHt["val_13"] = "参考折标系数"
        temHt["create_date"] = "系统时间"
        temHt["s_desc"] = "系统备注"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["file_id"] = "附件信息" 
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["file_id"] = "select main_id,file_name as cn_name from t_file_info" 
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
