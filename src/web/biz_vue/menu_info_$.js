//按钮事件新增或编辑
var BA9E13_type = "";
//其他页面传到本页面参数
var BA9E13_param = {};
//暂时没用
var BA9E13_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BA9E13_ary_group_id = null;var BA9E13_ary_is_ava = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];var BA9E13_ary_role_id = [{'main_id': '1', 'cn_name': '开发者'}, {'main_id': '2', 'cn_name': '系统管理员'}, {'main_id': '3', 'cn_name': '运维人员'}, {'main_id': '4', 'cn_name': '普通用户'}];
/*declare select options end*/

//业务逻辑数据开始
function BA9E13_menu_info_biz_start(inputdata) {
	BA9E13_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "N01_menu_info$group_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BA9E13_get_N01_menu_info$group_id");	
    /*biz end*/
}

/*biz step begin*/
function BA9E13_format_group_id(value, row, index) {    var objResult = value;    for(i = 0; i < BA9E13_ary_group_id.length; i++) {        var obj = BA9E13_ary_group_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BA9E13_format_is_ava(value, row, index) {    var objResult = value;    for(i = 0; i < BA9E13_ary_is_ava.length; i++) {        var obj = BA9E13_ary_is_ava[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BA9E13_format_role_id(value, row, index) {    var objResult = value;    for(i = 0; i < BA9E13_ary_role_id.length; i++) {        var obj = BA9E13_ary_role_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BA9E13_get_N01_menu_info$group_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_menu_info$group_id) == false)        return false;    BA9E13_ary_group_id = input.N01_menu_info$group_id;    if($("#BA9E13_group_id").is("select") && $("#BA9E13_group_id")[0].options.length == 0)    {        $.each(BA9E13_ary_group_id, function (i, obj) {            addOptionValue("BA9E13_group_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#BA9E13_is_ava").is("select") && $("#BA9E13_is_ava")[0].options.length == 0)    {        $.each(BA9E13_ary_is_ava, function (i, obj) {            addOptionValue("BA9E13_is_ava", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#BA9E13_role_id").is("select") && $("#BA9E13_role_id")[0].options.length == 0)    {        $.each(BA9E13_ary_role_id, function (i, obj) {            addOptionValue("BA9E13_role_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    BA9E13_init_menu_info();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function BA9E13_page_end(){
	if(BA9E13_param["type"] == "edit"){
		BA9E13_get_edit_info();
	}
}

//页面初始化方法
function BA9E13_init_menu_info() {
	//type = getUrlParam("type");
	if(BA9E13_param["type"] == "add"){
		$("#BA9E13_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BA9E13_group_id").val(BA9E13_param["group_id"]);        $("#BA9E13_menu_name").val(BA9E13_param["menu_name"]);        $("#BA9E13_is_ava").val(BA9E13_param["is_ava"]);        $("#BA9E13_role_id").val(BA9E13_param["role_id"]);		
		/*Get Find Select param end*/
		
	}
	else if(BA9E13_param["type"] == "edit"){
	
	}
	
	//表单验证
	BA9E13_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BA9E13_create_date").val() == "")    {        $("#BA9E13_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BA9E13_create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BA9E13_page_end();
}

//提交表单数据
function BA9E13_SubmitForm(){
	if(BA9E13_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_menu_info",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#BA9E13_group_id").val()                ,"param_value2": s_encode($("#BA9E13_menu_url").val())                ,"param_value3": s_encode($("#BA9E13_menu_name").val())                ,"param_value4": $("#BA9E13_is_ava").val()                ,"param_value5": s_encode($("#BA9E13_role_id").val())                ,"param_value6": $("#BA9E13_create_date").val()                ,"param_value7": s_encode($("#BA9E13_s_desc").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BA9E13_get_N01_ins_menu_info");
	}
	else if(BA9E13_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_menu_info",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#BA9E13_group_id").val()                ,"param_value2": s_encode($("#BA9E13_menu_url").val())                ,"param_value3": s_encode($("#BA9E13_menu_name").val())                ,"param_value4": $("#BA9E13_is_ava").val()                ,"param_value5": s_encode($("#BA9E13_role_id").val())                ,"param_value6": $("#BA9E13_create_date").val()                ,"param_value7": s_encode($("#BA9E13_s_desc").val())                ,"param_value8": $("#BA9E13_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BA9E13_get_N01_upd_menu_info");
	}
}

//vue回调
function BA9E13_menu_info_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#BA9E13_save_menu_info_Edit").click(function () {
	$("form[name='BA9E13_DataModal']").submit();
})

/*修改数据*/
function BA9E13_get_N01_upd_menu_info(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_menu_info) == true)
	{
		swal("修改数据成功!", "", "success");
		AA9E13_menu_info_query();
		BA9E13_clear_validate();
		layer.close(BA9E13_param["ly_index"]);
	}
}

/*添加数据*/
function BA9E13_get_N01_ins_menu_info(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_menu_info) == true)
	{
		swal("添加数据成功!", "", "success");
		AA9E13_menu_info_query();
		BA9E13_clear_validate();
		layer.close(BA9E13_param["ly_index"]);
	}
}

//取消编辑
$("#BA9E13_cancel_menu_info_Edit").click(function () {
	layer.close(BA9E13_param["ly_index"]);
	BA9E13_clear_validate();
	$("[id^='BA9E13_div']").hide();
})

//清除查找框
function BA9E13_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BA9E13_clear_validate(){
	$("#BA9E13_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BA9E13_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BA9E13_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BA9E13_clear_edit_info(){
	var inputs = $("#BA9E13_DataModal").find('input');
	var selects = $("#BA9E13_DataModal").find("select");
	var textareas = $("#BA9E13_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	BA9E13_init_menu_info();
}

//页面输入框赋值
function BA9E13_get_edit_info(){
	var rowData = $("#AA9E13_menu_info_Events").bootstrapTable('getData')[AA9E13_select_menu_info_rowId];
	var inputs = $("#BA9E13_DataModal").find('input');
	var selects = $("#BA9E13_DataModal").find("select");
	var textareas = $("#BA9E13_DataModal").find('textarea');
	$.each(selects, function (i, obj) {
		if(typeof(eval("AA9E13_ary_"+obj.id.toString().replace("BA9E13_",""))) == "object")
		{
			$.each(eval("AA9E13_ary_"+obj.id.toString().replace("BA9E13_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("BA9E13_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("BA9E13_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("AA9E13_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#BA9E13_DataModal").find('[id="'+"BA9E13_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("BA9E13_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("BA9E13_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//form验证
function BA9E13_checkFormInput() {
    BA9E13_validate = $("#BA9E13_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BA9E13_main_id: {}            ,BA9E13_group_id: {}            ,BA9E13_menu_url: {}            ,BA9E13_menu_name: {}            ,BA9E13_is_ava: {}            ,BA9E13_role_id: {}            ,BA9E13_create_date: {date: true,required : true,maxlength:19}            ,BA9E13_s_desc: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BA9E13_main_id: {}            ,BA9E13_group_id: {}            ,BA9E13_menu_url: {}            ,BA9E13_menu_name: {}            ,BA9E13_is_ava: {}            ,BA9E13_role_id: {}            ,BA9E13_create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BA9E13_s_desc: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BA9E13_SubmitForm();
        	return false;
        }
    })
}