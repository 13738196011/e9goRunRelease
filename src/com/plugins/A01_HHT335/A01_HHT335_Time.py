#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import time,json,importlib,binascii,datetime
from com.zxy.z_debug import z_debug
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_IHGDW0.A01_Fun import A01_Fun
from com.plugins.A01_IHGDW0.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_IHGDW0 import A01_Para
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.comport.ComModBus import ComModBus
from com.plugins.A01_IHGDW0.monitor_data_model import monitor_data_model
from com.plugins.A01_IHGDW0.AB_Alarm_model import AB_Alarm_model
from openpyxl.descriptors.base import Length
from com.zxy.z_debug import z_debug
class A01_HHT335_Time(z_debug):
    strContinue      = "1"   
    strResult        = ""
    param_value1     = "-1"
    def __init__(self):
        pass
    def init_start(self):
        try:
            temprotocol = A01_Fun.Get_protocol("A01_HHT335")
            if temprotocol is not None and temprotocol.attcom_port != "":
                temA01modbus = ComModBus()
                if self.param_value1 == "1":
                    temCmd = "AA35011251BB"
                    temA01modbus.get_data_com_hex(temprotocol.attcom_port, temCmd)
                elif self.param_value1 == "0":
                    temCmd = "AA35010051BB"
                    temA01modbus.get_data_com_hex(temprotocol.attcom_port, temCmd)
                pass                
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            pass
    def Judge_Do_Sample(self):
        temResult = False
        try:
            temprotocol = A01_Fun.Get_protocol("A01_HHT335")
            if temprotocol is not None and temprotocol.attcom_port != "":
                temA01modbus = ComModBus()
                temCmd = "AA40BB"
                CmdStr = temA01modbus.get_data_com_hex(temprotocol.attcom_port, temCmd)
                bhexStr = str(binascii.b2a_hex(CmdStr).decode(Com_Para.U_CODE)).upper()
                hexStr = []
                iIndex = 1
                temt = ""
                for tems in bhexStr:
                    temt = temt + tems
                    if iIndex % 2 == 0:
                        hexStr.append(temt)
                        temt = ""
                    iIndex = iIndex + 1
                if hexStr[0] == "CC" and hexStr[1] == "DD" and hexStr[2] == "AA":
                    if hexStr[4] == "02":
                        temResult = True
                    else:
                        temResult = False
                pass                
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
            temResult = False
        finally:
            pass
        return temResult
    def Judge_Alarm(self,inputmonitor_data_model):
        if A01_Para.objAB_Alarm_model is None:
            A01_Para.objAB_Alarm_model = AB_Alarm_model()
        try:
            for tce in A01_Para.ht_t_code_element:
                temValue = getattr(inputmonitor_data_model, tce.attelement_val.lower())
                if tce.attup_level != "" and float(temValue) > float(tce.attup_level):
                    self.strResult = tce.attelement_name+"数据超标报警,监测值:"+temValue+",报警限值:"+tce.attup_level+",数据时间："+str(inputmonitor_data_model.get_time)
                    A01_Para.objAB_Alarm_model.data_get_time = str(inputmonitor_data_model.get_time)
                    A01_Para.objAB_Alarm_model.data_type = "1"
                    A01_Para.objAB_Alarm_model.element_name = tce.attelement_name
                    A01_Para.objAB_Alarm_model.m_value = temValue
                    A01_Para.objAB_Alarm_model.alarm_value = tce.attup_level
                    A01_Para.objAB_Alarm_model.strResult = self.strResult
                    break
                elif tce.attdown_level != "" and float(temValue) < float(tce.attdown_level):
                    self.strResult = tce.attelement_name+"数据超标报警,监测值:"+temValue+",报警限值:"+tce.attup_level+",数据时间："+str(inputmonitor_data_model.get_time)
                    A01_Para.objAB_Alarm_model.data_get_time = str(inputmonitor_data_model.get_time)
                    A01_Para.objAB_Alarm_model.data_type = "1"
                    A01_Para.objAB_Alarm_model.element_name = tce.attelement_name
                    A01_Para.objAB_Alarm_model.m_value = temValue
                    A01_Para.objAB_Alarm_model.alarm_value = tce.attup_level
                    A01_Para.objAB_Alarm_model.strResult = self.strResult
                    break
            pass
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            pass
    def AB_Judge_Time(self):
        temResult = False
        if A01_Para.codsampletime is not None and A01_Para.objAB_Alarm_model.data_get_time is not None:
            temcodsampletime = Com_Fun.GetDateInput('%Y-%m-%d %H:%M:%S','%Y-%m-%d %H:%M:%S',str(A01_Para.codsampletime))
            temdata_get_time = Com_Fun.GetDateInput('%Y-%m-%d %H:%M:%S','%Y-%m-%d %H:%M:%S',str(A01_Para.objAB_Alarm_model.data_get_time))
            if A01_Para.objAB_Alarm_model.AB_get_time is None:
                if Com_Fun.DateTimeAdd(temcodsampletime,"H",1) < datetime.datetime.now() and Com_Fun.DateTimeAdd(temdata_get_time,"H",2) > datetime.datetime.now():
                    temResult = True
            else:
                temAB_get_time = Com_Fun.GetDateInput('%Y-%m-%d %H:%M:%S','%Y-%m-%d %H:%M:%S',str(A01_Para.objAB_Alarm_model.AB_get_time))
                if Com_Fun.DateTimeAdd(temcodsampletime,"H",1) < datetime.datetime.now() and Com_Fun.DateTimeAdd(temdata_get_time,"H",2) > datetime.datetime.now() and Com_Fun.DateTimeAdd(temAB_get_time,"H",1) <= datetime.datetime.now():
                    temResult = True
        return temResult
    def Alerm_Sample(self):
        try:
            if A01_Para.objAB_Alarm_model is None:
                A01_Para.objAB_Alarm_model = AB_Alarm_model()
            if Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "station_type") != "1":
                return None
            temprotocol = A01_Fun.Get_protocol("A01_HHT335")
            if temprotocol is not None and temprotocol.com_port != "" and self.AB_Judge_Time(self):
                temA01modbus = ComModBus()
                temCmd = "AA25BB"
                CmdStr = temA01modbus.get_data_com_hex(temprotocol.com_port, temCmd)
                bhexStr = str(binascii.b2a_hex(CmdStr).decode(Com_Para.U_CODE)).upper()
                hexStr = []
                iIndex = 1
                temt = ""
                for tems in bhexStr:
                    temt = temt + tems
                    if iIndex % 2 == 0:
                        hexStr.append(temt)
                        temt = ""
                    iIndex = iIndex + 1
                if str(hexStr[0]).upper() == "CC" and str(hexStr[1]).upper() == "BB" and str(hexStr[1]).upper() == "CC" and str(hexStr[-1]).upper() == "BB":
                    iIndex = 0
                    for samIndex in hexStr[2:-1]:
                        if int(samIndex.upper(),16) == 0:                            
                            samCmd = "AA1102"+hex(iIndex - 3)+"020100BB"
                            CmdStr = temA01modbus.get_data_com_hex(temprotocol.com_port, samCmd)
                            A01_Para.objAB_Alarm_model.AB_get_time = str(Com_Fun.GetTimeDef())
                        iIndex = iIndex + 1
                else:
                    return None
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            pass