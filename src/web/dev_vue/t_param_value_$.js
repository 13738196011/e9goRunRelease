//按钮事件新增或编辑
var B0CCFC_type = "";
//其他页面传到本页面参数
var B0CCFC_param = {};
//暂时没用
var B0CCFC_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

//主从表传递参数
function B0CCFC_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function B0CCFC_t_param_value_biz_start(inputdata) {
	B0CCFC_param = inputdata;
	layer.close(ly_index);
	B0CCFC_param_set();
    /*biz begin*/
    B0CCFC_init_t_param_value()	
    /*biz end*/
}

/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function B0CCFC_page_end(){
	if(B0CCFC_param["type"] == "edit"){
		B0CCFC_get_edit_info();
	}
}

//页面初始化方法
function B0CCFC_init_t_param_value() {
	//type = getUrlParam("type");
	if(B0CCFC_param["type"] == "add"){
		$("#B0CCFC_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#B0CCFC_param_key").val(B0CCFC_param["param_key"]);        $("#B0CCFC_param_name").val(B0CCFC_param["param_name"]);		
		/*Get Find Select param end*/	
	}
	else if(B0CCFC_param["type"] == "edit"){
	
	}
	
	//表单验证
	B0CCFC_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B0CCFC_create_date").val() == "")    {        $("#B0CCFC_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B0CCFC_create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B0CCFC_page_end();
}

//提交表单数据
function B0CCFC_SubmitForm(){
	if(B0CCFC_param["type"] == "add"){
		var inputdata = {
				"param_name": "T02_ins_t_param_value",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#B0CCFC_param_key").val())                ,"param_value2": s_encode($("#B0CCFC_param_value").val())                ,"param_value3": s_encode($("#B0CCFC_param_name").val())                ,"param_value4": s_encode($("#B0CCFC_s_desc").val())                ,"param_value5": $("#B0CCFC_create_date").val()				
	            /*insert param end*/
			};
		get_ajax_staticurl(inputdata, "B0CCFC_get_T02_ins_t_param_value");
	}
	else if(B0CCFC_param["type"] == "edit"){
		var inputdata = {
				"param_name": "T02_upd_t_param_value",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#B0CCFC_param_key").val())                ,"param_value2": s_encode($("#B0CCFC_param_value").val())                ,"param_value3": s_encode($("#B0CCFC_param_name").val())                ,"param_value4": s_encode($("#B0CCFC_s_desc").val())                ,"param_value5": $("#B0CCFC_create_date").val()                ,"param_value6": $("#B0CCFC_main_id").val()				
	            /*update param end*/
			};
		get_ajax_staticurl(inputdata, "B0CCFC_get_T02_upd_t_param_value");
	}
}

//vue回调
function B0CCFC_t_param_value_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#B0CCFC_save_t_param_value_Edit").click(function () {
	$("form[name='B0CCFC_DataModal']").submit();
})

/*修改数据*/
function B0CCFC_get_T02_upd_t_param_value(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T02_upd_t_param_value) == true)
	{
		swal("修改数据成功!", "", "success");
		A0CCFC_t_param_value_query();
		B0CCFC_clear_validate();
		layer.close(B0CCFC_param["ly_index"]);
	}
}

/*添加数据*/
function B0CCFC_get_T02_ins_t_param_value(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T02_ins_t_param_value) == true)
	{
		swal("添加数据成功!", "", "success");
		A0CCFC_t_param_value_query();
		B0CCFC_clear_validate();
		layer.close(B0CCFC_param["ly_index"]);
	}
}

//取消编辑
$("#B0CCFC_cancel_t_param_value_Edit").click(function () {
	layer.close(B0CCFC_param["ly_index"]);
	B0CCFC_clear_validate();
	$("[id^='B0CCFC_div']").hide();
})

//清除查找框
function B0CCFC_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B0CCFC_clear_validate(){
	$("#B0CCFC_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B0CCFC_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B0CCFC_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B0CCFC_clear_edit_info(){
	var inputs = $("#B0CCFC_DataModal").find('input');
	var selects = $("#B0CCFC_DataModal").find("select");
	var textareas = $("#B0CCFC_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	B0CCFC_init_t_param_value();
}

//页面输入框赋值
function B0CCFC_get_edit_info(){
	var rowData = $("#A0CCFC_t_param_value_Events").bootstrapTable('getData')[A0CCFC_select_t_param_value_rowId];
	var inputs = $("#B0CCFC_DataModal").find('input');
	var selects = $("#B0CCFC_DataModal").find("select");
	var textareas = $("#B0CCFC_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"A0CCFC","B0CCFC");
}

//form验证
function B0CCFC_checkFormInput() {
    B0CCFC_validate = $("#B0CCFC_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B0CCFC_main_id: {}            ,B0CCFC_param_key: {}            ,B0CCFC_param_value: {}            ,B0CCFC_param_name: {}            ,B0CCFC_s_desc: {}            ,B0CCFC_create_date: {date: true,required : true,maxlength:19}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B0CCFC_main_id: {}            ,B0CCFC_param_key: {}            ,B0CCFC_param_value: {}            ,B0CCFC_param_name: {}            ,B0CCFC_s_desc: {}            ,B0CCFC_create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B0CCFC_SubmitForm();
        	return false;
        }
    })
}