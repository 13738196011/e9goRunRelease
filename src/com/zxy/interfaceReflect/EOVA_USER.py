#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.common.EncryptUtil import EncryptUtil
from com.zxy.z_debug import z_debug
class EOVA_USER(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None  
    param_value2     = None  #密码加密
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = "1"
    def __init__(self):
        pass
    def init_start(self):       
        if Com_Para.EncryptUtilType == 1:
            self.param_value2 = EncryptUtil.getSM32(self.param_value2)
        elif Com_Para.EncryptUtilType == 2:
            self.param_value2 = EncryptUtil.getMd5(self.param_value2).lower()
        elif Com_Para.EncryptUtilType == 3:
            self.param_value2 = EncryptUtil.getSha1(self.param_value2)
        if self.param_name == "T01_upd_pwd":
            if Com_Para.EncryptUtilType == 1:
                self.param_value1 = EncryptUtil.getSM32(self.param_value1)
            elif Com_Para.EncryptUtilType == 2:
                self.param_value1 = EncryptUtil.getMd5(self.param_value1).lower()
            elif Com_Para.EncryptUtilType == 3:
                self.param_value1 = EncryptUtil.getSha1(self.param_value1)
            self.strResult = ""
            self.strContinue = "1"
            if str(Com_Fun.getLogin_Id(self.session_id)) != self.param_value3 :
                self.strResult = "{\"T01_upd_pwd\": [{\"s_result\": 0, \"error_desc\": \"error user info!\"}]}"
                self.strContinue = "0"
        else:
            temOpd = Ope_DB_Cent()
            if temOpd.Get_eova_user_rid(Com_Fun.getLogin_Id(self.session_id)) != 1:
                self.strResult = "{\""+self.param_name+"\": [{\"s_result\": 0, \"error_desc\": \"禁止操作，用户权限不足!\"}]}"
                self.strContinue = "0"
