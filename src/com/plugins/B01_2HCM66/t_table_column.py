#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_table_column(object):
    attMAIN_ID = -1
    attTABLE_ID = 0
    attCOLUMN_CN_NAME = ""
    attCOLUMN_EN_NAME = ""
    attCOLUMN_TYPE = 0
    attCOLUMN_LENGTH = 0
    attCOLUMN_QRY = 0
    attCOLUMN_QRY_FORMAT = ""
    attCOLUMN_FIND_HTML = ""
    attCOLUMN_FIND_RETURN = ""
    attCREATE_DATE = None
    attS_DESC = ""
    lenCOLUMN_CN_NAME = 50
    lenCOLUMN_EN_NAME = 50
    lenCOLUMN_QRY_FORMAT = 2000
    lenCOLUMN_FIND_HTML = 50
    lenCOLUMN_FIND_RETURN = 500
    lenS_DESC = 500
    def __init__(self):
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["MAIN_ID"] = "主键"
        temHt["TABLE_ID"] = "元数据表"
        temHt["COLUMN_CN_NAME"] = "字段中文名"
        temHt["COLUMN_EN_NAME"] = "字段英文名"
        temHt["COLUMN_TYPE"] = "字段类型"
        temHt["COLUMN_LENGTH"] = "字段长度"
        temHt["COLUMN_QRY"] = "是否查询条件"
        temHt["COLUMN_QRY_FORMAT"] = "下拉框或查找框数据集"
        temHt["COLUMN_FIND_HTML"] = "查找框子页面"
        temHt["COLUMN_FIND_RETURN"] = "查找框返回字段"
        temHt["CREATE_DATE"] = "系统时间"
        temHt["S_DESC"] = "系统备注"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["COLUMN_EN_NAME"] = "字段英文名"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["TABLE_ID"] = "select MAIN_ID,TABLE_CN_NAME || TABLE_EN_NAME as CN_NAME from t_single_table"
        temHtSelect["COLUMN_TYPE"] = [{"MAIN_ID":"1","CN_NAME":"STRING"},{"MAIN_ID":"2","CN_NAME":"INT"},{"MAIN_ID":"3","CN_NAME":"FLOAT"},{"MAIN_ID":"4","CN_NAME":"DATE"}]
        temHtSelect["COLUMN_QRY"] = [{"MAIN_ID":"0","CN_NAME":""},{"MAIN_ID":"1","CN_NAME":"输入框"},{"MAIN_ID":"2","CN_NAME":"下拉框"},{"MAIN_ID":"3","CN_NAME":"查找框"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        temHtFind["TABLE_ID"] = "t_single_table.vue"
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        temHtFReturn["TABLE_ID"] = ["MAIN_ID","TABLE_EN_NAME"]
        return temHtFReturn
