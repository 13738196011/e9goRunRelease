//按钮事件新增或编辑
var BD0AFB_type = "";
//其他页面传到本页面参数
var BD0AFB_param = {};
//暂时没用
var BD0AFB_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BD0AFB_ary_SUB_USER_ID = null;var BD0AFB_ary_PROC_ID = null;var BD0AFB_ary_LIMIT_TAG = [{'MAIN_ID': '1', 'CN_NAME': '年'}, {'MAIN_ID': '2', 'CN_NAME': '月'}, {'MAIN_ID': '3', 'CN_NAME': '日'}];
/*declare select options end*/

//主从表传递参数
function BD0AFB_param_set(){
	/*Main Subsuv Table Param Begin*/
    if(BD0AFB_param.hasOwnProperty("PROC_ID_cn_name"))        $("#BD0AFB_find_PROC_ID_cn_name").val(s_decode(BD0AFB_param["PROC_ID_cn_name"]));    if(BD0AFB_param.hasOwnProperty("PROC_ID"))        $("#BD0AFB_find_PROC_ID").val(BD0AFB_param["PROC_ID"]);    if(BD0AFB_param.hasOwnProperty("hidden_find")){        $("#BD0AFB_Ope_PROC_ID").hide();        $("#BD0AFB_Clear_PROC_ID").hide();    }	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function BD0AFB_t_sub_userpower_biz_start(inputdata) {
	BD0AFB_param = inputdata;
	layer.close(ly_index);
	BD0AFB_param_set();
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_sub_userpower$SUB_USER_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BD0AFB_get_N01_t_sub_userpower$SUB_USER_ID");	
    /*biz end*/
}

/*biz step begin*/
function BD0AFB_format_SUB_USER_ID(value, row, index) {    var objResult = value;    for(i = 0; i < BD0AFB_ary_SUB_USER_ID.length; i++) {        var obj = BD0AFB_ary_SUB_USER_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BD0AFB_format_PROC_ID(value, row, index) {    var objResult = value;    for(i = 0; i < BD0AFB_ary_PROC_ID.length; i++) {        var obj = BD0AFB_ary_PROC_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BD0AFB_format_LIMIT_TAG(value, row, index) {    var objResult = value;    for(i = 0; i < BD0AFB_ary_LIMIT_TAG.length; i++) {        var obj = BD0AFB_ary_LIMIT_TAG[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BD0AFB_get_N01_t_sub_userpower$SUB_USER_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_sub_userpower$SUB_USER_ID) == false)        return false;    BD0AFB_ary_SUB_USER_ID = input.N01_t_sub_userpower$SUB_USER_ID;    if($("#BD0AFB_SUB_USER_ID").is("select") && $("#BD0AFB_SUB_USER_ID")[0].options.length == 0)    {        $.each(BD0AFB_ary_SUB_USER_ID, function (i, obj) {            addOptionValue("BD0AFB_SUB_USER_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    var inputdata = {        "param_name": "N01_t_proc_inparam$PROC_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BD0AFB_get_N01_t_proc_inparam$PROC_ID");}function BD0AFB_get_N01_t_proc_inparam$PROC_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_inparam$PROC_ID) == false)        return false;    BD0AFB_ary_PROC_ID = input.N01_t_proc_inparam$PROC_ID;    if($("#BD0AFB_PROC_ID").is("select") && $("#BD0AFB_PROC_ID")[0].options.length == 0)    {        $.each(BD0AFB_ary_PROC_ID, function (i, obj) {            addOptionValue("BD0AFB_PROC_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#BD0AFB_LIMIT_TAG").is("select") && $("#BD0AFB_LIMIT_TAG")[0].options.length == 0)    {        $.each(BD0AFB_ary_LIMIT_TAG, function (i, obj) {            addOptionValue("BD0AFB_LIMIT_TAG", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    BD0AFB_init_t_sub_userpower();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function BD0AFB_PROC_ID_cn_name_fun(){    index_subhtml = "t_proc_name.vue"    random_subhtml = "";    if(loadHtmlSubVueFun("dev_vue/t_proc_name.vue","BD0AFB_t_sub_userpower_call_vue") == true){        var n = Get_RandomDiv("","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#_t_proc_name_Events').bootstrapTable('resetView');                _param["ly_index"] = index;                _param["target_name"] = "BD0AFB_find_PROC_ID_cn_name"                _param["target_id"] = "BD0AFB_PROC_ID"                _param["sourc_id"] = "MAIN_ID"                _param["sourc_name"] = "INF_CN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function BD0AFB_page_end(){
	if(BD0AFB_param["type"] == "edit"){
		BD0AFB_get_edit_info();
	}
}

//页面初始化方法
function BD0AFB_init_t_sub_userpower() {
	//type = getUrlParam("type");
	if(BD0AFB_param["type"] == "add"){
		$("#BD0AFB_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BD0AFB_SUB_USER_ID").val(BD0AFB_param["SUB_USER_ID"]);        $("#BD0AFB_PROC_ID").val(BD0AFB_param["PROC_ID"]);        $("#BD0AFB_find_PROC_ID_cn_name").val(BD0AFB_param["PROC_ID_cn_name"]);		
		/*Get Find Select param end*/	
	}
	else if(BD0AFB_param["type"] == "edit"){
	
	}
	
	//表单验证
	BD0AFB_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BD0AFB_LIMIT_DATE").val() == "")    {        $("#BD0AFB_LIMIT_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BD0AFB_LIMIT_DATE',        type: 'datetime',        trigger: 'click'    });    if($("#BD0AFB_FIRST_DATE").val() == "")    {        $("#BD0AFB_FIRST_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BD0AFB_FIRST_DATE',        type: 'datetime',        trigger: 'click'    });    if($("#BD0AFB_CREATE_DATE").val() == "")    {        $("#BD0AFB_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BD0AFB_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BD0AFB_page_end();
}

//提交表单数据
function BD0AFB_SubmitForm(){
	if(BD0AFB_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_sub_userpower",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#BD0AFB_SUB_USER_ID").val()                ,"param_value2": $("#BD0AFB_PROC_ID").val()                ,"param_value3": $("#BD0AFB_LIMIT_DATE").val()                ,"param_value4": $("#BD0AFB_USE_LIMIT").val()                ,"param_value5": $("#BD0AFB_LIMIT_NUMBER").val()                ,"param_value6": $("#BD0AFB_LIMIT_TAG").val()                ,"param_value7": $("#BD0AFB_FIRST_DATE").val()                ,"param_value8": s_encode($("#BD0AFB_S_DESC").val())                ,"param_value9": $("#BD0AFB_CREATE_DATE").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BD0AFB_get_N01_ins_t_sub_userpower");
	}
	else if(BD0AFB_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_sub_userpower",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#BD0AFB_SUB_USER_ID").val()                ,"param_value2": $("#BD0AFB_PROC_ID").val()                ,"param_value3": $("#BD0AFB_LIMIT_DATE").val()                ,"param_value4": $("#BD0AFB_USE_LIMIT").val()                ,"param_value5": $("#BD0AFB_LIMIT_NUMBER").val()                ,"param_value6": $("#BD0AFB_LIMIT_TAG").val()                ,"param_value7": $("#BD0AFB_FIRST_DATE").val()                ,"param_value8": s_encode($("#BD0AFB_S_DESC").val())                ,"param_value9": $("#BD0AFB_CREATE_DATE").val()                ,"param_value10": $("#BD0AFB_MAIN_ID").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BD0AFB_get_N01_upd_t_sub_userpower");
	}
}

//vue回调
function BD0AFB_t_sub_userpower_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_proc_name.vue"){        var n = Get_RandomDiv("",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":BD0AFB_type,                    "ly_index":index,                    "target_name":"BD0AFB_find_PROC_ID_cn_name",                    "target_id":"BD0AFB_find_PROC_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"INF_CN_NAME"                };                loadScript_hasparam("dev_vue/t_proc_name.js","_t_proc_name_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//for表单提交
$("#BD0AFB_save_t_sub_userpower_Edit").click(function () {
	$("form[name='BD0AFB_DataModal']").submit();
})

/*修改数据*/
function BD0AFB_get_N01_upd_t_sub_userpower(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_sub_userpower) == true)
	{
		swal("修改数据成功!", "", "success");
		AD0AFB_t_sub_userpower_query();
		BD0AFB_clear_validate();
		layer.close(BD0AFB_param["ly_index"]);
	}
}

/*添加数据*/
function BD0AFB_get_N01_ins_t_sub_userpower(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_sub_userpower) == true)
	{
		swal("添加数据成功!", "", "success");
		AD0AFB_t_sub_userpower_query();
		BD0AFB_clear_validate();
		layer.close(BD0AFB_param["ly_index"]);
	}
}

//取消编辑
$("#BD0AFB_cancel_t_sub_userpower_Edit").click(function () {
	layer.close(BD0AFB_param["ly_index"]);
	BD0AFB_clear_validate();
	$("[id^='BD0AFB_div']").hide();
})

//清除查找框
function BD0AFB_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BD0AFB_clear_validate(){
	$("#BD0AFB_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BD0AFB_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BD0AFB_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BD0AFB_clear_edit_info(){
	var inputs = $("#BD0AFB_DataModal").find('input');
	var selects = $("#BD0AFB_DataModal").find("select");
	var textareas = $("#BD0AFB_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
    BD0AFB_clear_input_cn_name('BD0AFB_find_PROC_ID_cn_name','BD0AFB_PROC_ID')	
	/*input validate clear end*/
	BD0AFB_init_t_sub_userpower();
}

//页面输入框赋值
function BD0AFB_get_edit_info(){
	var rowData = $("#AD0AFB_t_sub_userpower_Events").bootstrapTable('getData')[AD0AFB_select_t_sub_userpower_rowId];
	var inputs = $("#BD0AFB_DataModal").find('input');
	var selects = $("#BD0AFB_DataModal").find("select");
	var textareas = $("#BD0AFB_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"AD0AFB","BD0AFB");
}

//form验证
function BD0AFB_checkFormInput() {
    BD0AFB_validate = $("#BD0AFB_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BD0AFB_MAIN_ID: {}            ,BD0AFB_SUB_USER_ID: {}            ,BD0AFB_PROC_ID: {}            ,BD0AFB_LIMIT_DATE: {date: true,required : true,maxlength:19}            ,BD0AFB_USE_LIMIT: {digits: true,required : true,maxlength:10}            ,BD0AFB_LIMIT_NUMBER: {digits: true,required : true,maxlength:10}            ,BD0AFB_LIMIT_TAG: {}            ,BD0AFB_FIRST_DATE: {date: true,required : true,maxlength:19}            ,BD0AFB_S_DESC: {}            ,BD0AFB_CREATE_DATE: {date: true,required : true,maxlength:19}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BD0AFB_MAIN_ID: {}            ,BD0AFB_SUB_USER_ID: {}            ,BD0AFB_PROC_ID: {}            ,BD0AFB_LIMIT_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BD0AFB_USE_LIMIT: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,BD0AFB_LIMIT_NUMBER: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,BD0AFB_LIMIT_TAG: {}            ,BD0AFB_FIRST_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BD0AFB_S_DESC: {}            ,BD0AFB_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BD0AFB_SubmitForm();
        	return false;
        }
    })
}