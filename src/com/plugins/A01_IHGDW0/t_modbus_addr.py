#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_modbus_addr(object):
    attmain_id = -1
    attstation_id = -1
    attmodbus_gateway = "1"
    attcom_port = ""
    attmodbus_addr = 0
    attmodbus_begin = 0
    attmodbus_length = 0
    attelement_id = -1
    attprotocol_id = -1
    attcreate_date = None
    atts_desc = ""
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")