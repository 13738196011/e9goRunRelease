//选择某一行
var AB2B81_select_t_server_ip_rowId = "";
//按钮事件新增或编辑
var AB2B81_type = "";
//其他页面传到本页面参数
var AB2B81_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var AB2B81_tem_up_protocol_id = "0";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var AB2B81_ary_up_protocol_id = null;
/*declare select options end*/

//业务逻辑数据开始
function AB2B81_t_server_ip_biz_start(inputparam) {
	layer.close(ly_index);
	AB2B81_param = inputparam;
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_server_ip$up_protocol_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AB2B81_get_N01_t_server_ip$up_protocol_id");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function AB2B81_format_up_protocol_id(value, row, index) {    var objResult = value;    for(i = 0; i < AB2B81_ary_up_protocol_id.length; i++) {        var obj = AB2B81_ary_up_protocol_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AB2B81_get_N01_t_server_ip$up_protocol_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_server_ip$up_protocol_id) == false)        return false;    AB2B81_ary_up_protocol_id = input.N01_t_server_ip$up_protocol_id;    $("#AB2B81_qry_up_protocol_id").append("<option value='-1'></option>")    $.each(AB2B81_ary_up_protocol_id, function (i, obj) {        addOptionValue("AB2B81_qry_up_protocol_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    AB2B81_init_t_server_ip();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function AB2B81_page_end(){
}

//上传地址设置显示列定义
var AB2B81_t_server_ip = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '上传平台IP',        field: 'SERVER_IP',        sortable: true        ,formatter: set_s_decode    },    {        title: '上传平台端口号',        field: 'SERVER_PORT',        sortable: true        ,formatter: set_s_decode    },    {        title: '上传协议名称',        field: 'SERVER_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '上传标识位(不得超过整数10)',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '上传协议模块',        field: 'UP_PROTOCOL_ID',        sortable: true        ,formatter: AB2B81_format_up_protocol_id    }	
	/*table column end*/
];

//页面初始化
function AB2B81_init_t_server_ip() {
	$(window).resize(function () {
		  $('#AB2B81_t_server_ip_Events').bootstrapTable('resetView');
	});
	//上传地址设置查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AB2B81_btn_t_server_ip_query').click();
}

//查询接口
function AB2B81_t_server_ip_query() {
    $('#AB2B81_t_server_ip_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_server_ip",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": AB2B81_tem_up_protocol_id        ,"param_value2": AB2B81_tem_up_protocol_id		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AB2B81_get_N01_sel_t_server_ip");
}

//查询结果
function AB2B81_get_N01_sel_t_server_ip(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_server_ip) == false)
        return false;    
	var s_data = input.N01_sel_t_server_ip;
    AB2B81_select_t_server_ip_rowId = "";
    $('#AB2B81_t_server_ip_Events').bootstrapTable('destroy');
    $("#AB2B81_t_server_ip_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AB2B81_t_server_ip_Events').find("tr.changeColor").removeClass('changeColor');
                AB2B81_select_t_server_ip_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AB2B81_t_server_ip_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AB2B81_select_t_server_ip_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AB2B81_param.hasOwnProperty("target_name"))
			{
				$("#"+AB2B81_param["target_id"]).val(eval("row."+AB2B81_param["sourc_id"].toUpperCase()));
				$("#"+AB2B81_param["target_name"]).val(s_decode(eval("row."+AB2B81_param["sourc_name"].toUpperCase())));				
				layer.close(AB2B81_param["ly_index"]);
			}
		},
        toolbar: "#AB2B81_t_server_ip_Toolbar",
        columns: AB2B81_t_server_ip,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AB2B81_btn_t_server_ip_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    AB2B81_tem_up_protocol_id = "0";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    BB2B81_ary_up_protocol_id = null;
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_server_ip$up_protocol_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AB2B81_get_N01_t_server_ip$up_protocol_id");	
    /*biz end*/
})

//查询按钮
$('#AB2B81_btn_t_server_ip_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    AB2B81_tem_up_protocol_id = $("#AB2B81_qry_up_protocol_id").val();	
    /*set query param end*/
	AB2B81_t_server_ip_query();
})

//vue回调
function AB2B81_t_server_ip_call_vue(objResult){
	if(index_subhtml == "t_server_ip_$.vue")
	{
		var n = Get_RandomDiv("BB2B81",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AB2B81_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temup_protocol_id = $("#AB2B81_qry_up_protocol_id").val();                if(temup_protocol_id != ""){                    inputdata["up_protocol_id"] = temup_protocol_id;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_server_ip_$.js","BB2B81_t_server_ip_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//新增按钮
$("#AB2B81_btn_t_server_ip_add").click(function () {
	AB2B81_type = "add";
	index_subhtml = "t_server_ip_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_server_ip_$.vue","AB2B81_t_server_ip_call_vue") == true){
		var n = Get_RandomDiv("BB2B81","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BB2B81_param["type"] = AB2B81_type;
				BB2B81_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temup_protocol_id = $("#AB2B81_qry_up_protocol_id").val();                if(temup_protocol_id != ""){                    BB2B81_param["up_protocol_id"] = temup_protocol_id;                }				
				/*Send Two FindSelect param end*/

				BB2B81_clear_edit_info();
			},
			end: function(){
				BB2B81_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AB2B81_btn_t_server_ip_edit").click(function () {
	if (AB2B81_select_t_server_ip_rowId != "") {
		AB2B81_type = "edit";
		index_subhtml = "t_server_ip_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_server_ip_$.vue","AB2B81_t_server_ip_call_vue") == true){
			var n = Get_RandomDiv("BB2B81","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BB2B81_param["type"] = AB2B81_type;
					BB2B81_param["ly_index"] = index;
					BB2B81_clear_edit_info();
					BB2B81_get_edit_info();
				},
				end: function(){
					BB2B81_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AB2B81_btn_t_server_ip_delete').click(function () {
	//单行选择
	var rowData = $("#AB2B81_t_server_ip_Events").bootstrapTable('getData')[AB2B81_select_t_server_ip_rowId];
	//多行选择
	var rowDatas = AB2B81_sel_row_t_server_ip();
	if (AB2B81_select_t_server_ip_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_server_ip",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AB2B81_N01_del_t_server_ip");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_server_ip",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AB2B81_N01_del_t_server_ip");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AB2B81_N01_del_t_server_ip(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_server_ip) == true)
		AB2B81_t_server_ip_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function AB2B81_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AB2B81_sel_row_t_server_ip(){
	//获得选中行
	var checkedbox= $("#AB2B81_t_server_ip_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}