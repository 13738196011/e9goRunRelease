//选择某一行
var AE35D9_select_t_table_column_rowId = "";
//按钮事件新增或编辑
var AE35D9_type = "";
//其他页面传到本页面参数
var AE35D9_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var AE35D9_tem_column_en_name = "";var AE35D9_tem_table_id = "-1";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var AE35D9_ary_table_id = null;var AE35D9_ary_column_type = [{'main_id': '1', 'cn_name': 'STRING'}, {'main_id': '2', 'cn_name': 'INT'}, {'main_id': '3', 'cn_name': 'FLOAT'}, {'main_id': '4', 'cn_name': 'DATE'}];var AE35D9_ary_column_qry = [{'main_id': '0', 'cn_name': ''}, {'main_id': '1', 'cn_name': '输入框'}, {'main_id': '2', 'cn_name': '下拉框'}, {'main_id': '3', 'cn_name': '查找框'}];
/*declare select options end*/

//业务逻辑数据开始
function AE35D9_t_table_column_biz_start(inputparam) {
	layer.close(ly_index);
	AE35D9_param = inputparam;
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_table_column$table_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE35D9_get_N01_t_table_column$table_id");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function AE35D9_format_table_id(value, row, index) {    var objResult = value;    for(i = 0; i < AE35D9_ary_table_id.length; i++) {        var obj = AE35D9_ary_table_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AE35D9_format_column_type(value, row, index) {    var objResult = value;    for(i = 0; i < AE35D9_ary_column_type.length; i++) {        var obj = AE35D9_ary_column_type[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AE35D9_format_column_qry(value, row, index) {    var objResult = value;    for(i = 0; i < AE35D9_ary_column_qry.length; i++) {        var obj = AE35D9_ary_column_qry[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AE35D9_get_N01_t_table_column$table_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_table_column$table_id) == false)        return false;    AE35D9_ary_table_id = input.N01_t_table_column$table_id;    AE35D9_init_t_table_column();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function AE35D9_table_id_cn_name_fun(){    index_subhtml = "t_single_table.html"    random_subhtml = "XXXXXX";    if(loadHtmlSubVueFun("biz_vue/t_single_table.html","AE35D9_t_table_column_call_vue") == true){        var n = Get_RandomDiv("XXXXXX","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#XXXXXX_t_single_table.html_Events').bootstrapTable('resetView');                XXXXXX_param["ly_index"] = index;                XXXXXX_param["target_name"] = "AE35D9_find_table_id_cn_name"                XXXXXX_param["target_id"] = "AE35D9_find_table_id"                XXXXXX_param["sourc_id"] = "MAIN_ID"                XXXXXX_param["sourc_name"] = "TABLE_EN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function AE35D9_page_end(){
}

//元数据字段管理显示列定义
var AE35D9_t_table_column = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '表名',        field: 'TABLE_ID',        sortable: true        ,formatter: AE35D9_format_table_id    },    {        title: '字段中文名',        field: 'COLUMN_CN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '字段英文名',        field: 'COLUMN_EN_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '字段类型',        field: 'COLUMN_TYPE',        sortable: true        ,formatter: AE35D9_format_column_type    },    {        title: '字段长度',        field: 'COLUMN_LENGTH',        sortable: true    },    {        title: '是否查询条件',        field: 'COLUMN_QRY',        sortable: true        ,formatter: AE35D9_format_column_qry    },    {        title: '下拉框或查找框数据集',        field: 'COLUMN_QRY_FORMAT',        sortable: true        ,formatter: set_s_decode    },    {        title: '查找框子页面',        field: 'COLUMN_FIND_HTML',        sortable: true        ,formatter: set_s_decode    },    {        title: '查找框返回字段',        field: 'COLUMN_FIND_RETURN',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '系统备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function AE35D9_init_t_table_column() {
	$(window).resize(function () {
		  $('#AE35D9_t_table_column_Events').bootstrapTable('resetView');
	});
	//元数据字段管理查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AE35D9_btn_t_table_column_query').click();
}

//查询接口
function AE35D9_t_table_column_query() {
    $('#AE35D9_t_table_column_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_table_column",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(AE35D9_tem_column_en_name)        ,"param_value2": AE35D9_tem_table_id        ,"param_value3": AE35D9_tem_table_id		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AE35D9_get_N01_sel_t_table_column");
}

//查询结果
function AE35D9_get_N01_sel_t_table_column(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_table_column) == false)
        return false;    
	var s_data = input.N01_sel_t_table_column;
    AE35D9_select_t_table_column_rowId = "";
    $('#AE35D9_t_table_column_Events').bootstrapTable('destroy');
    $("#AE35D9_t_table_column_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AE35D9_t_table_column_Events').find("tr.changeColor").removeClass('changeColor');
                AE35D9_select_t_table_column_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AE35D9_t_table_column_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AE35D9_select_t_table_column_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AE35D9_param.hasOwnProperty("target_name"))
			{
				$("#"+AE35D9_param["target_id"]).val(eval("row."+AE35D9_param["sourc_id"].toUpperCase()));
				$("#"+AE35D9_param["target_name"]).val(s_decode(eval("row."+AE35D9_param["sourc_name"].toUpperCase())));				
				layer.close(AE35D9_param["ly_index"]);
			}
		},
        toolbar: "#AE35D9_t_table_column_Toolbar",
        columns: AE35D9_t_table_column,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AE35D9_btn_t_table_column_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    AE35D9_tem_column_en_name = "";    AE35D9_tem_table_id = "-1";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    BE35D9_ary_table_id = null;    $("#AE35D9_find_table_id_cn_name").val("");    $("#AE35D9_find_table_id").val("-1");
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_table_column$table_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE35D9_get_N01_t_table_column$table_id");	
    /*biz end*/
})

//查询按钮
$('#AE35D9_btn_t_table_column_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    AE35D9_tem_column_en_name = $("#AE35D9_qry_column_en_name").val();    AE35D9_tem_table_id = $("#AE35D9_find_table_id").val();	
    /*set query param end*/
	AE35D9_t_table_column_query();
})

//vue回调
function AE35D9_t_table_column_call_vue(objResult){
	if(index_subhtml == "t_table_column_$.vue")
	{
		var n = Get_RandomDiv("BE35D9",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AE35D9_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temcolumn_en_name = $("#AE35D9_qry_column_en_name").val();                if(temcolumn_en_name != ""){                    inputdata["column_en_name"] = temcolumn_en_name;                }                var temtable_id = $("#AE35D9_find_table_id_cn_name").val();                if(temtable_id != ""){                    inputdata["table_id_cn_name"] = temtable_id;                    inputdata["table_id"] = $("#AE35D9_table_id").val();                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_table_column_$.js","BE35D9_t_table_column_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_single_table.html"){        var n = Get_RandomDiv("XXXXXX",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":AE35D9_type,                    "ly_index":index,                    "target_name":"AE35D9_find_table_id_cn_name",                    "target_id":"AE35D9_find_table_id",                    "sourc_id":"MAIN_ID",                    "sourc_name":"TABLE_EN_NAME"                };                loadScript_hasparam("biz_vue/t_single_table.html.js","XXXXXX_t_single_table.html_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//新增按钮
$("#AE35D9_btn_t_table_column_add").click(function () {
	AE35D9_type = "add";
	index_subhtml = "t_table_column_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_table_column_$.vue","AE35D9_t_table_column_call_vue") == true){
		var n = Get_RandomDiv("BE35D9","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BE35D9_param["type"] = AE35D9_type;
				BE35D9_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temcolumn_en_name = $("#AE35D9_qry_column_en_name").val();                if(temcolumn_en_name != ""){                    BE35D9_param["column_en_name"] = temcolumn_en_name;                }                var temtable_id = $("#AE35D9_find_table_id_cn_name").val();                if(temtable_id != ""){                    BE35D9_param["table_id_cn_name"] = temtable_id;                    BE35D9_param["table_id"] = $("#AE35D9_table_id").val();                }				
				/*Send Two FindSelect param end*/

				BE35D9_clear_edit_info();
			},
			end: function(){
				BE35D9_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AE35D9_btn_t_table_column_edit").click(function () {
	if (AE35D9_select_t_table_column_rowId != "") {
		AE35D9_type = "edit";
		index_subhtml = "t_table_column_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_table_column_$.vue","AE35D9_t_table_column_call_vue") == true){
			var n = Get_RandomDiv("BE35D9","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BE35D9_param["type"] = AE35D9_type;
					BE35D9_param["ly_index"] = index;
					BE35D9_clear_edit_info();
					BE35D9_get_edit_info();
				},
				end: function(){
					BE35D9_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AE35D9_btn_t_table_column_delete').click(function () {
	//单行选择
	var rowData = $("#AE35D9_t_table_column_Events").bootstrapTable('getData')[AE35D9_select_t_table_column_rowId];
	//多行选择
	var rowDatas = AE35D9_sel_row_t_table_column();
	if (AE35D9_select_t_table_column_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_table_column",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AE35D9_N01_del_t_table_column");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_table_column",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AE35D9_N01_del_t_table_column");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AE35D9_N01_del_t_table_column(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_table_column) == true)
		AE35D9_t_table_column_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function AE35D9_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AE35D9_sel_row_t_table_column(){
	//获得选中行
	var checkedbox= $("#AE35D9_t_table_column_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}