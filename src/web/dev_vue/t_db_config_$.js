//按钮事件新增或编辑
var BF8DA3_type = "";
//其他页面传到本页面参数
var BF8DA3_param = {};
//暂时没用
var BF8DA3_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

//主从表传递参数
function BF8DA3_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function BF8DA3_t_db_config_biz_start(inputdata) {
	BF8DA3_param = inputdata;
	layer.close(ly_index);
	BF8DA3_param_set();
    /*biz begin*/
    BF8DA3_init_t_db_config()	
    /*biz end*/
}

/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function BF8DA3_page_end(){
	if(BF8DA3_param["type"] == "edit"){
		BF8DA3_get_edit_info();
	}
}

//页面初始化方法
function BF8DA3_init_t_db_config() {
	//type = getUrlParam("type");
	if(BF8DA3_param["type"] == "add"){
		$("#BF8DA3_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
		
		/*Get Find Select param end*/	
	}
	else if(BF8DA3_param["type"] == "edit"){
	
	}
	
	//表单验证
	BF8DA3_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BF8DA3_CREATE_DATE").val() == "")    {        $("#BF8DA3_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BF8DA3_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BF8DA3_page_end();
}

//提交表单数据
function BF8DA3_SubmitForm(){
	if(BF8DA3_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_db_config",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#BF8DA3_DB_CN_NAME").val())                ,"param_value2": s_encode($("#BF8DA3_DB_DRiverClassName").val())                ,"param_value3": s_encode($("#BF8DA3_DB_url").val())                ,"param_value4": s_encode($("#BF8DA3_DB_username").val())                ,"param_value5": s_encode($("#BF8DA3_DB_password").val())                ,"param_value6": s_encode($("#BF8DA3_DB_version").val())                ,"param_value7": s_encode($("#BF8DA3_DB_code").val())                ,"param_value8": s_encode($("#BF8DA3_S_DESC").val())                ,"param_value9": $("#BF8DA3_CREATE_DATE").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BF8DA3_get_N01_ins_t_db_config");
	}
	else if(BF8DA3_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_db_config",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#BF8DA3_DB_CN_NAME").val())                ,"param_value2": s_encode($("#BF8DA3_DB_DRiverClassName").val())                ,"param_value3": s_encode($("#BF8DA3_DB_url").val())                ,"param_value4": s_encode($("#BF8DA3_DB_username").val())                ,"param_value5": s_encode($("#BF8DA3_DB_password").val())                ,"param_value6": s_encode($("#BF8DA3_DB_version").val())                ,"param_value7": s_encode($("#BF8DA3_DB_code").val())                ,"param_value8": s_encode($("#BF8DA3_S_DESC").val())                ,"param_value9": $("#BF8DA3_CREATE_DATE").val()                ,"param_value10": $("#BF8DA3_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BF8DA3_get_N01_upd_t_db_config");
	}
}

//vue回调
function BF8DA3_t_db_config_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#BF8DA3_save_t_db_config_Edit").click(function () {
	$("form[name='BF8DA3_DataModal']").submit();
})

/*修改数据*/
function BF8DA3_get_N01_upd_t_db_config(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_db_config) == true)
	{
		swal("修改数据成功!", "", "success");
		AF8DA3_t_db_config_query();
		BF8DA3_clear_validate();
		layer.close(BF8DA3_param["ly_index"]);
	}
}

/*添加数据*/
function BF8DA3_get_N01_ins_t_db_config(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_db_config) == true)
	{
		swal("添加数据成功!", "", "success");
		AF8DA3_t_db_config_query();
		BF8DA3_clear_validate();
		layer.close(BF8DA3_param["ly_index"]);
	}
}

//取消编辑
$("#BF8DA3_cancel_t_db_config_Edit").click(function () {
	layer.close(BF8DA3_param["ly_index"]);
	BF8DA3_clear_validate();
	$("[id^='BF8DA3_div']").hide();
})

//清除查找框
function BF8DA3_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BF8DA3_clear_validate(){
	$("#BF8DA3_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BF8DA3_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BF8DA3_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BF8DA3_clear_edit_info(){
	var inputs = $("#BF8DA3_DataModal").find('input');
	var selects = $("#BF8DA3_DataModal").find("select");
	var textareas = $("#BF8DA3_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	BF8DA3_init_t_db_config();
}

//页面输入框赋值
function BF8DA3_get_edit_info(){
	var rowData = $("#AF8DA3_t_db_config_Events").bootstrapTable('getData')[AF8DA3_select_t_db_config_rowId];
	var inputs = $("#BF8DA3_DataModal").find('input');
	var selects = $("#BF8DA3_DataModal").find("select");
	var textareas = $("#BF8DA3_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"AF8DA3","BF8DA3");
}

//form验证
function BF8DA3_checkFormInput() {
    BF8DA3_validate = $("#BF8DA3_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BF8DA3_main_id: {}            ,BF8DA3_DB_CN_NAME: {}            ,BF8DA3_DB_DRiverClassName: {}            ,BF8DA3_DB_url: {}            ,BF8DA3_DB_username: {}            ,BF8DA3_DB_password: {}            ,BF8DA3_DB_version: {}            ,BF8DA3_DB_code: {}            ,BF8DA3_S_DESC: {}            ,BF8DA3_CREATE_DATE: {date: true,required : true,maxlength:19}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BF8DA3_main_id: {}            ,BF8DA3_DB_CN_NAME: {}            ,BF8DA3_DB_DRiverClassName: {}            ,BF8DA3_DB_url: {}            ,BF8DA3_DB_username: {}            ,BF8DA3_DB_password: {}            ,BF8DA3_DB_version: {}            ,BF8DA3_DB_code: {}            ,BF8DA3_S_DESC: {}            ,BF8DA3_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BF8DA3_SubmitForm();
        	return false;
        }
    })
}