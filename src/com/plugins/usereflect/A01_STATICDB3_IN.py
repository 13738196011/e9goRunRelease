#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''
import json,os,importlib
from com.zxy.common import Com_Para
from com.zxy.db_Self.Db_Common_Self import Db_Common_Self
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.z_debug import z_debug
class A01_STATICDB3_IN(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = 1
    attRDA             = ""
    attRBC             = ""
    attTableName       = ""
    temAtts = []
    temMainSub = []
    temLen = {}
    temAttCN = {}
    temQryAtt = {}
    temSelectAtt = {}
    temFindAtt = {}
    temFReturnAtt = {}
    attFFD = "biz_vue"
    objN = None
    def __init__(self):
        self.temAtts = []
        self.temLen = {}
        self.temAttCN = {}
        self.temQryAtt = {}
        self.temSelectAtt = {}
        self.temFindAtt = {}
        self.temFReturnAtt = {}
        self.objN = None
    def init_start(self):
        try:
            self.attRDA = "A"+Com_Fun.Get_New_GUID()[0:5]
            self.attRBC = "B"+Com_Fun.Get_New_GUID()[0:5]
            if self.param_value9 != "":
                self.attFFD = self.param_value9
            opc = Ope_DB_Cent()
            opc.Create_Table3(self.param_value1,self.param_value2,self.param_value3,self.param_value4)            
            opc.Create_StaticView_Interface(self.param_value1,self.param_value2,self.param_value3,self.param_value4,"N01")
            self.Create_Web_Html_bootstrap()
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+repr(e)
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
                temLog = self.debug_info()+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        self.strContinue = 0
    def Create_Web_Html_bootstrap(self):
        temAryCP = self.param_value2.split(".")
        if self.param_value2 != "" and len(temAryCP) >= 1:
            temTABLE_NAME = temAryCP[len(temAryCP) - 1]
            self.attTableName = temTABLE_NAME
            templatePath = Com_Para.ApplicationPath +Com_Para.zxyPath+ "web"+Com_Para.zxyPath+ "template"
            tempTargetPath = Com_Para.ApplicationPath +Com_Para.zxyPath+ "web"+Com_Para.zxyPath+ self.attFFD
            if os.path.exists(tempTargetPath) == False:
                os.mkdir(tempTargetPath)
            self.init_att(temTABLE_NAME)
            self.WriteBizHtml_1(templatePath,tempTargetPath,temTABLE_NAME)
            self.WriteBizHtml_2(templatePath,tempTargetPath,temTABLE_NAME)
            self.WriteBizJs_1(templatePath,tempTargetPath,temTABLE_NAME)
            self.WriteBizJs_2(templatePath,tempTargetPath,temTABLE_NAME)
    def UpdRandom(self,temTABLE_NAME):
        temDb_dbc = Db_Common2()
        temStrSql = "update menu_info set S_DESC = '"+self.attRDA+"' where menu_url like '"+self.attFFD+"%"+temTABLE_NAME+".vue%'" 
        temDb_dbc.CommonExec_Sql(temStrSql)
    def GetRandom(self,temTABLE_NAME):
        temResult = "XXXXXX"
        temDb_dbc = Db_Common2()
        temStrSql = "select S_DESC from menu_info where menu_url like '"+self.attFFD+"%"+temTABLE_NAME+"%'" 
        temResultSet = temDb_dbc.Common_Sql(temStrSql)
        for temObj in temResultSet:
            temResult = Com_Fun.NoNull(temObj[0])
        return temResult
    def init_att(self,temTABLE_NAME):        
        objC = importlib.import_module(self.param_value2)  
        self.objN = getattr(objC,temTABLE_NAME)
        for att in self.objN.__dict__:
            if str(att).find("att") == 0:
                self.temAtts.append(str(att)[3:len(str(att))])
            elif str(att).find("len") == 0:
                self.temLen[str(att)[3:len(str(att))]] = str(getattr(self.objN,att))        
        fun_us = getattr(self.objN,"Get_Att_CN")    #利用反射调用对象中的属性func_name
        self.temAttCN = fun_us(self.objN)        
        fun_us = getattr(self.objN,"Qry_Att_CN")
        self.temQryAtt = fun_us(self.objN)    
        fun_us = getattr(self.objN,"Select_Att_CN")
        self.temSelectAtt = fun_us(self.objN)    
        fun_us = getattr(self.objN,"Find_Att_CN")
        self.temFindAtt = fun_us(self.objN)
        fun_us = getattr(self.objN,"FReturn_Att_CN")
        self.temFReturnAtt = fun_us(self.objN)
        odc = Ope_DB_Cent()
        self.temMainSub = odc.GetMainSubInfo(temTABLE_NAME,self.attFFD)
    def WriteBizJs_2(self,templatePath,tempTargetPath,temTABLE_NAME):
        readFile = None
        writeFile = None            
        try: 
            if os.path.exists(templatePath+Com_Para.zxyPath+"template_table3_$.js") == False :
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"template_table3_$.js模板文件不存在"+ "\"}]}"
                return ""
            else:
                readFile = open(templatePath+Com_Para.zxyPath+"template_table3_$.js", 'r',encoding=Com_Para.U_CODE)
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+"_$.js") == False :
                writeFile = open(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+"_$.js", 'a',encoding=Com_Para.U_CODE)
            else:
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\""+temTABLE_NAME+"_$.js文件已存在"+ "\"}]}"
                return ""
            while True and readFile is not None and writeFile is not None:
                temLines = readFile.readline()
                if self.param_value3 != "":
                    temLines = temLines.replace("{template_table_CN}",self.param_value3).replace("{$RDA$}",self.attRDA).replace("{$RBC$}",self.attRBC).replace("{$FFD$}",self.attFFD)
                temLines = temLines.replace("{template_table_EN}",temTABLE_NAME).replace("{$RDA$}",self.attRDA).replace("{$RBC$}",self.attRBC).replace("{$FFD$}",self.attFFD)
                writeFile.write(temLines)                
                if temLines.find("*declare select options begin*") != -1:
                    for key in list(self.temSelectAtt.keys()):
                        temValue = Com_Fun.GetHashTable(self.temSelectAtt,key)
                        if isinstance(temValue,str):
                            writeFile.write("\rvar "+self.attRBC+"_ary_"+key+" = null;")
                        elif isinstance(temValue,list):
                            writeFile.write("\rvar "+self.attRBC+"_ary_"+key+" = "+str(temValue)+";")
                elif temLines.find("*refresh select options begin*") != -1:
                    for key in list(self.temSelectAtt.keys()):
                        temValue = Com_Fun.GetHashTable(self.temSelectAtt,key)
                        if isinstance(temValue,str):
                            writeFile.write("\r    "+self.attRBC+"_ary_"+key+" = null;")
                elif temLines.find("*biz begin*") != -1:
                    if len(self.temSelectAtt) == 0:                  
                        writeFile.write("\r    "+self.attRBC+"_init_"+temTABLE_NAME+"()")
                    else:
                        bAjax = False
                        for key in list(self.temSelectAtt.keys()):
                            if isinstance(self.temSelectAtt[key],str) and self.temSelectAtt[key].find("select ") == 0:
                                writeFile.write("\r    var inputdata = {")
                                writeFile.write("\r        \"param_name\": \"N01_"+temTABLE_NAME+"$"+key+"\",")
                                writeFile.write("\r        \"session_id\": session_id,")
                                writeFile.write("\r        \"login_id\": login_id")
                                writeFile.write("\r    };")
                                writeFile.write("\r    ly_index = layer.load();")
                                writeFile.write("\r    get_ajax_baseurl(inputdata, \""+self.attRBC+"_get_N01_"+temTABLE_NAME+"$"+key+"\");")                                                          
                                bAjax = True
                                break
                            elif isinstance(self.temSelectAtt[key],str) and self.temSelectAtt[key].find("select ") == -1:
                                writeFile.write("\r    var inputdata = {")
                                writeFile.write("\r        \"param_name\": \""+self.temSelectAtt[key]+"\",")
                                writeFile.write("\r        \"session_id\": session_id,")
                                writeFile.write("\r        \"login_id\": login_id")
                                writeFile.write("\r    };")
                                writeFile.write("\r    ly_index = layer.load();")
                                writeFile.write("\r    get_ajax_baseurl(inputdata, \""+self.attRBC+"_get_"+self.temSelectAtt[key]+"\");")                                                          
                                bAjax = True
                                break
                        if bAjax == False:
                            for key in list(self.temSelectAtt.keys()): 
                                if isinstance(self.temSelectAtt[key],list): 
                                    writeFile.write("\r    $.each("+self.attRBC+"_ary_"+key+", function (i, obj) {")
                                    writeFile.write("\r        addOptionValue(\""+self.attRBC+"_"+key+"\", obj[GetLowUpp(\"main_id\")], obj[GetLowUpp(\"cn_name\")]);")
                                    writeFile.write("\r    });")                            
                            writeFile.write("\r    "+self.attRBC+"_init_"+temTABLE_NAME+"()")
                elif temLines.find("*biz step begin*") != -1:
                    for key in list(self.temSelectAtt.keys()):                   
                        writeFile.write("\rfunction "+self.attRBC+"_format_"+key+"(value, row, index) {")
                        writeFile.write("\r    var objResult = value;")
                        writeFile.write("\r    for(i = 0; i < "+self.attRBC+"_ary_"+key+".length; i++) {")
                        writeFile.write("\r        var obj = "+self.attRBC+"_ary_"+key+"[i];")
                        writeFile.write("\r        if (obj[GetLowUpp(\"main_id\")].toString() == value.toString()) {")
                        writeFile.write("\r            objResult = obj[GetLowUpp(\"cn_name\")];")
                        writeFile.write("\r            break;")
                        writeFile.write("\r        }")
                        writeFile.write("\r    }")
                        writeFile.write("\r    return objResult;")
                        writeFile.write("\r}")
                        writeFile.write("\r")                    
                    bFlag = False
                    iIndex = 0                         
                    for key in list(self.temSelectAtt.keys()):   
                        if isinstance(self.temSelectAtt[key],str)  and self.temSelectAtt[key].find("select ") == 0:
                            if bFlag == True:
                                writeFile.write("\r    var inputdata = {")
                                writeFile.write("\r        \"param_name\": \"N01_"+temTABLE_NAME+"$"+key+"\",")
                                writeFile.write("\r        \"session_id\": session_id,")
                                writeFile.write("\r        \"login_id\": login_id")
                                writeFile.write("\r    };")
                                writeFile.write("\r    ly_index = layer.load();")
                                writeFile.write("\r    get_ajax_baseurl(inputdata, \""+self.attRBC+"_get_N01_"+temTABLE_NAME+"$"+key+"\");")
                                writeFile.write("\r}")            
                            writeFile.write("\r")                                            
                            writeFile.write("\rfunction "+self.attRBC+"_get_N01_"+temTABLE_NAME+"$"+key+"(input) {")
                            writeFile.write("\r    layer.close(ly_index);")
                            writeFile.write("\r    //查询失败")
                            writeFile.write("\r    if (Call_QryResult(input.N01_"+temTABLE_NAME+"$"+key+") == false)")
                            writeFile.write("\r        return false;")
                            writeFile.write("\r    "+self.attRBC+"_ary_"+key+" = input.N01_"+temTABLE_NAME+"$"+key+";")                           
                            writeFile.write("\r    if($(\"#"+self.attRBC+"_"+key+"\").is(\"select\") && $(\"#"+self.attRBC+"_"+key+"\")[0].options.length == 0)")
                            writeFile.write("\r    {")
                            writeFile.write("\r        $.each("+self.attRBC+"_ary_"+key+", function (i, obj) {")
                            writeFile.write("\r            addOptionValue(\""+self.attRBC+"_"+key+"\", obj[GetLowUpp(\"main_id\")], obj[GetLowUpp(\"cn_name\")]);")
                            writeFile.write("\r        });")
                            writeFile.write("\r    }")
                            iIndex = iIndex + 1
                            bFlag = True     
                        elif isinstance(self.temSelectAtt[key],str) and self.temSelectAtt[key].find("select ") == -1:
                            if bFlag == True:
                                writeFile.write("\r    var inputdata = {")
                                writeFile.write("\r        \"param_name\": \""+self.temSelectAtt[key]+"\",")
                                writeFile.write("\r        \"session_id\": session_id,")
                                writeFile.write("\r        \"login_id\": login_id")
                                writeFile.write("\r    };")
                                writeFile.write("\r    ly_index = layer.load();")
                                writeFile.write("\r    get_ajax_baseurl(inputdata, \""+self.attRBC+"_get_"+self.temSelectAtt[key]+"\");")
                                writeFile.write("\r}")            
                            writeFile.write("\r")                                            
                            writeFile.write("\rfunction "+self.attRBC+"_get_"+self.temSelectAtt[key]+"(input) {")
                            writeFile.write("\r    layer.close(ly_index);")
                            writeFile.write("\r    //查询失败")
                            writeFile.write("\r    if (Call_QryResult(input."+self.temSelectAtt[key]+") == false)")
                            writeFile.write("\r        return false;")
                            writeFile.write("\r    "+self.attRBC+"_ary_"+key+" = input."+self.temSelectAtt[key]+";")                           
                            writeFile.write("\r    if($(\"#"+self.attRBC+"_"+key+"\").is(\"select\") && $(\"#"+self.attRBC+"_"+key+"\")[0].options.length == 0)")
                            writeFile.write("\r    {")
                            writeFile.write("\r        $.each("+self.attRBC+"_ary_"+key+", function (i, obj) {")
                            writeFile.write("\r            addOptionValue(\""+self.attRBC+"_"+key+"\", obj[GetLowUpp(\"main_id\")], obj[GetLowUpp(\"cn_name\")]);")
                            writeFile.write("\r        });")
                            writeFile.write("\r    }")
                            iIndex = iIndex + 1
                            bFlag = True                   
                    if bFlag:
                        for key in list(self.temSelectAtt.keys()): 
                            if isinstance(self.temSelectAtt[key],list): 
                                writeFile.write("\r    if($(\"#"+self.attRBC+"_"+key+"\").is(\"select\") && $(\"#"+self.attRBC+"_"+key+"\")[0].options.length == 0)")
                                writeFile.write("\r    {")
                                writeFile.write("\r        $.each("+self.attRBC+"_ary_"+key+", function (i, obj) {")
                                writeFile.write("\r            addOptionValue(\""+self.attRBC+"_"+key+"\", obj[GetLowUpp(\"main_id\")], obj[GetLowUpp(\"cn_name\")]);")
                                writeFile.write("\r        });")
                                writeFile.write("\r    }")
                        writeFile.write("\r    "+self.attRBC+"_init_"+temTABLE_NAME+"();")
                        writeFile.write("\r}")
                elif temLines.find("*form datetime init begin*") != -1:
                    writeFile.write(self.AppenFDIB())                
                elif temLines.find("*insert param begin*") != -1:
                    writeFile.write(self.AppendIPB())
                elif temLines.find("*update param begin*") != -1:
                    writeFile.write(self.AppendUPB())
                elif temLines.find("*input check rules begin*") != -1:
                    writeFile.write(self.AppendICRB())
                elif temLines.find("*input check messages begin*") != -1:
                    writeFile.write(self.AppendICMB())                
                elif temLines.find("*find qry fun begin*") != -1:
                    writeFile.write(self.AppenFQFB2())              
                elif temLines.find("*input validate clear begin*") != -1:
                    writeFile.write(self.AppenIVCB())  
                elif temLines.find("*get find subvue bgein*") != -1:
                    writeFile.write(self.AppenGFSB2())
                elif temLines.find("*Get Find Select param bgein*") != -1:
                    writeFile.write(self.AppenGFSPB())
                elif temLines.find("*Main Subsuv Table Param Begin*") != -1:
                    writeFile.write(self.AppenMSTPB2())
                    pass
                if not temLines:
                    break
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if readFile != None:
                readFile.close()
            if writeFile != None:
                writeFile.close()
    def AppendUPB(self):
        temResult = ""
        temIndex = 0
        for key in list(self.temAttCN.keys()):
            if key.upper() == "MAIN_ID":   
                pass
            else:
                temIndex = temIndex + 1
                if isinstance(getattr(self.objN,"att"+key),str):
                    temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": s_encode($(\"#"+self.attRBC+"_"+key+"\").val())"
                    pass
                elif isinstance(getattr(self.objN,"att"+key),int):
                    temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": $(\"#"+self.attRBC+"_"+key+"\").val()"
                    pass
                elif isinstance(getattr(self.objN,"att"+key),float):
                    temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": $(\"#"+self.attRBC+"_"+key+"\").val()"
                    pass
                elif getattr(self.objN,"att"+key) is None:
                    temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": $(\"#"+self.attRBC+"_"+key+"\").val()"
        temIndex = temIndex + 1
        temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": $(\"#"+self.attRBC+"_MAIN_ID\").val()"
        return temResult
    def AppendICRB(self):
        temResult = ""
        temIndex = 0
        for key in list(self.temAttCN.keys()):
            if key.upper() == "MAIN_ID":   
                temResult = temResult + "\r            "+self.attRBC+"_"+key+": {}"
                pass
            else:
                temIndex = temIndex + 1
                if isinstance(getattr(self.objN,"att"+key),str):
                    temResult = temResult + "\r            ,"+self.attRBC+"_"+key+": {}"
                    pass
                elif isinstance(getattr(self.objN,"att"+key),int):
                    if Com_Fun.GetHashTable(self.temSelectAtt, key) != "":
                        temResult = temResult + "\r            ,"+self.attRBC+"_"+key+": {}"
                    else:
                        temResult = temResult + "\r            ,"+self.attRBC+"_"+key+": {digits: true,required : true,maxlength:10}"
                    pass
                elif isinstance(getattr(self.objN,"att"+key),float):
                    if Com_Fun.GetHashTable(self.temSelectAtt, key) != "":
                        temResult = temResult + "\r            ,"+self.attRBC+"_"+key+": {}"
                    else:
                        temResult = temResult + "\r            ,"+self.attRBC+"_"+key+": {number: true,required : true,maxlength:14}"
                    pass
                elif getattr(self.objN,"att"+key) is None:
                    temResult = temResult + "\r            ,"+self.attRBC+"_"+key+": {date: true,required : true,maxlength:19}"
        return temResult
    def AppendICMB(self):
        temResult = ""
        temIndex = 0
        for key in list(self.temAttCN.keys()):
            if key.upper() == "MAIN_ID": 
                temResult = temResult + "\r            "+self.attRBC+"_"+key+": {}"  
                pass
            else:
                temIndex = temIndex + 1
                if isinstance(getattr(self.objN,"att"+key),str):
                    temResult = temResult + "\r            ,"+self.attRBC+"_"+key+": {}"
                    pass
                elif isinstance(getattr(self.objN,"att"+key),int):                    
                    if Com_Fun.GetHashTable(self.temSelectAtt, key) != "":
                        temResult = temResult + "\r            ,"+self.attRBC+"_"+key+": {}"
                    else:
                        temResult = temResult + "\r            ,"+self.attRBC+"_"+key+": {digits: \"必须输入整数\",required : \"必须输入整数\",maxlength:\"长度不能超过10\" }"
                    pass
                elif isinstance(getattr(self.objN,"att"+key),float):
                    if Com_Fun.GetHashTable(self.temSelectAtt, key) != "":
                        temResult = temResult + "\r            ,"+self.attRBC+"_"+key+": {}"
                    else:
                        temResult = temResult + "\r            ,"+self.attRBC+"_"+key+": {number: \"必须输入合法的小数\",required : \"必须输入合法的小数\",maxlength:\"长度不能超过14\"}"
                    pass
                elif getattr(self.objN,"att"+key) is None:
                    temResult = temResult + "\r            ,"+self.attRBC+"_"+key+": {date: \"必须输入正确格式的日期\",required : \"必须输入正确格式的日期\",maxlength:\"长度不能超过19\"}"
        return temResult
    def AppendIPB(self):
        temResult = ""
        temIndex = 0
        for key in list(self.temAttCN.keys()):
            if key.upper() == "MAIN_ID":   
                pass
            else:
                temIndex = temIndex + 1
                if isinstance(getattr(self.objN,"att"+key),str):
                    temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": s_encode($(\"#"+self.attRBC+"_"+key+"\").val())"
                    pass
                elif isinstance(getattr(self.objN,"att"+key),int):
                    temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": $(\"#"+self.attRBC+"_"+key+"\").val()"
                    pass
                elif isinstance(getattr(self.objN,"att"+key),float):
                    temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": $(\"#"+self.attRBC+"_"+key+"\").val()"
                    pass
                elif getattr(self.objN,"att"+key) is None:
                    temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": $(\"#"+self.attRBC+"_"+key+"\").val()"
        return temResult
    def AppenFDIB(self):
        temResult = ""
        for key in list(self.temAttCN.keys()):
            if isinstance(getattr(self.objN,"att"+key),str):
                pass
            elif isinstance(getattr(self.objN,"att"+key),int):
                pass
            elif isinstance(getattr(self.objN,"att"+key),float):
                pass
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\r    if($(\"#"+self.attRBC+"_"+key+"\").val() == \"\")"
                temResult = temResult + "\r    {"
                temResult = temResult + "\r        $(\"#"+self.attRBC+"_"+key+"\").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));"               
                temResult = temResult + "\r    }"
                temResult = temResult + "\r    laydate.render({"
                temResult = temResult + "\r        elem: '#"+self.attRBC+"_"+key+"',"
                temResult = temResult + "\r        type: 'datetime',"
                temResult = temResult + "\r        trigger: 'click'"
                temResult = temResult + "\r    });" 
        return temResult
    def WriteBizJs_1(self,templatePath,tempTargetPath,temTABLE_NAME):
        readFile = None
        writeFile = None            
        try: 
            if os.path.exists(templatePath+Com_Para.zxyPath+"template_table3.js") == False :
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"template_table3.js模板文件不存在"+ "\"}]}"
                return ""
            else:
                readFile = open(templatePath+Com_Para.zxyPath+"template_table3.js", 'r',encoding=Com_Para.U_CODE)
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+".js") == False :
                writeFile = open(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+".js", 'a',encoding=Com_Para.U_CODE)
            else:
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\""+temTABLE_NAME+".js文件已存在"+ "\"}]}"
                return ""
            while True and readFile is not None and writeFile is not None:
                temLines = readFile.readline()
                if self.param_value3 != "":
                    temLines = temLines.replace("{template_table_CN}",self.param_value3).replace("{$RDA$}",self.attRDA).replace("{$RBC$}",self.attRBC).replace("{$FFD$}",self.attFFD)                    
                temLines = temLines.replace("{template_table_EN}",temTABLE_NAME).replace("{$RDA$}",self.attRDA).replace("{$RBC$}",self.attRBC).replace("{$FFD$}",self.attFFD)
                writeFile.write(temLines)
                if temLines.find("*declare select options begin*") != -1:
                    for key in list(self.temSelectAtt.keys()):
                        temValue = Com_Fun.GetHashTable(self.temSelectAtt,key)
                        if isinstance(temValue,str):
                            writeFile.write("\rvar "+self.attRDA+"_ary_"+key+" = null;")
                        elif isinstance(temValue,list):
                            writeFile.write("\rvar "+self.attRDA+"_ary_"+key+" = "+str(temValue)+";")
                elif temLines.find("*refresh select options begin*") != -1:
                    for key in list(self.temSelectAtt.keys()):
                        temValue = Com_Fun.GetHashTable(self.temSelectAtt,key)
                        if isinstance(temValue,str):
                            writeFile.write("\r    "+self.attRBC+"_ary_"+key+" = null;")
                    for key in list(self.temFindAtt.keys()):
                        writeFile.write("\r    $(\"#"+self.attRDA+"_find_"+key+"_cn_name\").val(\"\");")
                        writeFile.write("\r    $(\"#"+self.attRDA+"_find_"+key+"\").val(\"-1\");")
                elif temLines.find("*biz begin*") != -1:
                    if len(self.temSelectAtt) == 0:                  
                        writeFile.write("\r    "+self.attRDA+"_init_"+temTABLE_NAME+"()")
                    else:
                        bAjax = False
                        for key in list(self.temSelectAtt.keys()):
                            if isinstance(self.temSelectAtt[key],str) and self.temSelectAtt[key].find("select ") == 0:
                                writeFile.write("\r    var inputdata = {")
                                writeFile.write("\r        \"param_name\": \"N01_"+temTABLE_NAME+"$"+key+"\",")
                                writeFile.write("\r        \"session_id\": session_id,")
                                writeFile.write("\r        \"login_id\": login_id")
                                writeFile.write("\r    };")
                                writeFile.write("\r    ly_index = layer.load();")
                                writeFile.write("\r    get_ajax_baseurl(inputdata, \""+self.attRDA+"_get_N01_"+temTABLE_NAME+"$"+key+"\");")
                                bAjax = True
                                break
                            elif isinstance(self.temSelectAtt[key],str) and self.temSelectAtt[key].find("select ") == -1:
                                writeFile.write("\r    var inputdata = {")
                                writeFile.write("\r        \"param_name\": \""+self.temSelectAtt[key]+"\",")
                                writeFile.write("\r        \"session_id\": session_id,")
                                writeFile.write("\r        \"login_id\": login_id")
                                writeFile.write("\r    };")
                                writeFile.write("\r    ly_index = layer.load();")
                                writeFile.write("\r    get_ajax_baseurl(inputdata, \""+self.attRDA+"_get_"+self.temSelectAtt[key]+"\");")
                                bAjax = True
                                break
                        if bAjax == False:
                            for key in list(self.temSelectAtt.keys()): 
                                if isinstance(self.temSelectAtt[key],list): 
                                    writeFile.write("\r    if($(\"#"+self.attRDA+"_qry_"+key+"\").is(\"select\") && $(\"#"+self.attRDA+"_qry_"+key+"\")[0].options.length == 0)")
                                    writeFile.write("\r    {")
                                    writeFile.write("\r        $(\"#"+self.attRDA+"_qry_"+key+"\").append(\"<option value='-1'></option>\")");
                                    writeFile.write("\r        $.each("+self.attRDA+"_ary_"+key+", function (i, obj) {")
                                    writeFile.write("\r            addOptionValue(\""+self.attRDA+"_qry_"+key+"\", obj[GetLowUpp(\"main_id\")], obj[GetLowUpp(\"cn_name\")]);")
                                    writeFile.write("\r        });")
                                    writeFile.write("\r    }")
                            writeFile.write("\r    "+self.attRDA+"_init_"+temTABLE_NAME+"()")
                elif temLines.find("*biz step begin*") != -1:
                    for key in list(self.temSelectAtt.keys()):                   
                        writeFile.write("\rfunction "+self.attRDA+"_format_"+key+"(value, row, index) {")
                        writeFile.write("\r    var objResult = value;")
                        writeFile.write("\r    for(i = 0; i < "+self.attRDA+"_ary_"+key+".length; i++) {")
                        writeFile.write("\r        var obj = "+self.attRDA+"_ary_"+key+"[i];")
                        writeFile.write("\r        if (obj[GetLowUpp(\"main_id\")].toString() == value.toString()) {")
                        writeFile.write("\r            objResult = obj[GetLowUpp(\"cn_name\")];")
                        writeFile.write("\r            break;")
                        writeFile.write("\r        }")
                        writeFile.write("\r    }")
                        writeFile.write("\r    return objResult;")
                        writeFile.write("\r}")
                        writeFile.write("\r")
                    bFlag = False
                    iIndex = 0                         
                    for key in list(self.temSelectAtt.keys()):   
                        if isinstance(self.temSelectAtt[key],str)  and self.temSelectAtt[key].find("select ") == 0:
                            if bFlag == True:
                                writeFile.write("\r    var inputdata = {")
                                writeFile.write("\r        \"param_name\": \"N01_"+temTABLE_NAME+"$"+key+"\",")
                                writeFile.write("\r        \"session_id\": session_id,")
                                writeFile.write("\r        \"login_id\": login_id")
                                writeFile.write("\r    };")
                                writeFile.write("\r    ly_index = layer.load();")
                                writeFile.write("\r    get_ajax_baseurl(inputdata, \""+self.attRDA+"_get_N01_"+temTABLE_NAME+"$"+key+"\");")
                                writeFile.write("\r}")            
                            writeFile.write("\r")                                            
                            writeFile.write("\rfunction "+self.attRDA+"_get_N01_"+temTABLE_NAME+"$"+key+"(input) {")
                            writeFile.write("\r    layer.close(ly_index);")
                            writeFile.write("\r    //查询失败")
                            writeFile.write("\r    if (Call_QryResult(input.N01_"+temTABLE_NAME+"$"+key+") == false)")
                            writeFile.write("\r        return false;")
                            writeFile.write("\r    "+self.attRDA+"_ary_"+key+" = input.N01_"+temTABLE_NAME+"$"+key+";")  
                            iIndex = iIndex + 1
                            bFlag = True
                        elif isinstance(self.temSelectAtt[key],str) and self.temSelectAtt[key].find("select ") == -1:
                            if bFlag == True:
                                writeFile.write("\r    var inputdata = {")
                                writeFile.write("\r        \"param_name\": \""+self.temSelectAtt[key]+"\",")
                                writeFile.write("\r        \"session_id\": session_id,")
                                writeFile.write("\r        \"login_id\": login_id")
                                writeFile.write("\r    };")
                                writeFile.write("\r    ly_index = layer.load();")
                                writeFile.write("\r    get_ajax_baseurl(inputdata, \""+self.attRDA+"_get_"+self.temSelectAtt[key]+"\");")
                                writeFile.write("\r}")            
                            writeFile.write("\r")                                            
                            writeFile.write("\rfunction "+self.attRDA+"_get_"+self.temSelectAtt[key]+"(input) {")
                            writeFile.write("\r    layer.close(ly_index);")
                            writeFile.write("\r    //查询失败")
                            writeFile.write("\r    if (Call_QryResult(input."+self.temSelectAtt[key]+") == false)")
                            writeFile.write("\r        return false;")
                            writeFile.write("\r    "+self.attRDA+"_ary_"+key+" = input."+self.temSelectAtt[key]+";")  
                            iIndex = iIndex + 1
                            bFlag = True
                    if bFlag:
                        for key2 in list(self.temSelectAtt.keys()):
                            for key in list(self.temQryAtt.keys()):                        
                                if key.upper() == key2.upper():
                                    writeFile.write("\r    $(\"#"+self.attRDA+"_qry_"+key+"\").append(\"<option value='-1'></option>\")");
                                    writeFile.write("\r    $.each("+self.attRDA+"_ary_"+key+", function (i, obj) {")
                                    writeFile.write("\r        addOptionValue(\""+self.attRDA+"_qry_"+key+"\", obj[GetLowUpp(\"main_id\")], obj[GetLowUpp(\"cn_name\")]);")
                                    writeFile.write("\r    });")
                                    writeFile.write("\r    if("+self.attRDA+"_param.hasOwnProperty(\""+key+"\"))")    
                                    writeFile.write("\r        $(\"#"+self.attRDA+"_qry_"+key+"\").val("+self.attRDA+"_param[\""+key+"\"]);")               
                        writeFile.write("\r    "+self.attRDA+"_init_"+temTABLE_NAME+"();")
                        writeFile.write("\r}")                        
                elif temLines.find("*declare query param begin*") != -1:
                    writeFile.write(self.AppenDQPB1())
                elif temLines.find("*find qry fun begin*") != -1:
                    writeFile.write(self.AppenFQFB())
                elif temLines.find("*table column begin*") != -1:
                    writeFile.write(self.AppendColumn())
                elif temLines.find("*query conditions init begin*") != -1:
                    writeFile.write(self.AppenQCIB())
                elif temLines.find("*get query param begin*") != -1:
                    writeFile.write(self.AppenGQPB())
                elif temLines.find("*set query param begin*") != -1:
                    writeFile.write(self.AppenSQPB())
                elif temLines.find("*add param value begin*") != -1:
                    writeFile.write(self.AppenAPVB())
                    pass
                elif temLines.find("*get find subvue bgein*") != -1:
                    writeFile.write(self.AppenGFSB())
                    pass
                elif temLines.find("*refresh query param begin*") != -1:
                    writeFile.write(self.AppenRQPB())
                    pass
                elif temLines.find("*Send One FindSelect param bgein*") != -1:
                    writeFile.write(self.AppenSOFPB())
                    pass
                elif temLines.find("*Send Two FindSelect param bgein*") != -1:
                    writeFile.write(self.AppenSTFPB())
                    pass
                elif temLines.find("*get tab subvue begin*") != -1:
                    writeFile.write(self.AppenGTSB())
                    pass
                elif temLines.find("*Tab Click Fun Begin*") != -1:
                    writeFile.write(self.AppenTCFB())
                    pass
                elif temLines.find("*Main Subsuv Table Param Begin*") != -1:
                    writeFile.write(self.AppenMSTPB())
                    pass
                if not temLines:
                    break
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if readFile != None:
                readFile.close()
            if writeFile != None:
                writeFile.close()
    def AppenSQPB(self):
        temResult = ""
        for key in list(self.temQryAtt.keys()):
            if isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\r    "+self.attRDA+"_tem_"+key+" = $(\"#"+self.attRDA+"_qry_"+key+"\").val();" 
                pass
            elif isinstance(getattr(self.objN,"att"+key),int):
                temResult = temResult + "\r    "+self.attRDA+"_tem_"+key+" = $(\"#"+self.attRDA+"_qry_"+key+"\").val();"
                pass
            elif isinstance(getattr(self.objN,"att"+key),float):
                temResult = temResult + "\r    "+self.attRDA+"_tem_"+key+" = $(\"#"+self.attRDA+"_qry_"+key+"\").val();"
                pass
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\r    "+self.attRDA+"_tem_begin_"+key+" = $(\"#"+self.attRDA+"_qry_begin_"+key+"\").val();"
                temResult = temResult + "\r    "+self.attRDA+"_tem_end_"+key+" = $(\"#"+self.attRDA+"_qry_end_"+key+"\").val();"
        for key in list(self.temFindAtt.keys()):
            if isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\r    "+self.attRDA+"_tem_"+key+" = $(\"#"+self.attRDA+"_find_"+key+"\").val();" 
            elif isinstance(getattr(self.objN,"att"+key),int):
                temResult = temResult + "\r    "+self.attRDA+"_tem_"+key+" = $(\"#"+self.attRDA+"_find_"+key+"\").val();"
            elif isinstance(getattr(self.objN,"att"+key),float):
                temResult = temResult + "\r    "+self.attRDA+"_tem_"+key+" = $(\"#"+self.attRDA+"_find_"+key+"\").val();"
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\r    "+self.attRDA+"_tem_begin_"+key+" = $(\"#"+self.attRDA+"_find_begin_"+key+"\").val();"
                temResult = temResult + "\r    "+self.attRDA+"_tem_end_"+key+" = $(\"#"+self.attRDA+"_find_end_"+key+"\").val();"
        return temResult
    def AppenTCFB(self):
        temResult = ""
        temIndex = 1
        for vms in self.temMainSub:
            temResult = temResult + "\r$(\"#"+self.attRDA+"_tab_"+str(temIndex)+"\").click(function(){"
            temResult = temResult + "\r    "+self.attRDA+"_Tab_Flag = "+str(temIndex)+";"
            temResult = temResult + "\r    "+self.attRDA+"_hide_tab_fun();"
            temResult = temResult + "\r    //主子表参数传递"
            temResult = temResult + "\r    var temPar = [{\"sourc_id\":\""+vms.attMAIN_COLUMN_EN_NAME+"\",\"target_id\":\""+vms.attSUB_COLUMN_EN_NAME+"\"},{\"sourc_id\":\""+vms.attMAIN_TITLE_COLUMN+"\",\"target_id\":\""+vms.attSUB_COLUMN_EN_NAME+"_cn_name\"}];"
            temResult = temResult + "\r    "+self.attRDA+"_show_tab_fun(\""+vms.attSUB_TABLE_EN_NAME+".vue\",\""+vms.attSUB_RANDOM+"\",temPar);"
            temResult = temResult + "\r});"
            temResult = temResult + "\r"
            temIndex = temIndex + 1        
        temResult = temResult + "\r//隐藏tab页选项卡"
        temResult = temResult + "\rfunction "+self.attRDA+"_hide_tab_fun(){"
        temResult = temResult + "\r    var n = null;"        
        for vms in self.temMainSub:
            temResult = temResult + "\r    n = Get_RDivNoBuild(\""+vms.attSUB_RANDOM+"\",\"\");"
            temResult = temResult + "\r    $(n).hide();"            
        temResult = temResult + "\r}" 
        temResult = temResult + "\r"
        temResult = temResult + "\r//判断是否sub子项div页面"
        temResult = temResult + "\rfunction "+self.attRDA+"_Is_Sub_Div(temDivId){"
        temResult = temResult + "\r    if(temDivId.indexOf(\"XX$TTT\") == 0)"
        temResult = temResult + "\r        return false;"
        for vms in self.temMainSub:
            temResult = temResult + "\r    else if(temDivId.indexOf(\""+vms.attSUB_RANDOM+"\") == 0)"
            temResult = temResult + "\r        return true;"
        temResult = temResult + "\r    return false;"
        temResult = temResult + "\r}"   
        temResult = temResult + "\r"
        return temResult
    def AppenGTSB(self):
        temResult = ""
        for vms in self.temMainSub:      
            temResult = temResult + "\r    else if(index_subhtml == \""+vms.attSUB_TABLE_EN_NAME+".vue\"){"
            temResult = temResult + "\r        var n = Get_RandomDiv(\""+vms.attSUB_RANDOM+"\",objResult);"
            temResult = temResult + "\r        $(n).show();"
            temResult = temResult + "\r        //传递参数"
            temResult = temResult + "\r        var inputdata = {\"hidden_find\":\"1\"};"
            temResult = temResult + "\r        if("+self.attRDA+"_rowCheckData != null){"
            temResult = temResult + "\r            inputdata[\""+vms.attSUB_COLUMN_EN_NAME+"_cn_name\"] = "+self.attRDA+"_rowCheckData[GetLowUpp(\""+vms.attMAIN_TITLE_COLUMN+"\")];"
            temResult = temResult + "\r            inputdata[\""+vms.attSUB_COLUMN_EN_NAME+"\"] = "+self.attRDA+"_rowCheckData[GetLowUpp(\""+vms.attMAIN_COLUMN_EN_NAME+"\")];"
            temResult = temResult + "\r        }"
            temResult = temResult + "\r        else{"
            temResult = temResult + "\r            inputdata[\""+vms.attSUB_COLUMN_EN_NAME+"_cn_name\"] = \"\";"
            temResult = temResult + "\r            inputdata[\""+vms.attSUB_COLUMN_EN_NAME+"\"] = \"-999\";"
            temResult = temResult + "\r        }"
            temResult = temResult + "\r        loadScript_hasparam(\""+self.attFFD+"/"+vms.attSUB_TABLE_EN_NAME+".js\",\""+vms.attSUB_RANDOM+"_"+vms.attSUB_TABLE_EN_NAME+"_biz_start\",inputdata);"
            temResult = temResult + "\r    }"
        return temResult
    def AppenSTFPB(self):
        temResult = ""
        for key in list(self.temQryAtt.keys()):
            temResult = temResult + "\r                var tem"+key+" = $(\"#"+self.attRDA+"_qry_"+key+"\").val();"
            temResult = temResult + "\r                if(tem"+key+" != \"\"){"
            temResult = temResult + "\r                    "+self.attRBC+"_param[\""+key+"\"] = tem"+key+";"
            temResult = temResult + "\r                }"
        for key in list(self.temFindAtt.keys()):
            temResult = temResult + "\r                var tem"+key+" = $(\"#"+self.attRDA+"_find_"+key+"_cn_name\").val();"
            temResult = temResult + "\r                if(tem"+key+" != \"\"){"
            temResult = temResult + "\r                    "+self.attRBC+"_param[\""+key+"_cn_name\"] = tem"+key+";"
            temResult = temResult + "\r                    "+self.attRBC+"_param[\""+key+"\"] = $(\"#"+self.attRDA+"_"+key+"\").val();"
            temResult = temResult + "\r                }"    
        return temResult
    def AppenMSTPB2(self):
        temResult = ""
        for key in list(self.temFindAtt.keys()):
            temResult = temResult + "\r    if("+self.attRBC+"_param.hasOwnProperty(\""+key+"_cn_name\"))"
            temResult = temResult + "\r        $(\"#"+self.attRBC+"_find_"+key+"_cn_name\").val(s_decode("+self.attRBC+"_param[\""+key+"_cn_name\"]));"
            temResult = temResult + "\r    if("+self.attRBC+"_param.hasOwnProperty(\""+key+"\"))"
            temResult = temResult + "\r        $(\"#"+self.attRBC+"_find_"+key+"\").val("+self.attRBC+"_param[\""+key+"\"]);"
            temResult = temResult + "\r    if("+self.attRBC+"_param.hasOwnProperty(\"hidden_find\")){"
            temResult = temResult + "\r        $(\"#"+self.attRBC+"_Ope_"+key+"\").hide();"
            temResult = temResult + "\r        $(\"#"+self.attRBC+"_Clear_"+key+"\").hide();"
            temResult = temResult + "\r    }"
        return temResult
    def AppenMSTPB(self):
        temResult = ""
        for key in list(self.temFindAtt.keys()):
            temResult = temResult + "\r    if("+self.attRDA+"_param.hasOwnProperty(\""+key+"_cn_name\"))"
            temResult = temResult + "\r        $(\"#"+self.attRDA+"_find_"+key+"_cn_name\").val(s_decode("+self.attRDA+"_param[\""+key+"_cn_name\"]));"
            temResult = temResult + "\r    if("+self.attRDA+"_param.hasOwnProperty(\""+key+"\"))"
            temResult = temResult + "\r        $(\"#"+self.attRDA+"_find_"+key+"\").val("+self.attRDA+"_param[\""+key+"\"]);"
            temResult = temResult + "\r    if("+self.attRDA+"_param.hasOwnProperty(\"hidden_find\")){"
            temResult = temResult + "\r        $(\"#"+self.attRDA+"_Ope_"+key+"\").hide();"
            temResult = temResult + "\r        $(\"#"+self.attRDA+"_Clear_"+key+"\").hide();"
            temResult = temResult + "\r    }"
        for key in list(self.temQryAtt.keys()):
            temResult = temResult + "\r    if("+self.attRDA+"_param.hasOwnProperty(\""+key+"\"))"
            temResult = temResult + "\r        $(\"#"+self.attRDA+"_qry_"+key+"\").val("+self.attRDA+"_param[\""+key+"\"]);"
            temResult = temResult + "\r    if("+self.attRDA+"_param.hasOwnProperty(\"hidden_find\")){"
            temResult = temResult + "\r        $(\"#"+self.attRDA+"_qry_"+key+"\").attr(\"disabled\", true);"
            temResult = temResult + "\r    }" 
        return temResult
    def AppenSOFPB(self):
        temResult = ""
        for key in list(self.temQryAtt.keys()):
            temResult = temResult + "\r                var tem"+key+" = $(\"#"+self.attRDA+"_qry_"+key+"\").val();"
            temResult = temResult + "\r                if(tem"+key+" != \"\"){"
            temResult = temResult + "\r                    inputdata[\""+key+"\"] = tem"+key+";"
            temResult = temResult + "\r                }"
        for key in list(self.temFindAtt.keys()):
            temResult = temResult + "\r                var tem"+key+" = $(\"#"+self.attRDA+"_find_"+key+"_cn_name\").val();"
            temResult = temResult + "\r                if(tem"+key+" != \"\"){"
            temResult = temResult + "\r                    inputdata[\""+key+"_cn_name\"] = tem"+key+";"
            temResult = temResult + "\r                    inputdata[\""+key+"\"] = $(\"#"+self.attRDA+"_"+key+"\").val();"
            temResult = temResult + "\r                }"    
        return temResult
    def AppenRQPB(self):
        temResult = ""
        for key in list(self.temQryAtt.keys()):
            if isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\r    "+self.attRDA+"_tem_"+key+" = \"\";"
            elif isinstance(getattr(self.objN,"att"+key),int):
                temResult = temResult + "\r    "+self.attRDA+"_tem_"+key+" = \"0\";"
            elif isinstance(getattr(self.objN,"att"+key),float):
                temResult = temResult + "\r    "+self.attRDA+"_tem_"+key+" = \"0\";"
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\r    "+self.attRDA+"_tem_begin_"+key+" = new Date().Format(\"yyyy-MM-dd hh:mm:ss\");"
                temResult = temResult + "\r    "+self.attRDA+"_tem_end_"+key+" = new Date().Format(\"yyyy-MM-dd hh:mm:ss\");"
        for key in list(self.temFindAtt.keys()):
            if isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\r    "+self.attRDA+"_tem_"+key+" = \"\";"
            elif isinstance(getattr(self.objN,"att"+key),int):
                temResult = temResult + "\r    "+self.attRDA+"_tem_"+key+" = \"-1\";"
            elif isinstance(getattr(self.objN,"att"+key),float):
                temResult = temResult + "\r    "+self.attRDA+"_tem_"+key+" = \"0\";"
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\r    "+self.attRDA+"_tem_begin_"+key+" = new Date().Format(\"yyyy-MM-dd hh:mm:ss\");"
                temResult = temResult + "\r    "+self.attRDA+"_tem_end_"+key+" = new Date().Format(\"yyyy-MM-dd hh:mm:ss\");"
        return temResult
    def AppenGQPB(self):
        temResult = ""
        temIndex = 0
        for key in list(self.temQryAtt.keys()):
            temIndex = temIndex + 1
            if isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": s_encode("+self.attRDA+"_tem_"+key+")"
                pass
            elif isinstance(getattr(self.objN,"att"+key),int):
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": "+self.attRDA+"_tem_"+key
                temIndex = temIndex + 1
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": "+self.attRDA+"_tem_"+key
                pass
            elif isinstance(getattr(self.objN,"att"+key),float):
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": "+self.attRDA+"_tem_"+key
                pass
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": "+self.attRDA+"_tem_begin_"+key
                temIndex = temIndex + 1
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": "+self.attRDA+"_tem_end_"+key
        for key in list(self.temFindAtt.keys()):
            temIndex = temIndex + 1
            if isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": s_encode("+self.attRDA+"_tem_"+key+")"
                temIndex = temIndex + 1
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": s_encode("+self.attRDA+"_tem_"+key+")"
                pass
            elif isinstance(getattr(self.objN,"att"+key),int):
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": "+self.attRDA+"_tem_"+key
                temIndex = temIndex + 1
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": "+self.attRDA+"_tem_"+key
                pass
            elif isinstance(getattr(self.objN,"att"+key),float):
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": "+self.attRDA+"_tem_"+key
                pass
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": "+self.attRDA+"_tem_begin_"+key
                temIndex = temIndex + 1
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": "+self.attRDA+"_tem_end_"+key
        return temResult
    def AppenQCIB(self):
        temResult = ""
        for key in list(self.temQryAtt.keys()):
            if isinstance(getattr(self.objN,"att"+key),str):
                pass
            elif isinstance(getattr(self.objN,"att"+key),int):
                pass
            elif isinstance(getattr(self.objN,"att"+key),float):
                pass
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\r   $(\"#"+self.attRDA+"_qry_begin_"+key+"\").val(DateAdd(\"d\", -5, new Date()).Format('yyyy-MM-dd hh:mm:ss'));"
                temResult = temResult + "\r   laydate.render({"
                temResult = temResult + "\r       elem: '#"+self.attRDA+"_qry_begin_"+key+"',"
                temResult = temResult + "\r       type: 'datetime',"
                temResult = temResult + "\r       trigger: 'click'"
                temResult = temResult + "\r   });"
                temResult = temResult + "\r   $(\"#"+self.attRDA+"_qry_end_"+key+"\").val(DateAdd(\"d\", 1, new Date()).Format('yyyy-MM-dd hh:mm:ss'));"
                temResult = temResult + "\r   laydate.render({"
                temResult = temResult + "\r       elem: '#"+self.attRDA+"_qry_end_"+key+"',"
                temResult = temResult + "\r       type: 'datetime',"
                temResult = temResult + "\r       trigger: 'click'"
                temResult = temResult + "\r   });"
        return temResult
    def AppenIVCB(self):
        temResult = ""
        for key in list(self.temFindAtt.keys()):
            temResult = temResult+"\r    "+self.attRBC+"_clear_input_cn_name('"+self.attRBC+"_find_"+key+"_cn_name','"+self.attRBC+"_"+key+"')"
        return temResult
    def AppenFQFB2(self):
        temResult = ""
        for key in list(self.temFindAtt.keys()):
            temValue = Com_Fun.GetHashTable(self.temFindAtt,key)
            temRandom = self.GetRandom(temValue)
            temResult = temResult+"\rfunction "+self.attRBC+"_"+key+"_cn_name_fun(){"
            temResult = temResult+"\r    index_subhtml = \""+temValue+"\"";
            temResult = temResult+"\r    random_subhtml = \""+temRandom+"\";"
            temResult = temResult+"\r    if(loadHtmlSubVueFun(\""+self.attFFD+"/"+temValue+"\",\""+self.attRBC+"_"+self.attTableName+"_call_vue\") == true){"
            temResult = temResult+"\r        var n = Get_RandomDiv(\""+temRandom+"\",\"\");"
            temResult = temResult+"\r        layer.open({"
            temResult = temResult+"\r            type: 1,"
            temResult = temResult+"\r            area: ['1100px', '600px'],"
            temResult = temResult+"\r            fixed: false, //不固定"
            temResult = temResult+"\r            maxmin: true,"            
            temRe = Com_Fun.GetHashTable(self.temFReturnAtt,key)            
            temResult = temResult+"\r            content: $(n),"
            temResult = temResult+"\r            success: function(layero, index){"  
            temResult = temResult+"\r                $('#"+temRandom+"_"+temValue.replace(".vue","")+"_Events').bootstrapTable('resetView');"        
            temResult = temResult+"\r                "+temRandom+"_param[\"ly_index\"] = index;"
            temResult = temResult+"\r                "+temRandom+"_param[\"target_name\"] = \""+self.attRBC+"_find_"+key+"_cn_name\"";
            temResult = temResult+"\r                "+temRandom+"_param[\"target_id\"] = \""+self.attRBC+"_"+key+"\"";
            temResult = temResult+"\r                "+temRandom+"_param[\"sourc_id\"] = \""+temRe[0]+"\"";
            temResult = temResult+"\r                "+temRandom+"_param[\"sourc_name\"] = \""+temRe[1]+"\"";
            temResult = temResult+"\r            },"
            temResult = temResult+"\r            end: function(){"
            temResult = temResult+"\r                $(n).hide();"
            temResult = temResult+"\r            }"
            temResult = temResult+"\r        });"
            temResult = temResult+"\r     }"
            temResult = temResult+"\r}"
        return temResult
    def AppenGFSPB(self):
        temResult = ""
        for key in list(self.temQryAtt.keys()):
            temResult = temResult + "\r        $(\"#"+self.attRBC+"_"+key+"\").val("+self.attRBC+"_param[\""+key+"\"]);"
        for key in list(self.temFindAtt.keys()):
            temResult = temResult + "\r        $(\"#"+self.attRBC+"_"+key+"\").val("+self.attRBC+"_param[\""+key+"\"]);"  
            temResult = temResult + "\r        $(\"#"+self.attRBC+"_find_"+key+"_cn_name\").val("+self.attRBC+"_param[\""+key+"_cn_name\"]);"      
        return temResult
    def AppenGFSB2(self):
        temResult = ""
        for key in list(self.temFindAtt.keys()):
            temValue = Com_Fun.GetHashTable(self.temFindAtt,key)            
            temRe = Com_Fun.GetHashTable(self.temFReturnAtt,key)
            temRandom = self.GetRandom(temValue)
            temResult = temResult+"\r    else if(index_subhtml == \""+temValue+"\"){"
            temResult = temResult+"\r        var n = Get_RandomDiv(\""+temRandom+"\",objResult);"
            temResult = temResult+"\r        layer.open({"
            temResult = temResult+"\r            type: 1,"
            temResult = temResult+"\r            area: ['1100px', '600px'],"
            temResult = temResult+"\r            fixed: false, //不固定"
            temResult = temResult+"\r            maxmin: true,"
            temResult = temResult+"\r            content: $(n),"
            temResult = temResult+"\r            success: function(layero, index){"
            temResult = temResult+"\r                var inputdata = {"
            temResult = temResult+"\r                    \"type\":"+self.attRBC+"_type,"
            temResult = temResult+"\r                    \"ly_index\":index,"
            temResult = temResult+"\r                    \"target_name\":\""+self.attRBC+"_find_"+key+"_cn_name\","
            temResult = temResult+"\r                    \"target_id\":\""+self.attRBC+"_find_"+key+"\","
            temResult = temResult+"\r                    \"sourc_id\":\""+temRe[0]+"\","
            temResult = temResult+"\r                    \"sourc_name\":\""+temRe[1]+"\""
            temResult = temResult+"\r                };"
            temResult = temResult+"\r                loadScript_hasparam(\""+self.attFFD+"/"+temValue.replace(".vue", "")+".js\",\""+temRandom+"_"+temValue.replace(".vue","")+"_biz_start\",inputdata);"
            temResult = temResult+"\r            },"
            temResult = temResult+"\r            end: function(){"
            temResult = temResult+"\r                $(n).hide();"
            temResult = temResult+"\r            }"
            temResult = temResult+"\r        });"
            temResult = temResult+"\r    }"
        return temResult
    def AppenGFSB(self):
        temResult = ""
        for key in list(self.temFindAtt.keys()):
            temValue = Com_Fun.GetHashTable(self.temFindAtt,key)            
            temRe = Com_Fun.GetHashTable(self.temFReturnAtt,key)
            temRandom = self.GetRandom(temValue)
            temResult = temResult+"\r    else if(index_subhtml == \""+temValue+"\"){"            
            temResult = temResult+"\r        var n = Get_RandomDiv(\""+temRandom+"\",objResult);"
            temResult = temResult+"\r        layer.open({"
            temResult = temResult+"\r            type: 1,"
            temResult = temResult+"\r            area: ['1100px', '600px'],"
            temResult = temResult+"\r            fixed: false, //不固定"
            temResult = temResult+"\r            maxmin: true,"
            temResult = temResult+"\r            content: $(n),"
            temResult = temResult+"\r            success: function(layero, index){"
            temResult = temResult+"\r                var inputdata = {"
            temResult = temResult+"\r                    \"type\":"+self.attRDA+"_type,"
            temResult = temResult+"\r                    \"ly_index\":index,"
            temResult = temResult+"\r                    \"target_name\":\""+self.attRDA+"_find_"+key+"_cn_name\","
            temResult = temResult+"\r                    \"target_id\":\""+self.attRDA+"_find_"+key+"\","
            temResult = temResult+"\r                    \"sourc_id\":\""+temRe[0]+"\","
            temResult = temResult+"\r                    \"sourc_name\":\""+temRe[1]+"\""
            temResult = temResult+"\r                };"
            temResult = temResult+"\r                loadScript_hasparam(\""+self.attFFD+"/"+temValue.replace(".vue", "")+".js\",\""+temRandom+"_"+temValue.replace(".vue","")+"_biz_start\",inputdata);"
            temResult = temResult+"\r            },"
            temResult = temResult+"\r            end: function(){"
            temResult = temResult+"\r                $(n).hide();"
            temResult = temResult+"\r            }"
            temResult = temResult+"\r        });"
            temResult = temResult+"\r    }"
        return temResult
    def AppenAPVB(self):
        temResult = ""
        for key in list(self.temFindAtt.keys()):
            temResult = temResult+"\r            $.each(inputs, function (i, obj) {"
            temResult = temResult+"\r                if (obj.id.toUpperCase() == \""+self.attRDA+"_find_"+key+"_cn_name\".toUpperCase()) {"
            temResult = temResult+"\r                    $(obj).val($(\"#"+self.attRDA+"_find_"+key+"_cn_name\").val());"
            temResult = temResult+"\r                }"
            temResult = temResult+"\r                else if ((\""+self.attRDA+"_find_\"+obj.id).toUpperCase() == \""+self.attRDA+"_find_"+key+"\".toUpperCase()){"
            temResult = temResult+"\r                    $(obj).val($(\"#"+self.attRDA+"_find_\"+obj.id).val());"
            temResult = temResult+"\r                }"
            temResult = temResult+"\r            });"
            temResult = temResult+"\r"
        return temResult
    def AppenFQFB(self):
        temResult = ""
        for key in list(self.temFindAtt.keys()):
            temValue = Com_Fun.GetHashTable(self.temFindAtt,key)
            temRandom = self.GetRandom(temValue)
            temRe = Com_Fun.GetHashTable(self.temFReturnAtt,key)
            temResult = temResult+"\rfunction "+self.attRDA+"_"+key+"_cn_name_fun(){"
            temResult = temResult+"\r    index_subhtml = \""+temValue+"\"";
            temResult = temResult+"\r    random_subhtml = \""+temRandom+"\";"
            temResult = temResult+"\r    if(loadHtmlSubVueFun(\""+self.attFFD+"/"+temValue+"\",\""+self.attRDA+"_"+self.attTableName+"_call_vue\") == true){"
            temResult = temResult+"\r        var n = Get_RandomDiv(\""+temRandom+"\",\"\");"
            temResult = temResult+"\r        layer.open({"
            temResult = temResult+"\r            type: 1,"
            temResult = temResult+"\r            area: ['1100px', '600px'],"
            temResult = temResult+"\r            fixed: false, //不固定"
            temResult = temResult+"\r            maxmin: true,"            
            temRe = Com_Fun.GetHashTable(self.temFReturnAtt,key)            
            temResult = temResult+"\r            content: $(n),"
            temResult = temResult+"\r            success: function(layero, index){"
            temResult = temResult+"\r                $('#"+temRandom+"_"+temValue.replace(".vue","")+"_Events').bootstrapTable('resetView');"
            temResult = temResult+"\r                "+temRandom+"_param[\"ly_index\"] = index;"
            temResult = temResult+"\r                "+temRandom+"_param[\"target_name\"] = \""+self.attRDA+"_find_"+key+"_cn_name\"";
            temResult = temResult+"\r                "+temRandom+"_param[\"target_id\"] = \""+self.attRDA+"_find_"+key+"\"";
            temResult = temResult+"\r                "+temRandom+"_param[\"sourc_id\"] = \""+temRe[0]+"\"";
            temResult = temResult+"\r                "+temRandom+"_param[\"sourc_name\"] = \""+temRe[1]+"\"";
            temResult = temResult+"\r            },"
            temResult = temResult+"\r            end: function(){"
            temResult = temResult+"\r                $(n).hide();"
            temResult = temResult+"\r            }"
            temResult = temResult+"\r        });"
            temResult = temResult+"\r     }"
            temResult = temResult+"\r}"
        return temResult
    def AppenDQPB1(self):
        temResult = ""
        for key in list(self.temQryAtt.keys()):
            if isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\rvar "+self.attRDA+"_tem_"+key+" = \"\";"
            elif isinstance(getattr(self.objN,"att"+key),int):
                temResult = temResult + "\rvar "+self.attRDA+"_tem_"+key+" = \"0\";"
            elif isinstance(getattr(self.objN,"att"+key),float):
                temResult = temResult + "\rvar "+self.attRDA+"_tem_"+key+" = \"0\";"
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\rvar "+self.attRDA+"_tem_begin_"+key+" = new Date().Format(\"yyyy-MM-dd hh:mm:ss\");"
                temResult = temResult + "\rvar "+self.attRDA+"_tem_end_"+key+" = new Date().Format(\"yyyy-MM-dd hh:mm:ss\");"
        for key in list(self.temFindAtt.keys()):
            if isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\rvar "+self.attRDA+"_tem_"+key+" = \"\";"
            elif isinstance(getattr(self.objN,"att"+key),int):
                temResult = temResult + "\rvar "+self.attRDA+"_tem_"+key+" = \"-1\";"
            elif isinstance(getattr(self.objN,"att"+key),float):
                temResult = temResult + "\rvar "+self.attRDA+"_tem_"+key+" = \"0\";"
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\rvar "+self.attRDA+"_tem_begin_"+key+" = new Date().Format(\"yyyy-MM-dd hh:mm:ss\");"
                temResult = temResult + "\rvar "+self.attRDA+"_tem_end_"+key+" = new Date().Format(\"yyyy-MM-dd hh:mm:ss\");"
        return temResult
    def AppendColumn(self):
        temResult = ""
        try:                
            if len(self.temAtts) > 0:
                temIndex = 0
                for temColumn in self.temAtts:
                    if temIndex > 0:
                        temResult = temResult + ","
                    temIndex = temIndex + 1
                    temFormat = ""
                    for key in list(self.temSelectAtt.keys()):                        
                        if key.upper() == temColumn.upper():
                            temFormat = "\r        ,formatter: "+self.attRDA+"_format_"+key
                            break                        
                    if temColumn.upper() == "MAIN_ID":                        
                        temResult = temResult + "\r    {"
                        temResult = temResult + "\r        title: '"+self.temAttCN[temColumn]+"',"
                        temResult = temResult + "\r        field: '"+temColumn.upper()+"',"#temColumn.upper()
                        temResult = temResult + "\r        sortable: true,"
                        temResult = temResult + "\r        visible: false"
                        temResult = temResult + "\r    }"
                    elif temColumn.upper() == "IS_AVA":                     
                        temResult = temResult + "\r    {"
                        temResult = temResult + "\r        title: '"+self.temAttCN[temColumn]+"',"
                        temResult = temResult + "\r        field: '"+temColumn.upper()+"',"
                        temResult = temResult + "\r        sortable: true"
                        temResult = temResult + temFormat#"\r        formatter: get_s_state"
                        temResult = temResult + "\r    }"
                    elif getattr(self.objN,"att"+temColumn) is None:                    
                        temResult = temResult + "\r    {"
                        temResult = temResult + "\r        title: '"+self.temAttCN[temColumn]+"',"
                        temResult = temResult + "\r        field: '"+temColumn.upper()+"',"
                        temResult = temResult + "\r        sortable: true"
                        if temFormat == "":
                            temFormat = "\r        ,formatter: set_time_decode"
                        temResult = temResult + temFormat
                        temResult = temResult + "\r    }"
                    elif isinstance(getattr(self.objN,"att"+temColumn),int):                    
                        temResult = temResult + "\r    {"
                        temResult = temResult + "\r        title: '"+self.temAttCN[temColumn]+"',"
                        temResult = temResult + "\r        field: '"+temColumn.upper()+"',"
                        temResult = temResult + "\r        sortable: true"
                        temResult = temResult + temFormat
                        temResult = temResult + "\r    }"
                    elif isinstance(getattr(self.objN,"att"+temColumn),float):                    
                        temResult = temResult + "\r    {"
                        temResult = temResult + "\r        title: '"+self.temAttCN[temColumn]+"',"
                        temResult = temResult + "\r        field: '"+temColumn.upper()+"',"
                        temResult = temResult + "\r        sortable: true"
                        temResult = temResult + temFormat
                        temResult = temResult + "\r    }"
                    elif isinstance(getattr(self.objN,"att"+temColumn),str):                    
                        temResult = temResult + "\r    {"
                        temResult = temResult + "\r        title: '"+self.temAttCN[temColumn]+"',"
                        temResult = temResult + "\r        field: '"+temColumn.upper()+"',"
                        temResult = temResult + "\r        sortable: true"
                        if temFormat == "":
                            temFormat = "\r        ,formatter: set_s_decode"
                        temResult = temResult + temFormat
                        temResult = temResult + "\r    }"
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        return temResult
    def AppendHMSTHtml(self):
        temResult = ""
        if len(self.temMainSub) > 0:            
            temResult = temResult + "\r<div class=\"col-sm-12\" id=\""+self.attRDA+"_TAB_MAIN\">"
            temResult = temResult + "\r    <div class=\"wrapper animated fadeInRight\">"
            temResult = temResult + "\r        <div class=\"row row-lg\">"
            temResult = temResult + "\r            <div class=\"col-sm-12\">" 
            temResult = temResult + "\r                <div class=\"example-wrap\">" 
            temResult = temResult + "\r                    <div class=\"clients-list\">"
            temResult = temResult + "\r                        <ul class=\"nav nav-tabs\">"
            temiIndex = 1
            for vms in self.temMainSub:
                temActive = ""
                if temiIndex == 1:
                    temActive = "active"
                temResult = temResult + "\r                            <li class=\""+temActive+"\" id=\""+self.attRDA+"_tab_"+str(temiIndex)+"\"><a data-toggle=\"tab\" href=\"#"+self.attRDA+"_tab-"+str(temiIndex)+"\"><i class=\"fa fa-bar-chart-o\"></i>"+vms.attSUB_TABLE_CN_NAME+"</a></li>" 
                temiIndex = temiIndex + 1
            temResult = temResult + "\r                        </ul>"
            temResult = temResult + "\r                    </div>"
            temResult = temResult + "\r                </div>" 
            temResult = temResult + "\r            </div>" 
            temResult = temResult + "\r        </div>" 
            temResult = temResult + "\r    </div>" 
            temResult = temResult + "\r</div>"
        return temResult
    def AppendFindCondHtml(self):
        temResult = ""
        for key in list(self.temFindAtt.keys()):
            temResult = temResult + "\r                                            <span type=\"find\">"
            temResult = temResult + "\r                                                <label for=\""+self.attRDA+"_find_"+key+"\" class=\"control-label\">"+self.temAttCN[key]+"</label>"
            temResult = temResult + "\r                                                <input class=\"form-control\" id=\""+self.attRDA+"_find_"+key+"_cn_name\" type=\"text\" name=\""+self.attRDA+"_find_"+key+"_cn_name\" readonly=\"true\">"
            temResult = temResult + "\r                                                <img src=\"../img/search.png\" id=\""+self.attRDA+"_Ope_"+key+"\" onclick=\""+self.attRDA+"_"+key+"_cn_name_fun()\">"
            temResult = temResult + "\r                                                <img src=\"../img/error.png\" id=\""+self.attRDA+"_Clear_"+key+"\" onclick=\""+self.attRDA+"_clear_input_cn_name(\'"+self.attRDA+"_find_"+key+"_cn_name\',\'"+self.attRDA+"_find_"+key+"\')\">"
            temResult = temResult + "\r                                                <input name=\""+self.attRDA+"_find_"+key+"\" id=\""+self.attRDA+"_find_"+key+"\" type=\"text\" value=\"-1\" style=\"display:none\">"
            temResult = temResult + "\r                                            </span>"
            temResult = temResult + "\r"
        return temResult
    def AppendQryCondHtml(self):
        temResult = ""
        for key in list(self.temQryAtt.keys()):
            temValue = Com_Fun.GetHashTable(self.temQryAtt,key)
            if isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\r                                            <label for=\""+self.attRDA+"_qry_"+key+"\" class=\"control-label\">"+temValue+":</label>"
                temInput = "<input id=\""+self.attRDA+"_qry_"+key+"\" name=\""+self.attRDA+"_qry_"+key+"\" class=\"form-control\" placeholder=\"请输入信息\">"
                for key2 in list(self.temSelectAtt.keys()):                        
                    if key.upper() == key2.upper():
                        temInput = "<select class=\"form-control\" id=\""+self.attRDA+"_qry_"+key+"\" name=\""+self.attRDA+"_qry_"+key+"\" placeholder=\"请输入信息\"></select>" 
                        break                
                temResult = temResult + "\r                                                    "+temInput
            elif isinstance(getattr(self.objN,"att"+key),int):
                temResult = temResult + "\r                                            <label for=\""+self.attRDA+"_qry_"+key+"\" class=\"control-label\">"+temValue+":</label>"
                temInput = "<input id=\""+self.attRDA+"_qry_"+key+"\" name=\""+self.attRDA+"_qry_"+key+"\" class=\"form-control\" placeholder=\"请输入整数\">"
                for key2 in list(self.temSelectAtt.keys()):                        
                    if key.upper() == key2.upper():
                        temInput = "<select class=\"form-control\" id=\""+self.attRDA+"_qry_"+key+"\" name=\""+self.attRDA+"_qry_"+key+"\" placeholder=\"请输入整数\"></select>" 
                        break
                temResult = temResult + "\r                                            "+temInput
            elif isinstance(getattr(self.objN,"att"+key),float):
                temResult = temResult + "\r                                            <label for=\"qry_"+key+"\" class=\"control-label\">"+temValue+":</label>"
                temInput = "<input id=\""+self.attRDA+"_qry_"+key+"\" name=\""+self.attRDA+"_qry_"+key+"\" class=\"form-control\" placeholder=\"请输入小数\">"
                for key2 in list(self.temSelectAtt.keys()):                        
                    if key.upper() == key2.upper():
                        temInput = "<select class=\"form-control\" id=\""+self.attRDA+"_qry_"+key+"\" name=\""+self.attRDA+"_qry_"+key+"\" placeholder=\"请输入小数\"></select>" 
                        break
                temResult = temResult + "\r                                            "+temInput
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\r                                            <label for=\""+self.attRDA+"_qry_begin_"+key+"\" class=\"control-label\">"+temValue+"开始时间:</label>"
                temResult = temResult + "\r                                            <input id=\""+self.attRDA+"_qry_begin_"+key+"\" name=\""+self.attRDA+"_qry_begin_"+key+"\" class=\"form-control datetimePicker\" placeholder=\"请选择开始时间\">"
                temResult = temResult + "\r                                            <label for=\""+self.attRDA+"_qry_end_"+key+"\" class=\"control-label\">"+temValue+"结束时间:</label>"
                temResult = temResult + "\r                                            <input id=\""+self.attRDA+"_qry_end_"+key+"\" name=\""+self.attRDA+"_qry_end_"+key+"\" class=\"form-control datetimePicker\" placeholder=\"请选择结束时间\">"
            temResult = temResult + "\r"
        return temResult
    def AppendEditFormHtml(self):
        temResult = ""
        for key in list(self.temAttCN.keys()):
            temValue = Com_Fun.GetHashTable(self.temAttCN,key)
            if key.upper() == "MAIN_ID":
                temResult = temResult + "\r                    <div class=\"col-sm-12 form-group\">"
                temResult = temResult + "\r                        <div class=\"col-sm-12\">"
                temResult = temResult + "\r                            <label for=\""+self.attRBC+"_"+key+"\" class=\"col-sm-3 control-label\">"+temValue+":*</label>" 
                temResult = temResult + "\r                            <div class=\"col-sm-9\">"
                temResult = temResult + "\r                                <input class=\"form-control\" id=\""+self.attRBC+"_"+key+"\" name=\""+self.attRBC+"_"+key+"\" type=\"text\" readonly=\"true\" />"
                temResult = temResult + "\r                            </div>"
                temResult = temResult + "\r                        </div>"
                temResult = temResult + "\r                    </div>"
            elif isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\r                    <div class=\"col-sm-12 form-group\">"
                temResult = temResult + "\r                        <div class=\"col-sm-12\">"
                temResult = temResult + "\r                            <label for=\""+self.attRBC+"_"+key+"\" class=\"col-sm-3 control-label\">"+temValue+":*</label>" 
                temResult = temResult + "\r                            <div class=\"col-sm-9\">"
                if int(getattr(self.objN,"len"+key)) >= 500:
                    temInput = "<textarea class=\"form-control\" id=\""+self.attRBC+"_"+key+"\" name=\""+self.attRBC+"_"+key+" rows=\"5\"></textarea>"
                else:
                    temInput = "<input class=\"form-control\" id=\""+self.attRBC+"_"+key+"\" name=\""+self.attRBC+"_"+key+"\" type=\"text\" placeholder=\"请输入信息\"/>"
                for key2 in list(self.temSelectAtt.keys()):                                            
                    if key.upper() == key2.upper():
                        if Com_Fun.GetHashTable(self.temFindAtt,key2) != "":
                            temInput = "\r                                 <span type=\"find\">"
                            temInput = temInput + "\r                                    <input class=\"form-control form-search\" id=\""+self.attRBC+"_find_"+key+"_cn_name\" name=\""+self.attRBC+"_find_"+key+"_cn_name\" type=\"text\" readonly=\"true\">"
                            temInput = temInput + "\r                                    <img src=\"../img/search.png\" onclick=\""+self.attRBC+"_"+key+"_cn_name_fun()\">"
                            temInput = temInput + "\r                                    <img src=\"../img/error.png\" onclick=\""+self.attRBC+"_clear_input_cn_name(\'"+self.attRBC+"_find_"+key+"_cn_name\',\'"+key+"\')\">"
                            temInput = temInput + "\r                                    <input name=\""+self.attRBC+"_"+key+"\" id=\""+self.attRBC+"_"+key+"\" value=\"\" type=\"text\" style=\"display:none\">"
                            temInput = temInput + "\r                                 </span>"
                        else:
                            temInput = "<select class=\"form-control\" id=\""+self.attRBC+"_"+key+"\" name=\""+self.attRBC+"_"+key+"\" placeholder=\"请输入信息\"></select>" 
                        break
                temResult = temResult + "\r                                "+temInput
                temResult = temResult + "\r                            </div>"
                temResult = temResult + "\r                        </div>"
                temResult = temResult + "\r                    </div>"
            elif isinstance(getattr(self.objN,"att"+key),int):                
                temResult = temResult + "\r                    <div class=\"col-sm-12 form-group\">"
                temResult = temResult + "\r                        <div class=\"col-sm-12\">"
                temResult = temResult + "\r                            <label for=\""+self.attRBC+"_"+key+"\" class=\"col-sm-3 control-label\">"+temValue+":*</label>" 
                temResult = temResult + "\r                            <div class=\"col-sm-9\">"
                temInput = "<input class=\"form-control\" id=\""+self.attRBC+"_"+key+"\" name=\""+self.attRBC+"_"+key+"\" type=\"text\" placeholder=\"请输入信息\"/>"
                for key2 in list(self.temSelectAtt.keys()):                        
                    if key.upper() == key2.upper():
                        if Com_Fun.GetHashTable(self.temFindAtt,key2) != "":
                            temInput = "\r                                 <span type=\"find\">"
                            temInput = temInput + "\r                                    <input class=\"form-control form-search\" id=\""+self.attRBC+"_find_"+key+"_cn_name\" name=\""+self.attRBC+"_find_"+key+"_cn_name\" type=\"text\" readonly=\"true\">"
                            temInput = temInput + "\r                                    <img src=\"../img/search.png\" onclick=\""+self.attRBC+"_"+key+"_cn_name_fun()\">"
                            temInput = temInput + "\r                                    <img src=\"../img/error.png\" onclick=\""+self.attRBC+"_clear_input_cn_name(\'"+self.attRBC+"_find_"+key+"_cn_name\',\'"+key+"\')\">"
                            temInput = temInput + "\r                                    <input name=\""+self.attRBC+"_"+key+"\" id=\""+self.attRBC+"_"+key+"\" value=\"-1\" type=\"text\" style=\"display:none\">"
                            temInput = temInput + "\r                                 </span>"
                        else:
                            temInput = "<select class=\"form-control\" id=\""+self.attRBC+"_"+key+"\" name=\""+self.attRBC+"_"+key+"\" placeholder=\"请输入信息\"></select>" 
                        break
                temResult = temResult + "\r                                "+temInput
                temResult = temResult + "\r                            </div>"
                temResult = temResult + "\r                        </div>"
                temResult = temResult + "\r                    </div>"
            elif isinstance(getattr(self.objN,"att"+key),float):                
                temResult = temResult + "\r                    <div class=\"col-sm-12 form-group\">"
                temResult = temResult + "\r                        <div class=\"col-sm-12\">"
                temResult = temResult + "\r                            <label for=\""+self.attRBC+"_"+key+"\" class=\"col-sm-3 control-label\">"+temValue+":*</label>" 
                temResult = temResult + "\r                            <div class=\"col-sm-9\">"
                temInput = "<input class=\"form-control\" id=\""+self.attRBC+"_"+key+"\" name=\""+self.attRBC+"_"+key+"\" type=\"text\" placeholder=\"请输入信息\"/>"
                for key2 in list(self.temSelectAtt.keys()):                        
                    if key.upper() == key2.upper():
                        if Com_Fun.GetHashTable(self.temFindAtt,key2) != "":
                            temInput = "\r                                 <span type=\"find\">"
                            temInput = temInput + "\r                                    <input class=\"form-control form-search\" id=\""+self.attRBC+"_find_"+key+"_cn_name\" name=\""+self.attRBC+"_find_"+key+"_cn_name\" type=\"text\" readonly=\"true\">"
                            temInput = temInput + "\r                                    <img src=\"../img/search.png\" id=\""+self.attRBC+"_Ope_"+key+"\" onclick=\""+key+"_cn_name_fun()\">"
                            temInput = temInput + "\r                                    <img src=\"../img/error.png\" id=\""+self.attRBC+"_Ope_"+key+"\" onclick=\""+self.attRBC+"_clear_input_cn_name(\'"+self.attRBC+"_find_"+key+"_cn_name\',\'"+self.attRBC+"_"+key+"\')\">"
                            temInput = temInput + "\r                                    <input name=\""+self.attRBC+"_"+key+"\" id=\""+self.attRBC+"_"+key+"\" value=\"-1\" type=\"text\" style=\"display:none\">"
                            temInput = temInput + "\r                                 </span>"
                        else:
                            temInput = "<select class=\"form-control\" id=\""+self.attRBC+"_"+key+"\" name=\""+self.attRBC+"_"+key+"\" placeholder=\"请输入信息\"></select>" 
                        break
                temResult = temResult + "\r                                "+temInput
                temResult = temResult + "\r                            </div>"
                temResult = temResult + "\r                        </div>"
                temResult = temResult + "\r                    </div>"
            elif getattr(self.objN,"att"+key) is None:                
                temResult = temResult + "\r                    <div class=\"col-sm-12 form-group\">"
                temResult = temResult + "\r                        <div class=\"col-sm-12\">"
                temResult = temResult + "\r                            <label for=\""+self.attRBC+"_"+key+"\" class=\"col-sm-3 control-label\">"+temValue+":*</label>" 
                temResult = temResult + "\r                            <div class=\"col-sm-9\">"
                temResult = temResult + "\r                                <input class=\"form-control datetimePicker\" id=\""+self.attRBC+"_"+key+"\" name=\""+self.attRBC+"_"+key+"\" type=\"text\"  placeholder=\"请设置时间格式\"/>"
                temResult = temResult + "\r                            </div>"
                temResult = temResult + "\r                        </div>"
                temResult = temResult + "\r                    </div>"
        temResult = temResult + "\r"
        return temResult
    def WriteBizHtml_1(self,templatePath,tempTargetPath,temTABLE_NAME):
        readFile = None
        writeFile = None            
        try: 
            if os.path.exists(templatePath+Com_Para.zxyPath+"template_table3.vue") == False :
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"template_table3.vue模板文件不存在"+ "\"}]}"
                return ""
            else:
                readFile = open(templatePath+Com_Para.zxyPath+"template_table3.vue", 'r',encoding=Com_Para.U_CODE)
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+".vue") == False :
                writeFile = open(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+".vue", 'a',encoding=Com_Para.U_CODE)
            else:
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\""+temTABLE_NAME+".vue文件已存在"+ "\"}]}"
                return ""
            while True and readFile is not None and writeFile is not None:
                temLines = readFile.readline()
                if self.param_value3 != "":
                    temLines = temLines.replace("{template_table_CN}",self.param_value3).replace("{$RDA$}",self.attRDA).replace("{$RBC$}",self.attRBC)
                temLines = temLines.replace("{template_table_EN}",temTABLE_NAME).replace("{$RDA$}",self.attRDA).replace("{$RBC$}",self.attRBC)
                writeFile.write(temLines)
                if temLines.find("<!-- query conditions begin -->") != -1:
                    writeFile.write(self.AppendQryCondHtml())
                    writeFile.write(self.AppendFindCondHtml())
                elif temLines.find("<!-- Html Main Sub Table begin -->") != -1:
                    writeFile.write(self.AppendHMSTHtml())
                if not temLines:
                    break
            self.UpdRandom(temTABLE_NAME)
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if readFile != None:
                readFile.close()
            if writeFile != None:
                writeFile.close()
    def WriteBizHtml_2(self,templatePath,tempTargetPath,temTABLE_NAME):
        readFile = None
        writeFile = None            
        try: 
            if os.path.exists(templatePath+Com_Para.zxyPath+"template_table3_$.vue") == False :
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"template_table3_$.vue模板文件不存在"+ "\"}]}"
                return ""
            else:
                readFile = open(templatePath+Com_Para.zxyPath+"template_table3_$.vue", 'r',encoding=Com_Para.U_CODE)
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+"_$.vue") == False :
                writeFile = open(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+"_$.vue", 'a',encoding=Com_Para.U_CODE)
            else:
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\""+temTABLE_NAME+"_$.vue文件已存在"+ "\"}]}"
                return ""
            while True and readFile is not None and writeFile is not None:
                temLines = readFile.readline()
                if self.param_value3 != "":
                    temLines = temLines.replace("{template_table_CN}",self.param_value3).replace("{$RDA$}",self.attRDA).replace("{$RBC$}",self.attRBC)
                temLines = temLines.replace("{template_table_EN}",temTABLE_NAME).replace("{$RDA$}",self.attRDA).replace("{$RBC$}",self.attRBC)
                writeFile.write(temLines)                
                if temLines.find("<!-- form column begin -->") != -1:
                    writeFile.write(self.AppendEditFormHtml())
                if not temLines:
                    break
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if readFile != None:
                readFile.close()
            if writeFile != None:
                writeFile.close()