#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import json, importlib, os,platform,datetime,xlrd,sys
from xlrd import xldate_as_tuple
from urllib.parse import unquote
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.db_Self.Db_Common_Self import Db_Common_Self
from com.plugins.A01_9LK33T import A01_Para
from com.plugins.A01_9LK33T.A01_Fun import A01_Fun
from com.zxy.business.Power_Control import Power_Control
from com.plugins.A01_9LK33T.t_station_info import t_station_info
from com.plugins.A01_9LK33T.t_code_element import t_code_element
from com.plugins.A01_9LK33T.t_station_ele_link import t_station_ele_link
from com.zxy.z_debug import z_debug
class Bus_Ope_DB_Cent(z_debug):
    def __init__(self):
        pass
    def init_page(self):
        self.monitor_had(0)
        self.monitor_had(30)
        if len(A01_Para.ht_t_station_info) == 0 or int(Com_Fun.GetTime("%M")) % 10 == 0:
            A01_Para.ht_t_station_info = self.Get_t_station_info()
            A01_Para.ht_t_code_element = self.Get_t_code_element()
            A01_Para.ht_t_station_ele_link = self.Get_t_station_ele_link()
    def monitor_had(self,iNum):
        temdbc2 = Db_Common2()
        tbName = "monitor_data_min"+Com_Fun.DateTimeAdd(datetime.datetime.now(), "d",iNum).strftime('%Y%m')
        tbName2 = "monitor_flag_min"+Com_Fun.DateTimeAdd(datetime.datetime.now(), "d",iNum).strftime('%Y%m')
        tbName3 = "monitor_max_min"+Com_Fun.DateTimeAdd(datetime.datetime.now(), "d",iNum).strftime('%Y%m')
        strSql = "select count(1) from "+tbName+" limit 1"
        temValue = temdbc2.Common_Sql(strSql)
        if temValue == None:
            temTdc = Com_Fun.GetHashTable(Com_Para.htDb,2)
            if temTdc.attDB_DriverClassName != "org.sqlite.JDBC":
                strSql = "Create  TABLE "+tbName+"("
                strSql = strSql+"MAIN_ID int(11) NOT NULL AUTO_INCREMENT"
                strSql = strSql+",station_code varchar(50) NOT NULL"
                strSql = strSql+",get_time datetime NOT NULL DEFAULT now()"
                strSql = strSql+",val_01 varchar(10)"
                strSql = strSql+",val_02 varchar(10)"
                strSql = strSql+",val_03 varchar(10)"
                strSql = strSql+",val_04 varchar(10)"
                strSql = strSql+",val_05 varchar(10)"
                strSql = strSql+",val_06 varchar(10)"
                strSql = strSql+",val_07 varchar(10)"
                strSql = strSql+",val_08 varchar(10)"
                strSql = strSql+",val_09 varchar(10)"
                strSql = strSql+",val_10 varchar(10)"
                strSql = strSql+",val_11 varchar(10)"
                strSql = strSql+",val_12 varchar(10)"
                strSql = strSql+",val_13 varchar(10)"
                strSql = strSql+",val_14 varchar(10)"
                strSql = strSql+",val_15 varchar(10)"
                strSql = strSql+",val_16 varchar(10)"
                strSql = strSql+",val_17 varchar(10)"
                strSql = strSql+",val_18 varchar(10)"
                strSql = strSql+",val_19 varchar(10)"
                strSql = strSql+",val_20 varchar(10)"
                strSql = strSql+",val_21 varchar(10)"
                strSql = strSql+",val_22 varchar(10)"
                strSql = strSql+",val_23 varchar(10)"
                strSql = strSql+",val_24 varchar(10)"
                strSql = strSql+",val_25 varchar(10)"
                strSql = strSql+",val_26 varchar(10)"
                strSql = strSql+",val_27 varchar(10)"
                strSql = strSql+",val_28 varchar(10)"
                strSql = strSql+",val_29 varchar(10)"
                strSql = strSql+",val_30 varchar(10)"
                strSql = strSql+",val_31 varchar(10)"
                strSql = strSql+",val_32 varchar(10)"
                strSql = strSql+",val_33 varchar(10)"
                strSql = strSql+",val_34 varchar(10)"
                strSql = strSql+",val_35 varchar(10)"
                strSql = strSql+",val_36 varchar(10)"
                strSql = strSql+",val_37 varchar(10)"
                strSql = strSql+",val_38 varchar(10)"
                strSql = strSql+",val_39 varchar(10)"
                strSql = strSql+",val_40 varchar(10)"
                strSql = strSql+",update_time datetime NOT NULL DEFAULT now()"
                strSql = strSql+",m_flag int"
                strSql = strSql+",m_flag1 int"
                strSql = strSql+",m_flag2 int"
                strSql = strSql+",m_flag3 int"
                strSql = strSql+",m_flag4 int"
                strSql = strSql+",m_flag5 int"
                strSql = strSql+",m_flag6 int"
                strSql = strSql+",m_flag7 int"
                strSql = strSql+",m_flag8 int"
                strSql = strSql+",m_flag9 int"
                strSql = strSql+",m_flag10 int"
                strSql = strSql+",PRIMARY KEY (MAIN_ID)"
                strSql = strSql+",UNIQUE KEY MAIN_ID (MAIN_ID)"
                strSql = strSql+");" 
            else:
                strSql = "Create  TABLE "+tbName+"("
                strSql = strSql+"[main_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"
                strSql = strSql+",[station_code] varchar(50) NOT NULL"
                strSql = strSql+",[get_time] datetime NOT NULL DEFAULT (datetime('now'))"
                strSql = strSql+",[val_01] varchar(10)"
                strSql = strSql+",[val_02] varchar(10)"
                strSql = strSql+",[val_03] varchar(10)"
                strSql = strSql+",[val_04] varchar(10)"
                strSql = strSql+",[val_05] varchar(10)"
                strSql = strSql+",[val_06] varchar(10)"
                strSql = strSql+",[val_07] varchar(10)"
                strSql = strSql+",[val_08] varchar(10)"
                strSql = strSql+",[val_09] varchar(10)"
                strSql = strSql+",[val_10] varchar(10)"
                strSql = strSql+",[val_11] varchar(10)"
                strSql = strSql+",[val_12] varchar(10)"
                strSql = strSql+",[val_13] varchar(10)"
                strSql = strSql+",[val_14] varchar(10)"
                strSql = strSql+",[val_15] varchar(10)"
                strSql = strSql+",[val_16] varchar(10)"
                strSql = strSql+",[val_17] varchar(10)"
                strSql = strSql+",[val_18] varchar(10)"
                strSql = strSql+",[val_19] varchar(10)"
                strSql = strSql+",[val_20] varchar(10)"
                strSql = strSql+",[val_21] varchar(10)"
                strSql = strSql+",[val_22] varchar(10)"
                strSql = strSql+",[val_23] varchar(10)"
                strSql = strSql+",[val_24] varchar(10)"
                strSql = strSql+",[val_25] varchar(10)"
                strSql = strSql+",[val_26] varchar(10)"
                strSql = strSql+",[val_27] varchar(10)"
                strSql = strSql+",[val_28] varchar(10)"
                strSql = strSql+",[val_29] varchar(10)"
                strSql = strSql+",[val_30] varchar(10)"
                strSql = strSql+",[val_31] varchar(10)"
                strSql = strSql+",[val_32] varchar(10)"
                strSql = strSql+",[val_33] varchar(10)"
                strSql = strSql+",[val_34] varchar(10)"
                strSql = strSql+",[val_35] varchar(10)"
                strSql = strSql+",[val_36] varchar(10)"
                strSql = strSql+",[val_37] varchar(10)"
                strSql = strSql+",[val_38] varchar(10)"
                strSql = strSql+",[val_39] varchar(10)"
                strSql = strSql+",[val_40] varchar(10)"
                strSql = strSql+",[update_time] datetime DEFAULT (datetime('now'))"
                strSql = strSql+",[m_flag] int"
                strSql = strSql+",[m_flag1] int"
                strSql = strSql+",[m_flag2] int"
                strSql = strSql+",[m_flag3] int"
                strSql = strSql+",[m_flag4] int"
                strSql = strSql+",[m_flag5] int"
                strSql = strSql+",[m_flag6] int"
                strSql = strSql+",[m_flag7] int"
                strSql = strSql+",[m_flag8] int"
                strSql = strSql+",[m_flag9] int"
                strSql = strSql+",[m_flag10] int"
                strSql = strSql+");"
            temdbc2.CommonExec_Sql(strSql)
            if temTdc.attDB_DriverClassName != "org.sqlite.JDBC":
                strSql = "Create  TABLE "+tbName2+"("
                strSql = strSql+"MAIN_ID int(11) NOT NULL AUTO_INCREMENT"
                strSql = strSql+",station_code varchar(50) NOT NULL"
                strSql = strSql+",get_time datetime NOT NULL DEFAULT now()"
                strSql = strSql+",val_01 varchar(10)"
                strSql = strSql+",val_02 varchar(10)"
                strSql = strSql+",val_03 varchar(10)"
                strSql = strSql+",val_04 varchar(10)"
                strSql = strSql+",val_05 varchar(10)"
                strSql = strSql+",val_06 varchar(10)"
                strSql = strSql+",val_07 varchar(10)"
                strSql = strSql+",val_08 varchar(10)"
                strSql = strSql+",val_09 varchar(10)"
                strSql = strSql+",val_10 varchar(10)"
                strSql = strSql+",val_11 varchar(10)"
                strSql = strSql+",val_12 varchar(10)"
                strSql = strSql+",val_13 varchar(10)"
                strSql = strSql+",val_14 varchar(10)"
                strSql = strSql+",val_15 varchar(10)"
                strSql = strSql+",val_16 varchar(10)"
                strSql = strSql+",val_17 varchar(10)"
                strSql = strSql+",val_18 varchar(10)"
                strSql = strSql+",val_19 varchar(10)"
                strSql = strSql+",val_20 varchar(10)"
                strSql = strSql+",val_21 varchar(10)"
                strSql = strSql+",val_22 varchar(10)"
                strSql = strSql+",val_23 varchar(10)"
                strSql = strSql+",val_24 varchar(10)"
                strSql = strSql+",val_25 varchar(10)"
                strSql = strSql+",val_26 varchar(10)"
                strSql = strSql+",val_27 varchar(10)"
                strSql = strSql+",val_28 varchar(10)"
                strSql = strSql+",val_29 varchar(10)"
                strSql = strSql+",val_30 varchar(10)"
                strSql = strSql+",val_31 varchar(10)"
                strSql = strSql+",val_32 varchar(10)"
                strSql = strSql+",val_33 varchar(10)"
                strSql = strSql+",val_34 varchar(10)"
                strSql = strSql+",val_35 varchar(10)"
                strSql = strSql+",val_36 varchar(10)"
                strSql = strSql+",val_37 varchar(10)"
                strSql = strSql+",val_38 varchar(10)"
                strSql = strSql+",val_39 varchar(10)"
                strSql = strSql+",val_40 varchar(10)"
                strSql = strSql+",update_time datetime NOT NULL DEFAULT now()"
                strSql = strSql+",m_flag int"
                strSql = strSql+",m_flag1 int"
                strSql = strSql+",m_flag2 int"
                strSql = strSql+",m_flag3 int"
                strSql = strSql+",m_flag4 int"
                strSql = strSql+",m_flag5 int"
                strSql = strSql+",m_flag6 int"
                strSql = strSql+",m_flag7 int"
                strSql = strSql+",m_flag8 int"
                strSql = strSql+",m_flag9 int"
                strSql = strSql+",m_flag10 int"
                strSql = strSql+",PRIMARY KEY (MAIN_ID)"
                strSql = strSql+",UNIQUE KEY MAIN_ID (MAIN_ID)"
                strSql = strSql+");" 
            else:
                strSql = "Create  TABLE "+tbName2+"("
                strSql = strSql+"[main_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"
                strSql = strSql+",[station_code] varchar(50) NOT NULL"
                strSql = strSql+",[get_time] datetime NOT NULL DEFAULT (datetime('now'))"
                strSql = strSql+",[val_01] varchar(10)"
                strSql = strSql+",[val_02] varchar(10)"
                strSql = strSql+",[val_03] varchar(10)"
                strSql = strSql+",[val_04] varchar(10)"
                strSql = strSql+",[val_05] varchar(10)"
                strSql = strSql+",[val_06] varchar(10)"
                strSql = strSql+",[val_07] varchar(10)"
                strSql = strSql+",[val_08] varchar(10)"
                strSql = strSql+",[val_09] varchar(10)"
                strSql = strSql+",[val_10] varchar(10)"
                strSql = strSql+",[val_11] varchar(10)"
                strSql = strSql+",[val_12] varchar(10)"
                strSql = strSql+",[val_13] varchar(10)"
                strSql = strSql+",[val_14] varchar(10)"
                strSql = strSql+",[val_15] varchar(10)"
                strSql = strSql+",[val_16] varchar(10)"
                strSql = strSql+",[val_17] varchar(10)"
                strSql = strSql+",[val_18] varchar(10)"
                strSql = strSql+",[val_19] varchar(10)"
                strSql = strSql+",[val_20] varchar(10)"
                strSql = strSql+",[val_21] varchar(10)"
                strSql = strSql+",[val_22] varchar(10)"
                strSql = strSql+",[val_23] varchar(10)"
                strSql = strSql+",[val_24] varchar(10)"
                strSql = strSql+",[val_25] varchar(10)"
                strSql = strSql+",[val_26] varchar(10)"
                strSql = strSql+",[val_27] varchar(10)"
                strSql = strSql+",[val_28] varchar(10)"
                strSql = strSql+",[val_29] varchar(10)"
                strSql = strSql+",[val_30] varchar(10)"
                strSql = strSql+",[val_31] varchar(10)"
                strSql = strSql+",[val_32] varchar(10)"
                strSql = strSql+",[val_33] varchar(10)"
                strSql = strSql+",[val_34] varchar(10)"
                strSql = strSql+",[val_35] varchar(10)"
                strSql = strSql+",[val_36] varchar(10)"
                strSql = strSql+",[val_37] varchar(10)"
                strSql = strSql+",[val_38] varchar(10)"
                strSql = strSql+",[val_39] varchar(10)"
                strSql = strSql+",[val_40] varchar(10)"
                strSql = strSql+",[update_time] datetime DEFAULT (datetime('now'))"
                strSql = strSql+",[m_flag] int"
                strSql = strSql+",[m_flag1] int"
                strSql = strSql+",[m_flag2] int"
                strSql = strSql+",[m_flag3] int"
                strSql = strSql+",[m_flag4] int"
                strSql = strSql+",[m_flag5] int"
                strSql = strSql+",[m_flag6] int"
                strSql = strSql+",[m_flag7] int"
                strSql = strSql+",[m_flag8] int"
                strSql = strSql+",[m_flag9] int"
                strSql = strSql+",[m_flag10] int"
                strSql = strSql+");"
            temdbc2.CommonExec_Sql(strSql)
            if temTdc.attDB_DriverClassName != "org.sqlite.JDBC":
                strSql = "Create  TABLE "+tbName3+"("
                strSql = strSql+"MAIN_ID int(11) NOT NULL AUTO_INCREMENT"
                strSql = strSql+",station_code varchar(50) NOT NULL"
                strSql = strSql+",get_time datetime NOT NULL DEFAULT now()"
                strSql = strSql+",val_01 varchar(10)"
                strSql = strSql+",val_02 varchar(10)"
                strSql = strSql+",val_03 varchar(10)"
                strSql = strSql+",val_04 varchar(10)"
                strSql = strSql+",val_05 varchar(10)"
                strSql = strSql+",val_06 varchar(10)"
                strSql = strSql+",val_07 varchar(10)"
                strSql = strSql+",val_08 varchar(10)"
                strSql = strSql+",val_09 varchar(10)"
                strSql = strSql+",val_10 varchar(10)"
                strSql = strSql+",val_11 varchar(10)"
                strSql = strSql+",val_12 varchar(10)"
                strSql = strSql+",val_13 varchar(10)"
                strSql = strSql+",val_14 varchar(10)"
                strSql = strSql+",val_15 varchar(10)"
                strSql = strSql+",val_16 varchar(10)"
                strSql = strSql+",val_17 varchar(10)"
                strSql = strSql+",val_18 varchar(10)"
                strSql = strSql+",val_19 varchar(10)"
                strSql = strSql+",val_20 varchar(10)"
                strSql = strSql+",val_21 varchar(10)"
                strSql = strSql+",val_22 varchar(10)"
                strSql = strSql+",val_23 varchar(10)"
                strSql = strSql+",val_24 varchar(10)"
                strSql = strSql+",val_25 varchar(10)"
                strSql = strSql+",val_26 varchar(10)"
                strSql = strSql+",val_27 varchar(10)"
                strSql = strSql+",val_28 varchar(10)"
                strSql = strSql+",val_29 varchar(10)"
                strSql = strSql+",val_30 varchar(10)"
                strSql = strSql+",val_31 varchar(10)"
                strSql = strSql+",val_32 varchar(10)"
                strSql = strSql+",val_33 varchar(10)"
                strSql = strSql+",val_34 varchar(10)"
                strSql = strSql+",val_35 varchar(10)"
                strSql = strSql+",val_36 varchar(10)"
                strSql = strSql+",val_37 varchar(10)"
                strSql = strSql+",val_38 varchar(10)"
                strSql = strSql+",val_39 varchar(10)"
                strSql = strSql+",val_40 varchar(10)"
                strSql = strSql+",update_time datetime NOT NULL DEFAULT now()"
                strSql = strSql+",m_flag int"
                strSql = strSql+",m_flag1 int"
                strSql = strSql+",m_flag2 int"
                strSql = strSql+",m_flag3 int"
                strSql = strSql+",m_flag4 int"
                strSql = strSql+",m_flag5 int"
                strSql = strSql+",m_flag6 int"
                strSql = strSql+",m_flag7 int"
                strSql = strSql+",m_flag8 int"
                strSql = strSql+",m_flag9 int"
                strSql = strSql+",m_flag10 int"
                strSql = strSql+",PRIMARY KEY (MAIN_ID)"
                strSql = strSql+",UNIQUE KEY MAIN_ID (MAIN_ID)"
                strSql = strSql+");" 
            else:
                strSql = "Create  TABLE "+tbName3+"("
                strSql = strSql+"[main_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"
                strSql = strSql+",[station_code] varchar(50) NOT NULL"
                strSql = strSql+",[get_time] datetime NOT NULL DEFAULT (datetime('now'))"
                strSql = strSql+",[val_01] varchar(80)"
                strSql = strSql+",[val_02] varchar(80)"
                strSql = strSql+",[val_03] varchar(80)"
                strSql = strSql+",[val_04] varchar(80)"
                strSql = strSql+",[val_05] varchar(80)"
                strSql = strSql+",[val_06] varchar(80)"
                strSql = strSql+",[val_07] varchar(80)"
                strSql = strSql+",[val_08] varchar(80)"
                strSql = strSql+",[val_09] varchar(80)"
                strSql = strSql+",[val_10] varchar(80)"
                strSql = strSql+",[val_11] varchar(80)"
                strSql = strSql+",[val_12] varchar(80)"
                strSql = strSql+",[val_13] varchar(80)"
                strSql = strSql+",[val_14] varchar(80)"
                strSql = strSql+",[val_15] varchar(80)"
                strSql = strSql+",[val_16] varchar(80)"
                strSql = strSql+",[val_17] varchar(80)"
                strSql = strSql+",[val_18] varchar(80)"
                strSql = strSql+",[val_19] varchar(80)"
                strSql = strSql+",[val_20] varchar(80)"
                strSql = strSql+",[val_21] varchar(80)"
                strSql = strSql+",[val_22] varchar(80)"
                strSql = strSql+",[val_23] varchar(80)"
                strSql = strSql+",[val_24] varchar(80)"
                strSql = strSql+",[val_25] varchar(80)"
                strSql = strSql+",[val_26] varchar(80)"
                strSql = strSql+",[val_27] varchar(80)"
                strSql = strSql+",[val_28] varchar(80)"
                strSql = strSql+",[val_29] varchar(80)"
                strSql = strSql+",[val_30] varchar(80)"
                strSql = strSql+",[val_31] varchar(80)"
                strSql = strSql+",[val_32] varchar(80)"
                strSql = strSql+",[val_33] varchar(80)"
                strSql = strSql+",[val_34] varchar(80)"
                strSql = strSql+",[val_35] varchar(80)"
                strSql = strSql+",[val_36] varchar(80)"
                strSql = strSql+",[val_37] varchar(80)"
                strSql = strSql+",[val_38] varchar(80)"
                strSql = strSql+",[val_39] varchar(80)"
                strSql = strSql+",[val_40] varchar(80)"
                strSql = strSql+",[update_time] datetime DEFAULT (datetime('now'))"
                strSql = strSql+",[m_flag] int"
                strSql = strSql+",[m_flag1] int"
                strSql = strSql+",[m_flag2] int"
                strSql = strSql+",[m_flag3] int"
                strSql = strSql+",[m_flag4] int"
                strSql = strSql+",[m_flag5] int"
                strSql = strSql+",[m_flag6] int"
                strSql = strSql+",[m_flag7] int"
                strSql = strSql+",[m_flag8] int"
                strSql = strSql+",[m_flag9] int"
                strSql = strSql+",[m_flag10] int"
                strSql = strSql+");"
            temdbc2.CommonExec_Sql(strSql)
        if iNum == 30:
            return 0
    def Get_t_station_ele_link(self):
        temStrSql = "select a.*,b.ELEMENT_VAL,b.ELEMENT_CODE,b.DTO_NUM,b.ELEMENT_NAME from t_station_ele_link a left join t_code_element b on a.ELEMENT_ID = b.MAIN_ID"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temt_station_ele_link = t_station_ele_link()
                vTime = ""
                temt_station_ele_link.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temt_station_ele_link.attSTATION_CODE = Com_Fun.NoNull(temItem[1])
                temt_station_ele_link.attELEMENT_ID = Com_Fun.ZeroNull(temItem[2])
                temt_station_ele_link.attUP_LEVEL = Com_Fun.NoNull(temItem[3])
                temt_station_ele_link.attDOWN_LEVEL = Com_Fun.NoNull(temItem[4])
                temt_station_ele_link.attUP_LIMIT = Com_Fun.NoNull(temItem[5])
                temt_station_ele_link.attDOWN_LIMIT = Com_Fun.NoNull(temItem[6])
                temt_station_ele_link.attS_STATE = Com_Fun.ZeroNull(temItem[7])
                temt_station_ele_link.attCREATE_DATE = Com_Fun.NoNull(temItem[8])
                temt_station_ele_link.attS_DESC = Com_Fun.NoNull(temItem[9])
                temt_station_ele_link.attELEMENT_VAL = Com_Fun.NoNull(temItem[10])
                temt_station_ele_link.attELEMENT_CODE = Com_Fun.NoNull(temItem[11])
                temt_station_ele_link.attDTO_NUM = Com_Fun.ZeroNull(temItem[12])
                temt_station_ele_link.attELEMENT_NAME = Com_Fun.NoNull(temItem[13])
                temList.append(temt_station_ele_link)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def Get_t_code_element(self):
        temStrSql = "select * from t_code_element"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temt_code_element = t_code_element()
                vTime = ""
                temt_code_element.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temt_code_element.attELEMENT_CODE = Com_Fun.NoNull(temItem[1])
                temt_code_element.attELEMENT_NAME = Com_Fun.NoNull(temItem[2])
                temt_code_element.attELEMENT_VAL = Com_Fun.NoNull(temItem[3])
                temt_code_element.attELEMENT_TYPE = Com_Fun.NoNull(temItem[4])
                temt_code_element.attELEMENT_UNIT = Com_Fun.NoNull(temItem[5])
                temt_code_element.attDTO_NUM = Com_Fun.NoNull(temItem[6])
                temt_code_element.attELEMENT_CODE2 = Com_Fun.NoNull(temItem[7])
                temt_code_element.attS_STATE = Com_Fun.ZeroNull(temItem[8])
                temt_code_element.attCREATE_DATE = Com_Fun.NoNull(temItem[9])
                temt_code_element.attS_DESC = Com_Fun.NoNull(temItem[10])
                temList.append(temt_code_element)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def Get_t_station_info(self):
        temStrSql = "select * from t_station_info"
        temDb2 = Db_Common2()
        temRs = temDb2.Common_Sql(temStrSql)
        temList = []
        try:
            for temItem in temRs:
                temt_station_info = t_station_info()
                vTime = ""
                temt_station_info.attMAIN_ID = Com_Fun.ZeroNull(temItem[0])
                temt_station_info.attSTATION_CODE = Com_Fun.NoNull(temItem[1])
                temt_station_info.attSTATION_NAME = Com_Fun.NoNull(temItem[2])
                temt_station_info.attSTATION_TYPE = Com_Fun.NoNull(temItem[3])
                temt_station_info.attMN_CODE = Com_Fun.NoNull(temItem[4])
                temt_station_info.attLONGITUDE = Com_Fun.NoNull(temItem[5])
                temt_station_info.attLATITUDE = Com_Fun.NoNull(temItem[6])
                temt_station_info.attS_STATE = Com_Fun.ZeroNull(temItem[7])
                temt_station_info.attCREATE_DATE = str(temItem[8])
                temt_station_info.attS_DESC = Com_Fun.NoNull(temItem[9])
                temList.append(temt_station_info)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+temStrSql +"\r\n"+repr(e)
            else:
                temLog = self.debug_info()+temStrSql +"\r\n"+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        return temList
    def upd_real(self,temmonitor_data_model):
        temdbc2 = Db_Common2()
        strSql = "update monitor_data_real set get_time = '"+str(temmonitor_data_model.get_time)+"' "
        for titems in A01_Para.ht_t_station_ele_link:
            if titems.attSTATION_CODE ==  temmonitor_data_model.station_code:
                temValue = getattr(temmonitor_data_model, titems.attELEMENT_VAL.lower())
                if temValue != "":
                    strSql = strSql+","+titems.attELEMENT_VAL+"='"+temValue+"' "            
        strRunSql = strSql +" where station_code = '"+temmonitor_data_model.station_code+"'"
        temdbc2.CommonExec_Sql(strRunSql)
    def ins_minute(self,temmonitor_data_model):
        if A01_Para.attLockMin.acquire():      
            if self.MinData_Had(temmonitor_data_model) == True:
                A01_Para.attLockMin.release()
                return None
            temdbc2 = Db_Common2()
            tbName = "monitor_data_min"+Com_Fun.GetDateInput("%Y%m","%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time)
            strSql = "insert into "+tbName+"(station_code,get_time,update_time,m_flag"
            strValue = " values('"+temmonitor_data_model.station_code+"','"+temmonitor_data_model.get_time[0:16]+":00"+"','"+Com_Fun.GetTimeDef()+"','0'"
            for titems in A01_Para.ht_t_station_ele_link:
                if titems.attSTATION_CODE ==  temmonitor_data_model.station_code:
                    strSql = strSql+","+titems.attELEMENT_VAL
                    strValue = strValue +",'"+getattr(temmonitor_data_model, titems.attELEMENT_VAL.lower())+"'"
            strRunSql = strSql +") "+strValue+")"
            temdbc2.CommonExec_Sql(strRunSql)
            A01_Para.attLockMin.release()
    def MinData_Had(self,temmonitor_data_model):
        temdbc2 = Db_Common2()
        tbName = "monitor_data_min"+Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time).strftime('%Y%m')
        strSql = "select count(1) from "+tbName+" where get_time ='"+Com_Fun.GetTimeInput("%Y-%m-%d %H:%M:%S",temmonitor_data_model.get_time).strftime("%Y-%m-%d %H:%M:00")+"' and station_code = '"+temmonitor_data_model.station_code+"'"
        temValue = temdbc2.Common_Sql(strSql)
        if temValue is None:
            return False
        elif Com_Fun.ZeroNull(temValue[0][0]) == 0:
            return False
        else:
            return True
    def HourData_Had(self,temmonitor_data_model):
        temdbc2 = Db_Common2()
        tbName = "monitor_data_hour"
        strSql = "select count(1) from "+tbName+" where get_time ='"+temmonitor_data_model.get_time+"' and station_code = '"+temmonitor_data_model.station_code+"'"
        temValue = temdbc2.Common_Sql(strSql)
        if temValue is None:
            return False
        elif Com_Fun.ZeroNull(temValue[0][0]) == 0:
            return False
        else:
            return True
    def ins_hour(self,temmonitor_data_model):  
        if A01_Para.attLockHour.acquire():      
            if self.HourData_Had(temmonitor_data_model) == True:
                A01_Para.attLockHour.release()
                return None
            temdbc2 = Db_Common2()
            tbName = "monitor_data_hour"
            strSql = "insert into "+tbName+"(station_code,get_time,update_time,m_flag"
            strValue = " values('"+temmonitor_data_model.station_code+"','"+temmonitor_data_model.get_time[0:16]+":00"+"','"+Com_Fun.GetTimeDef()+"','0'"
            for titems in A01_Para.ht_t_station_ele_link:
                if titems.attSTATION_CODE ==  temmonitor_data_model.station_code:
                    temAlerm = ""
                    temValue = getattr(temmonitor_data_model, titems.attELEMENT_VAL.lower())              
                    try:
                        if titems.attDOWN_LIMIT != "" and temValue != "" and float(titems.attDOWN_LIMIT) > float(temValue) :
                            if temAlerm == "":
                                temAlerm = titems.attELEMENT_NAME+"超量程报警，报警值"+titems.attDOWN_LIMIT+",监测值"+temValue
                            else:
                                temAlerm = temAlerm+";"+titems.attELEMENT_NAME+"超量程报警，报警值"+titems.attDOWN_LIMIT+",监测值"+temValue
                        elif titems.attUP_LIMIT != "" and temValue != "" and float(titems.attUP_LIMIT) < float(temValue) :
                            if temAlerm == "":
                                temAlerm = titems.attELEMENT_NAME+"超量程报警，报警值"+titems.attUP_LIMIT+",监测值"+temValue
                            else:
                                temAlerm = temAlerm+";"+titems.attELEMENT_NAME+"超量程报警，报警值"+titems.attUP_LIMIT+",监测值"+temValue
                        elif titems.attDOWN_LEVEL != "" and temValue != "" and float(titems.attDOWN_LEVEL) > float(temValue) :
                            if temAlerm == "":
                                temAlerm = titems.attELEMENT_NAME+"超标报警，报警值"+titems.attDOWN_LEVEL+",监测值"+temValue
                            else:
                                temAlerm = temAlerm+";"+titems.attELEMENT_NAME+"超标报警，报警值"+titems.attDOWN_LEVEL+",监测值"+temValue
                        elif titems.attUP_LEVEL != "" and temValue != "" and float(titems.attUP_LEVEL) < float(temValue) :
                            if temAlerm == "":
                                temAlerm = titems.attELEMENT_NAME+"超标报警，报警值"+titems.attUP_LEVEL+",监测值"+temValue
                            else:
                                temAlerm = temAlerm+";"+titems.attELEMENT_NAME+"超标报警，报警值"+titems.attUP_LEVEL+",监测值"+temValue   
                        if temAlerm != "":
                            strSqlAlerm = "insert into t_station_alarm(STATION_CODE,ELEMENT_ID,ALARM_INFO,DATA_VAL,GET_TIME,UP_LEVEL,DOWN_LEVEL,UP_LIMIT,DOWN_LIMIT,S_STATE,CREATE_DATE) "
                            strSqlAlerm = strSqlAlerm + " values('"+temmonitor_data_model.station_code+"','"+str(titems.attELEMENT_ID)+"','"+temAlerm+"','"+str(temValue)+"','"
                            strSqlAlerm = strSqlAlerm +str(temmonitor_data_model.get_time) +"','"+str(titems.attUP_LEVEL)+"','"+str(titems.attDOWN_LEVEL)+"','"+str(titems.attUP_LIMIT)+"','"+str(titems.attDOWN_LIMIT)+"','0','"+Com_Fun.GetTimeDef()+"')"
                            temdbc2.CommonExec_Sql(strSqlAlerm);
                    except Exception as e:
                        temLog = ""
                        if str(type(self)) == "<class 'type'>":
                            temLog = self.debug_info(self)+strSqlAlerm +"\r\n"+repr(e)
                        else:
                            temLog = self.debug_info()+strSqlAlerm +"\r\n"+repr(e)
                        uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
                        uL.WriteLog()
                    strSql = strSql+","+titems.attELEMENT_VAL
                    strValue = strValue +",'"+temValue+"'"    
            strRunSql = strSql +") "+strValue+")"
            temdbc2.CommonExec_Sql(strRunSql)
            A01_Para.attLockHour.release()