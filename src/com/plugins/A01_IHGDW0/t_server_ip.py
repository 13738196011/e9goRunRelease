#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_server_ip(object):
    attmain_id = -1
    attserver_ip = ""
    attserver_port = ""
    attserver_name = ""
    attcreate_date = None
    atts_desc = ""
    attup_protocol_id = -1
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")