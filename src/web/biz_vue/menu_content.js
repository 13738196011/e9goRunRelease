//选择某一行
var AAB8DC_select_menu_content_rowId = "";
//按钮事件新增或编辑
var AAB8DC_type = "";
//其他页面传到本页面参数
var AAB8DC_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var AAB8DC_tem_menu_id = "-1";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var AAB8DC_ary_menu_id = null;var AAB8DC_ary_is_ava = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];var AAB8DC_ary_role_id = [{'main_id': '1', 'cn_name': '开发者'}, {'main_id': '2', 'cn_name': '系统管理员'}, {'main_id': '3', 'cn_name': '运维人员'}, {'main_id': '4', 'cn_name': '普通用户'}];
/*declare select options end*/

//业务逻辑数据开始
function AAB8DC_menu_content_biz_start(inputparam) {
	layer.close(ly_index);
	AAB8DC_param = inputparam;
    /*biz begin*/
    var inputdata = {        "param_name": "N01_menu_content$menu_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AAB8DC_get_N01_menu_content$menu_id");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function AAB8DC_format_menu_id(value, row, index) {    var objResult = value;    for(i = 0; i < AAB8DC_ary_menu_id.length; i++) {        var obj = AAB8DC_ary_menu_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AAB8DC_format_is_ava(value, row, index) {    var objResult = value;    for(i = 0; i < AAB8DC_ary_is_ava.length; i++) {        var obj = AAB8DC_ary_is_ava[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AAB8DC_format_role_id(value, row, index) {    var objResult = value;    for(i = 0; i < AAB8DC_ary_role_id.length; i++) {        var obj = AAB8DC_ary_role_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AAB8DC_get_N01_menu_content$menu_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_menu_content$menu_id) == false)        return false;    AAB8DC_ary_menu_id = input.N01_menu_content$menu_id;    AAB8DC_init_menu_content();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function AAB8DC_menu_id_cn_name_fun(){    index_subhtml = "menu_info.html"    random_subhtml = "XXXXXX";    if(loadHtmlSubVueFun("biz_vue/menu_info.html","AAB8DC_menu_content_call_vue") == true){        var n = Get_RandomDiv("XXXXXX","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#XXXXXX_menu_info.html_Events').bootstrapTable('resetView');                XXXXXX_param["ly_index"] = index;                XXXXXX_param["target_name"] = "AAB8DC_find_menu_id_cn_name"                XXXXXX_param["target_id"] = "AAB8DC_find_menu_id"                XXXXXX_param["sourc_id"] = "main_id"                XXXXXX_param["sourc_name"] = "menu_name"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function AAB8DC_page_end(){
}

//首页菜单显示列定义
var AAB8DC_menu_content = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '菜单url',        field: 'MENU_ID',        sortable: true        ,formatter: AAB8DC_format_menu_id    },    {        title: '是否启用',        field: 'IS_AVA',        sortable: true        ,formatter: AAB8DC_format_is_ava    },    {        title: '角色',        field: 'ROLE_ID',        sortable: true        ,formatter: AAB8DC_format_role_id    }	
	/*table column end*/
];

//页面初始化
function AAB8DC_init_menu_content() {
	$(window).resize(function () {
		  $('#AAB8DC_menu_content_Events').bootstrapTable('resetView');
	});
	//首页菜单查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AAB8DC_btn_menu_content_query').click();
}

//查询接口
function AAB8DC_menu_content_query() {
    $('#AAB8DC_menu_content_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_menu_content",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": AAB8DC_tem_menu_id        ,"param_value2": AAB8DC_tem_menu_id		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AAB8DC_get_N01_sel_menu_content");
}

//查询结果
function AAB8DC_get_N01_sel_menu_content(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_menu_content) == false)
        return false;    
	var s_data = input.N01_sel_menu_content;
    AAB8DC_select_menu_content_rowId = "";
    $('#AAB8DC_menu_content_Events').bootstrapTable('destroy');
    $("#AAB8DC_menu_content_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AAB8DC_menu_content_Events').find("tr.changeColor").removeClass('changeColor');
                AAB8DC_select_menu_content_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AAB8DC_menu_content_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AAB8DC_select_menu_content_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AAB8DC_param.hasOwnProperty("target_name"))
			{
				$("#"+AAB8DC_param["target_id"]).val(eval("row."+AAB8DC_param["sourc_id"].toUpperCase()));
				$("#"+AAB8DC_param["target_name"]).val(s_decode(eval("row."+AAB8DC_param["sourc_name"].toUpperCase())));				
				layer.close(AAB8DC_param["ly_index"]);
			}
		},
        toolbar: "#AAB8DC_menu_content_Toolbar",
        columns: AAB8DC_menu_content,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AAB8DC_btn_menu_content_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    AAB8DC_tem_menu_id = "-1";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    BAB8DC_ary_menu_id = null;    $("#AAB8DC_find_menu_id_cn_name").val("");    $("#AAB8DC_find_menu_id").val("-1");
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_menu_content$menu_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AAB8DC_get_N01_menu_content$menu_id");	
    /*biz end*/
})

//查询按钮
$('#AAB8DC_btn_menu_content_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    AAB8DC_tem_menu_id = $("#AAB8DC_find_menu_id").val();	
    /*set query param end*/
	AAB8DC_menu_content_query();
})

//vue回调
function AAB8DC_menu_content_call_vue(objResult){
	if(index_subhtml == "menu_content_$.vue")
	{
		var n = Get_RandomDiv("BAB8DC",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AAB8DC_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temmenu_id = $("#AAB8DC_find_menu_id_cn_name").val();                if(temmenu_id != ""){                    inputdata["menu_id_cn_name"] = temmenu_id;                    inputdata["menu_id"] = $("#AAB8DC_menu_id").val();                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/menu_content_$.js","BAB8DC_menu_content_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "menu_info.html"){        var n = Get_RandomDiv("XXXXXX",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":AAB8DC_type,                    "ly_index":index,                    "target_name":"AAB8DC_find_menu_id_cn_name",                    "target_id":"AAB8DC_find_menu_id",                    "sourc_id":"main_id",                    "sourc_name":"menu_name"                };                loadScript_hasparam("biz_vue/menu_info.html.js","XXXXXX_menu_info.html_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//新增按钮
$("#AAB8DC_btn_menu_content_add").click(function () {
	AAB8DC_type = "add";
	index_subhtml = "menu_content_$.vue";
	if(loadHtmlSubVueFun("biz_vue/menu_content_$.vue","AAB8DC_menu_content_call_vue") == true){
		var n = Get_RandomDiv("BAB8DC","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BAB8DC_param["type"] = AAB8DC_type;
				BAB8DC_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temmenu_id = $("#AAB8DC_find_menu_id_cn_name").val();                if(temmenu_id != ""){                    BAB8DC_param["menu_id_cn_name"] = temmenu_id;                    BAB8DC_param["menu_id"] = $("#AAB8DC_menu_id").val();                }				
				/*Send Two FindSelect param end*/

				BAB8DC_clear_edit_info();
			},
			end: function(){
				BAB8DC_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AAB8DC_btn_menu_content_edit").click(function () {
	if (AAB8DC_select_menu_content_rowId != "") {
		AAB8DC_type = "edit";
		index_subhtml = "menu_content_$.vue";
		if(loadHtmlSubVueFun("biz_vue/menu_content_$.vue","AAB8DC_menu_content_call_vue") == true){
			var n = Get_RandomDiv("BAB8DC","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BAB8DC_param["type"] = AAB8DC_type;
					BAB8DC_param["ly_index"] = index;
					BAB8DC_clear_edit_info();
					BAB8DC_get_edit_info();
				},
				end: function(){
					BAB8DC_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AAB8DC_btn_menu_content_delete').click(function () {
	//单行选择
	var rowData = $("#AAB8DC_menu_content_Events").bootstrapTable('getData')[AAB8DC_select_menu_content_rowId];
	//多行选择
	var rowDatas = AAB8DC_sel_row_menu_content();
	if (AAB8DC_select_menu_content_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_menu_content",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AAB8DC_N01_del_menu_content");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_menu_content",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AAB8DC_N01_del_menu_content");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AAB8DC_N01_del_menu_content(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_menu_content) == true)
		AAB8DC_menu_content_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function AAB8DC_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AAB8DC_sel_row_menu_content(){
	//获得选中行
	var checkedbox= $("#AAB8DC_menu_content_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}