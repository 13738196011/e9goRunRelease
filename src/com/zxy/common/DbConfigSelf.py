#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import configparser
from com.zxy.common import Com_Para
from com.zxy.z_debug import z_debug
class DbConfigSelf(z_debug):
    def __init__(self):
        pass
    @staticmethod
    def GetDbConfigSelfNew():
        temPath = Com_Para.ApplicationPath + Com_Para.zxyPath + Com_Para.ACTIONPATH
        temProp = configparser.ConfigParser()
        try:
            temProp.read(temPath)
            Com_Para.driverClassName = temProp.get("DB", "dataSource.driverClassName")            
            if Com_Para.driverClassName == "org.sqlite.JDBC" :
                Com_Para.incrementalConnections = 1
                Com_Para.maxConnections = 1
                Com_Para.IL_DB = 1
                temStrRem = temProp.get("DB","dataSource.url").replace("@",Com_Para.ApplicationPath + Com_Para.zxyPath)
                Com_Para.url = temStrRem
            else:
                Com_Para.url = temProp.get("DB", "dataSource.url")
            Com_Para.username = temProp.get("DB", "dataSource.username")
            Com_Para.password = temProp.get("DB", "dataSource.password")
            Com_Para.port = temProp.getint("DB", "server.port")
            Com_Para.UserDebug = temProp.getint("DB", "userDebug")
        except Exception as e:
            print("GetDbConfigSelfNew:"+repr(e))
        finally:
            pass