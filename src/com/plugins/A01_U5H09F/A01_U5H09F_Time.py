#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import time,json,importlib,datetime,binascii
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_IHGDW0.A01_Fun import A01_Fun
from com.plugins.A01_IHGDW0.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_IHGDW0 import A01_Para
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.comport.ComModBus import ComModBus
from com.plugins.A01_IHGDW0.monitor_data_model import monitor_data_model
from com.zxy.z_debug import z_debug
class A01_U5H09F_Time(z_debug):
    strContinue      = "1"   
    strResult        = ""
    def __init__(self):
        pass
    def init_start(self):
        try:
            temprotocol = A01_Fun.Get_protocol("A01_U5H09F")
            if temprotocol is not None:
                temIndex = 0
                red = None
                temmonitor_data_model = A01_Fun.Get_monitor_sec()
                temmonitor_flag_model = A01_Fun.Get_monitor_flag_sec()
                for temt_modbus_addr in A01_Para.ht_t_modbus_addr:                
                    if temt_modbus_addr.attprotocol_id == temprotocol.attmain_id:
                        data_val = ""  
                        RFlag = False  
                        if red is None : 
                            temA01modbus = ComModBus()
                            comPort = A01_Fun.Get_portcom(temprotocol.attmain_id)
                            temByt = temA01modbus.get_data_com_hex(comPort,"010400010004A00900")
                            temStr = ""
                            n = 0
                            for i in temByt:
                                temStr = temStr + " "+str(int(temByt[n]))
                                n = n + 1
                            red = A01_Fun.Get16BytFloat(temByt)
                            if RFlag == False:
                                if len(red) > 2*temIndex+1:   
                                    n = (int)(red[2*temIndex+0])
                                    m = (int)(red[2*temIndex+1])
                                    data_val = str(temA01modbus.ReadFloat(n,m,False))
                                elif len(red) > 2*temIndex:
                                    n = (int)(red[2*temIndex+0])
                                    m = 0
                                    data_val = str(temA01modbus.ReadFloat(n,m,False))
                            else:
                                data_val = red[temIndex]
                        else:
                            if RFlag == False:
                                if len(red) > 2*temIndex+1:   
                                    n = (int)(red[2*temIndex+0])
                                    m = (int)(red[2*temIndex+1])
                                    data_val = str(temA01modbus.ReadFloat(n,m,False))
                                elif len(red) > 2*temIndex:
                                    n = (int)(red[2*temIndex+0])
                                    m = 0
                                    data_val = str(temA01modbus.ReadFloat(n,m,False))
                            else:
                                data_val = red[temIndex]                                
                        temIndex = temIndex + 1   
                        temcode_element = A01_Fun.Get_element_obj(temt_modbus_addr.attelement_id)
                        if hasattr(temmonitor_data_model, temcode_element.attelement_val.lower()):
                            setattr(temmonitor_data_model, temcode_element.attelement_val.lower(),data_val)                        
                        if hasattr(temmonitor_flag_model, temcode_element.attelement_val.lower()):
                            setattr(temmonitor_flag_model, temcode_element.attelement_val.lower(),"N")                      
                if A01_Para.attLock.acquire():
                    Com_Fun.SetHashTable(A01_Para.ht_monitor_sec, temmonitor_data_model.get_time, temmonitor_data_model)
                    Com_Fun.SetHashTable(A01_Para.ht_monitor_flag_sec, temmonitor_flag_model.get_time, temmonitor_flag_model)
                    A01_Para.attLock.release()                     
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            pass