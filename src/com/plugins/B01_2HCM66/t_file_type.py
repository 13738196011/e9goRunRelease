#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_file_type(object):
    attmain_id = -1
    atttype_name = ""
    attcol_length = 40
    attrow_length = 100
    attcreate_date = None
    attis_ava = 1
    atts_desc = ""
    lentype_name = 500
    lens_desc = 2000
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S") 
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["type_name"] = "附件类别名称"
        temHt["col_length"] = "最大列"  
        temHt["row_length"] = "最大行"      
        temHt["create_date"] = "系统时间"
        temHt["is_ava"] = "是否启用 "
        temHt["s_desc"] = "系统备注" 
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["type_name"] = "类别名称"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["is_ava"] = [{"main_id":"1","cn_name":"启用"},{"main_id":"0","cn_name":"禁用"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn