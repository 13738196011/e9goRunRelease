#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''
import json,os,importlib
from com.zxy.common import Com_Para
from com.zxy.db_Self.Db_Common_Self import Db_Common_Self
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.plugins.usereflect.A01_STATICDB2_IN import A01_STATICDB2_IN
from com.plugins.usereflect.A01_STATICDB3_IN import A01_STATICDB3_IN
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.z_debug import z_debug
class A01_STATICDBI_IN(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = 1
    def __init__(self):
        pass
    def init_start(self):
        try:
            if self.param_value10 is None or self.param_value10 == "0":
                opc = Ope_DB_Cent()
                opc.Create_Table(self.param_value1,self.param_value2,self.param_value3,self.param_value4)   
                self.Create_Web_Html()
            else:
                a01_st2 = A01_STATICDB2_IN()
                a01_st2.param_name = "A01_STATICDB2"
                a01_st2.param_value1 = self.param_value1
                a01_st2.param_value2 = self.param_value2
                a01_st2.param_value3 = self.param_value3
                a01_st2.param_value4 = self.param_value4
                a01_st2.param_value10 = self.param_value10
                a01_st2.init_start()
                a01_st3 = A01_STATICDB3_IN()
                a01_st3.param_name = "A01_STATICDB3"
                a01_st3.param_value1 = self.param_value1
                a01_st3.param_value2 = self.param_value2
                a01_st3.param_value3 = self.param_value3
                a01_st3.param_value4 = self.param_value4
                a01_st3.param_value10 = self.param_value10
                a01_st3.init_start()
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+repr(e)
            else:
                temLog = self.debug_info()+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        self.strContinue = 0
    def Create_Web_Html(self):
        temAryCP = self.param_value2.split(".")
        if self.param_value2 != "" and len(temAryCP) >= 1:
            temTABLE_NAME = temAryCP[len(temAryCP) - 1]
            templatePath = Com_Para.ApplicationPath +Com_Para.zxyPath+ "web"+Com_Para.zxyPath+ "template"
            tempTargetPath = Com_Para.ApplicationPath +Com_Para.zxyPath+ "web"+Com_Para.zxyPath+ "biz_target"
            if os.path.exists(tempTargetPath) == False:
                os.mkdir(tempTargetPath)
            self.WriteBizHtml(templatePath,tempTargetPath,temTABLE_NAME)
            self.WriteBizJs(templatePath,tempTargetPath,temTABLE_NAME)
    def WriteBizJs(self,templatePath,tempTargetPath,temTABLE_NAME):
        readFile = None
        writeFile = None            
        try: 
            if os.path.exists(templatePath+Com_Para.zxyPath+"template_table.js") == False :
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"template_table.js模板文件不存在"+ "\"}]}"
                return ""
            else:
                readFile = open(templatePath+Com_Para.zxyPath+"template_table.js", 'r',encoding=Com_Para.U_CODE)
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+".js") == False :
                writeFile = open(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+".js", 'a',encoding=Com_Para.U_CODE)
            else:
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\""+temTABLE_NAME+".js文件已存在"+ "\"}]}"
                return ""
            temAtts = []
            temLen = {}
            temAttCN = {}
            objC = importlib.import_module(self.param_value2)  
            objN = getattr(objC,temTABLE_NAME)
            for att in objN.__dict__:
                if str(att).find("att") == 0:
                    temAtts.append(str(att)[3:len(str(att))])
                elif str(att).find("len") == 0:
                    temLen[str(att)[3:len(str(att))]] = str(getattr(objN,att))        
            fun_us = getattr(objN,"Get_Att_CN")    #利用反射调用对象中的属性func_name
            temAttCN = fun_us(objN)
            while True and readFile is not None and writeFile is not None:
                temLines = readFile.readline()
                if self.param_value3 != "":
                    temLines = temLines.replace("{template_table}",self.param_value3)
                temLines = temLines.replace("template_table",temTABLE_NAME)
                writeFile.write(temLines)
                if temLines.find("*biz begin*") != -1:
                    pass
                elif temLines.find("*biz step begin*") != -1:
                    pass
                elif temLines.find("*table column begin*") != -1:
                    writeFile.write(self.AppendColumn(temAtts,temLen,temAttCN,objN))
                    pass
                elif temLines.find("*datetime format begin*") != -1:
                    writeFile.write(self.AppendDateTimeFm(temAtts,temLen,temAttCN,objN))
                    pass
                elif temLines.find("*insert param begin*") != -1:
                    writeFile.write(self.AppendInsertParam(temAtts,temLen,temAttCN,objN))
                    pass
                elif temLines.find("*update param begin*") != -1:
                    writeFile.write(self.AppendUpdateParam(temAtts,temLen,temAttCN,objN))
                    pass
                elif temLines.find("*delete param begin*") != -1:
                    writeFile.write(self.AppendDeleteParam(temAtts,temLen,temAttCN,objN))
                    pass
                if not temLines:
                    break
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if readFile != None:
                readFile.close()
            if writeFile != None:
                writeFile.close()
    def AppendDeleteParam(self,temAtts,temLen,temAttCN,objN):
        temResult = "            ,param_value1:rowMAIN_ID\r"
        return temResult
    def AppendUpdateParam(self,temAtts,temLen,temAttCN,objN):
        temResult = ""
        try:
            if len(temAtts) > 0:
                temIndex = 0
                for temColumn in temAtts:
                    if temColumn.upper() == "MAIN_ID":   
                        pass
                    else:
                        temIndex = temIndex + 1
                        temResult = temResult + "            ,param_value"+str(temIndex)+":$(\"#"+temColumn.upper()+"\").val()\r"
                temIndex = temIndex + 1
                temResult = temResult + "            ,param_value"+str(temIndex)+":rowMAIN_ID\r"
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,self.param_value2+"==>"+repr(e))#打印异常信息
            else:
                self.debug_in(self.param_value2+"==>"+repr(e))#打印异常信息
        return temResult
    def AppendInsertParam(self,temAtts,temLen,temAttCN,objN):
        temResult = ""
        try:
            if len(temAtts) > 0:
                temIndex = 0
                for temColumn in temAtts:
                    if temColumn.upper() == "MAIN_ID":   
                        pass
                    else:
                        temIndex = temIndex + 1
                        temResult = temResult + "            ,param_value"+str(temIndex)+":$(\"#"+temColumn.upper()+"\").val()\r"
        except Exception as e:            
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,self.param_value2+repr(e))#打印异常信息
            else:
                self.debug_in(self.param_value2+repr(e))#打印异常信息
        return temResult
    def AppendDateTimeFm(self,temAtts,temLen,temAttCN,objN):
        temResult = ""
        try:
            if len(temAtts) > 0:
                for temColumn in temAtts:
                    if temColumn.upper() == "MAIN_ID":   
                        pass
                    elif getattr(objN,"att"+temColumn) is None:
                        temResult = temResult + "                    laydate.render({\r"
                        temResult = temResult + "                        elem: '#"+temColumn.upper()+"',\r"
                        temResult = temResult + "                        type: 'datetime',\r"
                        temResult = temResult + "                    });\r"
                    elif isinstance(getattr(objN,"att"+temColumn),int):   
                        pass
                    elif isinstance(getattr(objN,"att"+temColumn),float):     
                        pass
                    elif isinstance(getattr(objN,"att"+temColumn),str):   
                        pass
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,self.param_value2+repr(e))#打印异常信息
            else:
                self.debug_in(self.param_value2+repr(e))#打印异常信息
        return temResult
    def AppendColumn(self,temAtts,temLen,temAttCN,objN):
        temResult = ""
        try:                
            if len(temAtts) > 0:
                temIndex = 0
                for temColumn in temAtts:
                    if temIndex > 0:
                        temResult = temResult + "    ,"
                    temIndex = temIndex + 1
                    if temColumn.upper() == "MAIN_ID":                        
                        temResult = temResult + "    {\r"
                        temResult = temResult + "        label: '"+temAttCN[temColumn]+"',\r"
                        temResult = temResult + "        name: '"+temColumn.upper()+"',\r"
                        temResult = temResult + "        width: '30px',\r"
                        temResult = temResult + "        index: '"+temColumn.upper()+"',\r"
                        temResult = temResult + "        editable: false,\r"
                        temResult = temResult + "        key: true,\r"
                        temResult = temResult + "        readOnly: true,\r"
                        temResult = temResult + "        editrules: {required: true}\r"
                        temResult = temResult + "    }\r"
                    elif temColumn.upper() == "IS_AVA":                        
                        temResult = temResult + "{\r"
                        temResult = temResult + "        label: '"+temAttCN[temColumn]+"',\r"
                        temResult = temResult + "        name: '"+temColumn.upper()+"',\r"
                        temResult = temResult + "        width: '40px',\r"
                        temResult = temResult + "        editable: true,\r"
                        temResult = temResult + "        editrules: true,\r"
                        temResult = temResult + "        edittype: 'select',\r"
                        temResult = temResult + "        formatter: 'select',\r"
                        temResult = temResult + "        editoptions: {\r"
                        temResult = temResult + "            value: {1: '是',0: '否'},\r"
                        temResult = temResult + "            defaultValue: '1'\r"
                        temResult = temResult + "        },\r"                        
                        temResult = temResult + "        formatoptions: {\r"
                        temResult = temResult + "            value: {1: '是',0: '否'}\r"
                        temResult = temResult + "        }\r"       
                        temResult = temResult + "    }\r"
                    elif getattr(objN,"att"+temColumn) is None:
                        temResult = temResult + "{\r"
                        temResult = temResult + "        label: '"+temAttCN[temColumn]+"',\r"
                        temResult = temResult + "        name: '"+temColumn.upper()+"',\r"
                        temResult = temResult + "        width: '70px',\r"
                        temResult = temResult + "        editable: true,\r"
                        temResult = temResult + "        editrules: true,\r"
                        temResult = temResult + "        readOnly: true,\r"
                        temResult = temResult + "        formatter: function (value, row) {return new Date(value).Format('yyyy-MM-dd hh:mm:ss');},\r"
                        temResult = temResult + "        editoptions: {defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')}\r"
                        temResult = temResult + "    }\r"
                    elif isinstance(getattr(objN,"att"+temColumn),int):
                        temResult = temResult + "{\r"
                        temResult = temResult + "        label: '"+temAttCN[temColumn]+"',\r"
                        temResult = temResult + "        name: '"+temColumn.upper()+"',\r"
                        temResult = temResult + "        width: '40px',\r"
                        temResult = temResult + "        editable: true,\r"
                        temResult = temResult + "        editrules: true,\r"
                        temResult = temResult + "        editoptions: {defaultValue: '1'}\r"
                        temResult = temResult + "    }\r"
                    elif isinstance(getattr(objN,"att"+temColumn),float):                        
                        temResult = temResult + "{\r"
                        temResult = temResult + "        label: '"+temAttCN[temColumn]+"',\r"
                        temResult = temResult + "        name: '"+temColumn.upper()+"',\r"
                        temResult = temResult + "        width: '40px',\r"
                        temResult = temResult + "        editable: true,\r"
                        temResult = temResult + "        editrules: true,\r"
                        temResult = temResult + "        editoptions: {defaultValue: '0'}\r"
                        temResult = temResult + "    }\r"
                    elif isinstance(getattr(objN,"att"+temColumn),str):
                        temResult = temResult + "{\r"
                        temResult = temResult + "        label: '"+temAttCN[temColumn]+"',\r"
                        temResult = temResult + "        name: '"+temColumn.upper()+"',\r"
                        temResult = temResult + "        width: '70px',\r"
                        temResult = temResult + "        editable: true,\r"
                        temResult = temResult + "        editrules: true\r"
                        temResult = temResult + "    }\r"
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,self.param_value2+repr(e))#打印异常信息
            else:
                self.debug_in(self.param_value2+repr(e))#打印异常信息
        return temResult
    def WriteBizHtml(self,templatePath,tempTargetPath,temTABLE_NAME):
        readFile = None
        writeFile = None            
        try: 
            if os.path.exists(templatePath+Com_Para.zxyPath+"template_table.html") == False :
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"template_table.html模板文件不存在"+ "\"}]}"
                return ""
            else:
                readFile = open(templatePath+Com_Para.zxyPath+"template_table.html", 'r',encoding=Com_Para.U_CODE)
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+".html") == False :
                writeFile = open(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+".html", 'a',encoding=Com_Para.U_CODE)
            else:
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\""+temTABLE_NAME+".html文件已存在"+ "\"}]}"
                return ""
            while True and readFile is not None and writeFile is not None:
                temLines = readFile.readline()
                if self.param_value3 != "":
                    temLines = temLines.replace("{template_table}",self.param_value3)
                temLines = temLines.replace("template_table",temTABLE_NAME)
                writeFile.write(temLines)
                if not temLines:
                    break
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if readFile != None:
                readFile.close()
            if writeFile != None:
                writeFile.close()