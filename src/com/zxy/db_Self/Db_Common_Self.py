#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.db_Self.DBManager_Self import DBManager_Self
from com.zxy.z_debug import z_debug
class Db_Common_Self(z_debug):    
    attSqlException = ""
    attRs_out = None
    attConn_a = None
    attColumnNames = []
    def __init__(self):
        pass
    def Common_SqlNoCommit(self, inputStrSql):
        temRs = None
        try:
            temDs = DBManager_Self()
            self.attConn_a = temDs.getConnection()
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock1.acquire()
            temRs = self.attConn_a.executeQueryNoCommit(inputStrSql)         
        except Exception as e:            
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+inputStrSql+"==>"+repr(e)
                self.debug_in(self,inputStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+inputStrSql+"==>"+repr(e)
                self.debug_in(inputStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        finally:
            temDs.returnConnection(self.attConn_a.attConnection)
            self.Close_Conn()
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock1.release()
        return temRs
    def Common_Sql(self, inputStrSql):
        temRs = None
        try:
            temDs = DBManager_Self()
            self.attConn_a = temDs.getConnection()
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock1.acquire()
            temRs = self.attConn_a.executeQuery(inputStrSql) 
            self.attColumnNames = self.attConn_a.attColumnNames          
        except Exception as e:        
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+inputStrSql+"==>"+repr(e)
                self.debug_in(self,inputStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+inputStrSql+"==>"+repr(e)
                self.debug_in(inputStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        finally:
            temDs.returnConnection(self.attConn_a.attConnection)
            self.Close_Conn()
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock1.release()
        return temRs
    def CommonExec_SqlRowID(self, inputStrSql):
        temIResult = -1
        try:
            temDs = DBManager_Self()
            self.attConn_a = temDs.getConnection()
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock1.acquire()
            temIResult = self.attConn_a.executeUpdateRowID(inputStrSql)         
        except Exception as e:        
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+inputStrSql+"==>"+repr(e)
                self.debug_in(self,inputStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+inputStrSql+"==>"+repr(e)
                self.debug_in(inputStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
            temIResult = -1
        finally:
            temDs.returnConnection(self.attConn_a.attConnection)
            self.Close_Conn()
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock1.release()
        return temIResult
    def CommonExec_Sql(self, inputStrSql):
        temIResult = -1
        try:
            temDs = DBManager_Self()
            self.attConn_a = temDs.getConnection()
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock1.acquire()
            temIResult = self.attConn_a.executeUpdate(inputStrSql)         
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+inputStrSql+"==>"+repr(e)
                self.debug_in(self,inputStrSql+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+inputStrSql+"==>"+repr(e)
                self.debug_in(inputStrSql+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
            temIResult = -1
        finally:
            temDs.returnConnection(self.attConn_a.attConnection)
            self.Close_Conn()
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock1.release()
        return temIResult    
    def Common_Sql_Proc(self,inputProName, inputParameters, inputParamTypes, inputParamOutName, inputParamOutType, inputTrn):
        try:    
            temDs = DBManager_Self()
            self.attConn_a = temDs.getConnection()        
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock1.acquire()
            if inputTrn.attINF_TYPE == "1":
                self.attRs_out = self.attConn_a.ParamExecuteQuery(inputProName,inputParameters,inputParamTypes,inputParamOutName,inputParamOutType)
            else:
                self.attRs_out = self.attConn_a.ParamExecuteQuery(inputProName,inputParameters,inputParamTypes,inputParamOutName,inputParamOutType,inputTrn.attINF_EN_SQL)
            self.attColumnNames = self.attConn_a.attColumnNames
        except Exception as e:
            self.attSqlException = "数据库操作出错请查看程序错误日志文件:" + inputProName + " "+ repr(e)        
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+inputProName+"==>"+repr(e)
                self.debug_in(self,inputProName+"==>"+repr(e))#打印异常信息
            else:
                temLog = self.debug_info()+inputProName+"==>"+repr(e)
                self.debug_in(inputProName+"==>"+repr(e))#打印异常信息
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        finally:
            temDs.returnConnection(self.attConn_a.attConnection)
            self.Close_Conn()
            if str(self.attConn_a.attConnection).find("sqlite3.Connection") != -1:
                Com_Para.Dblock1.release()
        return self.attRs_out
    def Close_Conn(self):
        if not self.attConn_a.attConnection is None:
            pass
