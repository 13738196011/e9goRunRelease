//按钮事件新增或编辑
var BA1D24_type = "";
//其他页面传到本页面参数
var BA1D24_param = {};
//暂时没用
var BA1D24_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BA1D24_ary_SUB_ID = null;var BA1D24_ary_PROC_ID = null;var BA1D24_ary_LIMIT_TAG = [{'MAIN_ID': '1', 'CN_NAME': '年'}, {'MAIN_ID': '2', 'CN_NAME': '月'}, {'MAIN_ID': '3', 'CN_NAME': '日'}];
/*declare select options end*/

//主从表传递参数
function BA1D24_param_set(){
	/*Main Subsuv Table Param Begin*/
    if(BA1D24_param.hasOwnProperty("PROC_ID_cn_name"))        $("#BA1D24_find_PROC_ID_cn_name").val(s_decode(BA1D24_param["PROC_ID_cn_name"]));    if(BA1D24_param.hasOwnProperty("PROC_ID"))        $("#BA1D24_find_PROC_ID").val(BA1D24_param["PROC_ID"]);    if(BA1D24_param.hasOwnProperty("hidden_find")){        $("#BA1D24_Ope_PROC_ID").hide();        $("#BA1D24_Clear_PROC_ID").hide();    }	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function BA1D24_t_sub_power_biz_start(inputdata) {
	BA1D24_param = inputdata;
	layer.close(ly_index);
	BA1D24_param_set();
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_sub_power$SUB_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BA1D24_get_N01_t_sub_power$SUB_ID");	
    /*biz end*/
}

/*biz step begin*/
function BA1D24_format_SUB_ID(value, row, index) {    var objResult = value;    for(i = 0; i < BA1D24_ary_SUB_ID.length; i++) {        var obj = BA1D24_ary_SUB_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BA1D24_format_PROC_ID(value, row, index) {    var objResult = value;    for(i = 0; i < BA1D24_ary_PROC_ID.length; i++) {        var obj = BA1D24_ary_PROC_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BA1D24_format_LIMIT_TAG(value, row, index) {    var objResult = value;    for(i = 0; i < BA1D24_ary_LIMIT_TAG.length; i++) {        var obj = BA1D24_ary_LIMIT_TAG[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BA1D24_get_N01_t_sub_power$SUB_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_sub_power$SUB_ID) == false)        return false;    BA1D24_ary_SUB_ID = input.N01_t_sub_power$SUB_ID;    if($("#BA1D24_SUB_ID").is("select") && $("#BA1D24_SUB_ID")[0].options.length == 0)    {        $.each(BA1D24_ary_SUB_ID, function (i, obj) {            addOptionValue("BA1D24_SUB_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    var inputdata = {        "param_name": "N01_t_proc_inparam$PROC_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BA1D24_get_N01_t_proc_inparam$PROC_ID");}function BA1D24_get_N01_t_proc_inparam$PROC_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_inparam$PROC_ID) == false)        return false;    BA1D24_ary_PROC_ID = input.N01_t_proc_inparam$PROC_ID;    if($("#BA1D24_PROC_ID").is("select") && $("#BA1D24_PROC_ID")[0].options.length == 0)    {        $.each(BA1D24_ary_PROC_ID, function (i, obj) {            addOptionValue("BA1D24_PROC_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#BA1D24_LIMIT_TAG").is("select") && $("#BA1D24_LIMIT_TAG")[0].options.length == 0)    {        $.each(BA1D24_ary_LIMIT_TAG, function (i, obj) {            addOptionValue("BA1D24_LIMIT_TAG", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    BA1D24_init_t_sub_power();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function BA1D24_PROC_ID_cn_name_fun(){    index_subhtml = "t_proc_name.vue"    random_subhtml = "";    if(loadHtmlSubVueFun("dev_vue/t_proc_name.vue","BA1D24_t_sub_power_call_vue") == true){        var n = Get_RandomDiv("","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#_t_proc_name_Events').bootstrapTable('resetView');                _param["ly_index"] = index;                _param["target_name"] = "BA1D24_find_PROC_ID_cn_name"                _param["target_id"] = "BA1D24_PROC_ID"                _param["sourc_id"] = "MAIN_ID"                _param["sourc_name"] = "INF_CN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function BA1D24_page_end(){
	if(BA1D24_param["type"] == "edit"){
		BA1D24_get_edit_info();
	}
}

//页面初始化方法
function BA1D24_init_t_sub_power() {
	//type = getUrlParam("type");
	if(BA1D24_param["type"] == "add"){
		$("#BA1D24_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BA1D24_SUB_ID").val(BA1D24_param["SUB_ID"]);        $("#BA1D24_PROC_ID").val(BA1D24_param["PROC_ID"]);        $("#BA1D24_find_PROC_ID_cn_name").val(BA1D24_param["PROC_ID_cn_name"]);		
		/*Get Find Select param end*/	
	}
	else if(BA1D24_param["type"] == "edit"){
	
	}
	
	//表单验证
	BA1D24_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BA1D24_LIMIT_DATE").val() == "")    {        $("#BA1D24_LIMIT_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BA1D24_LIMIT_DATE',        type: 'datetime',        trigger: 'click'    });    if($("#BA1D24_FIRST_DATE").val() == "")    {        $("#BA1D24_FIRST_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BA1D24_FIRST_DATE',        type: 'datetime',        trigger: 'click'    });    if($("#BA1D24_CREATE_DATE").val() == "")    {        $("#BA1D24_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BA1D24_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BA1D24_page_end();
}

//提交表单数据
function BA1D24_SubmitForm(){
	if(BA1D24_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_sub_power",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#BA1D24_SUB_ID").val()                ,"param_value2": $("#BA1D24_PROC_ID").val()                ,"param_value3": $("#BA1D24_LIMIT_DATE").val()                ,"param_value4": $("#BA1D24_USE_LIMIT").val()                ,"param_value5": $("#BA1D24_LIMIT_NUMBER").val()                ,"param_value6": s_encode($("#BA1D24_LIMIT_TAG").val())                ,"param_value7": $("#BA1D24_FIRST_DATE").val()                ,"param_value8": s_encode($("#BA1D24_S_DESC").val())                ,"param_value9": $("#BA1D24_CREATE_DATE").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BA1D24_get_N01_ins_t_sub_power");
	}
	else if(BA1D24_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_sub_power",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#BA1D24_SUB_ID").val()                ,"param_value2": $("#BA1D24_PROC_ID").val()                ,"param_value3": $("#BA1D24_LIMIT_DATE").val()                ,"param_value4": $("#BA1D24_USE_LIMIT").val()                ,"param_value5": $("#BA1D24_LIMIT_NUMBER").val()                ,"param_value6": s_encode($("#BA1D24_LIMIT_TAG").val())                ,"param_value7": $("#BA1D24_FIRST_DATE").val()                ,"param_value8": s_encode($("#BA1D24_S_DESC").val())                ,"param_value9": $("#BA1D24_CREATE_DATE").val()                ,"param_value10": $("#BA1D24_MAIN_ID").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BA1D24_get_N01_upd_t_sub_power");
	}
}

//vue回调
function BA1D24_t_sub_power_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "t_proc_name.vue"){        var n = Get_RandomDiv("",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":BA1D24_type,                    "ly_index":index,                    "target_name":"BA1D24_find_PROC_ID_cn_name",                    "target_id":"BA1D24_find_PROC_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"INF_CN_NAME"                };                loadScript_hasparam("dev_vue/t_proc_name.js","_t_proc_name_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
}

//for表单提交
$("#BA1D24_save_t_sub_power_Edit").click(function () {
	$("form[name='BA1D24_DataModal']").submit();
})

/*修改数据*/
function BA1D24_get_N01_upd_t_sub_power(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_sub_power) == true)
	{
		swal("修改数据成功!", "", "success");
		AA1D24_t_sub_power_query();
		BA1D24_clear_validate();
		layer.close(BA1D24_param["ly_index"]);
	}
}

/*添加数据*/
function BA1D24_get_N01_ins_t_sub_power(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_sub_power) == true)
	{
		swal("添加数据成功!", "", "success");
		AA1D24_t_sub_power_query();
		BA1D24_clear_validate();
		layer.close(BA1D24_param["ly_index"]);
	}
}

//取消编辑
$("#BA1D24_cancel_t_sub_power_Edit").click(function () {
	layer.close(BA1D24_param["ly_index"]);
	BA1D24_clear_validate();
	$("[id^='BA1D24_div']").hide();
})

//清除查找框
function BA1D24_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BA1D24_clear_validate(){
	$("#BA1D24_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BA1D24_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BA1D24_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BA1D24_clear_edit_info(){
	var inputs = $("#BA1D24_DataModal").find('input');
	var selects = $("#BA1D24_DataModal").find("select");
	var textareas = $("#BA1D24_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
    BA1D24_clear_input_cn_name('BA1D24_find_PROC_ID_cn_name','BA1D24_PROC_ID')	
	/*input validate clear end*/
	BA1D24_init_t_sub_power();
}

//页面输入框赋值
function BA1D24_get_edit_info(){
	var rowData = $("#AA1D24_t_sub_power_Events").bootstrapTable('getData')[AA1D24_select_t_sub_power_rowId];
	var inputs = $("#BA1D24_DataModal").find('input');
	var selects = $("#BA1D24_DataModal").find("select");
	var textareas = $("#BA1D24_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"AA1D24","BA1D24");
}

//form验证
function BA1D24_checkFormInput() {
    BA1D24_validate = $("#BA1D24_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BA1D24_MAIN_ID: {}            ,BA1D24_SUB_ID: {}            ,BA1D24_PROC_ID: {}            ,BA1D24_LIMIT_DATE: {date: true,required : true,maxlength:19}            ,BA1D24_USE_LIMIT: {digits: true,required : true,maxlength:10}            ,BA1D24_LIMIT_NUMBER: {digits: true,required : true,maxlength:10}            ,BA1D24_LIMIT_TAG: {}            ,BA1D24_FIRST_DATE: {date: true,required : true,maxlength:19}            ,BA1D24_S_DESC: {}            ,BA1D24_CREATE_DATE: {date: true,required : true,maxlength:19}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BA1D24_MAIN_ID: {}            ,BA1D24_SUB_ID: {}            ,BA1D24_PROC_ID: {}            ,BA1D24_LIMIT_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BA1D24_USE_LIMIT: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,BA1D24_LIMIT_NUMBER: {digits: "必须输入整数",required : "必须输入整数",maxlength:"长度不能超过10" }            ,BA1D24_LIMIT_TAG: {}            ,BA1D24_FIRST_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BA1D24_S_DESC: {}            ,BA1D24_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BA1D24_SubmitForm();
        	return false;
        }
    })
}