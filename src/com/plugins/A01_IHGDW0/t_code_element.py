#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_code_element(object):
    attmain_id = -1
    attelement_code = ""
    attelement_name = ""
    attelement_val = ""
    attelement_typ = -1
    attelement_unit = ""
    attelement_state = 1
    attcreate_date = None
    atts_desc = ""
    attdot_num = ""
    attup_level = ""
    attdown_level = ""
    attup_limit = ""
    attdown_limit = ""
    attelement_code2 = ""
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")        