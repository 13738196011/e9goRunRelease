#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class v_selproc(object):
    attmain_id = -1
    attINF_CN_NAME = ""
    attINF_EN_NAME = ""
    attIS_AUTHORITY = 0
    lenINF_CN_NAME = 50
    lenINF_EN_NAME = 50
    def __init__(self):
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["INF_CN_NAME"] = "接口中文名"
        temHt["INF_EN_NAME"] = "接口英文名"
        temHt["IS_AUTHORITY"] = "是否权限认证"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["INF_CN_NAME"] = "接口中文名"
        temHtQry["INF_EN_NAME"] = "接口英文名"
        temHtQry["IS_AUTHORITY"] = "是否权限认证"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["IS_AUTHORITY"] = [{"MAIN_ID":"1","CN_NAME":"是"},{"MAIN_ID":"0","CN_NAME":"否"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
