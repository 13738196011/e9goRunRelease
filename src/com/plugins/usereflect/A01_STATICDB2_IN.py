#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''
import json,os,importlib
from com.zxy.common import Com_Para
from com.zxy.db_Self.Db_Common_Self import Db_Common_Self
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.business.Ope_DB_Cent import Ope_DB_Cent
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.z_debug import z_debug
class A01_STATICDB2_IN(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = 1
    temAtts = []
    temLen = {}
    temAttCN = {}
    temQryAtt = {}
    temSelectAtt = {}
    temFindAtt = {}
    temFReturnAtt = {}
    objN = None
    def __init__(self):
        self.temAtts = []
        self.temLen = {}
        self.temAttCN = {}
        self.temQryAtt = {}
        self.temSelectAtt = {}
        self.temFindAtt = {}
        self.temFReturnAtt = {}
        self.objN = None
    def init_start(self):
        try:
            opc = Ope_DB_Cent()
            opc.Create_Table(self.param_value1,self.param_value2,self.param_value3,self.param_value4)            
            opc.Create_StaticView_Interface(self.param_value1,self.param_value2,self.param_value3,self.param_value4,"T01")
            self.Create_Web_Html_bootstrap()
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+repr(e)
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
                temLog = self.debug_info()+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        self.strContinue = 0
    def Create_Web_Html_bootstrap(self):
        temAryCP = self.param_value2.split(".")
        if self.param_value2 != "" and len(temAryCP) >= 1:
            temTABLE_NAME = temAryCP[len(temAryCP) - 1]
            templatePath = Com_Para.ApplicationPath +Com_Para.zxyPath+ "web"+Com_Para.zxyPath+ "template"
            tempTargetPath = Com_Para.ApplicationPath +Com_Para.zxyPath+ "web"+Com_Para.zxyPath+ "biz_target"
            if os.path.exists(tempTargetPath) == False:
                os.mkdir(tempTargetPath)
            self.init_att(temTABLE_NAME)                
            self.WriteBizHtml_1(templatePath,tempTargetPath,temTABLE_NAME)
            self.WriteBizHtml_2(templatePath,tempTargetPath,temTABLE_NAME)
            self.WriteBizJs_1(templatePath,tempTargetPath,temTABLE_NAME)
            self.WriteBizJs_2(templatePath,tempTargetPath,temTABLE_NAME)
    def init_att(self,temTABLE_NAME):        
        objC = importlib.import_module(self.param_value2)  
        self.objN = getattr(objC,temTABLE_NAME)
        for att in self.objN.__dict__:
            if str(att).find("att") == 0:
                self.temAtts.append(str(att)[3:len(str(att))])
            elif str(att).find("len") == 0:
                self.temLen[str(att)[3:len(str(att))]] = str(getattr(self.objN,att))        
        fun_us = getattr(self.objN,"Get_Att_CN")    #利用反射调用对象中的属性func_name
        self.temAttCN = fun_us(self.objN)        
        fun_us = getattr(self.objN,"Qry_Att_CN")
        self.temQryAtt = fun_us(self.objN)    
        fun_us = getattr(self.objN,"Select_Att_CN")
        self.temSelectAtt = fun_us(self.objN)    
        fun_us = getattr(self.objN,"Find_Att_CN")
        self.temFindAtt = fun_us(self.objN)
        fun_us = getattr(self.objN,"FReturn_Att_CN")
        self.temFReturnAtt = fun_us(self.objN)
    def WriteBizJs_2(self,templatePath,tempTargetPath,temTABLE_NAME):
        readFile = None
        writeFile = None            
        try: 
            if os.path.exists(templatePath+Com_Para.zxyPath+"template_table2_$.js") == False :
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"template_table2_$.js模板文件不存在"+ "\"}]}"
                return ""
            else:
                readFile = open(templatePath+Com_Para.zxyPath+"template_table2_$.js", 'r',encoding=Com_Para.U_CODE)
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+"_$.js") == False :
                writeFile = open(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+"_$.js", 'a',encoding=Com_Para.U_CODE)
            else:
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\""+temTABLE_NAME+"_$.js文件已存在"+ "\"}]}"
                return ""
            while True and readFile is not None and writeFile is not None:
                temLines = readFile.readline()
                if self.param_value3 != "":
                    temLines = temLines.replace("{template_table_CN}",self.param_value3)
                temLines = temLines.replace("{template_table_EN}",temTABLE_NAME)
                writeFile.write(temLines)                
                if temLines.find("*declare select options begin*") != -1:
                    for key in list(self.temSelectAtt.keys()):
                        temValue = Com_Fun.GetHashTable(self.temSelectAtt,key)
                        if isinstance(temValue,str):
                            writeFile.write("\rvar ary_"+key+" = null;")
                        elif isinstance(temValue,list):
                            writeFile.write("\rvar ary_"+key+" = "+str(temValue)+";")
                elif temLines.find("*biz begin*") != -1:
                    if len(self.temSelectAtt) == 0:                  
                        writeFile.write("\r    init_"+temTABLE_NAME+"()")
                    else:
                        bAjax = False
                        for key in list(self.temSelectAtt.keys()):
                            if isinstance(self.temSelectAtt[key],str) and self.temSelectAtt[key].find("select ") == 0:
                                writeFile.write("\r    var inputdata = {")
                                writeFile.write("\r        \"param_name\": \"T01_"+temTABLE_NAME+"$"+key+"\",")
                                writeFile.write("\r        \"session_id\": session_id,")
                                writeFile.write("\r        \"login_id\": login_id")
                                writeFile.write("\r    };")
                                writeFile.write("\r    ly_index = layer.load();")
                                writeFile.write("\r    get_ajax_baseurl(inputdata, \"get_T01_"+temTABLE_NAME+"$"+key+"\");")                                                          
                                bAjax = True
                                break
                            elif isinstance(self.temSelectAtt[key],str) and self.temSelectAtt[key].find("select ") == -1:
                                writeFile.write("\r    var inputdata = {")
                                writeFile.write("\r        \"param_name\": \""+self.temSelectAtt[key]+"\",")
                                writeFile.write("\r        \"session_id\": session_id,")
                                writeFile.write("\r        \"login_id\": login_id")
                                writeFile.write("\r    };")
                                writeFile.write("\r    ly_index = layer.load();")
                                writeFile.write("\r    get_ajax_baseurl(inputdata, \"get_"+self.temSelectAtt[key]+"\");")                                                          
                                bAjax = True
                                break
                        if bAjax == False:
                            for key in list(self.temSelectAtt.keys()): 
                                if isinstance(self.temSelectAtt[key],list): 
                                    writeFile.write("\r    $.each(ary_"+key+", function (i, obj) {")
                                    writeFile.write("\r        addOptionValue(\""+key+"\", obj[GetLowUpp(\"main_id\")], obj[GetLowUpp(\"cn_name\")]);")
                                    writeFile.write("\r    });")                            
                            writeFile.write("\r    init_"+temTABLE_NAME+"()")
                elif temLines.find("*biz step begin*") != -1:
                    for key in list(self.temSelectAtt.keys()):                   
                        writeFile.write("\rfunction format_"+key+"(value, row, index) {")
                        writeFile.write("\r    var objResult = value;")
                        writeFile.write("\r    for(i = 0; i < ary_"+key+".length; i++) {")
                        writeFile.write("\r        var obj = ary_"+key+"[i];")
                        writeFile.write("\r        if (obj[GetLowUpp(\"main_id\")].toString() == value.toString()) {")
                        writeFile.write("\r            objResult = obj[GetLowUpp(\"cn_name\")];")
                        writeFile.write("\r            break;")
                        writeFile.write("\r        }")
                        writeFile.write("\r    }")
                        writeFile.write("\r    return objResult;")
                        writeFile.write("\r}")
                        writeFile.write("\r")                    
                    bFlag = False
                    iIndex = 0                         
                    for key in list(self.temSelectAtt.keys()):   
                        if isinstance(self.temSelectAtt[key],str)  and self.temSelectAtt[key].find("select ") == 0:
                            if bFlag == True:
                                writeFile.write("\r    var inputdata = {")
                                writeFile.write("\r        \"param_name\": \"T01_"+temTABLE_NAME+"$"+key+"\",")
                                writeFile.write("\r        \"session_id\": session_id,")
                                writeFile.write("\r        \"login_id\": login_id")
                                writeFile.write("\r    };")
                                writeFile.write("\r    ly_index = layer.load();")
                                writeFile.write("\r    get_ajax_baseurl(inputdata, \"get_T01_"+temTABLE_NAME+"$"+key+"\");")
                                writeFile.write("\r}")            
                            writeFile.write("\r")                                            
                            writeFile.write("\rfunction get_T01_"+temTABLE_NAME+"$"+key+"(input) {")
                            writeFile.write("\r    layer.close(ly_index);")
                            writeFile.write("\r    //查询失败")
                            writeFile.write("\r    if (Call_QryResult(input.T01_"+temTABLE_NAME+"$"+key+") == false)")
                            writeFile.write("\r        return false;")
                            writeFile.write("\r    ary_"+key+" = input.T01_"+temTABLE_NAME+"$"+key+";")                           
                            writeFile.write("\r    if($(\"#"+key+"\").is(\"select\") && $(\"#"+key+"\")[0].options.length == 0)")
                            writeFile.write("\r    {")
                            writeFile.write("\r        $.each(ary_"+key+", function (i, obj) {")
                            writeFile.write("\r            addOptionValue(\""+key+"\", obj[GetLowUpp(\"main_id\")], obj[GetLowUpp(\"cn_name\")]);")
                            writeFile.write("\r        });")
                            writeFile.write("\r    }")
                            iIndex = iIndex + 1
                            bFlag = True     
                        elif isinstance(self.temSelectAtt[key],str) and self.temSelectAtt[key].find("select ") == -1:
                            if bFlag == True:
                                writeFile.write("\r    var inputdata = {")
                                writeFile.write("\r        \"param_name\": \""+self.temSelectAtt[key]+"\",")
                                writeFile.write("\r        \"session_id\": session_id,")
                                writeFile.write("\r        \"login_id\": login_id")
                                writeFile.write("\r    };")
                                writeFile.write("\r    ly_index = layer.load();")
                                writeFile.write("\r    get_ajax_baseurl(inputdata, \"get_"+self.temSelectAtt[key]+"\");")
                                writeFile.write("\r}")            
                            writeFile.write("\r")                                            
                            writeFile.write("\rfunction get_"+self.temSelectAtt[key]+"(input) {")
                            writeFile.write("\r    layer.close(ly_index);")
                            writeFile.write("\r    //查询失败")
                            writeFile.write("\r    if (Call_QryResult(input."+self.temSelectAtt[key]+") == false)")
                            writeFile.write("\r        return false;")
                            writeFile.write("\r    ary_"+key+" = input."+self.temSelectAtt[key]+";")                           
                            writeFile.write("\r    if($(\"#"+key+"\").is(\"select\") && $(\"#"+key+"\")[0].options.length == 0)")
                            writeFile.write("\r    {")
                            writeFile.write("\r        $.each(ary_"+key+", function (i, obj) {")
                            writeFile.write("\r            addOptionValue(\""+key+"\", obj[GetLowUpp(\"main_id\")], obj[GetLowUpp(\"cn_name\")]);")
                            writeFile.write("\r        });")
                            writeFile.write("\r    }")
                            iIndex = iIndex + 1
                            bFlag = True                   
                    if bFlag:
                        for key in list(self.temSelectAtt.keys()): 
                            if isinstance(self.temSelectAtt[key],list): 
                                writeFile.write("\r    if($(\"#"+key+"\").is(\"select\") && $(\"#"+key+"\")[0].options.length == 0)")
                                writeFile.write("\r    {")
                                writeFile.write("\r        $.each(ary_"+key+", function (i, obj) {")
                                writeFile.write("\r            addOptionValue(\""+key+"\", obj[GetLowUpp(\"main_id\")], obj[GetLowUpp(\"cn_name\")]);")
                                writeFile.write("\r        });")
                                writeFile.write("\r    }")
                        writeFile.write("\r    init_"+temTABLE_NAME+"();")
                        writeFile.write("\r}")
                elif temLines.find("*form datetime init begin*") != -1:
                    writeFile.write(self.AppenFDIB())                
                elif temLines.find("*insert param begin*") != -1:
                    writeFile.write(self.AppendIPB())
                elif temLines.find("*update param begin*") != -1:
                    writeFile.write(self.AppendUPB())
                elif temLines.find("*input check rules begin*") != -1:
                    writeFile.write(self.AppendICRB())
                elif temLines.find("*input check messages begin*") != -1:
                    writeFile.write(self.AppendICMB())                
                elif temLines.find("*find qry fun begin*") != -1:
                    writeFile.write(self.AppenFQFB2())                       
                if not temLines:
                    break
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if readFile != None:
                readFile.close()
            if writeFile != None:
                writeFile.close()
    def AppendUPB(self):
        temResult = ""
        temIndex = 0
        for key in list(self.temAttCN.keys()):
            if key.upper() == "MAIN_ID":   
                pass
            else:
                temIndex = temIndex + 1
                if isinstance(getattr(self.objN,"att"+key),str):
                    temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": s_encode($(\"#"+key+"\").val())"
                    pass
                elif isinstance(getattr(self.objN,"att"+key),int):
                    temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": $(\"#"+key+"\").val()"
                    pass
                elif isinstance(getattr(self.objN,"att"+key),float):
                    temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": $(\"#"+key+"\").val()"
                    pass
                elif getattr(self.objN,"att"+key) is None:
                    temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": $(\"#"+key+"\").val()"
        temIndex = temIndex + 1
        temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": $(\"#main_id\").val()"
        return temResult
    def AppendICRB(self):
        temResult = ""
        temIndex = 0
        for key in list(self.temAttCN.keys()):
            if key.upper() == "MAIN_ID":   
                temResult = temResult + "\r            "+key+": {}"
                pass
            else:
                temIndex = temIndex + 1
                if isinstance(getattr(self.objN,"att"+key),str):
                    temResult = temResult + "\r            ,"+key+": {}"
                    pass
                elif isinstance(getattr(self.objN,"att"+key),int):
                    if Com_Fun.GetHashTable(self.temSelectAtt, key) != "":
                        temResult = temResult + "\r            ,"+key+": {}"
                    else:
                        temResult = temResult + "\r            ,"+key+": {digits: true,required : true,maxlength:10}"
                    pass
                elif isinstance(getattr(self.objN,"att"+key),float):
                    if Com_Fun.GetHashTable(self.temSelectAtt, key) != "":
                        temResult = temResult + "\r            ,"+key+": {}"
                    else:
                        temResult = temResult + "\r            ,"+key+": {number: true,required : true,maxlength:14}"
                    pass
                elif getattr(self.objN,"att"+key) is None:
                    temResult = temResult + "\r            ,"+key+": {date: true,required : true,maxlength:19}"
        return temResult
    def AppendICMB(self):
        temResult = ""
        temIndex = 0
        for key in list(self.temAttCN.keys()):
            if key.upper() == "MAIN_ID": 
                temResult = temResult + "\r            "+key+": {}"  
                pass
            else:
                temIndex = temIndex + 1
                if isinstance(getattr(self.objN,"att"+key),str):
                    temResult = temResult + "\r            ,"+key+": {}"
                    pass
                elif isinstance(getattr(self.objN,"att"+key),int):                    
                    if Com_Fun.GetHashTable(self.temSelectAtt, key) != "":
                        temResult = temResult + "\r            ,"+key+": {}"
                    else:
                        temResult = temResult + "\r            ,"+key+": {digits: \"必须输入整数\",required : \"必须输入整数\",maxlength:\"长度不能超过10\" }"
                    pass
                elif isinstance(getattr(self.objN,"att"+key),float):
                    if Com_Fun.GetHashTable(self.temSelectAtt, key) != "":
                        temResult = temResult + "\r            ,"+key+": {}"
                    else:
                        temResult = temResult + "\r            ,"+key+": {number: \"必须输入合法的小数\",required : \"必须输入合法的小数\",maxlength:\"长度不能超过14\"}"
                    pass
                elif getattr(self.objN,"att"+key) is None:
                    temResult = temResult + "\r            ,"+key+": {date: \"必须输入正确格式的日期\",required : \"必须输入正确格式的日期\",maxlength:\"长度不能超过19\"}"
        return temResult
    def AppendIPB(self):
        temResult = ""
        temIndex = 0
        for key in list(self.temAttCN.keys()):
            if key.upper() == "MAIN_ID":   
                pass
            else:
                temIndex = temIndex + 1
                if isinstance(getattr(self.objN,"att"+key),str):
                    temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": s_encode($(\"#"+key+"\").val())"
                    pass
                elif isinstance(getattr(self.objN,"att"+key),int):
                    temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": $(\"#"+key+"\").val()"
                    pass
                elif isinstance(getattr(self.objN,"att"+key),float):
                    temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": $(\"#"+key+"\").val()"
                    pass
                elif getattr(self.objN,"att"+key) is None:
                    temResult = temResult + "\r                ,\"param_value"+str(temIndex)+"\": $(\"#"+key+"\").val()"
        return temResult
    def AppenFDIB(self):
        temResult = ""
        for key in list(self.temAttCN.keys()):
            if isinstance(getattr(self.objN,"att"+key),str):
                pass
            elif isinstance(getattr(self.objN,"att"+key),int):
                pass
            elif isinstance(getattr(self.objN,"att"+key),float):
                pass
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\r    if($(\"#"+key+"\").val() == \"\")"
                temResult = temResult + "\r    {"
                temResult = temResult + "\r        $(\"#"+key+"\").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));"               
                temResult = temResult + "\r    }"
                temResult = temResult + "\r    laydate.render({"
                temResult = temResult + "\r        elem: '#"+key+"',"
                temResult = temResult + "\r        type: 'datetime',"
                temResult = temResult + "\r        trigger: 'click'"
                temResult = temResult + "\r    });" 
        return temResult
    def WriteBizJs_1(self,templatePath,tempTargetPath,temTABLE_NAME):
        readFile = None
        writeFile = None            
        try: 
            if os.path.exists(templatePath+Com_Para.zxyPath+"template_table2.js") == False :
                self.strResult = "{\""+ self.param_name+ "\":[{\"s_result\":\"0\",\"error_desc\":\"template_table2.js模板文件不存在"+ "\"}]}"
                return ""
            else:
                readFile = open(templatePath+Com_Para.zxyPath+"template_table2.js", 'r',encoding=Com_Para.U_CODE)
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+".js") == False :
                writeFile = open(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+".js", 'a',encoding=Com_Para.U_CODE)
            else:
                self.strResult = "{\""+ self.param_name+ "\":[{\"s_result\":\"0\",\"error_desc\":\""+temTABLE_NAME+".js文件已存在"+ "\"}]}"
                return ""
            while True and readFile is not None and writeFile is not None:
                temLines = readFile.readline()
                if self.param_value3 != "":
                    temLines = temLines.replace("{template_table_CN}",self.param_value3)
                temLines = temLines.replace("{template_table_EN}",temTABLE_NAME)
                writeFile.write(temLines)
                if temLines.find("*declare select options begin*") != -1:
                    for key in list(self.temSelectAtt.keys()):
                        temValue = Com_Fun.GetHashTable(self.temSelectAtt,key)
                        if isinstance(temValue,str):
                            writeFile.write("\rvar ary_"+key+" = null;")
                        elif isinstance(temValue,list):
                            writeFile.write("\rvar ary_"+key+" = "+str(temValue)+";")
                elif temLines.find("*biz begin*") != -1:
                    if len(self.temSelectAtt) == 0:                  
                        writeFile.write("\r    init_"+temTABLE_NAME+"()")
                    else:
                        bAjax = False
                        for key in list(self.temSelectAtt.keys()):
                            if isinstance(self.temSelectAtt[key],str) and self.temSelectAtt[key].find("select ") == 0:
                                writeFile.write("\r    var inputdata = {")
                                writeFile.write("\r        \"param_name\": \"T01_"+temTABLE_NAME+"$"+key+"\",")
                                writeFile.write("\r        \"session_id\": session_id,")
                                writeFile.write("\r        \"login_id\": login_id")
                                writeFile.write("\r    };")
                                writeFile.write("\r    ly_index = layer.load();")
                                writeFile.write("\r    get_ajax_baseurl(inputdata, \"get_T01_"+temTABLE_NAME+"$"+key+"\");")
                                bAjax = True
                                break
                            elif isinstance(self.temSelectAtt[key],str) and self.temSelectAtt[key].find("select ") == -1:
                                writeFile.write("\r    var inputdata = {")
                                writeFile.write("\r        \"param_name\": \""+self.temSelectAtt[key]+"\",")
                                writeFile.write("\r        \"session_id\": session_id,")
                                writeFile.write("\r        \"login_id\": login_id")
                                writeFile.write("\r    };")
                                writeFile.write("\r    ly_index = layer.load();")
                                writeFile.write("\r    get_ajax_baseurl(inputdata, \"get_"+self.temSelectAtt[key]+"\");")
                                bAjax = True
                                break
                        if bAjax == False:
                            for key in list(self.temSelectAtt.keys()): 
                                if isinstance(self.temSelectAtt[key],list): 
                                    writeFile.write("\r    if($(\"#qry_"+key+"\").is(\"select\") && $(\"#qry_"+key+"\")[0].options.length == 0)")
                                    writeFile.write("\r    {")
                                    writeFile.write("\r        $(\"#qry_"+key+"\").append(\"<option value='-1'></option>\")");
                                    writeFile.write("\r        $.each(ary_"+key+", function (i, obj) {")
                                    writeFile.write("\r            addOptionValue(\"qry_"+key+"\", obj[GetLowUpp(\"main_id\")], obj[GetLowUpp(\"cn_name\")]);")
                                    writeFile.write("\r        });")
                                    writeFile.write("\r    }")
                            writeFile.write("\r    init_"+temTABLE_NAME+"()")
                elif temLines.find("*biz step begin*") != -1:
                    for key in list(self.temSelectAtt.keys()):                   
                        writeFile.write("\rfunction format_"+key+"(value, row, index) {")
                        writeFile.write("\r    var objResult = value;")
                        writeFile.write("\r    for(i = 0; i < ary_"+key+".length; i++) {")
                        writeFile.write("\r        var obj = ary_"+key+"[i];")
                        writeFile.write("\r        if (obj[GetLowUpp(\"main_id\")].toString() == value.toString()) {")
                        writeFile.write("\r            objResult = obj[GetLowUpp(\"cn_name\")];")
                        writeFile.write("\r            break;")
                        writeFile.write("\r        }")
                        writeFile.write("\r    }")
                        writeFile.write("\r    return objResult;")
                        writeFile.write("\r}")
                        writeFile.write("\r")
                    bFlag = False
                    iIndex = 0                         
                    for key in list(self.temSelectAtt.keys()):   
                        if isinstance(self.temSelectAtt[key],str)  and self.temSelectAtt[key].find("select ") == 0:
                            if bFlag == True:
                                writeFile.write("\r    var inputdata = {")
                                writeFile.write("\r        \"param_name\": \"T01_"+temTABLE_NAME+"$"+key+"\",")
                                writeFile.write("\r        \"session_id\": session_id,")
                                writeFile.write("\r        \"login_id\": login_id")
                                writeFile.write("\r    };")
                                writeFile.write("\r    ly_index = layer.load();")
                                writeFile.write("\r    get_ajax_baseurl(inputdata, \"get_T01_"+temTABLE_NAME+"$"+key+"\");")
                                writeFile.write("\r}")            
                            writeFile.write("\r")                                            
                            writeFile.write("\rfunction get_T01_"+temTABLE_NAME+"$"+key+"(input) {")
                            writeFile.write("\r    layer.close(ly_index);")
                            writeFile.write("\r    //查询失败")
                            writeFile.write("\r    if (Call_QryResult(input.T01_"+temTABLE_NAME+"$"+key+") == false)")
                            writeFile.write("\r        return false;")
                            writeFile.write("\r    ary_"+key+" = input.T01_"+temTABLE_NAME+"$"+key+";")  
                            iIndex = iIndex + 1
                            bFlag = True
                        elif isinstance(self.temSelectAtt[key],str) and self.temSelectAtt[key].find("select ") == -1:
                            if bFlag == True:
                                writeFile.write("\r    var inputdata = {")
                                writeFile.write("\r        \"param_name\": \""+self.temSelectAtt[key]+"\",")
                                writeFile.write("\r        \"session_id\": session_id,")
                                writeFile.write("\r        \"login_id\": login_id")
                                writeFile.write("\r    };")
                                writeFile.write("\r    ly_index = layer.load();")
                                writeFile.write("\r    get_ajax_baseurl(inputdata, \"get_"+self.temSelectAtt[key]+"\");")
                                writeFile.write("\r}")            
                            writeFile.write("\r")                                            
                            writeFile.write("\rfunction get_"+self.temSelectAtt[key]+"(input) {")
                            writeFile.write("\r    layer.close(ly_index);")
                            writeFile.write("\r    //查询失败")
                            writeFile.write("\r    if (Call_QryResult(input."+self.temSelectAtt[key]+") == false)")
                            writeFile.write("\r        return false;")
                            writeFile.write("\r    ary_"+key+" = input."+self.temSelectAtt[key]+";")  
                            iIndex = iIndex + 1
                            bFlag = True
                    if bFlag:
                        for key2 in list(self.temSelectAtt.keys()):
                            for key in list(self.temQryAtt.keys()):                        
                                if key.upper() == key2.upper():
                                    writeFile.write("\r    $(\"#qry_"+key+"\").append(\"<option value='-1'></option>\")");
                                    writeFile.write("\r    $.each(ary_"+key+", function (i, obj) {")
                                    writeFile.write("\r        addOptionValue(\"qry_"+key+"\", obj[GetLowUpp(\"main_id\")], obj[GetLowUpp(\"cn_name\")]);")
                                    writeFile.write("\r    });")                        
                        writeFile.write("\r    init_"+temTABLE_NAME+"();")
                        writeFile.write("\r}")                        
                elif temLines.find("*declare query param begin*") != -1:
                    writeFile.write(self.AppenDQPB1())
                elif temLines.find("*find qry fun begin*") != -1:
                    writeFile.write(self.AppenFQFB())
                elif temLines.find("*table column begin*") != -1:
                    writeFile.write(self.AppendColumn())
                    pass
                elif temLines.find("*query conditions init begin*") != -1:
                    writeFile.write(self.AppenQCIB())
                    pass
                elif temLines.find("*get query param begin*") != -1:
                    writeFile.write(self.AppenGQPB())
                    pass
                elif temLines.find("*set query param begin*") != -1:
                    writeFile.write(self.AppenSQPB())
                    pass
                elif temLines.find("*add param value begin*") != -1:
                    writeFile.write(self.AppenAPVB())
                    pass
                if not temLines:
                    break
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if readFile != None:
                readFile.close()
            if writeFile != None:
                writeFile.close()
    def AppenSQPB(self):
        temResult = ""
        for key in list(self.temQryAtt.keys()):
            if isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\r    tem_"+key+" = $(\"#qry_"+key+"\").val();" 
                pass
            elif isinstance(getattr(self.objN,"att"+key),int):
                temResult = temResult + "\r    tem_"+key+" = $(\"#qry_"+key+"\").val();"
                pass
            elif isinstance(getattr(self.objN,"att"+key),float):
                temResult = temResult + "\r    tem_"+key+" = $(\"#qry_"+key+"\").val();"
                pass
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\r    tem_begin_"+key+" = $(\"#qry_begin_"+key+"\").val();"
                temResult = temResult + "\r    tem_end_"+key+" = $(\"#qry_end_"+key+"\").val();"
        for key in list(self.temFindAtt.keys()):
            if isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\r    tem_"+key+" = $(\"#find_"+key+"\").val();" 
            elif isinstance(getattr(self.objN,"att"+key),int):
                temResult = temResult + "\r    tem_"+key+" = $(\"#find_"+key+"\").val();"
            elif isinstance(getattr(self.objN,"att"+key),float):
                temResult = temResult + "\r    tem_"+key+" = $(\"#find_"+key+"\").val();"
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\r    tem_begin_"+key+" = $(\"#find_begin_"+key+"\").val();"
                temResult = temResult + "\r    tem_end_"+key+" = $(\"#find_end_"+key+"\").val();"
        return temResult
    def AppenGQPB(self):
        temResult = ""
        temIndex = 0
        for key in list(self.temQryAtt.keys()):
            temIndex = temIndex + 1
            if isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": s_encode(tem_"+key+")"
                pass
            elif isinstance(getattr(self.objN,"att"+key),int):
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": tem_"+key
                temIndex = temIndex + 1
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": tem_"+key
                pass
            elif isinstance(getattr(self.objN,"att"+key),float):
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": tem_"+key
                pass
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": tem_begin_"+key
                temIndex = temIndex + 1
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": tem_end_"+key
        for key in list(self.temFindAtt.keys()):
            temIndex = temIndex + 1
            if isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": s_encode(tem_"+key+")"
                pass
            elif isinstance(getattr(self.objN,"att"+key),int):
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": tem_"+key
                temIndex = temIndex + 1
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": tem_"+key
                pass
            elif isinstance(getattr(self.objN,"att"+key),float):
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": tem_"+key
                pass
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": tem_begin_"+key
                temIndex = temIndex + 1
                temResult = temResult + "\r        ,\"param_value"+str(temIndex)+"\": tem_end_"+key
        return temResult
    def AppenQCIB(self):
        temResult = ""
        for key in list(self.temQryAtt.keys()):
            if isinstance(getattr(self.objN,"att"+key),str):
                pass
            elif isinstance(getattr(self.objN,"att"+key),int):
                pass
            elif isinstance(getattr(self.objN,"att"+key),float):
                pass
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\r   $(\"#qry_begin_"+key+"\").val(DateAdd(\"d\", -5, new Date()).Format('yyyy-MM-dd hh:mm:ss'));"
                temResult = temResult + "\r   laydate.render({"
                temResult = temResult + "\r       elem: '#qry_begin_"+key+"',"
                temResult = temResult + "\r       type: 'datetime',"
                temResult = temResult + "\r       trigger: 'click'"
                temResult = temResult + "\r   });"
                temResult = temResult + "\r   $(\"#qry_end_"+key+"\").val(DateAdd(\"d\", 1, new Date()).Format('yyyy-MM-dd hh:mm:ss'));"
                temResult = temResult + "\r   laydate.render({"
                temResult = temResult + "\r       elem: '#qry_end_"+key+"',"
                temResult = temResult + "\r       type: 'datetime',"
                temResult = temResult + "\r       trigger: 'click'"
                temResult = temResult + "\r   });"
        return temResult
    def AppenFQFB2(self):
        temResult = ""
        for key in list(self.temFindAtt.keys()):
            temValue = Com_Fun.GetHashTable(self.temFindAtt,key)
            temResult = temResult+"\r"
            temResult = temResult+"\rfunction "+key+"_cn_name_fun(){"
            temResult = temResult+"\r    layer.open({"
            temResult = temResult+"\r        type: 2,"
            temResult = temResult+"\r        area: ['1000px', '570px'],"
            temResult = temResult+"\r        fixed: false, //不固定"
            temResult = temResult+"\r        maxmin: true,"            
            temRe = Com_Fun.GetHashTable(self.temFReturnAtt,key)            
            temResult = temResult+"\r        content: \""+temValue+"?target_name=find_"+key+"_cn_name&target_id="+key+"&sourc_id="+temRe[0]+"&sourc_name="+temRe[1]+"\","
            temResult = temResult+"\r        success: function(layero, index){"
            temResult = temResult+"\r            //var body = layer.getChildFrame('body',index);"
            temResult = temResult+"\r            //var main_id = $(\"#find_"+key+"\").val();"
            temResult = temResult+"\r        }"
            temResult = temResult+"\r    });"
            temResult = temResult+"\r}"
        return temResult
    def AppenAPVB(self):
        temResult = ""
        for key in list(self.temFindAtt.keys()):
            temResult = temResult+"\r            $.each(inputs, function (i, obj) {"
            temResult = temResult+"\r                if (obj.id.toUpperCase() == \"find_"+key+"_cn_name\".toUpperCase()) {"
            temResult = temResult+"\r                    $(obj).val($(\"#find_"+key+"_cn_name\").val());"
            temResult = temResult+"\r                }"
            temResult = temResult+"\r                else if ((\"find_\"+obj.id).toUpperCase() == \"find_"+key+"\".toUpperCase()){"
            temResult = temResult+"\r                    $(obj).val($(\"#find_\"+obj.id).val());"
            temResult = temResult+"\r                }"
            temResult = temResult+"\r            });"
            temResult = temResult+"\r"
        return temResult
    def AppenFQFB(self):
        temResult = ""
        for key in list(self.temFindAtt.keys()):
            temValue = Com_Fun.GetHashTable(self.temFindAtt,key)
            temResult = temResult+"\r"
            temResult = temResult+"\rfunction "+key+"_cn_name_fun(){"
            temResult = temResult+"\r    layer.open({"
            temResult = temResult+"\r        type: 2,"
            temResult = temResult+"\r        area: ['1100px', '600px'],"
            temResult = temResult+"\r        fixed: false, //不固定"
            temResult = temResult+"\r        maxmin: true,"            
            temRe = Com_Fun.GetHashTable(self.temFReturnAtt,key)            
            temResult = temResult+"\r        content: \""+temValue+"?target_name=find_"+key+"_cn_name&target_id=find_"+key+"&sourc_id="+temRe[0]+"&sourc_name="+temRe[1]+"\","
            temResult = temResult+"\r        success: function(layero, index){"
            temResult = temResult+"\r            //var body = layer.getChildFrame('body',index);"
            temResult = temResult+"\r            //var main_id = $(\"#find_"+key+"\").val();"
            temResult = temResult+"\r        }"
            temResult = temResult+"\r    });"
            temResult = temResult+"\r}"
        return temResult
    def AppenDQPB1(self):
        temResult = ""
        for key in list(self.temQryAtt.keys()):
            if isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\rvar tem_"+key+" = \"\";"
            elif isinstance(getattr(self.objN,"att"+key),int):
                temResult = temResult + "\rvar tem_"+key+" = \"0\";"
            elif isinstance(getattr(self.objN,"att"+key),float):
                temResult = temResult + "\rvar tem_"+key+" = \"0\";"
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\rvar tem_begin_"+key+" = new Date().Format(\"yyyy-MM-dd hh:mm:ss\");"
                temResult = temResult + "\rvar tem_end_"+key+" = new Date().Format(\"yyyy-MM-dd hh:mm:ss\");"
        for key in list(self.temFindAtt.keys()):
            if isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\rvar tem_"+key+" = \"\";"
            elif isinstance(getattr(self.objN,"att"+key),int):
                temResult = temResult + "\rvar tem_"+key+" = \"-1\";"
            elif isinstance(getattr(self.objN,"att"+key),float):
                temResult = temResult + "\rvar tem_"+key+" = \"0\";"
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\rvar tem_begin_"+key+" = new Date().Format(\"yyyy-MM-dd hh:mm:ss\");"
                temResult = temResult + "\rvar tem_end_"+key+" = new Date().Format(\"yyyy-MM-dd hh:mm:ss\");"
        return temResult
    def AppendColumn(self):
        temResult = ""
        try:                
            if len(self.temAtts) > 0:
                temIndex = 0
                for temColumn in self.temAtts:
                    if temIndex > 0:
                        temResult = temResult + ","
                    temIndex = temIndex + 1
                    temFormat = ""
                    for key in list(self.temSelectAtt.keys()):                        
                        if key.upper() == temColumn.upper():
                            temFormat = "\r        ,formatter: format_"+key
                            break                        
                    if temColumn.upper() == "MAIN_ID":                        
                        temResult = temResult + "\r    {"
                        temResult = temResult + "\r        title: '"+self.temAttCN[temColumn]+"',"
                        temResult = temResult + "\r        field: '"+temColumn.upper()+"',"
                        temResult = temResult + "\r        sortable: true,"
                        temResult = temResult + "\r        visible: false"
                        temResult = temResult + "\r    }"
                    elif temColumn.upper() == "IS_AVA":                     
                        temResult = temResult + "\r    {"
                        temResult = temResult + "\r        title: '"+self.temAttCN[temColumn]+"',"
                        temResult = temResult + "\r        field: '"+temColumn.upper()+"',"
                        temResult = temResult + "\r        sortable: true"
                        temResult = temResult + temFormat#"\r        formatter: get_s_state"
                        temResult = temResult + "\r    }"
                    elif getattr(self.objN,"att"+temColumn) is None:                    
                        temResult = temResult + "\r    {"
                        temResult = temResult + "\r        title: '"+self.temAttCN[temColumn]+"',"
                        temResult = temResult + "\r        field: '"+temColumn.upper()+"',"
                        temResult = temResult + "\r        sortable: true"
                        temResult = temResult + "\r    }"
                    elif isinstance(getattr(self.objN,"att"+temColumn),int):                    
                        temResult = temResult + "\r    {"
                        temResult = temResult + "\r        title: '"+self.temAttCN[temColumn]+"',"
                        temResult = temResult + "\r        field: '"+temColumn.upper()+"',"
                        temResult = temResult + "\r        sortable: true"
                        temResult = temResult + temFormat
                        temResult = temResult + "\r    }"
                    elif isinstance(getattr(self.objN,"att"+temColumn),float):                    
                        temResult = temResult + "\r    {"
                        temResult = temResult + "\r        title: '"+self.temAttCN[temColumn]+"',"
                        temResult = temResult + "\r        field: '"+temColumn.upper()+"',"
                        temResult = temResult + "\r        sortable: true"
                        temResult = temResult + temFormat
                        temResult = temResult + "\r    }"
                    elif isinstance(getattr(self.objN,"att"+temColumn),str):                    
                        temResult = temResult + "\r    {"
                        temResult = temResult + "\r        title: '"+self.temAttCN[temColumn]+"',"
                        temResult = temResult + "\r        field: '"+temColumn.upper()+"',"
                        temResult = temResult + "\r        sortable: true"
                        if temFormat == "":
                            temFormat = "\r        ,formatter: set_s_decode"
                        temResult = temResult + temFormat
                        temResult = temResult + "\r    }"
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        return temResult
    def AppendFindCondHtml(self):
        temResult = ""
        for key in list(self.temFindAtt.keys()):
            temResult = temResult + "\r                                                    <span type=\"find\">"
            temResult = temResult + "\r                                                        <label for=\"find_"+key+"\" class=\"control-label\">"+self.temAttCN[key]+"</label>"
            temResult = temResult + "\r                                                        <input class=\"form-control\" id=\"find_"+key+"_cn_name\" type=\"text\" name=\"find_"+key+"_cn_name\" readonly=\"true\">"
            temResult = temResult + "\r                                                        <img src=\"../img/search.png\" onclick=\""+key+"_cn_name_fun()\">"
            temResult = temResult + "\r                                                        <img src=\"../img/error.png\" onclick=\"clear_input_cn_name(\'find_"+key+"_cn_name\',\'find_"+key+"\')\">"
            temResult = temResult + "\r                                                        <input name=\"find_"+key+"\" id=\"find_"+key+"\" type=\"text\" value=\"-1\" style=\"display:none\">"
            temResult = temResult + "\r                                                    </span>"
        return temResult
    def AppendQryCondHtml(self):
        temResult = ""
        for key in list(self.temQryAtt.keys()):
            temValue = Com_Fun.GetHashTable(self.temQryAtt,key)
            if isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\r                                                    <label for=\"qry_"+key+"\" class=\"control-label\">"+temValue+":</label>"
                temInput = "<input id=\"qry_"+key+"\" name=\"qry_"+key+"\" class=\"form-control\" placeholder=\"请输入信息\">"
                for key2 in list(self.temSelectAtt.keys()):                        
                    if key.upper() == key2.upper():
                        temInput = "<select class=\"form-control\" id=\"qry_"+key+"\" name=\"qry_"+key+"\" placeholder=\"请输入信息\"></select>" 
                        break                
                temResult = temResult + "\r                                                    "+temInput
            elif isinstance(getattr(self.objN,"att"+key),int):
                temResult = temResult + "\r                                                    <label for=\"qry_"+key+"\" class=\"control-label\">"+temValue+":</label>"
                temInput = "<input id=\"qry_"+key+"\" name=\"qry_"+key+"\" class=\"form-control\" placeholder=\"请输入整数\">"
                for key2 in list(self.temSelectAtt.keys()):                        
                    if key.upper() == key2.upper():
                        temInput = "<select class=\"form-control\" id=\"qry_"+key+"\" name=\"qry_"+key+"\" placeholder=\"请输入整数\"></select>" 
                        break
                temResult = temResult + "\r                                                    "+temInput
            elif isinstance(getattr(self.objN,"att"+key),float):
                temResult = temResult + "\r                                                    <label for=\"qry_"+key+"\" class=\"control-label\">"+temValue+":</label>"
                temInput = "<input id=\"qry_"+key+"\" name=\"qry_"+key+"\" class=\"form-control\" placeholder=\"请输入小数\">"
                for key2 in list(self.temSelectAtt.keys()):                        
                    if key.upper() == key2.upper():
                        temInput = "<select class=\"form-control\" id=\"qry_"+key+"\" name=\"qry_"+key+"\" placeholder=\"请输入小数\"></select>" 
                        break
                temResult = temResult + "\r                                                    "+temInput
            elif getattr(self.objN,"att"+key) is None:
                temResult = temResult + "\r                                                    <label for=\"qry_begin_"+key+"\" class=\"control-label\">"+temValue+"开始时间:</label>"
                temResult = temResult + "\r                                                    <input id=\"qry_begin_"+key+"\" name=\"qry_begin_"+key+"\" class=\"form-control datetimePicker\" placeholder=\"请选择开始时间\">"
                temResult = temResult + "\r                                                    <label for=\"qry_end_"+key+"\" class=\"control-label\">"+temValue+"结束时间:</label>"
                temResult = temResult + "\r                                                    <input id=\"qry_end_"+key+"\" name=\"qry_end_"+key+"\" class=\"form-control datetimePicker\" placeholder=\"请选择结束时间\">"
        return temResult
    def AppendEditFormHtml(self):
        temResult = ""
        for key in list(self.temAttCN.keys()):
            temValue = Com_Fun.GetHashTable(self.temAttCN,key)
            if key.upper() == "MAIN_ID":
                temResult = temResult + "\r                            <div class=\"col-sm-12 form-group\">"
                temResult = temResult + "\r                                <div class=\"col-sm-12\">"
                temResult = temResult + "\r                                    <label for=\""+key+"\" class=\"col-sm-3 control-label\">"+temValue+":*</label>" 
                temResult = temResult + "\r                                    <div class=\"col-sm-9\">"
                temResult = temResult + "\r                                        <input class=\"form-control\" id=\""+key+"\" name=\""+key+"\" type=\"text\" readonly=\"true\" />"
                temResult = temResult + "\r                                    </div>"
                temResult = temResult + "\r                                </div>"
                temResult = temResult + "\r                            </div>"
            elif isinstance(getattr(self.objN,"att"+key),str):
                temResult = temResult + "\r                            <div class=\"col-sm-12 form-group\">"
                temResult = temResult + "\r                                <div class=\"col-sm-12\">"
                temResult = temResult + "\r                                    <label for=\""+key+"\" class=\"col-sm-3 control-label\">"+temValue+":*</label>" 
                temResult = temResult + "\r                                    <div class=\"col-sm-9\">"
                temInput = "<input class=\"form-control\" id=\""+key+"\" name=\""+key+"\" type=\"text\" placeholder=\"请输入信息\"/>"
                for key2 in list(self.temSelectAtt.keys()):                                            
                    if key.upper() == key2.upper():
                        if Com_Fun.GetHashTable(self.temFindAtt,key2) != "":
                            temInput = "\r                                         <span type=\"find\">"
                            temInput = temInput + "\r                                            <input class=\"form-control form-search\" id=\"find_"+key+"_cn_name\" name=\"find_"+key+"_cn_name\" type=\"text\" readonly=\"true\">"
                            temInput = temInput + "\r                                            <img src=\"../img/search.png\" onclick=\""+key+"_cn_name_fun()\">"
                            temInput = temInput + "\r                                            <img src=\"../img/error.png\" onclick=\"clear_input_cn_name(\'find_"+key+"_cn_name\',\'"+key+"\')\">"
                            temInput = temInput + "\r                                            <input name=\""+key+"\" id=\""+key+"\" value=\"\" type=\"text\" style=\"display:none\">"
                            temInput = temInput + "\r                                         </span>"
                        else:
                            temInput = "<select class=\"form-control\" id=\""+key+"\" name=\""+key+"\" placeholder=\"请输入信息\"></select>" 
                        break
                temResult = temResult + "\r                                        "+temInput
                temResult = temResult + "\r                                    </div>"
                temResult = temResult + "\r                                </div>"
                temResult = temResult + "\r                            </div>"
            elif isinstance(getattr(self.objN,"att"+key),int):                
                temResult = temResult + "\r                            <div class=\"col-sm-12 form-group\">"
                temResult = temResult + "\r                                <div class=\"col-sm-12\">"
                temResult = temResult + "\r                                    <label for=\""+key+"\" class=\"col-sm-3 control-label\">"+temValue+":*</label>" 
                temResult = temResult + "\r                                    <div class=\"col-sm-9\">"
                temInput = "<input class=\"form-control\" id=\""+key+"\" name=\""+key+"\" type=\"text\" placeholder=\"请输入信息\"/>"
                for key2 in list(self.temSelectAtt.keys()):                        
                    if key.upper() == key2.upper():
                        if Com_Fun.GetHashTable(self.temFindAtt,key2) != "":
                            temInput = "\r                                         <span type=\"find\">"
                            temInput = temInput + "\r                                            <input class=\"form-control form-search\" id=\"find_"+key+"_cn_name\" name=\"find_"+key+"_cn_name\" type=\"text\" readonly=\"true\">"
                            temInput = temInput + "\r                                            <img src=\"../img/search.png\" onclick=\""+key+"_cn_name_fun()\">"
                            temInput = temInput + "\r                                            <img src=\"../img/error.png\" onclick=\"clear_input_cn_name(\'find_"+key+"_cn_name\',\'"+key+"\')\">"
                            temInput = temInput + "\r                                            <input name=\""+key+"\" id=\""+key+"\" value=\"-1\" type=\"text\" style=\"display:none\">"
                            temInput = temInput + "\r                                         </span>"
                        else:
                            temInput = "<select class=\"form-control\" id=\""+key+"\" name=\""+key+"\" placeholder=\"请输入信息\"></select>" 
                        break
                temResult = temResult + "\r                                        "+temInput
                temResult = temResult + "\r                                    </div>"
                temResult = temResult + "\r                                </div>"
                temResult = temResult + "\r                            </div>"
            elif isinstance(getattr(self.objN,"att"+key),float):                
                temResult = temResult + "\r                            <div class=\"col-sm-12 form-group\">"
                temResult = temResult + "\r                                <div class=\"col-sm-12\">"
                temResult = temResult + "\r                                    <label for=\""+key+"\" class=\"col-sm-3 control-label\">"+temValue+":*</label>" 
                temResult = temResult + "\r                                    <div class=\"col-sm-9\">"
                temInput = "<input class=\"form-control\" id=\""+key+"\" name=\""+key+"\" type=\"text\" placeholder=\"请输入信息\"/>"
                for key2 in list(self.temSelectAtt.keys()):                        
                    if key.upper() == key2.upper():
                        if Com_Fun.GetHashTable(self.temFindAtt,key2) != "":
                            temInput = "\r                                         <span type=\"find\">"
                            temInput = temInput + "\r                                            <input class=\"form-control form-search\" id=\"find_"+key+"_cn_name\" name=\"find_"+key+"_cn_name\" type=\"text\" readonly=\"true\">"
                            temInput = temInput + "\r                                            <img src=\"../img/search.png\" onclick=\""+key+"_cn_name_fun()\">"
                            temInput = temInput + "\r                                            <img src=\"../img/error.png\" onclick=\"clear_input_cn_name(\'find_"+key+"_cn_name\',\'"+key+"\')\">"
                            temInput = temInput + "\r                                            <input name=\""+key+"\" id=\""+key+"\" value=\"-1\" type=\"text\" style=\"display:none\">"
                            temInput = temInput + "\r                                         </span>"
                        else:
                            temInput = "<select class=\"form-control\" id=\""+key+"\" name=\""+key+"\" placeholder=\"请输入信息\"></select>" 
                        break
                temResult = temResult + "\r                                        "+temInput
                temResult = temResult + "\r                                    </div>"
                temResult = temResult + "\r                                </div>"
                temResult = temResult + "\r                            </div>"
            elif getattr(self.objN,"att"+key) is None:                
                temResult = temResult + "\r                            <div class=\"col-sm-12 form-group\">"
                temResult = temResult + "\r                                <div class=\"col-sm-12\">"
                temResult = temResult + "\r                                    <label for=\""+key+"\" class=\"col-sm-3 control-label\">"+temValue+":*</label>" 
                temResult = temResult + "\r                                    <div class=\"col-sm-9\">"
                temResult = temResult + "\r                                        <input class=\"form-control datetimePicker\" id=\""+key+"\" name=\""+key+"\" type=\"text\"  placeholder=\"请设置时间格式\"/>"
                temResult = temResult + "\r                                    </div>"
                temResult = temResult + "\r                                </div>"
                temResult = temResult + "\r                            </div>"
        temResult = temResult + "\r"
        return temResult
    def WriteBizHtml_1(self,templatePath,tempTargetPath,temTABLE_NAME):
        readFile = None
        writeFile = None            
        try: 
            if os.path.exists(templatePath+Com_Para.zxyPath+"template_table2.html") == False :
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"template_table2.html模板文件不存在"+ "\"}]}"
                return ""
            else:
                readFile = open(templatePath+Com_Para.zxyPath+"template_table2.html", 'r',encoding=Com_Para.U_CODE)
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+".html") == False :
                writeFile = open(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+".html", 'a',encoding=Com_Para.U_CODE)
            else:
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\""+temTABLE_NAME+".html文件已存在"+ "\"}]}"
                return ""
            while True and readFile is not None and writeFile is not None:
                temLines = readFile.readline()
                if self.param_value3 != "":
                    temLines = temLines.replace("{template_table_CN}",self.param_value3)
                temLines = temLines.replace("{template_table_EN}",temTABLE_NAME)
                writeFile.write(temLines)
                if temLines.find("<!-- query conditions begin -->") != -1:
                    writeFile.write(self.AppendQryCondHtml())
                    writeFile.write(self.AppendFindCondHtml())
                if not temLines:
                    break
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if readFile != None:
                readFile.close()
            if writeFile != None:
                writeFile.close()
    def WriteBizHtml_2(self,templatePath,tempTargetPath,temTABLE_NAME):
        readFile = None
        writeFile = None            
        try: 
            if os.path.exists(templatePath+Com_Para.zxyPath+"template_table2_$.html") == False :
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\"template_table2_$.html模板文件不存在"+ "\"}]}"
                return ""
            else:
                readFile = open(templatePath+Com_Para.zxyPath+"template_table2_$.html", 'r',encoding=Com_Para.U_CODE)
            if os.path.exists(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+"_$.html") == False :
                writeFile = open(tempTargetPath+Com_Para.zxyPath+temTABLE_NAME+"_$.html", 'a',encoding=Com_Para.U_CODE)
            else:
                self.strResult = "{\""+ self.param_name+ "\":[{\""+Com_Fun.GetLowUpp("s_result")+"\":\"0\",\""+Com_Fun.GetLowUpp("error_desc")+"\":\""+temTABLE_NAME+"_$.html文件已存在"+ "\"}]}"
                return ""
            while True and readFile is not None and writeFile is not None:
                temLines = readFile.readline()
                if self.param_value3 != "":
                    temLines = temLines.replace("{template_table_CN}",self.param_value3)
                temLines = temLines.replace("{template_table_EN}",temTABLE_NAME)
                writeFile.write(temLines)                
                if temLines.find("<!-- form column begin -->") != -1:
                    writeFile.write(self.AppendEditFormHtml())
                if not temLines:
                    break
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            if readFile != None:
                readFile.close()
            if writeFile != None:
                writeFile.close()