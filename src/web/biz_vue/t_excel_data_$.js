//按钮事件新增或编辑
var BE34FE_type = "";
//其他页面传到本页面参数
var BE34FE_param = {};
//暂时没用
var BE34FE_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BE34FE_ary_file_id = null;var BE34FE_ary_excel_tag_id = null;
/*declare select options end*/

//业务逻辑数据开始
function BE34FE_t_excel_data_biz_start(inputdata) {
	BE34FE_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_excel_data$file_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BE34FE_get_N01_t_excel_data$file_id");	
    /*biz end*/
}

/*biz step begin*/
function BE34FE_format_file_id(value, row, index) {    var objResult = value;    for(i = 0; i < BE34FE_ary_file_id.length; i++) {        var obj = BE34FE_ary_file_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BE34FE_format_excel_tag_id(value, row, index) {    var objResult = value;    for(i = 0; i < BE34FE_ary_excel_tag_id.length; i++) {        var obj = BE34FE_ary_excel_tag_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BE34FE_get_N01_t_excel_data$file_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_excel_data$file_id) == false)        return false;    BE34FE_ary_file_id = input.N01_t_excel_data$file_id;    if($("#BE34FE_file_id").is("select") && $("#BE34FE_file_id")[0].options.length == 0)    {        $.each(BE34FE_ary_file_id, function (i, obj) {            addOptionValue("BE34FE_file_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    var inputdata = {        "param_name": "N01_t_excel_data$excel_tag_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BE34FE_get_N01_t_excel_data$excel_tag_id");}function BE34FE_get_N01_t_excel_data$excel_tag_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_excel_data$excel_tag_id) == false)        return false;    BE34FE_ary_excel_tag_id = input.N01_t_excel_data$excel_tag_id;    if($("#BE34FE_excel_tag_id").is("select") && $("#BE34FE_excel_tag_id")[0].options.length == 0)    {        $.each(BE34FE_ary_excel_tag_id, function (i, obj) {            addOptionValue("BE34FE_excel_tag_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    BE34FE_init_t_excel_data();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function BE34FE_page_end(){
	if(BE34FE_param["type"] == "edit"){
		BE34FE_get_edit_info();
	}
}

//页面初始化方法
function BE34FE_init_t_excel_data() {
	//type = getUrlParam("type");
	if(BE34FE_param["type"] == "add"){
		$("#BE34FE_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BE34FE_file_id").val(BE34FE_param["file_id"]);        $("#BE34FE_excel_tag_id").val(BE34FE_param["excel_tag_id"]);		
		/*Get Find Select param end*/	
	}
	else if(BE34FE_param["type"] == "edit"){
	
	}
	
	//表单验证
	BE34FE_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BE34FE_create_date").val() == "")    {        $("#BE34FE_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BE34FE_create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BE34FE_page_end();
}

//提交表单数据
function BE34FE_SubmitForm(){
	if(BE34FE_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_excel_data",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": $("#BE34FE_file_id").val()                ,"param_value2": $("#BE34FE_excel_tag_id").val()                ,"param_value3": s_encode($("#BE34FE_data_value").val())                ,"param_value4": $("#BE34FE_create_date").val()                ,"param_value5": s_encode($("#BE34FE_s_desc").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BE34FE_get_N01_ins_t_excel_data");
	}
	else if(BE34FE_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_excel_data",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": $("#BE34FE_file_id").val()                ,"param_value2": $("#BE34FE_excel_tag_id").val()                ,"param_value3": s_encode($("#BE34FE_data_value").val())                ,"param_value4": $("#BE34FE_create_date").val()                ,"param_value5": s_encode($("#BE34FE_s_desc").val())                ,"param_value6": $("#BE34FE_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BE34FE_get_N01_upd_t_excel_data");
	}
}

//vue回调
function BE34FE_t_excel_data_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#BE34FE_save_t_excel_data_Edit").click(function () {
	$("form[name='BE34FE_DataModal']").submit();
})

/*修改数据*/
function BE34FE_get_N01_upd_t_excel_data(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_excel_data) == true)
	{
		swal("修改数据成功!", "", "success");
		AE34FE_t_excel_data_query();
		BE34FE_clear_validate();
		layer.close(BE34FE_param["ly_index"]);
	}
}

/*添加数据*/
function BE34FE_get_N01_ins_t_excel_data(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_excel_data) == true)
	{
		swal("添加数据成功!", "", "success");
		AE34FE_t_excel_data_query();
		BE34FE_clear_validate();
		layer.close(BE34FE_param["ly_index"]);
	}
}

//取消编辑
$("#BE34FE_cancel_t_excel_data_Edit").click(function () {
	layer.close(BE34FE_param["ly_index"]);
	BE34FE_clear_validate();
	$("[id^='BE34FE_div']").hide();
})

//清除查找框
function BE34FE_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BE34FE_clear_validate(){
	$("#BE34FE_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BE34FE_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BE34FE_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BE34FE_clear_edit_info(){
	var inputs = $("#BE34FE_DataModal").find('input');
	var selects = $("#BE34FE_DataModal").find("select");
	var textareas = $("#BE34FE_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	BE34FE_init_t_excel_data();
}

//页面输入框赋值
function BE34FE_get_edit_info(){
	var rowData = $("#AE34FE_t_excel_data_Events").bootstrapTable('getData')[AE34FE_select_t_excel_data_rowId];
	var inputs = $("#BE34FE_DataModal").find('input');
	var selects = $("#BE34FE_DataModal").find("select");
	var textareas = $("#BE34FE_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"AE34FE","BE34FE");
	
	/*
	$.each(selects, function (i, obj) {
		if(typeof(eval("AE34FE_ary_"+obj.id.toString().replace("BE34FE_",""))) == "object")
		{
			$.each(eval("AE34FE_ary_"+obj.id.toString().replace("BE34FE_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE34FE_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("BE34FE_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("AE34FE_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#BE34FE_DataModal").find('[id="'+"BE34FE_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE34FE_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("BE34FE_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});*/
}

//form验证
function BE34FE_checkFormInput() {
    BE34FE_validate = $("#BE34FE_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BE34FE_main_id: {}            ,BE34FE_file_id: {}            ,BE34FE_excel_tag_id: {}            ,BE34FE_data_value: {}            ,BE34FE_create_date: {date: true,required : true,maxlength:19}            ,BE34FE_s_desc: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BE34FE_main_id: {}            ,BE34FE_file_id: {}            ,BE34FE_excel_tag_id: {}            ,BE34FE_data_value: {}            ,BE34FE_create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BE34FE_s_desc: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BE34FE_SubmitForm();
        	return false;
        }
    })
}