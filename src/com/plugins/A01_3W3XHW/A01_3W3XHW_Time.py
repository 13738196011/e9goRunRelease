#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import time,json,importlib
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_IHGDW0.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_IHGDW0 import A01_Para
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.comport.ComModBus import ComModBus
from com.plugins.A01_IHGDW0.monitor_data_model import monitor_data_model
from com.plugins.A01_3W3XHW.HJpacket import HJpacket
from com.plugins.A01_IHGDW0.A01_Fun import A01_Fun
from com.zxy.z_debug import z_debug
class A01_3W3XHW_Time(z_debug):
    strContinue      = "1"   
    strResult        = ""
    def __init__(self):
        pass
    def init_start(self):
        try:            
            for temv_prot_element in A01_Para.ht_v_prot_element:
                if temv_prot_element.attprot_name == "A01_3W3XHW":
                    self.SendB3_1(self,temv_prot_element.attcom_port)
                    break
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            pass
    def SendB3_1(self,temcom_port):
        temA01modbus = ComModBus()
        temHJpacket = HJpacket()
        temHJpacket.init_B_3_1()
        temSocketValue = temHJpacket.GetSendPack_CP()
        temA01modbus.get_data_com_value_immed(temcom_port, temSocketValue)
    def SendB3_3(self,temcom_port):
        temA01modbus = ComModBus()
        temHJpacket = HJpacket()
        temHJpacket.init_B_3_3()
        for temv_prot_element in A01_Para.ht_v_prot_element:
            if temv_prot_element.attprot_name == "A01_3W3XHW":
                temHJpacket.attCP = temHJpacket.attCP+"PolId="+temv_prot_element.attelement_code+";"
        temHJpacket.attCP = temHJpacket.attCP+"InfoId=i12001;"
        temSocketValue = temHJpacket.GetSendPack_CP()
        temA01modbus.get_data_com_value_immed(temcom_port, temSocketValue)
    def SendB3_4(self,temcom_port):
        temA01modbus = ComModBus()
        temHJpacket = HJpacket()
        temHJpacket.init_B_3_4()
        for temv_prot_element in A01_Para.ht_v_prot_element:
            if temv_prot_element.attprot_name == "A01_3W3XHW":
                temHJpacket.attCP = temHJpacket.attCP+"PolId="+temv_prot_element.attelement_code+";"
        temHJpacket.attCP = temHJpacket.attCP+"InfoId=i13001;"
        temSocketValue = temHJpacket.GetSendPack_CP()
        temA01modbus.get_data_com_value_immed(temcom_port, temSocketValue)
    def SendB3_5(self,temcom_port):
        temA01modbus = ComModBus()
        temHJpacket = HJpacket()
        temHJpacket.init_B_3_5()
        for temv_prot_element in A01_Para.ht_v_prot_element:
            if temv_prot_element.attprot_name == "A01_3W3XHW":
                temHJpacket.attCP = temHJpacket.attCP+"PolId="+temv_prot_element.attelement_code+";"
        temHJpacket.attCP = temHJpacket.attCP+"SystemTime="+Com_Fun.GetTime("%Y%m%d%H%M%S")+";"
        temSocketValue = temHJpacket.GetSendPack_CP()
        temA01modbus.get_data_com_value_immed(temcom_port, temSocketValue)
    def SendB3_6(self,temcom_port):
        temA01modbus = ComModBus()
        temHJpacket = HJpacket()
        temHJpacket.init_B_3_6()
        iIndex = 0
        for temv_prot_element in A01_Para.ht_v_prot_element:
            if temv_prot_element.attprot_name == "A01_3W3XHW":
                if iIndex  != 0:
                    temHJpacket.attCP = temHJpacket.attCP+";"
                temHJpacket.attCP = temHJpacket.attCP+"PolId="+temv_prot_element.attelement_code
                iIndex = 1
        temSocketValue = temHJpacket.GetSendPack_CP()
        temA01modbus.get_data_com_value_immed(temcom_port, temSocketValue)
    def SendB3_7(self,temcom_port):
        temA01modbus = ComModBus()
        temHJpacket = HJpacket()
        temHJpacket.init_B_3_7()
        iIndex = 0
        for temv_prot_element in A01_Para.ht_v_prot_element:
            if temv_prot_element.attprot_name == "A01_3W3XHW":
                if iIndex  != 0:
                    temHJpacket.attCP = temHJpacket.attCP+";"
                temHJpacket.attCP = temHJpacket.attCP+"PolId="+temv_prot_element.attelement_code
                iIndex = 1
        temSocketValue = temHJpacket.GetSendPack_CP()
        temA01modbus.get_data_com_value_immed(temcom_port, temSocketValue)
    def SendB3_8(self,temcom_port):
        temA01modbus = ComModBus()
        temHJpacket = HJpacket()
        temHJpacket.init_B_3_8()
        iIndex = 0
        for temv_prot_element in A01_Para.ht_v_prot_element:
            if temv_prot_element.attprot_name == "A01_3W3XHW":
                if iIndex  != 0:
                    temHJpacket.attCP = temHJpacket.attCP+";"
                temHJpacket.attCP = temHJpacket.attCP+"PolId="+temv_prot_element.attelement_code
                iIndex = 1
        temSocketValue = temHJpacket.GetSendPack_CP()
        temA01modbus.get_data_com_value_immed(temcom_port, temSocketValue)