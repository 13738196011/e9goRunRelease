#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
class t_protocol(object):
    attmain_id = -1
    attprot_name = ""
    attoem_name = ""
    attcreate_date = None
    atts_desc = ""
    atts_state = 1
    attcom_port = ""
    def __init__(self):
        self.attcreate_date = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")