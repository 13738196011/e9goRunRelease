#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import time,json,struct,urllib.parse
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.interfaceReflect.A01_A1B2C3 import A01_A1B2C3
from com.plugins.A01_6GG8T3 import A01_Para
from com.zxy.z_debug import z_debug
class ClientReg_Time(z_debug):
    strContinue      = "1"
    strResult        = ""
    def __init__(self):
        pass
    def init_start(self):
        localClient = Com_Fun.GetHashTableNone(Com_Para.dClientThreadList,Com_Para.ServerIPList.replace(":","|"))
        if localClient is not None:
            Com_Fun.SendSocket("##CMD##\r\n",localClient)
        s = Com_Fun.GetHashTableNone(Com_Para.dClientThreadList, str(Com_Para.ServerIP)+"|"+str(Com_Para.ServerPort))            
        temResult = "@##ST=01&DATATIME=" + Com_Fun.GetTime("%Y%m%d%H%M%S") + "&CHILD_NODE="+ Com_Para.Child_Code + "&CHILD_NAME="+Com_Para.Child_Name+"&COND_VALUE="+A01_Para.objRand+"##@\r\n"
        if s is not None:
            Com_Fun.SendSocket(temResult,s)
        if int(Com_Fun.GetTime("%H")) == 23 and int(Com_Fun.GetTime("%M")) == 25:
            self.Get_ServerTime(self)
    def Get_ServerTime(self):
        strCon = "sub_code=8A0731CC39614C90A5D474BC17253713&sub_usercode=114A6DB3BBE6419DA3768E6E25127310&param_name=T01_Get_Time"
        inputUrl = "http://" + Com_Para.ServerIP + ":"+ str(Com_Para.ServerWebPort)+ "/CenterData/getdata.jsp?" + strCon
        m_result = Com_Fun.PostHttp(inputUrl, "")
        if m_result == "-1" or m_result == "":
            pass
        else:    
            try:            
                temJso = json.loads(m_result)
                temJsoary = temJso["T01_Get_Time"]
                for temobj in temJsoary:
                    temSResult = temobj["stime"]
                    objAl = A01_A1B2C3()
                    objAl.param_value2 = temSResult
                    if Com_Para.devsys == "linux":
                        m_result = objAl.ChangeDateLinux()
                    else:
                        m_result = objAl.ChangeDateWin()
            except Exception as e:
                if str(type(self)) == "<class 'type'>":
                    self.debug_in(self,repr(e))#打印异常信息
                else:
                    self.debug_in(repr(e))#打印异常信息