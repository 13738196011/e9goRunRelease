#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import socket
import threading
import time
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.tcp.TcpServer import TcpServer
from com.zxy.z_debug import z_debug
class ServerThread(z_debug):
    attDtTimeOut = 60
    attInput = ""
    attNum = ""
    attPort = 0
    attThreadSize = 500
    def __init__(self, temInput, temNum, temPort):
        self.attInput = temInput
        self.attNum = temNum
        self.attPort = temPort
    def run(self):
        global dServThreadList        
        try:            
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.bind(('0.0.0.0', self.attPort))
            s.listen(self.attThreadSize)
            while True:
                try:
                    sock, addr = s.accept()
                    ckeys = sock.getpeername()[0] + '|' + str(sock.getpeername()[1])             
                    ts = TcpServer(ckeys)
                    t = threading.Thread(target=ts.server_link, args=(sock, addr), name="ServerClient" + ckeys)
                    t.start()
                except Exception as en:
                    if str(type(self)) == "<class 'type'>":
                        self.debug_in(self,repr(en))#打印异常信息
                    else:
                        self.debug_in(repr(en))#打印异常信息
                finally:
                    time.sleep(0.1)
        except Exception as e:            
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
            pass