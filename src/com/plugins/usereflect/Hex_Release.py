#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import json,binascii,re
from com.zxy.comport.ComModBus import ComModBus
from com.zxy.z_debug import z_debug
class Hex_Release(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = "0"
    rec_byte        = None
    rec_hex         = ""
    rec_ary         = []
    error_desc      = ""
    s_result        = 1
    def __init__(self):
        pass
    def init_start(self):
        if self.rec_byte is not None and self.s_result == 1:                
            self.rec_hex = (str(binascii.b2a_hex(self.rec_byte))[2:-1]).upper()
            self.rec_ary = re.findall(".{2}",self.rec_hex.upper())
            if self.crc_check() == False:
                self.s_result = 0
                self.error_desc = "指令返回结果异常:指令返回结果CRC校验失败"
        else:
            self.s_result = 0
            self.error_desc = "指令返回结果异常:指令返回结果为None"
    def crc_check(self):
        crcByte = ComModBus._getCrc16(self.rec_byte[0:len(self.rec_byte) - 2])
        if self.rec_byte[len(self.rec_byte) - 2:len(self.rec_byte)] == crcByte:
            return True
        else:
            return False
    def A01_00T5L4(self,temType):
        self.init_start()        
        if self.s_result == 0:
            pass
        elif temType == 1:
            if len(self.rec_ary) < 5:
                self.s_result = 0
                self.error_desc = "指令返回结果异常:返回结果长度错误"
            elif self.rec_ary[1] == "83":
                self.s_result = 0
                if self.rec_ary[2] == "01":
                    self.error_desc = "指令返回结果异常:非法功能"
                elif self.rec_ary[2] == "02":
                    self.error_desc = "指令返回结果异常:非法数据地址"
                elif self.rec_ary[2] == "03":
                    self.error_desc = "指令返回结果异常:非法数据值"
                elif self.rec_ary[2] == "04":
                    self.error_desc = "指令返回结果异常:从站设备故障"
                elif self.rec_ary[2] == "05":
                    self.error_desc = "指令返回结果异常:从站设备忙"
            elif self.rec_hex == "01060110000089F3":
                self.s_result = 1
                self.error_desc = "指令返回结果正常:控制指令发送成功"
            elif self.rec_hex == "xxx":
                self.s_result = 1
                self.error_desc = "指令返回结果正常:时间校准指令发送成功"
            elif self.rec_hex == "xxx":
                self.s_result = 1
                self.error_desc = "指令返回结果正常:设置运行模式指令发送成功"
            else:
                self.s_result = 0
                self.error_desc = "指令返回结果异常:"+self.rec_hex
    def A01_00K4T3(self,temType):
        self.init_start()        
        if self.s_result == 0:
            pass
        elif temType == 1:
            if len(self.rec_ary) < 5:
                self.s_result = 0
                self.error_desc = "指令返回结果异常:返回结果长度错误"
            elif self.rec_ary[1] == "83":
                self.s_result = 0
                if self.rec_ary[2] == "01":
                    self.error_desc = "指令返回结果异常:非法功能"
                elif self.rec_ary[2] == "02":
                    self.error_desc = "指令返回结果异常:非法数据地址"
                elif self.rec_ary[2] == "03":
                    self.error_desc = "指令返回结果异常:非法数据值"
                elif self.rec_ary[2] == "04":
                    self.error_desc = "指令返回结果异常:从站设备故障"
                elif self.rec_ary[2] == "05":
                    self.error_desc = "指令返回结果异常:从站设备忙"
            elif self.rec_hex == "01101200000104B1":
                self.s_result = 1
                self.error_desc = "指令返回结果正常:控制指令发送成功"
            elif self.rec_hex == "011012000004C4B2":
                self.s_result = 1
                self.error_desc = "指令返回结果正常:时间校准指令发送成功"
            elif self.rec_hex == "01101200000244B0":
                self.s_result = 1
                self.error_desc = "指令返回结果正常:设置运行模式指令发送成功"
            else:
                self.s_result = 0
                self.error_desc = "指令返回结果异常:"+self.rec_hex
    def A01_88H5UT(self,temType):
        self.init_start()        
        if self.s_result == 0:
            pass
        elif temType == "1":
            if len(self.rec_ary) < 5:
                self.s_result = 0
                self.error_desc = "指令返回结果异常:返回结果长度错误"
            elif self.rec_ary[1] == "83":
                self.s_result = 0
                if self.rec_ary[2] == "01":
                    self.error_desc = "指令返回结果异常:非法功能"
                elif self.rec_ary[2] == "02":
                    self.error_desc = "指令返回结果异常:非法数据地址"
                elif self.rec_ary[2] == "03":
                    self.error_desc = "指令返回结果异常:非法数据值"
                elif self.rec_ary[2] == "04":
                    self.error_desc = "指令返回结果异常:从站设备故障"
                elif self.rec_ary[2] == "05":
                    self.error_desc = "指令返回结果异常:从站设备忙"
            elif self.rec_hex == "01101200000104B1":
                self.s_result = 1
                self.error_desc = "指令返回结果正常:控制指令发送成功"
            elif self.rec_hex == "011012000004C4B2":
                self.s_result = 1
                self.error_desc = "指令返回结果正常:时间校准指令发送成功"
            elif self.rec_hex == "01101200000244B0":
                self.s_result = 1
                self.error_desc = "指令返回结果正常:设置运行模式指令发送成功"
            else:
                self.s_result = 0
                self.error_desc = "指令返回结果异常:"+self.rec_hex
    def A01_77H4UY(self,temType):
        self.init_start()        
        if self.s_result == 0:
            pass
        elif temType == 1:
            if len(self.rec_ary) < 5:
                self.s_result = 0
                self.error_desc = "指令返回结果异常:返回结果长度错误"
            elif self.rec_ary[1] == "83":
                self.s_result = 0
                if self.rec_ary[2] == "01":
                    self.error_desc = "指令返回结果异常:非法功能"
                elif self.rec_ary[2] == "02":
                    self.error_desc = "指令返回结果异常:非法数据地址"
                elif self.rec_ary[2] == "03":
                    self.error_desc = "指令返回结果异常:非法数据值"
                elif self.rec_ary[2] == "04":
                    self.error_desc = "指令返回结果异常:从站设备故障"
                elif self.rec_ary[2] == "05":
                    self.error_desc = "指令返回结果异常:从站设备忙"
            elif self.rec_hex == "01101200000104B1":
                self.s_result = 1
                self.error_desc = "指令返回结果正常:控制指令发送成功"
            elif self.rec_hex == "011012000004C4B2":
                self.s_result = 1
                self.error_desc = "指令返回结果正常:时间校准指令发送成功"
            elif self.rec_hex == "01101200000244B0":
                self.s_result = 1
                self.error_desc = "指令返回结果正常:设置运行模式指令发送成功"
            else:
                self.s_result = 0
                self.error_desc = "指令返回结果异常:"+self.rec_hex               