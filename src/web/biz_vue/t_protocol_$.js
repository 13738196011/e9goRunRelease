//按钮事件新增或编辑
var BDC767_type = "";
//其他页面传到本页面参数
var BDC767_param = {};
//暂时没用
var BDC767_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var BDC767_ary_s_state = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];var BDC767_ary_com_port = null;
/*declare select options end*/

//业务逻辑数据开始
function BDC767_t_protocol_biz_start(inputdata) {
	BDC767_param = inputdata;
	layer.close(ly_index);
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_protocol$com_port",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "BDC767_get_N01_t_protocol$com_port");	
    /*biz end*/
}

/*biz step begin*/
function BDC767_format_s_state(value, row, index) {    var objResult = value;    for(i = 0; i < BDC767_ary_s_state.length; i++) {        var obj = BDC767_ary_s_state[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BDC767_format_com_port(value, row, index) {    var objResult = value;    for(i = 0; i < BDC767_ary_com_port.length; i++) {        var obj = BDC767_ary_com_port[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function BDC767_get_N01_t_protocol$com_port(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_protocol$com_port) == false)        return false;    BDC767_ary_com_port = input.N01_t_protocol$com_port;    if($("#BDC767_com_port").is("select") && $("#BDC767_com_port")[0].options.length == 0)    {        $.each(BDC767_ary_com_port, function (i, obj) {            addOptionValue("BDC767_com_port", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    if($("#BDC767_s_state").is("select") && $("#BDC767_s_state")[0].options.length == 0)    {        $.each(BDC767_ary_s_state, function (i, obj) {            addOptionValue("BDC767_s_state", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);        });    }    BDC767_init_t_protocol();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function BDC767_page_end(){
	if(BDC767_param["type"] == "edit"){
		BDC767_get_edit_info();
	}
}

//页面初始化方法
function BDC767_init_t_protocol() {
	//type = getUrlParam("type");
	if(BDC767_param["type"] == "add"){
		$("#BDC767_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
        $("#BDC767_prot_name").val(BDC767_param["prot_name"]);        $("#BDC767_oem_name").val(BDC767_param["oem_name"]);        $("#BDC767_s_state").val(BDC767_param["s_state"]);        $("#BDC767_com_port").val(BDC767_param["com_port"]);		
		/*Get Find Select param end*/	
	}
	else if(BDC767_param["type"] == "edit"){
	
	}
	
	//表单验证
	BDC767_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BDC767_create_date").val() == "")    {        $("#BDC767_create_date").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BDC767_create_date',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BDC767_page_end();
}

//提交表单数据
function BDC767_SubmitForm(){
	if(BDC767_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_protocol",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#BDC767_prot_name").val())                ,"param_value2": s_encode($("#BDC767_oem_name").val())                ,"param_value3": $("#BDC767_create_date").val()                ,"param_value4": s_encode($("#BDC767_s_desc").val())                ,"param_value5": $("#BDC767_s_state").val()                ,"param_value6": s_encode($("#BDC767_com_port").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BDC767_get_N01_ins_t_protocol");
	}
	else if(BDC767_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_protocol",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#BDC767_prot_name").val())                ,"param_value2": s_encode($("#BDC767_oem_name").val())                ,"param_value3": $("#BDC767_create_date").val()                ,"param_value4": s_encode($("#BDC767_s_desc").val())                ,"param_value5": $("#BDC767_s_state").val()                ,"param_value6": s_encode($("#BDC767_com_port").val())                ,"param_value7": $("#BDC767_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BDC767_get_N01_upd_t_protocol");
	}
}

//vue回调
function BDC767_t_protocol_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#BDC767_save_t_protocol_Edit").click(function () {
	$("form[name='BDC767_DataModal']").submit();
})

/*修改数据*/
function BDC767_get_N01_upd_t_protocol(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_protocol) == true)
	{
		swal("修改数据成功!", "", "success");
		ADC767_t_protocol_query();
		BDC767_clear_validate();
		layer.close(BDC767_param["ly_index"]);
	}
}

/*添加数据*/
function BDC767_get_N01_ins_t_protocol(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_protocol) == true)
	{
		swal("添加数据成功!", "", "success");
		ADC767_t_protocol_query();
		BDC767_clear_validate();
		layer.close(BDC767_param["ly_index"]);
	}
}

//取消编辑
$("#BDC767_cancel_t_protocol_Edit").click(function () {
	layer.close(BDC767_param["ly_index"]);
	BDC767_clear_validate();
	$("[id^='BDC767_div']").hide();
})

//清除查找框
function BDC767_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BDC767_clear_validate(){
	$("#BDC767_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BDC767_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BDC767_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BDC767_clear_edit_info(){
	var inputs = $("#BDC767_DataModal").find('input');
	var selects = $("#BDC767_DataModal").find("select");
	var textareas = $("#BDC767_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	BDC767_init_t_protocol();
}

//页面输入框赋值
function BDC767_get_edit_info(){
	var rowData = $("#ADC767_t_protocol_Events").bootstrapTable('getData')[ADC767_select_t_protocol_rowId];
	var inputs = $("#BDC767_DataModal").find('input');
	var selects = $("#BDC767_DataModal").find("select");
	var textareas = $("#BDC767_DataModal").find('textarea');
	$.each(selects, function (i, obj) {
		if(typeof(eval("ADC767_ary_"+obj.id.toString().replace("BDC767_",""))) == "object")
		{
			$.each(eval("ADC767_ary_"+obj.id.toString().replace("BDC767_","")), function (inx, obj_sub) {
				addOptionValue($(obj).id,obj_sub[GetLowUpp("main_id")],obj_sub[GetLowUpp("cn_name")]);
			});
		}
	});
	Object.keys(rowData).forEach(function (key) {
		$.each(inputs, function (i, obj) {
			if (obj.id.toUpperCase() == ("BDC767_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
			else if(obj.id.toUpperCase() == ("BDC767_find_"+key+"_cn_name").toUpperCase()){
				$.each(eval("ADC767_ary_"+key), function (jindex, obj2) {
					if(obj2[GetLowUpp("main_id")] == rowData[key]){
						$("#BDC767_DataModal").find('[id="'+"BDC767_"+("find_"+key+"_cn_name")+'"]').val(obj2[GetLowUpp("cn_name")]);
					}
				});
			}
		});
		$.each(selects, function (i, obj) {
			if (obj.id.toUpperCase() == ("BDC767_"+key).toUpperCase()) {
				$(obj).val(rowData[key]);
			}
		});
		$.each(textareas, function (i, obj) {
			if (obj.id.toUpperCase() == ("BDC767_"+key).toUpperCase()) {
				if(typeof(rowData[key]) == "string")
				{
					$(obj).val(s_decode(rowData[key]));
				}
				else
				{
					$(obj).val(rowData[key]);
				}
			}
		});
	});
}

//form验证
function BDC767_checkFormInput() {
    BDC767_validate = $("#BDC767_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BDC767_main_id: {}            ,BDC767_prot_name: {}            ,BDC767_oem_name: {}            ,BDC767_create_date: {date: true,required : true,maxlength:19}            ,BDC767_s_desc: {}            ,BDC767_s_state: {}            ,BDC767_com_port: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BDC767_main_id: {}            ,BDC767_prot_name: {}            ,BDC767_oem_name: {}            ,BDC767_create_date: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BDC767_s_desc: {}            ,BDC767_s_state: {}            ,BDC767_com_port: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BDC767_SubmitForm();
        	return false;
        }
    })
}