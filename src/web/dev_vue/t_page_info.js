//主从表tab选项卡标志位
var AD67FA_Tab_Flag = -1;
//选择某一行
var AD67FA_select_t_page_info_rowId = "";
//按钮事件新增或编辑
var AD67FA_type = "";
//其他页面传到本页面参数
var AD67FA_param = {};
//table选中数据，如无则默认第一条
var AD67FA_rowCheckData = null;

/*定义查询条件变量*/
/*declare query param begin*/

/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

/*绑定show监听事件*/
if(AD67FA_Tab_Flag == "-1"){
	var n = Get_RandomDiv("AD67FA","");
	$(n).bind("show", function(objTag){
		AD67FA_Adjust_Sub_Sequ();
		if(AD67FA_Tab_Flag > 0)
			AD67FA_adjust_tab();		
	});
}

//设置主从表div顺序
function AD67FA_Adjust_Sub_Sequ(){
	var temSubDivs = $("#content-main").find("div[data-id]");
	var MainDiv = $(Get_RandomDiv("AD67FA",""));
	for(i = 0; i < temSubDivs.length; i++) {
		if(AD67FA_Is_Sub_Div(temSubDivs[i].id)){
			$(temSubDivs[i]).before($(MainDiv));
			break;
		}
	}
}

//主从表传递参数
function AD67FA_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function AD67FA_t_page_info_biz_start(inputparam) {
	layer.close(ly_index);
	AD67FA_param = inputparam;
	//主从表传递参数
	AD67FA_param_set();	
    /*biz begin*/
    AD67FA_init_t_page_info()	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function AD67FA_page_end(){
	AD67FA_adjust_tab();
}

//接口集应用模板显示列定义
var AD67FA_t_page_info = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '模板编号',        field: 'PAGE_CODE',        sortable: true        ,formatter: set_s_decode    },    {        title: '模板描述',        field: 'PAGE_TITLE',        sortable: true        ,formatter: set_s_decode    },    {        title: '扩展字段',        field: 'PAGE_URL',        sortable: true        ,formatter: set_s_decode    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '扩展字段2',        field: 'JS_URL',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function AD67FA_init_t_page_info() {
	$(window).resize(function () {
		  $('#AD67FA_t_page_info_Events').bootstrapTable('resetView');
	});
	//接口集应用模板查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AD67FA_btn_t_page_info_query').click();
}

//查询接口
function AD67FA_t_page_info_query() {
    $('#AD67FA_t_page_info_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_page_info",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AD67FA_get_N01_sel_t_page_info");
}

//查询结果
function AD67FA_get_N01_sel_t_page_info(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_page_info) == false)
        return false;
    AD67FA_rowCheckData = null;
    //调整table各列宽度
    $.each(AD67FA_t_page_info, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_t_page_info;
    if(s_data.length > 0)
    	AD67FA_rowCheckData = s_data[0];    
    AD67FA_select_t_page_info_rowId = "";
    AD67FA_Tab_Flag = 0;
    $('#AD67FA_t_page_info_Events').bootstrapTable('destroy');
    $("#AD67FA_t_page_info_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AD67FA_t_page_info_Events').find("tr.changeColor").removeClass('changeColor');
                AD67FA_select_t_page_info_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AD67FA_t_page_info_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AD67FA_select_t_page_info_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#AD67FA_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AD67FA_param.hasOwnProperty("target_name"))
			{
				$("#"+AD67FA_param["target_id"]).val(eval("row."+AD67FA_param["sourc_id"].toUpperCase()));
				$("#"+AD67FA_param["target_name"]).val(s_decode(eval("row."+AD67FA_param["sourc_name"].toUpperCase())));				
				layer.close(AD67FA_param["ly_index"]);
			}
		},
        toolbar: "#AD67FA_t_page_info_Toolbar",
        columns: AD67FA_t_page_info,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        onPostBody:function(){
        	if(s_data.length != 0)
            	AD67FA_page_end();
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AD67FA_btn_t_page_info_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/

	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/

	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    AD67FA_init_t_page_info()	
    /*biz end*/
})

//查询按钮
$('#AD67FA_btn_t_page_info_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
	
    /*set query param end*/
	AD67FA_t_page_info_query();
})

//vue回调
function AD67FA_t_page_info_call_vue(objResult){
	//选择某条记录或自动选择第一条
	if (AD67FA_select_t_page_info_rowId != "") 
		AD67FA_rowCheckData = $("#AD67FA_t_page_info_Events").bootstrapTable('getData')[AD67FA_select_t_page_info_rowId];
	
	if(index_subhtml == "t_page_info_$.vue")
	{
		var n = Get_RandomDiv("BD67FA",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AD67FA_type,"ly_index":index};
				if(AD67FA_param.hasOwnProperty("hidden_find")){
					inputdata["hidden_find"] = "1";
				}				
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("dev_vue/t_page_info_$.js","BD67FA_t_page_info_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/
    else if(index_subhtml == "t_page_item.vue"){        var n = Get_RandomDiv("A1A6C5",objResult);        $(n).show();        //传递参数        var inputdata = {"hidden_find":"1"};        if(AD67FA_rowCheckData != null){            inputdata["PAGE_ID_cn_name"] = AD67FA_rowCheckData[GetLowUpp("PAGE_CODE")];            inputdata["PAGE_ID"] = AD67FA_rowCheckData[GetLowUpp("MAIN_ID")];        }        else{            inputdata["PAGE_ID_cn_name"] = "";            inputdata["PAGE_ID"] = "-999";        }        loadScript_hasparam("dev_vue/t_page_item.js","A1A6C5_t_page_item_biz_start",inputdata);    }
    else if(index_subhtml == "t_sub_sys.vue"){
        var n = Get_RandomDiv("AE16A0",objResult);
        $(n).show();
        //传递参数
        var inputdata = {};
        loadScript_hasparam("dev_vue/t_sub_sys.js","AE16A0_t_sub_sys_biz_start",inputdata);
    }
    else if(index_subhtml == "t_sub_user.vue"){
        var n = Get_RandomDiv("A013B7",objResult);
        $(n).show();
        //传递参数
        var inputdata = {};
        loadScript_hasparam("dev_vue/t_sub_user.js","A013B7_t_sub_user_biz_start",inputdata);
    }
    else if(index_subhtml == "debug_t_page_info.vue"){
    	var n = Get_RandomDiv("A33541",objResult);	
		layer.open({
			type: 1,
	        area: ['1000px', '800px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
	        	inputdata = {};
	        	loadScript_hasparam("dev_vue/debug_t_page_info.js","A33541_debug_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
    }
	
	
	
	/*get tab subvue end*/
}

//新增按钮
$("#AD67FA_btn_t_page_info_add").click(function () {
	AD67FA_type = "add";
	index_subhtml = "t_page_info_$.vue";
	if(loadHtmlSubVueFun("dev_vue/t_page_info_$.vue","AD67FA_t_page_info_call_vue") == true){
		var n = Get_RandomDiv("BD67FA","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BD67FA_param["type"] = AD67FA_type;
				BD67FA_param["ly_index"]= index;
				if(AD67FA_param.hasOwnProperty("hidden_find")){
					BD67FA_param["hidden_find"] = "1";
				}
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
				
				/*Send Two FindSelect param end*/

				BD67FA_clear_edit_info();
			},
			end: function(){
				BD67FA_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AD67FA_btn_t_page_info_edit").click(function () {
	if (AD67FA_select_t_page_info_rowId != "") {
		AD67FA_type = "edit";
		index_subhtml = "t_page_info_$.vue";
		if(loadHtmlSubVueFun("dev_vue/t_page_info_$.vue","AD67FA_t_page_info_call_vue") == true){
			var n = Get_RandomDiv("BD67FA","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BD67FA_param["type"] = AD67FA_type;
					BD67FA_param["ly_index"] = index;
					if(AD67FA_param.hasOwnProperty("hidden_find")){
						BD67FA_param["hidden_find"] = "1";
					}
					BD67FA_clear_edit_info();
					BD67FA_get_edit_info();
				},
				end: function(){
					BD67FA_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AD67FA_btn_t_page_info_delete').click(function () {
	//单行选择
	var rowData = $("#AD67FA_t_page_info_Events").bootstrapTable('getData')[AD67FA_select_t_page_info_rowId];
	//多行选择
	var rowDatas = AD67FA_sel_row_t_page_info();
	if (AD67FA_select_t_page_info_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_page_info",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AD67FA_N01_del_t_page_info");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_page_info",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AD67FA_N01_del_t_page_info");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AD67FA_N01_del_t_page_info(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_page_info) == true)
		AD67FA_t_page_info_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function AD67FA_adjust_tab(){
	if(typeof($("#AD67FA_TAB_MAIN")[0]) != "undefined" && $("#AD67FA_TAB_MAIN")[0].length != 0){
		AD67FA_Tab_Flag = 1;
		$(Get_RDivNoBuild("AD67FA","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("AD67FA","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("AD67FA","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#AD67FA_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/
$("#AD67FA_tab_1").click(function(){    AD67FA_Tab_Flag = 1;    AD67FA_hide_tab_fun();    //主子表参数传递    var temPar = [{"sourc_id":"MAIN_ID","target_id":"PAGE_ID"},{"sourc_id":"PAGE_CODE","target_id":"PAGE_ID_cn_name"}];    AD67FA_show_tab_fun("t_page_item.vue","A1A6C5",temPar);});

$("#AD67FA_tab_2").click(function(){
    AD67FA_Tab_Flag = 1;
    AD67FA_hide_tab_fun();
    //主子表参数传递
    var temPar = {};
    AD67FA_show_tab_fun("t_sub_user.vue","A013B7",temPar);
});
//隐藏tab页选项卡function AD67FA_hide_tab_fun(){    var n = null;    n = Get_RDivNoBuild("A1A6C5","");    $(n).hide();
    n = Get_RDivNoBuild("A013B7","");
    $(n).hide();}//判断是否sub子项div页面function AD67FA_Is_Sub_Div(temDivId){    if(temDivId.indexOf("XX$TTT") == 0)        return false;    else if(temDivId.indexOf("A1A6C5") == 0)        return true;
    else if(temDivId.indexOf("A013B7") == 0)
        return true;    return false;}
/*Tab Click Fun End*/

function AD67FA_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("dev_vue/"+inputUrl,"AD67FA_t_page_info_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (AD67FA_select_t_page_info_rowId != "") 
			rowData = $("#AD67FA_t_page_info_Events").bootstrapTable('getData')[AD67FA_select_t_page_info_rowId];
		else
			rowData = AD67FA_rowCheckData;
		//$("#AD67FA_t_page_info_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			var temFlag = true;
			$.each(temPar, function (i, obj) {
				if(eval(inputrandom+"_param.hasOwnProperty(\""+obj.target_id+"\")") && eval(inputrandom+"_param[\""+obj.target_id+"\"]").toString() == rowData[obj.sourc_id].toString())
					temFlag = false;
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			if(temFlag){
				//传递子页面隐藏find功能
				eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
				//参数传递并赋值
				eval(inputrandom+"_param_set()");
				var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
				$("#"+inputrandom+"_btn_"+tbName+"_query").click();
			}
		}
		else{
			$.each(temPar, function (i, obj) {
				if(obj.sourc_id.toUpperCase() == "MAIN_ID")
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"-999\"");
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"\"");
			});
			//传递子页面隐藏find功能
			eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
			var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
			$("#"+inputrandom+"_btn_"+tbName+"_query").click();		
		}
	}
}

//接口调试
$("#AD67FA_btn_debug_t_page_info").click(function () {
	index_subhtml = "debug_t_page_info.vue";
	if(loadHtmlSubVueFun("dev_vue/debug_t_page_info.vue","AD67FA_t_page_info_call_vue") == true){
		var n = Get_RandomDiv("A33541","");
		layer.open({
			type: 1,
			area: ['1000px', '800px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				/*Send Two FindSelect param end*/
				A33541_debug_biz_start();
			},
			end: function(){
				$(n).hide();
			}
		});
	}
});

//清除 查找框
function AD67FA_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AD67FA_sel_row_t_page_info(){
	//获得选中行
	var checkedbox= $("#AD67FA_t_page_info_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}