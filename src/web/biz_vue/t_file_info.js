//选择某一行
var AE32DA_select_t_file_info_rowId = "";
//按钮事件新增或编辑
var AE32DA_type = "";
//其他页面传到本页面参数
var AE32DA_param = {};

/*定义查询条件变量*/
/*declare query param begin*/
var AE32DA_tem_file_name = "";var AE32DA_tem_file_type_id = "0";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var AE32DA_ary_file_type_id = null;var AE32DA_ary_is_ava = [{'main_id': '1', 'cn_name': '启用'}, {'main_id': '0', 'cn_name': '禁用'}];
/*declare select options end*/

//业务逻辑数据开始
function AE32DA_t_file_info_biz_start(inputparam) {
	layer.close(ly_index);
	AE32DA_param = inputparam;
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_file_info$file_type_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE32DA_get_N01_t_file_info$file_type_id");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function AE32DA_format_file_type_id(value, row, index) {    var objResult = value;    for(i = 0; i < AE32DA_ary_file_type_id.length; i++) {        var obj = AE32DA_ary_file_type_id[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AE32DA_format_is_ava(value, row, index) {    var objResult = value;    for(i = 0; i < AE32DA_ary_is_ava.length; i++) {        var obj = AE32DA_ary_is_ava[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function AE32DA_get_N01_t_file_info$file_type_id(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_file_info$file_type_id) == false)        return false;    AE32DA_ary_file_type_id = input.N01_t_file_info$file_type_id;    $("#AE32DA_qry_file_type_id").append("<option value='-1'></option>")    $.each(AE32DA_ary_file_type_id, function (i, obj) {        addOptionValue("AE32DA_qry_file_type_id", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    AE32DA_init_t_file_info();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function AE32DA_page_end(){
}

//上传附件表显示列定义
var AE32DA_t_file_info = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '附件类别ID',        field: 'FILE_TYPE_ID',        sortable: true        ,formatter: AE32DA_format_file_type_id    },    {        title: '附件中文名称',        field: 'FILE_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '附件URL',        field: 'FILE_URL',        sortable: true        ,formatter: set_s_decode    },    {        title: '是否启用 ',        field: 'IS_AVA',        sortable: true        ,formatter: AE32DA_format_is_ava    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '系统备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function AE32DA_init_t_file_info() {
	$(window).resize(function () {
		  $('#AE32DA_t_file_info_Events').bootstrapTable('resetView');
	});
	//上传附件表查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#AE32DA_btn_t_file_info_query').click();
}

//查询接口
function AE32DA_t_file_info_query() {
    $('#AE32DA_t_file_info_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_file_info",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": s_encode(AE32DA_tem_file_name)        ,"param_value2": AE32DA_tem_file_type_id        ,"param_value3": AE32DA_tem_file_type_id		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "AE32DA_get_N01_sel_t_file_info");
}

//查询结果
function AE32DA_get_N01_sel_t_file_info(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_file_info) == false)
        return false;    
	var s_data = input.N01_sel_t_file_info;
    AE32DA_select_t_file_info_rowId = "";
    $('#AE32DA_t_file_info_Events').bootstrapTable('destroy');
    $("#AE32DA_t_file_info_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#AE32DA_t_file_info_Events').find("tr.changeColor").removeClass('changeColor');
                AE32DA_select_t_file_info_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#AE32DA_t_file_info_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                AE32DA_select_t_file_info_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(AE32DA_param.hasOwnProperty("target_name"))
			{
				$("#"+AE32DA_param["target_id"]).val(eval("row."+AE32DA_param["sourc_id"].toUpperCase()));
				$("#"+AE32DA_param["target_name"]).val(s_decode(eval("row."+AE32DA_param["sourc_name"].toUpperCase())));				
				layer.close(AE32DA_param["ly_index"]);
			}
		},
        toolbar: "#AE32DA_t_file_info_Toolbar",
        columns: AE32DA_t_file_info,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#AE32DA_btn_t_file_info_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    AE32DA_tem_file_name = "";    AE32DA_tem_file_type_id = "0";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    BE32DA_ary_file_type_id = null;
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_file_info$file_type_id",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "AE32DA_get_N01_t_file_info$file_type_id");	
    /*biz end*/
})

//查询按钮
$('#AE32DA_btn_t_file_info_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    AE32DA_tem_file_name = $("#AE32DA_qry_file_name").val();    AE32DA_tem_file_type_id = $("#AE32DA_qry_file_type_id").val();	
    /*set query param end*/
	AE32DA_t_file_info_query();
})

//vue回调
function AE32DA_t_file_info_call_vue(objResult){
	if(index_subhtml == "t_file_info_$.vue")
	{
		var n = Get_RandomDiv("BE32DA",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":AE32DA_type,"ly_index":index};
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temfile_name = $("#AE32DA_qry_file_name").val();                if(temfile_name != ""){                    inputdata["file_name"] = temfile_name;                }                var temfile_type_id = $("#AE32DA_qry_file_type_id").val();                if(temfile_type_id != ""){                    inputdata["file_type_id"] = temfile_type_id;                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("biz_vue/t_file_info_$.js","BE32DA_t_file_info_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//新增按钮
$("#AE32DA_btn_t_file_info_add").click(function () {
	AE32DA_type = "add";
	index_subhtml = "t_file_info_$.vue";
	if(loadHtmlSubVueFun("biz_vue/t_file_info_$.vue","AE32DA_t_file_info_call_vue") == true){
		var n = Get_RandomDiv("BE32DA","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				BE32DA_param["type"] = AE32DA_type;
				BE32DA_param["ly_index"]= index;
				
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temfile_name = $("#AE32DA_qry_file_name").val();                if(temfile_name != ""){                    BE32DA_param["file_name"] = temfile_name;                }                var temfile_type_id = $("#AE32DA_qry_file_type_id").val();                if(temfile_type_id != ""){                    BE32DA_param["file_type_id"] = temfile_type_id;                }				
				/*Send Two FindSelect param end*/

				BE32DA_clear_edit_info();
			},
			end: function(){
				BE32DA_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#AE32DA_btn_t_file_info_edit").click(function () {
	if (AE32DA_select_t_file_info_rowId != "") {
		AE32DA_type = "edit";
		index_subhtml = "t_file_info_$.vue";
		if(loadHtmlSubVueFun("biz_vue/t_file_info_$.vue","AE32DA_t_file_info_call_vue") == true){
			var n = Get_RandomDiv("BE32DA","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					BE32DA_param["type"] = AE32DA_type;
					BE32DA_param["ly_index"] = index;
					BE32DA_clear_edit_info();
					BE32DA_get_edit_info();
				},
				end: function(){
					BE32DA_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#AE32DA_btn_t_file_info_delete').click(function () {
	//单行选择
	var rowData = $("#AE32DA_t_file_info_Events").bootstrapTable('getData')[AE32DA_select_t_file_info_rowId];
	//多行选择
	var rowDatas = AE32DA_sel_row_t_file_info();
	if (AE32DA_select_t_file_info_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_file_info",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "AE32DA_N01_del_t_file_info");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_file_info",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "AE32DA_N01_del_t_file_info");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function AE32DA_N01_del_t_file_info(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_file_info) == true)
		AE32DA_t_file_info_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//清除 查找框
function AE32DA_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function AE32DA_sel_row_t_file_info(){
	//获得选中行
	var checkedbox= $("#AE32DA_t_file_info_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}