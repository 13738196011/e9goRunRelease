//按钮事件新增或编辑
var B35691_type = "";
//其他页面传到本页面参数
var B35691_param = {};
//暂时没用
var B35691_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/
var B35691_ary_IS_AVA = [{'MAIN_ID': '1', 'CN_NAME': '启用'}, {'MAIN_ID': '0', 'CN_NAME': '禁用'}];var B35691_ary_ROLE_ID = [{'MAIN_ID': '1', 'CN_NAME': '开发者'}, {'MAIN_ID': '2', 'CN_NAME': '系统管理员'}, {'MAIN_ID': '3', 'CN_NAME': '运维人员'}, {'MAIN_ID': '4', 'CN_NAME': '普通用户'}];
/*declare select options end*/

//主从表传递参数
function B35691_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function B35691_menu_group_biz_start(inputdata) {
	B35691_param = inputdata;
	layer.close(ly_index);
	B35691_param_set();
    /*biz begin*/
    $.each(B35691_ary_IS_AVA, function (i, obj) {        addOptionValue("B35691_IS_AVA", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    $.each(B35691_ary_ROLE_ID, function (i, obj) {        addOptionValue("B35691_ROLE_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });    B35691_init_menu_group()	
    /*biz end*/
}

/*biz step begin*/
function B35691_format_IS_AVA(value, row, index) {    var objResult = value;    for(i = 0; i < B35691_ary_IS_AVA.length; i++) {        var obj = B35691_ary_IS_AVA[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function B35691_format_ROLE_ID(value, row, index) {    var objResult = value;    for(i = 0; i < B35691_ary_ROLE_ID.length; i++) {        var obj = B35691_ary_ROLE_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function B35691_page_end(){
	$('#B35691_ROLE_ID').selectpicker('refresh'); 
	$('#B35691_ROLE_ID').selectpicker('show');
	if(B35691_param["type"] == "edit"){
		B35691_get_edit_info();
	}
}

//页面初始化方法
function B35691_init_menu_group() {
	//type = getUrlParam("type");
	if(B35691_param["type"] == "add"){
		$("#B35691_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
		
		/*Get Find Select param end*/	
	}
	else if(B35691_param["type"] == "edit"){
	
	}
	
	//表单验证
	B35691_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#B35691_CREATE_DATE").val() == "")    {        $("#B35691_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#B35691_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	B35691_page_end();
}

//提交表单数据
function B35691_SubmitForm(){
	if(B35691_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_menu_group",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#B35691_GROUP_EN_NAME").val())                ,"param_value2": s_encode($("#B35691_GROUP_CN_NAME").val())                ,"param_value3": s_encode($("#B35691_GROUP_ICON").val())                ,"param_value4": $("#B35691_IS_AVA").val()                ,"param_value5": s_encode($("#B35691_ROLE_ID").val())                ,"param_value6": $("#B35691_CREATE_DATE").val()                ,"param_value7": s_encode($("#B35691_S_DESC").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "B35691_get_N01_ins_menu_group");
	}
	else if(B35691_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_menu_group",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#B35691_GROUP_EN_NAME").val())                ,"param_value2": s_encode($("#B35691_GROUP_CN_NAME").val())                ,"param_value3": s_encode($("#B35691_GROUP_ICON").val())                ,"param_value4": $("#B35691_IS_AVA").val()                ,"param_value5": s_encode($("#B35691_ROLE_ID").val())                ,"param_value6": $("#B35691_CREATE_DATE").val()                ,"param_value7": s_encode($("#B35691_S_DESC").val())                ,"param_value8": $("#B35691_MAIN_ID").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "B35691_get_N01_upd_menu_group");
	}
}

//vue回调
function B35691_menu_group_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#B35691_save_menu_group_Edit").click(function () {
	$("form[name='B35691_DataModal']").submit();
})

/*修改数据*/
function B35691_get_N01_upd_menu_group(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_menu_group) == true)
	{
		swal("修改数据成功!", "", "success");
		A35691_menu_group_query();
		B35691_clear_validate();
		layer.close(B35691_param["ly_index"]);
	}
}

/*添加数据*/
function B35691_get_N01_ins_menu_group(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_menu_group) == true)
	{
		swal("添加数据成功!", "", "success");
		A35691_menu_group_query();
		B35691_clear_validate();
		layer.close(B35691_param["ly_index"]);
	}
}

//取消编辑
$("#B35691_cancel_menu_group_Edit").click(function () {
	layer.close(B35691_param["ly_index"]);
	B35691_clear_validate();
	$("[id^='B35691_div']").hide();
})

//清除查找框
function B35691_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function B35691_clear_validate(){
	$("#B35691_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#B35691_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#B35691_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function B35691_clear_edit_info(){
	var inputs = $("#B35691_DataModal").find('input');
	var selects = $("#B35691_DataModal").find("select");
	var textareas = $("#B35691_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	B35691_init_menu_group();
}

//页面输入框赋值
function B35691_get_edit_info(){
	var rowData = $("#A35691_menu_group_Events").bootstrapTable('getData')[A35691_select_menu_group_rowId];
	var inputs = $("#B35691_DataModal").find('input');
	var selects = $("#B35691_DataModal").find("select");
	var textareas = $("#B35691_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"A35691","B35691");
	
	//多选框赋值
	$.each(selects, function (i, obj) {
		if (obj.id.toUpperCase() == "B35691_ROLE_ID") {
			var arr = s_decode(rowData["ROLE_ID"]).split(',');
			$(obj).selectpicker('val', arr);
		}
	});
}

//form验证
function B35691_checkFormInput() {
    B35691_validate = $("#B35691_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            B35691_main_id: {}            ,B35691_GROUP_EN_NAME: {}            ,B35691_GROUP_CN_NAME: {}            ,B35691_GROUP_ICON: {}            ,B35691_IS_AVA: {}            ,B35691_ROLE_ID: {}            ,B35691_CREATE_DATE: {date: true,required : true,maxlength:19}            ,B35691_S_DESC: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            B35691_main_id: {}            ,B35691_GROUP_EN_NAME: {}            ,B35691_GROUP_CN_NAME: {}            ,B35691_GROUP_ICON: {}            ,B35691_IS_AVA: {}            ,B35691_ROLE_ID: {}            ,B35691_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,B35691_S_DESC: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	B35691_SubmitForm();
        	return false;
        }
    })
}