#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class V_VTC001(object):
    attMAIN_ID = -1
    attTABLE_CN_NAME = ""
    attTABLE_EN_NAME = ""
    attCOLUMN_CN_NAME = ""
    attCOLUMN_EN_NAME = ""
    attCOLUMN_TYPE = ""
    lenTABLE_CN_NAME = 50
    lenTABLE_EN_NAME = 50
    lenCOLUMN_CN_NAME = 50
    lenCOLUMN_EN_NAME = 50
    lenCOLUMN_TYPE = 500
    def __init__(self):
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["main_id"] = "主键"
        temHt["MAIN_ID"] = "字段主键"
        temHt["TABLE_CN_NAME"] = "表中文名"
        temHt["TABLE_EN_NAME"] = "表英文名"
        temHt["COLUMN_CN_NAME"] = "字段中文名"
        temHt["COLUMN_EN_NAME"] = "字段英文名"
        temHt["COLUMN_TYPE"] = "字段类型"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        temHtQry["TABLE_CN_NAME"] = "表中文名"
        temHtQry["TABLE_EN_NAME"] = "表英文名"
        temHtQry["COLUMN_CN_NAME"] = "字段中文名"
        temHtQry["COLUMN_EN_NAME"] = "字段英文名"
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["COLUMN_TYPE"] = [{"MAIN_ID":"1","CN_NAME":"STRING"},{"MAIN_ID":"2","CN_NAME":"INT"},{"MAIN_ID":"3","CN_NAME":"FLOAT"},{"MAIN_ID":"4","CN_NAME":"DATE"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
