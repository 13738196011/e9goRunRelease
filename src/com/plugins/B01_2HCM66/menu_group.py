#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class menu_group(object):
    attMAIN_ID = -1
    attGROUP_EN_NAME = ""
    attGROUP_CN_NAME = ""
    attGROUP_ICON = ""
    attIS_AVA = 0
    attROLE_ID = ""
    attCREATE_DATE = None
    attS_DESC = ""
    lenGROUP_EN_NAME = 50
    lenGROUP_CN_NAME = 50
    lenGROUP_ICON = 50
    lenROLE_ID = 500
    lenS_DESC = 500
    def __init__(self):
        self.attCREATE_DATE = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["MAIN_ID"] = "主键"
        temHt["GROUP_EN_NAME"] = "菜单组英文名"
        temHt["GROUP_CN_NAME"] = "菜单组中文名"
        temHt["GROUP_ICON"] = "菜单组图标"
        temHt["IS_AVA"] = "是否启用"
        temHt["ROLE_ID"] = "角色"
        temHt["CREATE_DATE"] = "系统时间"
        temHt["S_DESC"] = "备注"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["IS_AVA"] = [{"MAIN_ID":"1","CN_NAME":"启用"},{"MAIN_ID":"0","CN_NAME":"禁用"}]
        temHtSelect["ROLE_ID"] = [{"MAIN_ID":"1","CN_NAME":"开发者"},{"MAIN_ID":"2","CN_NAME":"系统管理员"},{"MAIN_ID":"3","CN_NAME":"运维人员"},{"MAIN_ID":"4","CN_NAME":"普通用户"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        return temHtFReturn
