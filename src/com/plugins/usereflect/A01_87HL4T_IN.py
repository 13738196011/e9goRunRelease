#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import time
import hmac
import hashlib
import base64
import urllib.parse
import json   #导入依赖库
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.z_debug import z_debug
class A01_87HL4T_IN(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = 1
    def __init__(self):
        pass
    def init_start(self):
        timestamp = str(round(time.time() * 1000))#'1629810258724'
        app_secret = 'SEC299cf1be687d974ec1d2e66b4daa429a9822d46d4f09de1096c9a616cba74dbe'
        app_secret_enc = app_secret.encode('utf-8')
        string_to_sign = '{}\n{}'.format(timestamp, app_secret)
        string_to_sign_enc = string_to_sign.encode('utf-8')
        hmac_code = hmac.new(app_secret_enc, string_to_sign_enc, digestmod=hashlib.sha256).digest()
        sign = base64.b64encode(hmac_code).decode('utf-8')
        headers={'Content-Type': 'application/json'}#定义数据类型
        webhook = 'https://oapi.dingtalk.com/robot/send?access_token=051d6889a0069ece1dc98ac75aa34949f04fb83dba2e7ad8c15c4eb4279562d2&timestamp='+timestamp+"&sign="+sign
        data = {"msgtype": "text","text": {"content": "能源报警："+self.param_value1},"isAtAll": True}
        res = Com_Fun.PostHttpHeader(webhook, json.dumps(data).encode("utf-8"), headers=headers)
        self.strResult = res
        self.strContinue = 0