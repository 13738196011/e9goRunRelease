#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import base64,os
from Crypto.Cipher import AES
class EncodeUtil(object):
    attKey    = "?lffzxy002@2016&!;"
    def __init__(self):
        pass
    @staticmethod
    def add_to_16(inputText):
        while len(inputText) % 16 != 0:
            inputText += '\0'
        return str.encode(inputText)  # 返回bytes
    @staticmethod
    def pad(text):
        count = len(text.encode('utf-8'))
        add = AES.block_size - (count % AES.block_size)
        entext = text + (chr(add) * add)
        return entext
    @staticmethod
    def aesEncrypt_2(inputContent,inputEncryptKey):   
        temAes = AES.new(EncodeUtil.add_to_16(inputEncryptKey), AES.MODE_ECB)  # 初始化加密器
        temResult2 = temAes.encrypt(EncodeUtil.pad(inputContent).encode("utf8"))    
        return temResult2
    @staticmethod
    def aesEncrypt(inputContent,inputEncryptKey):   
        temAes = AES.new(EncodeUtil.add_to_16(inputEncryptKey), AES.MODE_ECB)  # 初始化加密器
        temResult = str(base64.encodebytes(temAes.encrypt(EncodeUtil.add_to_16(inputContent))), encoding='utf8').replace('\n', '')
        return temResult
    @staticmethod
    def aesDecrypt(inputContent,inputEncryptKey): 
        temAes = AES.new(EncodeUtil.add_to_16(inputEncryptKey), AES.MODE_ECB)  # 初始化加密器
        temResult = str(temAes.decrypt(base64.decodebytes(bytes(inputContent, encoding='utf8'))).rstrip(b'\0').decode("utf8"))  # 解密
        return temResult