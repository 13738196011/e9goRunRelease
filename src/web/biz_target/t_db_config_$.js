var type = "";

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

//业务逻辑数据开始
function biz_start() {
	layer.close(ly_index);
    /*biz begin*/
    init_t_db_config()	
    /*biz end*/
}

/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function page_end(){
}


//页面初始化方法
function init_t_db_config() {
	type = getUrlParam("type");
	if(type == "add"){
		$("#main_id").parent().parent().parent().hide();
	}
	else if(type == "edit"){
	
	}
	
	//表单验证
	checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#CREATE_DATE").val() == "")    {        $("#CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
}

//提交表单数据
function SubmitForm(){
	if(type == "add"){
		var inputdata = {
				"param_name": "T01_ins_t_db_config",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#DB_CN_NAME").val())                ,"param_value2": s_encode($("#DB_DriverClassName").val())                ,"param_value3": s_encode($("#DB_url").val())                ,"param_value4": s_encode($("#DB_username").val())                ,"param_value5": s_encode($("#DB_password").val())                ,"param_value6": s_encode($("#DB_version").val())                ,"param_value7": s_encode($("#DB_Code").val())                ,"param_value8": s_encode($("#S_DESC").val())                ,"param_value9": $("#CREATE_DATE").val()				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_ins_t_db_config");
	}
	else if(type == "edit"){
		var inputdata = {
				"param_name": "T01_upd_t_db_config",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#DB_CN_NAME").val())                ,"param_value2": s_encode($("#DB_DriverClassName").val())                ,"param_value3": s_encode($("#DB_url").val())                ,"param_value4": s_encode($("#DB_username").val())                ,"param_value5": s_encode($("#DB_password").val())                ,"param_value6": s_encode($("#DB_version").val())                ,"param_value7": s_encode($("#DB_Code").val())                ,"param_value8": s_encode($("#S_DESC").val())                ,"param_value9": $("#CREATE_DATE").val()                ,"param_value10": $("#main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "get_T01_upd_t_db_config");
	}
}

//以下为页面表单验证
$("#save_t_db_config_Edit").click(function () {
	$("form[name='DataModal']").submit();
})

/*修改数据*/
function get_T01_upd_t_db_config(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_upd_t_db_config) == true)
	{
        //init();
		parent.swal("修改数据成功!", "", "success");
		parent.t_db_config_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

/*添加数据*/
function get_T01_ins_t_db_config(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.T01_ins_t_db_config) == true)
	{
		parent.swal("添加数据成功!", "", "success");
		parent.t_db_config_query();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
}

//取消编辑
$("#cancel_t_db_config_Edit").click(function () {
	var index = parent.layer.getFrameIndex(window.name);
	parent.layer.close(index);
})

function clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

$(document).ready(function () {
});

window.onpageshow = function (e) {
    if (e.persisted || (window.performance.navigation.type == 2)) {
        is_history_back = 1;
    } else {
        is_history_back = 0;
    }
    //页面初始化
    init_page();
};

//form验证
function checkFormInput() {
    $("#DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            main_id: {}            ,DB_CN_NAME: {}            ,DB_DriverClassName: {}            ,DB_url: {}            ,DB_username: {}            ,DB_password: {}            ,DB_version: {}            ,DB_Code: {}            ,S_DESC: {}            ,CREATE_DATE: {date: true,required : true,maxlength:19}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            main_id: {}            ,DB_CN_NAME: {}            ,DB_DriverClassName: {}            ,DB_url: {}            ,DB_username: {}            ,DB_password: {}            ,DB_version: {}            ,DB_Code: {}            ,S_DESC: {}            ,CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	SubmitForm();
        	return false;
        }
    })
}