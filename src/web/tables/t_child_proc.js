﻿var iTop = 0;
var ileft = 0;
var ly_index;

function biz_start(){
}

//子设备接口信息
var t_child_proc = [{
        label: '主键',
        name: 'MAIN_ID',
        width: '30px',
        index: 'MAIN_ID',
        editable: false,
        key: true,
        readOnly: true,
        editrules: {
            required: true
        }
    }, {
        label: '子设备主键',
        name: 'CHILD_ID',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '子设备接口中文名',
        name: 'INF_CN_NAME',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '子设备接口英文名',
        name: 'INF_EN_NAME',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '备注',
        name: 'S_DESC',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '系统时间',
        name: 'CREATE_DATE',
        width: '70px',
        editable: true,
        editrules: true,
        readOnly: true,
        formatter: function (value, row) {
            return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
        },
        editoptions: {
            defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')
        }
    }
];

function T01_sel_t_child_proc(input) {
    data = input.T01_sel_t_child_proc;
	layer.close(ly_index);
    //查询失败
    if (Call_QryResult(data) == false)
        return false;
	
    $("#table_list_1").jqGrid({
        data: data,
        datatype: "local",
        height: "450",
        autowidth: true,
        shrinkToFit: true,
        rowNum: 10,
        rowList: [10, 20, 30],
        colModel: t_child_proc,
        editurl: 'clientArray',
        altRows: true,
        pager: "#pager_list_1",
        viewrecords: true,
        caption: "子设备接口列表"
    });
    //导航后面，增加增删改查按钮
    $("#table_list_1").jqGrid("navGrid", "#pager_list_1", {
        edit: true,
        add: true,
        del: true,
        search: true,
        view: false,
        position: "left",
        cloneToTop: false
    }, {
        beforeShowForm: function () {
            $('#MAIN_ID').attr('readOnly', true);
            laydate.render({
                elem: '#CREATE_DATE',
                type: 'datetime'
            });
        },
        onclickSubmit: updateDbByMainId,
        closeAfterEdit: true
    }, {
        beforeShowForm: function () {
            laydate.render({
                elem: '#CREATE_DATE',
                type: 'datetime'
            });
        },
        onclickSubmit: insertDb,
        closeAfterAdd: true
    }, {
        top: iTop,
        left: ileft,
        onclickSubmit: delDbByMainId
    }, {
        closeAfterSearch: true
    }, {
        height: 200,
        reloadAfterSubmit: true
    });
}

//初始化
function init() {
    iTop = (winHeight - 300) / 2;
    ileft = (winWidth - 300) / 2;
    $.jgrid.defaults.styleUI = "Bootstrap";
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_t_child_proc",
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_child_proc",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

$(document).ready(function () {
    init_page();
    getWindowSize();
    init();
    $(window).bind("resize",
        function () {
        var width = $(".jqGrid_wrapper").width();
        $("#table_list_1").setGridWidth(width);
    });
});

function T01_ins_t_child_proc(input) {
    data = input.T01_ins_t_child_proc;
	layer.close(ly_index);
    //查询失败
    if (Call_OpeResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
}

var insertDb = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_ins_t_child_proc",
            session_id: session_id,
            param_value1: $("#CHILD_ID").val(),
            param_value2: $("#INF_CN_NAME").val(),
            param_value3: $("#INF_EN_NAME").val(),
            param_value4: $("#S_DESC").val(),
            param_value5: $("#CREATE_DATE").val()
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_ins_t_child_proc",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_upd_t_child_proc(input) {
    data = input.T01_upd_t_child_proc;
	layer.close(ly_index);
    //查询失败
    if (Call_OpeResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
}

var updateDbByMainId = function () {
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_upd_t_child_proc",
            session_id: session_id,
            param_value1: $("#CHILD_ID").val(),
            param_value2: $("#INF_CN_NAME").val(),
            param_value3: $("#INF_EN_NAME").val(),
            param_value4: $("#S_DESC").val(),
            param_value5: $("#CREATE_DATE").val(),
            param_value6: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_upd_t_child_proc",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_del_t_child_proc(input) {
    data = input.T01_del_t_child_proc;
	layer.close(ly_index);
    //查询失败
    if (Call_OpeResult(data) == false)
        return false;
    $("#table_list_1").trigger("reloadGrid");
}

var delDbByMainId = function () {
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_del_t_child_proc",
            session_id: session_id,
            param_value1: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_del_t_child_proc",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};