//主从表tab选项卡标志位
var A1A6C5_Tab_Flag = -1;
//选择某一行
var A1A6C5_select_t_page_item_rowId = "";
//按钮事件新增或编辑
var A1A6C5_type = "";
//其他页面传到本页面参数
var A1A6C5_param = {};
//table选中数据，如无则默认第一条
var A1A6C5_rowCheckData = null;

/*定义查询条件变量*/
/*declare query param begin*/
var A1A6C5_tem_PAGE_ID = "0";var A1A6C5_tem_PROC_ID = "-1";
/*declare query param end*/

/*定义下拉框集合定义*/
/*declare select options begin*/
var A1A6C5_ary_PAGE_ID = null;var A1A6C5_ary_PROC_ID = null;var A1A6C5_ary_ITEM_TYPE = [{'MAIN_ID': '1', 'CN_NAME': '表格模式'}, {'MAIN_ID': '2', 'CN_NAME': '列表模式'}];
/*declare select options end*/

/*绑定show监听事件*/
if(A1A6C5_Tab_Flag == "-1"){
	var n = Get_RandomDiv("A1A6C5","");
	$(n).bind("show", function(objTag){
		A1A6C5_Adjust_Sub_Sequ();
		if(A1A6C5_Tab_Flag > 0)
			A1A6C5_adjust_tab();		
	});
}

//设置主从表div顺序
function A1A6C5_Adjust_Sub_Sequ(){
	var temSubDivs = $("#content-main").find("div[data-id]");
	var MainDiv = $(Get_RandomDiv("A1A6C5",""));
	for(i = 0; i < temSubDivs.length; i++) {
		if(A1A6C5_Is_Sub_Div(temSubDivs[i].id)){
			$(temSubDivs[i]).before($(MainDiv));
			break;
		}
	}
}

//主从表传递参数
function A1A6C5_param_set(){
	/*Main Subsuv Table Param Begin*/
    if(A1A6C5_param.hasOwnProperty("PROC_ID_cn_name"))        $("#A1A6C5_find_PROC_ID_cn_name").val(s_decode(A1A6C5_param["PROC_ID_cn_name"]));    if(A1A6C5_param.hasOwnProperty("PROC_ID"))        $("#A1A6C5_find_PROC_ID").val(A1A6C5_param["PROC_ID"]);    if(A1A6C5_param.hasOwnProperty("hidden_find")){        $("#A1A6C5_Ope_PROC_ID").hide();        $("#A1A6C5_Clear_PROC_ID").hide();    }
    if(A1A6C5_param.hasOwnProperty("PAGE_ID"))
        $("#A1A6C5_qry_PAGE_ID").val(A1A6C5_param["PAGE_ID"]);
    if(A1A6C5_param.hasOwnProperty("hidden_find")){
        $("#A1A6C5_qry_PAGE_ID").attr("disabled", true);
    }
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function A1A6C5_t_page_item_biz_start(inputparam) {
	layer.close(ly_index);
	A1A6C5_param = inputparam;
	//主从表传递参数
	A1A6C5_param_set();	
    /*biz begin*/
    var inputdata = {        "param_name": "N01_t_page_item$PAGE_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A1A6C5_get_N01_t_page_item$PAGE_ID");	
    /*biz end*/
}

/*业务函数步骤*/
/*biz step begin*/
function A1A6C5_format_PAGE_ID(value, row, index) {    var objResult = value;    for(i = 0; i < A1A6C5_ary_PAGE_ID.length; i++) {        var obj = A1A6C5_ary_PAGE_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A1A6C5_format_PROC_ID(value, row, index) {    var objResult = value;    for(i = 0; i < A1A6C5_ary_PROC_ID.length; i++) {        var obj = A1A6C5_ary_PROC_ID[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A1A6C5_format_ITEM_TYPE(value, row, index) {    var objResult = value;    for(i = 0; i < A1A6C5_ary_ITEM_TYPE.length; i++) {        var obj = A1A6C5_ary_ITEM_TYPE[i];        if (obj[GetLowUpp("main_id")].toString() == value.toString()) {            objResult = obj[GetLowUpp("cn_name")];            break;        }    }    return objResult;}function A1A6C5_get_N01_t_page_item$PAGE_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_page_item$PAGE_ID) == false)        return false;    A1A6C5_ary_PAGE_ID = input.N01_t_page_item$PAGE_ID;    var inputdata = {        "param_name": "N01_t_proc_inparam$PROC_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A1A6C5_get_N01_t_proc_inparam$PROC_ID");}function A1A6C5_get_N01_t_proc_inparam$PROC_ID(input) {    layer.close(ly_index);    //查询失败    if (Call_QryResult(input.N01_t_proc_inparam$PROC_ID) == false)        return false;    A1A6C5_ary_PROC_ID = input.N01_t_proc_inparam$PROC_ID;    $("#A1A6C5_qry_PAGE_ID").append("<option value='-1'></option>")    $.each(A1A6C5_ary_PAGE_ID, function (i, obj) {        addOptionValue("A1A6C5_qry_PAGE_ID", obj[GetLowUpp("main_id")], obj[GetLowUpp("cn_name")]);    });
    if(A1A6C5_param.hasOwnProperty("PAGE_ID"))
        $("#A1A6C5_qry_PAGE_ID").val(A1A6C5_param["PAGE_ID"]);
        A1A6C5_init_t_page_item();}
/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/
function A1A6C5_PROC_ID_cn_name_fun(){    index_subhtml = "v_selproc.vue"    random_subhtml = "";    if(loadHtmlSubVueFun("dev_vue/v_selproc.vue","A1A6C5_t_page_item_call_vue") == true){        var n = Get_RandomDiv("","");        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                $('#C772A0_v_selproc_name_Events').bootstrapTable('resetView');                C772A0_param["ly_index"] = index;                C772A0_param["target_name"] = "A1A6C5_find_PROC_ID_cn_name"                C772A0_param["target_id"] = "A1A6C5_find_PROC_ID"                C772A0_param["sourc_id"] = "MAIN_ID"                C772A0_param["sourc_name"] = "INF_CN_NAME"            },            end: function(){                $(n).hide();            }        });     }}
/*find qry fun end*/

/*页面结束*/
function A1A6C5_page_end(){
	A1A6C5_adjust_tab();
}

//接口集模板子项显示列定义
var A1A6C5_t_page_item = [
	{ 
		checkbox:true
	},
	/*table column begin*/
    {        title: '主键',        field: 'MAIN_ID',        sortable: true,        visible: false    },    {        title: '接口集应用模板',        field: 'PAGE_ID',        sortable: true        ,formatter: A1A6C5_format_PAGE_ID    },    {        title: '接口名称',        field: 'PROC_ID',        sortable: true        ,formatter: A1A6C5_format_PROC_ID    },    {        title: '子项名称',        field: 'ITEM_NAME',        sortable: true        ,formatter: set_s_decode    },    {        title: '子项类别',        field: 'ITEM_TYPE',        sortable: true        ,formatter: A1A6C5_format_ITEM_TYPE    },    {        title: '统计列数',        field: 'COLUMN_COUNT',        sortable: true    },    {        title: '系统时间',        field: 'CREATE_DATE',        sortable: true        ,formatter: set_time_decode    },    {        title: '备注',        field: 'S_DESC',        sortable: true        ,formatter: set_s_decode    },    {        title: '输入参数',        field: 'URL_IN_PARAM',        sortable: true        ,formatter: set_s_decode    }	
	/*table column end*/
];

//页面初始化
function A1A6C5_init_t_page_item() {
	$(window).resize(function () {
		  $('#A1A6C5_t_page_item_Events').bootstrapTable('resetView');
	});
	//接口集模板子项查询条件初始化设置
	/*query conditions init begin*/
	
    /*query conditions init end*/
	
	$('#A1A6C5_btn_t_page_item_query').click();
}

//查询接口
function A1A6C5_t_page_item_query() {
    $('#A1A6C5_t_page_item_Events').bootstrapTable("removeAll");
	//以下为特殊字段列表查询，无特殊字段时不需要
	var inputdata = {
		"param_name": "N01_sel_t_page_item",
		"session_id": session_id,
		"login_id": login_id
		/*传递查询条件变量*/
        /*get query param begin*/
        ,"param_value1": A1A6C5_tem_PAGE_ID        ,"param_value2": A1A6C5_tem_PAGE_ID        ,"param_value3": A1A6C5_tem_PROC_ID        ,"param_value4": A1A6C5_tem_PROC_ID		
        /*get query param end*/
	};
	ly_index = layer.load();
	get_ajax_baseurl(inputdata, "A1A6C5_get_N01_sel_t_page_item");
}

//查询结果
function A1A6C5_get_N01_sel_t_page_item(input) {
	layer.close(ly_index);
	//查询失败
    if (Call_QryResult(input.N01_sel_t_page_item) == false)
        return false;
    A1A6C5_rowCheckData = null;
    //调整table各列宽度
    $.each(A1A6C5_t_page_item, function (i, obj) {
		if(obj.title != undefined){
			obj.width = obj.title.length * 14 + 37;
		}
	});
    
	var s_data = input.N01_sel_t_page_item;
    if(s_data.length > 0)
    	A1A6C5_rowCheckData = s_data[0];    
    A1A6C5_select_t_page_item_rowId = "";
    A1A6C5_Tab_Flag = 0;
    $('#A1A6C5_t_page_item_Events').bootstrapTable('destroy');
    $("#A1A6C5_t_page_item_Events").bootstrapTable({
        uniqueId: 'main_id',
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        onClickRow: function (row, $element) {
            // 判断是否已选中
            if ($($element).hasClass("changeColor")) {
                $('#A1A6C5_t_page_item_Events').find("tr.changeColor").removeClass('changeColor');
                A1A6C5_select_t_page_item_rowId = "";
            }
            else {
                // 未点击则，为当前行添加 class='changeColor'
                $('#A1A6C5_t_page_item_Events').find("tr.changeColor").removeClass('changeColor');
                $($element).addClass('changeColor');                　
                A1A6C5_select_t_page_item_rowId = $element.attr('data-index');
                
                //设置子表查询条件
                /*set child table qry begin*/
                
                $($("#A1A6C5_TAB_MAIN").find(".active")[0]).click();
                /*set child table qry end*/
            }
        },
		onDblClickRow: function (row){
			if(A1A6C5_param.hasOwnProperty("target_name"))
			{
				$("#"+A1A6C5_param["target_id"]).val(eval("row."+A1A6C5_param["sourc_id"].toUpperCase()));
				$("#"+A1A6C5_param["target_name"]).val(s_decode(eval("row."+A1A6C5_param["sourc_name"].toUpperCase())));				
				layer.close(A1A6C5_param["ly_index"]);
			}
		},
        toolbar: "#A1A6C5_t_page_item_Toolbar",
        columns: A1A6C5_t_page_item,
        data: s_data,
        pageNumber: 1,
        pageSize: 10, // 每页的记录行数（*）
        pageList: [10,50,100,1000,2000], // 可供选择的每页的行数（*）
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        onPostBody:function(){
        	if(s_data.length != 0)
            	A1A6C5_page_end();
        }
    });
    
    //子表数据查询动作
    /*child table data begin*/
    
    /*child table data end*/
}

//刷新按钮
$('#A1A6C5_btn_t_page_item_refresh').click(function () {
	/*重置查询条件变量*/
	/*refresh query param begin*/
    A1A6C5_tem_PAGE_ID = "0";    A1A6C5_tem_PROC_ID = "-1";
	/*refresh query param end*/

	/*重置下拉框集合定义*/
	/*refresh select options begin*/
    B1A6C5_ary_PAGE_ID = null;    B1A6C5_ary_PROC_ID = null;    $("#A1A6C5_find_PROC_ID_cn_name").val("");    $("#A1A6C5_find_PROC_ID").val("-1");
	/*refresh select options end*/
	
	/*页面重置重新加载*/
	/*biz begin*/
    var inputdata = {        "param_name": "N01_t_page_item$PAGE_ID",        "session_id": session_id,        "login_id": login_id    };    ly_index = layer.load();    get_ajax_baseurl(inputdata, "A1A6C5_get_N01_t_page_item$PAGE_ID");	
    /*biz end*/
})

//查询按钮
$('#A1A6C5_btn_t_page_item_query').click(function () {
	/*设置查询条件变量*/
    /*set query param begin*/
    A1A6C5_tem_PAGE_ID = $("#A1A6C5_qry_PAGE_ID").val();    A1A6C5_tem_PROC_ID = $("#A1A6C5_find_PROC_ID").val();	
    /*set query param end*/
	A1A6C5_t_page_item_query();
})

//vue回调
function A1A6C5_t_page_item_call_vue(objResult){
	//选择某条记录或自动选择第一条
	if (A1A6C5_select_t_page_item_rowId != "") 
		A1A6C5_rowCheckData = $("#A1A6C5_t_page_item_Events").bootstrapTable('getData')[A1A6C5_select_t_page_item_rowId];
	
	if(index_subhtml == "t_page_item_$.vue")
	{
		var n = Get_RandomDiv("B1A6C5",objResult);	
		layer.open({
			type: 1,
	        area: ['1100px', '600px'],
	        fixed: false, //不固定
	        maxmin: true,
	        content: $(n),
	        success: function(layero, index){
				var inputdata = {"type":A1A6C5_type,"ly_index":index};
				if(A1A6C5_param.hasOwnProperty("hidden_find")){
					inputdata["hidden_find"] = "1";
				}				
				/*查询条件参数传递至子页面,初次加载*/
				/*Send One FindSelect param bgein*/
                var temPAGE_ID = $("#A1A6C5_qry_PAGE_ID").val();                if(temPAGE_ID != ""){                    inputdata["PAGE_ID"] = temPAGE_ID;                }                var temPROC_ID = $("#A1A6C5_find_PROC_ID_cn_name").val();                if(temPROC_ID != ""){                    inputdata["PROC_ID_cn_name"] = temPROC_ID;                    inputdata["PROC_ID"] = $("#A1A6C5_PROC_ID").val();                }					
				/*Send One FindSelect param end*/
				
	        	loadScript_hasparam("dev_vue/t_page_item_$.js","B1A6C5_t_page_item_biz_start",inputdata);
	        },
			end: function(){
				$(n).hide();
			}
	    });
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
    else if(index_subhtml == "v_selproc.vue"){        var n = Get_RandomDiv("",objResult);        layer.open({            type: 1,            area: ['1100px', '600px'],            fixed: false, //不固定            maxmin: true,            content: $(n),            success: function(layero, index){                var inputdata = {                    "type":A1A6C5_type,                    "ly_index":index,                    "target_name":"A1A6C5_find_PROC_ID_cn_name",                    "target_id":"A1A6C5_find_PROC_ID",                    "sourc_id":"MAIN_ID",                    "sourc_name":"INF_CN_NAME"                };                loadScript_hasparam("dev_vue/v_selproc.js","C772A0_v_selproc_biz_start",inputdata);            },            end: function(){                $(n).hide();            }        });    }	
	/*get find subvue end*/
	
	/*tab页显示子页面*/
	/*get tab subvue begin*/

	/*get tab subvue end*/
}

//新增按钮
$("#A1A6C5_btn_t_page_item_add").click(function () {
	A1A6C5_type = "add";
	index_subhtml = "t_page_item_$.vue";
	if(loadHtmlSubVueFun("dev_vue/t_page_item_$.vue","A1A6C5_t_page_item_call_vue") == true){
		var n = Get_RandomDiv("B1A6C5","");
		layer.open({
			type: 1,
			area: ['1100px', '600px'],
			fixed: false, //不固定
			maxmin: true,
			content: $(n),
			success: function(layero, index){
				B1A6C5_param["type"] = A1A6C5_type;
				B1A6C5_param["ly_index"]= index;
				if(A1A6C5_param.hasOwnProperty("hidden_find")){
					B1A6C5_param["hidden_find"] = "1";
				}
				/*查询条件参数传递至子页面,再次加载*/
			    /*Send Two FindSelect param bgein*/
                var temPAGE_ID = $("#A1A6C5_qry_PAGE_ID").val();                if(temPAGE_ID != ""){                    B1A6C5_param["PAGE_ID"] = temPAGE_ID;                }                var temPROC_ID = $("#A1A6C5_find_PROC_ID_cn_name").val();                if(temPROC_ID != ""){                    B1A6C5_param["PROC_ID_cn_name"] = temPROC_ID;                    B1A6C5_param["PROC_ID"] = $("#A1A6C5_PROC_ID").val();                }				
				/*Send Two FindSelect param end*/

				B1A6C5_clear_edit_info();
			},
			end: function(){
				B1A6C5_clear_validate();
				$(n).hide();
			}
		});
	}
})

//编辑按钮
$("#A1A6C5_btn_t_page_item_edit").click(function () {
	if (A1A6C5_select_t_page_item_rowId != "") {
		A1A6C5_type = "edit";
		index_subhtml = "t_page_item_$.vue";
		if(loadHtmlSubVueFun("dev_vue/t_page_item_$.vue","A1A6C5_t_page_item_call_vue") == true){
			var n = Get_RandomDiv("B1A6C5","");
			layer.open({
				type: 1,
				area: ['1100px', '600px'],
				fixed: false, //不固定
				maxmin: true,
				content: $(n),
				success: function(layero, index){
					B1A6C5_param["type"] = A1A6C5_type;
					B1A6C5_param["ly_index"] = index;
					if(A1A6C5_param.hasOwnProperty("hidden_find")){
						B1A6C5_param["hidden_find"] = "1";
					}
					B1A6C5_clear_edit_info();
					B1A6C5_get_edit_info();
				},
				end: function(){
					B1A6C5_clear_validate();
					$(n).hide();
				}
			});
		}
	} else {
		swal({
            title: "提示信息",
            text: "无法修改记录，请选择正确记录修改!"
        });
    }
})

//删除按钮
$('#A1A6C5_btn_t_page_item_delete').click(function () {
	//单行选择
	var rowData = $("#A1A6C5_t_page_item_Events").bootstrapTable('getData')[A1A6C5_select_t_page_item_rowId];
	//多行选择
	var rowDatas = A1A6C5_sel_row_t_page_item();
	if (A1A6C5_select_t_page_item_rowId != "" || rowDatas.length > 0) {
    	swal({
            title: "告警",
            text: "确认要删除吗?删除数据将无法恢复!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
        },
		function () {
        	if(rowDatas.length > 0)
        	{
	        	$.each(rowDatas, function (i, obj) {
					var delete_main_id = obj.MAIN_ID;
					var inputdata = {
						"param_name": "N01_del_t_page_item",
						"session_id": session_id,
						"login_id": login_id,
						"param_value1": delete_main_id
					};
					ly_index = layer.load();
					get_ajax_baseurl(inputdata, "A1A6C5_N01_del_t_page_item");
	        	});
        	}
        	else
        	{
        		var delete_main_id = rowData["MAIN_ID"];
				var inputdata = {
					"param_name": "N01_del_t_page_item",
					"session_id": session_id,
					"login_id": login_id,
					"param_value1": delete_main_id
				};
				ly_index = layer.load();
				get_ajax_baseurl(inputdata, "A1A6C5_N01_del_t_page_item");
        	}
		}); 
    } else {
    	swal({
            title: "提示信息",
            text: "无法删除记录，请选择正确记录删除!"
        });
    }
})

//删除结果
function A1A6C5_N01_del_t_page_item(input) {
	layer.close(ly_index);
	if(Call_OpeResult(input.N01_del_t_page_item) == true)
		A1A6C5_t_page_item_query();
}

//特殊字符串数据解码
function set_s_decode(value, row, index) {
    return s_decode(value);
}

//时间数据解码
function set_time_decode(value, row, index) {
  var timeResult = s_decode(value);
  return timeResult.replace("T"," ");
}

//主从表选项卡动态添加
function A1A6C5_adjust_tab(){
	if(typeof($("#A1A6C5_TAB_MAIN")[0]) != "undefined" && $("#A1A6C5_TAB_MAIN")[0].length != 0){
		A1A6C5_Tab_Flag = 1;
		$(Get_RDivNoBuild("A1A6C5","")).find(".wrapper-content").css("padding","20px 20px 0px 20px");
		$(Get_RDivNoBuild("A1A6C5","")).find(".ibox").css("margin-bottom","0px");
		$(Get_RDivNoBuild("A1A6C5","")).find(".ibox-content").css("padding","15px 20px 0px");		
		$($("#A1A6C5_TAB_MAIN").find(".active")[0]).click();
	}
}

/*tab选项卡按钮点击事件*/
/*Tab Click Fun Begin*/
//隐藏tab页选项卡function A1A6C5_hide_tab_fun(){    var n = null;}//判断是否sub子项div页面function A1A6C5_Is_Sub_Div(temDivId){    if(temDivId.indexOf("XX$TTT") == 0)        return false;    return false;}
/*Tab Click Fun End*/

function A1A6C5_show_tab_fun(inputUrl,inputrandom,temPar){
	index_subhtml = inputUrl;
	random_subhtml = inputrandom;
	if(loadHtmlSubVueFun("dev_vue/"+inputUrl,"A1A6C5_t_page_item_call_vue") == true){
		var n = Get_RandomDiv(inputrandom,"");
		$(n).show();
		var rowData = null;
		//选择某条记录或自动选择第一条并传递参数
		if (A1A6C5_select_t_page_item_rowId != "") 
			rowData = $("#A1A6C5_t_page_item_Events").bootstrapTable('getData')[A1A6C5_select_t_page_item_rowId];
		else
			rowData = A1A6C5_rowCheckData;
		//$("#A1A6C5_t_page_item_Events").bootstrapTable('getData')[0];
		if(rowData != null){
			var temFlag = true;
			$.each(temPar, function (i, obj) {
				if(eval(inputrandom+"_param.hasOwnProperty(\""+obj.target_id+"\")") && eval(inputrandom+"_param[\""+obj.target_id+"\"]").toString() == rowData[obj.sourc_id].toString())
					temFlag = false;
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \""+rowData[obj.sourc_id]+"\"");
			});
			if(temFlag){
				//传递子页面隐藏find功能
				eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
				//参数传递并赋值
				eval(inputrandom+"_param_set()");
				var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
				$("#"+inputrandom+"_btn_"+tbName+"_query").click();
			}
		}
		else{
			$.each(temPar, function (i, obj) {
				if(obj.sourc_id.toUpperCase() == "MAIN_ID")
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"-999\"");
				else
					eval(inputrandom+"_param[\""+obj.target_id+"\"] = \"\"");
			});
			//传递子页面隐藏find功能
			eval(inputrandom+"_param[\"hidden_find\"] = \"1\"");
			//参数传递并赋值
			eval(inputrandom+"_param_set()");
			var tbName = inputUrl.substring(0,inputUrl.indexOf(".vue"));		
			$("#"+inputrandom+"_btn_"+tbName+"_query").click();		
		}
	}
}

//清除 查找框
function A1A6C5_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//选择多行数据进行业务操作
function A1A6C5_sel_row_t_page_item(){
	//获得选中行
	var checkedbox= $("#A1A6C5_t_page_item_Events").bootstrapTable('getSelections'); 
	//将选中行数据转成jsonStr
	var jsonStr=JSON.stringify(checkedbox);
	//将jsonStr转成jsonObject对象	 
	var jsonObject=jQuery.parseJSON(jsonStr);
	return jsonObject;
	//接着就可以遍历jsonObject数组对象，取出并操作数据
	//alert(jsonObject);
}