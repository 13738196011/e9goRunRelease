#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_IHGDW0 import A01_Para
class monitor_flag_model(object):
    main_id = -1
    station_code = ""
    get_time = None
    val_01 = ""
    val_02 = ""
    val_03 = ""
    val_04 = ""
    val_05 = ""
    val_06 = ""
    val_07 = ""
    val_08 = ""
    val_09 = ""
    val_10 = ""
    val_11 = ""
    val_12 = ""
    val_13 = ""
    val_14 = ""
    val_15 = ""
    val_16 = ""
    val_17 = ""
    val_18 = ""
    val_19 = ""
    val_20 = ""
    val_21 = ""
    val_22 = ""
    val_23 = ""
    val_24 = ""
    val_25 = ""
    val_26 = ""
    val_27 = ""
    val_28 = ""
    val_29 = ""
    val_30 = ""
    val_31 = ""
    val_32 = ""
    val_33 = ""
    val_34 = ""
    val_35 = ""
    val_36 = ""
    val_37 = ""
    val_38 = ""
    val_39 = ""
    val_40 = ""
    update_time = None
    m_flag = 0
    m_flag1 = 0
    m_flag2 = 0
    m_flag3 = 0
    m_flag4 = 0
    m_flag5 = 0
    m_flag6 = 0
    m_flag7 = 0
    m_flag8 = 0
    m_flag9 = 0
    m_flag10 = 0
    def __init__(self):
        self.get_time = Com_Fun.GetTimeNum(Com_Fun.GetTime("%Y-%m-%d %H:%M:%S"))
        self.update_time = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")  
        self.station_code = Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "station_code")      