#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import time,json,importlib,binascii
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_IHGDW0.A01_Fun import A01_Fun
from com.plugins.A01_IHGDW0.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_IHGDW0 import A01_Para
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.common import Com_Para
from com.zxy.db2.Db_Common2 import Db_Common2
from com.zxy.comport.ComModBus import ComModBus
from com.plugins.A01_IHGDW0.monitor_data_model import monitor_data_model
from openpyxl.descriptors.base import Length
from com.zxy.z_debug import z_debug
class A01_OX9TJ2_Time(z_debug):
    strContinue      = "1"   
    strResult        = ""
    def __init__(self):
        pass
    def init_start(self):
        try:            
            temprotocol = A01_Fun.Get_protocol("A01_OX9TJ2")
            if temprotocol is not None:
                comPort = "-N"
                red = None   
                temmonitor_data_model = A01_Fun.Get_monitor_sec()
                temmonitor_flag_model = A01_Fun.Get_monitor_flag_sec()
                a01011 = 0 
                for temv_prot_element in A01_Para.ht_v_prot_element:       
                    if temv_prot_element.attprot_name == "A01_OX9TJ2":                        
                        temIndex = -1
                        data_val_int = 0
                        data_val = ""
                        if red is None : 
                            temA01modbus = ComModBus()
                            comPort = A01_Fun.Get_portcom(temprotocol.attmain_id)
                            inputByte1 = "2330310D"
                            temByt = temA01modbus.get_data_com_hex_flag(comPort,inputByte1,"\r")
                            for temv_t_modbus_addr in A01_Para.ht_t_modbus_addr: 
                                if temv_t_modbus_addr.attcom_port == comPort and str(temv_t_modbus_addr.attelement_id) == str(temv_prot_element.attmain_id):
                                    temIndex = int(temv_t_modbus_addr.attmodbus_begin)
                                    break
                            if temByt is not None:
                                red = A01_Fun.GetBytToAry(temByt)
                                iMinV = 1.0
                                iMaxV = 5.0
                                if Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "A01_OX9TJ2_MINV"+str(temIndex)) != "":
                                    iMinV = float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "A01_OX9TJ2_MINV"+str(temIndex)))
                                elif Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "A01_OX9TJ2_MINV") != "":
                                    iMinV = float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "A01_OX9TJ2_MINV"))
                                if Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "A01_OX9TJ2_MAXV"+str(temIndex)) != "":
                                    iMaxV = float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "A01_OX9TJ2_MAXV"+str(temIndex)))
                                elif Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "A01_OX9TJ2_MAXV") != "":
                                    iMaxV = float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "A01_OX9TJ2_MAXV"))
                                data_val_int = A01_Fun.ModToDigt(float(red[temIndex]),iMaxV,iMinV,float(temv_prot_element.attprot_up_limit),float(temv_prot_element.attprot_down_limit))
                            else:
                                return None
                        else:
                            for temv_t_modbus_addr in A01_Para.ht_t_modbus_addr: 
                                if temv_t_modbus_addr.attcom_port == comPort and str(temv_t_modbus_addr.attelement_id) == str(temv_prot_element.attmain_id):
                                    temIndex = int(temv_t_modbus_addr.attmodbus_begin)
                                    break
                            iMinV = 1.0
                            iMaxV = 5.0
                            if Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "A01_OX9TJ2_MINV"+str(temIndex)) != "":
                                iMinV = float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "A01_OX9TJ2_MINV"+str(temIndex)))
                            elif Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "A01_OX9TJ2_MINV") != "":
                                iMinV = float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "A01_OX9TJ2_MINV"))
                            if Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "A01_OX9TJ2_MAXV"+str(temIndex)) != "":
                                iMaxV = float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "A01_OX9TJ2_MAXV"+str(temIndex)))
                            elif Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "A01_OX9TJ2_MAXV") != "":
                                iMaxV = float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "A01_OX9TJ2_MAXV"))
                            data_val_int = A01_Fun.ModToDigt(float(red[temIndex]),iMaxV,iMinV,float(temv_prot_element.attprot_up_limit),float(temv_prot_element.attprot_down_limit))                          
                        if temv_prot_element.attprot_up_limit != "" and temv_prot_element.attprot_down_limit != "":
                            data_val = str(round(data_val_int,temv_prot_element.attdot_num))
                            if temv_prot_element.attelement_code == "a01011":
                                a01011 = data_val_int
                            elif temv_prot_element.attelement_code == "w00000" and float(data_val) <= 0:
                                data_val = "0"
                            elif temv_prot_element.attelement_code == "w20116" and float(data_val) <= 0:
                                data_val = "0"
                        if hasattr(temmonitor_data_model, temv_prot_element.attelement_val.lower()) and data_val != "":
                            setattr(temmonitor_data_model, temv_prot_element.attelement_val.lower(),data_val)                        
                        if hasattr(temmonitor_flag_model, temv_prot_element.attelement_val.lower()):
                            setattr(temmonitor_flag_model, temv_prot_element.attelement_val.lower(),"N")                                
                    elif temv_prot_element.attelement_code == "a05002" or temv_prot_element.attelement_code == "a24087" or temv_prot_element.attelement_code == "a24088" or temv_prot_element.attelement_code == "a25002" or temv_prot_element.attelement_code == "a25003" or temv_prot_element.attelement_code == "a25005"  or temv_prot_element.attelement_code == "a24087" :               
                        varVT = getattr(A01_Fun.Get_monitor_flag_sec_last(),temv_prot_element.attelement_val.lower())
                        if varVT != "":
                            setattr(temmonitor_flag_model, temv_prot_element.attelement_val.lower(),varVT)    
                for temv_prot_element in A01_Para.ht_v_prot_element:
                    if temv_prot_element.attelement_code == "a00000" and a01011 > 0:
                        a00000 = str(round(a01011 * float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "speed_ratio")) * float(Com_Fun.GetHashTable(A01_Para.ht_t_param_value, "area_measure")) ,temv_prot_element.attdot_num))
                        if hasattr(temmonitor_data_model, temv_prot_element.attelement_val.lower()):
                            setattr(temmonitor_data_model, temv_prot_element.attelement_val.lower(),a00000)                        
                        if hasattr(temmonitor_flag_model, temv_prot_element.attelement_val.lower()):
                            setattr(temmonitor_flag_model, temv_prot_element.attelement_val.lower(),"N")
                        break; 
                if A01_Para.attLock.acquire():
                    Com_Fun.SetHashTable(A01_Para.ht_monitor_sec, temmonitor_data_model.get_time, temmonitor_data_model)
                    Com_Fun.SetHashTable(A01_Para.ht_monitor_flag_sec, temmonitor_flag_model.get_time, temmonitor_flag_model)
                    A01_Para.attLock.release()                
        except Exception as e:
            if str(type(self)) == "<class 'type'>":
                self.debug_in(self,repr(e))#打印异常信息
            else:
                self.debug_in(repr(e))#打印异常信息
        finally:
            pass