#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''
import json,os,importlib
from com.zxy.common import Com_Para
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.adminlog.UsAdmin_Log import UsAdmin_Log
from com.zxy.z_debug import z_debug
class T01_JKTFSA_OUT(z_debug):
    strResult        = ""
    session_id       = ""
    param_name       = ""
    param_value1     = None
    param_value2     = None
    param_value3     = None
    param_value4     = None
    param_value5     = None
    param_value6     = None
    param_value7     = None
    param_value8     = None
    param_value9     = None
    param_value10    = None
    strContinue      = 1
    def __init__(self):
        pass
    def init_start(self):
        try:
            temSession = Com_Fun.GetHashTableNone(Com_Para.htSession,self.session_id)
            jso = json.loads(self.strResult)
            jsoAry = jso[self.param_name]
            temFlag = False
            temSessionRoleID = json.loads(temSession[0])["SESSION_INFO"][0][Com_Fun.GetLowUpp("rid")]
            nSub = 0
            for i in range(0,len(jsoAry)):
                jsoObject = jsoAry[i - nSub]
                temFlag = False
                temRoleids= str(Com_Fun.py_urldecode(jsoObject["ROLE_ID"])).split(",")
                for temRole in temRoleids:
                    if str(temRole) == str(temSessionRoleID):
                        temFlag = True
                        break
                if temFlag == False:
                    jsoAry.remove(jsoObject)
                    nSub = nSub + 1
            jso[self.param_name] = jsoAry
            self.strResult = json.dumps(jso,ensure_ascii=False)
        except Exception as e:
            temLog = ""
            if str(type(self)) == "<class 'type'>":
                temLog = self.debug_info(self)+repr(e)
            else:
                temLog = self.debug_info()+repr(e)
            uL = UsAdmin_Log(Com_Para.ApplicationPath, temLog)
            uL.WriteLog()
        self.strContinue = "0"
