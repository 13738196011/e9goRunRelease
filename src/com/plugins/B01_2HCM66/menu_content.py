#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: auto
'''

from com.zxy.common.Com_Fun import Com_Fun
class menu_content(object):
    attMAIN_ID = -1
    attMENU_ID = 0
    attIS_AVA = 0
    attROLE_ID = ""
    lenROLE_ID = 500
    def __init__(self):
        pass
    def Get_Att_CN(self):
        temHt = {}
        temHt["MAIN_ID"] = "主键"
        temHt["MENU_ID"] = "菜单url"
        temHt["IS_AVA"] = "是否启用"
        temHt["ROLE_ID"] = "角色"
        return temHt
    def Qry_Att_CN(self):
        temHtQry = {}
        return temHtQry
    def Select_Att_CN(self):
        temHtSelect = {}
        temHtSelect["MENU_ID"] = "select MAIN_ID,menu_name || menu_url as CN_NAME from menu_info"
        temHtSelect["IS_AVA"] = [{"MAIN_ID":"1","CN_NAME":"启用"},{"MAIN_ID":"0","CN_NAME":"禁用"}]
        temHtSelect["ROLE_ID"] = [{"MAIN_ID":"1","CN_NAME":"开发者"},{"MAIN_ID":"2","CN_NAME":"系统管理员"},{"MAIN_ID":"3","CN_NAME":"运维人员"},{"MAIN_ID":"4","CN_NAME":"普通用户"}]
        return temHtSelect
    def Find_Att_CN(self):
        temHtFind = {}
        temHtFind["MENU_ID"] = "menu_info.vue"
        return temHtFind
    def FReturn_Att_CN(self):
        temHtFReturn = {}
        temHtFReturn["MENU_ID"] = ["MAIN_ID","MENU_NAME"]
        return temHtFReturn
