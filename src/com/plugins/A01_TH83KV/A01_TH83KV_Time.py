#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import datetime
from com.plugins.A01_IHGDW0.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_LXNOQU.A01_LXNOQU_Time import A01_LXNOQU_Time
from com.plugins.A01_IHGDW0 import A01_Para
from com.plugins.A01_R790KS.A01_R790KS_Time import A01_R790KS_Time
from com.plugins.A01_T8MN36.A01_T8MN36_Time import A01_T8MN36_Time
from com.plugins.A01_IHGDW0.monitor_data_model import monitor_data_model
from com.zxy.common.Com_Fun import Com_Fun
from com.zxy.z_debug import z_debug
class A01_TH83KV_Time(z_debug):
    strContinue      = "1"   
    strResult        = ""
    def __init__(self):
        pass
    def init_start(self):
        if A01_Para.objUpHourFlag == True:
            A01_Para.objUpHourFlag = False
            try:
                bod = Bus_Ope_DB_Cent()
                temmonitor_hour_dataList = bod.get_noupload_data_hour()
                temmonitor_flag_dataList = bod.get_noupload_flag_hour()
                temmonitor_max_dataList = bod.get_noupload_max_hour()
                for temmonitor_hour_model in temmonitor_hour_dataList:
                    temmonitor_flag_model = None
                    temmonitor_max_model = None
                    for temflag in temmonitor_flag_dataList:
                        if temflag.get_time == temmonitor_hour_model.get_time:
                            temmonitor_flag_model = temflag
                            break
                    for temmax in temmonitor_max_dataList:
                        if temmax.get_time == temmonitor_hour_model.get_time:
                            temmonitor_max_model = temmax
                            break
                    for temv_server_protocol in A01_Para.ht_v_server_protocol:
                        if temv_server_protocol.attprot_name == "A01_LXNOQU" and int(getattr(temmonitor_hour_model, "m_flag"+str(temv_server_protocol.atts_desc))) == 0:
                            a01_time = A01_LXNOQU_Time()
                            a01_time.strResult = self.strResult
                            a01_time.strIP = temv_server_protocol.attserver_ip
                            a01_time.strPort = temv_server_protocol.attserver_port
                            iR = a01_time.SendC17(temmonitor_hour_model,temmonitor_flag_model,temmonitor_max_model)
                            if iR > 0:
                                bod.set_upload_hour_flag(temmonitor_hour_model,"m_flag"+str(temv_server_protocol.atts_desc))
                        elif temv_server_protocol.attprot_name == "A01_R790KS" and int(getattr(temmonitor_hour_model, "m_flag"+str(temv_server_protocol.atts_desc))) == 0:
                            a01_time = A01_R790KS_Time()
                            a01_time.strResult = self.strResult
                            a01_time.strIP = temv_server_protocol.attserver_ip
                            a01_time.strPort = temv_server_protocol.attserver_port
                            iR = a01_time.SendC17(temmonitor_hour_model,temmonitor_flag_model,temmonitor_max_model)
                            if iR > 0:
                                bod.set_upload_hour_flag(temmonitor_hour_model,"m_flag"+str(temv_server_protocol.atts_desc))
                        elif temv_server_protocol.attprot_name == "A01_T8MN36" and int(getattr(temmonitor_hour_model, "m_flag"+str(temv_server_protocol.atts_desc))) == 0:
                            a01_time = A01_T8MN36_Time()
                            a01_time.strResult = self.strResult
                            a01_time.strIP = temv_server_protocol.attserver_ip
                            a01_time.strPort = temv_server_protocol.attserver_port
                            iR = a01_time.SendC17(temmonitor_hour_model,temmonitor_flag_model,temmonitor_max_model)
                            if iR > 0:
                                bod.set_upload_hour_flag(temmonitor_hour_model,"m_flag"+str(temv_server_protocol.atts_desc))
                if int(Com_Fun.GetTime("%M")) <= 0:
                    bodc = Bus_Ope_DB_Cent()
                    temmonitor_data_model = monitor_data_model()
                    temmonitor_data_model.get_time = Com_Fun.DateTimeAdd(datetime.datetime.now(),"d",-1).strftime("%Y-%m-%d %H:%M:%S")
                    if bodc.get_day_had(temmonitor_data_model) == False:
                        temhourmonitor_data_model = bodc.ins_data_day(temmonitor_data_model)
                    temmonitor_day_dataList = bod.get_noupload_data_day()
                    for temmonitor_day_model in temmonitor_day_dataList:                    
                        temmonitor_flag_model = bod.get_noupload_flag_day(temmonitor_day_model)
                        temmonitor_max_model = bod.get_noupload_max_day(temmonitor_day_model)
                        for temv_server_protocol in A01_Para.ht_v_server_protocol:
                            if temv_server_protocol.attprot_name == "A01_LXNOQU" and int(getattr(temmonitor_day_model, "m_flag"+str(temv_server_protocol.atts_desc))) == 0:
                                a01_time = A01_LXNOQU_Time()
                                a01_time.strResult = self.strResult
                                a01_time.strIP = temv_server_protocol.attserver_ip
                                a01_time.strPort = temv_server_protocol.attserver_port
                                iR = a01_time.SendC18(temmonitor_day_model,temmonitor_flag_model,temmonitor_max_model)
                                if iR > 0:
                                    bod.set_upload_day_flag(temmonitor_day_model,"m_flag"+str(temv_server_protocol.atts_desc))
                            elif temv_server_protocol.attprot_name == "A01_R790KS" and int(getattr(temmonitor_day_model, "m_flag"+str(temv_server_protocol.atts_desc))) == 0:
                                a01_time = A01_R790KS_Time()
                                a01_time.strResult = self.strResult
                                a01_time.strIP = temv_server_protocol.attserver_ip
                                a01_time.strPort = temv_server_protocol.attserver_port
                                iR = a01_time.SendC18(temmonitor_day_model,temmonitor_flag_model,temmonitor_max_model)
                                if iR > 0:
                                    bod.set_upload_day_flag(temmonitor_day_model,"m_flag"+str(temv_server_protocol.atts_desc))
                            elif temv_server_protocol.attprot_name == "A01_T8MN36" and int(getattr(temmonitor_day_model, "m_flag"+str(temv_server_protocol.atts_desc))) == 0:
                                a01_time = A01_T8MN36_Time()
                                a01_time.strResult = self.strResult
                                a01_time.strIP = temv_server_protocol.attserver_ip
                                a01_time.strPort = temv_server_protocol.attserver_port
                                iR = a01_time.SendC18(temmonitor_day_model,temmonitor_flag_model,temmonitor_max_model)
                                if iR > 0:
                                    bod.set_upload_day_flag(temmonitor_day_model,"m_flag"+str(temv_server_protocol.atts_desc))
            except Exception as e:
                if str(type(self)) == "<class 'type'>":
                    self.debug_in(self,repr(e))#打印异常信息
                else:
                    self.debug_in(repr(e))#打印异常信息
            A01_Para.objUpHourFlag = True