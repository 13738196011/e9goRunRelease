#! python3
# -*- coding: utf-8 -
'''
Created on 2017年05月10日
@author: zxyong 13738196011
'''

import hashlib
from com.zxy.common import Com_Para
class EncryptUtil(object):
    def __init__(self):
        pass
    @staticmethod
    def getMd5(inputValue):
        temValue = hashlib.md5()
        temValue.update(inputValue.encode(encoding=Com_Para.U_CODE))
        return temValue.hexdigest().upper()
    @staticmethod
    def getSha1(inputValue):
        temValue = hashlib.sha1()
        temValue.update(inputValue.encode(encoding=Com_Para.U_CODE))
        return temValue.hexdigest().upper()
    @staticmethod
    def getSM32(inputValue):
        return EncryptUtil.getSha1(EncryptUtil.getMd5(inputValue))[0:32]