#! python3
# -*- coding: utf-8 -
'''
Created on 2020年05月10日
@author: zxyong 13738196011
'''

import time,json,struct,urllib.parse
from com.zxy.common.Com_Fun import Com_Fun
from com.plugins.A01_C3T8H8.Bus_Ope_DB_Cent import Bus_Ope_DB_Cent
from com.plugins.A01_C3T8H8.A01_C3T8H8_MODBUS import A01_C3T8H8_MODBUS
from com.zxy.db3.Db_Common3 import Db_Common3
from com.zxy.common import Com_Para
from com.zxy.z_debug import z_debug
class A01_C3T8H8_Time(z_debug):
    strContinue      = "1"   
    strResult        = ""
    def __init__(self):
        pass
    def init_start(self):
        bodc = Bus_Ope_DB_Cent()
        bodc.init_page()    
        temt_station_info = bodc.Get_t_station_info()
        temdbc3 = Db_Common3()
        temA01modbus = A01_C3T8H8_MODBUS()
        for tsi in temt_station_info:
            tbMonth = "z_w_cond_"+Com_Fun.GetTime("%Y%m")
            get_time = Com_Fun.GetTime("%Y-%m-%d %H:%M:00")
            update_time = Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")
            inputValues = {}                         
            inputValues["param_value1"] = tsi.attstation_code
            inputValues["param_value2"] = Com_Fun.py_urlencode(get_time)
            temt_modbus_addr = bodc.Get_t_modbus_addr_by_station_id(str(tsi.attmain_id))
            all_value = ""
            iIndex = 0
            red = temA01modbus.get_data_rtu(temt_modbus_addr[0].attcom_port,1, 0, 50)
            for tmda in temt_modbus_addr:
                strSqlInsert = "insert into "
                strSqlValues = "values "
                strSqlInsert = strSqlInsert + tbMonth+"(element_id,station_type,cond_value,get_time,create_date,is_ava) "               
                iIndex = iIndex + 1
                data_val = (float)(red[tmda.attmodbus_begin])
                iDev = 10
                if iIndex <= 3:
                    iDev = 10
                elif iIndex <=6 :
                    iDev = 100
                else:
                    iDev = 1000                    
                all_value = data_val / iDev                
                strSqlValues = strSqlValues+"('"+tmda.attelement_val+"','"+tsi.attstation_code+"','"+str(all_value)+"','"+get_time+"','"+Com_Fun.GetTime("%Y-%m-%d %H:%M:%S")+"','0')"    
                temdbc3.CommonExec_Sql(strSqlInsert+strSqlValues)
                time.sleep(0.5)