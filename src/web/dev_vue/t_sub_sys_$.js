//按钮事件新增或编辑
var BE16A0_type = "";
//其他页面传到本页面参数
var BE16A0_param = {};
//暂时没用
var BE16A0_validate = "";

/*定义下拉框集合定义*/
/*declare select options begin*/

/*declare select options end*/

//主从表传递参数
function BE16A0_param_set(){
	/*Main Subsuv Table Param Begin*/
	
	/*Main Subsuv Table Param end*/
}

//业务逻辑数据开始
function BE16A0_t_sub_sys_biz_start(inputdata) {
	BE16A0_param = inputdata;
	layer.close(ly_index);
	BE16A0_param_set();
    /*biz begin*/
    BE16A0_init_t_sub_sys()	
    /*biz end*/
}

/*biz step begin*/

/*biz step end*/

/*查找框函数*/
/*find qry fun begin*/

/*find qry fun end*/

/*页面结束*/
function BE16A0_page_end(){
	if(BE16A0_param["type"] == "edit"){
		BE16A0_get_edit_info();
	}
}

//页面初始化方法
function BE16A0_init_t_sub_sys() {
	//type = getUrlParam("type");
	if(BE16A0_param["type"] == "add"){
		$("#BE16A0_main_id").parent().parent().parent().hide();
		
		/*父页面查询条件参数传递至子页面并赋值*/
		/*Get Find Select param bgein*/
		
		/*Get Find Select param end*/	
	}
	else if(BE16A0_param["type"] == "edit"){
	
	}
	
	//表单验证
	BE16A0_checkFormInput();
	/*时间格式初始化*/
	/*form datetime init begin*/
    if($("#BE16A0_LIMIT_DATE").val() == "")    {        $("#BE16A0_LIMIT_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BE16A0_LIMIT_DATE',        type: 'datetime',        trigger: 'click'    });    if($("#BE16A0_CREATE_DATE").val() == "")    {        $("#BE16A0_CREATE_DATE").val(new Date().Format('yyyy-MM-dd hh:mm:ss'));    }    laydate.render({        elem: '#BE16A0_CREATE_DATE',        type: 'datetime',        trigger: 'click'    });	
    /*form datetime init end*/
	
	BE16A0_page_end();
}

//提交表单数据
function BE16A0_SubmitForm(){
	if(BE16A0_param["type"] == "add"){
		var inputdata = {
				"param_name": "N01_ins_t_sub_sys",
				"session_id": session_id,
				"login_id": login_id
	            /*insert param begin*/
                ,"param_value1": s_encode($("#BE16A0_SUB_NAME").val())                ,"param_value2": s_encode($("#BE16A0_SUB_CODE").val())                ,"param_value3": $("#BE16A0_LIMIT_DATE").val()                ,"param_value4": s_encode($("#BE16A0_S_DESC").val())                ,"param_value5": $("#BE16A0_CREATE_DATE").val()                ,"param_value6": s_encode($("#BE16A0_IP_ADDR").val())				
	            /*insert param end*/
			};
		get_ajax_baseurl(inputdata, "BE16A0_get_N01_ins_t_sub_sys");
	}
	else if(BE16A0_param["type"] == "edit"){
		var inputdata = {
				"param_name": "N01_upd_t_sub_sys",
				"session_id": session_id,
				"login_id": login_id
	            /*update param begin*/
                ,"param_value1": s_encode($("#BE16A0_SUB_NAME").val())                ,"param_value2": s_encode($("#BE16A0_SUB_CODE").val())                ,"param_value3": $("#BE16A0_LIMIT_DATE").val()                ,"param_value4": s_encode($("#BE16A0_S_DESC").val())                ,"param_value5": $("#BE16A0_CREATE_DATE").val()                ,"param_value6": s_encode($("#BE16A0_IP_ADDR").val())                ,"param_value7": $("#BE16A0_main_id").val()				
	            /*update param end*/
			};
		get_ajax_baseurl(inputdata, "BE16A0_get_N01_upd_t_sub_sys");
	}
}

//vue回调
function BE16A0_t_sub_sys_call_vue(objResult){
	if(index_subhtml == "XXXXXX")
	{
		
	}
	/*查询条件弹窗子页面*/
    /*get find subvue bgein*/
	
	/*get find subvue end*/
}

//for表单提交
$("#BE16A0_save_t_sub_sys_Edit").click(function () {
	$("form[name='BE16A0_DataModal']").submit();
})

/*修改数据*/
function BE16A0_get_N01_upd_t_sub_sys(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_upd_t_sub_sys) == true)
	{
		swal("修改数据成功!", "", "success");
		AE16A0_t_sub_sys_query();
		BE16A0_clear_validate();
		layer.close(BE16A0_param["ly_index"]);
	}
}

/*添加数据*/
function BE16A0_get_N01_ins_t_sub_sys(input) {
	layer.close(ly_index);
	if (Call_OpeResult(input.N01_ins_t_sub_sys) == true)
	{
		swal("添加数据成功!", "", "success");
		AE16A0_t_sub_sys_query();
		BE16A0_clear_validate();
		layer.close(BE16A0_param["ly_index"]);
	}
}

//取消编辑
$("#BE16A0_cancel_t_sub_sys_Edit").click(function () {
	layer.close(BE16A0_param["ly_index"]);
	BE16A0_clear_validate();
	$("[id^='BE16A0_div']").hide();
})

//清除查找框
function BE16A0_clear_input_cn_name(obj1,obj2){
	$("#"+obj1).val("");
	$("#"+obj2).val("-1");
}

//清除验证缓存
function BE16A0_clear_validate(){
	$("#BE16A0_DataModal").find(".has-error").each(function(){
		$(this).removeClass('has-error');
	});
	$("#BE16A0_DataModal").find(".has-success").each(function(){
	 	$(this).removeClass('has-success');
	});
	$("#BE16A0_DataModal").find(".glyphicon").each(function(){
	 	$(this).remove();
	});
}

//输入框重置
function BE16A0_clear_edit_info(){
	var inputs = $("#BE16A0_DataModal").find('input');
	var selects = $("#BE16A0_DataModal").find("select");
	var textareas = $("#BE16A0_DataModal").find('textarea');
	$.each(inputs, function (i, obj) {
		$(obj).val("");
	});
	$.each(selects, function (i, obj) {
		$(obj).val("");
	});
	$.each(textareas, function (i, obj) {
		$(obj).val("");
	});
	/*清除输入框验证信息*/
	/*input validate clear begin*/
	
	/*input validate clear end*/
	BE16A0_init_t_sub_sys();
}

//页面输入框赋值
function BE16A0_get_edit_info(){
	var rowData = $("#AE16A0_t_sub_sys_Events").bootstrapTable('getData')[AE16A0_select_t_sub_sys_rowId];
	var inputs = $("#BE16A0_DataModal").find('input');
	var selects = $("#BE16A0_DataModal").find("select");
	var textareas = $("#BE16A0_DataModal").find('textarea');
	
	//通用子页面输入框赋值
	Com_edit_info(rowData,inputs,selects,textareas,"AE16A0","BE16A0");
}

//form验证
function BE16A0_checkFormInput() {
    BE16A0_validate = $("#BE16A0_DataModal").validate({
        errorElement: 'span',
        errorClass: 'help-block',
        rules: {
        	/*input check rules begin*/
            BE16A0_main_id: {}            ,BE16A0_SUB_NAME: {}            ,BE16A0_SUB_CODE: {}            ,BE16A0_LIMIT_DATE: {date: true,required : true,maxlength:19}            ,BE16A0_S_DESC: {}            ,BE16A0_CREATE_DATE: {date: true,required : true,maxlength:19}            ,BE16A0_IP_ADDR: {}			
            /*input check rules end*/
        },
        messages: {
        	/*input check messages begin*/
            BE16A0_main_id: {}            ,BE16A0_SUB_NAME: {}            ,BE16A0_SUB_CODE: {}            ,BE16A0_LIMIT_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BE16A0_S_DESC: {}            ,BE16A0_CREATE_DATE: {date: "必须输入正确格式的日期",required : "必须输入正确格式的日期",maxlength:"长度不能超过19"}            ,BE16A0_IP_ADDR: {}			
            /*input check messages end*/
        },
        errorPlacement: function (error, element) {
            element.next().remove();
            element.after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error has-feedback');
        },
        success: function (label) {
            var el = label.closest('.form-group').find("input");
            el.next().remove();
            el.after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            label.closest('.form-group').removeClass('has-error').addClass("has-feedback has-success");
            label.remove();
        },
        submitHandler: function (form) {
        	BE16A0_SubmitForm();
        	return false;
        }
    })
}